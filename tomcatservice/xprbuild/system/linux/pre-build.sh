#! /bin/bash
## This script is used to setup the build environment. It is used setup the build and test environment. It performs
## either of these tasks
##   1. Install Linux Libraries
##   2. Setup environment
##   3. Download data required to perform build.
##
## DO NOT USE SUDO in the scripts. These scripts are run as sudo user

set -e

setup_jdk(){
echo "--------- Installing JDK ----------"
current_dir=$pwd
cd /tmp
wget https://download.java.net/java/GA/jdk10/10/binaries/openjdk-10_linux-x64_bin.tar.gz
tar xzvf openjdk-10_linux-x64_bin.tar.gz
sudo cp -TRv jdk-10 /usr/lib/jvm/java-10-openjdk-amd64/
sudo update-alternatives --install /usr/bin/java java /usr/lib/jvm/java-10-openjdk-amd64/bin/java 1
sudo update-alternatives --install /usr/bin/javac javac /usr/lib/jvm/java-10-openjdk-amd64/bin/javac 1
echo -e "1" | sudo update-alternatives --config java 
echo -e "1" | sudo update-alternatives --config javac
echo "--------- JDK Installation Done ----------"
java --version
cd $current_dir
}

setup_maven(){
	apt-get install -y maven
	return
	MAVEN_VERSION=3.5.4
	echo "-------- Installing Maven --------"
	cd ~
	wget http://mirrors.estointernet.in/apache/maven/maven-3/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz
	tar -xvzf apache-maven-${MAVEN_VERSION}-bin.tar.gz
	ln -s ~/apache-maven-${MAVEN_VERSION}/bin/mvn /usr/local/bin/mvn
	/usr/local/bin/mvn --version
	echo "-------- Maven Installation Done ------"
}

setup_node(){
	echo "-------- Installing NodeJs --------"
  wget https://nodejs.org/dist/v10.15.3/node-v10.15.3-linux-x64.tar.xz
  tar xvf node-v10.15.3-linux-x64.tar.xz
  cp -rf node-v10.15.3-linux-x64/bin /usr/local/
  cp -rf node-v10.15.3-linux-x64/include /usr/local/
  cp -rf node-v10.15.3-linux-x64/lib /usr/local/
  cp -rf node-v10.15.3-linux-x64/share /usr/local/
  node -v
  npm -v
	echo "-------- NodeJs Installation Done --------"
}

setup_tomcat(){
	echo "-------- Installing Tomcat --------"
  tomcat_v=8.5.4
  tomcat_dir=/usr/local/tomcat
  cd /tmp
  wget http://mirrors.estointernet.in/apache/tomcat/tomcat-9/v9.0.17/bin/apache-tomcat-9.0.17.tar.gz
  #wget https://archive.apache.org/dist/tomcat/tomcat-8/v${tomcat_v}/bin/apache-tomcat-${tomcat_v}.tar.gz
  mkdir $tomcat_dir -p
  tar xvf apache-tomcat-9.*.tar.gz -C $tomcat_dir --strip-components=1
	echo "-------- Tomcat Installation Done --------"
}

setup_jdk
#setup_tomcat
setup_maven
#setup_node

