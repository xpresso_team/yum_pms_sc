package com.yum.pa.service;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.yum.comp.service.AbstractDao;
import com.yum.pa.domain.ClasificationIFValue;
import com.yum.pa.domain.FunctionLTRating;
import com.yum.pa.domain.MyGoal;
import com.yum.pa.domain.OverAllRating;
import com.yum.pa.domain.PASheetData;
import com.yum.pa.domain.Rating;
import com.yum.pms.dao.AppraisalPeriod;
import com.yum.pms.dao.EmployeeNew;
import com.yum.pms.dao.EmployeeYearWise;
import com.yum.pms.dao.Goal;
import com.yum.pms.dao.GoalOrg;
import com.yum.pms.dao.GoalSection;
import com.yum.pms.dao.GoalSheet;
import com.yum.pms.dao.PA_ConfigIF;
import com.yum.pms.dao.PA_ConfigPromotionCriteria;
import com.yum.pms.dao.PA_ConfigRatingBellCurveIFRange;
import com.yum.pms.dao.PA_ConfigTF;
import com.yum.pms.dao.PA_EmployeeCurrentLevelSince;
import com.yum.pms.dao.PA_GoalRating;
import com.yum.pms.dao.PA_OverallRatingAdmin;
import com.yum.pms.dao.PA_OverallRatingLT;
import com.yum.pms.dao.PA_OverallRatingStatusMaster;
import com.yum.pms.dao.PA_OverallRatingSup;
import com.yum.pms.dao.PA_RatingMaster;
import com.yum.pms.dao.PA_SheetMaster;
import com.yum.pms.dao.PA_SheetStatusMaster;
import com.yum.pms.dao.PA_SheetStatusWorkFlow;
import com.yum.pms.dao.PaCultureRatingMstr;
import com.yum.pms.exception.CommonException;
import com.yum.pms.exception.YumPMSDataSaveException;
import com.yum.pms.service.AppraisalService;
import com.yum.pms.service.EmployeeServices;
import com.yum.pms.service.GoalServices;
import com.yum.pms.service.YumHibernateUtilServices;
import com.yum.pms.utils.SendMail;
import com.yum.pms.utils.YumPmsConstants;
import com.yum.pms.utils.YumPmsUtils;

/**
 * This DAO class will deal with PA module
 * @author dipak.swain
 */
/**
 * @author dipak.swain
 *
 */
@SuppressWarnings("unchecked")
@Repository
public class PADaoImpl  extends AbstractDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(PADaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private GoalServices goalService;

	@Autowired
	private EmployeeServices empService;
	
	@Autowired
	private AppraisalService apprService;
	
	@Autowired
	private YumHibernateUtilServices util;
	
	@Autowired
	private SendMail mailSender;

	public boolean testConnection() {
		Session session = YumHibernateUtilServices.getSession(sessionFactory);
		return session != null;
	}

	/**
	 * @param goalIds
	 * @return all goal section object from goalIds
	 */
	public List<GoalSection> getAllGoalSections(List<Integer> goalIds) {

		if(CollectionUtils.isEmpty(goalIds)) {
			return Collections.emptyList();
		}
		String hql = "FROM GoalSection AS GS WHERE GS.goal.goalId IN (:goalIds)";

		List<GoalSection> goalSectionList = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createQuery(hql)
				.setParameterList("goalIds", goalIds)
				.list();
		// Remove the rejected goal sections which are tagged
		String sql = "SELECT DISTINCT tag_to_goal_section_id FROM tagged_emp WHERE tag_accept_flag <> 1";
		Set<Integer> rejectedGoalSectionIds = new HashSet<>(YumHibernateUtilServices.getSession(sessionFactory).createSQLQuery(sql).list());
		if(CollectionUtils.isNotEmpty(rejectedGoalSectionIds) && CollectionUtils.isNotEmpty(goalSectionList)) {
			goalSectionList = 
					goalSectionList.stream()
					.filter(goalSection -> !rejectedGoalSectionIds.contains(goalSection.getGoalSectionId()))
					.collect(Collectors.toList());
		}
		return goalSectionList;
	}

	/**
	 * @param goalId
	 * @return
	 */
	public List<GoalSection> getGoalSectionList(Integer goalId) {

		if(goalId == null) {
			return Collections.emptyList();
		}
		return goalService.getGoalSectionListbyGoalId(new Goal(goalId), sessionFactory);
	}

	/**
	 * @param empId
	 * @param appraisalId
	 * @return
	 * @throws Exception
	 */
	public List<Goal> getMyGoalIdList(Integer empId, Integer appraisalId) {

		if(empId == null || appraisalId == null) {
			return Collections.emptyList();
		}
		GoalSheet goalSheet = getGoalSheetByEmpIdApprId(empId, appraisalId);
		if(goalSheet.getGoalSheetId() == 0) {
			LOGGER.info(YumPmsConstants.ERROR_GOAL_SHEET, goalSheet.getGoalSheetId());
			return Collections.emptyList();
		}
		return goalService.getGoalListbyGoalSheetId(goalSheet, sessionFactory);
	}

	/**
	 * @return List<PA_RatingMaster>
	 */
	public List<PA_RatingMaster> getPARatingMaster() {

		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(PA_RatingMaster.class)
				.add(Restrictions.eq(YumPmsConstants.ACTIVE, true))
				.list();
	}

	/**
	 * @param paSheetId
	 * @param goalId
	 * @return PA_GoalRating
	 */
	public List<PA_GoalRating> getPaGoalRating(Integer paSheetId, Integer goalId) {

		if(paSheetId == null || goalId == null) {
			return Collections.emptyList();
		}
		List<PA_GoalRating> goalRating = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(PA_GoalRating.class)
				.add(Restrictions.eq(YumPmsConstants.PA_SHEET, new PA_SheetMaster(paSheetId)))
				.add(Restrictions.eq(YumPmsConstants.GOAL, new Goal(goalId)))
				.add(Restrictions.eq(YumPmsConstants.ACTIVE, true))
				.list();
		return goalRating == null ? Collections.emptyList() : goalRating;
	}

	/**
	 * @param empId
	 * @param appraisalId
	 * @return PA_SheetMaster
	 */
	public PA_SheetMaster getPASheetData(Integer empId, Integer appraisalId) {

		if(empId == null || appraisalId == null) {
			return new PA_SheetMaster();
		}
		PA_SheetMaster sheetMaster = 
				(PA_SheetMaster)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(PA_SheetMaster.class)
				.add(Restrictions.eq(YumPmsConstants.EMPLOYEE_MODEL, new EmployeeNew(empId)))
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, new AppraisalPeriod(appraisalId)))
				.add(Restrictions.eq(YumPmsConstants.IS_ACTIVE, true))
				.uniqueResult();
		return sheetMaster == null ? new PA_SheetMaster() : sheetMaster;
	}

	/**
	 * @param paSheetMstr
	 * @return PA_OverallRatingSup
	 */
	public PA_OverallRatingSup getOverallRatingSup(PA_SheetMaster paSheetMstr) {

		if(paSheetMstr == null || YumPmsUtils.isNullOrZero(paSheetMstr.getPaSheetId())) {
			LOGGER.info(YumPmsConstants.ERROR_PA_SHEET_MASTER, paSheetMstr != null ? paSheetMstr.getPaSheetId() : 0);
			return new PA_OverallRatingSup();
		}
		PA_OverallRatingSup overallRating = 
				(PA_OverallRatingSup)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(PA_OverallRatingSup.class)
				.add(Restrictions.eq(YumPmsConstants.PA_SHEET, paSheetMstr))
				.uniqueResult();
		return overallRating == null ? new PA_OverallRatingSup() : overallRating;
	}

	/**
	 * @param paSheetMstr
	 * @return PA_OverallRatingLT
	 */
	public PA_OverallRatingLT getOverallRatingLT(PA_SheetMaster paSheetMstr) {

		if(paSheetMstr == null || YumPmsUtils.isNullOrZero(paSheetMstr.getPaSheetId())) {
			LOGGER.info(YumPmsConstants.ERROR_PA_SHEET_MASTER, paSheetMstr != null ? paSheetMstr.getPaSheetId() : 0);
			return new PA_OverallRatingLT();
		}
		PA_OverallRatingLT overallRating = 
				(PA_OverallRatingLT)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(PA_OverallRatingLT.class)
				.add(Restrictions.eq(YumPmsConstants.PA_SHEET, paSheetMstr))
				.uniqueResult();
		return overallRating == null ? new PA_OverallRatingLT() : overallRating;
	}

	/**
	 * @param paSheetMstr
	 * @return PA_OverallRatingLT
	 */
	public PA_OverallRatingAdmin getOverallRatingAdmin(PA_SheetMaster paSheetMstr) {

		if(paSheetMstr == null || paSheetMstr.getPaSheetId() == null || paSheetMstr.getPaSheetId() == 0) {
			LOGGER.info(YumPmsConstants.ERROR_PA_SHEET_MASTER, paSheetMstr != null ? paSheetMstr.getPaSheetId() : 0);
			return new PA_OverallRatingAdmin();
		}
		PA_OverallRatingAdmin overallRating = 
				(PA_OverallRatingAdmin)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(PA_OverallRatingAdmin.class)
				.add(Restrictions.eq(YumPmsConstants.PA_SHEET, paSheetMstr))
				.uniqueResult();
		return overallRating == null ? new PA_OverallRatingAdmin() : overallRating;
	}
	
	/**
	 * @param appraisalPeriod
	 * @return PA_OverallRatingLT
	 */
	public List<Integer> getAllPromotionCriteriaByApprId(Integer apprId) {

		if(YumPmsUtils.isNullOrZero(apprId)) {
			return Collections.emptyList();
		}
		String sql = String.format("SELECT DISTINCT T.promotion_criteria_id  FROM (SELECT DISTINCT promotion_criteria_id" 
				+ " FROM pa_overall_rating_Admin WHERE Admin_promo_nom = 1 AND promotion_criteria_id IS NOT NULL AND appraisal_period_id = :%s" 
				+ " UNION"
				+ " SELECT DISTINCT promotion_criteria_id FROM pa_overall_rating_LT WHERE LT_promo_nom = 1 AND promotion_criteria_id IS NOT NULL"
				+ " AND appraisal_period_id = :%s) AS T", YumPmsConstants.APPR_ID, YumPmsConstants.APPR_ID);
		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(sql)
				.setInteger(YumPmsConstants.APPR_ID, apprId)
				.list();

	}

	/**
	 * get the Rating of BellCurve IFRange
	 * @param apprId
	 * @param ratingId
	 * @return PA_ConfigRatingBellCurveIFRange
	 */
	public PA_ConfigRatingBellCurveIFRange getRatingBellCurveIFRange(Integer apprId, Integer ratingId) {

		if(YumPmsUtils.isNullOrZero(apprId) || YumPmsUtils.isNullOrZero(ratingId)) {
			return new PA_ConfigRatingBellCurveIFRange();
		}
		PA_ConfigRatingBellCurveIFRange bellCurveIFRange = 
				(PA_ConfigRatingBellCurveIFRange)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(PA_ConfigRatingBellCurveIFRange.class)
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, new AppraisalPeriod(apprId)))
				.add(Restrictions.eq(YumPmsConstants.PA_RATING, new PA_RatingMaster(ratingId)))
				.uniqueResult();
		return bellCurveIFRange == null ? new PA_ConfigRatingBellCurveIFRange() : bellCurveIFRange;
	}

	/**
	 * @param apprId
	 * @return All the PA_ConfigRatingBellCurveIFRange objects in that appraisal period
	 */
	public List<PA_ConfigRatingBellCurveIFRange> getRatingBellCurveIFRange(Integer apprId) {

		if(YumPmsUtils.isNullOrZero(apprId)) {
			return Collections.emptyList();
		}
		List<PA_ConfigRatingBellCurveIFRange> bellCurveIFRangeList = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(PA_ConfigRatingBellCurveIFRange.class)
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, new AppraisalPeriod(apprId)))
				.list();
		return CollectionUtils.isEmpty(bellCurveIFRangeList) ? Collections.emptyList() : bellCurveIFRangeList;
	}

	/**
	 * @param managerId
	 * @return List<Integer> All Reportees's Id
	 * @throws Exception 
	 */
	public List<Integer> getAllReportees(Integer managerId, Integer apprId) {

		if(YumPmsUtils.isNullOrZero(apprId)) {
			apprId = apprService.getDefaultAppraisalService().getAppraisalPeriodId();
		}
		List<Integer> empIds = null;
		try {
			empIds = empService.getNonDrEmpIdListByLtsId(managerId, apprId);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_FAIL_TO_FETCH, YumPmsConstants.EMPLOYEE_MODEL , e.getMessage());
			throw new HibernateException(e);
		}
		return CollectionUtils.isEmpty(empIds) ? Collections.emptyList() : empIds;
	}

	/**
	 * @return all the employee IDs
	 */
	public List<Integer> getAllEmployeeIds() {

		return YumPmsUtils.getEmptyListIfNull(empService.getAllActiveEmpIds());
	}

	/**
	 * This function returns manager of the passed employee
	 * @param empId
	 * @return Employee
	 */
	public EmployeeNew getManagerByEmployee(Integer empId, Integer apprId) {

		if(YumPmsUtils.isNullOrZero(empId)) {
			return new EmployeeNew();
		}
		EmployeeYearWise empYearWise = empService.getEmpYearWiseData(empId, apprId);
		return empYearWise != null && empYearWise.getManager() != null ? empYearWise.getManager() : new EmployeeNew();
	}

	/**
	 * @param orgGoalIds
	 * @param goalIds
	 * @return map of GoalId & GoalOrg object
	 */
	public Map<Integer, GoalOrg> getOrgGoalMapByGoalIds(List<Integer> orgGoalIds, List<Integer> goalIds) {

		if(CollectionUtils.isEmpty(orgGoalIds) || CollectionUtils.isEmpty(goalIds)) {
			LOGGER.info("Either orgGoalIds or goalIds are empty, orgGoalIds = {}, goalIds = {}", orgGoalIds, goalIds);
			return Collections.emptyMap();
		}
		String hql = String.format("FROM GoalOrg WHERE orgGoalId IN (:%s)", YumPmsConstants.ORG_GOAL_IDS);
		List<GoalOrg> orgGoalList =
				YumHibernateUtilServices.getSession(sessionFactory)
				.createQuery(hql)
				.setParameterList(YumPmsConstants.ORG_GOAL_IDS, orgGoalIds)
				.list();
		Map<Integer, GoalOrg> map = new HashMap<>(orgGoalIds.size());
		for(int i=0; i < goalIds.size(); i++) {
			map.put(goalIds.get(i), orgGoalList.get(i));
		}
		return map;
	}

	/**
	 * get OrgGoal By OrgGoalId
	 * @param orgGoalId
	 * @return GoalOrg
	 */
	public GoalOrg getOrgGoalByOrgGoalId(Integer orgGoalId) {

		if(YumPmsUtils.isNullOrZero(orgGoalId)) {
			return new GoalOrg();
		}
		return fetchById(GoalOrg.class, orgGoalId);
	}

	/**
	 * This method save all the PA_GoalRating list
	 * @param goalRatingList
	 * @return saved GoalRating List
	 * @throws YumPMSDataSaveException
	 */
	public List<PA_GoalRating> savePAGoalRating(List<PA_GoalRating> goalRatingList) throws YumPMSDataSaveException {

		if(CollectionUtils.isEmpty(goalRatingList)) {
			return Collections.emptyList();
		}
		Session session = null;
		List<PA_GoalRating> savedGoalRatingList = new ArrayList<>(goalRatingList.size());
		try {
			session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
			for(PA_GoalRating goalRating : goalRatingList) {
				goalRating = (PA_GoalRating) session.merge(goalRating);
				savedGoalRatingList.add(goalRating);
			}
			YumHibernateUtilServices.commitTransaction(session);
		} catch(Exception e) {
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException(YumPmsConstants.ERROR_FAIL_TO_SAVE , YumPmsConstants.PA_GOAL_RATING);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return savedGoalRatingList;
	}

	/**
	 * Save the PA_OverallRatingSup object
	 * @param overalRatingSup
	 * @return the saved PA_OverallRatingSup
	 * @throws YumPMSDataSaveException
	 */
	public PA_OverallRatingSup savePAOveralRatingSup(PA_OverallRatingSup overalRatingSup) throws YumPMSDataSaveException {

		if(overalRatingSup == null) {
			return new PA_OverallRatingSup();
		}
		Session session = null;
		try {
			session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
			overalRatingSup = (PA_OverallRatingSup) session.merge(overalRatingSup);
			YumHibernateUtilServices.commitTransaction(session);  
		} catch(Exception e) {
			YumHibernateUtilServices.rollbackTransaction(session);
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			throw new YumPMSDataSaveException(YumPmsConstants.ERROR_FAIL_TO_SAVE , YumPmsConstants.PA_OVERALL_RATING_SUP);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return overalRatingSup;
	}

	/**
	 * Save all PA_OverallRatingLT objects
	 * @param overallRatingLtList
	 * @return all saved PA_OverallRatingLT objects
	 * @throws YumPMSDataSaveException
	 */
	public List<PA_OverallRatingLT> savePaOveralRatingLt(List<PA_OverallRatingLT> overallRatingLtList) throws YumPMSDataSaveException {

		if(CollectionUtils.isEmpty(overallRatingLtList)) {
			return Collections.emptyList();
		}
		Session session = null;
		List<PA_OverallRatingLT> savedOverallRatingLtList = new ArrayList<>(overallRatingLtList.size());
		try {
			session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
			for(PA_OverallRatingLT overalRatingLT : overallRatingLtList) {
				overalRatingLT = (PA_OverallRatingLT) session.merge(overalRatingLT);
				savedOverallRatingLtList.add(overalRatingLT);
			}
			YumHibernateUtilServices.commitTransaction(session);   
		} catch(Exception e) {
			YumHibernateUtilServices.rollbackTransaction(session);
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			throw new YumPMSDataSaveException(YumPmsConstants.ERROR_FAIL_TO_SAVE , YumPmsConstants.PA_OVERALL_RATING_LT);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return savedOverallRatingLtList;
	}

	/**
	 * Save all PA_OverallRatingAdmin objects
	 * @param overallRatingAdminList
	 * @return all saved PA_OverallRatingAdmin objects
	 * @throws YumPMSDataSaveException
	 */
	public List<PA_OverallRatingAdmin> savePaOveralRatingAdmin(List<PA_OverallRatingAdmin> overallRatingAdminList) throws YumPMSDataSaveException {

		if(CollectionUtils.isEmpty(overallRatingAdminList)) {
			return Collections.emptyList();
		}
		Session session = null;
		List<PA_OverallRatingAdmin> savedOverallRatingLtList = new ArrayList<>(overallRatingAdminList.size());
		try {
			session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
			for(PA_OverallRatingAdmin overalRatingLT : overallRatingAdminList) {
				overalRatingLT = (PA_OverallRatingAdmin) session.merge(overalRatingLT);
				savedOverallRatingLtList.add(overalRatingLT);
			}
			YumHibernateUtilServices.commitTransaction(session);   
		} catch(Exception e) {
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException(YumPmsConstants.ERROR_FAIL_TO_SAVE , YumPmsConstants.PA_OVERALL_RATING_LT);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return savedOverallRatingLtList;
	}

	/**
	 * Save the PA_SheetMaster object
	 * @param paSheetMaster
	 * @return saved pa_SheetMaster object
	 * @throws YumPMSDataSaveException
	 */
	public PA_SheetMaster savePaSheetMaster(PA_SheetMaster paSheetMaster) throws YumPMSDataSaveException {

		if(paSheetMaster == null) {
			return new PA_SheetMaster();
		}
		Session session = null;
		try {
			session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
			paSheetMaster = (PA_SheetMaster) session.merge(paSheetMaster);
			YumHibernateUtilServices.commitTransaction(session);
		} catch(Exception e) {
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException(YumPmsConstants.ERROR_FAIL_TO_SAVE , YumPmsConstants.PA_SHEET_MASTER);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return paSheetMaster;
	}

	/**
	 * This is used to know the next work-flow status id 
	 * @param current OverallRating Status WorkFlow Id
	 * @return next Possible PaOverallRating Status Id
	 */
	public Integer getOverallRatingFlowNextStatus(Integer currentStatus) {

		if(currentStatus == null) {
			return 0;
		}
		String hql = "SELECT T.nextPossiblePaOverallRatingStatusId FROM PA_OverallRatingStatusWorkFlow T"
				+ " WHERE T.paOverallRatingStatusId = :currentStatus";
		return (Integer)YumHibernateUtilServices.getSession(sessionFactory)
				.createQuery(hql)
				.setInteger("currentStatus", currentStatus)
				.uniqueResult();
	}

	/**
	 * get PaSheet Work-Flow NextStatus
	 * @param currentStatus
	 * @return next work-flow
	 */
	public Integer getPaSheetFlowNextStatus(Integer currentStatus) {

		if(currentStatus == null) {
			return 0;
		}
		String hql = "SELECT T.nextPossiblePaSheetStatusId FROM PA_SheetStatusWorkFlow T"
				+ " WHERE T.paSheetStatusId = :currentStatus";
		return (Integer)YumHibernateUtilServices.getSession(sessionFactory)
				.createQuery(hql)
				.setInteger("currentStatus", currentStatus)
				.uniqueResult();
	}

	/**
	 * get GoalSheet By EmpId and ApprId
	 * @param empId
	 * @param appraisalId
	 * @return GoalSheet object
	 */
	public GoalSheet getGoalSheetByEmpIdApprId(Integer empId, Integer appraisalId) {

		if(empId == null || appraisalId == null) {
			return new GoalSheet();
		}
		GoalSheet goalSheet = null;
		try {
			goalSheet = goalService.getGoalSheetbyEmpId(new EmployeeNew(empId), new AppraisalPeriod(appraisalId), sessionFactory);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_FAIL_TO_FETCH, YumPmsConstants.GOAL_SHEET_TABLE , e.getMessage());
			throw new HibernateException(e);
		}
		if(goalSheet == null || goalSheet.getGoalSheetId() == 0) {
			return new GoalSheet();
		}
		return goalSheet;
	}

	/**
	 * fetch the persistence object by id
	 * @param clazz
	 * @param id
	 * @return the persistence object
	 */
	public <T> T fetchById(Class<T> clazz, Number id) {

		if(clazz == null || id == null) {
			throw new IllegalArgumentException(YumPmsConstants.ARGUMENT_CAN_NOT_NULL);
		}
		Session session = YumHibernateUtilServices.getSession(sessionFactory);
		T obj = session.get(clazz, id);
		if(obj == null) {
			LOGGER.error(YumPmsConstants.ERROR_FAIL_TO_FETCH, clazz.getName(), YumPmsConstants.IDENTIFIER_CAN_NOT_NULL);
			throw new IllegalArgumentException(String.format(YumPmsConstants.GIVEN_ID_NOT_AVAILABLE, clazz.getSimpleName()));
		}
		return obj;
	}
	
	/**
	 * fetch all the records from provided table
	 * @param clazz
	 * @return all the records
	 */
	public <T> List<T> fetchAll(Class<T> clazz) {
		if(clazz == null) {
			throw new IllegalArgumentException(YumPmsConstants.ARGUMENT_CAN_NOT_NULL);
		}
		return YumHibernateUtilServices.getSession(sessionFactory).createCriteria(clazz).list();
	}

	/**
	 * update PaSheetStatus in PA_SheetMaster
	 * @param paSheetId
	 * @param statusId
	 * @param updatedBy
	 * @return message; either success or failure
	 */
	public String updatePaSheetStatus(Integer paSheetId, Integer statusId, String updatedBy) {

		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
		String msg = StringUtils.EMPTY;
		try {
			PA_SheetMaster paStatusMaster = fetchById(PA_SheetMaster.class, paSheetId);
			if(paStatusMaster != null && BooleanUtils.toBoolean(paStatusMaster.getIsActive())) {
				paStatusMaster.setPaSheetStatus(new PA_SheetStatusMaster(statusId));
				paStatusMaster.setUpdatedBy(updatedBy);
				session.merge(paStatusMaster);
				YumHibernateUtilServices.commitTransaction(session);
				msg = YumPmsConstants.UPDATED_SUCESSFULLY;
			}
		} catch(Exception e) {
			YumHibernateUtilServices.rollbackTransaction(session);
			LOGGER.error(YumPmsConstants.ERROR_FAIL_TO_SAVE, YumPmsConstants.PA_SHEET_MASTER , e.getMessage());
			throw new HibernateException(YumPmsConstants.ERROR_FAIL_TO_UPDATE, e);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return msg;
	}

	/**
	 * update PaOverallRatingStatus in PA_SheetMaster
	 * @param paSheetId
	 * @param statusId
	 * @param updatedBy
	 * @return message; either success or failure
	 */
	public String updatePaOverallRatingStatus(Integer paSheetId, Integer statusId, String updatedBy) {

		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
		String msg = StringUtils.EMPTY;
		try {
			PA_SheetMaster paStatusMaster = fetchById(PA_SheetMaster.class, paSheetId);
			if(paStatusMaster != null) {
				paStatusMaster.setPaOverallRatingStatus(new PA_OverallRatingStatusMaster(statusId));
				paStatusMaster.setUpdatedBy(updatedBy);
				session.merge(paStatusMaster);
				YumHibernateUtilServices.commitTransaction(session);
				msg = YumPmsConstants.UPDATED_SUCESSFULLY;
			}
		} catch(Exception e) {
			YumHibernateUtilServices.rollbackTransaction(session);
			LOGGER.error(YumPmsConstants.ERROR_FAIL_TO_SAVE, YumPmsConstants.PA_SHEET_MASTER , e.getMessage());
			throw new HibernateException(YumPmsConstants.ERROR_FAIL_TO_UPDATE, e);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return msg;
	}

	/**
	 * get the PaSheetStatus By PaSheetId
	 * @param paSheetId
	 * @return PA_SheetStatusMaster by paSheetId
	 */
	public PA_SheetStatusMaster getPaSheetStatusByPaSheetId(Integer paSheetId) {

		if(YumPmsUtils.isNullOrZero(paSheetId)) {
			return new PA_SheetStatusMaster();
		}
		PA_SheetMaster paSheet = fetchById(PA_SheetMaster.class, paSheetId);
		if(paSheet != null && BooleanUtils.toBoolean(paSheet.getIsActive())) {
			return paSheet.getPaSheetStatus();
		}
		return new PA_SheetStatusMaster();
	}

	/**
	 * get the PaOverallRatingStatus By PaSheetId
	 * @param paSheetId
	 * @return PA_OverallRatingStatusMaster by paSheetId
	 */
	public PA_OverallRatingStatusMaster getPaOverallRatingStatusByPaSheetId(Integer paSheetId) {

		if(YumPmsUtils.isNullOrZero(paSheetId)) {
			return new PA_OverallRatingStatusMaster();
		}
		PA_SheetMaster paSheet = fetchById(PA_SheetMaster.class, paSheetId);
		if(paSheet != null) {
			return paSheet.getPaOverallRatingStatus();
		}
		return new PA_OverallRatingStatusMaster();
	}

	/**
	 * get PaStatus WorkFlows By StatusId
	 * @param paSheetStatusId
	 * @return
	 */
	public List<PA_SheetStatusWorkFlow> getPaStatusWorkFlowsByStatusId(Integer paSheetStatusId) {

		return YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(PA_SheetStatusWorkFlow.class)
				.add(Restrictions.eq(YumPmsConstants.PA_SHEET_STATUS_ID, paSheetStatusId))
				.list();
	}

	/**
	 * execute PaEmailNotification Procedure
	 * @param paSheetId
	 * @param param2
	 * @return all email sending parameters
	 */
	public List<Object[]> executePaEmailNotificationProcedure(Integer paSheetId, String param2) {
		if(util.isActiveSp(sessionFactory, "sp_EmailNotification_ByPA_java")) {
			String sql = String.format("exec sp_EmailNotification_ByPA_java :%s, :%s", YumPmsConstants.PARAM_1, YumPmsConstants.PARAM_2);
			List<Object[]> results = YumHibernateUtilServices.getSession(sessionFactory).createSQLQuery(sql)
					.setString(YumPmsConstants.PARAM_1,  String.valueOf(paSheetId))
					.setString(YumPmsConstants.PARAM_2, param2)
					.list();
			return CollectionUtils.isEmpty(results) ? Collections.emptyList() : results;
		}else {
			return Collections.emptyList();
		}
	}

	/**
	 * get If Value Percent By ApprId
	 * @param apprId
	 * @return IF_value_weighted_avg_indicator_percent column value
	 */
	public Double getIfValuePercentByApprId(int apprId) {

		String hql = String.format("SELECT T.ifValueWeightedAvgIndicatorPercent FROM PA_ConfigIF T WHERE T.appraisalPeriod = :%s",
				YumPmsConstants.APPR_ID);
		return (Double) YumHibernateUtilServices.getSession(sessionFactory)
				.createQuery(hql)
				.setParameter(YumPmsConstants.APPR_ID, new AppraisalPeriod(apprId))
				.uniqueResult();
	}
	
	/**
	 * Get PA_ConfigIF object by appraisal id
	 * @param apprId
	 * @return PA_ConfigIF object
	 */
	public PA_ConfigIF getPaConfigIf(int apprId) {

		return (PA_ConfigIF) YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(PA_ConfigIF.class)
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, new AppraisalPeriod(apprId)))
				.uniqueResult();
	}
	
	/**
	 * Get PA_ConfigTF object by appraisal id
	 * @param apprId
	 * @return PA_ConfigTF object
	 */
	public PA_ConfigTF getPaConfigTf(int apprId) {

		return (PA_ConfigTF) YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(PA_ConfigTF.class)
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, new AppraisalPeriod(apprId)))
				.uniqueResult();
	}

	/**
	 * get PaConfigTf By ApprId
	 * @param apprId
	 * @return
	 */
	public PA_ConfigTF getPaConfigTfByApprId(int apprId) {

		PA_ConfigTF paConfigTf =  
				(PA_ConfigTF)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(PA_ConfigTF.class)
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, new AppraisalPeriod(apprId)))
				.uniqueResult();
		return paConfigTf == null ? new PA_ConfigTF() : paConfigTf;
	}

	/**
	 * get Employee Tenure InCurrentLevel
	 * @param empId
	 * @return the date in which the given employee promoted to some level
	 */
	public Date getEmpTenureInCurrentLevel(int empId) {

		String hql = "SELECT T.inCurrentLevelSince FROM PA_EmployeeCurrentLevelSince T WHERE T.employee = :empId";
		return (Date)YumHibernateUtilServices.getSession(sessionFactory)
				.createQuery(hql)
				.setParameter(YumPmsConstants.EMP_ID, new EmployeeNew(empId))
				.uniqueResult();
	}

	/**
	 * get Employee Current Level Since
	 * @param empId
	 * @return 
	 */
	public PA_EmployeeCurrentLevelSince getEmpCurrentLevelSince(int empId) {

		return fetchById(PA_EmployeeCurrentLevelSince.class, empId);
	}
	
	/**
	 * The getPaEligibilityDate() will return all the provided emp_id and it's pa rating eligibility(true/false)
	 * @param apprId
	 * @param empIds
	 * @return Map<Integer, Boolean>
	 */
	public Map<Integer, Boolean> getPaEligibilityDate(Integer apprId, List<Integer> empIds) {

		LOGGER.info("getPaEligibilityDate({}, {})", apprId, empIds);
		if(apprId == null || CollectionUtils.isEmpty(empIds)) {
			return Collections.emptyMap();
		}
		String sql = "SELECT emp_id, (CASE WHEN DOJ_formatted <= pa_eligibility_date THEN 'true' ELSE 'false' END) AS ratingEnabled " + 
				"FROM (" + 
				"SELECT emp_id, DOJ_formatted, " + 
				"(SELECT pa_eligibility_date FROM appraisal_period WHERE appraisal_period_id = :apprId AND active = 1) AS pa_eligibility_date " + 
				"FROM employee_new WHERE emp_id IN (:empId)" + 
				") T";
		List<Object[]> result = YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(sql)
				.setInteger(YumPmsConstants.APPR_ID, apprId)
				.setParameterList(YumPmsConstants.EMP_ID, empIds)
				.list();
		if(CollectionUtils.isEmpty(result)) {
			return Collections.emptyMap();
		}
		return result.stream().collect(
				Collectors.toMap(
						object -> YumPmsUtils.castToInt(object[0]), 
						object -> BooleanUtils.toBoolean(String.valueOf(object[1]))
						)
				);
	}

	/**
	 * get All employee's VariablePay
	 * @param apprId
	 * @param empIds
	 * @return
	 */
	public Map<Integer, Double> getVariablePayEmpWise(Integer apprId, List<Integer> empIds) {

		if(apprId == null || CollectionUtils.isEmpty(empIds)) {
			return Collections.emptyMap();
		}
		String sql = String.format(YumPmsConstants.QUERY_FOR_GET_VPAY_WITH_EMP_ID, YumPmsConstants.APPR_ID, YumPmsConstants.EMP_ID);
		
		List<Object[]> result = YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(sql)
				.setInteger(YumPmsConstants.APPR_ID, apprId)
				.setParameterList(YumPmsConstants.EMP_ID, empIds)
				.list();

		if(CollectionUtils.isEmpty(result)) {
			return Collections.emptyMap();
		}
		return result.stream().collect(
				Collectors.toMap(
						object -> YumPmsUtils.castToInt(object[0]), 
						object -> YumPmsUtils.castToDouble(object[1])
						)
				);
	}

	public Integer getPreviousApprId(Integer apprId) {

		/* String sql = new StringBuilder("SELECT TOP 1 T2.appraisal_period_id FROM appraisal_period T1, appraisal_period T2 ")
				.append("WHERE T1.active = 1 AND T2.active = 1 AND T1.appraisal_period_id = :apprId ")
				.append("AND T2.period_start_yyyymm < T1.period_start_yyyymm ")
				.append("ORDER BY T2.period_start_yyyymm DESC").toString(); // (appraisal_desc - 1) has used to get previous Appr_Id 
				We will remove this SQL later*/
		String sql = "SELECT TOP 1 appraisal_period_id FROM appraisal_period WHERE active = 1 AND CAST(period_desc AS INT) ="
				+ " (SELECT CAST(period_desc AS INT) - 1 FROM appraisal_period WHERE appraisal_period_id = :apprId AND active = 1)";

		return (Integer)YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(sql)
				.setInteger(YumPmsConstants.APPR_ID, apprId)
				.uniqueResult();
	}

	/**
	 * save Employee Current Level Since
	 * @param object
	 * @return the same saved object
	 * @throws YumPMSDataSaveException
	 */
	public PA_EmployeeCurrentLevelSince saveEmpCurrentLevelSince(PA_EmployeeCurrentLevelSince object) throws YumPMSDataSaveException {

		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
		try {
			object = (PA_EmployeeCurrentLevelSince)session.merge(object);
			YumHibernateUtilServices.commitTransaction(session);
		} catch(Exception e) {
			YumHibernateUtilServices.rollbackTransaction(session);
			LOGGER.error(YumPmsConstants.ERROR_FAIL_TO_SAVE, e);
			throw new YumPMSDataSaveException(YumPmsConstants.ERROR_FAIL_TO_SAVE, e);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return object;
	}

	/**
	 * checkPromotionEligibility check promotion eligibility has or not
	 * @param sessionFactory
	 * @return
	 */
	public Boolean checkPromotionEligibility(PASheetData uiObject) {
	    String role = uiObject.getSaveByFlag();
	    List<ClasificationIFValue> listOfClasificationValues = uiObject.getSupRating().getListClasificationIFValue();
		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.doReturningWork(connection -> {
					try (CallableStatement function = connection.prepareCall(YumPmsConstants.FUN_PA_PROMOTION_ELIGIBILITY)) {
						function.registerOutParameter(1, Types.INTEGER);
						function.setInt(2, NumberUtils.toInt(uiObject.getEmpId()));
						function.setInt(3, NumberUtils.toInt(uiObject.getApprId())); 
						switch(StringUtils.trimToEmpty(StringUtils.lowerCase(role))) {
						case YumPmsConstants.ROLE_SUPERVISOR:
							function.setInt(4, uiObject.getSupRating().getRatingId()); 

							for(ClasificationIFValue ifNode : listOfClasificationValues) {
								checkNodes(function, ifNode); 
							}
							break;
						case YumPmsConstants.ROLE_LT:	
							function.setInt(4, uiObject.getFunLtRating().getRatingId()); 

							for(ClasificationIFValue ifNode : listOfClasificationValues) {
								checkNodes(function, ifNode); 
							}
							break;
						case YumPmsConstants.ROLE_ADMIN:
							function.setInt(4, uiObject.getAdminRating().getRatingId()); 

							for(ClasificationIFValue ifNode : listOfClasificationValues) {
								checkNodes(function, ifNode); 
							}
							break;
						default:
							LOGGER.info(YumPmsConstants.INFO_ABOUT_FUNCTION_PARAMITER);
							function.setInt(4, 0); 
							function.setDouble(5, 0); 
							break;
						}

						function.execute();
						return function.getInt(1) == 1;
					}
				});
	}

	private void checkNodes(CallableStatement function,
			ClasificationIFValue ifNode) throws SQLException {
		if(ifNode.getIsEditeable())
			function.setDouble(5, ifNode.getIfValue());
	}
	
	/**
	 * checkPromotionEligibilityCheck return the promotion criteria Id on which the promotion is eligible
	 * @param sessionFactory
	 * @return
	 */
	public Integer getPromotionCriteriaId(PASheetData saveObject) {

		String role = saveObject.getSaveByFlag();
		int ratingId = 0;
		List<ClasificationIFValue> classificationIfValues = null;
		switch(StringUtils.trimToEmpty(StringUtils.lowerCase(role))) {
		case YumPmsConstants.ROLE_SUPERVISOR:
			ratingId = YumPmsUtils.resolve(() -> saveObject.getSupRating().getRatingId().intValue()).orElse(0);
			classificationIfValues = YumPmsUtils.getEmptyListIfNull(saveObject.getSupRating().getListClasificationIFValue());
			break;
		case YumPmsConstants.ROLE_LT:
			ratingId = YumPmsUtils.resolve(() -> saveObject.getFunLtRating().getRatingId().intValue()).orElse(0);
			classificationIfValues = YumPmsUtils.getEmptyListIfNull(saveObject.getFunLtRating().getListClasificationIFValue());
			break;
		case YumPmsConstants.ROLE_ADMIN:
			ratingId = YumPmsUtils.resolve(() -> saveObject.getAdminRating().getRatingId().intValue()).orElse(0);
			classificationIfValues = YumPmsUtils.getEmptyListIfNull(saveObject.getAdminRating().getListClasificationIFValue());
			break;
		default:
			LOGGER.info(YumPmsConstants.INFO_ABOUT_FUNCTION_PARAMITER);
			ratingId = 0;
			classificationIfValues = Collections.emptyList();
			break;
		}
		ClasificationIFValue ifValueObj = 
				classificationIfValues.stream().filter(ClasificationIFValue :: getIsEditeable).findFirst().orElse(new ClasificationIFValue());
		final int paramRatingId = ratingId;
		final double paramIfValue = YumPmsUtils.resolve(() -> ifValueObj.getIfValue().doubleValue()).orElse(0.0);
		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.doReturningWork(connection -> {
					try (CallableStatement function = connection.prepareCall(YumPmsConstants.FUN_PA_PROMOTION_CRITERIA_ID)) {
						function.registerOutParameter(1, Types.INTEGER);
						function.setInt(2, NumberUtils.toInt(saveObject.getEmpId()));
						function.setInt(3, NumberUtils.toInt(saveObject.getApprId())); 
						function.setInt(4, paramRatingId); 
						function.setDouble(5, paramIfValue);

						function.execute();
						return function.getInt(1);
					}
				});
	}

	/**
	 * Send email to supervisor's supervisor
	 * @param paSheetId
	 */
	public void sendEmailToSup2(int paSheetId) {
		try {
			if(util.isActiveSp(sessionFactory, "sp_EmailNotification_PA_ToSup2_OnLTSubmission")) {
				Session session = YumHibernateUtilServices.getSession(sessionFactory);
				SQLQuery q = session.createSQLQuery(String.format(YumPmsConstants.SP_PA_EMAIL_TO_SUP_2, YumPmsConstants.PARAM_1));
				q.setString(YumPmsConstants.PARAM_1,  String.valueOf(paSheetId)); 
				List<Object[]> results = q.list();
				if(CollectionUtils.isNotEmpty(results)) {
					Object[] tuples = results.get(0);
					String mailTo = YumPmsUtils.toStringorNull(tuples[0]);
					String mailCc = YumPmsUtils.toStringorNull(tuples[1]);
					String subject = YumPmsUtils.toStringorNull(tuples[2]);
					String body = YumPmsUtils.toStringorNull(tuples[3]);
					mailSender.sendMail(mailTo, mailCc, subject, body, null);
				}
			}
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			throw new CommonException(ex);
		}
	}

	/**
	 * save or update Pa ConfigIf table
	 * @param paConfigIf
	 * @return saved PA_ConfigIF object
	 * @throws YumPMSDataSaveException
	 */
	public PA_ConfigIF savePaConfigIf(PA_ConfigIF paConfigIf) throws YumPMSDataSaveException {

		if(paConfigIf == null || paConfigIf.getAppraisalPeriod() == null 
				|| YumPmsUtils.isNullOrZero(paConfigIf.getAppraisalPeriod().getAppraisalPeriodId())) {
			return new PA_ConfigIF();
		}
		Session session = null;
		try {
			session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
			paConfigIf = (PA_ConfigIF) session.merge(paConfigIf);
			YumHibernateUtilServices.commitTransaction(session);
		} catch(Exception e) {
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException(YumPmsConstants.ERROR_FAIL_TO_SAVE , YumPmsConstants.PA_SHEET_MASTER);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return paConfigIf;
	}

	/**
	 * save or update Pa ConfigTf table
	 * @param paConfigTf
	 * @return saved PA_ConfigTF object
	 * @throws YumPMSDataSaveException
	 */
	public PA_ConfigTF savePaConfigTf(PA_ConfigTF paConfigTf) throws YumPMSDataSaveException {

		if(paConfigTf == null || paConfigTf.getAppraisalPeriod() == null 
				|| YumPmsUtils.isNullOrZero(paConfigTf.getAppraisalPeriod().getAppraisalPeriodId())) {
			return new PA_ConfigTF();
		}
		Session session = null;
		try {
			session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
			paConfigTf = (PA_ConfigTF) session.merge(paConfigTf);
			YumHibernateUtilServices.commitTransaction(session);
		} catch(Exception e) {
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException(YumPmsConstants.ERROR_FAIL_TO_SAVE , YumPmsConstants.PA_SHEET_MASTER);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return paConfigTf;
	}

	/**
	 * save all parameterized PA_ConfigRatingBellCurveIFRange records
	 * @param bellCurveIfRangeList
	 * @return the saved PA_ConfigRatingBellCurveIFRange records
	 * @throws YumPMSDataSaveException
	 */
	public List<PA_ConfigRatingBellCurveIFRange> savePaBellCurveIfRange(List<PA_ConfigRatingBellCurveIFRange> bellCurveIfRangeList) 
			throws YumPMSDataSaveException {

		if(CollectionUtils.isEmpty(bellCurveIfRangeList)) {
			return Collections.emptyList();
		}
		List<PA_ConfigRatingBellCurveIFRange> savedBellCurveIfRangeList = new ArrayList<>(bellCurveIfRangeList.size());
		Session session = null;
		try {
			session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
			for (PA_ConfigRatingBellCurveIFRange bellCurveIfRange : bellCurveIfRangeList) {
				bellCurveIfRange = (PA_ConfigRatingBellCurveIFRange) session.merge(bellCurveIfRange);
				savedBellCurveIfRangeList.add(bellCurveIfRange);
			}
			YumHibernateUtilServices.commitTransaction(session);
		} catch(Exception e) {
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException(YumPmsConstants.ERROR_FAIL_TO_SAVE , YumPmsConstants.PA_CONFIG_RATING_BELL_CURVE_IF_RANGE);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return savedBellCurveIfRangeList;
	}
	
	/**
	 * This is used in Administration Promotion criteria screen
	 * Get the LTS IDP Options from idp_performance_category_ui_config table
	 */
	public Map<String, String> getLtsIdpOptions() {

		String sql = new StringBuilder("SELECT T1.UI_control_value, T1.UI_control_label ")
				.append("FROM idp_performance_category_ui_config T1 INNER JOIN idp_performance_mstr T2 ")
				.append("ON T1.idp_performance_id = T2.idp_performance_id ")
				.append("AND T2.idp_performance_desc = 'LTS Rating' ")		
				.append("AND T2.is_active = 1 AND T1.is_active = 1").toString();
		List<Object[]> result = YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(sql)
				.list();
		if(CollectionUtils.isEmpty(result)) {
			return Collections.emptyMap();
		}
		return result.stream().collect(
				Collectors.toMap(
						object -> YumPmsUtils.toString(object[0]), 
						object -> YumPmsUtils.toString(object[1])
						)
				);

	}
	
	/**
	 * get ConfigPromotionCriteria by appraisal id
	 * @param apprId
	 * @return List<PA_ConfigPromotionCriteria> objects
	 */
	public List<PA_ConfigPromotionCriteria> getConfigPromotionCriteria(Integer apprId) {

		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(PA_ConfigPromotionCriteria.class)
				.add(Restrictions.eq(YumPmsConstants.ACTIVE, true))
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, new AppraisalPeriod(apprId)))
				.list();
	}
	
	/**
	 * save ConfigPromotionCriteria objects
	 * @param criterias
	 * @throws YumPMSDataSaveException
	 */
	public List<PA_ConfigPromotionCriteria> saveConfigPromotionCriteria(List<PA_ConfigPromotionCriteria> criterias) throws YumPMSDataSaveException {
		
		if(CollectionUtils.isEmpty(criterias)) {
			throw new YumPMSDataSaveException(YumPmsConstants.ERROR_PROMOTION_CRITERIAS_BLANK);
		}
		List<PA_ConfigPromotionCriteria> savedcriterias = new ArrayList<>(criterias.size());
		Session session = null;
		try {
			session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
			for (PA_ConfigPromotionCriteria criteria : criterias) {
				criteria = (PA_ConfigPromotionCriteria) session.merge(criteria);
				savedcriterias.add(criteria);
			}
			YumHibernateUtilServices.commitTransaction(session);
		} catch(Exception e) {
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException(YumPmsConstants.ERROR_FAIL_TO_SAVE , e);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return savedcriterias;
	}
	
	/**
	 * Get all unique IF logic from pa_config_promotion_criteria table
	 * @return List<String>
	 */
	public List<String> getAllIfLogic() {
		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery("SELECT DISTINCT IF_check_logic FROM pa_config_promotion_criteria WHERE IsActive = 1 AND IF_check_logic IS NOT NULL")
				.list();
	}

	// ---------------------------------------- Other mapping related functionalities goes here -----------------------------------------------
	/**
	 * @param paSheetMstr
	 * @return OverAllRating
	 */
	public OverAllRating getOverAllRating(PA_SheetMaster paSheetMstr) {

		OverAllRating overAllRating = new OverAllRating();
		if(paSheetMstr.getPaSheetId() == null || paSheetMstr.getPaSheetId() == 0) {
			LOGGER.info(YumPmsConstants.ERROR_PA_SHEET_MASTER, paSheetMstr.getPaSheetId());
			overAllRating.setRatingsList(getRatingDetailsList());
			return overAllRating;
		}

		PA_OverallRatingSup overallRatingSup = getOverallRatingSup(paSheetMstr);
		if(overallRatingSup != null && YumPmsUtils.isNotNullAndNonZero(overallRatingSup.getPaOverallRatingSupId())) { 
			overAllRating.setOverAllRatingId(overallRatingSup.getPaOverallRatingSupId());
			PA_RatingMaster paRating = overallRatingSup.getPaRating();
			PaCultureRatingMstr cultureRatingMstr = overallRatingSup.getCultureRatingMstr();
			if(paRating != null) {
				overAllRating.setRatingId(paRating.getPaRatingId());
				overAllRating.setRatingDesc(paRating.getPaRatingDesc());
				overAllRating.setRatingValue(paRating.getPaRatingShortDesc());

				overAllRating.setCultureRatingId(null != cultureRatingMstr? cultureRatingMstr.getCultureRatingId() : null);
				overAllRating.setCultureRatingDesc(null != cultureRatingMstr? cultureRatingMstr.getCultureRatingDesc() : null);
				overAllRating.setCultureRatingValue(null != cultureRatingMstr? cultureRatingMstr.getPaCulturalRatingShortDesc() : null); 
			}
			overAllRating.setRemarks(overallRatingSup.getSupRemark());
			overAllRating.setPromotion(overallRatingSup.getSupPromoNom());
			// Promotion Nomination for August month added as per client request
			overAllRating.setPromoNomAug(BooleanUtils.toBoolean(overallRatingSup.getSupPromoNomAug()));
			
			// Dev CheckIn Box
			overAllRating.setDevCheckIn(overallRatingSup.getDevCheckIn());
			overAllRating.setDevCheckinText(overallRatingSup.getDevCheckInRremarks()); 

			Integer apprId = 0;
			if(overallRatingSup.getPaSheet().getAppraisalPeriod() != null) {
				apprId = overallRatingSup.getPaSheet().getAppraisalPeriod().getAppraisalPeriodId();
			}
			Integer paRatingId = overallRatingSup.getPaRating() != null ? overallRatingSup.getPaRating().getPaRatingId() : 0;
			PA_ConfigRatingBellCurveIFRange ifRange = getRatingBellCurveIFRange(apprId, paRatingId);
			overAllRating.setIfRangeUpper(YumPmsUtils.resolve(ifRange :: getIfRangeValueUpper).orElse(0.0)); 
			overAllRating.setIfRangeLower(YumPmsUtils.resolve(ifRange :: getIfRangeValueLower).orElse(0.0)); 
		}
		// set If value with classification depend on designation
		overAllRating.setListClasificationIFValue(getDesignationTenure(paSheetMstr, null != overallRatingSup ? overallRatingSup.getSupIf(): null)); 
		overAllRating.setRatingsList(getRatingDetailsList()); 
		overAllRating.setCultureRatingsList(getCultureRatingDetailsList());
		return  overAllRating;
	}

	/**
	 * @param paSheetMstr
	 * @return FunctionLTRating
	 */
	public FunctionLTRating getFunctionLTRating(PA_SheetMaster paSheetMstr) {

		FunctionLTRating rating = new FunctionLTRating();
		if(paSheetMstr.getPaSheetId() == null || paSheetMstr.getPaSheetId() == 0) {
			LOGGER.info(YumPmsConstants.ERROR_PA_SHEET_MASTER, paSheetMstr.getPaSheetId());
			return rating;
		}
		PA_OverallRatingLT ltRating = getOverallRatingLT(paSheetMstr);
		if(ltRating != null && YumPmsUtils.isNotNullAndNonZero(ltRating.getPaOverallRatingLTId())) {
			rating.setOverAllRatingId(ltRating.getPaOverallRatingLTId());
			rating.setRemarks(ltRating.getLtRemark());
			rating.setPromotion(ltRating.getLtPromoNom());
			rating.setPromoNomAug(BooleanUtils.toBoolean(ltRating.getLtPromoNomAug()));

			PA_RatingMaster paRating = ltRating.getPaRating();
			PaCultureRatingMstr cultureRatingMstr = ltRating.getCultureRatingMstr();
			if(paRating != null) {
				rating.setRatingId(paRating.getPaRatingId()); 
				rating.setRatingDesc(paRating.getPaRatingDesc());
				rating.setRatingValue(paRating.getPaRatingShortDesc());

				rating.setCultureRatingId(null != cultureRatingMstr? cultureRatingMstr.getCultureRatingId() : null);
				rating.setCultureRatingDesc(null != cultureRatingMstr? cultureRatingMstr.getCultureRatingDesc() : null);
				rating.setCultureRatingValue(null != cultureRatingMstr? cultureRatingMstr.getPaCulturalRatingShortDesc() : null); 
			}
			rating.setAdditionalIncreaseNom(ltRating.getAdditionalIncreaseNom());

			// Dev CheckIn Box
			rating.setDevCheckIn(ltRating.getDevCheckIn());
			rating.setDevCheckinText(ltRating.getDevCheckInRremarks()); 
			Integer apprId = 0;
			if(ltRating.getPaSheet().getAppraisalPeriod() != null) {
				apprId = ltRating.getPaSheet().getAppraisalPeriod().getAppraisalPeriodId();
			}
			Integer paRatingId = ltRating.getPaRating() != null ? ltRating.getPaRating().getPaRatingId() : 0;
			rating.setIsEligable(ltRating.getIsEligable());

			PA_ConfigRatingBellCurveIFRange ifRange = getRatingBellCurveIFRange(apprId, paRatingId);
			rating.setIfRangeUpper(YumPmsUtils.resolve(ifRange :: getIfRangeValueUpper).orElse(0.0)); 
			rating.setIfRangeLower(YumPmsUtils.resolve(ifRange :: getIfRangeValueLower).orElse(0.0)); 
		}
		// set If value with classification depend on designation
		rating.setListClasificationIFValue(getDesignationTenure(paSheetMstr, null != ltRating ? ltRating.getLtIF(): null)); 
		rating.setRatingsList(getRatingDetailsList());
		rating.setCultureRatingsList(getCultureRatingDetailsList());
		return rating;
	}

	/**
	 * @param paSheetMstr
	 * @return FunctionLTRating
	 */
	public FunctionLTRating getAdminRating(PA_SheetMaster paSheetMstr) {

		FunctionLTRating rating = new FunctionLTRating();
		if(YumPmsUtils.isNullOrZero(paSheetMstr.getPaSheetId())) {
			LOGGER.info(YumPmsConstants.ERROR_PA_SHEET_MASTER, paSheetMstr.getPaSheetId());
			return rating;
		}
		PA_OverallRatingAdmin adminRating = getOverallRatingAdmin(paSheetMstr);
		if(adminRating != null && YumPmsUtils.isNotNullAndNonZero(adminRating.getPaOverallRatingAdminId())) {
			rating.setOverAllRatingId(adminRating.getPaOverallRatingAdminId());
			rating.setRemarks(adminRating.getAdminRemark());
			rating.setPromotion(adminRating.getAdminPromoNom());
			rating.setPromoNomAug(BooleanUtils.toBoolean(adminRating.getAdminPromoNomAug()));

			PA_RatingMaster paRating = adminRating.getPaRating();
			PaCultureRatingMstr cultureRatingMstr = adminRating.getCultureRatingMstr();
			if(paRating != null) {
				rating.setRatingId(paRating.getPaRatingId()); 
				rating.setRatingDesc(paRating.getPaRatingDesc());
				rating.setRatingValue(paRating.getPaRatingShortDesc());

				rating.setCultureRatingId(null != cultureRatingMstr? cultureRatingMstr.getCultureRatingId() : null);
				rating.setCultureRatingDesc(null != cultureRatingMstr? cultureRatingMstr.getCultureRatingDesc() : null);
				rating.setCultureRatingValue(null != cultureRatingMstr? cultureRatingMstr.getPaCulturalRatingShortDesc() : null); 
			}
			rating.setAdditionalIncreaseNom(adminRating.getAdditionalIncreaseNom());
			// Dev CheckIn Box
			rating.setDevCheckIn(adminRating.getDevCheckIn());
			rating.setDevCheckinText(adminRating.getDevCheckInRremarks()); 

			Integer apprId = 0;
			if(adminRating.getPaSheet().getAppraisalPeriod() != null) {
				apprId = adminRating.getPaSheet().getAppraisalPeriod().getAppraisalPeriodId();
			}
			Integer paRatingId = adminRating.getPaRating() != null ? adminRating.getPaRating().getPaRatingId() : 0;

			PA_ConfigRatingBellCurveIFRange ifRange = getRatingBellCurveIFRange(apprId, paRatingId);
			rating.setIfRangeUpper(YumPmsUtils.resolve(ifRange :: getIfRangeValueUpper).orElse(0.0)); 
			rating.setIfRangeLower(YumPmsUtils.resolve(ifRange :: getIfRangeValueLower).orElse(0.0)); 
		}
		// set If value with classification depend on designation
		rating.setListClasificationIFValue(getDesignationTenure(paSheetMstr, null != adminRating ? adminRating.getAdminIfValue(): null)); 
		rating.setRatingsList(getRatingDetailsList());
		rating.setCultureRatingsList(getCultureRatingDetailsList());
		return rating;
	}

	/**
	 * @param goalId
	 * @return List<MyGoal>
	 */
	public List<MyGoal> getMyGoalSectionList(Integer goalId) {

		if(goalId == null) {
			Collections.emptyList();
		}
		return getGoalSectionList(goalId).stream().map(MyGoal :: new).collect(Collectors.toList());
	}

	/**
	 * @return List<Rating>
	 */
	public List<Rating> getRatingDetailsList() {

		return getPARatingMaster().stream().map(Rating :: new).collect(Collectors.toList());
	}

	public Map<Integer, List<MyGoal>> getMyGoalMapByGoalIds(List<Integer> goalIds) {

		List<GoalSection> allGoalSections = getAllGoalSections(goalIds);
		if(CollectionUtils.isEmpty(allGoalSections)) {
			return Collections.emptyMap();
		}
		Map<Integer, List<GoalSection>> collect = 
				allGoalSections.stream()
				.collect(
						Collectors.groupingBy(
								YumPmsUtils :: getGoalIdFromGoalSection, 
								HashMap :: new, 
								Collectors.toList()
								)
						);
		if(MapUtils.isEmpty(collect)) {
			return Collections.emptyMap();
		}
		Map<Integer, List<MyGoal>> resultMap = new HashMap<>(collect.size());
		for(Map.Entry<Integer, List<GoalSection>> entry : collect.entrySet()) {
			resultMap.put(entry.getKey(), entry.getValue().stream().map(MyGoal :: new).collect(Collectors.toList()));
		}
		return resultMap;
	}
	
	/**
	 * @param empId
	 * @param appraisalPeriodId
	 * @return
	 */
	public PA_OverallRatingAdmin getOverallRatingAdminbyEmpId(Integer empId, Integer appraisalPeriodId) {

		if(empId == null || appraisalPeriodId == null || empId == 0 || appraisalPeriodId == 0) {
			LOGGER.info(YumPmsConstants.ERROR_DUE_TO_GET_PA_RATING_ADMIN, 0);
			return new PA_OverallRatingAdmin();
		}
		PA_OverallRatingAdmin overallRating = 
				(PA_OverallRatingAdmin)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(PA_OverallRatingAdmin.class)
				.add(Restrictions.eq(YumPmsConstants.EMP, new EmployeeNew(empId)))
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, new AppraisalPeriod(appraisalPeriodId)))
				.uniqueResult();
		return overallRating == null ? new PA_OverallRatingAdmin() : overallRating;
	}
	
	/**
	 * @return PaCultureRatingMstr
	 */
	public List<PaCultureRatingMstr> getPACultureRatingMaster() {

		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(PaCultureRatingMstr.class)
				.add(Restrictions.eq(YumPmsConstants.ACTIVE, true))
				.list();
	}
	
	/**
	 * @return Rating
	 */
	public List<Rating> getCultureRatingDetailsList() {
		return getPACultureRatingMaster().stream().map(Rating :: new).collect(Collectors.toList());
	}
	
	private List<ClasificationIFValue> getDesignationTenure(PA_SheetMaster sheetMastr, Double ifValue) {
		List<ClasificationIFValue> clasificationIFValueList = new ArrayList<>();
		List<Map<String, String>> listOfEmpIfTenureMap = executeDynamicProcedure(YumPmsConstants.SP_EMPLOYEE_DESIGNATION_TUNERE, sheetMastr.getEmployee().getEmpId(), sheetMastr.getAppraisalPeriod().getAppraisalPeriodId());
		for(Map<String, String> empIfTenureMap : listOfEmpIfTenureMap) {
			ClasificationIFValue ifClasification = new ClasificationIFValue();
			for(Map.Entry<String,String> entry : empIfTenureMap.entrySet()) {
				
				if(entry.getKey().equalsIgnoreCase(YumPmsConstants.OUT_PARAM_DESIGNATION)) {
					ifClasification.setDesignation(entry.getValue());
					if(entry.getValue().equalsIgnoreCase(YumPmsConstants.VALUE_AREA_COACH) || entry.getValue().equalsIgnoreCase(YumPmsConstants.VALUE_SR_AREA_COACH)) {
						ifClasification.setIsEditeable(false);
						ifClasification.setIfValue(0.0); 
					}else {
						ifClasification.setIsEditeable(true);
						ifClasification.setIfValue(ifValue);
					}						
				}else if(entry.getKey().equalsIgnoreCase(YumPmsConstants.OUT_PARAM_DAYS)) {
					ifClasification.setTenure(NumberUtils.toInt(entry.getValue()));
				}else if(entry.getKey().equalsIgnoreCase(YumPmsConstants.OUT_PARAM_VPAY)) {
					ifClasification.setvPay(NumberUtils.toDouble(entry.getValue())); 
				}else if(entry.getKey().equalsIgnoreCase(YumPmsConstants.OUT_PARAM_BUDGET)) {
					ifClasification.setIndivusalBudget(NumberUtils.toDouble(entry.getValue())); 
				}else if(entry.getKey().equalsIgnoreCase(YumPmsConstants.OUT_PARAM_CTC)) {
					ifClasification.setCtc(NumberUtils.toDouble(entry.getValue()));  
				}
			}
			clasificationIFValueList.add(ifClasification);
		}
		return clasificationIFValueList;
	}
	
	public void updateCompLetterEmailStatus(String empIds, Integer apprId, String purpose) {
		
		/*String sql = "UPDATE pa_sheet_mstr SET letter_email_sending_status = 1 WHERE appraisal_period_id = :apprId AND emp_id IN(:empId)";
		List<Integer> arrEmpIds = Arrays.asList(empIds.split(YumPmsConstants.COMMA))
				.stream().map(StringUtils :: trim).map(NumberUtils :: toInt).filter(empId -> empId != 0).collect(Collectors.toList());
		if(CollectionUtils.isEmpty(arrEmpIds)) {
			return false;
		}
		int count = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(sql)
				.setParameter(YumPmsConstants.APPR_ID, apprId)
				.setParameterList(YumPmsConstants.EMP_ID, arrEmpIds)
				.executeUpdate();
		return count == arrEmpIds.size();*/
		if(StringUtils.isNotEmpty(empIds)) {
			executeProcedure(YumPmsConstants.SP_COMP_LETTER_STATUS, empIds, apprId, purpose, YumPmsConstants.EMAIL, 1);
		}
	}
	
	public void updateCompLetterPermission(String empIds, Integer apprId, String purpose, int status) {
		
		/*String sql = " UPDATE pa_sheet_mstr SET letter_download_permission = :status WHERE appraisal_period_id = :apprId AND emp_id IN(:empId)";
		int count = 
			YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(sql)
				.setParameter(YumPmsConstants.STATUS, status)
				.setParameter(YumPmsConstants.APPR_ID, apprId)
				.setParameterList(YumPmsConstants.EMP_ID, empIds)
				.executeUpdate();
		return count == empIds.size();*/
		if(StringUtils.isNotEmpty(empIds)) {
			executeProcedure(YumPmsConstants.SP_COMP_LETTER_STATUS, empIds, apprId, purpose, YumPmsConstants.DOWNLOAD, status);
		}
	}
}
