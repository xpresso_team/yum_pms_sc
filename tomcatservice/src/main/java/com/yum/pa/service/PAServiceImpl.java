package com.yum.pa.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yum.comp.service.CompensationDao;
import com.yum.pa.domain.ClasificationIFValue;
import com.yum.pa.domain.EmployeeSalaryInfo;
import com.yum.pa.domain.FunctionLTRating;
import com.yum.pa.domain.MyGoal;
import com.yum.pa.domain.PASheetData;
import com.yum.pa.domain.Rating;
import com.yum.pa.domain.SelfSheetData;
import com.yum.pms.dao.AppraisalPeriod;
import com.yum.pms.dao.CompRevisedSalaryEffectiveDuration;
import com.yum.pms.dao.EmployeeNew;
import com.yum.pms.dao.EmployeeYearWiseNotEffective;
import com.yum.pms.dao.Goal;
import com.yum.pms.dao.GoalOrg;
import com.yum.pms.dao.GoalSection;
import com.yum.pms.dao.GoalSheet;
import com.yum.pms.dao.PA_ConfigPromotionPercentIndicator;
import com.yum.pms.dao.PA_ConfigRatingBellCurveIFRange;
import com.yum.pms.dao.PA_GoalRating;
import com.yum.pms.dao.PA_OverallRatingAdmin;
import com.yum.pms.dao.PA_OverallRatingLT;
import com.yum.pms.dao.PA_OverallRatingStatusMaster;
import com.yum.pms.dao.PA_OverallRatingSup;
import com.yum.pms.dao.PA_RatingMaster;
import com.yum.pms.dao.PA_SheetMaster;
import com.yum.pms.dao.PA_SheetStatusMaster;
import com.yum.pms.dao.PA_SheetStatusWorkFlow;
import com.yum.pms.dao.PaCultureRatingMstr;
import com.yum.pms.domain.EmployeeCurrentDetails;
import com.yum.pms.domain.StatusWorkFlow;
import com.yum.pms.exception.CommonException;
import com.yum.pms.exception.YumPMSDataSaveException;
import com.yum.pms.service.AppraisalService;
import com.yum.pms.service.EmployeeServices;
import com.yum.pms.utils.DateTimeUtils;
import com.yum.pms.utils.ResourceUtils;
import com.yum.pms.utils.SendMail;
import com.yum.pms.utils.WorkFlowStatus;
import com.yum.pms.utils.YumPmsConstants;
import com.yum.pms.utils.YumPmsUtils;

/**
 * @author dipak.swain
 * @since 04/18/2018
 * 
 * Service Implementation for PA module
 */
@Service
public class PAServiceImpl implements IPAService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PAServiceImpl.class);
	
	@Autowired
	private PADaoImpl paDao;
	
	@Autowired
	private EmployeeServices employeeService;
	
	@Autowired
	private AppraisalService appraisalService;
	
	@Autowired
	private CompensationDao compDao;
	
	@Autowired
	private SendMail mailSender;
	
	/**
	 * getPASheetData(String empId, String appraisalId, String role)
	 * return List<PASheetData>
	 */
	@Override
	public List<PASheetData> getPASheetData(Integer empId, Integer appraisalId, String role) {

		LOGGER.info("PAServiceImpl.getPASheetData({}, {})", empId, appraisalId);
		if(YumPmsUtils.isNullOrZero(empId) || YumPmsUtils.isNullOrZero(appraisalId) || StringUtils.isEmpty(role)) {
			return Collections.emptyList();
		}
		
		List<PASheetData> paSheetDataResult = null;
		switch(StringUtils.trimToEmpty(StringUtils.lowerCase(role))) {
		case YumPmsConstants.ROLE_SELF:
			paSheetDataResult = Arrays.asList(getPaSheetDataByEmpIdApprId(empId, appraisalId, true));
			break;
		case YumPmsConstants.ROLE_SUPERVISOR:
			paSheetDataResult = getSupervisorPaSheetData(empId, appraisalId);
			break;
		case YumPmsConstants.ROLE_LT:
			paSheetDataResult = getLtPaSheetData(empId, appraisalId);
			if(CollectionUtils.isNotEmpty(paSheetDataResult)) { 
				paSheetDataResult.get(0).setIfWeightedPercentage(paDao.getIfValuePercentByApprId(appraisalId));
			}
			break;
		case YumPmsConstants.ROLE_ADMIN:
			paSheetDataResult = getAdminPaSheetData(YumPmsUtils.intToStr(empId), appraisalId);
			break;
		default:
			paSheetDataResult = Collections.emptyList();
			break;
		}
		if(!YumPmsConstants.ROLE_SELF.equalsIgnoreCase(role)) {
			setRatingEnableFlag(appraisalId, paSheetDataResult);
		}
		return CollectionUtils.isEmpty(paSheetDataResult) ? Collections.emptyList() : Collections.unmodifiableList(paSheetDataResult);
	}

	/**
	 * Save All PA Sheet data list and return the saved data list
	 * @param List<PASheetData> saveObjectList
	 * @throws YumPMSDataSaveException
	 * @return List<PASheetData>
	 */
	@Override
	public List<PASheetData> savePASheetData(List<PASheetData> saveObjectList) throws YumPMSDataSaveException {
		
		if(CollectionUtils.isEmpty(saveObjectList)) {
			return Collections.emptyList();
		}	
		
		PASheetData saveObject = saveObjectList.get(0);
		saveObject = null != saveObject ? saveObject : new PASheetData();
		String role = saveObject.getSaveByFlag();
		Integer empId = NumberUtils.toInt(saveObject.getEmpId());
		Integer appraisalId = NumberUtils.toInt(saveObject.getApprId());
		
		List<PASheetData> paSheetDataResult = null;
		switch(StringUtils.trimToEmpty(StringUtils.lowerCase(role))) {
		case YumPmsConstants.ROLE_SELF:
			saveSelfPaSheetData(saveObject);	
			paSheetDataResult = Arrays.asList(getPaSheetDataByEmpIdApprId(empId, appraisalId, true));
			break;
		case YumPmsConstants.ROLE_SUPERVISOR:
			if(saveObject.getSupRating() != null) {
				saveSupervisorPaSheetData(saveObject);			
				paSheetDataResult = getSupervisorPaSheetData(empId, appraisalId);
			}
			break;
		case YumPmsConstants.ROLE_LT:
			boolean isOk = budgetCheckForVpay(saveObjectList);		
			saveLtPaSheetData(saveObjectList);	
			paSheetDataResult = getLtPaSheetData(NumberUtils.toInt(saveObject.getLtId()), appraisalId);
			if(CollectionUtils.isNotEmpty(paSheetDataResult)) {
				paSheetDataResult.get(0).setUnderBudget(isOk); 
			}
			break;
		case YumPmsConstants.ROLE_ADMIN:
			paSheetDataResult = proceedToSaveAdminPaSheet(saveObjectList);
			break;
		default:
			paSheetDataResult = Collections.emptyList();
			break;
		}
		if(!YumPmsConstants.ROLE_SELF.equalsIgnoreCase(role)) {
			setRatingEnableFlag(appraisalId, paSheetDataResult);
		}
		return paSheetDataResult;
	}

	/**
	 * Prepare pa Sheet data list to save 
	 * @param saveObjectList
	 * @return List<PASheetData>
	 * @throws YumPMSDataSaveException
	 */
	private List<PASheetData> proceedToSaveAdminPaSheet(List<PASheetData> saveObjectList) throws YumPMSDataSaveException {

		List<PASheetData> paSheetDataResult = new ArrayList<>();
		for(PASheetData saveObject : saveObjectList) {
			int empId = NumberUtils.toInt(saveObject.getEmpId());
			int apprId = NumberUtils.toInt(saveObject.getApprId());
			int criterialId = BooleanUtils.toBoolean(saveObject.getPromotionEligibility()) ? paDao.getPromotionCriteriaId(saveObject) : -1;
			saveAdminPaSheetData(saveObjectList, criterialId);	
			PASheetData paSheetData = prepareAdminPaSheetData(saveObject);
			paSheetData.setPromotionEligibility(saveObject.getPromotionEligibility());
			paSheetDataResult.add(paSheetData);

			int gradeDesc = employeeService.getCurrentGradeDesc(empId, apprId) + 1;
			Integer nextGradeId = employeeService.getGradeMstrByDescription(String.valueOf(gradeDesc)).getGradeId();

			int apprDesc = NumberUtils.toInt(appraisalService.getAppraisalPeriodbyId(apprId).getPeriodDesc()) + 1;
			Integer nextApprId = appraisalService.getAppraisalPeriodbyDescription(String.valueOf(apprDesc)).getAppraisalPeriodId();

			CompRevisedSalaryEffectiveDuration effectiveDurationDate = compDao.getEffectiveDurationDate(new AppraisalPeriod(apprId));

			if(BooleanUtils.toBoolean(saveObject.getAdminRating().getPromotion())) {
				EmployeeYearWiseNotEffective empNotEffective = employeeService.getNotEffectiveEmpData(empId);
				if(YumPmsUtils.isGreaterThanZero(empNotEffective.getEmpYearWiseId())) {
					empNotEffective.setApprPeriodId(nextApprId);
					empNotEffective.setGradeId(nextGradeId);
					empNotEffective.setEffectiveDateId(effectiveDurationDate);  
					employeeService.insertOrUpdateNotEffectiveEmpData(empNotEffective);
				} else {
					EmployeeCurrentDetails employeeYearWise = employeeService.getEmployeeDetailsByUserId(saveObject.getEmpId(), apprId, YumPmsConstants.EMPLOYEE_ID);
					employeeYearWise.setApprPeriodId(nextApprId);
					employeeYearWise.setGradeId(nextGradeId);
					employeeYearWise.setEffectiveDuration(effectiveDurationDate);  
					employeeService.insertOrUpdateNotEffectiveEmpData(new EmployeeYearWiseNotEffective(employeeYearWise));
				}
			}
		}
		return paSheetDataResult;
	}

	/**
	 * create Administration Pa SheetData
	 * @param saveObject
	 * @return PASheetData
	 * @throws YumPMSDataSaveException
	 */
	private PASheetData prepareAdminPaSheetData(PASheetData saveObject) {
		
		Integer appraisalId = NumberUtils.toInt(saveObject.getApprId());
		PASheetData paSheetData = getPaSheetDataByEmpIdApprId(NumberUtils.toInt(saveObject.getEmpId()), appraisalId, false); 
		if(paSheetData.getPaSheetId() != null) {
			paSheetData.setSupRating(paDao.getOverAllRating(new PA_SheetMaster(paSheetData.getPaSheetId(), new EmployeeNew(NumberUtils.toInt(paSheetData.getEmpId())), new AppraisalPeriod(NumberUtils.toInt(paSheetData.getApprId())))));
			paSheetData.setFunLtRating(paDao.getFunctionLTRating(new PA_SheetMaster(paSheetData.getPaSheetId(), new EmployeeNew(NumberUtils.toInt(paSheetData.getEmpId())), new AppraisalPeriod(NumberUtils.toInt(paSheetData.getApprId())))));
			paSheetData.setAdminRating(paDao.getAdminRating(new PA_SheetMaster(paSheetData.getPaSheetId(), new EmployeeNew(NumberUtils.toInt(paSheetData.getEmpId())), new AppraisalPeriod(NumberUtils.toInt(paSheetData.getApprId())))));
			paSheetData.setAdminEmpId(saveObject.getAdminEmpId());
		}
		return paSheetData;
	}

	/**
	 * budgetCheckForVpay it checked variable pay is under budget or not
	 * @param saveObjectList
	 * @return Boolean
	 */
	private Boolean budgetCheckForVpay(List<PASheetData> saveObjectList) {
		
		if(CollectionUtils.isEmpty(saveObjectList)) {
			return false;
		}
		BigDecimal totalCalculatedBudget = BigDecimal.ZERO;
		BigDecimal totalActualBudget = BigDecimal.ZERO;
		
		for(PASheetData paSheet : saveObjectList) {
			EmployeeSalaryInfo empSal = new EmployeeSalaryInfo();
			empSal.setEmpId(NumberUtils.toInt(paSheet.getEmpId())); 

			List<ClasificationIFValue> ifVpayNodeList = paSheet.getFunLtRating().getListClasificationIFValue();
			for(ClasificationIFValue ifVpayValue : ifVpayNodeList) {
				if(ifVpayValue.getIsEditeable()) {
					empSal.setIfValue(ifVpayValue.getIfValue());
					empSal.setvPay(null != ifVpayValue.getvPay()?ifVpayValue.getvPay() : 0);
					empSal.setActualBudget(ifVpayValue.getIndivusalBudget()); 
					empSal.setTunere(ifVpayValue.getTenure());
				}
			}			
			empSal.setCalculatedBudget(((YumPmsUtils.multiply(empSal.getvPay(), empSal.getIfValue()) / 100) * empSal.getTunere())/365); 
			totalCalculatedBudget = totalCalculatedBudget.add(YumPmsUtils.doubleToBigDecimal(empSal.getCalculatedBudget()));
			totalActualBudget = totalActualBudget.add(YumPmsUtils.doubleToBigDecimal(empSal.getActualBudget())) ;  			
		}
		totalCalculatedBudget = YumPmsUtils.getRoundedValue(totalCalculatedBudget); 	
		totalActualBudget = YumPmsUtils.getRoundedValue(totalActualBudget); 	
		return totalActualBudget.compareTo(totalCalculatedBudget) >= 0 ? true : false;
	}

	/** 
	 * Fetch List of PA_ConfigRatingBellCurveIFRange against appraisalId
	 * @param Integer apprId
	 * @return List<PA_ConfigRatingBellCurveIFRange>
	 */
	@Override
	public List<PA_ConfigRatingBellCurveIFRange> getIfRange(Integer apprId) {
		return paDao.getRatingBellCurveIFRange(apprId); 
	}
	
	/**
	 * getWorkStatusByPaSheetId : Used to get next work-flow object by paSheetId. We can get the status from this object.
	 * @param Integer paSheetId
	 * @return StatusWorkFlow
	 */
	@Override
	public StatusWorkFlow getWorkStatusByPaSheetId(Integer paSheetId) {
		
		PA_SheetStatusMaster paSheetStatus = paDao.getPaSheetStatusByPaSheetId(paSheetId);
		Integer paSheetStatusId = paSheetStatus != null ? paSheetStatus.getPaSheetStatusId() : 0;
		if(paSheetStatusId != null && paSheetStatusId != 0) {
			List<PA_SheetStatusWorkFlow> paStatusWorkFlows = paDao.getPaStatusWorkFlowsByStatusId(paSheetStatusId);
			StatusWorkFlow statusWorkFlow = new StatusWorkFlow();
			statusWorkFlow.setCurrentStatusId(paSheetStatusId);
			statusWorkFlow.setCurrentStatusName(paSheetStatus.getPaSheetStatusDesc());
			if(CollectionUtils.isNotEmpty(paStatusWorkFlows)) {
				for(PA_SheetStatusWorkFlow workFlow : paStatusWorkFlows) {
					statusWorkFlow.getPossibleNextStutsNameAndUrl().put(
							workFlow.getNextPossiblePaSheetStatusId(), 
							Arrays.asList(String.valueOf(workFlow.getNextPossiblePaSheetStatusId()), workFlow.getWebServiceURL()));
				}
			} else {
				statusWorkFlow.setMessage("PASheet Does Not Exist In The Database"); 
			}
			return statusWorkFlow;
		}
		return new StatusWorkFlow();
	}

	/**
	 * updatePaSheetStatus : Used to update StatusWorkFlow
	 * @param int paSheetId: PA sheet ID
	 * @param int statusId: PA sheet status ID
	 * @param String createdBy: This is logged-in user's user ID.
	 * @throws YumPMSDataSaveException 
	 * @return StatusWorkFlow : updated status object
	 */
	@Override
	public StatusWorkFlow updatePaSheetStatus(int paSheetId, int statusId, String createdBy) throws YumPMSDataSaveException {

		String msg = paDao.updatePaSheetStatus(paSheetId, statusId, createdBy);
		StatusWorkFlow statusWorkFlow = getWorkStatusByPaSheetId(paSheetId);
		Integer actionStatusId = null == statusWorkFlow ? 0 : statusWorkFlow.getCurrentStatusId();
		if(!YumPmsConstants.PA_WORK_FLOW_INITIAL_STATUS_ID.equals(actionStatusId)) {
			String param2 = null;
			switch(WorkFlowStatus.toValue(actionStatusId)) {
			case SUBMITTED:
				param2 = YumPmsConstants.WORK_FLOW_STATUS_DESC_SUBMITTED;
				break;
			case ROLLED_BACK:
				param2 = YumPmsConstants.WORK_FLOW_STATUS_DESC_ROLLEDBACK;
				break;
			case DISCUSS_AND_AGREED:
				param2 = YumPmsConstants.WORK_FLOW_STATUS_DESC_APPROVED;
				break;
			case RECALLED:
				param2 = YumPmsConstants.WORK_FLOW_STATUS_DESC_RECALLED;
				break;
			default :
				param2 = StringUtils.EMPTY;
				break;
			}
			if(YumPmsConstants.UPDATED_SUCESSFULLY.equalsIgnoreCase(msg)) {	
				sendEmail(paSheetId, actionStatusId, param2);
			}
		}
		return statusWorkFlow;
	}
	
	/**
	 * Used to check the promotion eligibility for each employee and set the flag against each object. 
	 * Set the PromotionEligibility flag to true/false
	 * @param List<PASheetData> uiObjectList
	 * @throws YumPMSDataSaveException
	 * @return List<PASheetData> 
	 */
	@Override
	public List<PASheetData> promotionEligibilityCheck(List<PASheetData> uiObjectList) throws YumPMSDataSaveException {
		List<PASheetData> sendObjectList = new ArrayList<>();
		for(PASheetData uiObject : uiObjectList) {
			String role = uiObject.getSaveByFlag();
			Integer ratingId = 0;
			Double ifValue = 0.0;
			switch(StringUtils.trimToEmpty(StringUtils.lowerCase(role))) {
			case YumPmsConstants.ROLE_SUPERVISOR:
				List<ClasificationIFValue> listOfClasificationValues = 
					YumPmsUtils.getEmptyListIfNull(uiObject.getSupRating().getListClasificationIFValue());
				ratingId = uiObject.getSupRating().getRatingId();				
				for(ClasificationIFValue ifNode : listOfClasificationValues) {
					ifValue = checkNodeEditable(ifValue, ifNode);
				}
				break;
			case YumPmsConstants.ROLE_LT:	
				List<ClasificationIFValue> listOfClasificationValues2 = 
					YumPmsUtils.getEmptyListIfNull(uiObject.getFunLtRating().getListClasificationIFValue());
				ratingId = uiObject.getFunLtRating().getRatingId();
				for(ClasificationIFValue ifNode : listOfClasificationValues2) {
					ifValue = checkNodeEditable(ifValue, ifNode);
				}
				break;
			case YumPmsConstants.ROLE_ADMIN:
				List<ClasificationIFValue> listOfClasificationValues3 = 
					YumPmsUtils.getEmptyListIfNull(uiObject.getAdminRating().getListClasificationIFValue());
				ratingId = uiObject.getAdminRating().getRatingId();
				for(ClasificationIFValue ifNode : listOfClasificationValues3) {
					ifValue = checkNodeEditable(ifValue, ifNode);
				}
				break;
			default:
				LOGGER.info(YumPmsConstants.INFO_ABOUT_FUNCTION_PARAMITER);
				ratingId = 0; 
				ifValue = 0.0; 
				break;
			}			 		 
			List<Map<String, String>> results = paDao.executeDynamicProcedure(YumPmsConstants.SP_PROMOTION_ELIGIBILITY,  
					NumberUtils.toInt(uiObject.getEmpId()),NumberUtils.toInt(uiObject.getApprId()),ratingId,ifValue);
			uiObject.setPromotionEligibility(results.get(0).get(YumPmsConstants.FLAG).equalsIgnoreCase("1")?true:false); 
			uiObject.setPromotionEligibilityMsg(results.get(0).get(YumPmsConstants.MESSAGE));

			sendObjectList.add(uiObject);
		}
		return sendObjectList;
	}

	private Double checkNodeEditable(Double ifValue, ClasificationIFValue ifNode) {
		if(ifNode.getIsEditeable())
			ifValue = ifNode.getIfValue();
		return ifValue;
	}
	
	/**
	 * Get Maximum promotion percentage value that has been configured against each appraisal period
	 * @param Integer apprId
	 * @return Double 
	 */
	@Override
	public Double getPromotionPercentage(Integer apprId) {
		
		LOGGER.info("getPromotionPercentage -- apprId = {}", apprId);
		PA_ConfigPromotionPercentIndicator percentIndicator = 
				paDao.findOneByApprId(PA_ConfigPromotionPercentIndicator.class, apprId).orElse(new PA_ConfigPromotionPercentIndicator());
		return percentIndicator.getPromotionPercentIndicator();
	}
	
	/**
	 * Save the promotion percentage against a particular appraisal year
	 * @param Integer apprId - appraisal period id
	 * @param Double percentage - max promotion percentage
	 * @param String updatedBy - Logged-in user's user id
	 */
	@Override
	public Double savePromotionPercentage(Integer apprId, Double percentage, String updatedBy) throws YumPMSDataSaveException {
	
		LOGGER.info("savePromotionPercentage -- apprId = {} and percentage = {}", apprId, percentage);
		
		PA_ConfigPromotionPercentIndicator percentIndicator = 
				paDao.findOneByApprId(PA_ConfigPromotionPercentIndicator.class, apprId).orElse(new PA_ConfigPromotionPercentIndicator());
		if(!YumPmsUtils.isGreaterThanZero(percentIndicator.getPaConfigPromoPercentId())) {
			percentIndicator.setCreatedBy(updatedBy);
			percentIndicator.setCreatedDate(DateTimeUtils.now());
		}
		percentIndicator.setUpdatedBy(updatedBy);
		percentIndicator.setUpdatedDate(DateTimeUtils.now());
		percentIndicator.setAppraisalPeriod(new AppraisalPeriod(apprId));
		percentIndicator.setPromotionPercentIndicator(percentage);
		try {
			PA_ConfigPromotionPercentIndicator percentIndicatorResult = 
					compDao.saveOrUpdate(percentIndicator).orElse(new PA_ConfigPromotionPercentIndicator());
			return percentIndicatorResult.getPromotionPercentIndicator();
		} catch (YumPMSDataSaveException e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			throw new YumPMSDataSaveException(e);
		}
	}

	
	//--------------------------------- Some mapping related functionalities goes here ---------------------------------------------	
	
	


	/**
	 * Save LT's PA sheet data
	 * @param saveObjectList
	 * @param saveObject
	 * @throws YumPMSDataSaveException
	 */
	private void saveLtPaSheetData(List<PASheetData> saveObjectList)throws YumPMSDataSaveException {
		
		saveObjectList = CollectionUtils.isEmpty(saveObjectList) ? Collections.emptyList() : saveObjectList;
		List<PA_OverallRatingLT> overallRatingLtList = new ArrayList<>(saveObjectList.size());
		
		for(PASheetData saveObjectForLt : saveObjectList) {
			PA_OverallRatingLT overallRatingLt = paDao.getOverallRatingLT(new PA_SheetMaster(saveObjectForLt.getPaSheetId())); 
			overallRatingLt = null != overallRatingLt ? overallRatingLt : new PA_OverallRatingLT();
			overallRatingLt.setPaSheet(new PA_SheetMaster(saveObjectForLt.getPaSheetId()));
			overallRatingLt.setIsEligable(saveObjectForLt.getPromotionEligibility());
			int ltRatingId = YumPmsUtils.resolve(() -> saveObjectForLt.getFunLtRating().getRatingId().intValue()).orElse(-1);
			checkLtRatingId(saveObjectForLt, overallRatingLt, ltRatingId);
			int cultureRatingId = YumPmsUtils.resolve(() -> saveObjectForLt.getSupRating().getCultureRatingId().intValue()).orElse(-1);
			checkCultureRatingId(saveObjectForLt, overallRatingLt,
					cultureRatingId);
			overallRatingLt.setAppraisalPeriod(new AppraisalPeriod(NumberUtils.toInt(saveObjectForLt.getApprId())));  
			overallRatingLt.setLtRemark(saveObjectForLt.getFunLtRating().getRemarks());
			overallRatingLt.setEmp(new EmployeeNew(NumberUtils.toInt(saveObjectForLt.getEmpId())));  
			overallRatingLt.setLtEmp(new EmployeeNew(NumberUtils.toInt(saveObjectForLt.getLtId())));
			overallRatingLt.setLtPromoNom(saveObjectForLt.getFunLtRating().getPromotion());
			overallRatingLt.setLtPromoNomAug(BooleanUtils.toBoolean(saveObjectForLt.getFunLtRating().getPromoNomAug()));
	
			List<ClasificationIFValue> listOfClasificationValues = saveObjectForLt.getFunLtRating().getListClasificationIFValue();
			for(ClasificationIFValue ifNode : listOfClasificationValues) {
				if(ifNode.getIsEditeable())
					overallRatingLt.setLtIF(ifNode.getIfValue());
			}			
			overallRatingLt.setAdditionalIncreaseNom(BooleanUtils.toBoolean(saveObjectForLt.getFunLtRating().getAdditionalIncreaseNom()));
			// Dev Checking Box
			overallRatingLt.setDevCheckIn(BooleanUtils.toBoolean(saveObjectForLt.getFunLtRating().getDevCheckIn()));
			overallRatingLt.setDevCheckInRremarks(overallRatingLt.getDevCheckIn() ? saveObjectForLt.getFunLtRating().getDevCheckinText() : null);
			setRatingLtCreatedUpdatedBy(saveObjectForLt, overallRatingLt);	
			if(null != overallRatingLt.getIsEligable()) {
				overallRatingLt.setPromotionCriteriaId(paDao.getPromotionCriteriaId(saveObjectForLt));
			}
			overallRatingLtList.add(overallRatingLt);
			statusUpdateInPaSheetMaster(saveObjectForLt);
		}
		paDao.savePaOveralRatingLt(overallRatingLtList);
	}

	private void setRatingLtCreatedUpdatedBy(PASheetData saveObjectForLt,
			PA_OverallRatingLT overallRatingLt) {
		if(null == overallRatingLt.getPaOverallRatingLTId()) {
			overallRatingLt.setCreatedBy(saveObjectForLt.getLtId()); 
			overallRatingLt.setUpdatedBy(saveObjectForLt.getLtId());
		} else {
			overallRatingLt.setUpdatedBy(saveObjectForLt.getLtId());
		}
	}

	private void checkCultureRatingId(PASheetData saveObjectForLt,
			PA_OverallRatingLT overallRatingLt, int cultureRatingId) {
		if(cultureRatingId > 0) {
			overallRatingLt.setCultureRatingMstr(new PaCultureRatingMstr(saveObjectForLt.getSupRating().getCultureRatingId())); 
		}
	}

	private void checkLtRatingId(PASheetData saveObjectForLt,
			PA_OverallRatingLT overallRatingLt, int ltRatingId) {
		if(ltRatingId > 0) {
			overallRatingLt.setPaRating(new PA_RatingMaster(saveObjectForLt.getFunLtRating().getRatingId()));
		}
	}

	/**
	 * save Admin's PA Sheet Data
	 * @param saveObjectList
	 * @param saveObject
	 * @throws YumPMSDataSaveException
	 */
	private void saveAdminPaSheetData(List<PASheetData> saveObjectList, Integer promotionCriteriaId)throws YumPMSDataSaveException {
		
		saveObjectList = CollectionUtils.isEmpty(saveObjectList) ? Collections.emptyList() : saveObjectList;
		List<PA_OverallRatingAdmin> overallRatingAdminList = new ArrayList<>(saveObjectList.size());
		
		for(PASheetData saveObjectForAdmin : saveObjectList) {
			PA_OverallRatingAdmin overallRatingAdmin = paDao.getOverallRatingAdmin(new PA_SheetMaster(saveObjectForAdmin.getPaSheetId())); 
			overallRatingAdmin = null != overallRatingAdmin ? overallRatingAdmin : new PA_OverallRatingAdmin();
			
			overallRatingAdmin.setPromotionCriteriaId(promotionCriteriaId);
			if(null != saveObjectForAdmin.getPromotionEligibility()) {
					overallRatingAdmin.setIsEligable(saveObjectForAdmin.getPromotionEligibility()); 
			}
			overallRatingAdmin.setPaSheet(new PA_SheetMaster(saveObjectForAdmin.getPaSheetId()));
			overallRatingAdmin.setAppraisalPeriod(new AppraisalPeriod(NumberUtils.toInt(saveObjectForAdmin.getApprId())));  
			overallRatingAdmin.setEmp(new EmployeeNew(NumberUtils.toInt(saveObjectForAdmin.getEmpId())));  
			overallRatingAdmin.setAdminEmp(new EmployeeNew(NumberUtils.toInt(saveObjectForAdmin.getAdminEmpId())));
			FunctionLTRating adminRating = saveObjectForAdmin.getAdminRating();
			if(adminRating != null) {
				if(YumPmsUtils.isGreaterThanZero(adminRating.getRatingId())) {
					overallRatingAdmin.setPaRating(new PA_RatingMaster(adminRating.getRatingId()));
				}
				if(YumPmsUtils.isGreaterThanZero(adminRating.getCultureRatingId())) {
					overallRatingAdmin.setCultureRatingMstr(new PaCultureRatingMstr(adminRating.getCultureRatingId())); 
				}
				overallRatingAdmin.setAdminRemark(adminRating.getRemarks());
				overallRatingAdmin.setAdminPromoNom(adminRating.getPromotion());
				overallRatingAdmin.setAdminPromoNomAug(BooleanUtils.toBoolean(adminRating.getPromoNomAug()));
				overallRatingAdmin.setAdditionalIncreaseNom(BooleanUtils.toBoolean(adminRating.getAdditionalIncreaseNom()));
				List<ClasificationIFValue> listOfClasificationValues = adminRating.getListClasificationIFValue();
				for(ClasificationIFValue ifNode : listOfClasificationValues) {
					if(ifNode.getIsEditeable())
						overallRatingAdmin.setAdminIfValue(ifNode.getIfValue());
				}		
				// Dev Checking Box
				overallRatingAdmin.setDevCheckIn(BooleanUtils.toBoolean(adminRating.getDevCheckIn()));
				overallRatingAdmin.setDevCheckInRremarks(overallRatingAdmin.getDevCheckIn() ? adminRating.getDevCheckinText() : null);
			}
			if(null == overallRatingAdmin.getPaOverallRatingAdminId()) {
				overallRatingAdmin.setCreatedBy(saveObjectForAdmin.getAdminEmpId()); 
				overallRatingAdmin.setUpdatedBy(saveObjectForAdmin.getAdminEmpId());
			} else {
				overallRatingAdmin.setUpdatedBy(saveObjectForAdmin.getAdminEmpId());
			}
			overallRatingAdminList.add(overallRatingAdmin);
			statusUpdateInPaSheetMaster(saveObjectForAdmin);
		}
		paDao.savePaOveralRatingAdmin(overallRatingAdminList);
	}

	/**
	 * save Supervisor's PA Sheet Data
	 * @param saveObject
	 * @throws YumPMSDataSaveException
	 */
	private void saveSupervisorPaSheetData(PASheetData saveObject) throws YumPMSDataSaveException {
		if(!saveObject.getGoalRating() && !BooleanUtils.toBoolean(saveObject.getAligningSup2())) {
				throw new CommonException(YumPmsConstants.ERROR_ALIGNING_SUP_2);
		}
		List<PA_GoalRating> paGoalRatingList = populatePaGoalRatingMaster(saveObject, true);
		paGoalRatingList = CollectionUtils.isEmpty(paGoalRatingList) ? Collections.emptyList() : paGoalRatingList;
		List<PA_GoalRating> savePaGoalRatingList = new ArrayList<>();
		for(PA_GoalRating paGoalRating : paGoalRatingList) {
			if(paGoalRating == null) {
				continue;
			}
			PA_RatingMaster  rating = new PA_RatingMaster(paGoalRating.getPaRatingSup().getPaRatingId());
			String supRemarks = paGoalRating.getSupRemark();
			paGoalRating.setPaRatingSup(rating);
			paGoalRating.setSupRemark(supRemarks);	
			paGoalRating.setUpdatedBy(saveObject.getSupervisorId());
			savePaGoalRatingList.add(paGoalRating);
		}
		paDao.savePAGoalRating(savePaGoalRatingList);
		PA_OverallRatingSup overalRatingSup = paDao.getOverallRatingSup(new PA_SheetMaster(saveObject.getPaSheetId())); 
		overalRatingSup = null != overalRatingSup? overalRatingSup : new PA_OverallRatingSup();
		
		overalRatingSup.setPaSheet(new PA_SheetMaster(saveObject.getPaSheetId()));
		int supRatingId  = YumPmsUtils.resolve(() -> saveObject.getSupRating().getRatingId().intValue()).orElse(-1);
		if(supRatingId > 0) {
			overalRatingSup.setPaRating(new PA_RatingMaster(saveObject.getSupRating().getRatingId()));
		}
		int cultureRatingId = YumPmsUtils.resolve(() -> saveObject.getSupRating().getCultureRatingId().intValue()).orElse(-1);
		if(cultureRatingId > 0) {
			overalRatingSup.setCultureRatingMstr(new PaCultureRatingMstr(saveObject.getSupRating().getCultureRatingId())); 
		}
		overalRatingSup.setSupRemark(saveObject.getSupRating().getRemarks());
		overalRatingSup.setSupEmp(new EmployeeNew(NumberUtils.toInt(saveObject.getSupervisorId())));  
		overalRatingSup.setSupPromoNom(saveObject.getSupRating().getPromotion());
		overalRatingSup.setSupPromoNomAug(BooleanUtils.toBoolean(saveObject.getSupRating().getPromoNomAug()));
		List<ClasificationIFValue> listOfClasificationValues = saveObject.getSupRating().getListClasificationIFValue();
		for(ClasificationIFValue ifNode : listOfClasificationValues) {
			if(ifNode.getIsEditeable())
				overalRatingSup.setSupIf(ifNode.getIfValue());
		}	
		// Dev Checking Box
		overalRatingSup.setDevCheckIn(BooleanUtils.toBoolean(saveObject.getSupRating().getDevCheckIn()));
		overalRatingSup.setDevCheckInRremarks(overalRatingSup.getDevCheckIn() ? saveObject.getSupRating().getDevCheckinText() : null);
		
		setSupPaSheetCreatedUpdatedByValue(saveObject, overalRatingSup);
		paDao.savePAOveralRatingSup(overalRatingSup);
		statusUpdateInPaSheetMaster(saveObject);
		// Trigger an email to supervisor's supervisor
		paDao.sendEmailToSup2(saveObject.getPaSheetId());
	}

	private void setSupPaSheetCreatedUpdatedByValue(PASheetData saveObject,
			PA_OverallRatingSup overalRatingSup) {
		if(null == overalRatingSup.getPaOverallRatingSupId()) {
			overalRatingSup.setCreatedBy(saveObject.getSupervisorId()); 
			overalRatingSup.setUpdatedBy(saveObject.getSupervisorId());
		} else {
			overalRatingSup.setUpdatedBy(saveObject.getSupervisorId());
		}
	}
	
	/**
	 * save Own PA Sheet Data
	 * @param saveObject
	 * @throws YumPMSDataSaveException
	 */
	private void saveSelfPaSheetData(PASheetData saveObject) throws YumPMSDataSaveException {
		List<PA_GoalRating> paGoalRatingList = populatePaGoalRatingMaster(saveObject, false);
		if(CollectionUtils.isNotEmpty(paGoalRatingList)) {
			paDao.savePAGoalRating(paGoalRatingList);
		}
	}
	
	private void statusUpdateInPaSheetMaster(PASheetData saveObject) throws YumPMSDataSaveException {
		
		PA_SheetMaster updatedSheetMaster = paDao.getPASheetData(
				NumberUtils.toInt(saveObject.getEmpId()), NumberUtils.toInt(saveObject.getApprId())); 
		if(saveObject.getRatingStatusId() != null) {
			updatedSheetMaster.setPaOverallRatingStatus(new PA_OverallRatingStatusMaster(saveObject.getRatingStatusId()));
		}
		updatedSheetMaster.setUpdatedBy(saveObject.getLtId());
		paDao.savePaSheetMaster(updatedSheetMaster);
	}
	
	/**
	 * get PA Sheet Data object for an employee against a particular appraisal year
	 * @param empId
	 * @param appraisalId
	 * @param isSelfOrSup
	 * @return PASheetData
	 */
	private PASheetData getPaSheetDataByEmpIdApprId(Integer empId, Integer appraisalId, boolean isSelfOrSup) {
		
		PA_SheetMaster paSheetMaster = paDao.getPASheetData(empId, appraisalId);
		if(null == paSheetMaster || YumPmsUtils.isNullOrZero(paSheetMaster.getPaSheetId())) {
			paSheetMaster = new PA_SheetMaster();
			paSheetMaster.setAppraisalPeriod(new AppraisalPeriod(appraisalId));
			paSheetMaster.setEmployee(new EmployeeNew(empId));
			paSheetMaster.setPaSheetStatus(new PA_SheetStatusMaster(YumPmsConstants.PA_WORK_FLOW_INITIAL_STATUS_ID));
			paSheetMaster.setPaOverallRatingStatus(new PA_OverallRatingStatusMaster(YumPmsConstants.PA_OVER_ALL_RATING_INITIAL_STATUS_ID));
			paSheetMaster.setCreatedBy(empId.toString());
			paSheetMaster.setUpdatedBy(empId.toString());
			try {
				paSheetMaster = paDao.savePaSheetMaster(paSheetMaster);
			} catch (YumPMSDataSaveException e) {
				LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
				throw new CommonException(e);
			}
		}
		
		PASheetData paSheet = createPaSheet(empId, appraisalId, paSheetMaster);
		if(isSelfOrSup) {
			List<Goal> goalList = paDao.getMyGoalIdList(empId, appraisalId);	
			if(CollectionUtils.isNotEmpty(goalList)) {
				List<SelfSheetData> selfSheetList = getSelfSheetList(paSheetMaster.getPaSheetId(), goalList);
				if(CollectionUtils.isNotEmpty(selfSheetList)) {
					paSheet.getSelfSheetList().addAll(selfSheetList);
				}
			}
		}
		return paSheet;
	}

	/**
	 * Create paSheetData object and fill the all the fields are return
	 * @param empId
	 * @param appraisalId
	 * @param paSheetMaster
	 * @return PASheetData
	 */
	private PASheetData createPaSheet(Integer empId, Integer appraisalId, PA_SheetMaster paSheetMaster) {
		
		PASheetData paSheet = new PASheetData();
		EmployeeCurrentDetails empDetail = employeeService.getEmployeeDetailsByUserId(String.valueOf(empId), appraisalId, YumPmsConstants.EMPLOYEE_ID);
		paSheet.setPaSheetId(paSheetMaster.getPaSheetId()); 
		paSheet.setEmpId(String.valueOf(empId));
		paSheet.setEmpName(empDetail.getEmpName());
		paSheet.setGradeId(empDetail.getGradeId());
		paSheet.setGrade(empDetail.getGrade());
		if(paSheetMaster.getAppraisalPeriod() != null) {
			paSheet.setApprId(String.valueOf(paSheetMaster.getAppraisalPeriod().getAppraisalPeriodId()));
			paSheet.setApprDesc(paSheetMaster.getAppraisalPeriod().getPeriodDesc());
		} else {
			paSheet.setApprId(String.valueOf(appraisalId));
		}
		paSheet.setSupervisorId(String.valueOf(empDetail.getManagerId()));
		paSheet.setSupervisorName(empDetail.getManagerName()); 
		paSheet.setEmpDesignation(empDetail.getDesignation());
		paSheet.setLtId(YumPmsUtils.intToStr(empDetail.getFunctionalLtsId()));
		paSheet.setLtName(empDetail.getFunctionalLtsName());
		paSheet.setFunction(empDetail.getFunctionName());
		boolean isGoalSheetSubmitted = false;
		GoalSheet goalSheet = paDao.getGoalSheetByEmpIdApprId(empId, appraisalId);
		if(goalSheet != null && goalSheet.getGoalStatus() != null) {
			isGoalSheetSubmitted = YumPmsConstants.PA_SHEET_DISCUSS_AND_AGREED_STATUS_ID.equals(goalSheet.getGoalStatus().getGoalStatusId());
		}
		paSheet.setIsGoalSheetSubmitted(isGoalSheetSubmitted);
		paSheet.setWorkflowId(paSheetMaster.getPaSheetStatus() == null ? null : paSheetMaster.getPaSheetStatus().getPaSheetStatusId());	
		paSheet.setRatingStatusId(
				paSheetMaster.getPaOverallRatingStatus() == null ? 0 : paSheetMaster.getPaOverallRatingStatus().getPaOverallRatingStatusId());
		paSheet.setPaSheetStatusDesc(
				paSheetMaster.getPaSheetStatus() == null ? StringUtils.EMPTY : paSheetMaster.getPaSheetStatus().getPaSheetStatusDesc());
		paSheet.setDownloadPermission(paSheetMaster.getLetterDownloadPermission());
		paSheet.setIsAccepted(paSheetMaster.getLetterAcceptance()); 
		return paSheet;
	}

	/**
	 * Get SelfSheet List from employee's goals
	 * @param paSheetMaster
	 * @param goalList
	 * @return List<SelfSheetData>
	 */
	private List<SelfSheetData> getSelfSheetList(Integer paSheetId, List<Goal> goalList) {
		
		if(YumPmsUtils.isNullOrZero(paSheetId)) {
			return Collections.emptyList();
		}
		List<Integer> goalIds = goalList.stream().map(Goal :: getGoalId).collect(Collectors.toList());
		Map<Integer, List<MyGoal>> myGoalMap = paDao.getMyGoalMapByGoalIds(goalIds);
		Map<Integer, Integer> orgGoalMap = goalList.stream().collect(Collectors.toMap(Goal :: getGoalId, Goal :: getOrgGoalId));
		List<Rating> ratingDetailsList = paDao.getRatingDetailsList();
		List<SelfSheetData> selfSheetDataList = new ArrayList<>();
		for(Map.Entry<Integer, List<MyGoal>> entry : myGoalMap.entrySet()) {
			
			Integer goalId = entry.getKey();
			List<MyGoal> myGoalSectionList = entry.getValue();
			if(CollectionUtils.isEmpty(myGoalSectionList)) {
				continue;
			}
			SelfSheetData selfSheet = new SelfSheetData();
			selfSheet.setGoalId(goalId);
			GoalOrg goalOrg = paDao.getOrgGoalByOrgGoalId(orgGoalMap.get(goalId));
			if(goalOrg != null) {
				selfSheet.setOrgGoalDesc(goalOrg.getOrgGoalDesc()); 
				selfSheet.setOrgGoalId(goalOrg.getOrgGoalId());		
			}
			selfSheet.setRatingsList(ratingDetailsList); 
			selfSheet.setMyGoalSectionList(myGoalSectionList);
			setEmpOrSupRating(paSheetId, goalId, myGoalSectionList);
			selfSheetDataList.add(selfSheet);
		}
		return selfSheetDataList;
	}

	/**
	 * Set employee's or supervisor's rating in myGoalSectionList
	 * @param paSheetId
	 * @param goalId
	 * @param myGoalSectionList
	 */
	private void setEmpOrSupRating(Integer paSheetId, Integer goalId, List<MyGoal> myGoalSectionList) {
		
		List<PA_GoalRating> paGoalRatings = paDao.getPaGoalRating(paSheetId, goalId);
		if(CollectionUtils.isNotEmpty(paGoalRatings)) {
			Map<Integer, PA_GoalRating> map = paGoalRatings.stream().collect(
					Collectors.toMap(paGoalRating -> paGoalRating.getGoalSection().getGoalSectionId(), Function.identity()));							
			for (MyGoal myGoalSection : myGoalSectionList) {
				PA_GoalRating paGoalRating = map.get(myGoalSection.getGoalSecId());
				if(paGoalRating != null) {
					if(paGoalRating.getPaRatingEmp() != null) {
						myGoalSection.setSelectedRatingEmp(String.valueOf(paGoalRating.getPaRatingEmp().getPaRatingId()));
					}
					if(paGoalRating.getPaRatingSup() != null) {
						myGoalSection.setSelectedRatingSup(String.valueOf(paGoalRating.getPaRatingSup().getPaRatingId()));
					}
					myGoalSection.setRatingRemarks(paGoalRating.getSupRemark());
					myGoalSection.setSelfRemarks(paGoalRating.getSelfRemark());
				}
			}
		}
	}
	
	/**
	 * get PaGoal RatingMaster list
	 * @param saveObject
	 * @throws YumPMSDataSaveException
	 * @return List<PA_GoalRating>
	 */
	private List<PA_GoalRating> populatePaGoalRatingMaster(PASheetData saveObject, boolean isSupervisor) throws YumPMSDataSaveException {

		Integer paSheetId = saveObject.getPaSheetId();
		if(null == paSheetId) {
			PA_SheetMaster paSheetMaster = new PA_SheetMaster();
			paSheetMaster.setAppraisalPeriod(new AppraisalPeriod(NumberUtils.toInt(saveObject.getApprId())));
			paSheetMaster.setEmployee(new EmployeeNew(NumberUtils.toInt(saveObject.getEmpId())));
			paSheetMaster.setPaSheetStatus(new PA_SheetStatusMaster(YumPmsConstants.PA_WORK_FLOW_INITIAL_STATUS_ID));
			paSheetMaster.setPaOverallRatingStatus(new PA_OverallRatingStatusMaster(YumPmsConstants.PA_OVER_ALL_RATING_INITIAL_STATUS_ID));
			paSheetMaster.setCreatedBy(saveObject.getEmpId());
			paSheetMaster.setUpdatedBy(saveObject.getEmpId());
			paSheetMaster.setLetterDownloadPermission(saveObject.getDownloadPermission());
			paSheetMaster.setLetterAcceptance(saveObject.getIsAccepted());

			paSheetMaster = paDao.savePaSheetMaster(paSheetMaster);
			paSheetId = paSheetMaster.getPaSheetId();
			saveObject.setPaSheetId(paSheetId);
		}
		List<PA_GoalRating> paGoalRatingList = getPaGoalRatingList(saveObject, isSupervisor);
		return CollectionUtils.isEmpty(paGoalRatingList) ? Collections.emptyList() : paGoalRatingList;
	}

	/**
	 * Get PaGoalRating List
	 * @param saveObject
	 * @return List<PA_GoalRating>
	 */
	private List<PA_GoalRating> getPaGoalRatingList(PASheetData saveObject, boolean isSupervisor) {
		
		List<SelfSheetData> selfSheetDataList = saveObject.getSelfSheetList();
		if(CollectionUtils.isEmpty(selfSheetDataList)) {
			return Collections.emptyList();
		}
		for(SelfSheetData selfSheet : selfSheetDataList) {
			List<MyGoal> myGoals = null;
			if(isSupervisor) {
				myGoals = selfSheet.getMyGoalSectionList().stream().filter(myGoal -> StringUtils.isNotBlank(myGoal.getSelectedRatingSup()))
						.collect(Collectors.toList());
			} else {
				myGoals = selfSheet.getMyGoalSectionList().stream().filter(myGoal -> StringUtils.isNotBlank(myGoal.getSelectedRatingEmp()))
						.collect(Collectors.toList());
			}
			selfSheet.setMyGoalSectionList(myGoals);
		}
		return prepareGoalRatingList(saveObject, isSupervisor);
	}

	/**
	 * Prepare GoalRatingList from selfSheetDataList
	 * @param saveObject
	 * @param isSupervisor
	 * @return List<PA_GoalRating>
	 */
	private List<PA_GoalRating> prepareGoalRatingList(PASheetData saveObject, boolean isSupervisor) {
		
		List<SelfSheetData> selfSheetDataList = saveObject.getSelfSheetList();
		List<PA_GoalRating> paGoalRatingList = new ArrayList<>(selfSheetDataList.size());
		for(SelfSheetData selfSheet : selfSheetDataList) {
			
			List<PA_GoalRating> goalRatingList = paDao.getPaGoalRating(saveObject.getPaSheetId(), selfSheet.getGoalId());
			Map<Integer, PA_GoalRating> map = goalRatingList.stream().collect(
					Collectors.toMap(goalRating -> goalRating.getGoalSection().getGoalSectionId(), Function.identity()));
			
			for(MyGoal myGoal : selfSheet.getMyGoalSectionList()) {
				PA_GoalRating individualRating = null;
				if(map.containsKey(myGoal.getGoalSecId())) {
					individualRating = map.get(myGoal.getGoalSecId());
					individualRating.setUpdatedBy(saveObject.getEmpId());
				} else {
					individualRating = new PA_GoalRating();
					individualRating.setCreatedBy(saveObject.getEmpId());
					individualRating.setUpdatedBy(saveObject.getEmpId());
				}
				if(myGoal.getGoalSecId() != null) {
					individualRating.setGoalSection(new GoalSection(myGoal.getGoalSecId()));  
				}
				individualRating.setPaSheet(new PA_SheetMaster(saveObject.getPaSheetId()));
				individualRating.setGoal(new Goal(selfSheet.getGoalId()));
				individualRating.setGoalOrg(new GoalOrg(selfSheet.getOrgGoalId()));
				checkIfSup(isSupervisor, myGoal, individualRating);
				int supEmpId = NumberUtils.toInt(saveObject.getSupervisorId());
				if(supEmpId != 0) {
					individualRating.setSupEmp(new EmployeeNew(supEmpId));  
				}
				individualRating.setSupRemark(myGoal.getRatingRemarks()); 
				individualRating.setSelfRemark(myGoal.getSelfRemarks()); //Newly added as per client's CR
				individualRating.setEmp(new EmployeeNew(NumberUtils.toInt(saveObject.getEmpId())));
				individualRating.setAppraisalPeriod(new AppraisalPeriod(NumberUtils.toInt(saveObject.getApprId())));
				
				paGoalRatingList.add(individualRating);
			}
		}
		return paGoalRatingList;
	}

	private void checkIfSup(boolean isSupervisor, MyGoal myGoal,
			PA_GoalRating individualRating) {
		if(isSupervisor) {
			individualRating.setPaRatingSup(new PA_RatingMaster(YumPmsUtils.toInteger(myGoal.getSelectedRatingSup()).getValue())); 
		} else {
			individualRating.setPaRatingEmp(new PA_RatingMaster(YumPmsUtils.toInteger(myGoal.getSelectedRatingEmp()).getValue()));
		}
	}
	
	/**
	 * Get Supervisor PaSheetData List
	 * @param empId
	 * @param appraisalId
	 * @return List<PASheetData>
	 */
	private List<PASheetData> getSupervisorPaSheetData(Integer empId, Integer appraisalId) {
		
		PASheetData pasheetData = getPaSheetDataByEmpIdApprId(empId, appraisalId, true);
		if(pasheetData.getPaSheetId() != null) {
			pasheetData.setSupRating(paDao.getOverAllRating(new PA_SheetMaster(pasheetData.getPaSheetId(), new EmployeeNew(empId), new AppraisalPeriod(appraisalId)))); 
		}
		return Arrays.asList(pasheetData);
	}
	
	/**
	 * Get LT's PaSheetData List
	 * @param empId
	 * @param appraisalId
	 * @return List<PASheetData>
	 */
	private List<PASheetData> getLtPaSheetData(Integer empId, Integer appraisalId) {
		
		List<Integer> empIdList = paDao.getAllReportees(empId, appraisalId);
		empIdList = CollectionUtils.isEmpty(empIdList) ? Collections.emptyList() : empIdList;
		List<PASheetData> paSheetDataList = new ArrayList<>(empIdList.size());
		for(Integer empid : empIdList) {
			PASheetData pasheetData = getPaSheetDataByEmpIdApprId(empid, appraisalId, false);
			if(pasheetData.getPaSheetId() != null) {
				pasheetData.setSupRating(paDao.getOverAllRating(new PA_SheetMaster(pasheetData.getPaSheetId(), new EmployeeNew(empid), new AppraisalPeriod(appraisalId))));
				pasheetData.setFunLtRating(paDao.getFunctionLTRating(new PA_SheetMaster(pasheetData.getPaSheetId(), new EmployeeNew(empid), new AppraisalPeriod(appraisalId))));
			}
			paSheetDataList.add(pasheetData);
		}
		return paSheetDataList;
	}
	
	/**
	 * Get AdminPaSheetData List
	 * @param appraisalId
	 * @param adminEmpId
	 * @return List<PASheetData>
	 */
	private List<PASheetData> getAdminPaSheetData(String adminEmpId, Integer appraisalId) {
		
		List<Integer> empIdList = paDao.getAllReportees(0, appraisalId);
		empIdList = CollectionUtils.isEmpty(empIdList) ? Collections.emptyList() : empIdList;
		List<PASheetData> paSheetDataList = new ArrayList<>(empIdList.size());
		for(Integer empid : empIdList) {
			PASheetData paSheetData = getPaSheetDataByEmpIdApprId(empid, appraisalId, false);
			if(paSheetData.getPaSheetId() != null) {
				FunctionLTRating ltRating = paDao.getFunctionLTRating(new PA_SheetMaster(paSheetData.getPaSheetId(), new EmployeeNew(empid), new AppraisalPeriod(appraisalId)));
				paSheetData.setSupRating(paDao.getOverAllRating(new PA_SheetMaster(paSheetData.getPaSheetId(), new EmployeeNew(empid), new AppraisalPeriod(appraisalId))));
				paSheetData.setFunLtRating(ltRating);
				paSheetData.setAdminRating(paDao.getAdminRating(new PA_SheetMaster(paSheetData.getPaSheetId(), new EmployeeNew(empid), new AppraisalPeriod(appraisalId))));
				paSheetData.setPromotionEligibility(ltRating.getIsEligable()); 
				paSheetData.setAdminEmpId(adminEmpId);
			}
			paSheetDataList.add(paSheetData);
		}
		return paSheetDataList;
	}
	
	/**
	 * Used to send email 
	 * @param paSheetId
	 * @param actionStatusId
	 * @param param2
	 */
	private void sendEmail(int paSheetId, Integer actionStatusId, String param2) {
		List<Object[]> results = paDao.executePaEmailNotificationProcedure(paSheetId, param2);
		for (Object[] tuples : results) {
			String attachmentPath = StringUtils.EMPTY;
			if(actionStatusId != null && actionStatusId.equals(1005)) {
				attachmentPath = createPaSheetPdfPath(paSheetId);
			}
			try {
				mailSender.sendMail(YumPmsUtils.toStringorNull(tuples[0]), 
						YumPmsUtils.toStringorNull(tuples[1]), 
						YumPmsUtils.toStringorNull(tuples[2]), 
						YumPmsUtils.toStringorNull(tuples[3]), 
						attachmentPath);
			} catch (Exception e) {
				LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			}
			if(StringUtils.isNotBlank(attachmentPath)) {
				ResourceUtils.delete(attachmentPath);
			}
		}
	}
	
	/**
	 * Create paSheet PDF file, store in a folder and return the path 
	 * @param paSheetId
	 * @return String
	 */
	private String createPaSheetPdfPath(int paSheetId) {

		PA_SheetMaster paSheet = paDao.fetchById(PA_SheetMaster.class, paSheetId);
		if(paSheet == null) {
			return StringUtils.EMPTY;
		}
		Integer apprId = paSheet.getAppraisalPeriod().getAppraisalPeriodId();
		List<PASheetData> paSheetDataList = getPASheetData(paSheet.getEmployee().getEmpId(), apprId, YumPmsConstants.ROLE_SELF);
		if(CollectionUtils.isEmpty(paSheetDataList)) {
			return StringUtils.EMPTY;
		}
		String fileName = YumPmsUtils.getPdfFileName(paSheet.getEmployee().getEmpName(), String.valueOf(apprId), YumPmsConstants.PA_SHEET_PDF_FILE);
		return ResourceUtils.createTempFile(fileName, YumPmsUtils.getPaSheetReportData(paSheetDataList.get(0)));
	}
	
	/**
	 * Extract all empIds from the provided parameter
	 * @param List<PASheetData> paSheetDataResult
	 * @return List<Integer>
	 */
	private List<Integer> extractEmpIds(List<PASheetData> paSheetDataResult) {
		
		List<Integer> empIdList = paSheetDataResult.stream()
				.map(PASheetData :: getEmpId)
				.map(NumberUtils :: toInt)
				.collect(Collectors.toList());
		return YumPmsUtils.getEmptyListIfNull(empIdList);
	}
	
	
	/**
	 * Used to set rating enable flag based on "appraisal_period.pa_eligible_date" column
	 * @param appraisalId
	 * @param paSheetDataResult
	 */
	private void setRatingEnableFlag(Integer appraisalId, List<PASheetData> paSheetDataResult) {
		Map<Integer, Boolean> paEligibilityDate = paDao.getPaEligibilityDate(appraisalId, extractEmpIds(paSheetDataResult));
		for(PASheetData paSheet : paSheetDataResult) {
			paSheet.setRatingEnabled(BooleanUtils.toBoolean(paEligibilityDate.get(NumberUtils.toInt(paSheet.getEmpId()))));
			if(!paSheet.getRatingEnabled()) {
				List<ClasificationIFValue> ifNodeList = 
						YumPmsUtils.resolve(() -> paSheet.getSupRating().getListClasificationIFValue()).orElse(new ArrayList<>());
				for(ClasificationIFValue ifNode : ifNodeList) {
					if(ifNode.getIsEditeable() && ifNode.getIfValue() == null) {
						paSheet.getSupRating().getListClasificationIFValue().get(0).setIfValue(YumPmsConstants.DEFAULT_IF_VALUE);
					}
				}
			}
		}
	}

	
}
