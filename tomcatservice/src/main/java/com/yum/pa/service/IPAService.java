package com.yum.pa.service;

import java.util.List;

import com.yum.pa.domain.PASheetData;
import com.yum.pms.dao.PA_ConfigRatingBellCurveIFRange;
import com.yum.pms.domain.StatusWorkFlow;
import com.yum.pms.exception.YumPMSDataSaveException;

public interface IPAService {
	
	List<PASheetData> getPASheetData(Integer empId, Integer apprId, String role);
	
	List<PASheetData> savePASheetData(List<PASheetData> saveObjectList) throws YumPMSDataSaveException;
	
	List<PA_ConfigRatingBellCurveIFRange> getIfRange(Integer apprId);

	StatusWorkFlow getWorkStatusByPaSheetId(Integer paSheetId);

	StatusWorkFlow updatePaSheetStatus(int paSheetId, int statusId, String createdBy) throws YumPMSDataSaveException;
	
	List<PASheetData> promotionEligibilityCheck(List<PASheetData> uiObject) throws YumPMSDataSaveException;

	Double getPromotionPercentage(Integer apprId);

	Double savePromotionPercentage(Integer apprId, Double percentage, String updatedBy) throws YumPMSDataSaveException; 
}
