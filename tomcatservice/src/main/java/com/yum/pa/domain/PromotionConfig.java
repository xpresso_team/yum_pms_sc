package com.yum.pa.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author dipak.swain
 * Used to deal with client/browser. Means this object convert to/from JSON and send/receive to/from
 */
public class PromotionConfig implements Serializable {
	
	private static final long serialVersionUID = -1489490511903586293L;

	private Integer apprId;
	
	private String statusMsg;
	
	private String logonUser;
	
	private Map<Integer, String> gradeList;
	
	private Map<Integer, String> ratingList;
	
	private Map<String, String> ifLogicList;
	
	private Map<String, String> ltsIdpOptions;
	
	private List<PromotionCriteria> promotionCriterias;
	
	public PromotionConfig() {
	}

	public PromotionConfig(Integer apprId) {
		this.apprId = apprId;
	}
	
	public PromotionConfig(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	public Integer getApprId() {
		return apprId;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	public String getLogonUser() {
		return logonUser;
	}

	public void setLogonUser(String logonUser) {
		this.logonUser = logonUser;
	}

	public void setApprId(Integer apprId) {
		this.apprId = apprId;
	}

	/**
	 * @return the gradeList
	 */
	public Map<Integer, String> getGradeList() {
		return gradeList;
	}

	/**
	 * @param gradeList the gradeList to set
	 */
	public void setGradeList(Map<Integer, String> gradeList) {
		this.gradeList = gradeList;
	}

	public Map<Integer, String> getRatingList() {
		return ratingList;
	}

	public void setRatingList(Map<Integer, String> ratingList) {
		this.ratingList = ratingList;
	}

	public Map<String, String> getIfLogicList() {
		return ifLogicList;
	}

	public void setIfLogicList(Map<String, String> ifLogicList) {
		this.ifLogicList = ifLogicList;
	}

	public Map<String, String> getLtsIdpOptions() {
		return ltsIdpOptions;
	}

	public void setLtsIdpOptions(Map<String, String> ltsIdpOptions) {
		this.ltsIdpOptions = ltsIdpOptions;
	}

	public List<PromotionCriteria> getPromotionCriterias() {
		return promotionCriterias;
	}

	public void setPromotionCriterias(List<PromotionCriteria> promotionCriterias) {
		this.promotionCriterias = promotionCriterias;
	}
	
}
