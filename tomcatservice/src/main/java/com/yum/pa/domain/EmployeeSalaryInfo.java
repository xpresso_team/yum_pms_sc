/**
 * 
 */
package com.yum.pa.domain;

import java.io.Serializable;

/**
 * @author jayanta.biswas
 *
 */
public class EmployeeSalaryInfo implements Serializable {

	private static final long serialVersionUID = 1369476507355786485L;

	private Integer empId;
	
	private Double vPay;
	
	private Double ifValue;
	
	private Double calculatedBudget;
	
	private Double actualBudget;
	
	private Integer tunere;
	
	public EmployeeSalaryInfo() {
		this.tunere = 0;
		this.ifValue = 0.0;
		this.vPay = 0.0;
		this.calculatedBudget = 0.0;
	}

	/**
	 * @return the empId
	 */
	public Integer getEmpId() {
		return empId;
	}

	/**
	 * @param empId the empId to set
	 */
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	/**
	 * @return the vPay
	 */
	public Double getvPay() {
		return vPay;
	}

	/**
	 * @param vPay the vPay to set
	 */
	public void setvPay(Double vPay) {
		this.vPay = vPay;
	}

	/**
	 * @return the ifValue
	 */
	public Double getIfValue() {
		return ifValue;
	}

	/**
	 * @param ifValue the ifValue to set
	 */
	public void setIfValue(Double ifValue) {
		this.ifValue = ifValue;
	}

	/**
	 * @return the calculatedBudget
	 */
	public Double getCalculatedBudget() {
		return calculatedBudget;
	}

	/**
	 * @param calculatedBudget the calculatedBudget to set
	 */
	public void setCalculatedBudget(Double calculatedBudget) {
		this.calculatedBudget = calculatedBudget;
	}

	/**
	 * @return the actualBudget
	 */
	public Double getActualBudget() {
		return actualBudget;
	}

	/**
	 * @param actualBudget the actualBudget to set
	 */
	public void setActualBudget(Double actualBudget) {
		this.actualBudget = actualBudget;
	}

	/**
	 * @return the tunere
	 */
	public Integer getTunere() {
		return tunere;
	}

	/**
	 * @param tunere the tunere to set
	 */
	public void setTunere(Integer tunere) {
		this.tunere = tunere;
	}
	
	
}
