package com.yum.pa.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SelfSheetData implements Serializable {
	
	private static final long serialVersionUID = 7453365197744275854L;

	private int paGoalRatingId;
	
	private Integer orgGoalId;
	
	private String orgGoalDesc;
	
	private Integer goalId;
	
	private List<MyGoal> myGoalSectionList = new ArrayList<>();
	
	private List<Rating> ratingsList = new ArrayList<>();
	
	

	/**
	 * @return the paGoalRatingId
	 */
	public int getPaGoalRatingId() {
		return paGoalRatingId;
	}

	/**
	 * @param paGoalRatingId the paGoalRatingId to set
	 */
	public void setPaGoalRatingId(int paGoalRatingId) {
		this.paGoalRatingId = paGoalRatingId;
	}

	/**
	 * @return the orgGoalId
	 */
	public Integer getOrgGoalId() {
		return orgGoalId;
	}

	/**
	 * @param orgGoalId the orgGoalId to set
	 */
	public void setOrgGoalId(Integer orgGoalId) {
		this.orgGoalId = orgGoalId;
	}

	/**
	 * @return the orgGoalDesc
	 */
	public String getOrgGoalDesc() {
		return orgGoalDesc;
	}

	/**
	 * @param orgGoalDesc the orgGoalDesc to set
	 */
	public void setOrgGoalDesc(String orgGoalDesc) {
		this.orgGoalDesc = orgGoalDesc;
	}

	/**
	 * @return the goalId
	 */
	public Integer getGoalId() {
		return goalId;
	}

	/**
	 * @param goalId the goalId to set
	 */
	public void setGoalId(Integer goalId) {
		this.goalId = goalId;
	}

	/**
	 * @return the myGoalSectionList
	 */
	public List<MyGoal> getMyGoalSectionList() {
		return myGoalSectionList;
	}

	/**
	 * @param myGoalSectionList the myGoalSectionList to set
	 */
	public void setMyGoalSectionList(List<MyGoal> myGoalSectionList) {
		this.myGoalSectionList = myGoalSectionList;
	}

	/**
	 * @return the ratingsList
	 */
	public List<Rating> getRatingsList() {
		return ratingsList;
	}

	/**
	 * @param ratingsList the ratingsList to set
	 */
	public void setRatingsList(List<Rating> ratingsList) {
		this.ratingsList = ratingsList;
	}
	
}
