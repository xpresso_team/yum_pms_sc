package com.yum.pa.domain;

public class FunctionLTRating extends OverAllRating {
	
	private static final long serialVersionUID = 1344751974149747265L;

	private Boolean additionalIncreaseNom;
	
	private Boolean isEligable;
	
	private Boolean devCheckIn;
	
	private String devCheckinText;

	/**
	 * @return the additionalIncreaseNom
	 */
	public Boolean getAdditionalIncreaseNom() {
		return additionalIncreaseNom;
	}

	/**
	 * @param additionalIncreaseNom the additionalIncreaseNom to set
	 */
	public void setAdditionalIncreaseNom(Boolean additionalIncreaseNom) {
		this.additionalIncreaseNom = additionalIncreaseNom;
	}

	/**
	 * @return the isEligable
	 */
	public Boolean getIsEligable() {
		return isEligable;
	}

	/**
	 * @param isEligable the isEligable to set
	 */
	public void setIsEligable(Boolean isEligable) {
		this.isEligable = isEligable;
	}

	/**
	 * @return the devCheckIn
	 */
	public Boolean getDevCheckIn() {
		return devCheckIn;
	}

	/**
	 * @param devCheckIn the devCheckIn to set
	 */
	public void setDevCheckIn(Boolean devCheckIn) {
		this.devCheckIn = devCheckIn;
	}

	/**
	 * @return the devCheckinText
	 */
	public String getDevCheckinText() {
		return devCheckinText;
	}

	/**
	 * @param devCheckinText the devCheckinText to set
	 */
	public void setDevCheckinText(String devCheckinText) {
		this.devCheckinText = devCheckinText;
	}
	
	
}
