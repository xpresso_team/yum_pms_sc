package com.yum.pa.domain;

import java.io.Serializable;
import java.util.Date;

import com.yum.pms.dao.GoalSection;
import com.yum.pms.utils.DateTimeUtils;

public class MyGoal implements Serializable {
	
	private static final long serialVersionUID = 4939532970348746681L;

	private Integer goalSecId;
	
	private String goalSecTimeline;
	
	private String goalSecDesc;
	
	private String selectedRatingEmp;
	
	private String selectedRatingSup;
	
	private String ratingRemarks;
	
	private String selfRemarks;
	
	private Date timeline;  // Now this field is used only for PA sheet PDF report. We can use it for other purpose also.
	
	public MyGoal() {}

	public MyGoal(Integer goalSecId, Date timeline, String goalSecDesc) {
		this.goalSecId = goalSecId;
		this.timeline = timeline;
		this.goalSecTimeline = DateTimeUtils.format(timeline, "yyyy-MM-dd");
		this.goalSecDesc = goalSecDesc;
	}
	
	public MyGoal(GoalSection goalSection) {
		this(goalSection.getGoalSectionId(), goalSection.getGoalSectionTimeline(), goalSection.getGoalSectionSummary());
	}

	/**
	 * @return the goalSecId
	 */
	public Integer getGoalSecId() {
		return goalSecId;
	}

	/**
	 * @param goalSecId the goalSecId to set
	 */
	public void setGoalSecId(Integer goalSecId) {
		this.goalSecId = goalSecId;
	}

	/**
	 * @return the goalSecTimeline
	 */
	public String getGoalSecTimeline() {
		return goalSecTimeline;
	}

	/**
	 * @param goalSecTimeline the goalSecTimeline to set
	 */
	public void setGoalSecTimeline(String goalSecTimeline) {
		this.goalSecTimeline = goalSecTimeline;
	}

	/**
	 * @return the goalSecDesc
	 */
	public String getGoalSecDesc() {
		return goalSecDesc;
	}

	/**
	 * @param goalSecDesc the goalSecDesc to set
	 */
	public void setGoalSecDesc(String goalSecDesc) {
		this.goalSecDesc = goalSecDesc;
	}

	/**
	 * @return the selectedRatingEmp
	 */
	public String getSelectedRatingEmp() {
		return selectedRatingEmp;
	}

	/**
	 * @param selectedRatingEmp the selectedRatingEmp to set
	 */
	public void setSelectedRatingEmp(String selectedRatingEmp) {
		this.selectedRatingEmp = selectedRatingEmp;
	}

	/**
	 * @return the selectedRatingSup
	 */
	public String getSelectedRatingSup() {
		return selectedRatingSup;
	}

	/**
	 * @param selectedRatingSup the selectedRatingSup to set
	 */
	public void setSelectedRatingSup(String selectedRatingSup) {
		this.selectedRatingSup = selectedRatingSup;
	}

	/**
	 * @return the ratingRemarks
	 */
	public String getRatingRemarks() {
		return ratingRemarks;
	}

	/**
	 * @param ratingRemarks the ratingRemarks to set
	 */
	public void setRatingRemarks(String ratingRemarks) {
		this.ratingRemarks = ratingRemarks;
	}

	/**
	 * @return the timeline
	 */
	public Date getTimeline() {
		return timeline;
	}

	/**
	 * @param timeline the timeline to set
	 */
	public void setTimeline(Date timeline) {
		this.timeline = timeline;
	}

	/**
	 * @return the selfRemarks
	 */
	public String getSelfRemarks() {
		return selfRemarks;
	}

	/**
	 * @param selfRemarks the selfRemarks to set
	 */
	public void setSelfRemarks(String selfRemarks) {
		this.selfRemarks = selfRemarks;
	}
	
}
