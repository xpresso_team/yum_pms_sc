package com.yum.pa.domain;

import java.util.ArrayList;
import java.util.List;

public class OverAllRating extends Rating {
	
	private static final long serialVersionUID = 3200629009429465866L;
	
	private Integer overAllRatingId;
	
	private String remarks;
	
	private Boolean promotion;
	
	private Double ifRangeLower;
	
	private Double ifRangeUpper;
	
	private List<ClasificationIFValue> listClasificationIFValue = new ArrayList<>();  
	
	private Boolean devCheckIn;
	
	private String devCheckinText;
	
	private List<Rating> ratingsList = new ArrayList<>();
	
	private List<Rating> cultureRatingsList = new ArrayList<>(); 
	
	private Boolean promoNomAug = false;
	
	public OverAllRating() {
	}

	public OverAllRating(String remarks, Boolean promotion) {
		this.remarks = remarks;
		this.promotion = promotion;
	}
	
	public OverAllRating(Integer overAllRatingId,Integer ratingId, String ratingDesc, String ratingValue, String remarks, Boolean promotion) {
		this.setOverAllRatingId(overAllRatingId); 
		this.setRatingId(ratingId);
		this.setRatingDesc(ratingDesc);
		this.setRatingValue(ratingValue);
		this.remarks = remarks;
		this.promotion = promotion;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the promotion
	 */
	public Boolean getPromotion() {
		return promotion;
	}

	/**
	 * @param promotion the promotion to set
	 */
	public void setPromotion(Boolean promotion) {
		this.promotion = promotion;
	}

	/**
	 * @return the overAllRatingId
	 */
	public Integer getOverAllRatingId() {
		return overAllRatingId;
	}

	/**
	 * @param overAllRatingId the overAllRatingId to set
	 */
	public void setOverAllRatingId(Integer overAllRatingId) {
		this.overAllRatingId = overAllRatingId;
	}
	
	/**
	 * @return the ifRangeLower
	 */
	public Double getIfRangeLower() {
		return ifRangeLower;
	}

	/**
	 * @param ifRangeLower the ifRangeLower to set
	 */
	public void setIfRangeLower(Double ifRangeLower) {
		this.ifRangeLower = ifRangeLower;
	}

	/**
	 * @return the ifRangeUpper
	 */
	public Double getIfRangeUpper() {
		return ifRangeUpper;
	}

	/**
	 * @param ifRangeUpper the ifRangeUpper to set
	 */
	public void setIfRangeUpper(Double ifRangeUpper) {
		this.ifRangeUpper = ifRangeUpper;
	}

	/**
	 * @return the devCheckIn
	 */
	public Boolean getDevCheckIn() {
		return devCheckIn;
	}

	/**
	 * @param devCheckIn the devCheckIn to set
	 */
	public void setDevCheckIn(Boolean devCheckIn) {
		this.devCheckIn = devCheckIn;
	}

	/**
	 * @return the devCheckinText
	 */
	public String getDevCheckinText() {
		return devCheckinText;
	}

	/**
	 * @param devCheckinText the devCheckinText to set
	 */
	public void setDevCheckinText(String devCheckinText) {
		this.devCheckinText = devCheckinText;
	}

	/**
	 * @return the listClasificationIFValue
	 */
	public List<ClasificationIFValue> getListClasificationIFValue() {
		return listClasificationIFValue;
	}

	/**
	 * @param listClasificationIFValue the listClasificationIFValue to set
	 */
	public void setListClasificationIFValue(List<ClasificationIFValue> listClasificationIFValue) {
		this.listClasificationIFValue = listClasificationIFValue;
	}

	/**
	 * @return the ratingsList
	 */
	public List<Rating> getRatingsList() {
		return ratingsList;
	}

	/**
	 * @param ratingsList the ratingsList to set
	 */
	public void setRatingsList(List<Rating> ratingsList) {
		this.ratingsList = ratingsList;
	}

	public Boolean getPromoNomAug() {
		return promoNomAug;
	}

	public void setPromoNomAug(Boolean promoNomAug) {
		this.promoNomAug = promoNomAug;
	}

	/**
	 * @return the cultureRatingsList
	 */
	public List<Rating> getCultureRatingsList() {
		return cultureRatingsList;
	}

	/**
	 * @param cultureRatingsList the cultureRatingsList to set
	 */
	public void setCultureRatingsList(List<Rating> cultureRatingsList) {
		this.cultureRatingsList = cultureRatingsList;
	}

}
