package com.yum.pa.domain;

import java.io.Serializable;

/**
 * @author jayanta.biswas
 * Created Date : 21-11-2018
 */
public class ClasificationIFValue implements Serializable {
	
	private static final long serialVersionUID = -138711902831168949L;

	private String designation;
	
	private Integer tenure;
	
	private Double ifValue;
	
	private Double vPay;
	
	private Double indivusalBudget;
	
	private Double ctc;
	
	private Boolean isEditeable;

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the tenure
	 */
	public Integer getTenure() {
		return tenure;
	}

	/**
	 * @param tenure the tenure to set
	 */
	public void setTenure(Integer tenure) {
		this.tenure = tenure;
	}

	/**
	 * @return the ifValue
	 */
	public Double getIfValue() {
		return ifValue;
	}

	/**
	 * @param ifValue the ifValue to set
	 */
	public void setIfValue(Double ifValue) {
		this.ifValue = ifValue;
	}

	/**
	 * @return the vPay
	 */
	public Double getvPay() {
		return vPay;
	}

	/**
	 * @param vPay the vPay to set
	 */
	public void setvPay(Double vPay) {
		this.vPay = vPay;
	}

	/**
	 * @return the isEditeable
	 */
	public Boolean getIsEditeable() {
		return isEditeable;
	}

	/**
	 * @param isEditeable the isEditeable to set
	 */
	public void setIsEditeable(Boolean isEditeable) {
		this.isEditeable = isEditeable;
	}

	/**
	 * @return the indivusalBudget
	 */
	public Double getIndivusalBudget() {
		return indivusalBudget;
	}

	/**
	 * @param indivusalBudget the indivusalBudget to set
	 */
	public void setIndivusalBudget(Double indivusalBudget) {
		this.indivusalBudget = indivusalBudget;
	}

	/**
	 * @return the ctc
	 */
	public Double getCtc() {
		return ctc;
	}

	/**
	 * @param ctc the ctc to set
	 */
	public void setCtc(Double ctc) {
		this.ctc = ctc;
	}
	
	
	

}
