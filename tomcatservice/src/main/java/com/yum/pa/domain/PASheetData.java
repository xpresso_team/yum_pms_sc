package com.yum.pa.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.yum.pms.utils.YumPmsConstants;

public class PASheetData implements Serializable {

	private static final long serialVersionUID = 2098137740347302520L;

	private Integer paSheetId;
	
	private String empId;
	
	private String empName;
	
	private Integer gradeId;
	
	private String grade;
	
	private String apprId;
	
	private String supervisorId;
	
	private String supervisorName;
	
	private String ltId;
	
	private String ltName;
	
	private String adminEmpId;
	
	private String function;
	
	private Integer workflowId = YumPmsConstants.PA_WORK_FLOW_INITIAL_STATUS_ID;
	
	private Integer ratingStatusId;
	
	private String saveByFlag;
	
	private Boolean isGoalSheetSubmitted;
	
	private Double ifWeightedPercentage;
	
	private Boolean underBudget = true;
	
	private Boolean promotionEligibility;
	
	private String promotionEligibilityMsg;
	
	private OverAllRating supRating = new OverAllRating();
	
	private FunctionLTRating funLtRating = new FunctionLTRating();
	
	private FunctionLTRating adminRating = new FunctionLTRating();
	
	private List<SelfSheetData> selfSheetList = new ArrayList<>();
	
	private Boolean aligningSup2;
	
	private Boolean ratingEnabled;
	
	private Boolean downloadPermission = false;
	
	private Boolean isAccepted = false;
	
	// These 3 fields are used in PDF report
	private String paSheetStatusDesc;
	
	private String empDesignation;
	
	private String apprDesc;
	
	private Boolean goalRating;

	/**
	 * @return the paSheetId
	 */
	public Integer getPaSheetId() {
		return paSheetId;
	}

	/**
	 * @param paSheetId the paSheetId to set
	 */
	public void setPaSheetId(Integer paSheetId) {
		this.paSheetId = paSheetId;
	}

	/**
	 * @return the empId
	 */
	public String getEmpId() {
		return empId;
	}

	/**
	 * @param empId the empId to set
	 */
	public void setEmpId(String empId) {
		this.empId = empId;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the apprId
	 */
	public String getApprId() {
		return apprId;
	}

	/**
	 * @param apprId the apprId to set
	 */
	public void setApprId(String apprId) {
		this.apprId = apprId;
	}

	/**
	 * @return the supervisorId
	 */
	public String getSupervisorId() {
		return supervisorId;
	}

	/**
	 * @param supervisorId the supervisorId to set
	 */
	public void setSupervisorId(String supervisorId) {
		this.supervisorId = supervisorId;
	}

	/**
	 * @return the supervisorName
	 */
	public String getSupervisorName() {
		return supervisorName;
	}

	/**
	 * @param supervisorName the supervisorName to set
	 */
	public void setSupervisorName(String supervisorName) {
		this.supervisorName = supervisorName;
	}

	/**
	 * @return the ltId
	 */
	public String getLtId() {
		return ltId;
	}

	/**
	 * @param ltId the ltId to set
	 */
	public void setLtId(String ltId) {
		this.ltId = ltId;
	}

	/**
	 * @return the ltName
	 */
	public String getLtName() {
		return ltName;
	}

	/**
	 * @param ltName the ltName to set
	 */
	public void setLtName(String ltName) {
		this.ltName = ltName;
	}

	/**
	 * @return the adminEmpId
	 */
	public String getAdminEmpId() {
		return adminEmpId;
	}

	/**
	 * @param adminEmpId the adminEmpId to set
	 */
	public void setAdminEmpId(String adminEmpId) {
		this.adminEmpId = adminEmpId;
	}

	/**
	 * @return the function
	 */
	public String getFunction() {
		return function;
	}

	/**
	 * @param function the function to set
	 */
	public void setFunction(String function) {
		this.function = function;
	}

	/**
	 * @return the workflowId
	 */
	public Integer getWorkflowId() {
		return workflowId;
	}

	/**
	 * @param workflowId the workflowId to set
	 */
	public void setWorkflowId(Integer workflowId) {
		this.workflowId = workflowId;
	}

	/**
	 * @return the saveByFlag
	 */
	public String getSaveByFlag() {
		return saveByFlag;
	}

	/**
	 * @param saveByFlag the saveByFlag to set
	 */
	public void setSaveByFlag(String saveByFlag) {
		this.saveByFlag = saveByFlag;
	}

	/**
	 * @return the isGoalSheetSubmitted
	 */
	public Boolean getIsGoalSheetSubmitted() {
		return isGoalSheetSubmitted;
	}

	/**
	 * @param isGoalSheetSubmitted the isGoalSheetSubmitted to set
	 */
	public void setIsGoalSheetSubmitted(Boolean isGoalSheetSubmitted) {
		this.isGoalSheetSubmitted = isGoalSheetSubmitted;
	}

	/**
	 * @return the ifWeightedPercentage
	 */
	public Double getIfWeightedPercentage() {
		return ifWeightedPercentage;
	}

	/**
	 * @param ifWeightedPercentage the ifWeightedPercentage to set
	 */
	public void setIfWeightedPercentage(Double ifWeightedPercentage) {
		this.ifWeightedPercentage = ifWeightedPercentage;
	}

	/**
	 * @return the underBudget
	 */
	public Boolean getUnderBudget() {
		return underBudget;
	}

	/**
	 * @param underBudget the underBudget to set
	 */
	public void setUnderBudget(Boolean underBudget) {
		this.underBudget = underBudget;
	}

	/**
	 * @return the promotionEligibility
	 */
	public Boolean getPromotionEligibility() {
		return promotionEligibility;
	}

	/**
	 * @param promotionEligibility the promotionEligibility to set
	 */
	public void setPromotionEligibility(Boolean promotionEligibility) {
		this.promotionEligibility = promotionEligibility;
	}

	/**
	 * @return the supRating
	 */
	public OverAllRating getSupRating() {
		return supRating;
	}

	/**
	 * @param supRating the supRating to set
	 */
	public void setSupRating(OverAllRating supRating) {
		this.supRating = supRating;
	}

	/**
	 * @return the funLtRating
	 */
	public FunctionLTRating getFunLtRating() {
		return funLtRating;
	}

	/**
	 * @param funLtRating the funLtRating to set
	 */
	public void setFunLtRating(FunctionLTRating funLtRating) {
		this.funLtRating = funLtRating;
	}

	/**
	 * @return the adminRating
	 */
	public FunctionLTRating getAdminRating() {
		return adminRating;
	}

	/**
	 * @param adminRating the adminRating to set
	 */
	public void setAdminRating(FunctionLTRating adminRating) {
		this.adminRating = adminRating;
	}

	/**
	 * @return the selfSheetList
	 */
	public List<SelfSheetData> getSelfSheetList() {
		return selfSheetList;
	}

	/**
	 * @param selfSheetList the selfSheetList to set
	 */
	public void setSelfSheetList(List<SelfSheetData> selfSheetList) {
		this.selfSheetList = selfSheetList;
	}

	/**
	 * @return the ratingStatusId
	 */
	public Integer getRatingStatusId() {
		return ratingStatusId;
	}

	/**
	 * @param ratingStatusId the ratingStatusId to set
	 */
	public void setRatingStatusId(Integer ratingStatusId) {
		this.ratingStatusId = ratingStatusId;
	}

	/**
	 * @return the aligningSup2
	 */
	public Boolean getAligningSup2() {
		return aligningSup2;
	}

	/**
	 * @param aligningSup2 the aligningSup2 to set
	 */
	public void setAligningSup2(Boolean aligningSup2) {
		this.aligningSup2 = aligningSup2;
	}

	/**
	 * @return the downloadPermission
	 */
	public Boolean getDownloadPermission() {
		return downloadPermission;
	}

	/**
	 * @param downloadPermission the downloadPermission to set
	 */
	public void setDownloadPermission(Boolean downloadPermission) {
		this.downloadPermission = downloadPermission;
	}

	/**
	 * @return the isAccepted
	 */
	public Boolean getIsAccepted() {
		return isAccepted;
	}

	/**
	 * @param isAccepted the isAccepted to set
	 */
	public void setIsAccepted(Boolean isAccepted) {
		this.isAccepted = isAccepted;
	}

	/**
	 * @return the paSheetStatusDesc
	 */
	public String getPaSheetStatusDesc() {
		return paSheetStatusDesc;
	}

	/**
	 * @param paSheetStatusDesc the paSheetStatusDesc to set
	 */
	public void setPaSheetStatusDesc(String paSheetStatusDesc) {
		this.paSheetStatusDesc = paSheetStatusDesc;
	}

	/**
	 * @return the empDesignation
	 */
	public String getEmpDesignation() {
		return empDesignation;
	}

	/**
	 * @param empDesignation the empDesignation to set
	 */
	public void setEmpDesignation(String empDesignation) {
		this.empDesignation = empDesignation;
	}

	/**
	 * @return the apprDesc
	 */
	public String getApprDesc() {
		return apprDesc;
	}

	/**
	 * @param apprDesc the apprDesc to set
	 */
	public void setApprDesc(String apprDesc) {
		this.apprDesc = apprDesc;
	}

	/**
	 * @return the gradeId
	 */
	public Integer getGradeId() {
		return gradeId;
	}

	/**
	 * @param gradeId the gradeId to set
	 */
	public void setGradeId(Integer gradeId) {
		this.gradeId = gradeId;
	}

	/**
	 * @return the grade
	 */
	public String getGrade() {
		return grade;
	}

	/**
	 * @param grade the grade to set
	 */
	public void setGrade(String grade) {
		this.grade = grade;
	}

	public Boolean getRatingEnabled() {
		return ratingEnabled;
	}

	public void setRatingEnabled(Boolean ratingEnabled) {
		this.ratingEnabled = ratingEnabled;
	}

	/**
	 * @return the promotionEligibilityMsg
	 */
	public String getPromotionEligibilityMsg() {
		return promotionEligibilityMsg;
	}

	/**
	 * @param promotionEligibilityMsg the promotionEligibilityMsg to set
	 */
	public void setPromotionEligibilityMsg(String promotionEligibilityMsg) {
		this.promotionEligibilityMsg = promotionEligibilityMsg;
	}

	/**
	 * @return the goalRating
	 */
	public Boolean getGoalRating() {
		return goalRating;
	}

	/**
	 * @param goalRating the goalRating to set
	 */
	public void setGoalRating(Boolean goalRating) {
		this.goalRating = goalRating;
	}
	
}
