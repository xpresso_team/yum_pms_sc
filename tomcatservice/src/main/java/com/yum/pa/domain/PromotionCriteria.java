package com.yum.pa.domain;

import java.io.Serializable;

import com.yum.pms.dao.PA_ConfigPromotionCriteria;

/**
 * @author dipak.swain
 * Used to deal with client/browser. Means this object convert to/from JSON and send/receive to/from
 */
public class PromotionCriteria implements Serializable {
	
	private static final long serialVersionUID = 4013656276622038949L;
	
	private Integer criteriaId;

	private Integer gradeFrom;
	
	private Integer gradeTo;
	
	private Integer tenureMonthsLower;
	
	private Integer tenureMonthsHigher;
	
	private Integer paRatingPrevious;
	
	private Integer paRatingCurrent;
	
	private Boolean ifApplicable;
	
	private Double ifMinCurrent;
	
	private String ifCheckLogic;
	
	private String ltsIdp;
	
	private boolean disable;
	
	public PromotionCriteria() {
	}
	
	public PromotionCriteria(PA_ConfigPromotionCriteria criteria) {
		
		this.criteriaId = criteria.getPaPromotionCriteriaId();
		this.gradeFrom = criteria.getGradeFrom();
		this.gradeTo = criteria.getGradeTo();
		this.tenureMonthsLower = criteria.getTenureMonthsRangeLower();
		this.tenureMonthsHigher = criteria.getTenureMonthsRangeHigher();
		this.paRatingPrevious = criteria.getPaRatingIdPrevious().getPaRatingId();
		this.paRatingCurrent = criteria.getPaRatingIdCurrent().getPaRatingId();
		this.ifMinCurrent = criteria.getIfMinCurrent();
		this.ifApplicable = this.ifMinCurrent != null && this.ifMinCurrent >= 0.0;
		this.ifCheckLogic = criteria.getIfCheckLogic();
		this.ltsIdp = criteria.getLtsIdpRatingCurrentMinLevel();
	}
	
	public Integer getCriteriaId() {
		return criteriaId;
	}

	public void setCriteriaId(Integer criteriaId) {
		this.criteriaId = criteriaId;
	}

	public Integer getGradeFrom() {
		return gradeFrom;
	}

	public void setGradeFrom(Integer gradeFrom) {
		this.gradeFrom = gradeFrom;
	}

	public Integer getGradeTo() {
		return gradeTo;
	}

	public void setGradeTo(Integer gradeTo) {
		this.gradeTo = gradeTo;
	}

	public Integer getTenureMonthsLower() {
		return tenureMonthsLower;
	}

	public void setTenureMonthsLower(Integer tenureMonthsLower) {
		this.tenureMonthsLower = tenureMonthsLower;
	}

	public Integer getTenureMonthsHigher() {
		return tenureMonthsHigher;
	}

	public void setTenureMonthsHigher(Integer tenureMonthsHigher) {
		this.tenureMonthsHigher = tenureMonthsHigher;
	}

	public Integer getPaRatingPrevious() {
		return paRatingPrevious;
	}

	public void setPaRatingPrevious(Integer paRatingPrevious) {
		this.paRatingPrevious = paRatingPrevious;
	}

	public Integer getPaRatingCurrent() {
		return paRatingCurrent;
	}

	public void setPaRatingCurrent(Integer paRatingCurrent) {
		this.paRatingCurrent = paRatingCurrent;
	}

	public Boolean getIfApplicable() {
		return ifApplicable;
	}

	public void setIfApplicable(Boolean ifApplicable) {
		this.ifApplicable = ifApplicable;
	}

	public Double getIfMinCurrent() {
		return ifMinCurrent;
	}

	public void setIfMinCurrent(Double ifMinCurrent) {
		this.ifMinCurrent = ifMinCurrent;
	}

	public String getIfCheckLogic() {
		return ifCheckLogic;
	}

	public void setIfCheckLogic(String ifCheckLogic) {
		this.ifCheckLogic = ifCheckLogic;
	}

	public String getLtsIdp() {
		return ltsIdp;
	}

	public void setLtsIdp(String ltsIdp) {
		this.ltsIdp = ltsIdp;
	}

	public boolean isDisable() {
		return disable;
	}

	public void setDisable(boolean disable) {
		this.disable = disable;
	}
	
}
