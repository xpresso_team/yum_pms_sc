package com.yum.pa.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.yum.pms.dao.PA_ConfigRatingBellCurveIFRange;

/**
 * @author dipak.swain
 *
 */
public class AdminPaConfig implements Serializable {
	
	private static final long serialVersionUID = 8730297298032408450L;

	private Integer apprId;
	
	private Double ifValue;
	
	private Double tfValue;
	
	private Double tfValueTb;
	
	private Double tfValuePh;
	
	private List<PA_ConfigRatingBellCurveIFRange> bellCurveIfRange = new ArrayList<>();
	
	private String sectionName;
	
	private String statusMsg;
	
	private String logonUser;
	
	public AdminPaConfig() {
	}
	
	public AdminPaConfig(Integer apprId) {
		this.apprId = apprId;
	}

	/**
	 * @return the apprId
	 */
	public Integer getApprId() {
		return apprId;
	}

	/**
	 * @param apprId the apprId to set
	 */
	public void setApprId(Integer apprId) {
		this.apprId = apprId;
	}

	/**
	 * @return the ifValue
	 */
	public Double getIfValue() {
		return ifValue;
	}

	/**
	 * @param ifValue the ifValue to set
	 */
	public void setIfValue(Double ifValue) {
		this.ifValue = ifValue;
	}

	/**
	 * @return the tfValue
	 */
	public Double getTfValue() {
		return tfValue;
	}

	/**
	 * @param tfValue the tfValue to set
	 */
	public void setTfValue(Double tfValue) {
		this.tfValue = tfValue;
	}

	/**
	 * @return the bellCurveIfRange
	 */
	public List<PA_ConfigRatingBellCurveIFRange> getBellCurveIfRange() {
		return bellCurveIfRange;
	}

	/**
	 * @param bellCurveIfRange the bellCurveIfRange to set
	 */
	public void setBellCurveIfRange(List<PA_ConfigRatingBellCurveIFRange> bellCurveIfRange) {
		this.bellCurveIfRange = bellCurveIfRange;
	}

	/**
	 * @return the sectionName
	 */
	public String getSectionName() {
		return sectionName;
	}

	/**
	 * @param sectionName the sectionName to set
	 */
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	/**
	 * @return the statusMsg
	 */
	public String getStatusMsg() {
		return statusMsg;
	}

	/**
	 * @param statusMsg the statusMsg to set
	 */
	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	/**
	 * @return the logonUser
	 */
	public String getLogonUser() {
		return logonUser;
	}

	/**
	 * @param logonUser the logonUser to set
	 */
	public void setLogonUser(String logonUser) {
		this.logonUser = logonUser;
	}

	/**
	 * @return the tfValueTb
	 */
	public Double getTfValueTb() {
		return tfValueTb;
	}

	/**
	 * @param tfValueTb the tfValueTb to set
	 */
	public void setTfValueTb(Double tfValueTb) {
		this.tfValueTb = tfValueTb;
	}

	/**
	 * @return the tfValuePh
	 */
	public Double getTfValuePh() {
		return tfValuePh;
	}

	/**
	 * @param tfValuePh the tfValuePh to set
	 */
	public void setTfValuePh(Double tfValuePh) {
		this.tfValuePh = tfValuePh;
	}
	
}
