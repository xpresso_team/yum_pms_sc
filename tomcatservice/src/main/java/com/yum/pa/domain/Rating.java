package com.yum.pa.domain;

import java.io.Serializable;

import com.yum.pms.dao.PA_RatingMaster;
import com.yum.pms.dao.PaCultureRatingMstr;

public class Rating implements Serializable {
	
	private static final long serialVersionUID = -8415022490967369251L;

	private Integer ratingId;
	
	private String ratingDesc;
	
	private String ratingValue;
	
	private Integer cultureRatingId;
	
	private String cultureRatingDesc;
	
	private String cultureRatingValue;
	
	public Rating() { }

	public Rating(Integer ratingId, String ratingDesc, String ratingValue) {
		this.ratingId = ratingId;
		this.ratingDesc = ratingDesc;
		this.ratingValue = ratingValue;
	}
	
	public Rating(PaCultureRatingMstr ratingMaster) {
		this.cultureRatingId = ratingMaster.getCultureRatingId();
		this.cultureRatingDesc = ratingMaster.getCultureRatingDesc();
		this.cultureRatingValue = ratingMaster.getPaCulturalRatingShortDesc();
	}
	
	public Rating(PA_RatingMaster ratingMaster) {
		this(ratingMaster.getPaRatingId(), ratingMaster.getPaRatingDesc(), ratingMaster.getPaRatingShortDesc());
	}

	/**
	 * @return the ratingId
	 */
	public Integer getRatingId() {
		return ratingId;
	}

	/**
	 * @param ratingId the ratingId to set
	 */
	public void setRatingId(Integer ratingId) {
		this.ratingId = ratingId;
	}

	/**
	 * @return the ratingDesc
	 */
	public String getRatingDesc() {
		return ratingDesc;
	}

	/**
	 * @param ratingDesc the ratingDesc to set
	 */
	public void setRatingDesc(String ratingDesc) {
		this.ratingDesc = ratingDesc;
	}

	/**
	 * @return the ratingValue
	 */
	public String getRatingValue() {
		return ratingValue;
	}

	/**
	 * @param ratingValue the ratingValue to set
	 */
	public void setRatingValue(String ratingValue) {
		this.ratingValue = ratingValue;
	}

	/**
	 * @return the cultureRatingId
	 */
	public Integer getCultureRatingId() {
		return cultureRatingId;
	}

	/**
	 * @param cultureRatingId the cultureRatingId to set
	 */
	public void setCultureRatingId(Integer cultureRatingId) {
		this.cultureRatingId = cultureRatingId;
	}

	/**
	 * @return the cultureRatingDesc
	 */
	public String getCultureRatingDesc() {
		return cultureRatingDesc;
	}

	/**
	 * @param cultureRatingDesc the cultureRatingDesc to set
	 */
	public void setCultureRatingDesc(String cultureRatingDesc) {
		this.cultureRatingDesc = cultureRatingDesc;
	}

	/**
	 * @return the cultureRatingValue
	 */
	public String getCultureRatingValue() {
		return cultureRatingValue;
	}

	/**
	 * @param cultureRatingValue the cultureRatingValue to set
	 */
	public void setCultureRatingValue(String cultureRatingValue) {
		this.cultureRatingValue = cultureRatingValue;
	}
	
}
