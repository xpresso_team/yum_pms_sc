package com.yum.idp.domain;

import java.io.Serializable;

import com.yum.pms.dao.IDP_SectionMaster;
import com.yum.pms.dao.IDP_SubSectionMaster;
import com.yum.pms.dao.IDP_SubSubSectionMaster;
import com.yum.pms.dao.IDP_SuggestedDevelopmentPlanMaster;
import com.yum.pms.dao.IDP_SuggestedGoalMaster;

public class IdpSuggestedDevelopmentActionPlan implements Serializable {

	private static final long serialVersionUID = -3777246416085818007L;
	
	private Integer idpSuggestedDevPlanSectionId;
	
	private String idpDevelopmentPlanSectionDesc;
	
	private Integer idpSuggestedGoalSectionId;
	
	private String createdBy;
	
	private String updatedBy;
	
	private String gradeRangeLower = "0";
	
	private String gradeRangeUpper = "99";
	
	private boolean removable = true;
	
	private String idpSubSubSectionDesc;
	
	private Integer idpSubSubSectionId;
	
	private Integer idpSubSectionId;
	
	private Integer idpSectionId;
	
	private Boolean isActive;
	
	public IdpSuggestedDevelopmentActionPlan() {
	}
	
	public IdpSuggestedDevelopmentActionPlan(Integer idpSuggestedGoalSectionId) {
		this.idpSuggestedGoalSectionId = idpSuggestedGoalSectionId;
	}

	public IdpSuggestedDevelopmentActionPlan(IDP_SuggestedDevelopmentPlanMaster suggestedDevelopmentPlanMstr) {
		this.idpSuggestedDevPlanSectionId = suggestedDevelopmentPlanMstr.getIdpSuggestedDevelopmentPlanSectionId();
		this.idpDevelopmentPlanSectionDesc = suggestedDevelopmentPlanMstr.getIdpDevelopmentPlanSectionDesc();
		this.createdBy = suggestedDevelopmentPlanMstr.getCreatedBy();
		this.updatedBy = suggestedDevelopmentPlanMstr.getUpdatedBy();
		this.gradeRangeLower = suggestedDevelopmentPlanMstr.getGradeRangeLower();
		this.gradeRangeUpper = suggestedDevelopmentPlanMstr.getGradeRangeUpper();
		this.removable = suggestedDevelopmentPlanMstr.isRemovable();
		this.isActive = suggestedDevelopmentPlanMstr.getIsActive();
		
		IDP_SuggestedGoalMaster suggestedGoalSectionMstr = suggestedDevelopmentPlanMstr.getSuggestedGoalMaster();
		if(suggestedGoalSectionMstr != null) {
			this.idpSuggestedGoalSectionId = suggestedGoalSectionMstr.getIdpSuggestedGoalSectionId();
		}
		
		IDP_SubSubSectionMaster subSubSectionMaster = suggestedDevelopmentPlanMstr.getSubSubSectionMaster();
		if(subSubSectionMaster != null) {
			this.idpSubSubSectionId = subSubSectionMaster.getIdpSubSubSectionId();
		}
		IDP_SubSectionMaster subSectionMaster = suggestedDevelopmentPlanMstr.getSubSectionMaster();
		if(subSectionMaster != null) {
			this.idpSubSectionId = subSectionMaster.getIdpSubSectionId();
		}
		IDP_SectionMaster sectionMaster = suggestedDevelopmentPlanMstr.getSectionMaster();
		if(sectionMaster != null) {
			this.idpSectionId = sectionMaster.getIdpSectionId();
		}
	}

	
	public Integer getIdpSuggestedDevelopmentPlanSectionId() {
		return idpSuggestedDevPlanSectionId;
	}

	public void setIdpSuggestedDevelopmentPlanSectionId(
			Integer idpSuggestedDevelopmentPlanSectionId) {
		this.idpSuggestedDevPlanSectionId = idpSuggestedDevelopmentPlanSectionId;
	}

	public String getIdpDevelopmentPlanSectionDesc() {
		return idpDevelopmentPlanSectionDesc;
	}

	public void setIdpDevelopmentPlanSectionDesc(
			String idpDevelopmentPlanSectionDesc) {
		this.idpDevelopmentPlanSectionDesc = idpDevelopmentPlanSectionDesc;
	}

	public Integer getIdpSuggestedGoalSectionId() {
		return idpSuggestedGoalSectionId;
	}

	public void setIdpSuggestedGoalSectionId(Integer idpSuggestedGoalSectionId) {
		this.idpSuggestedGoalSectionId = idpSuggestedGoalSectionId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getGradeRangeLower() {
		return gradeRangeLower;
	}

	public void setGradeRangeLower(String gradeRangeLower) {
		this.gradeRangeLower = gradeRangeLower;
	}

	public String getGradeRangeUpper() {
		return gradeRangeUpper;
	}

	public void setGradeRangeUpper(String gradeRangeUpper) {
		this.gradeRangeUpper = gradeRangeUpper;
	}

	public boolean isRemovable() {
		return removable;
	}

	public void setRemovable(boolean removable) {
		this.removable = removable;
	}

	public Integer getIdpSubSubSectionId() {
		return idpSubSubSectionId;
	}

	public void setIdpSubSubSectionId(Integer idpSubSubSectionId) {
		this.idpSubSubSectionId = idpSubSubSectionId;
	}

	public Integer getIdpSubSectionId() {
		return idpSubSectionId;
	}

	public void setIdpSubSectionId(Integer idpSubSectionId) {
		this.idpSubSectionId = idpSubSectionId;
	}

	public Integer getIdpSectionId() {
		return idpSectionId;
	}

	public void setIdpSectionId(Integer idpSectionId) {
		this.idpSectionId = idpSectionId;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getIdpSubSubSectionDesc() {
		return idpSubSubSectionDesc;
	}

	public void setIdpSubSubSectionDesc(String idpSubSubSectionDesc) {
		this.idpSubSubSectionDesc = idpSubSubSectionDesc;
	}

	

}
