package com.yum.idp.domain;

import java.io.Serializable;

import com.yum.pms.dao.IDP_SectionMaster;
import com.yum.pms.dao.IDP_SubSectionMaster;

public class IdpSubCategory implements Serializable {
	
	private static final long serialVersionUID = 26390194401805984L;

	private Integer idpSubSectionId;
	
	private String idpSubSectionDesc;
	
	private String createdBy;
	
	private String updatedBy;
	
	private String gradeRangeLower = "0";
	
	private String gradeRangeUpper = "99";
	
	private boolean removable = true;
	
	private Integer idpSectionId;
	
	private Boolean isActive;
	
	private Integer apprId;
	
	public Integer getApprId() {
		return apprId;
	}

	public void setApprId(Integer apprId) {
		this.apprId = apprId;
	}

	public IdpSubCategory() {
	}
	
	public IdpSubCategory(Integer idpSubSectionId) {
		this.idpSubSectionId = idpSubSectionId;
	}

	public IdpSubCategory(IDP_SubSectionMaster subSection) {
		this.idpSubSectionId = subSection.getIdpSubSectionId();
		this.idpSubSectionDesc = subSection.getIdpSubSectionDesc();
		this.createdBy = subSection.getCreatedBy();
		this.updatedBy = subSection.getUpdatedBy();
		this.gradeRangeLower = subSection.getGradeRangeLower();
		this.gradeRangeUpper = subSection.getGradeRangeUpper();
		this.removable = subSection.isRemovable();
		this.isActive = subSection.getIsActive();
		this.apprId = subSection.getApprId();
		IDP_SectionMaster sectionMaster = subSection.getSectionMaster();
		if(sectionMaster != null) {
			this.idpSectionId = sectionMaster.getIdpSectionId();
		}
	}

	public Integer getIdpSubSectionId() {
		return idpSubSectionId;
	}

	public void setIdpSubSectionId(Integer idpSubSectionId) {
		this.idpSubSectionId = idpSubSectionId;
	}

	public String getIdpSubSectionDesc() {
		return idpSubSectionDesc;
	}

	public void setIdpSubSectionDesc(String idpSubSectionDesc) {
		this.idpSubSectionDesc = idpSubSectionDesc;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getGradeRangeLower() {
		return gradeRangeLower;
	}

	public void setGradeRangeLower(String gradeRangeLower) {
		this.gradeRangeLower = gradeRangeLower;
	}

	public String getGradeRangeUpper() {
		return gradeRangeUpper;
	}

	public void setGradeRangeUpper(String gradeRangeUpper) {
		this.gradeRangeUpper = gradeRangeUpper;
	}

	public boolean isRemovable() {
		return removable;
	}

	public void setRemovable(boolean removable) {
		this.removable = removable;
	}

	public Integer getIdpSectionId() {
		return idpSectionId;
	}

	public void setIdpSectionId(Integer idpSectionId) {
		this.idpSectionId = idpSectionId;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}
