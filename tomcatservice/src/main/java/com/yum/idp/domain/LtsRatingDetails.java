/**
 * 
 */
package com.yum.idp.domain;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * @author jayanta.biswas
 *
 */
public class LtsRatingDetails implements Serializable {

	private static final long serialVersionUID = -5216663206050693267L;

	private Integer idpSheetId;
	
	private Integer appraisalId;
	
	private Integer reporteeId;
	
	private String reporteeName;
	
	private Integer userId;
	
	private String username;
	
	private String designation;
	
	private String grade;
	
	private Integer supervisorId;
	
	private String supervisorName;

	private String remarksSup = StringUtils.EMPTY;
	
	private String remarksLts = StringUtils.EMPTY;
	
	private String remarksHr = StringUtils.EMPTY;
	
	private String cultureRatingSup = StringUtils.EMPTY;
	
	private String cutureRatingLts = StringUtils.EMPTY;
	
	private String cultureRatingHr = StringUtils.EMPTY;
	
	private String ltsRatingSup = StringUtils.EMPTY;
	
	private String ltsRatingLts = StringUtils.EMPTY;
	
	private String ltsRatingHr = StringUtils.EMPTY;
	
	private Integer ratingStatusId;
	
	private List<UiControll> performanceList;
	
	private String submittedTo;
	
	private String operationMsg;
	
	private Integer functionLtsId;
	
	private String functionLtsName;
	
	private Boolean isChecked;
	/**
	 * @return the idpSheetId
	 */
	public Integer getIdpSheetId() {
		return idpSheetId;
	}

	/**
	 * @param idpSheetId the idpSheetId to set
	 */
	public void setIdpSheetId(Integer idpSheetId) {
		this.idpSheetId = idpSheetId;
	}

	/**
	 * @return the reporteeId
	 */
	public Integer getReporteeId() {
		return reporteeId;
	}

	/**
	 * @return the appraisalId
	 */
	public Integer getAppraisalId() {
		return appraisalId;
	}

	/**
	 * @param appraisalId the appraisalId to set
	 */
	public void setAppraisalId(Integer appraisalId) {
		this.appraisalId = appraisalId;
	}

	/**
	 * @param reporteeId the reporteeId to set
	 */
	public void setReporteeId(Integer reporteeId) {
		this.reporteeId = reporteeId;
	}

	/**
	 * @return the reporteeName
	 */
	public String getReporteeName() {
		return reporteeName;
	}

	/**
	 * @param reporteeName the reporteeName to set
	 */
	public void setReporteeName(String reporteeName) {
		this.reporteeName = reporteeName;
	}

	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the grade
	 */
	public String getGrade() {
		return grade;
	}

	/**
	 * @param grade the grade to set
	 */
	public void setGrade(String grade) {
		this.grade = grade;
	}

	/**
	 * @return the supervisorId
	 */
	public Integer getSupervisorId() {
		return supervisorId;
	}

	/**
	 * @param supervisorId the supervisorId to set
	 */
	public void setSupervisorId(Integer supervisorId) {
		this.supervisorId = supervisorId;
	}

	/**
	 * @return the supervisorName
	 */
	public String getSupervisorName() {
		return supervisorName;
	}

	/**
	 * @param supervisorName the supervisorName to set
	 */
	public void setSupervisorName(String supervisorName) {
		this.supervisorName = supervisorName;
	}

	/**
	 * @return the remarksSup
	 */
	public String getRemarksSup() {
		return remarksSup;
	}

	/**
	 * @param remarksSup the remarksSup to set
	 */
	public void setRemarksSup(String remarksSup) {
		this.remarksSup = remarksSup;
	}

	/**
	 * @return the remarksLts
	 */
	public String getRemarksLts() {
		return remarksLts;
	}

	/**
	 * @param remarksLts the remarksLts to set
	 */
	public void setRemarksLts(String remarksLts) {
		this.remarksLts = remarksLts;
	}

	/**
	 * @return the remarksHr
	 */
	public String getRemarksHr() {
		return remarksHr;
	}

	/**
	 * @param remarksHr the remarksHr to set
	 */
	public void setRemarksHr(String remarksHr) {
		this.remarksHr = remarksHr;
	}

	/**
	 * @return the cultureRatingSup
	 */
	public String getCultureRatingSup() {
		return cultureRatingSup;
	}

	/**
	 * @param cultureRatingSup the cultureRatingSup to set
	 */
	public void setCultureRatingSup(String cultureRatingSup) {
		this.cultureRatingSup = cultureRatingSup;
	}

	/**
	 * @return the cutureRatingLts
	 */
	public String getCutureRatingLts() {
		return cutureRatingLts;
	}

	/**
	 * @param cutureRatingLts the cutureRatingLts to set
	 */
	public void setCutureRatingLts(String cutureRatingLts) {
		this.cutureRatingLts = cutureRatingLts;
	}

	/**
	 * @return the cultureRatingHr
	 */
	public String getCultureRatingHr() {
		return cultureRatingHr;
	}

	/**
	 * @param cultureRatingHr the cultureRatingHr to set
	 */
	public void setCultureRatingHr(String cultureRatingHr) {
		this.cultureRatingHr = cultureRatingHr;
	}

	/**
	 * @return the ltsRatingSup
	 */
	public String getLtsRatingSup() {
		return ltsRatingSup;
	}

	/**
	 * @param ltsRatingSup the ltsRatingSup to set
	 */
	public void setLtsRatingSup(String ltsRatingSup) {
		this.ltsRatingSup = ltsRatingSup;
	}

	/**
	 * @return the ltsRatingLts
	 */
	public String getLtsRatingLts() {
		return ltsRatingLts;
	}

	/**
	 * @param ltsRatingLts the ltsRatingLts to set
	 */
	public void setLtsRatingLts(String ltsRatingLts) {
		this.ltsRatingLts = ltsRatingLts;
	}

	/**
	 * @return the ltsRatingHr
	 */
	public String getLtsRatingHr() {
		return ltsRatingHr;
	}

	/**
	 * @param ltsRatingHr the ltsRatingHr to set
	 */
	public void setLtsRatingHr(String ltsRatingHr) {
		this.ltsRatingHr = ltsRatingHr;
	}

	/**
	 * @return the ratingStatusId
	 */
	public Integer getRatingStatusId() {
		return ratingStatusId;
	}

	/**
	 * @param ratingStatusId the ratingStatusId to set
	 */
	public void setRatingStatusId(Integer ratingStatusId) {
		this.ratingStatusId = ratingStatusId;
	}

	/**
	 * @return the performanceList
	 */
	public List<UiControll> getPerformanceList() {
		return performanceList;
	}

	/**
	 * @param performanceList the performanceList to set
	 */
	public void setPerformanceList(List<UiControll> performanceList) {
		this.performanceList = performanceList;
	}

	/**
	 * @return the submittedTo
	 */
	public String getSubmittedTo() {
		return submittedTo;
	}

	/**
	 * @param submittedTo the submittedTo to set
	 */
	public void setSubmittedTo(String submittedTo) {
		this.submittedTo = submittedTo;
	}

	/**
	 * @return the operationMsg
	 */
	public String getOperationMsg() {
		return operationMsg;
	}

	/**
	 * @param operationMsg the operationMsg to set
	 */
	public void setOperationMsg(String operationMsg) {
		this.operationMsg = operationMsg;
	}

	/**
	 * @return the functionLtsId
	 */
	public Integer getFunctionLtsId() {
		return functionLtsId;
	}

	/**
	 * @param functionLtsId the functionLtsId to set
	 */
	public void setFunctionLtsId(Integer functionLtsId) {
		this.functionLtsId = functionLtsId;
	}

	/**
	 * @return the functionLtsName
	 */
	public String getFunctionLtsName() {
		return functionLtsName;
	}

	/**
	 * @param functionLtsName the functionLtsName to set
	 */
	public void setFunctionLtsName(String functionLtsName) {
		this.functionLtsName = functionLtsName;
	}

	/**
	 * @return the isChecked
	 */
	public Boolean getIsChecked() {
		return isChecked;
	}

	/**
	 * @param isChecked the isChecked to set
	 */
	public void setIsChecked(Boolean isChecked) {
		this.isChecked = isChecked;
	}
	
	

}
