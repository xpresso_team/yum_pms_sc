package com.yum.idp.domain;

import java.io.Serializable;

import com.yum.pms.dao.IDP_SectionMaster;
import com.yum.pms.dao.IDP_SubSectionMaster;
import com.yum.pms.dao.IDP_SubSubSectionMaster;

public class IdpSubSubCategory implements Serializable {
	
	private static final long serialVersionUID = 7665574334188795661L;

	private Integer idpSubSubSectionId;
	
	private String idpSubSubSectionDesc;
	
	private String createdBy;
	
	private String updatedBy;
	
	private String gradeRangeLower = "0";
	
	private String gradeRangeUpper = "99";
	
	private boolean removable = true;
	
	private Integer idpSubSectionId;
	
	private Integer idpSectionId;
	
	private Boolean isActive;
	
	private Integer apprId;
	
	public Integer getApprId() {
		return apprId;
	}

	public void setApprId(Integer apprId) {
		this.apprId = apprId;
	}

	public IdpSubSubCategory() {
	}
	
	public IdpSubSubCategory(Integer idpSubSubSectionId) {
		this.idpSubSubSectionId = idpSubSubSectionId;
	}

	public IdpSubSubCategory(IDP_SubSubSectionMaster subSubSection) {
		this.idpSubSubSectionId = subSubSection.getIdpSubSubSectionId();
		this.idpSubSubSectionDesc = subSubSection.getIdpSubSubSectionDesc();
		this.createdBy = subSubSection.getCreatedBy();
		this.updatedBy = subSubSection.getUpdatedBy();
		this.gradeRangeLower = subSubSection.getGradeRangeLower();
		this.gradeRangeUpper = subSubSection.getGradeRangeUpper();
		this.removable = subSubSection.isRemovable();
		this.isActive = subSubSection.getIsActive();
		this.apprId = subSubSection.getApprId();
		IDP_SubSectionMaster subSectionMaster = subSubSection.getSubSectionMaster();
		if(subSectionMaster != null) {
			this.idpSubSectionId = subSectionMaster.getIdpSubSectionId();
		}
		IDP_SectionMaster sectionMaster = subSubSection.getSectionMaster();
		if(sectionMaster != null) {
			this.idpSectionId = sectionMaster.getIdpSectionId();
		}
	}

	public Integer getIdpSubSubSectionId() {
		return idpSubSubSectionId;
	}

	public void setIdpSubSubSectionId(Integer idpSubSubSectionId) {
		this.idpSubSubSectionId = idpSubSubSectionId;
	}

	public String getIdpSubSubSectionDesc() {
		return idpSubSubSectionDesc;
	}

	public void setIdpSubSubSectionDesc(String idpSubSubSectionDesc) {
		this.idpSubSubSectionDesc = idpSubSubSectionDesc;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getGradeRangeLower() {
		return gradeRangeLower;
	}

	public void setGradeRangeLower(String gradeRangeLower) {
		this.gradeRangeLower = gradeRangeLower;
	}

	public String getGradeRangeUpper() {
		return gradeRangeUpper;
	}

	public void setGradeRangeUpper(String gradeRangeUpper) {
		this.gradeRangeUpper = gradeRangeUpper;
	}

	public boolean isRemovable() {
		return removable;
	}

	public void setRemovable(boolean removable) {
		this.removable = removable;
	}

	public Integer getIdpSubSectionId() {
		return idpSubSectionId;
	}

	public void setIdpSubSectionId(Integer idpSubSectionId) {
		this.idpSubSectionId = idpSubSectionId;
	}

	public Integer getIdpSectionId() {
		return idpSectionId;
	}

	public void setIdpSectionId(Integer idpSectionId) {
		this.idpSectionId = idpSectionId;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	
}
