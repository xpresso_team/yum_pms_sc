package com.yum.idp.domain;

import java.io.Serializable;

public class UiSubControll implements Serializable {

	private static final long serialVersionUID = -3610724083187251789L;

	private Integer Id;
	
	private String label;
	
	private String value;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	

}
