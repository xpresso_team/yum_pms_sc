package com.yum.idp.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UiControll implements Serializable {

	private static final long serialVersionUID = -8189542133549067160L;
	
	private Integer id;
	
	private Integer dataId;

	private Integer desDetailID;
	
	private String label;
	
	private String name;
	
	private String type;
	
	private List<UiSubControll> options = new ArrayList<>();
	
	private String value;
	
	private String ltRatingValue;
	
	private String supRatingValue;
	
	private String validation;
	
	private String column = "double_part";
	
	private String remarks;
	
	private Boolean actionPending;
	
	/**
	 * @return
	 */
	public Integer getDesDetailID() {
		return desDetailID;
	}

	/**
	 * @param desDetailID
	 */
	public void setDesDetailID(Integer desDetailID) {
		this.desDetailID = desDetailID;
	}

	/**
	 * @return
	 */
	public Integer getDataId() {
		return dataId;
	}

	/**
	 * @param dataId
	 */
	public void setDataId(Integer dataId) {
		this.dataId = dataId;
	}

	/**
	 * @return
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return
	 */
	public List<UiSubControll> getOptions() {
		return options;
	}

	/**
	 * @param options
	 */
	public void setOptions(List<UiSubControll> options) {
		this.options = options;
	}

	/**
	 * @return
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the ltRatingValue
	 */
	public String getLtRatingValue() {
		return ltRatingValue;
	}

	/**
	 * @param ltRatingValue the ltRatingValue to set
	 */
	public void setLtRatingValue(String ltRatingValue) {
		this.ltRatingValue = ltRatingValue;
	}

	/**
	 * @return the supRatingValue
	 */
	public String getSupRatingValue() {
		return supRatingValue;
	}

	/**
	 * @param supRatingValue the supRatingValue to set
	 */
	public void setSupRatingValue(String supRatingValue) {
		this.supRatingValue = supRatingValue;
	}

	/**
	 * @return
	 */
	public String getValidation() {
		return validation;
	}

	/**
	 * @param validation
	 */
	public void setValidation(String validation) {
		this.validation = validation;
	}

	/**
	 * @return
	 */
	public String getColumn() {
		return column;
	}

	/**
	 * @param column
	 */
	public void setColumn(String column) {
		this.column = column;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the actionPending
	 */
	public Boolean getActionPending() {
		return actionPending;
	}

	/**
	 * @param actionPending the actionPending to set
	 */
	public void setActionPending(Boolean actionPending) {
		this.actionPending = actionPending;
	}

}
