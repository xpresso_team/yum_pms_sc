/**
 * 
 */
package com.yum.idp.domain;

import java.io.Serializable;

/**
 * @author jayanta.biswas
 *
 */
public class RemarksDetails implements Serializable {

	private static final long serialVersionUID = 687702960551684431L;

	private Integer idpRemarksId;
	
	private String howILeadAppreciateRemarks;
	
	private String howILeadEffectivenessRemarks;
	
	private String howILeadGrowthRemarks;
	
	private String myPlanRemarks;
	
	private String developmentActionPlanRemarks;
	
	private String performanceCheckInGoalsRemarks;
	
	private String balanceYearGoalsRemarks;

	/**
	 * @return the idpRemarksId
	 */
	public Integer getIdpRemarksId() {
		return idpRemarksId;
	}

	/**
	 * @param idpRemarksId the idpRemarksId to set
	 */
	public void setIdpRemarksId(Integer idpRemarksId) {
		this.idpRemarksId = idpRemarksId;
	}

	/**
	 * @return the howILeadAppreciateRemarks
	 */
	public String getHowILeadAppreciateRemarks() {
		return howILeadAppreciateRemarks;
	}

	/**
	 * @param howILeadAppreciateRemarks the howILeadAppreciateRemarks to set
	 */
	public void setHowILeadAppreciateRemarks(String howILeadAppreciateRemarks) {
		this.howILeadAppreciateRemarks = howILeadAppreciateRemarks;
	}

	/**
	 * @return the howILeadEffectivenessRemarks
	 */
	public String getHowILeadEffectivenessRemarks() {
		return howILeadEffectivenessRemarks;
	}

	/**
	 * @param howILeadEffectivenessRemarks the howILeadEffectivenessRemarks to set
	 */
	public void setHowILeadEffectivenessRemarks(String howILeadEffectivenessRemarks) {
		this.howILeadEffectivenessRemarks = howILeadEffectivenessRemarks;
	}

	/**
	 * @return the howILeadGrowthRemarks
	 */
	public String getHowILeadGrowthRemarks() {
		return howILeadGrowthRemarks;
	}

	/**
	 * @param howILeadGrowthRemarks the howILeadGrowthRemarks to set
	 */
	public void setHowILeadGrowthRemarks(String howILeadGrowthRemarks) {
		this.howILeadGrowthRemarks = howILeadGrowthRemarks;
	}

	/**
	 * @return the myPlanRemarks
	 */
	public String getMyPlanRemarks() {
		return myPlanRemarks;
	}

	/**
	 * @param myPlanRemarks the myPlanRemarks to set
	 */
	public void setMyPlanRemarks(String myPlanRemarks) {
		this.myPlanRemarks = myPlanRemarks;
	}

	/**
	 * @return the developmentActionPlanRemarks
	 */
	public String getDevelopmentActionPlanRemarks() {
		return developmentActionPlanRemarks;
	}

	/**
	 * @param developmentActionPlanRemarks the developmentActionPlanRemarks to set
	 */
	public void setDevelopmentActionPlanRemarks(String developmentActionPlanRemarks) {
		this.developmentActionPlanRemarks = developmentActionPlanRemarks;
	}

	/**
	 * @return the performanceCheckInGoalsRemarks
	 */
	public String getPerformanceCheckInGoalsRemarks() {
		return performanceCheckInGoalsRemarks;
	}

	/**
	 * @param performanceCheckInGoalsRemarks the performanceCheckInGoalsRemarks to set
	 */
	public void setPerformanceCheckInGoalsRemarks(String performanceCheckInGoalsRemarks) {
		this.performanceCheckInGoalsRemarks = performanceCheckInGoalsRemarks;
	}

	/**
	 * @return the balanceYearGoalsRemarks
	 */
	public String getBalanceYearGoalsRemarks() {
		return balanceYearGoalsRemarks;
	}

	/**
	 * @param balanceYearGoalsRemarks the balanceYearGoalsRemarks to set
	 */
	public void setBalanceYearGoalsRemarks(String balanceYearGoalsRemarks) {
		this.balanceYearGoalsRemarks = balanceYearGoalsRemarks;
	}
	
	
}
