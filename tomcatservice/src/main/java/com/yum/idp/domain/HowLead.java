package com.yum.idp.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HowLead implements Serializable {

	private static final long serialVersionUID = 7015275849176239405L;
	
	private List<HowLeadSubData> appreciate = new ArrayList<>();
	private List<HowLeadSubData> effectiveness = new ArrayList<>();
	private HowLeadSubData growth = new HowLeadSubData();
	
	
	public List<HowLeadSubData> getAppreciate() {
		return appreciate;
	}
	public void setAppreciate(List<HowLeadSubData> appreciate) {
		this.appreciate = appreciate;
	}
	public List<HowLeadSubData> getEffectiveness() {
		return effectiveness;
	}
	public void setEffectiveness(List<HowLeadSubData> effectiveness) {
		this.effectiveness = effectiveness;
	}
	public HowLeadSubData getGrowth() {
		return growth;
	}
	public void setGrowth(HowLeadSubData growth) {
		this.growth = growth;
	}
	
	
	
}
