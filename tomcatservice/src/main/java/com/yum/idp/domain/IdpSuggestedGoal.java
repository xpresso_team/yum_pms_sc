package com.yum.idp.domain;

import java.io.Serializable;

import com.yum.pms.dao.IDP_SectionMaster;
import com.yum.pms.dao.IDP_SubSectionMaster;
import com.yum.pms.dao.IDP_SubSubSectionMaster;
import com.yum.pms.dao.IDP_SuggestedGoalMaster;

public class IdpSuggestedGoal implements Serializable {
	
	private static final long serialVersionUID = 4106090435671914210L;

	private Integer idpSuggestedGoalSectionId;
	
	private String idpSuggestedGoalSectionDesc;
	
	private String createdBy;
	
	private String updatedBy;
	
	private String gradeRangeLower = "0";
	
	private String gradeRangeUpper = "99";
	
	private boolean removable = true;
	
	private String idpSubSubSectionDesc;
	
	private Integer idpSubSubSectionId;
	
	private Integer idpSubSectionId;
	
	private Integer idpSectionId;
	
	private Boolean isActive;
	
	public IdpSuggestedGoal() {
	}
	
	public IdpSuggestedGoal(Integer idpSuggestedGoalSectionId) {
		this.idpSuggestedGoalSectionId = idpSuggestedGoalSectionId;
	}

	public IdpSuggestedGoal(IDP_SuggestedGoalMaster suggestedGoalSection) {
		this.idpSuggestedGoalSectionId = suggestedGoalSection.getIdpSuggestedGoalSectionId();
		this.idpSuggestedGoalSectionDesc = suggestedGoalSection.getIdpSuggestedGoalSectionDesc();
		this.createdBy = suggestedGoalSection.getCreatedBy();
		this.updatedBy = suggestedGoalSection.getUpdatedBy();
		this.gradeRangeLower = suggestedGoalSection.getGradeRangeLower();
		this.gradeRangeUpper = suggestedGoalSection.getGradeRangeUpper();
		this.removable = suggestedGoalSection.isRemovable();
		this.isActive = suggestedGoalSection.getIsActive();
		IDP_SubSubSectionMaster subSubSectionMaster = suggestedGoalSection.getSubSubSectionMaster();
		if(subSubSectionMaster != null) {
			this.idpSubSubSectionId = subSubSectionMaster.getIdpSubSubSectionId();
		}
		IDP_SubSectionMaster subSectionMaster = suggestedGoalSection.getSubSectionMaster();
		if(subSectionMaster != null) {
			this.idpSubSectionId = subSectionMaster.getIdpSubSectionId();
		}
		IDP_SectionMaster sectionMaster = suggestedGoalSection.getSectionMaster();
		if(sectionMaster != null) {
			this.idpSectionId = sectionMaster.getIdpSectionId();
		}
	}

	public Integer getIdpSuggestedGoalSectionId() {
		return idpSuggestedGoalSectionId;
	}

	public void setIdpSuggestedGoalSectionId(Integer idpSuggestedGoalSectionId) {
		this.idpSuggestedGoalSectionId = idpSuggestedGoalSectionId;
	}

	public String getIdpSuggestedGoalSectionDesc() {
		return idpSuggestedGoalSectionDesc;
	}

	public void setIdpSuggestedGoalSectionDesc(String idpSuggestedGoalSectionDesc) {
		this.idpSuggestedGoalSectionDesc = idpSuggestedGoalSectionDesc;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getGradeRangeLower() {
		return gradeRangeLower;
	}

	public void setGradeRangeLower(String gradeRangeLower) {
		this.gradeRangeLower = gradeRangeLower;
	}

	public String getGradeRangeUpper() {
		return gradeRangeUpper;
	}

	public void setGradeRangeUpper(String gradeRangeUpper) {
		this.gradeRangeUpper = gradeRangeUpper;
	}

	public boolean isRemovable() {
		return removable;
	}

	public void setRemovable(boolean removable) {
		this.removable = removable;
	}

	public Integer getIdpSubSubSectionId() {
		return idpSubSubSectionId;
	}

	public void setIdpSubSubSectionId(Integer idpSubSubSectionId) {
		this.idpSubSubSectionId = idpSubSubSectionId;
	}

	public Integer getIdpSubSectionId() {
		return idpSubSectionId;
	}

	public void setIdpSubSectionId(Integer idpSubSectionId) {
		this.idpSubSectionId = idpSubSectionId;
	}

	public Integer getIdpSectionId() {
		return idpSectionId;
	}

	public void setIdpSectionId(Integer idpSectionId) {
		this.idpSectionId = idpSectionId;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getIdpSubSubSectionDesc() {
		return idpSubSubSectionDesc;
	}

	public void setIdpSubSubSectionDesc(String idpSubSubSectionDesc) {
		this.idpSubSubSectionDesc = idpSubSubSectionDesc;
	}
	
}
