package com.yum.idp.domain;

import java.io.Serializable;
import java.util.Date;

public class BalanceOfGoal implements Serializable {

	private static final long serialVersionUID = -2477248171958738272L;

	private Integer balanceYrGoalId;
	
	private String balanceGoalDesc;
	
	private Integer goalSectionId;
	
	private Boolean isChecked;
	
	private Date updatedDate;
	
	private Date idpGoalSectionTimeline;
	
	private Boolean isDeletedFromGoalSheet = false;

	public Integer getBalanceYrGoalId() {
		return balanceYrGoalId;
	}

	public void setBalanceYrGoalId(Integer balanceYrGoalId) {
		this.balanceYrGoalId = balanceYrGoalId;
	}

	public String getBalanceGoalDesc() {
		return balanceGoalDesc;
	}

	public void setBalanceGoalDesc(String balanceGoalDesc) {
		this.balanceGoalDesc = balanceGoalDesc;
	}

	public Integer getGoalSectionId() {
		return goalSectionId;
	}

	public void setGoalSectionId(Integer goalSectionId) {
		this.goalSectionId = goalSectionId;
	}

	public Boolean getIsChecked() {
		return isChecked;
	}

	public void setIsChecked(Boolean isChecked) {
		this.isChecked = isChecked;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Date getIdpGoalSectionTimeline() {
		return idpGoalSectionTimeline;
	}

	public void setIdpGoalSectionTimeline(Date idpGoalSectionTimeline) {
		this.idpGoalSectionTimeline = idpGoalSectionTimeline;
	}

	public Boolean getIsDeletedFromGoalSheet() {
		return isDeletedFromGoalSheet;
	}

	public void setIsDeletedFromGoalSheet(Boolean isDeletedFromPms) {
		this.isDeletedFromGoalSheet = isDeletedFromPms;
	}

	@Override
	public String toString() {
		return "BalanceOfGoal [balanceYrGoalId=" + balanceYrGoalId + ", balanceGoalDesc=" + balanceGoalDesc
				+ ", goalSectionId=" + goalSectionId + ", isChecked=" + isChecked + ", updatedDate=" + updatedDate
				+ ", idpGoalSectionTimeline=" + idpGoalSectionTimeline + ", isDeletedFromPms=" + isDeletedFromGoalSheet + "]";
	}

}
