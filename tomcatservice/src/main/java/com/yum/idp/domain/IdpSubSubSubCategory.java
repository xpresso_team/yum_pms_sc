package com.yum.idp.domain;

import java.io.Serializable;

import com.yum.pms.dao.IDP_SectionMaster;
import com.yum.pms.dao.IDP_SubSectionMaster;
import com.yum.pms.dao.IDP_SubSubSectionMaster;
import com.yum.pms.dao.IDP_SubSubSubSectionMaster;

public class IdpSubSubSubCategory implements Serializable {
	
	private static final long serialVersionUID = 4106090435671914210L;

	private Integer idpSubSubSubSectionId;
	
	private String idpSubSubSubSectionDesc;
	
	private String createdBy;
	
	private String updatedBy;
	
	private String gradeRangeLower = "0";
	
	private String gradeRangeUpper = "99";
	
	private boolean removable = true;
	
	private Integer idpSubSubSectionId;
	
	private Integer idpSubSectionId;
	
	private Integer idpSectionId;
	
	private Boolean isActive;
	
	public IdpSubSubSubCategory() {
	}
	
	public IdpSubSubSubCategory(Integer idpSubSubSubSectionId) {
		this.idpSubSubSubSectionId = idpSubSubSubSectionId;
	}

	public IdpSubSubSubCategory(IDP_SubSubSubSectionMaster subSubSubSection) {
		this.idpSubSubSubSectionId = subSubSubSection.getIdpSubSubSubSectionId();
		this.idpSubSubSubSectionDesc = subSubSubSection.getIdpSubSubSubSectionDesc();
		this.createdBy = subSubSubSection.getCreatedBy();
		this.updatedBy = subSubSubSection.getUpdatedBy();
		this.gradeRangeLower = subSubSubSection.getGradeRangeLower();
		this.gradeRangeUpper = subSubSubSection.getGradeRangeUpper();
		this.removable = subSubSubSection.isRemovable();
		this.isActive = subSubSubSection.getIsActive();
		IDP_SubSubSectionMaster subSubSectionMaster = subSubSubSection.getSubSubSectionMaster();
		if(subSubSectionMaster != null) {
			this.idpSubSubSectionId = subSubSectionMaster.getIdpSubSubSectionId();
		}
		IDP_SubSectionMaster subSectionMaster = subSubSubSection.getSubSectionMaster();
		if(subSectionMaster != null) {
			this.idpSubSectionId = subSectionMaster.getIdpSubSectionId();
		}
		IDP_SectionMaster sectionMaster = subSubSubSection.getSectionMaster();
		if(sectionMaster != null) {
			this.idpSectionId = sectionMaster.getIdpSectionId();
		}
	}

	public Integer getIdpSubSubSubSectionId() {
		return idpSubSubSubSectionId;
	}

	public void setIdpSubSubSubSectionId(Integer idpSubSubSubSectionId) {
		this.idpSubSubSubSectionId = idpSubSubSubSectionId;
	}

	public String getIdpSubSubSubSectionDesc() {
		return idpSubSubSubSectionDesc;
	}

	public void setIdpSubSubSubSectionDesc(String idpSubSubSubSectionDesc) {
		this.idpSubSubSubSectionDesc = idpSubSubSubSectionDesc;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getGradeRangeLower() {
		return gradeRangeLower;
	}

	public void setGradeRangeLower(String gradeRangeLower) {
		this.gradeRangeLower = gradeRangeLower;
	}

	public String getGradeRangeUpper() {
		return gradeRangeUpper;
	}

	public void setGradeRangeUpper(String gradeRangeUpper) {
		this.gradeRangeUpper = gradeRangeUpper;
	}

	public boolean isRemovable() {
		return removable;
	}

	public void setRemovable(boolean removable) {
		this.removable = removable;
	}

	public Integer getIdpSubSubSectionId() {
		return idpSubSubSectionId;
	}

	public void setIdpSubSubSectionId(Integer idpSubSubSectionId) {
		this.idpSubSubSectionId = idpSubSubSectionId;
	}

	public Integer getIdpSubSectionId() {
		return idpSubSectionId;
	}

	public void setIdpSubSectionId(Integer idpSubSectionId) {
		this.idpSubSectionId = idpSubSectionId;
	}

	public Integer getIdpSectionId() {
		return idpSectionId;
	}

	public void setIdpSectionId(Integer idpSectionId) {
		this.idpSectionId = idpSectionId;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	
}
