package com.yum.idp.domain;

import java.io.Serializable;
import java.util.List;

import com.yum.pms.dao.IDP_SelectedSuggestedGoalDtl;

/**
 * @author jayanta.biswas
 * @comment This is a pojo class for data sending
 * @version 1.0
 * @since   21-12-2017
 */
public class IdpUIConfig implements Serializable {

	private static final long serialVersionUID = -7796701037255109496L;
	
	private Integer empId;
	
	private String empName;
	
	private String empGrade;
	
	private Integer appraisalId;
	
	private String appraisalDescription;
	
	private Integer idpSheetId;
	
	private String idpSheetStatus;
	
	private String SupervisorName;
	
	private Integer supervisorId;
	
	private String functionName;
	
	private String functionLTName;
	
	private String empDesignation;
	
	private Integer ltId;

	private List<UiControll> planList;
	
	private List<DevelopmentActionPlan> developmentActionPlanList;
	
	private List<IdpSuggestedDevelopmentActionPlan> suggestedDevActionPlanMstr;
	
	private List<UiControll> performenceList;
	
	private List<BalanceOfGoal> balanceGoal;
	
	private HowLead howLead;
	
	private List<IdpSuggestedGoal> idpSuggestedGoals;
	
	private List<IDP_SelectedSuggestedGoalDtl> idpSelectedSuggestedGoalList;
	
	private String flag;
	
	private Boolean GoalSheetIsCompleted = false;
	
	private String submitedTo;
	
	private String performanceRemarks;
	
	private Integer ratingStatusId;
	
	private Boolean aligningSup2;
	
	private Boolean rollBackEnable = true;
	
	private RemarksDetails remarksDetails = new RemarksDetails();
	

	public List<UiControll> getPlanList() {
		return planList;
	}

	public void setPlanList(List<UiControll> planList) {
		this.planList = planList;
	}

	public List<DevelopmentActionPlan> getDevelopmentActionPlanList() {
		return developmentActionPlanList;
	}

	public void setDevelopmentActionPlanList(List<DevelopmentActionPlan> developmentActionPlanList) {
		this.developmentActionPlanList = developmentActionPlanList;
	}

	public List<UiControll> getPerformenceList() {
		return performenceList;
	}

	public void setPerformenceList(List<UiControll> performenceList) {
		this.performenceList = performenceList;
	}

	public List<BalanceOfGoal> getBalanceGoal() {
		return balanceGoal;
	}

	public void setBalanceGoal(List<BalanceOfGoal> balanceGoal) {
		this.balanceGoal = balanceGoal;
	}

	public HowLead getHowLead() {
		return howLead;
	}

	public void setHowLead(HowLead howLead) {
		this.howLead = howLead;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public Integer getAppraisalId() {
		return appraisalId;
	}

	public void setAppraisalId(Integer appraisalId) {
		this.appraisalId = appraisalId;
	}

	public Integer getIdpSheetId() {
		return idpSheetId;
	}

	public void setIdpSheetId(Integer idpSheetId) {
		this.idpSheetId = idpSheetId;
	}

	public String getIdpSheetStatus() {
		return idpSheetStatus;
	}

	public void setIdpSheetStatus(String idpSheetStatus) {
		this.idpSheetStatus = idpSheetStatus;
	}

	public String getSupervisorName() {
		return SupervisorName;
	}

	public void setSupervisorName(String supervisorName) {
		SupervisorName = supervisorName;
	}

	public Integer getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(Integer supervisorId) {
		this.supervisorId = supervisorId;
	}

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	public String getFunctionLTName() {
		return functionLTName;
	}

	public void setFunctionLTName(String functionLTName) {
		this.functionLTName = functionLTName;
	}

	public Integer getLtId() {
		return ltId;
	}

	public void setLtId(Integer ltId) {
		this.ltId = ltId;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public Boolean getGoalSheetIsCompleted() {
		return GoalSheetIsCompleted;
	}

	public void setGoalSheetIsCompleted(Boolean goalSheetIsCompleted) {
		GoalSheetIsCompleted = goalSheetIsCompleted;
	}

	public String getEmpGrade() {
		return empGrade;
	}

	public void setEmpGrade(String empGrade) {
		this.empGrade = empGrade;
	}

	public String getAppraisalDescription() {
		return appraisalDescription;
	}

	public void setAppraisalDescription(String appraisalDescription) {
		this.appraisalDescription = appraisalDescription;
	}

	public String getEmpDesignation() {
		return empDesignation;
	}

	public void setEmpDesignation(String empDesignation) {
		this.empDesignation = empDesignation;
	}

	public String getSubmitedTo() {
		return submitedTo;
	}

	public void setSubmitedTo(String submitedTo) {
		this.submitedTo = submitedTo;
	}

	public String getPerformanceRemarks() {
		return performanceRemarks;
	}

	public void setPerformanceRemarks(String performanceRemarks) {
		this.performanceRemarks = performanceRemarks;
	}

	public Integer getRatingStatusId() {
		return ratingStatusId;
	}

	public void setRatingStatusId(Integer ratingStatusId) {
		this.ratingStatusId = ratingStatusId;
	}

	/**
	 * @return the rollBackEnable
	 */
	public Boolean getRollBackEnable() {
		return rollBackEnable;
	}

	/**
	 * @param rollBackEnable the rollBackEnable to set
	 */
	public void setRollBackEnable(Boolean rollBackEnable) {
		this.rollBackEnable = rollBackEnable;
	}

	/**
	 * @return the remarksDetails
	 */
	public RemarksDetails getRemarksDetails() {
		return remarksDetails;
	}

	/**
	 * @param remarksDetails the remarksDetails to set
	 */
	public void setRemarksDetails(RemarksDetails remarksDetails) {
		this.remarksDetails = remarksDetails;
	}

	/**
	 * @return the aligningSup2
	 */
	public Boolean getAligningSup2() {
		return aligningSup2;
	}

	/**
	 * @param aligningSup2 the aligningSup2 to set
	 */
	public void setAligningSup2(Boolean aligningSup2) {
		this.aligningSup2 = aligningSup2;
	}

	public List<IdpSuggestedGoal> getIdpSuggestedGoals() {
		return idpSuggestedGoals;
	}

	public void setIdpSuggestedGoals(List<IdpSuggestedGoal> idpSuggestedGoals) {
		this.idpSuggestedGoals = idpSuggestedGoals;
	}

	public List<IDP_SelectedSuggestedGoalDtl> getIdpSelectedSuggestedGoalList() {
		return idpSelectedSuggestedGoalList;
	}

	public void setIdpSelectedSuggestedGoalList(
			List<IDP_SelectedSuggestedGoalDtl> idpSelectedSuggestedGoalList) {
		this.idpSelectedSuggestedGoalList = idpSelectedSuggestedGoalList;
	}

	public List<IdpSuggestedDevelopmentActionPlan> getSuggestedDevActionPlanMstr() {
		return suggestedDevActionPlanMstr;
	}

	public void setSuggestedDevActionPlanMstr(
			List<IdpSuggestedDevelopmentActionPlan> suggestedDevActionPlanMstr) {
		this.suggestedDevActionPlanMstr = suggestedDevActionPlanMstr;
	}

	@Override
	public String toString() {
		return "IdpUIConfig [empId=" + empId + ", empName=" + empName
				+ ", empGrade=" + empGrade + ", appraisalId=" + appraisalId
				+ ", appraisalDescription=" + appraisalDescription
				+ ", idpSheetId=" + idpSheetId + ", idpSheetStatus="
				+ idpSheetStatus + ", SupervisorName=" + SupervisorName
				+ ", supervisorId=" + supervisorId + ", functionName="
				+ functionName + ", functionLTName=" + functionLTName
				+ ", empDesignation=" + empDesignation + ", ltId=" + ltId
				+ ", planList=" + planList + ", developmentActionPlanList="
				+ developmentActionPlanList + ", suggestedDevActionPlanMstr="
				+ suggestedDevActionPlanMstr + ", performenceList="
				+ performenceList + ", balanceGoal=" + balanceGoal
				+ ", howLead=" + howLead + ", idpSuggestedGoals="
				+ idpSuggestedGoals + ", idpSelectedSuggestedGoalList="
				+ idpSelectedSuggestedGoalList + ", flag=" + flag
				+ ", GoalSheetIsCompleted=" + GoalSheetIsCompleted
				+ ", submitedTo=" + submitedTo + ", performanceRemarks="
				+ performanceRemarks + ", ratingStatusId=" + ratingStatusId
				+ ", aligningSup2=" + aligningSup2 + ", rollBackEnable="
				+ rollBackEnable + ", remarksDetails=" + remarksDetails + "]";
	}

	
	
}
