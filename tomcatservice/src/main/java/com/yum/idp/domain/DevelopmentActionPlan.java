package com.yum.idp.domain;

import java.io.Serializable;

/**
 * @author jayanta.biswas
 * @comment This is a pojo class for data sending
 * @version 1.0
 * @since   23-01-2018
 */
public class DevelopmentActionPlan implements Serializable {

	private static final long serialVersionUID = -4875022319373923298L;

	private Integer devActionId;
	
	private String devActionSummary;
	
	private Boolean isOnGoing;
	
	private String remarks;
	
	private String createdBy;
	
	private String createdDate;
	
	private String updatedBy;
	
	private String updatedDate;
	
	private String timeline;
	
	private Boolean isDelete = false;
	
	private Integer idpSuggestedGoalSectionId;
	
	private Integer idpSuggestedDevelopmentPlanSectionId;

	public Integer getDevActionId() {
		return devActionId;
	}

	public void setDevActionId(Integer devActionId) {
		this.devActionId = devActionId;
	}

	public String getDevActionSummary() {
		return devActionSummary;
	}

	public void setDevActionSummary(String devActionSummary) {
		this.devActionSummary = devActionSummary;
	}

	public Boolean getIsOnGoing() {
		return isOnGoing;
	}

	public void setIsOnGoing(Boolean isOnGoing) {
		this.isOnGoing = isOnGoing;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getTimeline() {
		return timeline;
	}

	public void setTimeline(String timeline) {
		this.timeline = timeline;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Integer getIdpSuggestedGoalSectionId() {
		return idpSuggestedGoalSectionId;
	}

	public void setIdpSuggestedGoalSectionId(Integer idpSuggestedGoalSectionId) {
		this.idpSuggestedGoalSectionId = idpSuggestedGoalSectionId;
	}

	public Integer getIdpSuggestedDevelopmentPlanSectionId() {
		return idpSuggestedDevelopmentPlanSectionId;
	}

	public void setIdpSuggestedDevelopmentPlanSectionId(
			Integer idpSuggestedDevelopmentPlanSectionId) {
		this.idpSuggestedDevelopmentPlanSectionId = idpSuggestedDevelopmentPlanSectionId;
	}

	@Override
	public String toString() {
		return "DevelopmentActionPlan [devActionId=" + devActionId
				+ ", devActionSummary=" + devActionSummary + ", isOnGoing="
				+ isOnGoing + ", remarks=" + remarks + ", createdBy="
				+ createdBy + ", createdDate=" + createdDate + ", updatedBy="
				+ updatedBy + ", updatedDate=" + updatedDate + ", timeline="
				+ timeline + ", isDelete=" + isDelete
				+ ", idpSuggestedGoalSectionId=" + idpSuggestedGoalSectionId
				+ ", idpSuggestedDevelopmentPlanSectionId="
				+ idpSuggestedDevelopmentPlanSectionId + "]";
	}

	

}
