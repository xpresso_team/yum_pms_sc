/**
 * 
 */
package com.yum.idp.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * @author jayanta.biswas
 *
 */
public class EmployeeIdpSheetStatusHistory implements Serializable {
	
	private static final long serialVersionUID = 7037659315860079160L;

	private int idpSheetId;
	
	private String updatedByEmpName; 

	private String goalStatusName;
	
	private Date updateDate;

	public int getIdpSheetId() {
		return idpSheetId;
	}

	public void setIdpSheetId(int idpSheetId) {
		this.idpSheetId = idpSheetId;
	}

	public String getUpdatedByEmpName() {
		return updatedByEmpName;
	}

	public void setUpdatedByEmpName(String updatedByEmpName) {
		this.updatedByEmpName = updatedByEmpName;
	}

	public String getGoalStatusName() {
		return goalStatusName;
	}

	public void setGoalStatusName(String goalStatusName) {
		this.goalStatusName = goalStatusName;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Override
	public String toString() {
		return "EmployeeIdpSheetHistory [idpSheetId=" + idpSheetId + ", updatedByEmpName=" + updatedByEmpName
				+ ", goalStatusName=" + goalStatusName + ", updateDate=" + updateDate + "]";
	}

}
