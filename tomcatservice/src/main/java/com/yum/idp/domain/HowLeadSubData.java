package com.yum.idp.domain;

import java.io.Serializable;
import java.util.Date;

public class HowLeadSubData implements Serializable {

	private static final long serialVersionUID = 585286469286642909L;

	private Integer catagoryDetailId;
	
	private Integer categoryId;
	
	private String categoryDesc;
	
	private String description;
	
	private Integer subCategoryId;
	
	private String subCategoryDesc;
	
	private Integer subSubCategoryId;
	
	private String subSubCategoryDesc;
	
	private Integer idpSuggestedGoalId;
	
	private String idpSuggestedGoalDesc;
	
	private Integer subSubSubCategoryId;
	
	private String subSubSubCategoryDesc;
	
	private String freeText;
	
	private Boolean isDelete;
	
	private String createdBy;
	
	private String updatedBy;
	
	private Date createdDate;
	
	private Date updatedDate;  
	
	private Integer sectionApprId;

	public Integer getCatagoryDetailId() {
		return catagoryDetailId;
	}

	public void setCatagoryDetailId(Integer catagoryDetailId) {
		this.catagoryDetailId = catagoryDetailId;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryDesc() {
		return categoryDesc;
	}

	public void setCategoryDesc(String categoryDesc) {
		this.categoryDesc = categoryDesc;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	/**
	 * @return the subCategoryId
	 */
	public Integer getSubCategoryId() {
		return subCategoryId;
	}

	/**
	 * @param subCategoryId the subCategoryId to set
	 */
	public void setSubCategoryId(Integer subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	/**
	 * @return the subCategoryDesc
	 */
	public String getSubCategoryDesc() {
		return subCategoryDesc;
	}

	/**
	 * @param subCategoryDesc the subCategoryDesc to set
	 */
	public void setSubCategoryDesc(String subCategoryDesc) {
		this.subCategoryDesc = subCategoryDesc;
	}

	/**
	 * @return the subSubCategoryId
	 */
	public Integer getSubSubCategoryId() {
		return subSubCategoryId;
	}

	/**
	 * @param subSubCategoryId the subSubCategoryId to set
	 */
	public void setSubSubCategoryId(Integer subSubCategoryId) {
		this.subSubCategoryId = subSubCategoryId;
	}

	/**
	 * @return the subSubCategoryDesc
	 */
	public String getSubSubCategoryDesc() {
		return subSubCategoryDesc;
	}

	/**
	 * @param subSubCategoryDesc the subSubCategoryDesc to set
	 */
	public void setSubSubCategoryDesc(String subSubCategoryDesc) {
		this.subSubCategoryDesc = subSubCategoryDesc;
	}

	/**
	 * @return the subSubSubCategoryId
	 */
	public Integer getSubSubSubCategoryId() {
		return subSubSubCategoryId;
	}

	/**
	 * @param subSubSubCategoryId the subSubSubCategoryId to set
	 */
	public void setSubSubSubCategoryId(Integer subSubSubCategoryId) {
		this.subSubSubCategoryId = subSubSubCategoryId;
	}

	/**
	 * @return the subSubSubCategoryDesc
	 */
	public String getSubSubSubCategoryDesc() {
		return subSubSubCategoryDesc;
	}

	/**
	 * @param subSubSubCategoryDesc the subSubSubCategoryDesc to set
	 */
	public void setSubSubSubCategoryDesc(String subSubSubCategoryDesc) {
		this.subSubSubCategoryDesc = subSubSubCategoryDesc;
	}

	/**
	 * @return the freeText
	 */
	public String getFreeText() {
		return freeText;
	}

	/**
	 * @param freeText the freeText to set
	 */
	public void setFreeText(String freeText) {
		this.freeText = freeText;
	}

	public Integer getIdpSuggestedGoalId() {
		return idpSuggestedGoalId;
	}

	public void setIdpSuggestedGoalId(Integer idpSuggestedGoalId) {
		this.idpSuggestedGoalId = idpSuggestedGoalId;
	}

	public String getIdpSuggestedGoalDesc() {
		return idpSuggestedGoalDesc;
	}

	public void setIdpSuggestedGoalDesc(String idpSuggestedGoalDesc) {
		this.idpSuggestedGoalDesc = idpSuggestedGoalDesc;
	}

	public Integer getSectionApprId() {
		return sectionApprId;
	}

	public void setSectionApprId(Integer sectionApprId) {
		this.sectionApprId = sectionApprId;
	}
	
	
	

}
