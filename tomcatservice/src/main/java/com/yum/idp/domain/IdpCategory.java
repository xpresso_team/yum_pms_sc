package com.yum.idp.domain;

import java.io.Serializable;

import com.yum.pms.dao.IDP_SectionMaster;

public class IdpCategory implements Serializable {
	
	private static final long serialVersionUID = -4410559021183018988L;

	private Integer idpSectionId;
	
	private String idpSectionDesc;
	
	private String createdBy;
	
	private String updatedBy;
	
	private String idpSectionHoverDesc;
	
	private String gradeRangeLower = "0";
	
	private String gradeRangeUpper = "99";
	
	private boolean removable = true;
	
	private Boolean isActive;
	
	private Integer apprId;
	
	private String idpSectionTypeFlag;
	
	public String getIdpSectionTypeFlag() {
		return idpSectionTypeFlag;
	}

	public void setIdpSectionTypeFlag(String idpSectionTypeFlag) {
		this.idpSectionTypeFlag = idpSectionTypeFlag;
	}

	public Integer getApprId() {
		return apprId;
	}

	public void setApprId(Integer apprId) {
		this.apprId = apprId;
	}

	public IdpCategory() {
	}

	public IdpCategory(Integer idpSectionId) {
		super();
		this.idpSectionId = idpSectionId;
	}

	public IdpCategory(IDP_SectionMaster section) {
		this.idpSectionId = section.getIdpSectionId();
		this.idpSectionDesc = section.getIdpSectionDesc();
		this.idpSectionHoverDesc = section.getIdpSectionHoverDesc();
		this.createdBy = section.getCreatedBy();
		this.updatedBy = section.getUpdatedBy();
		this.gradeRangeLower = section.getGradeRangeLower();
		this.gradeRangeUpper = section.getGradeRangeUpper();
		this.removable = section.isRemovable();
		this.isActive = section.getIsActive();
		this.apprId = section.getApprId();
		this.idpSectionTypeFlag= section.getIdpSectionTypeFlag();
	}
	
	public Integer getIdpSectionId() {
		return idpSectionId;
	}

	public void setIdpSectionId(Integer idpSectionId) {
		this.idpSectionId = idpSectionId;
	}

	public String getIdpSectionDesc() {
		return idpSectionDesc;
	}

	public void setIdpSectionDesc(String idpSectionDesc) {
		this.idpSectionDesc = idpSectionDesc;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getIdpSectionHoverDesc() {
		return idpSectionHoverDesc;
	}

	public void setIdpSectionHoverDesc(String idpSectionHoverDesc) {
		this.idpSectionHoverDesc = idpSectionHoverDesc;
	}

	public String getGradeRangeLower() {
		return gradeRangeLower;
	}

	public void setGradeRangeLower(String gradeRangeLower) {
		this.gradeRangeLower = gradeRangeLower;
	}

	public String getGradeRangeUpper() {
		return gradeRangeUpper;
	}

	public void setGradeRangeUpper(String gradeRangeUpper) {
		this.gradeRangeUpper = gradeRangeUpper;
	}

	public boolean isRemovable() {
		return removable;
	}

	public void setRemovable(boolean removable) {
		this.removable = removable;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}
