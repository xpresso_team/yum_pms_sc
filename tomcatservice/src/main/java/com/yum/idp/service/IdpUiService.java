package com.yum.idp.service;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yum.pms.dao.IDP_PerformanceUIConfig;
import com.yum.pms.dao.IDP_PlanUIConfig;
import com.yum.pms.service.YumHibernateUtilServices;
import com.yum.pms.utils.YumPmsConstants;


/**
 * @author jayanta.biswas
 * @comment This Class created for fetch all UI related information for design of IDP form.
 * @version 1.0
 * @since   21-12-2017
 */
@Service
@SuppressWarnings("unchecked")
public class IdpUiService {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	/**
	 * @return List<IDP_PlanUIConfig> planUiList
	 * @throws Exception
	 */
	public List<IDP_PlanUIConfig> getPlanUI() {	

		return YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(IDP_PlanUIConfig.class)
				.createAlias(YumPmsConstants.IDP_PLAN_MASTER, YumPmsConstants.PLAN)
				.addOrder(Order.asc("plan.idpPlanId"))
				.list();
	}
	
	/**
	 * @return List<IDP_PerformanceUIConfig> performanceUiList
	 * @throws Exception
	 */
	public List<IDP_PerformanceUIConfig> getPerformanceUI() {	

		return YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(IDP_PerformanceUIConfig.class)
				.list();
	}

}
