package com.yum.idp.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yum.pms.dao.IDP_PerformanceRatingStatus;
import com.yum.pms.dao.IDP_SheetMaster;
import com.yum.pms.dao.IDP_SheetStatus;
import com.yum.pms.dao.IDP_SheetStatusWorkFlow;
import com.yum.pms.dao.IDP_SheetWorkflowHistory;
import com.yum.pms.dao.IdpPerformanceRatingWorkflow;
import com.yum.pms.exception.YumPMSDataSaveException;
import com.yum.pms.service.YumHibernateUtilServices;
import com.yum.pms.utils.YumPmsConstants;

/**
 * @author jayanta.biswas
 *
 */
@Service
@SuppressWarnings("unchecked")
public class WorkFlowHistoryService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(WorkFlowHistoryService.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	/**
	 * @param Integer idpSheetId
	 * @return  List<IDP_SheetWorkflowHistory> lIdpSheetHistory
	 */
	public List<IDP_SheetWorkflowHistory> getWorkFlowHistoryStatus(Integer idpSheetId) {

		String hql = String.format("FROM IDP_SheetWorkflowHistory idpwfw WHERE idpwfw.idpSheetId = :%s ORDER BY createdDate DESC", 
				YumPmsConstants.IDP_SHEET_ID); 
		Query query = YumHibernateUtilServices.getSession(sessionFactory)
				.createQuery(hql)
				.setParameter(YumPmsConstants.IDP_SHEET_ID, idpSheetId);
		List<IDP_SheetWorkflowHistory> lIdpSheetHistory = null;
		try {
			lIdpSheetHistory = query.list();
		} catch (Exception e) {
			//no action required
		}
		return lIdpSheetHistory;
	}
	
	public IDP_SheetStatus getIdpStatus(Integer idpStatusID) {

		return 
				(IDP_SheetStatus)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(IDP_SheetStatus.class)
				.add(Restrictions.eq(YumPmsConstants.IDP_SHEET_STATUS_ID, idpStatusID))
				.uniqueResult(); 
	}
	
	public IDP_SheetStatus getIdpStatusIdByIdpSheetId(Integer idpSheetId) {

		IDP_SheetStatus idpSheetStatus = 
				(IDP_SheetStatus) YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(IDP_SheetMaster.class)
				.setProjection(Projections.projectionList().add(Projections.property(YumPmsConstants.IDP_STATUS)))
				.add(Restrictions.eq(YumPmsConstants.IDP_SHEET_ID, idpSheetId)).uniqueResult();  
		return idpSheetStatus == null ? new IDP_SheetStatus() : idpSheetStatus;
	}
	
	public IDP_PerformanceRatingStatus getIdpRatingStatusIdByIdpSheetId(Integer idpSheetId) {

		IDP_PerformanceRatingStatus idpSheetStatus = 
				(IDP_PerformanceRatingStatus)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(IDP_SheetMaster.class)
				.setProjection(Projections.projectionList()
						.add(Projections.property(YumPmsConstants.IDP_PERFORMANCE_RATING_LTS_WORKFLOW_STATUS_ID)))
				.add(Restrictions.eq(YumPmsConstants.IDP_SHEET_ID, idpSheetId))
				.uniqueResult();  
		return idpSheetStatus == null ? new IDP_PerformanceRatingStatus() : idpSheetStatus;
	}

	
	public List<IDP_SheetStatusWorkFlow> getWorkStatusByIdpSheetId(Integer idpSheetStatusId) {

		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(IDP_SheetStatusWorkFlow.class)
				.add(Restrictions.eq(YumPmsConstants.IDP_SHEET_STATUS_ID, idpSheetStatusId))
				.list();  
	}
	
	public List<IdpPerformanceRatingWorkflow> getRatingStatusByIdpSheetId(Integer ratingStatusId) {

		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(IdpPerformanceRatingWorkflow.class)
				.add(Restrictions.eq(YumPmsConstants.IDP_PERFORMANCE_RATING_LTS_WORKFLOW_STATUS_ID, ratingStatusId))
				.list();  
	}
	
	/**
	 * @param Integer idpSheetId
	 * @param Integer statusId
	 * @param String createdBy
	 * @return String msg
	 * @throws YumPMSDataSaveException
	 */
	public String updateIdpSheetStatus(Integer idpSheetId, Integer statusId, String createdBy, Integer ratingStatusId) throws YumPMSDataSaveException {

		String msg = StringUtils.EMPTY;
		IDP_PerformanceRatingStatus ratingStatus = new IDP_PerformanceRatingStatus();
		ratingStatus.setIdpPerformanceRatingLTSWorkflowStatusId(ratingStatusId);

		IDP_SheetStatus idpStatus = new IDP_SheetStatus();
		idpStatus.setIdpSheetStatusId(statusId); 
		IDP_SheetMaster passingGoalSheet = YumHibernateUtilServices.getSession(sessionFactory).get(IDP_SheetMaster.class, idpSheetId);
		passingGoalSheet.setIdpStatus(idpStatus);  
		passingGoalSheet.setUpdatedBy(createdBy);
		passingGoalSheet.setIdpPerformanceRatingLTSWorkflowStatusId(ratingStatus);
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
		try {
			currentSession.merge(passingGoalSheet);
			YumHibernateUtilServices.commitTransaction(currentSession); 
			msg = YumPmsConstants.UPDATED_SUCESSFULLY;
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to update Goal Sheet Status Values", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return msg;
	}
	
	/**
	 * @param Integer idpSheetId
	 * @param Integer selectedGoalId
	 * @return String msg
	 * @throws YumPMSDataSaveException
	 */
	
	/*public String updateIdpSelectedSuggestedGoal(Integer idpSheetId, Integer selectedGoalId) throws YumPMSDataSaveException {
		String msg = StringUtils.EMPTY;

		IDP_SheetMaster passingSelectedSuggestedGoal = YumHibernateUtilServices.getSession(sessionFactory).get(IDP_SheetMaster.class, idpSheetId);
		passingSelectedSuggestedGoal.setIdpSelectedSuggestedGoal(selectedGoalId);
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
		try {
			currentSession.merge(passingSelectedSuggestedGoal);
			YumHibernateUtilServices.commitTransaction(currentSession); 
			msg = YumPmsConstants.UPDATED_SUCESSFULLY;
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to update Suggested Goal", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return msg;
	}*/
	
	/**
	 * @param Integer idp_sheet_id
	 * @return IDP_SheetMaster idpSheet
	 */
	public IDP_SheetMaster getIdpSheetbyId(Integer idpSheetId) {
		
		IDP_SheetMaster idpSheet = 
				(IDP_SheetMaster)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(IDP_SheetMaster.class)
				.add(Restrictions.eq(YumPmsConstants.IDP_SHEET_ID, idpSheetId))
				.uniqueResult(); 
		return idpSheet == null ? new IDP_SheetMaster() : idpSheet;
	}
	
	/**
	 * @param IDP_SheetWorkflowHistory idpWkflwHistory
	 * @return IDP_SheetWorkflowHistory idpWkflwHistory
	 * @throws YumPMSDataSaveException
	 */
	public IDP_SheetWorkflowHistory saveIdpSheetHistory(IDP_SheetWorkflowHistory idpWkflwHistory) throws YumPMSDataSaveException {

		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try{
			idpWkflwHistory = (IDP_SheetWorkflowHistory) currentSession.merge(idpWkflwHistory);
			YumHibernateUtilServices.commitTransaction(currentSession);    
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to save Goal Sheet History", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return idpWkflwHistory;
	}
}
