package com.yum.idp.service;

import java.util.List;

import com.yum.idp.domain.EmployeeIdpSheetStatusHistory;
import com.yum.idp.domain.IdpUIConfig;
import com.yum.idp.domain.LtsRatingDetails;
import com.yum.pms.dao.IDP_SectionMaster;
import com.yum.pms.domain.StatusWorkFlow;
import com.yum.pms.exception.YumPMSDataSaveException;

/**
 * @author jayanta.biswas
 * @comment This is a Interface All method of business logic of IDP Section, is Declared There
 * @version 1.0
 * @since   21-12-2017
 */
public interface IIDPService {
	
	public IdpUIConfig getIdpUi(Integer empId, Integer appraisalId, Boolean isReportee);
	
	public IdpUIConfig getIdpUiSuggestedGoalSection(Integer empId, Integer appraisalId, Boolean isReportee);
	
	public IdpUIConfig saveIdpForm(IdpUIConfig idpForm) throws YumPMSDataSaveException;
	
	public List<IDP_SectionMaster> getSectionList();
	
	public List<EmployeeIdpSheetStatusHistory> getWorkFlowHistoryStatus(Integer idpSheetId); 
	
	public StatusWorkFlow getWorkStatusByIdpSheetId(Integer idpSheetId, Boolean isRatingStatusId);
	
	public StatusWorkFlow updateIdpSheetStatus(Integer idpSheetId,Integer statusId, String createdBy, Integer ratingStatusId)
			throws YumPMSDataSaveException;

	public List<LtsRatingDetails> getLtsRatingDetails(Integer empId, Integer appraisalId);
	
	public LtsRatingDetails saveLTsRatingDetails(LtsRatingDetails saveObject) throws YumPMSDataSaveException;
	
	public List<LtsRatingDetails> saveBulkLTsRatingDetails( List<LtsRatingDetails> saveObjectList) throws YumPMSDataSaveException;
	
	public String updateRatingThroughMailByLt(Integer empId, Integer appraisalId, String rating) throws YumPMSDataSaveException;

	public IdpUIConfig getPrevOpportunity(Integer empId, Integer appraisalId);

	/*public IdpUIConfig saveIdpSelectedSuggestedGoal(Integer idpSheetId,
			Integer selectedGoalId);*/
}
