package com.yum.idp.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yum.idp.domain.BalanceOfGoal;
import com.yum.idp.domain.DevelopmentActionPlan;
import com.yum.idp.domain.EmployeeIdpSheetStatusHistory;
import com.yum.idp.domain.HowLead;
import com.yum.idp.domain.HowLeadSubData;
import com.yum.idp.domain.IdpSuggestedDevelopmentActionPlan;
import com.yum.idp.domain.IdpSuggestedGoal;
import com.yum.idp.domain.IdpUIConfig;
import com.yum.idp.domain.LtsRatingDetails;
import com.yum.idp.domain.RemarksDetails;
import com.yum.idp.domain.UiControll;
import com.yum.idp.domain.UiSubControll;
import com.yum.pms.dao.AppraisalPeriod;
import com.yum.pms.dao.EmployeeNew;
import com.yum.pms.dao.GoalSheet;
import com.yum.pms.dao.IDP_BalanceYearGoal;
import com.yum.pms.dao.IDP_DevelopmentActionPlan;
import com.yum.pms.dao.IDP_PerformanceCategoryMaster;
import com.yum.pms.dao.IDP_PerformanceRating;
import com.yum.pms.dao.IDP_PerformanceRatingStatus;
import com.yum.pms.dao.IDP_PerformanceUIConfig;
import com.yum.pms.dao.IDP_PlanDetails;
import com.yum.pms.dao.IDP_PlanMaster;
import com.yum.pms.dao.IDP_PlanUIConfig;
import com.yum.pms.dao.IDP_Remarks;
import com.yum.pms.dao.IDP_SectionDtl;
import com.yum.pms.dao.IDP_SectionMaster;
import com.yum.pms.dao.IDP_SelectedSuggestedGoalDtl;
import com.yum.pms.dao.IDP_SheetMaster;
import com.yum.pms.dao.IDP_SheetStatus;
import com.yum.pms.dao.IDP_SheetStatusWorkFlow;
import com.yum.pms.dao.IDP_SheetWorkflowHistory;
import com.yum.pms.dao.IDP_SubSectionMaster;
import com.yum.pms.dao.IDP_SubSubSectionMaster;
import com.yum.pms.dao.IDP_SubSubSubSectionMaster;
import com.yum.pms.dao.IDP_SuggestedDevelopmentPlanMaster;
import com.yum.pms.dao.IdpPerformanceRatingWorkflow;
import com.yum.pms.dao.Idp_PerformanceRatingReporting;
import com.yum.pms.domain.EmailDomain;
import com.yum.pms.domain.EmployeeCurrentDetails;
import com.yum.pms.domain.StatusWorkFlow;
import com.yum.pms.exception.CommonException;
import com.yum.pms.exception.YumPMSDataSaveException;
import com.yum.pms.service.AppraisalService;
import com.yum.pms.service.EmployeeServices;
import com.yum.pms.service.GoalServices;
import com.yum.pms.service.YumHibernateServiceImpl;
import com.yum.pms.utils.ResourceUtils;
import com.yum.pms.utils.SendMail;
import com.yum.pms.utils.YumPmsConstants;
import com.yum.pms.utils.YumPmsUtils;

/**
 * This Class have all methods, 
 * which invoke from IDPController/IDP section.
 *
 * @author  Jayanta Biswas
 * @version 1.0
 * @since   21-12-2017
 */
@Service
public class IDPServiceImpl implements IIDPService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IDPServiceImpl.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private IdpUiService uiService;
	
	@Autowired
	private IdpFormService idpFormService;
	
	@Autowired
	private IDPUtilService idpUtilService;
	
	@Autowired
	private EmployeeServices employeeService;
	
	@Autowired
	private GoalServices goalServices;
	
	@Autowired
	private WorkFlowHistoryService workFlowService;
	
	@Autowired
	private AppraisalService appraisalService;
	
	@Autowired
	private YumHibernateServiceImpl service;
	
	@Autowired
	private IDPUtilService idpUtil;

	@Autowired
	private SendMail mailSender;
	
	/* (Return all data of IDP from )
	 * @see com.yum.idp.service.IIDPService#getIdpUi(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public IdpUIConfig getIdpUi(Integer empId, Integer appraisalId, Boolean isReportee){
		IDP_SheetMaster idpSheetMaster = idpFormService.getIdpSheetMaster(appraisalId, empId);
		EmployeeCurrentDetails employee = employeeService.getEmployeeDetailsByUserId(empId.toString(), appraisalId, YumPmsConstants.EMPLOYEE_ID); 
		GoalSheet goalSheet = 
				goalServices.getGoalSheetbyEmpId(new EmployeeNew(employee.getEmpId()), new AppraisalPeriod(appraisalId), sessionFactory);

		int goalSheetStatusId = null != goalSheet && null != goalSheet.getGoalStatus() ? goalSheet.getGoalStatus().getGoalStatusId().intValue() : 0;
		IdpUIConfig idpUi = new IdpUIConfig();
		
		idpSheetMaster = checkCreateIdpSheetIfNotExist(empId, appraisalId,
				idpSheetMaster, employee, goalSheetStatusId, idpUi);
		
		idpUi.setIdpSheetId(null != idpSheetMaster ? idpSheetMaster.getIdpSheetId() : 0);
		String idpSheetStatus = (null != idpSheetMaster && null != idpSheetMaster.getIdpStatus()) 
				? idpSheetMaster.getIdpStatus().getIdpSheetStatusName(): StringUtils.EMPTY;
		idpUi.setIdpSheetStatus(idpSheetStatus); 
		idpUi.setEmpId(empId);
		// added null checking
		Integer ratingStatusId = (null != idpSheetMaster && null != idpSheetMaster.getIdpPerformanceRatingLTSWorkflowStatusId()) ? 
				idpSheetMaster.getIdpPerformanceRatingLTSWorkflowStatusId().getIdpPerformanceRatingLTSWorkflowStatusId() : 0;
		idpUi.setRatingStatusId(ratingStatusId);
		idpUi.setEmpName(employee.getEmpName());
		idpUi.setEmpGrade(employee.getGrade());
		idpUi.setEmpDesignation(employee.getDesignation()); 
		idpUi.setAppraisalId(appraisalId);
		idpUi.setAppraisalDescription(appraisalService.getAppraisalPeriodbyId(appraisalId).getPeriodDesc());
		idpUi.setSupervisorId(employee.getManagerId());
		idpUi.setSupervisorName(employee.getManagerName());
		idpUi.setLtId(employee.getFunctionalLtsId());
		idpUi.setFunctionLTName(employee.getFunctionalLtsName());
		idpUi.setFunctionName(employee.getFunctionName());
		idpUi.setGoalSheetIsCompleted(goalSheetStatusId == 1005);
		int employeeGrade = NumberUtils.toInt(employee.getGrade());
		idpUi.setPlanList(getPlanList(appraisalId, idpSheetMaster));
		List<UiControll> performanceList = getPerformanceList(idpSheetMaster);
		checkEmpGrade(employeeGrade, performanceList);
		idpUi.setPerformenceList(performanceList);
		idpUi.setRollBackEnable(performanceList.isEmpty() ? true : !BooleanUtils.toBoolean(performanceList.get(0).getActionPending())); 
		idpUi.setPerformanceRemarks(performanceList.get(0).getRemarks());
		idpUi.setDevelopmentActionPlanList(getDevelopmentActionPlan(appraisalId, idpSheetMaster)); 
		
		idpUi.setBalanceGoal(getBalanceYrGoals(appraisalId, idpSheetMaster)); 
		idpUi.setHowLead(getSectionDetailList(appraisalId, idpSheetMaster));
		
		
		//Getting selected IDP suggested goal id/description
		
		if(idpUi.getIdpSheetId()>0 && !idpUi.getHowLead().getEffectiveness().isEmpty() && idpSheetMaster!=null){
			List<IdpSuggestedGoal> suggestedGoalLists = getSuggestiveGoalList(appraisalId, idpUi.getIdpSheetId());
			idpUi.setIdpSuggestedGoals(suggestedGoalLists);
			
		} 
		if(YumPmsUtils.isGreaterThanZero(idpUi.getIdpSheetId())) {
			idpUi.setIdpSelectedSuggestedGoalList(
				idpSheetMaster.getIdpSelectedSuggestedGoalList().stream()
				.filter(suggestedGoal -> suggestedGoal.getIsActive()).collect(Collectors.toList()));
		}
		
		List<IDP_SuggestedDevelopmentPlanMaster> idpSuggestedDevPlan = service.getSuggestedDevelopmentPlanList();
		if(idpSuggestedDevPlan != null) {
			if(null != idpUi.getIdpSelectedSuggestedGoalList()){
				Set<Integer> selectedSuggestedGoalId = idpUi.getIdpSelectedSuggestedGoalList().stream()
						.map(IDP_SelectedSuggestedGoalDtl::getIdpSelectedSuggestedGoalId)
						.collect(Collectors.toSet());
				List<IdpSuggestedDevelopmentActionPlan> suggestedGoalList = idpSuggestedDevPlan.stream()
						.filter(devActionPlan -> devActionPlan.getSuggestedGoalMaster() != null && selectedSuggestedGoalId.contains(devActionPlan.getSuggestedGoalMaster().getIdpSuggestedGoalSectionId()))
						.map(IdpSuggestedDevelopmentActionPlan :: new)
						.collect(Collectors.toList());
				idpUi.setSuggestedDevActionPlanMstr(suggestedGoalList);
			} else {
				List<IdpSuggestedDevelopmentActionPlan> suggestedGoalList = idpSuggestedDevPlan.stream().map(IdpSuggestedDevelopmentActionPlan :: new).collect(Collectors.toList());
				idpUi.setSuggestedDevActionPlanMstr(suggestedGoalList);
			}
		}
		
		idpUi.setRemarksDetails(getAllRemarks(idpSheetMaster)); 
		return idpUi;
	}

	private IDP_SheetMaster checkCreateIdpSheetIfNotExist(Integer empId,
			Integer appraisalId, IDP_SheetMaster idpSheetMaster,
			EmployeeCurrentDetails employee, int goalSheetStatusId,
			IdpUIConfig idpUi) {
		if(goalSheetStatusId == 1005) {
			if(idpSheetMaster == null) {
				idpSheetMaster = insertIdpSheet(empId, appraisalId, employee.getEmpName());
			}
			idpUi.setIdpSheetId(idpSheetMaster.getIdpSheetId());
		}
		return idpSheetMaster;
	}

	private void checkEmpGrade(int employeeGrade,
			List<UiControll> performanceList) {
		if(employeeGrade >= 12) {
			performanceList.remove(2); 
		} else if(employeeGrade < 12) {
			performanceList.remove(1); 
			performanceList.remove(1);
		}
	}

	/* (Return all data of IDP suggested goal section )
	 * @see com.yum.idp.service.IIDPService#getIdpUiSuggestedGoalSection(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public IdpUIConfig getIdpUiSuggestedGoalSection(Integer empId, Integer appraisalId, Boolean isReportee) {

		IDP_SheetMaster idpSheetMaster = idpFormService.getIdpSheetMaster(appraisalId, empId);

		IdpUIConfig idpUi = new IdpUIConfig();
		EmployeeCurrentDetails employee = employeeService.getEmployeeDetailsByUserId(empId.toString(), appraisalId, YumPmsConstants.EMPLOYEE_ID);
		GoalSheet goalSheet = 
				goalServices.getGoalSheetbyEmpId(new EmployeeNew(employee.getEmpId()), new AppraisalPeriod(appraisalId), sessionFactory);
		int goalSheetStatusId = null != goalSheet && null != goalSheet.getGoalStatus() ? goalSheet.getGoalStatus().getGoalStatusId().intValue() : 0;
		
		idpUi.setIdpSheetId(null != idpSheetMaster ? idpSheetMaster.getIdpSheetId() : 0);
		idpUi.setGoalSheetIsCompleted(goalSheetStatusId == 1005);
		String idpSheetStatus = (null != idpSheetMaster && null != idpSheetMaster.getIdpStatus()) 
				? idpSheetMaster.getIdpStatus().getIdpSheetStatusName(): StringUtils.EMPTY;
		idpUi.setIdpSheetStatus(idpSheetStatus); 
		if(idpSheetMaster!=null){
			List<IdpSuggestedGoal> suggestedGoalLists = getSuggestiveGoalList(appraisalId, idpSheetMaster.getIdpSheetId());
			idpUi.setIdpSuggestedGoals(suggestedGoalLists);
			idpUi.setIdpSelectedSuggestedGoalList(idpSheetMaster.getIdpSelectedSuggestedGoalList().stream().filter(suggestedGoal -> suggestedGoal.getIsActive()).collect(Collectors.toList()));
		}
		
		return idpUi;
	}

	
	/* (Save or update record of all part of IDP form and return the same object which is inserted or updated)
	 * @see com.yum.idp.service.IIDPService#saveIdpForm(com.yum.idp.domain.IdpUIConfig)
	 */
	@Override
	public IdpUIConfig saveIdpForm(IdpUIConfig idpForm) throws YumPMSDataSaveException {
		
		int empId = idpForm.getEmpId();
		IDP_SheetMaster idpSheetMaster = idpFormService.getIdpSheetMaster(idpForm.getAppraisalId(), empId);

		// Save My Plan====================
		List<UiControll> myPlanListResponse = new ArrayList<>();
		myPlanListResponse = flagPlanList(idpForm, empId, idpSheetMaster,
				myPlanListResponse);
		// save Performance Checking============	
		List<UiControll> performenceListResponse = new ArrayList<>();
		performenceListResponse = flagPerformanceList(idpForm, empId,
				idpSheetMaster, performenceListResponse);
		// save DevelopmentActionPlan Details============	
		List<DevelopmentActionPlan> developmentActonPlanResponse = new ArrayList<>();
		developmentActonPlanResponse = flagDevActionPlanList(idpForm, empId,
				idpSheetMaster, developmentActonPlanResponse);
		// save Balance Goals============	
		List<BalanceOfGoal> balanceGoalResponse = new ArrayList<>();
		balanceGoalResponse = flagBalanceGoal(idpForm, empId, idpSheetMaster,
				balanceGoalResponse);		
		// save How I Lead Details============	
		HowLead howLeadResponse = new HowLead();
		if(null != idpForm.getFlag() && idpForm.getFlag().equalsIgnoreCase("howLead") ) {
			howLeadResponse = saveHowLead(idpForm.getHowLead(), idpSheetMaster);
			
			IdpUIConfig getIdpUiData = getIdpUi(empId,idpSheetMaster.getAppraisalPeriod().getAppraisalPeriodId(), false); 
			IdpUIConfig idpFormReturn = new IdpUIConfig();
			
			idpFormReturn.setIdpSuggestedGoals(getIdpUiData.getIdpSuggestedGoals());
			idpFormReturn.setIdpSelectedSuggestedGoalList(getIdpUiData.getIdpSelectedSuggestedGoalList());
			idpFormReturn.setSuggestedDevActionPlanMstr(getIdpUiData.getSuggestedDevActionPlanMstr());
			idpFormReturn.setDevelopmentActionPlanList(getIdpUiData.getDevelopmentActionPlanList()); 
			idpFormReturn.setIdpSheetId(idpSheetMaster.getIdpSheetId());
			idpFormReturn.setHowLead(howLeadResponse);
			
			return idpFormReturn;
		}
		// save Only Remarks Details============		
		RemarksDetails allRemarksResponse = new RemarksDetails();
		if(null != idpForm.getFlag() && idpForm.getFlag().equalsIgnoreCase("remarks") && null != idpForm.getRemarksDetails()) {
			allRemarksResponse = saveAllRemarks(idpForm.getRemarksDetails(), idpSheetMaster);
		}
		
		// save Selected Suggested Goal List Details============	
		List<IDP_SelectedSuggestedGoalDtl> selectedSuggestedGoalListResponse = new ArrayList<IDP_SelectedSuggestedGoalDtl>();
		IdpUIConfig idpFormReturn = new IdpUIConfig();
		if(null != idpForm.getFlag() && idpForm.getFlag().equalsIgnoreCase("idpSelectedSuggestedGoalList") ) {
			selectedSuggestedGoalListResponse = saveSelectedSuggestedGoalList(idpForm.getIdpSelectedSuggestedGoalList(), idpSheetMaster);
			idpFormReturn.setIdpSuggestedGoals(getIdpUiSuggestedGoalSection(empId, idpSheetMaster.getAppraisalPeriod().getAppraisalPeriodId(),true).getIdpSuggestedGoals());
			idpFormReturn.setIdpSelectedSuggestedGoalList(selectedSuggestedGoalListResponse.stream().filter(suggestedGoal -> suggestedGoal.getIsActive()).collect(Collectors.toList()));
			
			List<IDP_SuggestedDevelopmentPlanMaster> idpSuggestedDevPlan = service.getSuggestedDevelopmentPlanList();
			if(idpSuggestedDevPlan != null) {
				Set<Integer> selectedSuggestedGoalId = idpFormReturn.getIdpSelectedSuggestedGoalList().stream()
						.map(IDP_SelectedSuggestedGoalDtl::getIdpSelectedSuggestedGoalId)
						.collect(Collectors.toSet());
				List<IdpSuggestedDevelopmentActionPlan> suggestedGoalList = idpSuggestedDevPlan.stream()
						.filter(devActionPlan -> devActionPlan.getSuggestedGoalMaster() != null && selectedSuggestedGoalId.contains(devActionPlan.getSuggestedGoalMaster().getIdpSuggestedGoalSectionId()))
						.map(IdpSuggestedDevelopmentActionPlan :: new)
						.collect(Collectors.toList());
				idpFormReturn.setSuggestedDevActionPlanMstr(suggestedGoalList);
			}
		}
		
		idpFormReturn.setIdpSheetId(idpSheetMaster.getIdpSheetId());
		idpFormReturn.setPlanList(myPlanListResponse); 
		idpFormReturn.setPerformenceList(performenceListResponse);
		idpFormReturn.setRollBackEnable(performenceListResponse.isEmpty() ? true : 
			!BooleanUtils.toBoolean(performenceListResponse.get(0).getActionPending()));
		idpFormReturn.setDevelopmentActionPlanList(developmentActonPlanResponse); 
		idpFormReturn.setBalanceGoal(balanceGoalResponse); 
		idpFormReturn.setHowLead(howLeadResponse); 
		if(!idpFormReturn.getHowLead().getEffectiveness().isEmpty()){
			idpFormReturn.setIdpSuggestedGoals(getIdpUiSuggestedGoalSection(empId, idpSheetMaster.getAppraisalPeriod().getAppraisalPeriodId(),true).getIdpSuggestedGoals());
			idpFormReturn.setIdpSelectedSuggestedGoalList(selectedSuggestedGoalListResponse.stream().filter(suggestedGoal -> suggestedGoal.getIsActive()).collect(Collectors.toList()));
		}
		idpFormReturn.setRemarksDetails(allRemarksResponse);
		return idpFormReturn;
	}

	private List<BalanceOfGoal> flagBalanceGoal(IdpUIConfig idpForm, int empId,
			IDP_SheetMaster idpSheetMaster,
			List<BalanceOfGoal> balanceGoalResponse)
			throws YumPMSDataSaveException {
		if(null != idpForm.getFlag() && idpForm.getFlag().equalsIgnoreCase("balanceGoal") ) {
			balanceGoalResponse = saveBalanceGoal(idpForm.getBalanceGoal(), idpSheetMaster, empId);
		}
		return balanceGoalResponse;
	}

	private List<DevelopmentActionPlan> flagDevActionPlanList(
			IdpUIConfig idpForm, int empId, IDP_SheetMaster idpSheetMaster,
			List<DevelopmentActionPlan> developmentActonPlanResponse)
			throws YumPMSDataSaveException {
		if(null != idpForm.getFlag() && idpForm.getFlag().equalsIgnoreCase("developmentActionPlanList") ) {
			developmentActonPlanResponse = saveDevelopmentActionPlan(idpForm.getDevelopmentActionPlanList(), idpSheetMaster, empId);
		}
		return developmentActonPlanResponse;
	}

	private List<UiControll> flagPerformanceList(IdpUIConfig idpForm,
			int empId, IDP_SheetMaster idpSheetMaster,
			List<UiControll> performenceListResponse)
			throws YumPMSDataSaveException {
		if(null != idpForm.getFlag() && idpForm.getFlag().equalsIgnoreCase("performenceList") ) {
			performenceListResponse = proceedToSavePerformanceRating(idpForm, empId, idpSheetMaster);
		}
		return performenceListResponse;
	}

	private List<UiControll> flagPlanList(IdpUIConfig idpForm, int empId,
			IDP_SheetMaster idpSheetMaster, List<UiControll> myPlanListResponse)
			throws YumPMSDataSaveException {
		if(idpForm.getFlag().equalsIgnoreCase("planList") ) { 
			myPlanListResponse = saveMyPlan(idpForm.getPlanList(), idpSheetMaster, empId);
		}
		return myPlanListResponse;
	}

	private List<UiControll> proceedToSavePerformanceRating(IdpUIConfig idpForm, int empId, IDP_SheetMaster idpSheetMaster) 
			throws YumPMSDataSaveException {
		
		boolean isSupervisor = YumPmsConstants.SUPERVISOR.equalsIgnoreCase(idpForm.getSubmitedTo());
		if(isSupervisor && !BooleanUtils.toBoolean(idpForm.getAligningSup2())) {
			throw new CommonException(YumPmsConstants.ERROR_ALIGNING_SUP_2);
		}
		List<UiControll> performenceListRequest = idpForm.getPerformenceList();
		List<UiControll> performenceListResponse = savePerFormanceRating(performenceListRequest, idpSheetMaster, empId, idpForm.getSubmitedTo(), 
				idpForm.getPerformanceRemarks(), idpForm.getRatingStatusId());
		if(isSupervisor) {
			service.sendMail(YumPmsConstants.SUPERVISOR_OF_SUPERVISOR, idpForm.getIdpSheetId());
			String ratingValue = !performenceListRequest.isEmpty()? performenceListRequest.get(0).getValue() : StringUtils.EMPTY;
			if(ratingValue.equalsIgnoreCase(YumPmsConstants.EMAIL_FOR_GOAL_RATING_E)) {
				service.sendMail(YumPmsConstants.APPROVAL_MAIL_TO_LT,idpForm.getIdpSheetId());
			}
		}
		return performenceListResponse;
	}

	/* (Fetch all category list for How To Lead section)
	 * @see com.yum.idp.service.IIDPService#getSectionList()
	 */
	@Override
	public List<IDP_SectionMaster> getSectionList() {
		return idpUtilService.getSectionList();
	}

	@Override
	public List<EmployeeIdpSheetStatusHistory> getWorkFlowHistoryStatus(Integer idpSheetId) {
		
		List<IDP_SheetWorkflowHistory> idpWorkFlowDetailsList = workFlowService.getWorkFlowHistoryStatus(idpSheetId);
		List<EmployeeIdpSheetStatusHistory> listWorkFlowHistory = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(idpWorkFlowDetailsList)) {
			for (IDP_SheetWorkflowHistory gsWorkFlowDetails : idpWorkFlowDetailsList) {		

				EmployeeIdpSheetStatusHistory lastWorkFlowDetails = new EmployeeIdpSheetStatusHistory();
				lastWorkFlowDetails.setIdpSheetId(idpSheetId);
				IDP_SheetStatus status = workFlowService.getIdpStatus(gsWorkFlowDetails.getIdpStatusId());
				lastWorkFlowDetails.setGoalStatusName(status.getIdpSheetStatusName());
				lastWorkFlowDetails.setUpdatedByEmpName(employeeService.getEmployeeById(gsWorkFlowDetails.getEmpId()).getEmpName());
				lastWorkFlowDetails.setUpdateDate(gsWorkFlowDetails.getCreatedDate());
				listWorkFlowHistory.add(lastWorkFlowDetails);
			}
		}
		return listWorkFlowHistory;
	}


	@Override
	public StatusWorkFlow getWorkStatusByIdpSheetId(Integer idpSheetId, Boolean ratingStatusId) {

		StatusWorkFlow statusWorkFlow = new StatusWorkFlow();
		if(null == ratingStatusId || !ratingStatusId) {
			IDP_SheetStatus currentIdpStatus = workFlowService.getIdpStatusIdByIdpSheetId(idpSheetId);
			List<IDP_SheetStatusWorkFlow> goalSheetStatusWorkFlowList = 
					workFlowService.getWorkStatusByIdpSheetId(currentIdpStatus.getIdpSheetStatusId());

			statusWorkFlow.setCurrentStatusId(currentIdpStatus.getIdpSheetStatusId());
			statusWorkFlow.setCurrentStatusName(currentIdpStatus.getIdpSheetStatusName());
			if(CollectionUtils.isNotEmpty(goalSheetStatusWorkFlowList)) {
				for(IDP_SheetStatusWorkFlow workFlow : goalSheetStatusWorkFlowList) {
					List<String> nextStatusNameAndUrl = new ArrayList<>();
					nextStatusNameAndUrl.add(workFlow.getNextPossibleIdpSheetStatus().getIdpSheetStatusName());
					nextStatusNameAndUrl.add(workFlow.getWebServiceURL());
					statusWorkFlow.getPossibleNextStutsNameAndUrl().put(workFlow.getNextPossibleIdpSheetStatus().getIdpSheetStatusId(), 
							nextStatusNameAndUrl);
				}
			} else {
				statusWorkFlow.setMessage("IDP Sheet Does Not Exist In The Database"); 
			}
		} else {
			IDP_PerformanceRatingStatus currentIdpStatus = workFlowService.getIdpRatingStatusIdByIdpSheetId(idpSheetId);
			List<IdpPerformanceRatingWorkflow> goalSheetStatusWorkFlowList = 
					workFlowService.getRatingStatusByIdpSheetId(currentIdpStatus.getIdpPerformanceRatingLTSWorkflowStatusId());

			statusWorkFlow.setCurrentStatusId(currentIdpStatus.getIdpPerformanceRatingLTSWorkflowStatusId());
			statusWorkFlow.setCurrentStatusName(currentIdpStatus.getIdpPerformanceRatingLTSWorkflowStatusDesc());
			if(CollectionUtils.isNotEmpty(goalSheetStatusWorkFlowList)) {
				for(IdpPerformanceRatingWorkflow workFlow : goalSheetStatusWorkFlowList) {
					List<String> nextStatusNameAndUrl = new ArrayList<>();
					nextStatusNameAndUrl.add(workFlow.getNextPossibleIdpSheetRatingStatus().getIdpPerformanceRatingLTSWorkflowStatusDesc());
					nextStatusNameAndUrl.add(workFlow.getWebServiceURL());
					statusWorkFlow.getPossibleNextStutsNameAndUrl().put(
							workFlow.getNextPossibleIdpSheetRatingStatus().getIdpPerformanceRatingLTSWorkflowStatusId(), nextStatusNameAndUrl);
				}
			} else {
				statusWorkFlow.setMessage("IDP Sheet Does Not Exist In The Database"); 
			}
		}
		return statusWorkFlow;
	}


	@Override
	public StatusWorkFlow updateIdpSheetStatus(Integer idpSheetId, Integer statusId, String createdBy, Integer ratingStatusId) 
			throws YumPMSDataSaveException {

		String msg = workFlowService.updateIdpSheetStatus(idpSheetId, statusId, createdBy, ratingStatusId);
		StatusWorkFlow nextStatus = getWorkStatusByIdpSheetId(idpSheetId, false);

		// Mail part developed at next time----------------------	
		if(nextStatus != null && !nextStatus.getCurrentStatusId().equals(1002)) {
			try {
				int actionStatusId = nextStatus.getCurrentStatusId() == null ? 0 : nextStatus.getCurrentStatusId();
				String param2 = null;
				switch(actionStatusId) {
				case 1003:
					param2 = YumPmsConstants.WORK_FLOW_STATUS_DESC_SUBMITTED;
					break;
				case 1004:
					param2 = YumPmsConstants.WORK_FLOW_STATUS_DESC_ROLLEDBACK;
					break;
				case 1005:
					param2 = YumPmsConstants.WORK_FLOW_STATUS_DESC_APPROVED;
					idpUtil.executeIdpVersionHistoryProcedure(idpSheetId);
					break;
				case 1006:
					param2 = YumPmsConstants.WORK_FLOW_STATUS_DESC_RECALLED;
					break;
				default:
					param2 = StringUtils.EMPTY;
					break;
				}
				if(YumPmsConstants.UPDATED_SUCESSFULLY.equalsIgnoreCase(msg)) {	
					sendMail(idpSheetId, actionStatusId, param2);
				}		
			} catch(Exception e) {
				LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			} 
		}
		//update goal sheet status history
		IDP_SheetWorkflowHistory idpWkflwHistory = new IDP_SheetWorkflowHistory();
		idpWkflwHistory.setIdpSheetId(idpSheetId);

		IDP_SheetMaster goalSheet = workFlowService.getIdpSheetbyId(idpSheetId);
		idpWkflwHistory.setAppraisalPeriodId(goalSheet.getAppraisalPeriod().getAppraisalPeriodId());
		idpWkflwHistory.setIdpStatusId(statusId);
		idpWkflwHistory.setEmpId(goalSheet.getEmployee().getEmpId());
		idpWkflwHistory.setCreatedDate(new Date());
		idpWkflwHistory.setCreatedBy(goalSheet.getEmployee().getEmpName());
		workFlowService.saveIdpSheetHistory(idpWkflwHistory);
		return null == nextStatus ? new StatusWorkFlow() : nextStatus;
	}

	/* (non-Javadoc)
	 * @see com.yum.idp.service.IIDPService#getLtsRatingDetails(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public List<LtsRatingDetails> getLtsRatingDetails(Integer empId, Integer appraisalId) {
		
		List<Integer> empIdList = employeeService.getNonDrEmpIdListByLtsId(empId, appraisalId);
		if(CollectionUtils.isEmpty(empIdList)) {
			return Collections.emptyList();
		}
		List<LtsRatingDetails> ltsRatingDetailsList = new ArrayList<>(empIdList.size());
		for(Integer employeeId : empIdList) {	
			
			IDP_SheetMaster idpSheetMaster = idpFormService.getIdpSheetMaster(appraisalId, employeeId);
			int ratingStatusId = 0;
			int idpSheetId = 0;
			Idp_PerformanceRatingReporting ratingReporting = null;
			if(null != idpSheetMaster) {
				idpSheetId = idpSheetMaster.getIdpSheetId();
				if(idpSheetMaster.getIdpPerformanceRatingLTSWorkflowStatusId() != null) {
					ratingStatusId = idpSheetMaster.getIdpPerformanceRatingLTSWorkflowStatusId().getIdpPerformanceRatingLTSWorkflowStatusId();
				}
				ratingReporting = idpFormService.getRatingRepotDetails(idpSheetMaster);
			}
			EmployeeCurrentDetails employee = 
					employeeService.getEmployeeDetailsByUserId(employeeId.toString(), appraisalId, YumPmsConstants.EMPLOYEE_ID);
			
			LtsRatingDetails ltsRatingDeatils = new LtsRatingDetails();
			ltsRatingDeatils.setIdpSheetId(idpSheetId);
			ltsRatingDeatils.setRatingStatusId(ratingStatusId);
			ltsRatingDeatils.setAppraisalId(appraisalId); 
			ltsRatingDeatils.setReporteeId(employee.getEmpId());
			ltsRatingDeatils.setReporteeName(employee.getEmpName());
			ltsRatingDeatils.setUserId(empId);
			ltsRatingDeatils.setUsername(empId != 0 ? employee.getFunctionalLtsName() : null);
			ltsRatingDeatils.setDesignation(employee.getDesignation());
			ltsRatingDeatils.setGrade(employee.getGrade());
			ltsRatingDeatils.setSupervisorId(employee.getManagerId());
			ltsRatingDeatils.setFunctionLtsId(employee.getFunctionalLtsId());
			ltsRatingDeatils.setFunctionLtsName(employee.getFunctionalLtsName());
			ltsRatingDeatils.setSupervisorName(employee.getManagerName());
			if(null != ratingReporting) {
				ltsRatingDeatils.setRemarksSup(ratingReporting.getRemarksSup());
				ltsRatingDeatils.setRemarksLts(ratingReporting.getRemarksLT());
				ltsRatingDeatils.setRemarksHr(ratingReporting.getRemarksHR());
				ltsRatingDeatils.setCultureRatingSup(ratingReporting.getRatingCulturebySup());
				ltsRatingDeatils.setCutureRatingLts(ratingReporting.getRatingCultureByLT());
				ltsRatingDeatils.setCultureRatingHr(ratingReporting.getRatingCulturebyHR());
				ltsRatingDeatils.setLtsRatingSup(ratingReporting.getRatingLTSbySup());
				ltsRatingDeatils.setLtsRatingLts(ratingReporting.getRatingLTSbyLT());
				ltsRatingDeatils.setLtsRatingHr(ratingReporting.getRatingLTSbyHR());
			}
			ltsRatingDeatils.setPerformanceList(null != idpSheetMaster ? getPerformanceRatingUiList(idpSheetMaster) : null); 

			ltsRatingDetailsList.add(ltsRatingDeatils);
			LOGGER.info("getLtsRatingDetails>>>>>>..Date29-05-2018.. {}", ltsRatingDeatils);
		}
		return ltsRatingDetailsList;
	}


	/* (non-Javadoc)
	 * @see com.yum.idp.service.IIDPService#saveLTsRatingDetails(com.yum.idp.domain.LtsRatingDetails)
	 */
	@Override
	public LtsRatingDetails saveLTsRatingDetails(LtsRatingDetails ltsRatingDetails) throws YumPMSDataSaveException {

		String lastRemarks = null != ltsRatingDetails.getRemarksHr() ? ltsRatingDetails.getRemarksHr() : ltsRatingDetails.getRemarksLts();
		IDP_SheetMaster idpSheetMst = idpFormService.getIdpSheetMasterById(ltsRatingDetails.getIdpSheetId());	
		List<UiControll> pergormanceRatingList = savePerFormanceRating(ltsRatingDetails.getPerformanceList(), idpSheetMst, 
				ltsRatingDetails.getReporteeId(), ltsRatingDetails.getSubmittedTo(), lastRemarks , ltsRatingDetails.getRatingStatusId());
		ltsRatingDetails.setPerformanceList(pergormanceRatingList); 

		IDP_PerformanceRatingStatus ratingStatus = new IDP_PerformanceRatingStatus();
		ratingStatus.setIdpPerformanceRatingLTSWorkflowStatusId(ltsRatingDetails.getRatingStatusId());	
		idpSheetMst.setIdpPerformanceRatingLTSWorkflowStatusId(ratingStatus);		
		idpFormService.insertOrUpdateIdpSheetDetails(idpSheetMst);

		return ltsRatingDetails;
	}

	/* (non-Javadoc)
	 * @see com.yum.idp.service.IIDPService#saveBulkLTsRatingDetails(java.util.List)
	 */
	@Override
	public List<LtsRatingDetails> saveBulkLTsRatingDetails(List<LtsRatingDetails> saveObjectList) throws YumPMSDataSaveException {
		
		List<LtsRatingDetails> ratingDetailsList = new ArrayList<>();
		List<Integer> empIds = new ArrayList<>();
		String submittedTo = StringUtils.EMPTY;
		int ltsId = 0;
		int appraisalId = 0;
		for(LtsRatingDetails ratingDetails : saveObjectList) {
			ltsId = ratingDetails.getUserId();
			appraisalId = ratingDetails.getAppraisalId();	
			submittedTo = ratingDetails.getSubmittedTo();
			ratingDetails = saveLTsRatingDetails(ratingDetails);
			empIds.add(ratingDetails.getReporteeId());
			ratingDetailsList.add(ratingDetails); 
		}
		// send mail to Admin/HR--------- 
		List<EmailDomain> emailDomainListForAdmin = 
				idpFormService.getMailDetailsFromSP(ltsId, YumPmsConstants.ADMIN, StringUtils.EMPTY, appraisalId);
		EmailDomain adminEmailDomain = CollectionUtils.isNotEmpty(emailDomainListForAdmin)?emailDomainListForAdmin.get(0):null;
		try {
			checkNullAdminEmailDomain(adminEmailDomain);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
		}

		// send mail to Supervisor or LT----------
		String sendingTo = submittedTo.equalsIgnoreCase(YumPmsConstants.LT) 
				|| submittedTo.equalsIgnoreCase(YumPmsConstants.SUPERVISOR_LT) ? YumPmsConstants.SUPERVISOR : YumPmsConstants.LT;
		String variableText = sendingTo.equalsIgnoreCase(YumPmsConstants.SUPERVISOR)? " by the LT." : " and submitted by the Admin.";
		String empListStr = StringUtils.join(empIds, YumPmsConstants.COMMA);
		StringBuilder body = new StringBuilder("Hi,  </BR></BR>" + "The PPR Rating for the below employees are modified")
				.append(variableText).append(" </BR></BR>  Emp Name </BR></BR> -------- </BR></BR>");
		List<EmailDomain> emailDomainsForSupervisor = idpFormService.getMailDetailsFromSP(ltsId, sendingTo, empListStr, appraisalId);
			if(CollectionUtils.isNotEmpty(emailDomainsForSupervisor)) {
				for(EmailDomain supervisorEmailDomain : emailDomainsForSupervisor) {
					List<String> empIdList = Arrays.asList(supervisorEmailDomain.getBody().trim().split(YumPmsConstants.COMMA)); 
					for(String empId : empIdList) {
						Map<String, String> empMap = idpFormService.empidToEmployeeDetails(Integer.valueOf(empId), appraisalId); 
						body.append(empMap.get("empName") + "</BR>");
						LOGGER.info("Body = {}", body); 
					}
					body.append("</BR> You can check the individual modified rating from \"Reportees IDP\" page in the application. </BR>")
						.append("Application URL: https://elakshay.kfc.co.in/yumpmsweb/login </BR></BR> Warm Regards,</BR> RSC HR");
					try {
						mailSender.sendMail(supervisorEmailDomain.getToMailId(), supervisorEmailDomain.getCcMailId(), supervisorEmailDomain.getSubject(), 
								body.toString(), null);
					} catch (Exception e) {
						LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
					}
					body.setLength(0);
					body.append("Hi,  </BR></BR>" + "The PPR Rating for the below employees are modified by the LT. ")
						.append("</BR></BR>  Emp Name </BR></BR> -------- </BR></BR>");
				}
			}
		return ratingDetailsList;
	}

	private void checkNullAdminEmailDomain(EmailDomain adminEmailDomain) {
		if(null != adminEmailDomain) {
			mailSender.sendMail(adminEmailDomain.getToMailId(), adminEmailDomain.getCcMailId(), adminEmailDomain.getSubject(), 
					adminEmailDomain.getBody(), null);
		}
	}

	/* (non-Java doc)
	 * updateRatingThroughMailByLt: update goal rating By LT through mail
	 * @see com.yum.idp.service.IIDPService#updateRatingThroughMailByLt(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public String updateRatingThroughMailByLt(Integer empId, Integer appraisalId, String ratingDesc) throws YumPMSDataSaveException {
		
		String msg = "Successfully Approved";
		if(null != empId && null != appraisalId &&  null != ratingDesc) {
			IDP_SheetMaster idpSheetMaster = idpFormService.getIdpSheetMaster(appraisalId, empId);
			IDP_PerformanceRating rating = idpFormService.getIdpPerformanceRatingActionPendingBasis(idpSheetMaster);
			boolean operationFlag = null != rating ? BooleanUtils.toBoolean(rating.getActionPending()) : false;
			if(rating != null && null != rating.getIdpPerformanceRatingId() && BooleanUtils.toBoolean(rating.getActionPending())) {
				rating.setActionPending(false);
				rating.setRatingDesc(ratingDesc);
				rating.setIdpPerformanceCategoryMaster(idpUtil.getCatagoryMasterByDescription(ratingDesc));
				idpFormService.insertOrUpdateIdpPerformanceRating(rating);

				Idp_PerformanceRatingReporting ratingReporting = idpFormService.getRatingRepotDetails(idpSheetMaster);
				ratingReporting.setRatingGoalByLt(ratingDesc);  
				idpFormService.insertOrUpdateIdpPerformanceRatingReporting(ratingReporting);
			}	
			if(!operationFlag) {
				msg = "You have already actioned on the rating. No further changes can be made. Please reach out to RSC HR for issues";
			} else if(!ratingDesc.equalsIgnoreCase(YumPmsConstants.EMAIL_FOR_GOAL_RATING_E)) {
				service.sendMail(YumPmsConstants.REJECTION_MAIL_TO_SUPERVISOR, idpSheetMaster.getIdpSheetId());
				msg = "Rating for "+idpSheetMaster.getEmployee().getEmpName()+" has been changed from Exceptional to " + ratingDesc;
			} else {
				msg = "Rating is approved";
			}
		}
		return msg;
	}


	// =========================================Supporting Methods===============================================================
	/**
	 * @param List<UiControll> myPlanListRequest 
	 * @param IDP_SheetMaster idpSheetMaster 
	 * @param int empId
	 * @return List<UiControll> myPlanListResponse
	 * @throws YumPMSDataSaveException
	 */
	public List<UiControll> saveMyPlan(List<UiControll> myPlanListRequest, IDP_SheetMaster idpSheetMaster, int empId) throws YumPMSDataSaveException {
		List<UiControll> myPlanListResponse = new ArrayList<>();
		for(UiControll uiControll : myPlanListRequest) {
			IDP_PlanDetails planDetails = new IDP_PlanDetails();
			IDP_PlanMaster palnMaster = new IDP_PlanMaster();
			palnMaster.setIdpPlanId(uiControll.getId());

			planDetails.setIdpSheetMaster(idpSheetMaster);
			planDetails.setIdpPlanMaster(palnMaster);
			planDetails.setIdp_plan_dtl_desc(uiControll.getValue());
			planDetails.setCreatedBy(String.valueOf(empId)); 
			planDetails.setUpdatedBy(String.valueOf(empId)); 
			if(uiControll.getDataId()!=null) {
				planDetails.setIdp_plan_dtl_id(uiControll.getDataId());
			}
			planDetails = idpFormService.insertOrUpdateIdpPlanDetails(planDetails);
			uiControll.setDataId(planDetails.getIdp_plan_dtl_id());
			myPlanListResponse.add(uiControll);			
		}
		return myPlanListResponse;
	}
	
	/**
	 * @param List<UiControll> performenceListRequest
	 * @param IDP_SheetMaster idpSheetMaster
	 * @param int empId
	 * @return List<UiControll> performenceListResponse
	 * @throws Exception 
	 */
	private List<UiControll> savePerFormanceRating(List<UiControll> performenceListRequest, IDP_SheetMaster idpSheetMaster, 
			int empId, String submitedFrom, String performanceRemarks, Integer ratingStatusId) throws YumPMSDataSaveException {
		
		List<UiControll> performenceListResponse = new ArrayList<>();
		int counter = 1;
		for(UiControll uiControll : performenceListRequest) {
			IDP_PerformanceRating ratingDescription = new IDP_PerformanceRating();
			String rating = uiControll.getValue();
			UiSubControll subControll = getSelectedRating(rating, uiControll);
			ratingDescription.setRatingDesc(subControll.getValue());
			ratingDescription.setActionPending(!StringUtils.equalsIgnoreCase(submitedFrom, YumPmsConstants.SUPERVISOR_LT) ? 
					StringUtils.equalsIgnoreCase(rating, YumPmsConstants.EMAIL_FOR_GOAL_RATING_E) : false);
			ratingDescription.setIdpPerformanceCategoryMaster(new IDP_PerformanceCategoryMaster(subControll.getId()));
			ratingDescription.setIdpSheetMaster(idpSheetMaster);
			ratingDescription.setCreatedBy(String.valueOf(empId));
			ratingDescription.setUpdatedBy(String.valueOf(empId));
			ratingDescription.setRemarksCurr(performanceRemarks);
			if(uiControll.getDataId() != null) {
				ratingDescription.setIdpPerformanceRatingId(uiControll.getDataId());
			}
			if(StringUtils.isNotEmpty(ratingDescription.getRatingDesc())) {
				ratingDescription = idpFormService.insertOrUpdateIdpPerformanceRating(ratingDescription);
				uiControll.setActionPending(ratingDescription.getActionPending());
				if(counter == 1 && !submitedFrom.equalsIgnoreCase(YumPmsConstants.EMPLOYEE)) {
					updatePerformanceRating1(idpSheetMaster, empId, submitedFrom, ratingDescription);
				} else if(counter == 2 && !submitedFrom.equalsIgnoreCase(YumPmsConstants.EMPLOYEE)) {
					updatePerformanceRating2(idpSheetMaster, empId, submitedFrom, ratingDescription);
				} else if(counter == 3 && !submitedFrom.equalsIgnoreCase(YumPmsConstants.EMPLOYEE)) {
					updatePerformanceRating3(idpSheetMaster, empId, submitedFrom, ratingDescription);
				}
				counter++;
			}				
			uiControll.setDesDetailID(ratingDescription.getIdpPerformanceRatingId());
			uiControll.setDataId(ratingDescription.getIdpPerformanceRatingId());
			performenceListResponse.add(uiControll);		
		}
		idpSheetMaster.setIdpPerformanceRatingLTSWorkflowStatusId(new IDP_PerformanceRatingStatus(ratingStatusId)); 
		idpFormService.insertOrUpdateIdpSheetDetails(idpSheetMaster);
		return performenceListResponse;
	}

	/**
	 * @param List<DevelopmentActionPlan> developmentActonPlanRequest
	 * @param IDP_SheetMaster idpSheetMaster
	 * @param int empId
	 * @return List<DevelopmentActionPlan> developmentActonPlanListResponse
	 * @throws YumPMSDataSaveException
	 */
	public List<DevelopmentActionPlan> saveDevelopmentActionPlan(List<DevelopmentActionPlan> developmentActonPlanRequest, 
			IDP_SheetMaster idpSheetMaster, int empId) throws YumPMSDataSaveException {
		
		List<DevelopmentActionPlan> developmentActonPlanListResponse = new ArrayList<>();
		for(DevelopmentActionPlan actionPlan : developmentActonPlanRequest) {
			IDP_DevelopmentActionPlan actionPlanDetails = new IDP_DevelopmentActionPlan();

			actionPlanDetails.setIdpSheetMaster(idpSheetMaster);
			actionPlanDetails.setDevelopmentActionPlanDes(actionPlan.getDevActionSummary());
			actionPlanDetails.setDevelopmentActionTimeLine(actionPlan.getTimeline());
			actionPlanDetails.setIsOngoing(actionPlan.getIsOnGoing());
			actionPlanDetails.setCreatedBy(String.valueOf(empId)); 
			actionPlanDetails.setUpdatedBy(String.valueOf(empId)); 
			actionPlanDetails.setIsActive(actionPlan.getIsDelete()?false:true);
			actionPlanDetails.setIdpSuggestedGoalSectionId(actionPlan.getIdpSuggestedGoalSectionId());
			actionPlanDetails.setIdpSuggestedDevelopmentPlanSectionId(actionPlan.getIdpSuggestedDevelopmentPlanSectionId());
			if(actionPlan.getDevActionId() > 0) {
				actionPlanDetails.setIdpDevelopmentActionPlanDtlId(actionPlan.getDevActionId());
			}		
			actionPlanDetails = idpFormService.insertOrUpdateIdpDevelopmentActionPlan(actionPlanDetails);		
			actionPlan.setDevActionId(actionPlanDetails.getIdpDevelopmentActionPlanDtlId());
			if(!BooleanUtils.toBoolean(actionPlan.getIsDelete())) {
				developmentActonPlanListResponse.add(actionPlan);	
			}
		}
		return developmentActonPlanListResponse;
	}

	/**
	 * @param List<BalanceOfGoal> balanceGoalRequest
	 * @param IDP_SheetMaster idpSheetMaster
	 * @param int empId
	 * @return  List<BalanceOfGoal> balanceGoalListResponse
	 * @throws Exception
	 */
	public List<BalanceOfGoal> saveBalanceGoal(List<BalanceOfGoal> balanceGoalRequest, IDP_SheetMaster idpSheetMaster, int empId) 
			throws YumPMSDataSaveException {
		
		List<BalanceOfGoal> balanceGoalListResponse = new ArrayList<>();
		for(BalanceOfGoal balanceGoal : balanceGoalRequest) {			
			IDP_BalanceYearGoal balanceYearGoals = new IDP_BalanceYearGoal(); 				
			balanceYearGoals.setIdpSheetMaster(idpSheetMaster);
			balanceYearGoals.setGoalSectionId(balanceGoal.getGoalSectionId());
			balanceYearGoals.setIsActive(balanceGoal.getIsChecked()?true:false);
			balanceYearGoals.setBalanceYearGoalsDesc(balanceGoal.getBalanceGoalDesc());
			balanceYearGoals.setGoalSectionTimeline(balanceGoal.getIdpGoalSectionTimeline());
			if(balanceGoal.getBalanceYrGoalId() > 0) {
				balanceYearGoals.setIdpBalanceYearGoalsId(balanceGoal.getBalanceYrGoalId()); 
				balanceYearGoals.setUpdatedBy(String.valueOf(empId)); 
			} else {
				balanceYearGoals.setCreatedBy(String.valueOf(empId)); 
				balanceYearGoals.setUpdatedBy(String.valueOf(empId)); 
			}
			balanceYearGoals = idpFormService.insertOrUpdateIdpBalanceYrGoals(balanceYearGoals);		
			balanceGoal.setBalanceYrGoalId(balanceYearGoals.getIdpBalanceYearGoalsId());
			if(BooleanUtils.toBoolean(balanceGoal.getIsChecked())) {
				balanceGoalListResponse.add(balanceGoal);			
			}
		}
		return balanceGoalListResponse;
	}
	
	/**
	 * @param saveSelectedSuggestedGoal
	 * @return
	 * @throws Exception
	 */
	private List<IDP_SelectedSuggestedGoalDtl> saveSelectedSuggestedGoalList(
			List<IDP_SelectedSuggestedGoalDtl> idpSelectedSuggestedGoalList,
			IDP_SheetMaster idpSheetMaster) {
		
		List<IDP_SelectedSuggestedGoalDtl> selectedSuggestedGoalResponse = new ArrayList<>();
		
		for(IDP_SelectedSuggestedGoalDtl selectedGoal : idpSelectedSuggestedGoalList) {	
			IDP_SelectedSuggestedGoalDtl setValues = new IDP_SelectedSuggestedGoalDtl();
			
			// Checking if its an insert operation then check if already exist then do not insert again
			if(YumPmsUtils.isLessThanZero(selectedGoal.getIdpSelectedSuggestedGoalDtlId())){
				List<IDP_SelectedSuggestedGoalDtl> getSelectedSuggestedGoalCheck = idpSheetMaster.getIdpSelectedSuggestedGoalList().stream()
				.filter(suggestedGoal -> suggestedGoal.getIsActive()).collect(Collectors.toList());
				if(!getSelectedSuggestedGoalCheck.isEmpty()){
					return getSelectedSuggestedGoalCheck;
				}
			}
			
			if(selectedGoal.getIdpSelectedSuggestedGoalDtlId()>0){
				setValues.setIdpSelectedSuggestedGoalDtlId(selectedGoal.getIdpSelectedSuggestedGoalDtlId());
				setValues.setUpdatedBy(idpSheetMaster.getEmployee().getEmpName());
			} else {
				setValues.setCreatedBy(idpSheetMaster.getEmployee().getEmpName());
				setValues.setUpdatedBy(idpSheetMaster.getEmployee().getEmpName());
			}
			
			if(selectedGoal.getIdpSelectedSuggestedGoalId() > 0){
				setValues.setIdpSelectedSuggestedGoalId(selectedGoal.getIdpSelectedSuggestedGoalId());
			} else {
				setValues.setIdpSelectedSuggestedGoalId(null);
			}
			
			setValues.setIdpSelectedSuggestedGoalOthersDesc(selectedGoal.getIdpSelectedSuggestedGoalOthersDesc());
			setValues.setIdpSheetMaster(new IDP_SheetMaster(idpSheetMaster.getIdpSheetId()));
			setValues.setIsActive(selectedGoal.getIsActive());
			setValues.setIsOnGoing(selectedGoal.getIsOnGoing());
			setValues.setTimeline(selectedGoal.getTimeline());
			setValues.setIdpSelectedSuggestedGoalDesc(selectedGoal.getIdpSelectedSuggestedGoalDesc());
			
			selectedSuggestedGoalResponse.add(setValues);
		}
		
		try {
			selectedSuggestedGoalResponse = idpFormService.insertOrUpdateIdpSelectedSuggestedGoals(selectedSuggestedGoalResponse);
		} catch (Exception e) {
			LOGGER.info("Error in Saving Updating data for Selected Suggested Goal",e);
		}
		
		return selectedSuggestedGoalResponse;
	}

	/**
	 * @param HowLead howLead
	 * @param IDP_SheetMaster idpSheetMaster
	 * @param int empId
	 * @return HowLead howLeadResponse
	 * @throws Exception
	 */
	private HowLead saveHowLead(HowLead howLead, IDP_SheetMaster idpSheetMaster) throws YumPMSDataSaveException {

		HowLead howLeadResponse = new HowLead();
		howLeadResponse.setAppreciate(populateAppreciateList(howLead, idpSheetMaster));
		howLeadResponse.setEffectiveness(populateEffectivenessList(howLead, idpSheetMaster));	
		// Growth set goes here
		HowLeadSubData growth = howLead.getGrowth();
		if(null != growth && StringUtils.isNotBlank(growth.getDescription())) {
			
			IDP_SectionDtl sectionDetailDto = new IDP_SectionDtl(); 
			IDP_SectionMaster sectionMaster = new IDP_SectionMaster();
			int idpSectionIdForGrowth = idpFormService.getIDPSectionmaster(YumPmsConstants.GROWTH_IN_UPPER_CASE).getIdpSectionId();
			sectionMaster.setIdpSectionId(null != growth.getCategoryId() ? growth.getCategoryId() : idpSectionIdForGrowth); 

			sectionDetailDto.setIdpSectionMaster(sectionMaster);
			sectionDetailDto.setIdpSheetMaster(idpSheetMaster);
			sectionDetailDto.setIdpSectionDtlDesc(growth.getDescription());
			sectionDetailDto.setIdpSectionTypeFlag(YumPmsConstants.GROWTH_IN_LOWER_CASE);
			sectionDetailDto.setIsActive(BooleanUtils.toBoolean(growth.getIsDelete()) ? false : true);
			sectionDetailDto.setCreatedBy(growth.getCreatedBy());
			sectionDetailDto.setUpdatedBy(growth.getUpdatedBy());
			if(null != growth.getCatagoryDetailId() && growth.getCatagoryDetailId() > 0) {
				sectionDetailDto.setIdpSectionDtlId(growth.getCatagoryDetailId()); 
				sectionDetailDto.setUpdatedBy(String.valueOf(idpSheetMaster.getEmployee().getEmpId()));
			} else {
				sectionDetailDto.setCreatedBy(String.valueOf(idpSheetMaster.getEmployee().getEmpId()));
				sectionDetailDto.setUpdatedBy(String.valueOf(idpSheetMaster.getEmployee().getEmpId()));
			}		
			sectionDetailDto = idpFormService.insertOrUpdateIdpSectionDetails(sectionDetailDto);		
			growth.setCatagoryDetailId(sectionDetailDto.getIdpSectionDtlId());

			if(!BooleanUtils.toBoolean(growth.getIsDelete())) {
				howLeadResponse.setGrowth(growth);
			}
		}
		return howLeadResponse;
	}

	/**
	 * @param allRemarks
	 * @return
	 * @throws Exception
	 */
	public RemarksDetails saveAllRemarks(RemarksDetails allRemarks, IDP_SheetMaster sheetMaster) throws YumPMSDataSaveException {
		
		LOGGER.info("IDP sheet ID = {} ", sheetMaster != null ? sheetMaster.getIdpSheetId() : 0);
		IDP_Remarks remarks = null;
		if(sheetMaster != null && YumPmsUtils.isGreaterThanZero(sheetMaster.getIdpSheetId())) {
			remarks = idpFormService.getAllRemarks(sheetMaster);
		}
		if(remarks == null) {
			remarks = new IDP_Remarks();
		}
		remarks.setIdpSheetMaster(sheetMaster);
		remarks.setHowILeadAppreciateRemarks(allRemarks.getHowILeadAppreciateRemarks());
		remarks.setHowILeadEffectivenessRemarks(allRemarks.getHowILeadEffectivenessRemarks());
		remarks.setHowILeadGrowthRemarks(allRemarks.getHowILeadGrowthRemarks());
		remarks.setMyPlanRemarks(allRemarks.getMyPlanRemarks());
		remarks.setDevelopmentActionPlanRemarks(allRemarks.getDevelopmentActionPlanRemarks());
		remarks.setPerformanceCheckInGoalsRemarks(allRemarks.getPerformanceCheckInGoalsRemarks());
		remarks.setBalanceYearGoalsRemarks(allRemarks.getBalanceYearGoalsRemarks());
		remarks = idpFormService.insertOrUpdateIdpRemarksDetails(remarks);
		allRemarks.setIdpRemarksId(remarks.getIdpRemarksId());
		return allRemarks;
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------------------
	/**
	 * @param Integer empId
	 * @param Integer appraisalId
	 * @param IDP_SheetMaster idpSheetMaster
	 * @return List<UiControll> finalPlanUiList
	 * @throws Exception finalPlanUiList	
	 */
	public List<UiControll> getPlanList(Integer appraisalId, IDP_SheetMaster idpSheetMaster) { 

		LOGGER.info("start service of plan>>>>>>>>.....:appraisalId = {}, ", appraisalId);
		UiControll previousPlan = new UiControll();
		IDP_PlanUIConfig previousPlanUIConfigDto = new IDP_PlanUIConfig();
		List<UiControll> planUiList = new ArrayList<>();
		List<IDP_PlanUIConfig> planUIConfigList = uiService.getPlanUI();	
		int previousPlanId = 0;
		boolean countFlag = true;
		for(IDP_PlanUIConfig planUIConfigDto : planUIConfigList) {
			UiControll plan = new UiControll();
			plan.setLabel(planUIConfigDto.getIdpPlanMaster().getIdpPlanDesc());
			plan.setName(YumPmsConstants.CONTROLE + planUIConfigDto.getIdpPlanMaster().getIdpPlanDesc() 
					+ YumPmsConstants.UNDERSCORE + planUIConfigDto.getIdpPlanMaster().getIdpPlanId());
			plan.setType(planUIConfigDto.getUI_control_type());
			plan.setId(planUIConfigDto.getIdpPlanMaster().getIdpPlanId());
			if(planUIConfigDto.getIdpPlanMaster().getIdpPlanId().intValue() == 1008 )
				plan.setColumn("single_part"); 

			if(planUIConfigDto.getIdpPlanMaster().getIdpPlanId().intValue() == previousPlanId) {
				if(countFlag) {
					UiSubControll prevSubControll = new UiSubControll();
					prevSubControll.setLabel(previousPlanUIConfigDto.getUiControlLabel()); 
					prevSubControll.setValue(previousPlanUIConfigDto.getUiControlValue());
					previousPlan.getOptions().add(prevSubControll);
					countFlag = false;
				}
				UiSubControll subControll = new UiSubControll();
				subControll.setLabel(planUIConfigDto.getUiControlLabel());
				subControll.setValue(planUIConfigDto.getUiControlValue());
				previousPlan.getOptions().add(subControll);

				continue;
			}
			previousPlan = plan;
			previousPlanUIConfigDto = planUIConfigDto;
			previousPlanId = planUIConfigDto.getIdpPlanMaster().getIdpPlanId();
			planUiList.add(plan);
			countFlag = true;
		}
		// fetch and set if store previous values against any controls===============	
		List<UiControll> finalPlanUiList = new ArrayList<>();
		for(UiControll control : planUiList) {
			IDP_PlanMaster planMaster = new IDP_PlanMaster();
			planMaster.setIdpPlanId(control.getId());
			IDP_PlanDetails data = idpFormService.getIdpPlanDetails(idpSheetMaster, planMaster);
			control.setValue(data.getIdp_plan_dtl_desc()); 
			control.setDataId(data.getIdp_plan_dtl_id());
			finalPlanUiList.add(control);
		}
		return finalPlanUiList;
	}


	/**
	 * @param Integer empId
	 * @param Integer appraisalId
	 * @param IDP_SheetMaster idpSheetMaster
	 * @return List<UiControll> finalPerformanceList
	 * @throws Exception
	 */
	private List<UiControll> getPerformanceList(IDP_SheetMaster idpSheetMaster) { 

		UiControll previousPerformance = new UiControll();
		IDP_PerformanceUIConfig previousPerformanceUIConfigDto = null;
		List<UiControll> performanceUiList = new ArrayList<>();
		List<IDP_PerformanceUIConfig> performanceUIConfigList = uiService.getPerformanceUI();
		int previousPlanId = 0;
		boolean countFlag = true;
		for(IDP_PerformanceUIConfig performanceUIConfigDto : performanceUIConfigList) {
			
			UiControll performance = new UiControll();
			performance.setLabel(performanceUIConfigDto.getIdpPerformanceMaster().getIdpPerformanceDesc());
			performance.setName(YumPmsConstants.CONTROLE + performanceUIConfigDto.getIdpPerformanceMaster().getIdpPerformanceDesc() + 
					YumPmsConstants.UNDERSCORE + performanceUIConfigDto.getIdpPerformanceMaster().getIdpPerformanceId());
			performance.setType(performanceUIConfigDto.getUI_control_type());
			performance.setId(performanceUIConfigDto.getIdpPerformanceMaster().getIdpPerformanceId());

			if(performanceUIConfigDto.getIdpPerformanceMaster().getIdpPerformanceId().intValue() == previousPlanId) {
				if(countFlag) {
					UiSubControll prevSubControll = new UiSubControll();
					if(previousPerformanceUIConfigDto != null) {
						prevSubControll.setLabel(previousPerformanceUIConfigDto.getUiControlLabel()); 
						prevSubControll.setValue(previousPerformanceUIConfigDto.getUiControlValue());
						prevSubControll.setId(previousPerformanceUIConfigDto.getIdpPerformanceCategoryMaster().getIdpPerformanceCategoryId());
					}
					previousPerformance.getOptions().add(prevSubControll);
					countFlag = false;
				}
				UiSubControll subControll = new UiSubControll();
				subControll.setLabel(performanceUIConfigDto.getUiControlLabel());
				subControll.setValue(performanceUIConfigDto.getUiControlValue());
				subControll.setId(performanceUIConfigDto.getIdpPerformanceCategoryMaster().getIdpPerformanceCategoryId()); 
				previousPerformance.getOptions().add(subControll);

				continue;
			}
			previousPerformance = performance;
			previousPerformanceUIConfigDto = performanceUIConfigDto;
			previousPlanId = performanceUIConfigDto.getIdpPerformanceMaster().getIdpPerformanceId();
			performanceUiList.add(performance);
			countFlag = true;
		}
		// fetch and set if store previous values against any controls
		return getFinalPerformanceList(idpSheetMaster, performanceUiList);
	}

	private List<UiControll> getFinalPerformanceList(IDP_SheetMaster idpSheetMaster, List<UiControll> performanceUiList) {

		List<UiControll> finalPerformanceList = new ArrayList<>();
		for(UiControll control : performanceUiList) {
			for(UiSubControll subControll : control.getOptions()) {
				IDP_PerformanceRating data = idpFormService.getIdpPerformanceRating(idpSheetMaster, 
						new IDP_PerformanceCategoryMaster(subControll.getId()));
				String controlValue = data.getRatingDesc();
				if(null != controlValue) {
					control.setValue(controlValue);
					Idp_PerformanceRatingReporting ratingRepotDetails = idpFormService.getRatingRepotDetails(idpSheetMaster);
					if(ratingRepotDetails != null) {
						control.setLtRatingValue(ratingRepotDetails.getRatingGoalByLt());
						control.setSupRatingValue(ratingRepotDetails.getRatingGoalBySup());
					}
					control.setDataId(data.getIdpPerformanceRatingId());
					control.setRemarks(data.getRemarksCurr());
					control.setActionPending(data.getActionPending());
					break;
				}
			}			
			finalPerformanceList.add(control);
		}
		return finalPerformanceList;
	}

	/**
	 * @param Integer empId
	 * @param Integer appraisalId
	 * @param IDP_SheetMaster idpSheetMaster
	 * @return List<DevelopmentActionPlan> developmentActionPlanResponseList
	 * @throws Exception
	 */
	private List<DevelopmentActionPlan> getDevelopmentActionPlan(Integer appraisalId, IDP_SheetMaster idpSheetMaster) { 

		LOGGER.debug("start service of getDevelopmentActionPlan>>>>>>>>.....: appraisalId = {}", appraisalId);
		List<IDP_DevelopmentActionPlan> developmentActonPlanList = idpFormService.getDevelopmentActonPlanList(idpSheetMaster);
		List<DevelopmentActionPlan> developmentActionPlanResponseList = new ArrayList<>(developmentActonPlanList.size());
		for(IDP_DevelopmentActionPlan actionPlanDto : developmentActonPlanList) {
			DevelopmentActionPlan actionPlan = new DevelopmentActionPlan();
			actionPlan.setDevActionId(actionPlanDto.getIdpDevelopmentActionPlanDtlId());
			actionPlan.setDevActionSummary(actionPlanDto.getDevelopmentActionPlanDes());
			actionPlan.setIsOnGoing(actionPlanDto.getIsOngoing());
			actionPlan.setTimeline(actionPlanDto.getDevelopmentActionTimeLine());
			actionPlan.setCreatedBy(actionPlan.getCreatedBy());
			actionPlanDto.setUpdatedDate(actionPlanDto.getUpdatedDate());
			actionPlan.setCreatedBy(actionPlanDto.getCreatedBy());
			actionPlan.setUpdatedBy(actionPlanDto.getUpdatedBy());
			actionPlan.setIdpSuggestedGoalSectionId(actionPlanDto.getIdpSuggestedGoalSectionId());
			actionPlan.setIdpSuggestedDevelopmentPlanSectionId(actionPlanDto.getIdpSuggestedDevelopmentPlanSectionId());
			developmentActionPlanResponseList.add(actionPlan);
		}
		return developmentActionPlanResponseList;
	}

	/**
	 * @param Integer empId
	 * @param Integer appraisalId
	 * @param IDP_SheetMaster idpSheetMaster
	 * @return List<BalanceOfGoal> balanceYrGoalList
	 * @throws Exception
	 */
	private List<BalanceOfGoal> getBalanceYrGoals(Integer appraisalId, IDP_SheetMaster idpSheetMaster) { 

		LOGGER.debug("start service of getBalanceYrGoals>>>>>>>>.....: appraisalId = {}", appraisalId);
		List<IDP_BalanceYearGoal> idpBalanceYrGoalList = idpFormService.getBalanceYrGoalList(idpSheetMaster);
		List<BalanceOfGoal> balanceYrGoalList = new ArrayList<>(idpBalanceYrGoalList.size());
		for(IDP_BalanceYearGoal balanceGoalDto : idpBalanceYrGoalList) {
			BalanceOfGoal balanceOfGoal = new BalanceOfGoal();
			balanceOfGoal.setBalanceYrGoalId(balanceGoalDto.getIdpBalanceYearGoalsId());
			balanceOfGoal.setBalanceGoalDesc(balanceGoalDto.getBalanceYearGoalsDesc());
			balanceOfGoal.setGoalSectionId(balanceGoalDto.getGoalSectionId());
			balanceOfGoal.setIsChecked(balanceGoalDto.getIsActive());
			balanceOfGoal.setUpdatedDate(balanceGoalDto.getUpdatedDate());
			balanceOfGoal.setIdpGoalSectionTimeline(balanceGoalDto.getGoalSectionTimeline());
			balanceOfGoal.setIsDeletedFromGoalSheet(idpFormService.isExistOnPms(balanceGoalDto.getGoalSectionId()) ? false : true);
			balanceYrGoalList.add(balanceOfGoal);
		}
		return balanceYrGoalList;
	}

	/**
	 * @param Integer empId
	 * @param Integer appraisalId
	 * @param Integer idpSheetMaster
	 * @return HowLead howLead
	 * @throws Exception
	 */
	private HowLead getSectionDetailList(Integer appraisalId, IDP_SheetMaster idpSheetMaster) { 

		LOGGER.debug("start service of getBalanceYrGoals>>>>>>>>.....: appraisalId = {}", appraisalId);
		List<IDP_SectionDtl> idpSectionDtlList = idpFormService.getSectionDtlList(idpSheetMaster);
		List<HowLeadSubData> appreciateList = new ArrayList<>();
		List<HowLeadSubData> effectivenessList = new ArrayList<>();
		HowLead howLead = new HowLead();
		HowLeadSubData growth = new HowLeadSubData();
		growth.setCategoryId(idpFormService.getIDPSectionmaster(YumPmsConstants.GROWTH_IN_UPPER_CASE).getIdpSectionId());
		boolean isGrowth = false;
		for(IDP_SectionDtl sectionDtlDto : idpSectionDtlList) {		
			
			if(sectionDtlDto.getIdpSectionTypeFlag().equalsIgnoreCase(YumPmsConstants.APPRECIATE)) {
				appreciateList.add(createAppreciateData(sectionDtlDto));
			} else if (sectionDtlDto.getIdpSectionTypeFlag().equalsIgnoreCase(YumPmsConstants.EFFECTIVENESS)) {
				effectivenessList.add(createEffictivenessData(sectionDtlDto));
			} else if(sectionDtlDto.getIdpSectionTypeFlag().equalsIgnoreCase(YumPmsConstants.GROWTH_IN_LOWER_CASE)) {
				isGrowth = true;
				growth.setCatagoryDetailId(sectionDtlDto.getIdpSectionDtlId());
				growth.setCategoryId(sectionDtlDto.getIdpSectionMaster().getIdpSectionId());
				growth.setCategoryDesc(sectionDtlDto.getIdpSectionMaster().getIdpSectionDesc());
				growth.setDescription(sectionDtlDto.getIdpSectionDtlDesc());
				growth.setIsDelete(false);
				growth.setCreatedBy(sectionDtlDto.getCreatedBy());
				growth.setCreatedDate(sectionDtlDto.getCreatedDate());
				growth.setUpdatedBy(sectionDtlDto.getUpdatedBy());
				growth.setUpdatedDate(sectionDtlDto.getUpdatedDate());	

				howLead.setGrowth(growth);
			}
		}
		howLead.setAppreciate(appreciateList);
		howLead.setEffectiveness(effectivenessList);
		if (!isGrowth) {
			howLead.setGrowth(growth);
		}
		return howLead;
	}

	/**
	 * @param Integer empId
	 * @param Integer appraisalId
	 * @param Integer idpSheetMaster
	 * @return HowLead howLead
	 * @throws Exception
	 */
	private HowLead getEffectivenessSectionDetailList(Integer appraisalId, IDP_SheetMaster idpSheetMaster) { 

		LOGGER.debug("start service of getEffectivenessSectionDetailList>>>>>>>>.....: appraisalId = {}", appraisalId);
		List<IDP_SectionDtl> idpSectionDtlList = idpFormService.getSectionDtlList(idpSheetMaster);
		List<HowLeadSubData> effectivenessList = new ArrayList<>();
		HowLead howLead = new HowLead();
		HowLeadSubData growth = new HowLeadSubData();
		growth.setCategoryId(idpFormService.getIDPSectionmaster(YumPmsConstants.GROWTH_IN_UPPER_CASE).getIdpSectionId());
		for(IDP_SectionDtl sectionDtlDto : idpSectionDtlList) {		
			if (sectionDtlDto.getIdpSectionTypeFlag().equalsIgnoreCase(YumPmsConstants.EFFECTIVENESS)) {
				effectivenessList.add(createEffictivenessData(sectionDtlDto));
			} 
		}
		howLead.setEffectiveness(effectivenessList);
		return howLead;
	}
	

	/**
	 * @param Integer empId
	 * @param Integer appraisalId
	 * @param Integer idpSheetMaster
	 * @return SuggestiveGoal suggestive Goal
	 * @throws Exception
	 */
	private List<IdpSuggestedGoal> getSuggestiveGoalList(Integer appraisalId, Integer idpSheetID) { 

		LOGGER.debug("start service of getSuggestiveGoalList>>>>>>>>.....: appraisalId = {}", appraisalId);
		List<IdpSuggestedGoal> suggestiveGoals = new ArrayList<>();
		suggestiveGoals = idpFormService.getSuggestedGoals(idpSheetID);
		if(CollectionUtils.isEmpty(suggestiveGoals)) {
			return Collections.emptyList();
		}
		return suggestiveGoals;
	}

	private void updatePerformanceRating3(IDP_SheetMaster idpSheetMaster, int empId, String submitedFrom, IDP_PerformanceRating ratingDescription) 
			throws YumPMSDataSaveException {

		Idp_PerformanceRatingReporting ratingReporting = null;
		if(submitedFrom.equalsIgnoreCase(YumPmsConstants.SUPERVISOR)) {
			ratingReporting = populatePerformanceRatingReporting(idpSheetMaster, empId);
			ratingReporting.setRatingLTSbySup(ratingDescription.getRatingDesc());
			ratingReporting.setRemarksSup(ratingDescription.getRemarksCurr());
			
		} else if(submitedFrom.equalsIgnoreCase(YumPmsConstants.SUPERVISOR_LT)) {
			ratingReporting = populatePerformanceRatingReporting(idpSheetMaster, empId);
			ratingReporting.setRatingLTSbySup(ratingDescription.getRatingDesc());
			ratingReporting.setRatingLTSbyLT(ratingDescription.getRatingDesc());
			ratingReporting.setRemarksSup(ratingDescription.getRemarksCurr());
			ratingReporting.setRemarksLT(ratingDescription.getRemarksCurr());
			ratingReporting.setRatingLTSbyHR(ratingDescription.getRatingDesc());
			ratingReporting.setRemarksHR(ratingDescription.getRemarksCurr());
			
		} else if(submitedFrom.equalsIgnoreCase(YumPmsConstants.LT)) {
			ratingReporting = populatePerformanceRatingReporting(idpSheetMaster, empId);
			ratingReporting.setRatingLTSbyLT(ratingDescription.getRatingDesc());
			ratingReporting.setRemarksLT(ratingDescription.getRemarksCurr());
			ratingReporting.setRatingLTSbyHR(ratingDescription.getRatingDesc());
			ratingReporting.setRemarksHR(ratingDescription.getRemarksCurr());
			
		} else if(submitedFrom.equalsIgnoreCase(YumPmsConstants.HR)) {
			ratingReporting = populatePerformanceRatingReporting(idpSheetMaster, empId);
			ratingReporting.setRatingLTSbyHR(ratingDescription.getRatingDesc());
			ratingReporting.setRemarksHR(ratingDescription.getRemarksCurr());
		}
		if(ratingReporting != null) {
			idpFormService.insertOrUpdateIdpPerformanceRatingReporting(ratingReporting);
		}
	}

	private void updatePerformanceRating2(IDP_SheetMaster idpSheetMaster, int empId, String submitedFrom, IDP_PerformanceRating ratingDescription) 
			throws YumPMSDataSaveException {

		Idp_PerformanceRatingReporting ratingReporting = null;
		if(submitedFrom.equalsIgnoreCase(YumPmsConstants.SUPERVISOR)) {
			ratingReporting = populatePerformanceRatingReporting(idpSheetMaster, empId);
			ratingReporting.setRatingCulturebySup(ratingDescription.getRatingDesc());
			ratingReporting.setRemarksSup(ratingDescription.getRemarksCurr());

		} else if(submitedFrom.equalsIgnoreCase(YumPmsConstants.SUPERVISOR_LT)) {
			ratingReporting = populatePerformanceRatingReporting(idpSheetMaster, empId);
			ratingReporting.setRatingCulturebySup(ratingDescription.getRatingDesc());
			ratingReporting.setRatingCultureByLT(ratingDescription.getRatingDesc()); 
			ratingReporting.setRemarksSup(ratingDescription.getRemarksCurr());
			ratingReporting.setRemarksLT(ratingDescription.getRemarksCurr());
			ratingReporting.setRatingCulturebyHR(ratingDescription.getRatingDesc());
			ratingReporting.setRemarksHR(ratingDescription.getRemarksCurr());	
			
		} else if(submitedFrom.equalsIgnoreCase(YumPmsConstants.LT)) {
			ratingReporting = populatePerformanceRatingReporting(idpSheetMaster, empId);
			ratingReporting.setRatingCultureByLT(ratingDescription.getRatingDesc());
			ratingReporting.setRemarksLT(ratingDescription.getRemarksCurr());	
			ratingReporting.setRatingCulturebyHR(ratingDescription.getRatingDesc());
			ratingReporting.setRemarksHR(ratingDescription.getRemarksCurr());
			
		} else if(submitedFrom.equalsIgnoreCase(YumPmsConstants.HR)) {
			ratingReporting = populatePerformanceRatingReporting(idpSheetMaster, empId);
			ratingReporting.setRatingCulturebyHR(ratingDescription.getRatingDesc());
			ratingReporting.setRemarksHR(ratingDescription.getRemarksCurr());						
		}
		if(ratingReporting != null) {
			idpFormService.insertOrUpdateIdpPerformanceRatingReporting(ratingReporting);
		}
	}

	private void updatePerformanceRating1(IDP_SheetMaster idpSheetMaster, int empId, String submitedFrom, IDP_PerformanceRating ratingDescription) 
			throws YumPMSDataSaveException {

		Idp_PerformanceRatingReporting ratingReporting = null;
		if(submitedFrom.equalsIgnoreCase(YumPmsConstants.SUPERVISOR)) {
			ratingReporting = populatePerformanceRatingReporting(idpSheetMaster, empId);
			ratingReporting.setRatingGoalBySup(ratingDescription.getRatingDesc());

		} else if(submitedFrom.equalsIgnoreCase(YumPmsConstants.SUPERVISOR_LT)) {
			ratingReporting = populatePerformanceRatingReporting(idpSheetMaster, empId);
			ratingReporting.setRatingGoalBySup(ratingDescription.getRatingDesc());
			ratingReporting.setRatingGoalByLt(ratingDescription.getRatingDesc()); 

		} else if(submitedFrom.equalsIgnoreCase(YumPmsConstants.LT)) {
			ratingReporting = populatePerformanceRatingReporting(idpSheetMaster, empId);
			ratingReporting.setRatingGoalByLt(ratingDescription.getRatingDesc()); 
		}
		if(ratingReporting != null) {
			idpFormService.insertOrUpdateIdpPerformanceRatingReporting(ratingReporting);
		}
	}
	
	private UiSubControll getSelectedRating(String rating, UiControll uiControll) {
		
		if(CollectionUtils.isNotEmpty(uiControll.getOptions())) {
			for(UiSubControll subControll : uiControll.getOptions()) {
				if(StringUtils.equalsIgnoreCase(rating, subControll.getValue())) {
					return subControll;
				}
			}
		}
		return new UiSubControll();
	}
	
	private Idp_PerformanceRatingReporting populatePerformanceRatingReporting(IDP_SheetMaster idpSheetMaster, int empId) {
		
		Idp_PerformanceRatingReporting ratingReporting = idpFormService.getRatingRepotDetails(idpSheetMaster);
		ratingReporting = ratingReporting != null ? ratingReporting : new Idp_PerformanceRatingReporting();
		ratingReporting.setEmployee(new EmployeeNew(empId));
		ratingReporting.setAppraisalPeriord(idpSheetMaster.getAppraisalPeriod());
		ratingReporting.setIdpSheetMaster(idpSheetMaster);
		return ratingReporting;
	}

	private List<HowLeadSubData> populateEffectivenessList(HowLead howLead, IDP_SheetMaster idpSheetMaster) throws YumPMSDataSaveException {
		
		List<HowLeadSubData> effectivenessResponse = new ArrayList<>(); 
		for(HowLeadSubData subData : howLead.getEffectiveness()) {			
			IDP_SectionDtl sectionDetailDto = new IDP_SectionDtl(); 
			IDP_SectionMaster sectionMaster = new IDP_SectionMaster();
			sectionMaster.setIdpSectionId(subData.getCategoryId()); 

			sectionDetailDto.setIdpSectionMaster(sectionMaster);
			sectionDetailDto.setIdpSheetMaster(idpSheetMaster);
			sectionDetailDto.setIdpSectionDtlDesc(subData.getDescription());

			IDP_SubSectionMaster subSectionMastr = new IDP_SubSectionMaster();
			subSectionMastr.setIdpSubSectionId(0<subData.getSubCategoryId()?subData.getSubCategoryId():0); 
			sectionDetailDto.setIdpSubSectionMaster(subSectionMastr);

			IDP_SubSubSectionMaster subSubSectionMaster = new IDP_SubSubSectionMaster();
			subSubSectionMaster.setIdpSubSubSectionId(0 < subData.getSubSubCategoryId() ? subData.getSubSubCategoryId() : 0);
			sectionDetailDto.setIdpSubSubSectionMaster(subSubSectionMaster);

			IDP_SubSubSubSectionMaster subSubSubSectionMaster = new IDP_SubSubSubSectionMaster();
			subSubSubSectionMaster.setIdpSubSubSubSectionId(0 < subData.getSubSubSubCategoryId()? subData.getSubSubSubCategoryId(): 0);
			sectionDetailDto.setIdpSubSubSubSectionMaster(subSubSubSectionMaster);

			sectionDetailDto.setIdpSectionTypeFlag(YumPmsConstants.EFFECTIVENESS);
			sectionDetailDto.setIsActive(subData.getIsDelete() ? false : true);
			sectionDetailDto.setCreatedBy(subData.getCreatedBy());
			sectionDetailDto.setUpdatedBy(subData.getUpdatedBy());
			if(null != subData.getCatagoryDetailId() && subData.getCatagoryDetailId() > 0) {
				sectionDetailDto.setIdpSectionDtlId(subData.getCatagoryDetailId()); 
				sectionDetailDto.setUpdatedBy(subData.getUpdatedBy());
			} else {
				sectionDetailDto.setCreatedBy(subData.getCreatedBy());
				sectionDetailDto.setUpdatedBy(subData.getCreatedBy());
			}		
			sectionDetailDto = idpFormService.insertOrUpdateIdpSectionDetails(sectionDetailDto);		 
			subData.setCatagoryDetailId(sectionDetailDto.getIdpSectionDtlId());
			subData.setCategoryDesc(idpFormService.getIdpSectionMasterById(subData.getCategoryId()).getIdpSectionDesc());

			if(!subData.getIsDelete()) {
				effectivenessResponse.add(subData);
			}
		}
		return effectivenessResponse;
	}

	private List<HowLeadSubData> populateAppreciateList(HowLead howLead, IDP_SheetMaster idpSheetMaster) throws YumPMSDataSaveException {
		
		List<HowLeadSubData> appreciateResponse = new ArrayList<>(); 
		for(HowLeadSubData subData : howLead.getAppreciate()) {			
			
			IDP_SectionDtl sectionDetailDto = new IDP_SectionDtl(); 
			IDP_SectionMaster sectionMaster = new IDP_SectionMaster();
			sectionMaster.setIdpSectionId(subData.getCategoryId()); 
			sectionDetailDto.setIdpSectionMaster(sectionMaster);
			sectionDetailDto.setIdpSheetMaster(idpSheetMaster);
			sectionDetailDto.setIdpSectionDtlDesc(subData.getDescription());

			IDP_SubSectionMaster subSectionMastr = new IDP_SubSectionMaster();
			subSectionMastr.setIdpSubSectionId(0 < subData.getSubCategoryId() ? subData.getSubCategoryId() : 0); 
			sectionDetailDto.setIdpSubSectionMaster(subSectionMastr);

			IDP_SubSubSectionMaster subSubSectionMaster = new IDP_SubSubSectionMaster();
			subSubSectionMaster.setIdpSubSubSectionId(0 < subData.getSubSubCategoryId() ? subData.getSubSubCategoryId() : 0);
			sectionDetailDto.setIdpSubSubSectionMaster(subSubSectionMaster);

			IDP_SubSubSubSectionMaster subSubSubSectionMaster = new IDP_SubSubSubSectionMaster();
			subSubSubSectionMaster.setIdpSubSubSubSectionId(0 < subData.getSubSubSubCategoryId() ? subData.getSubSubSubCategoryId() : 0);
			sectionDetailDto.setIdpSubSubSubSectionMaster(subSubSubSectionMaster);


			sectionDetailDto.setIdpSectionTypeFlag(YumPmsConstants.APPRECIATE);
			sectionDetailDto.setIsActive(subData.getIsDelete() ? false : true);

			if(null != subData.getCatagoryDetailId() && subData.getCatagoryDetailId() > 0) {
				sectionDetailDto.setIdpSectionDtlId(subData.getCatagoryDetailId());
				sectionDetailDto.setUpdatedBy(subData.getUpdatedBy());
				sectionDetailDto.setCreatedBy(subData.getCreatedBy());
			} else {
				sectionDetailDto.setCreatedBy(subData.getCreatedBy());
				sectionDetailDto.setUpdatedBy(subData.getCreatedBy());
			}
			sectionDetailDto = idpFormService.insertOrUpdateIdpSectionDetails(sectionDetailDto);

			subData.setCatagoryDetailId(sectionDetailDto.getIdpSectionDtlId());
			subData.setCategoryDesc(idpFormService.getIdpSectionMasterById(subData.getCategoryId()).getIdpSectionDesc());

			if(!BooleanUtils.toBoolean(subData.getIsDelete())) {
				appreciateResponse.add(subData);
			}
		}
		return appreciateResponse;
	}

	private HowLeadSubData createEffictivenessData(IDP_SectionDtl sectionDtlDto) {
		
		HowLeadSubData effectiveness = new HowLeadSubData();
		effectiveness.setCatagoryDetailId(sectionDtlDto.getIdpSectionDtlId());
		effectiveness.setCategoryId(sectionDtlDto.getIdpSectionMaster().getIdpSectionId());
		effectiveness.setCategoryDesc(sectionDtlDto.getIdpSectionMaster().getIdpSectionDesc());
		effectiveness.setDescription(sectionDtlDto.getIdpSectionDtlDesc());
		try {
			effectiveness.setSubCategoryId(sectionDtlDto.getIdpSubSectionMaster().getIdpSubSectionId());
			effectiveness.setSubCategoryDesc(sectionDtlDto.getIdpSubSectionMaster().getIdpSubSectionDesc());
		} catch (ObjectNotFoundException e) {
			effectiveness.setSubCategoryId(-1);
		}
		try {
			effectiveness.setSubSubCategoryId(sectionDtlDto.getIdpSubSubSectionMaster().getIdpSubSubSectionId());
			effectiveness.setSubSubCategoryDesc(sectionDtlDto.getIdpSubSubSectionMaster().getIdpSubSubSectionDesc());
		} catch (ObjectNotFoundException e) {
			effectiveness.setSubSubCategoryId(-1);
		}
		try {
			if(null != sectionDtlDto.getIdpSubSubSubSectionMaster()) {
				effectiveness.setSubSubSubCategoryId(sectionDtlDto.getIdpSubSubSubSectionMaster().getIdpSubSubSubSectionId());
				effectiveness.setSubSubSubCategoryDesc(sectionDtlDto.getIdpSubSubSubSectionMaster().getIdpSubSubSubSectionDesc());
			}
		} catch (ObjectNotFoundException e) {
			effectiveness.setSubSubSubCategoryId(-1);
		}
		effectiveness.setSectionApprId(sectionDtlDto.getIdpSectionMaster().getApprId());
		effectiveness.setIsDelete(false);
		effectiveness.setCreatedBy(sectionDtlDto.getCreatedBy());
		effectiveness.setCreatedDate(sectionDtlDto.getCreatedDate());
		effectiveness.setUpdatedBy(sectionDtlDto.getUpdatedBy());
		effectiveness.setUpdatedDate(sectionDtlDto.getUpdatedDate());
		return effectiveness;
	}

	private HowLeadSubData createAppreciateData(IDP_SectionDtl sectionDtlDto) {

		HowLeadSubData appreciate = new HowLeadSubData();
		appreciate.setCatagoryDetailId(sectionDtlDto.getIdpSectionDtlId());
		appreciate.setCategoryId(sectionDtlDto.getIdpSectionMaster().getIdpSectionId());
		appreciate.setCategoryDesc(sectionDtlDto.getIdpSectionMaster().getIdpSectionDesc());
		appreciate.setDescription(sectionDtlDto.getIdpSectionDtlDesc());
		try {
			appreciate.setSubCategoryId(sectionDtlDto.getIdpSubSectionMaster().getIdpSubSectionId());
			appreciate.setSubCategoryDesc(sectionDtlDto.getIdpSubSectionMaster().getIdpSubSectionDesc());
		} catch (ObjectNotFoundException e) {
			appreciate.setSubCategoryId(-1);
		}
		try {
			appreciate.setSubSubCategoryId(sectionDtlDto.getIdpSubSubSectionMaster().getIdpSubSubSectionId());
			appreciate.setSubSubCategoryDesc(sectionDtlDto.getIdpSubSubSectionMaster().getIdpSubSubSectionDesc());
		} catch (ObjectNotFoundException e) {
			appreciate.setSubSubCategoryId(-1);
		}
		try {
			IDP_SubSubSubSectionMaster subSubSubSection = sectionDtlDto.getIdpSubSubSubSectionMaster();
			if(subSubSubSection != null) {
				appreciate.setSubSubSubCategoryId(subSubSubSection.getIdpSubSubSubSectionId());
				appreciate.setSubSubSubCategoryDesc(subSubSubSection.getIdpSubSubSubSectionDesc());
			}
		} catch (ObjectNotFoundException e) {
			appreciate.setSubSubSubCategoryId(-1);
		}
		appreciate.setIsDelete(false);
		appreciate.setCreatedBy(sectionDtlDto.getCreatedBy());
		appreciate.setCreatedDate(sectionDtlDto.getCreatedDate());
		appreciate.setUpdatedBy(sectionDtlDto.getUpdatedBy());
		appreciate.setUpdatedDate(sectionDtlDto.getUpdatedDate());
		return appreciate;
	}

	public List<UiControll> getPerformanceRatingUiList(IDP_SheetMaster idpSheetMaster) {
		
		UiControll previousPerformance = new UiControll();
		IDP_PerformanceUIConfig previousPerformanceUIConfigDto = null;
		List<UiControll> performanceUiList = new ArrayList<>();
		List<IDP_PerformanceUIConfig> performanceUIConfigList = uiService.getPerformanceUI();
		int previousPlanId = 0;
		boolean countFlag = true;
		for(IDP_PerformanceUIConfig performanceUIConfigDto : performanceUIConfigList) {
			UiControll performance = new UiControll();
			performance.setLabel(performanceUIConfigDto.getIdpPerformanceMaster().getIdpPerformanceDesc());
			performance.setName(YumPmsConstants.CONTROLE + performanceUIConfigDto.getIdpPerformanceMaster().getIdpPerformanceDesc() 
					+ YumPmsConstants.UNDERSCORE + performanceUIConfigDto.getIdpPerformanceMaster().getIdpPerformanceId());
			performance.setType(performanceUIConfigDto.getUI_control_type());
			performance.setId(performanceUIConfigDto.getIdpPerformanceMaster().getIdpPerformanceId());

			if(performanceUIConfigDto.getIdpPerformanceMaster().getIdpPerformanceId().intValue() == previousPlanId) {
				if(countFlag) {
					UiSubControll prevSubControll = new UiSubControll();
					if(previousPerformanceUIConfigDto != null) {
						prevSubControll.setLabel(previousPerformanceUIConfigDto.getUiControlLabel()); 
						prevSubControll.setValue(previousPerformanceUIConfigDto.getUiControlValue());
						prevSubControll.setId(previousPerformanceUIConfigDto.getIdpPerformanceCategoryMaster().getIdpPerformanceCategoryId());
					}
					previousPerformance.getOptions().add(prevSubControll);
					countFlag = false;
				}
				UiSubControll subControll = new UiSubControll();
				subControll.setLabel(performanceUIConfigDto.getUiControlLabel());
				subControll.setValue(performanceUIConfigDto.getUiControlValue());
				subControll.setId(performanceUIConfigDto.getIdpPerformanceCategoryMaster().getIdpPerformanceCategoryId()); 
				previousPerformance.getOptions().add(subControll);
				continue;
			}
			previousPerformance = performance;
			previousPerformanceUIConfigDto = performanceUIConfigDto;
			previousPlanId = performanceUIConfigDto.getIdpPerformanceMaster().getIdpPerformanceId();
			performanceUiList.add(performance);
			countFlag = true;
		}
		// fetch and set if store previous values against any controls===============	
		return getFinalPerformanceDataList(idpSheetMaster, performanceUiList);
	}

	private List<UiControll> getFinalPerformanceDataList(IDP_SheetMaster idpSheetMaster, List<UiControll> performanceUiList) {
		
		List<UiControll> finalPerformanceList = new ArrayList<>();
		for(UiControll control : performanceUiList) {
			for(UiSubControll subControll : control.getOptions()) {
				IDP_PerformanceRating data = idpFormService.getIdpPerformanceRating(idpSheetMaster, 
						new IDP_PerformanceCategoryMaster(subControll.getId()));
				String controlValue = data.getRatingDesc();
				if(null != controlValue) {
					control.setValue(controlValue);
					control.setDataId(data.getIdpPerformanceRatingId());
					control.setRemarks(data.getRemarksCurr());
					break;
				}
			}			
			finalPerformanceList.add(control);
		}
		return finalPerformanceList;
	}

	/**
	 * Get IDP Remarks by IDP sheet Id
	 * @param idpSheetMaster
	 * @return
	 */
	public RemarksDetails getAllRemarks(IDP_SheetMaster idpSheetMaster) { 

		LOGGER.debug("start service of getAllRemarks>>>>>>>>.....: idpSheetId = {}", idpSheetMaster != null ? idpSheetMaster.getIdpSheetId() : null);
		IDP_Remarks allRemarks = idpFormService.getAllRemarks(idpSheetMaster);	
		RemarksDetails remarksDetails = new RemarksDetails();
		if(allRemarks != null) {
			remarksDetails.setIdpRemarksId(allRemarks.getIdpRemarksId());
			remarksDetails.setHowILeadAppreciateRemarks(allRemarks.getHowILeadAppreciateRemarks()); 
			remarksDetails.setHowILeadEffectivenessRemarks(allRemarks.getHowILeadEffectivenessRemarks());
			remarksDetails.setHowILeadGrowthRemarks(allRemarks.getHowILeadGrowthRemarks());
			remarksDetails.setMyPlanRemarks(allRemarks.getMyPlanRemarks());
			remarksDetails.setDevelopmentActionPlanRemarks(allRemarks.getDevelopmentActionPlanRemarks());
			remarksDetails.setPerformanceCheckInGoalsRemarks(allRemarks.getPerformanceCheckInGoalsRemarks());
			remarksDetails.setBalanceYearGoalsRemarks(allRemarks.getBalanceYearGoalsRemarks());
		}
		return remarksDetails;
	}

	/**
	 * Send email about the IDP sheet status and also attached the IDP PDF sheet if the status id is 1005
	 * @param idpSheetId
	 * @param statusId
	 * @param param2
	 * @throws Exception
	 */
	private void sendMail(Integer idpSheetId, int statusId, String param2) {

		List<Object[]> results = idpUtil.getIdpStatusChangeEmailParams(idpSheetId.toString(), param2);
		if(CollectionUtils.isEmpty(results)) {
			return;
		}
		for (Object[] tuples : results) {
			String attachmentPath = StringUtils.EMPTY;
			if(statusId == 1005) {
				attachmentPath = createIdpSheetPdfPath(idpSheetId);
			}
			mailSender.sendMail(YumPmsUtils.toStringorNull(tuples[0]), 
					YumPmsUtils.toStringorNull(tuples[1]), 
					YumPmsUtils.toStringorNull(tuples[2]), 
					YumPmsUtils.toStringorNull(tuples[3]), 
					attachmentPath);
			if(StringUtils.isNotBlank(attachmentPath)) {
				ResourceUtils.delete(attachmentPath);
			}
		}
	}


	/**
	 * create IDP Sheet report PDF Path
	 * @param idpSheetId
	 * @return the PDF report path
	 * @throws Exception
	 */
	private String createIdpSheetPdfPath(Integer idpSheetId) {

		IDP_SheetMaster idpSheet = workFlowService.getIdpSheetbyId(idpSheetId);
		Integer apprId = idpSheet.getAppraisalPeriod().getAppraisalPeriodId();
		IdpUIConfig idpUIConfig = getIdpUi(idpSheet.getEmployee().getEmpId(),apprId , false);
		String fileName = YumPmsUtils.getPdfFileName(idpUIConfig.getEmpName(), idpUIConfig.getAppraisalDescription(), 
				YumPmsConstants.IDP_SHEET_PDF_FILE);
		return ResourceUtils.createTempFile(fileName, YumPmsUtils.getIDPSheetReportData(idpUIConfig));
	}

	/**
	 * Insert a new IDP Sheet record
	 * @param empId
	 * @param appraisalId
	 * @param employee
	 * @return
	 */
	private IDP_SheetMaster insertIdpSheet(Integer empId, Integer appraisalId, String empName) {

		IDP_SheetMaster idpSheetMaster = new IDP_SheetMaster();
		idpSheetMaster.getAppraisalPeriod().setAppraisalPeriodId(appraisalId); 
		idpSheetMaster.getEmployee().setEmpId(empId); 
		idpSheetMaster.setIsActive(true);
		idpSheetMaster.setCreatedBy(String.valueOf(empId)); 
		try {
			idpSheetMaster = idpFormService.insertOrUpdateIdpSheetDetails(idpSheetMaster);
			updateIdpSheetStatus(idpSheetMaster.getIdpSheetId(), 1002, empName, 1001);
		} catch (YumPMSDataSaveException e) {
			throw new CommonException(e);
		}
		return idpSheetMaster;
	}

	@Override
	public IdpUIConfig getPrevOpportunity(Integer empId, Integer appraisalId) {
		IDP_SheetMaster idpSheetMaster = idpFormService.getIdpSheetMaster(appraisalId, empId);
		EmployeeCurrentDetails employee = employeeService.getEmployeeDetailsByUserId(empId.toString(), appraisalId, YumPmsConstants.EMPLOYEE_ID); 
		GoalSheet goalSheet = 
				goalServices.getGoalSheetbyEmpId(new EmployeeNew(employee.getEmpId()), new AppraisalPeriod(appraisalId), sessionFactory);

		int goalSheetStatusId = null != goalSheet && null != goalSheet.getGoalStatus() ? goalSheet.getGoalStatus().getGoalStatusId().intValue() : 0;
		IdpUIConfig idpUiConfigData = new IdpUIConfig();
		idpUiConfigData.setIdpSheetId(null != idpSheetMaster ? idpSheetMaster.getIdpSheetId() : 0);
		String idpSheetStatus = (null != idpSheetMaster && null != idpSheetMaster.getIdpStatus()) 
				? idpSheetMaster.getIdpStatus().getIdpSheetStatusName(): StringUtils.EMPTY;
		idpUiConfigData.setIdpSheetStatus(idpSheetStatus); 
		idpUiConfigData.setEmpId(empId);
		idpUiConfigData.setEmpName(employee.getEmpName());
		idpUiConfigData.setEmpGrade(employee.getGrade());
		idpUiConfigData.setEmpDesignation(employee.getDesignation()); 
		idpUiConfigData.setAppraisalId(appraisalId);
		idpUiConfigData.setAppraisalDescription(appraisalService.getAppraisalPeriodbyId(appraisalId).getPeriodDesc());
		idpUiConfigData.setSupervisorId(employee.getManagerId());
		idpUiConfigData.setSupervisorName(employee.getManagerName());
		idpUiConfigData.setLtId(employee.getFunctionalLtsId());
		idpUiConfigData.setFunctionLTName(employee.getFunctionalLtsName());
		idpUiConfigData.setFunctionName(employee.getFunctionName());
		idpUiConfigData.setGoalSheetIsCompleted(goalSheetStatusId == 1005);
		
		idpUiConfigData.setHowLead(getEffectivenessSectionDetailList(appraisalId, idpSheetMaster));
		
		return idpUiConfigData;
	}
	


}