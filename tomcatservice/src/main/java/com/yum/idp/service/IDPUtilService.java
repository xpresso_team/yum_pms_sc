/**
 * 
 */
package com.yum.idp.service;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yum.pms.dao.IDP_PerformanceCategoryMaster;
import com.yum.pms.dao.IDP_SectionMaster;
import com.yum.pms.service.YumHibernateUtilServices;
import com.yum.pms.utils.YumPmsConstants;

/**
 * @author jayanta.biswas
 * @version 1.0
 * @since   24-01-2018
 */
@Service
@SuppressWarnings("unchecked")
public class IDPUtilService {
	
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	private YumHibernateUtilServices util;

	/**
	 * @return Map<String, Object> mapOfItem
	 */
	public List<IDP_SectionMaster> getSectionList() {
		
		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(IDP_SectionMaster.class)
				.add(Restrictions.eq(YumPmsConstants.IS_ACTIVE, true))
				.add(Restrictions.ne(YumPmsConstants.IDP_SECTION_DESC, YumPmsConstants.GROWTH_IN_UPPER_CASE))
				.list();	 		
	}
	
	public IDP_PerformanceCategoryMaster getCatagoryMasterByDescription(String description) {

		IDP_PerformanceCategoryMaster catagoryMaster = 
				(IDP_PerformanceCategoryMaster)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(IDP_PerformanceCategoryMaster.class)
				.add(Restrictions.eq(YumPmsConstants.IDP_PERFORMANCE_CATEGORY_DESC, description))
				.uniqueResult();  
		return null != catagoryMaster ? catagoryMaster : new IDP_PerformanceCategoryMaster(); 
	}
	
	/**
	 * Used to get all the email parameters when a supervisor submit a rating by providing exceptional
	 * @param idpSheetId
	 * @return
	 */
	public List<Object[]> getEmailParamsOnExceptionalRatingBySup(int idpSheetId) {
		List<Object[]> results = null;
		if(util.isActiveSp(sessionFactory, "sp_EmailNotification_IDP_ToLT_OnExceptionalRatingBySup")) {
			 results = 
					 YumHibernateUtilServices.getSession(sessionFactory)
					 .createSQLQuery(String.format(YumPmsConstants.SP_IDP_EMAIL_TO_LT_ON_EXCEPTIONAL_RATING, YumPmsConstants.PARAM_1))
					 .setString(YumPmsConstants.PARAM_1,  String.valueOf(idpSheetId))
					 .list();
		}
		 return CollectionUtils.isEmpty(results) ? Collections.emptyList() : results;
	 }
	
	/**
	 * Used to get all the email parameters when a LT reject the exceptional rating given by supervisor
	 * @param idpSheetId
	 * @return
	 */
	public List<Object[]> getEmailParamsOnRejectionExceptionalRating(int idpSheetId) {
		List<Object[]> results = null;
		if(util.isActiveSp(sessionFactory, "sp_EmailNotification_IDP_ToSup_OnExceptionalRatingRejectedByLT")) {
			 results = 
					 YumHibernateUtilServices.getSession(sessionFactory)
					 .createSQLQuery(String.format(YumPmsConstants.SP_IDP_EMAIL_TO_SUP_ON_RATING_REJECTION, YumPmsConstants.PARAM_1))
					 .setString(YumPmsConstants.PARAM_1,  String.valueOf(idpSheetId))
					 .list();
		}
		 return CollectionUtils.isEmpty(results) ? Collections.emptyList() : results;
	 }
	
	 /**
	  * This returns the email parameters of the supervisor's supervisor. Used when supervisor edit the performance mid-year check-in data
	 * @param idpSheetId
	 * @return
	 */
	public List<Object[]> getSup2EmailParams(int idpSheetId) {
		List<Object[]> results = null;
			if(util.isActiveSp(sessionFactory, "sp_EmailNotification_IDP_ToSup2_OnLTSubmission")) {
			 results = 
					 YumHibernateUtilServices.getSession(sessionFactory)
					 .createSQLQuery(String.format(YumPmsConstants.SP_IDP_EMAIL_TO_SUP_2, YumPmsConstants.PARAM_1))
					 .setString(YumPmsConstants.PARAM_1,  String.valueOf(idpSheetId))
					 .list();
		}
		 return CollectionUtils.isEmpty(results) ? Collections.emptyList() : results;
	 }
	
	 /**
	  * THis returns the email parameter when an IDP status change
	 * @param idpSheetId
	 * @param status
	 * @return
	 */
	public List<Object[]> getIdpStatusChangeEmailParams(String idpSheetId, String status) {
		if(util.isActiveSp(sessionFactory, "sp_EmailNotification_ByIDP_java")) {
			 List<Object[]> results = 
					 YumHibernateUtilServices.getSession(sessionFactory)
					 .createSQLQuery(String.format(YumPmsConstants.SP_IDP_EMAIL_STATUS_CHANGE, YumPmsConstants.PARAM_1, YumPmsConstants.PARAM_2))
					 .setString(YumPmsConstants.PARAM_1,  idpSheetId)
					 .setString(YumPmsConstants.PARAM_2, status)
					 .list();
			 return CollectionUtils.isEmpty(results) ? Collections.emptyList() : results;
		}else {
			return Collections.emptyList();
		}
	 }
	
	/**
	 * execute IDP Version History Procedure
	 * @param idpSheetId
	 */
	public void executeIdpVersionHistoryProcedure(Integer idpSheetId) {
		
		YumHibernateUtilServices.getSession(sessionFactory)
		.createSQLQuery(String.format(YumPmsConstants.SP_IDP_VERSION_HISTORY, YumPmsConstants.PARAM_1))
		.setInteger(YumPmsConstants.PARAM_1, idpSheetId)
		.executeUpdate();
	}
	
}
