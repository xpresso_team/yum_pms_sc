package com.yum.idp.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yum.idp.domain.IdpSuggestedGoal;
import com.yum.pms.dao.GoalSection;
import com.yum.pms.dao.IDP_BalanceYearGoal;
import com.yum.pms.dao.IDP_DevelopmentActionPlan;
import com.yum.pms.dao.IDP_PerformanceCategoryMaster;
import com.yum.pms.dao.IDP_PerformanceRating;
import com.yum.pms.dao.IDP_PlanDetails;
import com.yum.pms.dao.IDP_PlanMaster;
import com.yum.pms.dao.IDP_Remarks;
import com.yum.pms.dao.IDP_SectionDtl;
import com.yum.pms.dao.IDP_SectionMaster;
import com.yum.pms.dao.IDP_SelectedSuggestedGoalDtl;
import com.yum.pms.dao.IDP_SheetMaster;
import com.yum.pms.dao.Idp_PerformanceRatingReporting;
import com.yum.pms.domain.EmailDomain;
import com.yum.pms.exception.YumPMSDataSaveException;
import com.yum.pms.service.YumHibernateUtilServices;
import com.yum.pms.utils.YumPmsConstants;
import com.yum.pms.utils.YumPmsUtils;

/**
* This Class created for transaction to DB ,
* when ever any event fire from IDP form.
*
* @author  Jayanta Biswas
* @version 1.0
* @since   21-12-2017
*/
@Service
@SuppressWarnings("unchecked")
public class IdpFormService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IdpFormService.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private YumHibernateUtilServices util;
	
	/**
	 * @param int appraisalId
	 * @param int empId
	 * @return IDP_SheetMaster idpSheetMaster
	 */
	public IDP_SheetMaster getIdpSheetMaster(int appraisalId, int empId) {	

		return 
				(IDP_SheetMaster) YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(IDP_SheetMaster.class)
				.createAlias(YumPmsConstants.APPRAISAL_PERIOD, YumPmsConstants.APPRAISAL)
				.createAlias(YumPmsConstants.EMPLOYEE_MODEL, YumPmsConstants.EMP)
				.add(Restrictions.eq("appraisal.appraisalPeriodId", appraisalId))
				.add(Restrictions.eq("emp.empId",empId)).uniqueResult(); 
	}
	
	/**
	 * @param sheetId
	 * @return
	 */
	public IDP_SheetMaster getIdpSheetMasterById(int sheetId) {	
		
		return 
				(IDP_SheetMaster)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(IDP_SheetMaster.class)
				.add(Restrictions.eq(YumPmsConstants.IDP_SHEET_ID, sheetId))
				.add(Restrictions.eq(YumPmsConstants.IS_ACTIVE, true))
				.uniqueResult(); 
	}
	
	/**
	 * @param IDP_SheetMaster sheetMaster
	 * @param IDP_PerformanceCategoryMaster catagoryMaster
	 * @return IDP_PerformanceRating
	 */
	public IDP_PerformanceRating getIdpPerformanceRating(IDP_SheetMaster sheetMaster, IDP_PerformanceCategoryMaster catagoryMaster) {	
		
		IDP_PerformanceRating performanceRating = 
				(IDP_PerformanceRating)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(IDP_PerformanceRating.class)
				.add(Restrictions.eq(YumPmsConstants.IDP_SHEET_MASTER, sheetMaster))
				.add(Restrictions.eq(YumPmsConstants.IDP_PERFORMANCE_CATEGORY_MASTER, catagoryMaster))
				.uniqueResult(); 
		return null == performanceRating ? new IDP_PerformanceRating() : performanceRating;
	}
	
	public IDP_PerformanceRating getIdpPerformanceRatingActionPendingBasis(IDP_SheetMaster sheetMaster) {	
		
		IDP_PerformanceRating performanceRating = 
				(IDP_PerformanceRating)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(IDP_PerformanceRating.class)
				.add(Restrictions.eq(YumPmsConstants.IDP_SHEET_MASTER, sheetMaster))
				.add(Restrictions.eq(YumPmsConstants.ACTION_PENDING, true))
				.uniqueResult(); 
		return null == performanceRating ? new IDP_PerformanceRating() : performanceRating;
	}
	
	/**
	 * @param IDP_SheetMaster sheetMaster
	 * @param IDP_PlanMaster planMaster
	 * @return IDP_PlanDetails planDetails
	 */
	public IDP_PlanDetails getIdpPlanDetails(IDP_SheetMaster sheetMaster, IDP_PlanMaster planMaster) {	
		
		IDP_PlanDetails planDetails = 
				(IDP_PlanDetails)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(IDP_PlanDetails.class)
				.add(Restrictions.eq(YumPmsConstants.IDP_SHEET_MASTER, sheetMaster))
				.add(Restrictions.eq(YumPmsConstants.IDP_PLAN_MASTER, planMaster))
				.uniqueResult(); 
		return null == planDetails ? new IDP_PlanDetails() : planDetails;
	}
	
	/**
	 * @param IDP_SheetMaster sheetMaster
	 * @return List<IDP_DevelopmentActionPlan> developmentActionList
	 */
	public List<IDP_DevelopmentActionPlan> getDevelopmentActonPlanList(IDP_SheetMaster sheetMaster) {	

		return
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(IDP_DevelopmentActionPlan.class)
				.add(Restrictions.eq(YumPmsConstants.IDP_SHEET_MASTER, sheetMaster))
				.add(Restrictions.eq(YumPmsConstants.IS_ACTIVE, true))
				.list(); 
	}
	
	/**
	 * @param IDP_SheetMaster sheetMaster
	 * @return List<IDP_BalanceYearGoal> developmentActionList
	 */
	public List<IDP_BalanceYearGoal> getBalanceYrGoalList(IDP_SheetMaster sheetMaster) {	
		
		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(IDP_BalanceYearGoal.class)
				.add(Restrictions.eq(YumPmsConstants.IDP_SHEET_MASTER, sheetMaster))
				.add(Restrictions.eq(YumPmsConstants.IS_ACTIVE, true))
				.list(); 
	}
	
	/**
	 * @param IDP_SheetMaster sheetMaster
	 * @return List<IDP_SectionDtl> sectionDtlList
	 */
	public List<IDP_SectionDtl> getSectionDtlList(IDP_SheetMaster sheetMaster) {	
		
		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(IDP_SectionDtl.class)
				.add(Restrictions.eq(YumPmsConstants.IDP_SHEET_MASTER, sheetMaster))
				.add(Restrictions.eq(YumPmsConstants.IS_ACTIVE, true))
				.list(); 
	}
	
	/**
	 * @param int goalSectionId
	 * @return Boolean
	 */
	public Boolean isExistOnPms(int goalSectionId) {	
		
		List<GoalSection> goalSectionList =
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(GoalSection.class)
				.add(Restrictions.eq(YumPmsConstants.GOAL_SECTION_ID, goalSectionId))
				.list(); 
		return  CollectionUtils.isEmpty(goalSectionList) ? false : true;
	}

	/**
	 * @param sheetMaster
	 * @return
	 */
	public Idp_PerformanceRatingReporting getRatingRepotDetails(IDP_SheetMaster sheetMaster) {	

		Idp_PerformanceRatingReporting ratingReporting  = 
				(Idp_PerformanceRatingReporting) YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(Idp_PerformanceRatingReporting.class)
				.add(Restrictions.eq(YumPmsConstants.IDP_SHEET_MASTER, sheetMaster))
				.add(Restrictions.eq(YumPmsConstants.IS_ACTIVE, true))
				.uniqueResult();
		return null != ratingReporting ? ratingReporting : new Idp_PerformanceRatingReporting();
	}
	
	/**
	 * @param value
	 * @return
	 */
	public IDP_SectionMaster getIDPSectionmaster(String value) {	
		
		return 
				(IDP_SectionMaster) YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(IDP_SectionMaster.class)
				.add(Restrictions.eq(YumPmsConstants.IDP_SECTION_DESC, value))
				.add(Restrictions.eq(YumPmsConstants.IS_ACTIVE, true))
				.uniqueResult(); 
	}
	
	/**
	 * @param int sectionMasterId
	 * @return IDP_SectionMaster idpSectionMaster
	 */
	public IDP_SectionMaster getIdpSectionMasterById(int sectionMasterId) {	
		
		IDP_SectionMaster idpSectionMaster = YumHibernateUtilServices.getSession(sessionFactory).get(IDP_SectionMaster.class, sectionMasterId);	
		return null != idpSectionMaster ? idpSectionMaster : new IDP_SectionMaster(); 
	}
	
	public IDP_Remarks getAllRemarks(IDP_SheetMaster sheetMaster) {	
		
		return 
				(IDP_Remarks)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(IDP_Remarks.class)
				.add(Restrictions.eq(YumPmsConstants.IDP_SHEET_MASTER, sheetMaster))
				.uniqueResult(); 
	}
//---------------------------------------------------------------------------------------------------------------------------	 	
	 /**
	 * @param IDP_SheetMaster idpSheetMaster
	 * @return IDP_SheetMaster idpSheetMaster
	 * @throws YumPMSDataSaveException
	 */
	public IDP_SheetMaster insertOrUpdateIdpSheetDetails(IDP_SheetMaster idpSheetMaster) throws YumPMSDataSaveException {
		
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			idpSheetMaster = (IDP_SheetMaster) currentSession.merge(idpSheetMaster);
			YumHibernateUtilServices.commitTransaction(currentSession);     
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to save or update IDP_SheetMaster", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return idpSheetMaster;				
	}

	 /**
	 * @param IDP_PlanDetails idpPlanDetails
	 * @return IDP_PlanDetails idpPlanDetails
	 * @throws YumPMSDataSaveException
	 */
	public IDP_PlanDetails insertOrUpdateIdpPlanDetails(IDP_PlanDetails idpPlanDetails) throws YumPMSDataSaveException {
		
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			idpPlanDetails = (IDP_PlanDetails) currentSession.merge(idpPlanDetails);
			YumHibernateUtilServices.commitTransaction(currentSession);    
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to save or update IDP_PlanDetails", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return idpPlanDetails;				
	}
	 
	 /**
	 * @param IDP_PerformanceRating idpPerformanceRating
	 * @return IDP_PerformanceRating idpPerformanceRating
	 * @throws YumPMSDataSaveException
	 */
	public IDP_PerformanceRating insertOrUpdateIdpPerformanceRating(IDP_PerformanceRating idpPerformanceRating) throws YumPMSDataSaveException {
		
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
		try {
			idpPerformanceRating = (IDP_PerformanceRating) currentSession.merge(idpPerformanceRating);
			YumHibernateUtilServices.commitTransaction(currentSession);   
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to save or update IDP_PerformanceRating", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return idpPerformanceRating;				
	}
	
	/**
	 * @param idpPerformanceRatingReporting
	 * @return
	 * @throws YumPMSDataSaveException
	 */
	public Idp_PerformanceRatingReporting insertOrUpdateIdpPerformanceRatingReporting(Idp_PerformanceRatingReporting idpPerformanceRatingReporting) 
			throws YumPMSDataSaveException {

		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			idpPerformanceRatingReporting = (Idp_PerformanceRatingReporting) currentSession.merge(idpPerformanceRatingReporting);
			YumHibernateUtilServices.commitTransaction(currentSession);  
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to save or update IDP_PerformanceRating", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return idpPerformanceRatingReporting;				
	}
	 
	 /**
	 * @param IDP_DevelopmentActionPlan idpDevelopmentActionPlan
	 * @return IDP_DevelopmentActionPlan
	 * @throws YumPMSDataSaveException
	 */
	public IDP_DevelopmentActionPlan insertOrUpdateIdpDevelopmentActionPlan(IDP_DevelopmentActionPlan idpDevelopmentActionPlan) 
			throws YumPMSDataSaveException {

		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);  
		try{
			idpDevelopmentActionPlan = (IDP_DevelopmentActionPlan) currentSession.merge(idpDevelopmentActionPlan);
			YumHibernateUtilServices.commitTransaction(currentSession); 
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to save or update IDP_DevelopmentActionPlan", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return idpDevelopmentActionPlan;				
	}
	
	/**
	 * @param IDP_BalanceYearGoal idpBalanceYearGoal
	 * @return IDP_BalanceYearGoal
	 * @throws YumPMSDataSaveException
	 */
	public IDP_BalanceYearGoal insertOrUpdateIdpBalanceYrGoals(IDP_BalanceYearGoal idpBalanceYearGoal) throws YumPMSDataSaveException {

		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
		try {
			idpBalanceYearGoal = (IDP_BalanceYearGoal) currentSession.merge(idpBalanceYearGoal);
			YumHibernateUtilServices.commitTransaction(currentSession);      
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException(YumPmsConstants.ERROR_FAIL_TO_UPDATE + YumPmsConstants.IDP_BALANCE_YEAR_GOAL, e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return idpBalanceYearGoal;				
	}
	
	/**
	 * @param IDP_SelectedSuggestedGoalDtl selectedSuggestedGoalResponse
	 * @return IDP_BalanceYearGoal
	 * @throws YumPMSDataSaveException
	 */
	public List<IDP_SelectedSuggestedGoalDtl> insertOrUpdateIdpSelectedSuggestedGoals(List<IDP_SelectedSuggestedGoalDtl> selectedSuggestedGoalResponse) throws YumPMSDataSaveException {

		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
		List<IDP_SelectedSuggestedGoalDtl> selectedSuggestedGoals = new ArrayList<>(selectedSuggestedGoalResponse.size());
		try {
			
			for(IDP_SelectedSuggestedGoalDtl selectedGoal : selectedSuggestedGoalResponse) {			
				selectedSuggestedGoals.add((IDP_SelectedSuggestedGoalDtl) currentSession.merge(selectedGoal));
			}
			
			YumHibernateUtilServices.commitTransaction(currentSession);      
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException(YumPmsConstants.ERROR_FAIL_TO_UPDATE + YumPmsConstants.IDP_SUGGESTED_GOAL_DETAIL, e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return selectedSuggestedGoals;				
	}
	
	/**
	 * @param IDP_SectionDtl sectionDetailDto
	 * @return IDP_SectionDtl sectionDetailDto
	 * @throws YumPMSDataSaveException
	 */
	public IDP_SectionDtl insertOrUpdateIdpSectionDetails(IDP_SectionDtl sectionDetailDto) throws YumPMSDataSaveException {

		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
		try {
			sectionDetailDto = (IDP_SectionDtl) currentSession.merge(sectionDetailDto);
			YumHibernateUtilServices.commitTransaction(currentSession);  
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException(YumPmsConstants.ERROR_FAIL_TO_UPDATE + YumPmsConstants.IDP_SECTION_DETAIL, e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return sectionDetailDto;				
	}
	
	/**
	 * @param ramarksDetails
	 * @return
	 * @throws YumPMSDataSaveException
	 */
	public IDP_Remarks insertOrUpdateIdpRemarksDetails(IDP_Remarks ramarksDetails) throws YumPMSDataSaveException {

		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
		try {
			ramarksDetails = (IDP_Remarks) currentSession.merge(ramarksDetails);
			YumHibernateUtilServices.commitTransaction(currentSession);     
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to save or update IDP_BalanceYearGoal", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return ramarksDetails;				
	}
	
	public List<IdpSuggestedGoal> getSuggestedGoals(Integer idpSheetId) {
		
		String sql = "select distinct T1.idp_suggested_goal_section_id, T1.idp_suggested_goal_section_desc, T1.idp_sub_sub_section_id, T3.idp_sub_sub_section_desc from idp_suggested_goal_mstr T1, idp_section_dtl T2, idp_sub_sub_section_mstr T3"
					+ " WHERE T1.idp_section_id = T2.idp_section_id" 
					+ " AND T1.idp_sub_section_id = T2.idp_sub_section_id"
					+ " AND T1.idp_sub_sub_section_id = T2.idp_sub_sub_section_id"
					+ " AND T2.idp_sub_sub_section_id = T3.idp_sub_sub_section_id"
					+ " AND T1.idp_sub_sub_section_id = T3.idp_sub_sub_section_id"	
					+ " AND T2.idp_sheet_id = :idpSheetId"
					+ " AND T2.is_active = 1"
					+ " AND T2.idp_section_type_flag = 'effectiveness'" 
					+ " AND T2.idp_sub_sub_section_id > 0";
		List<Object[]> result = YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(sql)
				.setInteger("idpSheetId", idpSheetId)
				.list(); 
		if(CollectionUtils.isEmpty(result)) {
			LOGGER.info("Got empty result set");
			return Collections.emptyList();
		}
		List<IdpSuggestedGoal> suggestedGoals = new ArrayList<>(result.size());
		for(Object[] tuple : result) {
			IdpSuggestedGoal suggestedGoal = new IdpSuggestedGoal();
			suggestedGoal.setIdpSuggestedGoalSectionId(YumPmsUtils.castToInt(tuple[0]));
			suggestedGoal.setIdpSuggestedGoalSectionDesc(YumPmsUtils.toString(tuple[1]));
			suggestedGoal.setIdpSubSubSectionId(YumPmsUtils.castToInt(tuple[2]));
			suggestedGoal.setIdpSubSubSectionDesc(YumPmsUtils.toString(tuple[3]));
			suggestedGoals.add(suggestedGoal);
		}
		return suggestedGoals;
	}
	
	//-------------------------------------------------------------------------------------------------------------------------------------
	public List<EmailDomain> getMailDetailsFromSP(Integer ltId, String sendingTo, String empList, Integer periodId) {
		
		List<Object[]> results = null;
		Session currentSession = YumHibernateUtilServices.getSession(sessionFactory); 
		if(sendingTo.equalsIgnoreCase(YumPmsConstants.ADMIN)) {
			if(util.isActiveSp(sessionFactory, "sp_EmailNotification_IDP_PPRRating_To_Admin")) {
				results = 
						currentSession.createSQLQuery("exec sp_EmailNotification_IDP_PPRRating_To_Admin :param1")
						.setInteger(YumPmsConstants.PARAM_1, ltId)
						.list();
			}
		} else if(sendingTo.equalsIgnoreCase(YumPmsConstants.SUPERVISOR)) {
			if(util.isActiveSp(sessionFactory, "sp_EmailNotification_IDP_PPRRating_To_Sup")) {
				results = 
						currentSession.createSQLQuery("exec sp_EmailNotification_IDP_PPRRating_To_Sup :param1, :param2, :param3")
						.setInteger(YumPmsConstants.PARAM_1, periodId)
						.setInteger(YumPmsConstants.PARAM_2, ltId)
						.setString(YumPmsConstants.PARAM_3, empList)
						.list();
			}
		} else if(sendingTo.equalsIgnoreCase(YumPmsConstants.LT)) {
			if(util.isActiveSp(sessionFactory, "sp_EmailNotification_IDP_PPRRating_To_LT")) {
				results = 
						currentSession.createSQLQuery("exec sp_EmailNotification_IDP_PPRRating_To_LT :param1, :param2, :param3")
						.setInteger(YumPmsConstants.PARAM_1, periodId)
						.setInteger(YumPmsConstants.PARAM_2, ltId)
						.setString(YumPmsConstants.PARAM_3, empList)
						.list();
			}
		}
		List<EmailDomain> domainList = new ArrayList<>();
		if(null != results) {
			for (Object[] tuples : results) {
				EmailDomain emailDomain = new EmailDomain();
				emailDomain.setToMailId(YumPmsUtils.toStringorNull(tuples[0]));
				emailDomain.setCcMailId(YumPmsUtils.toStringorNull(tuples[1]));
				emailDomain.setSubject(YumPmsUtils.toStringorNull(tuples[2]));
				emailDomain.setBody(YumPmsUtils.toStringorNull(tuples[3]));
				domainList.add(emailDomain);
			}
		}
		currentSession.flush();
		currentSession.close();	
		if(results != null) {
			results.clear();	
		}
		return domainList;
	}
	
	public Map<String, String> empidToEmployeeDetails(Integer empId, Integer appraisalId) {

		List<Object[]> resultSet = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(String.format("exec sp_logic_EmpDetails :%s, :%s", YumPmsConstants.PARAM_1, YumPmsConstants.PARAM_2))
				.setInteger(YumPmsConstants.PARAM_1, empId)
				.setInteger(YumPmsConstants.PARAM_2, appraisalId)
				.list();
		Map<String, String> empDetailsMap = new HashMap<>();	
		for (Object[] tuples : resultSet) {
			empDetailsMap.put(YumPmsConstants.EMP_ID, YumPmsUtils.toStringorNull(tuples[0]));
			empDetailsMap.put(YumPmsConstants.EMP_NAME, YumPmsUtils.toStringorNull(tuples[1]));
		}
		return empDetailsMap;
	}

}
