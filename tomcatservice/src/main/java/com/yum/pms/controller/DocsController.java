package com.yum.pms.controller;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yum.comp.domain.CompensationLetter;
import com.yum.comp.domain.EmailDownload;
import com.yum.comp.service.CompensationService;
import com.yum.idp.domain.IdpUIConfig;
import com.yum.idp.service.IIDPService;
import com.yum.pa.domain.PASheetData;
import com.yum.pa.service.IPAService;
import com.yum.pms.dao.AppraisalPeriod;
import com.yum.pms.dao.CompLetterStatus;
import com.yum.pms.dao.EmployeeNew;
import com.yum.pms.dao.GlobalLetter;
import com.yum.pms.dao.ReferenceDoc;
import com.yum.pms.domain.EmployeeGoalSheet;
import com.yum.pms.exception.CommonException;
import com.yum.pms.service.IYumHibernateService;
import com.yum.pms.utils.ResourceUtils;
import com.yum.pms.utils.YumPmsConstants;
import com.yum.pms.utils.YumPmsUtils;

@CrossOrigin(origins = YumPmsConstants.ANGULAR_DEV_URL)
@Controller
public class DocsController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DocsController.class);
	
	@Autowired
	private IYumHibernateService service;
	
	@Autowired
	private IIDPService idpService;
	
	@Autowired
	private IPAService paService;
	
	@Autowired
	private CompensationService compService;
	
	// PDF Creation And Download Content = Goal-Sheet---------------------------------	
	@RequestMapping(value = "/getPDFGoalSheet", method = RequestMethod.GET)
	public void getPDFGoalSheet(HttpServletResponse response, 
			@RequestParam(YumPmsConstants.PARAM_EMP_ID) Integer empId, 
			@RequestParam(YumPmsConstants.PARAM_APPRAISAL_ID) Integer appraisalId) {
		try {
			EmployeeGoalSheet employeeGoalSheet =  service.getGoalSheetDetailsByEmployeeId(empId,appraisalId, true);
			String fileName = YumPmsUtils.getPdfFileName(employeeGoalSheet.getEmpName(), employeeGoalSheet.getPeriodDesc(), 
					YumPmsConstants.GOAL_SHEET_PDF_FILE);
			addHeaderForPdfDownload(response, fileName);
			writeInToHttpResponse(response, YumPmsUtils.getGoalSheetReportData(employeeGoalSheet));
		}catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			throw new CommonException(ex.getMessage());
		}
	}
	
	// PDF Creation And Download Content = IDP-Sheet---------------------------------	
	@RequestMapping(value = "/getIDPSheetReport", method = RequestMethod.GET)
	public void getPDFIdpSheet(HttpServletResponse response, 
			@RequestParam(YumPmsConstants.PARAM_EMP_ID) Integer empId, 
			@RequestParam(YumPmsConstants.PARAM_APPR_ID) Integer appraisalId) {
		try {
			IdpUIConfig idpUIConfig = idpService.getIdpUi(empId,appraisalId, false);
			String fileName = YumPmsUtils.getPdfFileName(idpUIConfig.getEmpName(), idpUIConfig.getAppraisalDescription(), 
					YumPmsConstants.IDP_SHEET_PDF_FILE);
			addHeaderForPdfDownload(response, fileName);
			writeInToHttpResponse(response, YumPmsUtils.getIDPSheetReportData(idpUIConfig));
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			throw new CommonException(ex.getMessage());
		}
	}
	
	// PDF Creation And Download Content = PA-Sheet---------------------------------	
	@RequestMapping(value = "/getPaSheetReport", method = RequestMethod.GET)
	public void getPDFPaSheet(
			HttpServletResponse response, 
			@RequestParam(YumPmsConstants.EMP_ID) Integer empId, 
			@RequestParam(YumPmsConstants.APPR_ID) Integer appraisalId,
			@RequestParam(YumPmsConstants.ROLE) String role) {
		try {
			List<PASheetData> paSheetDataList = paService.getPASheetData(empId, appraisalId, role);
			if(CollectionUtils.isNotEmpty(paSheetDataList)) {

				PASheetData sheetData = paSheetDataList.get(0);
				if(sheetData != null && YumPmsUtils.isNotNullAndNonZero(sheetData.getPaSheetId())) {
					String fileName = YumPmsUtils.getPdfFileName(sheetData.getEmpName(), sheetData.getApprDesc(), YumPmsConstants.PA_SHEET_PDF_FILE);
					addHeaderForPdfDownload(response, fileName);
					writeInToHttpResponse(response, YumPmsUtils.getPaSheetReportData(sheetData));
				}
			}
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			throw new CommonException(ex.getMessage());
		}
	}
	
	@RequestMapping(value = "/downloadOrAcceptCompLetter", method = RequestMethod.GET)
	public void downloadOrAcceptCompLetter(HttpServletResponse response, 
			@RequestParam(YumPmsConstants.EMP_ID) Integer empId, 
			@RequestParam(YumPmsConstants.APPR_ID) Integer apprId, 
			@RequestParam(YumPmsConstants.SALARY_TYPE) String salaryType,
			@RequestParam(value = YumPmsConstants.COMP_LETTER_ACCEPT, required = false) Boolean compLetterAccept) {
		
		LOGGER.info("getCompensationLetter: params are: empId = {}, apprId = {}, salaryType = {} and compLetterAcceptance = {}", 
				empId, apprId, salaryType, compLetterAccept);
		try {
			Map<String, byte[]> map = compService.downloadOrAcceptCompLetter(empId, apprId, salaryType, BooleanUtils.toBoolean(compLetterAccept));
			if(MapUtils.isNotEmpty(map)) {
				Entry<String, byte[]> entry = map.entrySet().iterator().next();
				addHeaderForPdfDownload(response, entry.getKey());	
				writeInToHttpResponse(response, entry.getValue());
			} else {
				throw new CommonException("The result map must content one entry");
			}
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			throw new CommonException(ex.getMessage());
		}
	}
	
	@RequestMapping(value = "/getCompensationLetter", method = RequestMethod.GET)
	public void getCompensationLetter(HttpServletResponse response, 
			@RequestParam(YumPmsConstants.EMP_ID) Integer empId, 
			@RequestParam(YumPmsConstants.APPR_ID) Integer apprId,
			@RequestParam(YumPmsConstants.SALARY_TYPE) String salaryType) {
		
		LOGGER.info("getCompensationLetter: params are: apprid = {} and empid = {}", apprId, empId);
		try {
			CompensationLetter letter = compService.getCompensationLetter(empId, apprId, salaryType, true);
			byte[] content = YumPmsUtils.getCompensationLetterContent(letter);
			String fileName = YumPmsUtils.getCompLetterFileName(letter.getEmpNme(), letter.getApprYearDesc(), salaryType);
			
			addHeaderForPdfDownload(response, fileName);
			writeInToHttpResponse(response, content);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			throw new CommonException(ex.getMessage());
		}
	}
	
	@RequestMapping(value = "/getCompLettersForDownload", method = RequestMethod.GET)
	public ResponseEntity<List<CompLetterStatus>> getCompLettersForDownload(@RequestParam(YumPmsConstants.EMP_ID) Integer empId, 
			@RequestParam(YumPmsConstants.APPR_ID) Integer apprId) {
		
		LOGGER.info("getCompLettersForDownload: params are: apprid = {} and empid = {}", apprId, empId);
		try {
			return new ResponseEntity<>(compService.getCompLettersForDownload(empId, apprId), HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(Collections.emptyList(), HttpStatus.INTERNAL_SERVER_ERROR);	
		}
	}
	
	//  ============================================File Handling ===================================================
	
	/**
	 * Used to get all global letter details in a particular appraisal period id
	 * @param apprId
	 * @param empId
	 * @return List<GlobalLetter>
	 */
	@RequestMapping(value = "/getGlobalLetterDetails", method = RequestMethod.GET)
	public ResponseEntity<List<GlobalLetter>> getGlobalLetterDetails(@RequestParam(YumPmsConstants.APPR_ID) Integer apprId, 
			@RequestParam(value = YumPmsConstants.EMP_ID, required = false) Integer empId) {

		try {
			return new ResponseEntity<>(service.getGlobalLetterDetails(apprId, empId), HttpStatus.OK);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(Collections.emptyList(), HttpStatus.INTERNAL_SERVER_ERROR);	
		}
	}
	
	/**
	 * upload a Global Letter into system
	 * @param uploadFile
	 * @param apprId
	 * @param empId
	 * @param fileDesc
	 * @return GlobalLetter
	 */
	@RequestMapping(value = "/uploadGlobalLetter", method = RequestMethod.POST, consumes = "multipart/form-data")
	public ResponseEntity<GlobalLetter> uploadGlobalLetter(@RequestParam(YumPmsConstants.PARAM_UPLOADFILE) MultipartFile uploadFile, 
			@RequestParam(YumPmsConstants.GLOBAL_LETTER_JSON) String globalLetterJson) {

		try {
			GlobalLetter globalLetter = new ObjectMapper().readValue(globalLetterJson, GlobalLetter.class);
			globalLetter.setEmployee(new EmployeeNew(globalLetter.getEmpId()));
			globalLetter.setAppraisalPeriod(new AppraisalPeriod(globalLetter.getApprId()));
			globalLetter = service.uploadGlobalLetter(uploadFile, globalLetter);
			return new ResponseEntity<>(globalLetter, HttpStatus.OK);
		} catch(Exception ex) {
			GlobalLetter globalLetter = new GlobalLetter();
			globalLetter.setStatusMsg("Sorry, Failed to save File. Try Again"); 
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(globalLetter, HttpStatus.INTERNAL_SERVER_ERROR);	
		}
	}
	
	/**
	 * delete a Global Letter by it's ID
	 * @param globalLetterId
	 * @return a status message
	 */
	@RequestMapping(value = "/deleteGlobalLetter", method = RequestMethod.GET)
	public ResponseEntity<Properties> deleteGlobalLetterDetails(@RequestParam(YumPmsConstants.GLOBAL_LETTER_ID) Integer globalLetterId) {

		Properties prop = new Properties();
		try {
			service.deleteGlobalLetterDetails(globalLetterId);
			prop.setProperty(YumPmsConstants.STATUS_MSG, YumPmsConstants.SUCCESS);
			return new ResponseEntity<>(prop, HttpStatus.OK);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			prop.setProperty(YumPmsConstants.STATUS_MSG, ex.getMessage());
			return new ResponseEntity<>(prop, HttpStatus.INTERNAL_SERVER_ERROR);	
		}
	}
	
	/**
	 * download Global Letter by it's ID
	 * @param globalLetterId
	 * @param response
	 */
	@RequestMapping(value = "/downloadGlobalLetter", method = RequestMethod.GET)
	public void downloadGlobalLetter(@RequestParam(YumPmsConstants.GLOBAL_LETTER_ID) Integer globalLetterId, HttpServletResponse response) {

		LOGGER.info("downloadGlobalLetter - parameter - globalLetterId : {}", globalLetterId);
		try {
			Map<String, byte[]> map = service.downloadGlobalLetter(globalLetterId);
			addHeaderForPdfDownload(response, map.entrySet().iterator().next().getKey());	
			writeInToHttpResponse(response, map.entrySet().iterator().next().getValue());
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			throw new CommonException(ex.getMessage());
		}
	}
	
	@RequestMapping(value = "/fileupload", method = RequestMethod.POST, consumes = "multipart/form-data", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ReferenceDoc> fileUpload(@RequestParam(YumPmsConstants.PARAM_UPLOADFILE) MultipartFile uploadFile, 
			@RequestParam(YumPmsConstants.PARAM_APPRAISAL_ID) Integer appraisalPeriodId, 
			@RequestParam(YumPmsConstants.PARAM_EMP_ID) Integer empId, 
			@RequestParam(YumPmsConstants.PARAM_FILEDESC) String fileDesc, 
			@RequestParam(YumPmsConstants.PARAM_IS_ADMIN) Boolean isAdminDoc){
		
		ReferenceDoc referenceDoc = new ReferenceDoc();
		try {
			if (uploadFile.isEmpty()) {
	            return new ResponseEntity<>(referenceDoc, HttpStatus.OK);
	        } else {
	        	referenceDoc.setAppraisalPeriodId(appraisalPeriodId);
	        	referenceDoc.setEmpId(empId);
	        	referenceDoc.setFileDesc(fileDesc);
	        	referenceDoc.setIsAdminDoc(BooleanUtils.toBoolean(isAdminDoc));	        	
	        	referenceDoc = service.fileUpload(uploadFile, referenceDoc);
	        }
		} catch(Exception ex) {
			referenceDoc.setOperationMassage("Sorry, Failed to save File. Try Again"); 
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(referenceDoc, HttpStatus.INTERNAL_SERVER_ERROR);	
		}
		return new ResponseEntity<>(referenceDoc, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getfiledetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ReferenceDoc>> getFilesDetails(@RequestParam (YumPmsConstants.PARAM_APPRAISAL_ID)Integer appraisalPeriodId, 
			@RequestParam(YumPmsConstants.PARAM_EMP_ID) Integer empId, 
			@RequestParam(YumPmsConstants.PARAM_IS_ADMIN) Boolean isAdminDoc) {
		
		LOGGER.info("Param Details>>>>>>>...:{} - {} - {}", appraisalPeriodId, empId, isAdminDoc);
		ReferenceDoc referenceDoc = new ReferenceDoc();
		List<ReferenceDoc> fileDetailsList = Collections.emptyList();
		try {
			referenceDoc.setAppraisalPeriodId(appraisalPeriodId);
			referenceDoc.setEmpId(empId);
			referenceDoc.setIsAdminDoc(BooleanUtils.toBoolean(isAdminDoc));
			fileDetailsList = service.getfilesdetailsList(referenceDoc);
			return new ResponseEntity<>(fileDetailsList, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(fileDetailsList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/filedownload", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> fileDownload(@RequestParam(YumPmsConstants.PARAM_FILEID) String fileId, 
			HttpServletRequest request, HttpServletResponse response) {
		
		LOGGER.info("File Id>>>>>>>...:{}", fileId );
		String msg = StringUtils.EMPTY;
		try {
			msg = service.fileDownload(fileId, request, response);			
			return new ResponseEntity<>(msg, HttpStatus.OK);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(msg, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	@RequestMapping(value = "/deleteFileById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Properties> deleteFileById(@RequestParam(YumPmsConstants.PARAM_FILEID) String id, 
			@RequestParam(YumPmsConstants.TABLE_NAME) String tableName) {
		
		LOGGER.info("Param is>>>>..deletable id:{}; deletable tableName: {}", id, tableName);  
		Properties prop = new Properties();
		try { 
			service.deleteById(id, tableName);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			prop.put(YumPmsConstants.MSG, StringUtils.EMPTY);
			return new ResponseEntity<>(prop, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(prop, HttpStatus.OK);
	}
	
	/**
	 * This method add HTTP headers in HttpServeltResponse object for PDF download 
	 * @param response
	 * @param fileName
	 */
	private void addHeaderForPdfDownload(HttpServletResponse response, String fileName) {
		
		response.addHeader(YumPmsConstants.HTTP_HEADER_CONTENT_TYPE, YumPmsConstants.HTTP_HEADER_VALUE_FORCE_DOWNLOAD);
		response.addHeader(YumPmsConstants.HTTP_HEADER_CONTENT_DISPOSITION, YumPmsConstants.HTTP_HEADER_ATTACHMENT + fileName);
	}
	
	private void writeInToHttpResponse(HttpServletResponse response, byte[] bytes) throws IOException {
		response.getOutputStream().write(bytes);
	}
	
//------------------------------------------------bulk email---------------------------------------------	
	/**
	 * @param response
	 * @param empId
	 * @param apprId
	 * @param isEmailSend
	 */
	@RequestMapping(value = "/getCompensationLetterPdf", method = RequestMethod.POST)
	public void getCompensationLetterPdfNew(HttpServletResponse response, @RequestBody EmailDownload emailDownload) {
		String empIds = emailDownload.getEmpIds();
		String purpose = emailDownload.getPurpose();
		Integer apprId = emailDownload.getApprId();
		Boolean isEmailSend = emailDownload.getIsEmail();
		
		LOGGER.info("getCompensationLetter: params are: apprid = {} and empid = {}", apprId, empIds);
		try {
			List<CompensationLetter> letterList = compService.getCompensationLetterNew(empIds, apprId, purpose);
			if(BooleanUtils.toBoolean(isEmailSend)) {								
				Map<Integer, List<CompensationLetter>> mapBySupervisorId = letterList.stream().collect(Collectors.groupingBy(CompensationLetter::getSupervisorId));   
				Map<Integer, String> mapByAttachedmentWithSupervisor = new HashMap<>();
				StringBuilder supEmpIds = new StringBuilder(StringUtils.EMPTY); 
				int counter=0;
				for(Map.Entry<Integer, List<CompensationLetter>> entry : mapBySupervisorId.entrySet()) {
					Map<String, byte[]> zipFilesMap = new HashMap<>();
					for(CompensationLetter letter : entry.getValue()) {
						byte[] content = YumPmsUtils.getCompensationLetterContent(letter);
						String fileName = YumPmsUtils.getCompLetterFileName(letter.getEmpNme(), letter.getApprYearDesc(), purpose);
						zipFilesMap.put(fileName, content);
					}
					String zipFilesName = YumPmsUtils.getPdfFileName(entry.getKey().toString(), entry.getValue().get(0).getApprYearDesc(), YumPmsConstants.PA_ZIP_FILE);
					mapByAttachedmentWithSupervisor.put(entry.getKey(), ResourceUtils.createZip(zipFilesName, zipFilesMap));
					supEmpIds = supEmpIds.append(entry.getKey().toString()); 
					if(++counter != mapBySupervisorId.entrySet().size())
						supEmpIds.append(YumPmsConstants.COMMA);	
				}
				compService.sendEmailWithCompensationLetter(empIds, supEmpIds.toString(), apprId, mapByAttachedmentWithSupervisor); 
				compService.updateCompLetterEmailStatus(empIds, apprId, purpose);
			} else {
				CompensationLetter compLetter = letterList.get(0);
				byte[] content = YumPmsUtils.getCompensationLetterContent(compLetter);
				if(content.length >0) {
					String fileName = YumPmsUtils.getCompLetterFileName(compLetter.getEmpNme(), compLetter.getApprYearDesc(), purpose);
					addHeaderForPdfDownload(response, fileName);	
					writeInToHttpResponse(response, content);
				}
			}
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			throw new CommonException(ex.getMessage());
		}
	}
	
	/**
	 * @param empIds with comma separated
	 * @param apprId
	 * @param emailPermission Map
	 */
	@RequestMapping(value = "/saveCompLetterDownloadPermission", method = RequestMethod.POST)
	public void saveCompLetterDownloadPermission(HttpServletResponse response, @RequestBody EmailDownload emailDownload) {
		
		String empIds = emailDownload.getEmpIds();
		Integer apprId = emailDownload.getApprId();
		String purpose = emailDownload.getPurpose();
		LOGGER.info("saveCompLetterDownloadPermission: params are: apprid = {} and empid = {}", apprId, empIds);
		try {
			if(StringUtils.isEmpty(empIds) || YumPmsUtils.isLessThanZero(apprId) || MapUtils.isEmpty(emailDownload.getPermissions())) {
				throw new CommonException("Invalid Request Data");
			}
			compService.updateCompLetterDownloadPermission(empIds, emailDownload.getPermissions(), apprId, purpose); 
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			throw new CommonException(ex.getMessage());
		}
	}
	
	/**
	 * @param empIds with comma separated
	 * @param apprId
	 * @param emailPermission Map
	 */
	@RequestMapping(value = "/bulkCompLetterDownload", method = RequestMethod.GET)
	public void bulkCompLetterDownload(HttpServletResponse response, 
			@RequestParam(YumPmsConstants.EMP_ID) String empIds, 
			@RequestParam(YumPmsConstants.APPR_ID) Integer apprId,
			@RequestParam(YumPmsConstants.SALARY_TYPE) String salaryType) {
		
		LOGGER.info("bulkCompLetterDownload: params are: apprid = {} and empids = {}", apprId, empIds);
		try {
			byte[] content = compService.bulkDownloadCompensationLetter(empIds, apprId, salaryType);
			String zipFilesName = YumPmsUtils.getPdfFileName("Admin_Download", salaryType, YumPmsConstants.PA_ZIP_FILE);
			addHeaderForPdfDownload(response, zipFilesName);	
			writeInToHttpResponse(response, content);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			throw new CommonException(ex.getMessage());
		}
	}

}
