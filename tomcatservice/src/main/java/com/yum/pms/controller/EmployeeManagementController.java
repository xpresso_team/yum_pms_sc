package com.yum.pms.controller;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yum.idp.domain.IdpSuggestedGoal;
import com.yum.pms.dao.EmployeeHelpTipViewStatus;
import com.yum.pms.domain.EmployeeCurrentDetails;
import com.yum.pms.service.EmployeeManagementService;
import com.yum.pms.service.IYumHibernateService;
import com.yum.pms.utils.YumPmsConstants;

@CrossOrigin(origins =YumPmsConstants.ANGULAR_DEV_URL)
@Controller
public class EmployeeManagementController {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeManagementController.class);

	@Autowired
	private EmployeeManagementService service;
	@Autowired
	private IYumHibernateService yumService;

	@RequestMapping(value = "/getdropdownlistForEmployeeManagement", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String,Object>> getdropdownlist(@RequestParam(value = YumPmsConstants.APPR_ID, required = false) Integer apprId) {

		LOGGER.info("get DropDown List For {}", "Employee Management Tab");   
		Map<String, Object> dropDownBox = null;
		try { 
			dropDownBox = service.getDropDownList(apprId);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(dropDownBox, HttpStatus.INTERNAL_SERVER_ERROR);			
		}
		return new ResponseEntity<>(dropDownBox, HttpStatus.OK);		
	}

	@RequestMapping(value = "/addOrEditEmployee", method = RequestMethod.POST, consumes = "multipart/form-data")
	public ResponseEntity<EmployeeCurrentDetails> addOrEditEmployee(@RequestParam(YumPmsConstants.EMPLOYEE_JSON) String empJson,
			@RequestParam(value = YumPmsConstants.SIGNATURE_IMAGE, required = false) MultipartFile signatureImage) {
		
		EmployeeCurrentDetails employee = null;
		try {
			employee = new ObjectMapper().readValue(empJson, EmployeeCurrentDetails.class);
			employee.setSignatureImage(signatureImage);
			LOGGER.info("Start addOrEditEmployee For {}", "Employee Management Tab");
			employee =  0 != employee.getEmpId() ? service.saveOrUpdateEmployeeDetails(employee) : employee;
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(employee, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}

	@RequestMapping(value = "/deleteEmployee", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EmployeeCurrentDetails> deleteById(@RequestBody EmployeeCurrentDetails employee) {

		LOGGER.info("Param is>>>>..deletable id: {}", employee != null ? employee.getEmpId() : 0);  
		try { 
			employee =  service.deleteEmployee(employee);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			if(employee != null) {
				employee.setOperationFlag(false);
			}
			return new ResponseEntity<>(employee, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}

	@RequestMapping(value = "/getemployeedetailsWithEmpId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EmployeeCurrentDetails> getemployeedetailsWithEmpId(@RequestParam(YumPmsConstants.EMP_ID) String empId,
			@RequestParam(value = YumPmsConstants.APPR_ID, required = false) Integer apprId) {  

		EmployeeCurrentDetails employee = null;
		try { 
			LOGGER.info("userid is>>>>.. {}", empId); 
			employee = yumService.getEmployeeDetailsByUserId(empId, apprId, "EMPLOYEE_ID");
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(employee, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(employee, HttpStatus.OK);		
	}
	
	@RequestMapping(value = "/updateEmployeeHelpViewStatus", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EmployeeHelpTipViewStatus> updateEmployeeHelpViewStatus(@RequestBody EmployeeHelpTipViewStatus employeeHelpTipViewData) {  
		EmployeeHelpTipViewStatus employeeHelpTipViewModel = null;
		try { 
			employeeHelpTipViewModel = service.addOrUpdateEmployeeViewRecord(employeeHelpTipViewData);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex); 
			return new ResponseEntity<>(employeeHelpTipViewModel, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(employeeHelpTipViewModel, HttpStatus.OK);
	}

}
