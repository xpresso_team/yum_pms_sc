/******************************************************************** 
 * Spring Controller for Auth Module 
 * 
 * http://www.abzooba.com
 * ------------------- 
 * Created By: keshav.kumar
 * Last Updated By: sudipta.chandra
 * Last Updated On: 23-Sep-2017 (11:34:34 AM) 
 *
 * The software and related user documentation are protected under 
 * copyright laws and remain the sole property of Abzooba. 
 * 
 * Technical support is available via the Abzooba website at 
 * http://www.abzooba.com
 **************************************************************************/

package com.yum.pms.controller;

import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.yum.idp.service.IIDPService;
import com.yum.pms.service.IYumHibernateService;
import com.yum.pms.utils.DbConnection;
import com.yum.pms.utils.YumPmsConstants;

@CrossOrigin(origins = YumPmsConstants.ANGULAR_DEV_URL)
@Controller
public class AuthController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

	@Autowired
	private IYumHibernateService service;

	@Autowired
	private IIDPService idpService;

	@RequestMapping(value = "/setForceUpdatedPass", method = RequestMethod.POST)
	public String updatePass(@RequestParam(value="password", required=true) String password , Model model) {
		//remove later
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String userEmail = auth.getName(); //get emailid of the logged in user

		if(updatePass(userEmail, password, YumPmsConstants.RESET)) {
			model.addAttribute(YumPmsConstants.PASS_CHANGED, YumPmsConstants.TRUE);
			SecurityContextHolder.clearContext();
			return "loginnew";
		} else {
			model.addAttribute(YumPmsConstants.FAILURE, YumPmsConstants.TRUE);
			return "forceUpdatePassword";
		}
	}


	@RequestMapping(value = "/setForgotPass", method = RequestMethod.POST)
	public String forgotPass(@RequestParam(value="email", required=true) String email , Model model) {

		//remove later
		LOGGER.info("Inside forgot password , email id is {}", email);
		char[] possibleCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?".toCharArray();
		String newPass = RandomStringUtils.random( 15, 0, possibleCharacters.length - 1, false, false, possibleCharacters, new SecureRandom());
		if(checkEmail(email)) {
			if(updatePass(email,newPass,"forgot")) {
				model.addAttribute("passSuccess", YumPmsConstants.TRUE);
				return "loginnew";	
			} else {
				model.addAttribute(YumPmsConstants.FAILURE, YumPmsConstants.TRUE);
				return "forgotPassword";
			}
		} else {
			model.addAttribute("failureNoEmail", YumPmsConstants.TRUE);
			return "forgotPassword";
		}
	}

	@RequestMapping(value = "/setChangedPass", method = RequestMethod.POST)
	public String changePass(@RequestParam(value = YumPmsConstants.SQL_PARAM_PASS, required = true) String password , String currentPassword, Model model) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String userEmail = auth.getName(); //get emailid of the logged in user

		if(checkPassword(userEmail, currentPassword)) {
			if(updatePass(userEmail, password, YumPmsConstants.RESET)) {
				model.addAttribute(YumPmsConstants.PASS_CHANGED, YumPmsConstants.TRUE);
				return YumPmsConstants.CHANGE_PASS;
			} else {
				model.addAttribute(YumPmsConstants.FAILURE, YumPmsConstants.TRUE);
				return YumPmsConstants.CHANGE_PASS;
			}
		} else {
			//password do not match
			LOGGER.info("Current password doesnt match ... for userEmail = {}", userEmail);
			model.addAttribute("failurePassMisMatch", YumPmsConstants.TRUE);
			return YumPmsConstants.CHANGE_PASS;	
		}
	}

	//	=====================================This Services Used By Batch Files============================================

	@RequestMapping(value = "/sendMailForNewJoinee", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> sendMailForNewJoinee() {

		LOGGER.info(YumPmsConstants.MAIL_PROCESSED_FOR, YumPmsConstants.NEW_JOINEE);
		try {
			service.sendMail(YumPmsConstants.NEW_JOINEE);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(YumPmsConstants.ERROR_EMAIL_CAN_NOT_SEND, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(YumPmsConstants.MAIL_SEND_SUCCESSFULLY, HttpStatus.OK);
	}

	@RequestMapping(value = "/sendMailForExistingJoinee", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> sendMailForExistingJoinee() {

		LOGGER.info(YumPmsConstants.MAIL_PROCESSED_FOR, YumPmsConstants.EXISTING_JOINEE);
		try {
			service.sendMail(YumPmsConstants.EXISTING_JOINEE);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(YumPmsConstants.ERROR_EMAIL_CAN_NOT_SEND, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(YumPmsConstants.MAIL_SEND_SUCCESSFULLY, HttpStatus.OK);
	}

	@RequestMapping(value = "/sendMailToManagerForGoalSheetSubmission", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> sendMailToManagerForGoalSheetSubmission() {

		LOGGER.info(YumPmsConstants.MAIL_PROCESSED_FOR, YumPmsConstants.GOAL_SHEET_SUBMIT);
		try {
			service.sendMail(YumPmsConstants.GOAL_SHEET_SUBMIT);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(YumPmsConstants.ERROR_EMAIL_CAN_NOT_SEND, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(YumPmsConstants.MAIL_SEND_SUCCESSFULLY, HttpStatus.OK);
	}	

	@RequestMapping(value = "/sendMailForLT", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> sendMailForLT() {

		LOGGER.info(YumPmsConstants.MAIL_PROCESSED_FOR, YumPmsConstants.LT_SUMMARIZED);
		try {
			service.sendMail(YumPmsConstants.LT_SUMMARIZED);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(YumPmsConstants.ERROR_EMAIL_CAN_NOT_SEND, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(YumPmsConstants.MAIL_SEND_SUCCESSFULLY, HttpStatus.OK);
	}

	@RequestMapping(value = "/sendMailForLTDetail", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> sendMailForLTDetail() {

		LOGGER.info(YumPmsConstants.MAIL_PROCESSED_FOR, YumPmsConstants.LT_DETAIL);
		try {
			service.sendMail(YumPmsConstants.LT_DETAIL);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(YumPmsConstants.ERROR_EMAIL_CAN_NOT_SEND, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(YumPmsConstants.MAIL_SEND_SUCCESSFULLY, HttpStatus.OK);
	}

	@RequestMapping(value = "/sendMailToDiscussedAndAgreedMonthlyQuarterly", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> sendMailToDiscussedAndAgreedMonthlyQuarterly() {

		LOGGER.info(YumPmsConstants.MAIL_PROCESSED_FOR, YumPmsConstants.DISCUSSED_AND_AGREED_MONTHLY_QUARTERLY);
		try {
			service.sendMail(YumPmsConstants.DISCUSSED_AND_AGREED_MONTHLY_QUARTERLY);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(YumPmsConstants.ERROR_EMAIL_CAN_NOT_SEND, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(YumPmsConstants.MAIL_SEND_SUCCESSFULLY, HttpStatus.OK);
	}

	//  Execute this service while LT give approve or reject of IDP Rating from mail.
	@RequestMapping(value = "/approveOrRejectionFromMailByLt", method = RequestMethod.GET)
	public ModelAndView approveOrRejectionFromMailByLt(
			@RequestParam(YumPmsConstants.PARAM_EMP_ID) Integer empId, 
			@RequestParam(YumPmsConstants.PARAM_APPR_ID) Integer appraisalId, 
			@RequestParam(YumPmsConstants.PARAM_RATING) String rating) {

		ModelAndView model = new ModelAndView("NewFile");
		try {
			String msg = idpService.updateRatingThroughMailByLt(empId, appraisalId, rating);
			model.addObject(YumPmsConstants.MSG, msg);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ModelAndView() ; 
		}	
		return model;
	}
	@RequestMapping(value = "/sendMailToNotDiscussedAndAgreedAndSubmitted", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> sendMailToNotDiscussedAndAgreedAndSubmitted() {

		LOGGER.info(YumPmsConstants.MAIL_PROCESSED_FOR, YumPmsConstants.NOT_DISCUSSED_AND_AGREED_IDP);
		try {
			service.sendMail(YumPmsConstants.NOT_DISCUSSED_AND_AGREED_IDP);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(YumPmsConstants.ERROR_EMAIL_CAN_NOT_SEND, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(YumPmsConstants.MAIL_SEND_SUCCESSFULLY, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/sendMailToAllPerposeThoughBatch", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> sendMailToAllPerposeThoughBatch(@RequestParam(YumPmsConstants.SP_NAME) String spName) {

		LOGGER.info(YumPmsConstants.MAIL_PROCESSED_FOR, YumPmsConstants.SP_NAME);
		try {
			service.sendMail(YumPmsConstants.PARAMITER_MAIL, spName);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(YumPmsConstants.ERROR_EMAIL_CAN_NOT_SEND, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(YumPmsConstants.MAIL_SEND_SUCCESSFULLY, HttpStatus.OK);
	}
//====================================================================================================================
	private boolean checkPassword(String email, String currentPassword) {

		int flag = 0;
		Connection conn = new DbConnection().getConnection();
		try(PreparedStatement stmt = conn.prepareStatement("SELECT * FROM emp_login WHERE username = ?")) {
			stmt.setString(1, email);
			try (ResultSet rs = stmt.executeQuery()) {
				String dbPassword = null;
				while (rs.next()) {
					LOGGER.info("Fetched current password from {}", "database");
					dbPassword = rs.getString("password");
				}
				if (new BCryptPasswordEncoder().matches(currentPassword, dbPassword)) {
					LOGGER.info("Password {}", "Match");
					flag = 1 ;
				} else {
					LOGGER.info("Password {}", "Doesn't Match");  
					flag = 0;
				}
			}
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
		}
		return flag != 0;
	}

	private boolean checkEmail(String email) {
		
		int flag = 0;
		Connection conn = new DbConnection().getConnection();
		try(PreparedStatement stmt = conn.prepareStatement("SELECT * FROM emp_login WHERE username = ?")) {
			stmt.setString(1, email);
			try(ResultSet rs = stmt.executeQuery()) {
				if (!rs.next()) {
					flag = 0 ;
				} else {
					flag = 1;
				}
			}
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
		}
		return flag != 0;
	}


	private boolean updatePass(String userEmail,String userPassword,String check) {

		String hashedPassword = new BCryptPasswordEncoder().encode(userPassword);
		LOGGER.info("Encoded password is {}", hashedPassword);
		PreparedStatement ps = null;
		try {
			Connection conn = new DbConnection().getConnection();
			ps = conn.prepareStatement("UPDATE emp_login SET password = ?, if_changed_initial_pass = ? WHERE username = ?");
			// set the prepared statement parameters
			ps.setString(1, hashedPassword);
			if(check == YumPmsConstants.RESET) {
				ps.setInt(2, 1);	
			} else {
				ps.setInt(2, 0);
			}
			ps.setString(3,userEmail);
			// call executeUpdate to execute our sql update statement
			ps.executeUpdate();
			return true;
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return false;
		} finally {
			try {
				if(ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			}
		}
	}

}
