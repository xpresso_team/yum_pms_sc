/******************************************************************** 
* Spring Controller for Admin Module 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: sudipta.chandra
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.controller;

import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.yum.comp.domain.CompensationUiConfig;
import com.yum.comp.domain.SalaryHeadData;
import com.yum.comp.domain.SpecialSalaryUiPojo;
import com.yum.comp.service.CompensationService;
import com.yum.idp.domain.IdpCategory;
import com.yum.idp.domain.IdpSubCategory;
import com.yum.idp.domain.IdpSubSubCategory;
import com.yum.idp.domain.IdpSubSubSubCategory;
import com.yum.idp.domain.IdpSuggestedDevelopmentActionPlan;
import com.yum.idp.domain.IdpSuggestedGoal;
import com.yum.pa.domain.AdminPaConfig;
import com.yum.pa.domain.PromotionConfig;
import com.yum.pms.dao.AppraisalPeriod;
import com.yum.pms.dao.CompBonusConfiguration;
import com.yum.pms.dao.IDP_SectionMaster;
import com.yum.pms.domain.EmployeeGoalStatus;
import com.yum.pms.domain.EmployeeOrgGoal;
import com.yum.pms.domain.EmployeeOrgGoalCeoGoal;
import com.yum.pms.domain.EmployeeRoleDetails;
import com.yum.pms.domain.NotificationSettings;
import com.yum.pms.service.EmployeeManagementService;
import com.yum.pms.service.IYumHibernateService;
import com.yum.pms.utils.YumPmsConstants;
import com.yum.pms.utils.YumPmsUtils;

/**
 * @author dipak.swain
 *
 */
@CrossOrigin(origins = YumPmsConstants.ANGULAR_DEV_URL)
@Controller
public class AdminController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminController.class);
	
	@Autowired
	private IYumHibernateService service;
	
	@Autowired
	private EmployeeManagementService empMgmtService;
	
	@Autowired
	private CompensationService compService;
	
	@RequestMapping(value = "/getgoalsheetstatusvalues", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<EmployeeGoalStatus>> getGoalSheetStatusValues() {  
		
		List<EmployeeGoalStatus> statusList = null;
		try { 
			statusList =  service.getGoalSheetStatusValues(); 
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(statusList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(statusList, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/savegoalsheetstatusvalues", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> saveGoalSheetStatusValues(@RequestBody EmployeeGoalStatus status) {
		
		try {
			service.saveGoalSheetStatusValues(status);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(YumPmsConstants.SAVED, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(YumPmsConstants.SAVED, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getorggoals", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<EmployeeOrgGoal>> getOrgGoals(@RequestParam("appid") Integer appraisalId) {  
		
		List<EmployeeOrgGoal> orgGoalList = null;
		try { 
			orgGoalList =  service.getOrgGoals(appraisalId); 
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(orgGoalList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(orgGoalList, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getorgceogoals", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<EmployeeOrgGoalCeoGoal>> getOrgCeoGoals(@RequestParam(YumPmsConstants.PARAM_APPR_ID) Integer appraisalId) {  
		
		List<EmployeeOrgGoalCeoGoal> orgCeoGoalList = null;
		try { 
			orgCeoGoalList =  service.getCeoOrgGoals(appraisalId); 
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(orgCeoGoalList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(orgCeoGoalList, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getnotificationsettings", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<NotificationSettings>> getNotificationSettings() {  
		
		List<NotificationSettings> notificationSettingsList = null;
		try { 
			notificationSettingsList =  service.getNotificationSettings(); 
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(notificationSettingsList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(notificationSettingsList, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/savenotificationsettings", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<NotificationSettings>> saveNotificationSetting(@RequestBody List<NotificationSettings> listNotificationSetting) {
		
		List<NotificationSettings> notificationSettingList = null;
		try {
			notificationSettingList = service.saveNotificationSetting(listNotificationSetting);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(notificationSettingList, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(notificationSettingList, HttpStatus.OK);
	}

	@RequestMapping(value = "/getapprperiodlist", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<AppraisalPeriod>> getApprPeriodList() {  
		
		List<AppraisalPeriod> appPeriodList = null;
		try { 
			appPeriodList =  service.getApprPeriodList(); 
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(appPeriodList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(appPeriodList, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/saveOrgGoals", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<EmployeeOrgGoal>> saveOrgGoals(@RequestBody List<EmployeeOrgGoal> employeeOrgGoalList) {
		
		try {
			employeeOrgGoalList = service.saveOrgGoals(employeeOrgGoalList);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(employeeOrgGoalList, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(employeeOrgGoalList, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/saveCeoGoals", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<EmployeeOrgGoalCeoGoal>> saveCeoGoals(@RequestBody List<EmployeeOrgGoalCeoGoal> employeeCeoGoalList) {
		
		try {
			employeeCeoGoalList = service.saveCeoGoals(employeeCeoGoalList);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(employeeCeoGoalList, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(employeeCeoGoalList, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/deleteById", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> deleteById(
			@RequestParam(YumPmsConstants.ID) Integer id, 
			@RequestParam(YumPmsConstants.TABLE_NAME) String tableName) {
		
		String msg = StringUtils.EMPTY;
		try { 
			msg =  service.deleteById(id, tableName);
		}catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			msg = YumPmsUtils.getExceptionMessage(ex);
			return new ResponseEntity<>(msg, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(msg, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/deleteByIdJSON", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Properties> deleteByIdJSON(
			@RequestParam(YumPmsConstants.ID) Integer id, 
			@RequestParam(YumPmsConstants.TABLE_NAME) String tableName) {
		
		Properties prop = new Properties();
		try { 
			String msg = service.deleteById(id, tableName);
			if(StringUtils.equalsIgnoreCase(msg, YumPmsConstants.ERROR_MSG_TRY_AGAIN)) {
				prop.put(YumPmsConstants.MSG, YumPmsConstants.ERROR_MSG_TRY_AGAIN);
			}
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			prop.put(YumPmsConstants.MSG, YumPmsUtils.getExceptionMessage(ex));
			return new ResponseEntity<>(prop, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(prop, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/getemployeerolelist", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<EmployeeRoleDetails>> getEmployeeRoleListList() {  
		
		List<EmployeeRoleDetails> empRoleList = null;
		try { 
			empRoleList =  service.getEmpRoleList(); 
		}catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);  
			return new ResponseEntity<>(empRoleList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(empRoleList, HttpStatus.OK);
	}

	@RequestMapping(value = "/saveapprperiodlist", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<AppraisalPeriod>> saveAppraisalPeriods(@RequestBody List<AppraisalPeriod> appraisalPeriodList) {
		
		try {
			appraisalPeriodList = service.saveAppraisalPeriods(appraisalPeriodList);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(appraisalPeriodList, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(appraisalPeriodList, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/saveemployeerolelist", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<EmployeeRoleDetails>> saveEmployeeRoles(@RequestBody List<EmployeeRoleDetails> employeeRoleList) {
		
		try {
			employeeRoleList = service.saveEmployeeRoles(employeeRoleList);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(employeeRoleList, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(employeeRoleList, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/gettemppassword/{userid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getTempPassword(@PathVariable String userid) {  
		
		String password = null;
		try { 
			String userId =  new String(Base64.getDecoder().decode(userid));
			LOGGER.info("userid is >>>>.. {}", userId);
			password = service.savePassword(userId);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(password, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(password, HttpStatus.OK);		
	}
	
	/**
	 * @return Get all the IDP category list
	 */
	@RequestMapping(value = "/getIdpCategoryList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<IDP_SectionMaster>> getIdpCategoryList() {  

		List<IDP_SectionMaster> idpCategoryList = null;
		try { 
			idpCategoryList = service.getIdpCategoryList();
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(idpCategoryList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(idpCategoryList, HttpStatus.OK);
	}
	
	
	/**
	 * @param sectionList: Save all IDP category list
	 * @return All saved IDP category list
	 */
	@RequestMapping(value = "/saveIdpCategoryList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<IdpCategory>> saveIdpCategoryList(@RequestBody List<IdpCategory> categoryList) {  

		List<IdpCategory> idpCategoryList = null;
		try { 
			idpCategoryList = service.saveIdpCategoryList(categoryList);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);  
			return new ResponseEntity<>(idpCategoryList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(idpCategoryList, HttpStatus.OK);
	}
	
	/**
	 * @param sectionList: Save all IDP sub category list
	 * @return All saved IDP sub category list
	 */
	@RequestMapping(value = "/saveIdpSubCategoryList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<IdpSubCategory>> saveIdpSubCategoryList(@RequestBody List<IdpSubCategory> subCategoryList) {  

		List<IdpSubCategory> idpCategoryList = null;
		try { 
			idpCategoryList = service.saveIdpSubCategoryList(subCategoryList);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(idpCategoryList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(idpCategoryList, HttpStatus.OK);
	}
	
	/**
	 * @param sectionList: Save all IDP sub sub category list
	 * @return All saved IDP sub sub category list
	 */
	@RequestMapping(value = "/saveIdpSubSubCategoryList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<IdpSubSubCategory>> saveIdpSubSubCategoryList(@RequestBody List<IdpSubSubCategory> subSubCategoryList) {  

		List<IdpSubSubCategory> idpCategoryList = null;
		try { 
			idpCategoryList = service.saveIdpSubSubCategoryList(subSubCategoryList);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex); 
			return new ResponseEntity<>(idpCategoryList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(idpCategoryList, HttpStatus.OK);
	}
	
	/**
	 * @param sectionList: Save all IDP sub sub category list
	 * @return All saved IDP sub sub category list
	 */
	@RequestMapping(value = "/saveIdpSubSubSubCategoryList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<IdpSubSubSubCategory>> saveIdpSubSubSubCategoryList(@RequestBody List<IdpSubSubSubCategory> subSubSubCategoryList) {  

		List<IdpSubSubSubCategory> idpCategoryList = null;
		try { 
			idpCategoryList = service.saveIdpSubSubSubCategoryList(subSubSubCategoryList);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex); 
			return new ResponseEntity<>(idpCategoryList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(idpCategoryList, HttpStatus.OK);
	}
	
	/**
	 * @param sectionList: Save IDP Suggested goals list
	 * @return All saved IDP Suggested goals list
	 */
	@RequestMapping(value = "/saveIdpSuggestedGoalList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<IdpSuggestedGoal>> saveIdpSuggestedGoalList(@RequestBody List<IdpSuggestedGoal> suggestedGoalList) {  
		List<IdpSuggestedGoal> idpSuggestedGoalList = null;
		try { 
			idpSuggestedGoalList = service.saveIdpSuggestedGoalList(suggestedGoalList);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex); 
			return new ResponseEntity<>(idpSuggestedGoalList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(idpSuggestedGoalList, HttpStatus.OK);
	}
	
	/**
	 * @param sectionList: Save IDP Suggestive Development Action Plan
	 * @return All saved IDP Suggestive Development Action Plan
	 */
	@RequestMapping(value = "/saveIdpSuggestedDevPlanList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<IdpSuggestedDevelopmentActionPlan>> saveIdpDevelopmentPlanList(@RequestBody List<IdpSuggestedDevelopmentActionPlan> developmentPlanist) {  
		List<IdpSuggestedDevelopmentActionPlan> idpDevelopmentPlanistList = null;
		try { 
			idpDevelopmentPlanistList = service.saveIdpDevelopmentPlanList(developmentPlanist);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex); 
			return new ResponseEntity<>(idpDevelopmentPlanistList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(idpDevelopmentPlanistList, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getAllIdpSections", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String,Object>> getAllIdpSections(@RequestParam(YumPmsConstants.APPR_ID) Integer apprId,
			@RequestParam(value = "Flag", required=false) String categoryFlag) {
		//
		Map<String,Object> allSections = null;
		try {
			allSections = service.getAllSections(apprId,categoryFlag);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);  
			return new ResponseEntity<>(allSections, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(allSections, HttpStatus.OK);
	}
	
	/**
	 * This service is used to delete All IDP categories from Admin module
	 * @param id - CategoryId
	 * @param tableName - Category Table name
	 * @return
	 */
	
	@RequestMapping(value = "/deleteIdpSection", params = {"id", "tableName"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Properties> deleteIdpSection(
			@RequestParam(YumPmsConstants.ID) Integer id, 
			@RequestParam(YumPmsConstants.TABLE_NAME) String tableName) {
		
		LOGGER.info("Param is>>>>..deletable id: {} ; deletable tableName: {}", id, tableName);
		Properties prop = new Properties();
		try { 
			service.deleteIdpSection(id, tableName);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			prop.put(YumPmsConstants.MSG, StringUtils.isEmpty(ex.getMessage()) ? ex.toString() : ex.getMessage());
			return new ResponseEntity<>(prop, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(prop, HttpStatus.OK);
	}
	
	/**
	 * get Admin's PA Configuration
	 * @param apprId
	 * @return Administrator Pa Configuration data
	 * @throws Exception 
	 */
	@RequestMapping(value = "/getAdminPaConfig", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AdminPaConfig> getAdminPaConfig(@RequestParam(YumPmsConstants.APPR_ID) Integer apprId) {
		
		AdminPaConfig adminPaConfig = null;
		try {
			adminPaConfig = service.getPaConfigData(apprId);
			adminPaConfig.setStatusMsg(YumPmsConstants.SUCCESS_FETCH_MSG);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			adminPaConfig = new AdminPaConfig(apprId);
			adminPaConfig.setStatusMsg(YumPmsUtils.getExceptionMessage(e));
			return new ResponseEntity<>(adminPaConfig, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(adminPaConfig, HttpStatus.OK);
	}
	
	/**
	 * save AdminPaConfig data
	 * @param adminPaConfig
	 * @return saved saveAdminPaConfig
	 */
	@RequestMapping(value = "/saveAdminPaConfig", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AdminPaConfig> saveAdminPaConfig(@RequestBody AdminPaConfig adminPaConfig) {
		
		try {
			service.savePaConfigData(adminPaConfig);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			if(adminPaConfig != null) {
				adminPaConfig.setStatusMsg(YumPmsUtils.getExceptionMessage(ex));
			}
			return new ResponseEntity<>(adminPaConfig, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		adminPaConfig = service.getPaConfigData(adminPaConfig.getApprId());
		if(adminPaConfig != null) {
			adminPaConfig.setStatusMsg(YumPmsConstants.SUCCESS_SAVE_MSG);
		}
		return new ResponseEntity<>(adminPaConfig, HttpStatus.OK);
	}
	
	/**
	 * get the Administrator Promotion Configuration data
	 * @param apprId
	 * @return Promotion Configuration data
	 */
	@RequestMapping(value = "/getAdminPromotionConfig", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PromotionConfig> getAdminPromotionConfig(@RequestParam(YumPmsConstants.APPR_ID) Integer apprId) {
		
		if(YumPmsUtils.isNullOrZero(apprId)) {
			return new ResponseEntity<>(new PromotionConfig(YumPmsConstants.ARGUMENT_CAN_NOT_NULL), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		try {
			PromotionConfig promotionConfig = service.getPromotionConfigData(apprId);
			if(promotionConfig != null) {
				promotionConfig.setStatusMsg(YumPmsConstants.SUCCESS_FETCH_MSG);
			}
			return new ResponseEntity<>(promotionConfig, HttpStatus.OK);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			PromotionConfig promotionConfig = new PromotionConfig(apprId);
			promotionConfig.setStatusMsg(YumPmsUtils.getExceptionMessage(ex));
			return new ResponseEntity<>(promotionConfig, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * save the Administration Promotion Configuration data
	 * @param promotionConfig
	 * @return Promotion Configuration data
	 */
	@RequestMapping(value = "/saveAdminPromotionConfig", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PromotionConfig> saveAdminPromotionConfig(@RequestBody PromotionConfig promotionConfig) {
		
		try {
			if(promotionConfig == null || YumPmsUtils.isNullOrZero(promotionConfig.getApprId())) {
				return new ResponseEntity<>(promotionConfig, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			service.savePromotionConfigData(promotionConfig);
			promotionConfig = service.getPromotionConfigData(promotionConfig.getApprId());
			if(promotionConfig != null) {
				promotionConfig.setStatusMsg(YumPmsConstants.SUCCESS_SAVE_MSG);
			}
			return new ResponseEntity<>(promotionConfig, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			if(promotionConfig != null) {
				promotionConfig.setStatusMsg(YumPmsUtils.getExceptionMessage(ex));
			}
			return new ResponseEntity<>(promotionConfig, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/getMasterDataDropdown/{dropdownName}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<Integer, String>> getMasterDataDropdown(@PathVariable String dropdownName) {
		
		Map<Integer, String> response = null;
		try {
			response = empMgmtService.getMasterTableDropdown(dropdownName);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**method name: getSalaryDetailsEmployeeWise
	 * @param empId
	 * @param apprId
	 * @return
	 */
	@RequestMapping(value = "/getSalaryDetailsEmployeeWise", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<SalaryHeadData>> getSalaryDetailsEmployeeWise(
			@RequestParam(YumPmsConstants.EMP_ID) String empId, 
			@RequestParam(YumPmsConstants.APPR_ID) String apprId) {
		List<SalaryHeadData> responseSalaryObjectList = null;
		try {
			responseSalaryObjectList = compService.getSalaryDetails(NumberUtils.toInt(empId), NumberUtils.toInt(apprId));
			return new ResponseEntity<>(responseSalaryObjectList, HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(responseSalaryObjectList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**method name: insertOrUpdateSalary
	 * @param uiSalaryObjectList
	 * @return
	 */
	@RequestMapping(value = "/insertOrUpdateSalary", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<SalaryHeadData>> insertOrUpdateSalary(@RequestBody List<SalaryHeadData> uiSalaryObjectList) {
		List<SalaryHeadData> responseSalaryObjectList = null;
		try {
			responseSalaryObjectList = compService.insertOrUpdateSalray(uiSalaryObjectList);
			return new ResponseEntity<>(responseSalaryObjectList, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(responseSalaryObjectList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**method name: getSpecialSalary
	 * @param curYear
	 * @param empId
	 * @param action
	 * @return
	 */
	@RequestMapping(value = "/getSpecialSalary", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<CompensationUiConfig>> getSpecialSalary(@RequestBody SpecialSalaryUiPojo  specialSalary) {
			Integer appraisalPeriord = specialSalary.getAppraisalPeriord(); 
			Integer nextAppraisalPeriord = specialSalary.getNextAppraisalPeriord();
			Integer empId = specialSalary.getEmpId();
			String dataList = specialSalary.getDataList();
			String action = specialSalary.getAction();
			String purpose = specialSalary.getPurpose();

		List<CompensationUiConfig> reviesSalaryList = new ArrayList<>();
		try { 
			reviesSalaryList =  compService.getSpecialSalary(appraisalPeriord, nextAppraisalPeriord, empId, purpose, 
											dataList, action); 
			LOGGER.info(YumPmsConstants.LOG_USER_ID, reviesSalaryList);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(reviesSalaryList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(reviesSalaryList, HttpStatus.OK);		
	}
	
	/**
	 * configure a New Appraisal year
	 * @param currentApprId
	 * @param nextApprId
	 * @return the status message
	 */
	@RequestMapping(value = "/configureNewAppraisalPeriod", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Properties> configureNewAppraisalPeriod(@RequestParam(YumPmsConstants.CURRENT_APPR_ID) Integer currentApprId,
			@RequestParam(YumPmsConstants.NEXT_APPR_ID) Integer nextApprId) {
		
		Properties prop = new Properties();
		try { 
			String str =  compService.configureNewAppraisalPeriod(currentApprId, nextApprId);
			prop.setProperty(YumPmsConstants.STATUS_MSG, str);
			LOGGER.info(YumPmsConstants.LOG_USER_ID, str);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			prop.setProperty(YumPmsConstants.STATUS_MSG, ex.getMessage());
			return new ResponseEntity<>(prop, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<>(prop, HttpStatus.OK);		
	}
	
	/**
	 * new year Configuration For BrandGoal
	 * @param apprId
	 * @return 
	 */
	@RequestMapping(value = "/yearConfigurationForBrandGoal", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Object>> yearConfigurationForBrandGoal(@RequestParam(YumPmsConstants.APPR_ID) Integer apprId) {
		
		try {
			Map<String, Object> map = service.yearConfigurationForBrandGoal(apprId);
			return new ResponseEntity<>(map, HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			Map<String, Object> map = new HashMap<>(1);
			map.put("error", e.getMessage());
			return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Fetch the Bonus Configuration by appraisal year
	 * @param apprId
	 * @return bonus configuration data
	 */
	@RequestMapping(value = "/getBonusConfig", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CompBonusConfiguration> getBonusConfig(@RequestParam(YumPmsConstants.APPR_ID) Integer apprId) {
		
		try {
			CompBonusConfiguration config = compService.getBonusConfig(apprId);
			config.setStatusMsg(YumPmsConstants.SUCCESS_FETCH_MSG);
			config.setApprId(apprId);
			return new ResponseEntity<>(config, HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			CompBonusConfiguration config = new CompBonusConfiguration();
			config.setStatusMsg(e.getMessage());
			config.setApprId(apprId);
			return new ResponseEntity<>(config, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Save the Bonus Configuration data by appraisal year
	 * @param request
	 * @return bonus configuration data
	 */
	@RequestMapping(value = "/saveBonusConfig", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CompBonusConfiguration> saveBonusConfig(@RequestBody CompBonusConfiguration request) {
		
		try {
			CompBonusConfiguration config = compService.saveBonusConfig(request);
			config.setStatusMsg(YumPmsConstants.SUCCESS_SAVE_MSG);
			config.setApprId(request.getApprId());
			return new ResponseEntity<>(config, HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			CompBonusConfiguration config = new CompBonusConfiguration();
			config.setStatusMsg(e.getMessage());
			return new ResponseEntity<>(config, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * save Employee Master table Data like designation, function, sub-function, category and grade
	 * @param dropdownName - like "emp_designation", "emp_grade", "emp_function", "emp_sub_function" and "emp_category" 
	 * @param requestData - The request data should be in this { "key1" : "value1", "key2" : "value3" } format
	 * @return saved mater table data
	 */
	@RequestMapping(value = "/saveEmployeeMasterData/{dropdownName}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<Integer, String>> saveEmployeeMasterData(@PathVariable String dropdownName, 
			@RequestBody Map<Integer, String> requestData) {
		
		try {
			return new ResponseEntity<>(empMgmtService.saveEmployeeMasterData(dropdownName, requestData), HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(Collections.emptyMap(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Delete an employee master table data by it's primary key
	 * @param dropdownName - like "emp_designation", "emp_grade", "emp_function", "emp_sub_function" and "emp_category"
	 * @param id - primary key
	 * @return status message
	 */
	@RequestMapping(value = "/deleteEmployeeMasterData/{dropdownName}/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Properties> deleteEmployeeMasterData(@PathVariable String dropdownName, @PathVariable Integer id) {
		
		Properties prop = new Properties();
		try {
			prop.setProperty(YumPmsConstants.STATUS, empMgmtService.deleteEmployeeMasterData(dropdownName, id));
			return new ResponseEntity<>(prop, HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			prop.setProperty(YumPmsConstants.STATUS, e.getMessage());
			return new ResponseEntity<>(prop, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
