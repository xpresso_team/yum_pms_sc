package com.yum.pms.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.yum.comp.domain.CompLetterUiData;
import com.yum.comp.domain.CompUiRevisedSalary;
import com.yum.comp.domain.CompensationLetter;
import com.yum.comp.domain.CompensationUiConfig;
import com.yum.comp.domain.DesgWiseVpay;
import com.yum.comp.domain.GeneralConfigData;
import com.yum.comp.domain.LetterTemplateConfig;
import com.yum.comp.domain.LookupUiConfig;
import com.yum.comp.domain.YearWisePayRangeData;
import com.yum.comp.service.CompensationService;
import com.yum.pms.dao.CompYearWiseMultiplier;
import com.yum.pms.utils.YumPmsConstants;

@CrossOrigin(origins = YumPmsConstants.ANGULAR_DEV_URL)
@Controller
public class CompensationController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CompensationController.class);
	
	@Autowired
	private CompensationService compService;
	
	/**method name : getRevisedSalaryCalculation
	 * @param curYear
	 * @param nextYear
	 * @param action
	 * @param empId
	 * @return
	 */
	@RequestMapping(value = "/getRevisedSalaryCalculation", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<CompensationUiConfig>> getRevisedSalaryCalculation(
			@RequestParam(YumPmsConstants.CURRENT_YEAR) String curYear, 
			@RequestParam(YumPmsConstants.NEXT_YEAR) String nextYear,
			@RequestParam(YumPmsConstants.ACTION) String action,
			@RequestParam(value = YumPmsConstants.EMP_ID, required = false) Integer empId) {

		List<CompensationUiConfig> reviesSalaryList = new ArrayList<>();
		try { 
			reviesSalaryList =  compService.getReviesSalary(empId, curYear, nextYear, action);
			LOGGER.info(YumPmsConstants.LOG_USER_ID, reviesSalaryList);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(reviesSalaryList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(reviesSalaryList, HttpStatus.OK);		
	}
	
	/**method name : getBonusCalculation
	 * @param curYear
	 * @param nextYear
	 * @param empId
	 * @return
	 */
	@RequestMapping(value = "/getBonusCalculation", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<CompensationUiConfig>> getBonusCalculation(
			@RequestParam(YumPmsConstants.CURRENT_YEAR) String curYear, 
			@RequestParam(YumPmsConstants.NEXT_YEAR) String nextYear,
			@RequestParam(value = YumPmsConstants.EMP_ID, required = false) Integer empId) {

		List<CompensationUiConfig> bonusSheetList = new ArrayList<>();
		try { 
			bonusSheetList =  compService.getBonusSheetList(empId, curYear, nextYear);
			LOGGER.info(YumPmsConstants.LOG_USER_ID, bonusSheetList);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(bonusSheetList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(bonusSheetList, HttpStatus.OK);		
	}
	
	@RequestMapping(value = "/getYearWisePayRange", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<YearWisePayRangeData> getYearWisePayRangeData(
			@RequestParam(value = YumPmsConstants.APPR_ID, required = false) Integer apprId) {
		try {
			YearWisePayRangeData yearWisePayRangeData = compService.getYearWisePayRangeData(apprId);
			yearWisePayRangeData.setStatusMsg(YumPmsConstants.SUCCESS_FETCH_MSG);
			return new ResponseEntity<>(yearWisePayRangeData, HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YearWisePayRangeData yearWisePayRangeData = new YearWisePayRangeData(apprId);
			yearWisePayRangeData.setStatusMsg(e.getMessage());
			return new ResponseEntity<>(yearWisePayRangeData, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/saveYearWisePayRange", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<YearWisePayRangeData> saveYearWisePayRangeData(@RequestBody YearWisePayRangeData yearWisePayRangeData) {
		
		Integer apprId = yearWisePayRangeData.getApprId();
		try {
			yearWisePayRangeData = compService.saveYearWisePayRangeData(yearWisePayRangeData);
			yearWisePayRangeData.setStatusMsg(YumPmsConstants.SUCCESS_SAVE_MSG);
			return new ResponseEntity<>(yearWisePayRangeData, HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			yearWisePayRangeData = new YearWisePayRangeData(apprId, Collections.emptyList());
			yearWisePayRangeData.setStatusMsg(e.getMessage());
			return new ResponseEntity<>(yearWisePayRangeData, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/getYearWiseMultiplier", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<CompYearWiseMultiplier>> getYearWiseMultiplierData(
			@RequestParam(value = YumPmsConstants.APPR_ID, required = false) Integer apprId) {
		
		List<CompYearWiseMultiplier> yearWiseMultiplierData = null;
		try {
			yearWiseMultiplierData = compService.getYearWiseMultiplierData(apprId);
			return new ResponseEntity<>(yearWiseMultiplierData, HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(yearWiseMultiplierData, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/saveYearWiseMultiplier", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<CompYearWiseMultiplier>> saveYearWiseMultiplierData(@RequestBody List<CompYearWiseMultiplier> multiplierData) {
		
		List<CompYearWiseMultiplier> yearWiseMultiplierData = null;
		try {
			yearWiseMultiplierData = compService.saveYearWiseMultiplierData(multiplierData);
			return new ResponseEntity<>(yearWiseMultiplierData, HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(yearWiseMultiplierData, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/getGradeWiseLookupData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LookupUiConfig> getGradeAndYearWiseLookupData(
			@RequestParam(value = YumPmsConstants.APPR_ID, required = false) Integer apprId) {
		
		LookupUiConfig lookupUiConfig = null;
		try {
			lookupUiConfig = compService.getGradeAndYearWiseLookupData(apprId);
			lookupUiConfig.setStatusMsg(YumPmsConstants.SUCCESS_FETCH_MSG);
			return new ResponseEntity<>(lookupUiConfig, HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			lookupUiConfig = new LookupUiConfig(apprId, Collections.emptyList());
			lookupUiConfig.setStatusMsg(e.getMessage());
			return new ResponseEntity<>(lookupUiConfig, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/saveGradeWiseLookupData", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LookupUiConfig> saveGradeAndYearWiseLookupData(@RequestBody LookupUiConfig lookupUiConfig) {
		
		Integer apprId = lookupUiConfig.getApprId();
		try {
			lookupUiConfig = compService.saveGradeAndYearWiseLookupData(lookupUiConfig);
			lookupUiConfig.setStatusMsg(YumPmsConstants.SUCCESS_SAVE_MSG);
			return new ResponseEntity<>(lookupUiConfig, HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			lookupUiConfig = new LookupUiConfig(apprId, Collections.emptyList());
			lookupUiConfig.setStatusMsg(e.getMessage());
			return new ResponseEntity<>(lookupUiConfig, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/getRevisedSalaryEffectiveDuration", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Object>> getRevisedSalaryEffectiveDuration(
			@RequestParam(value = YumPmsConstants.APPR_ID, required = false) Integer apprId) {
		
		try {
			Map<String, Object> resultMap = compService.getRevisedSalaryEffectiveDuration(apprId);
			resultMap.put(YumPmsConstants.STATUS_MSG, YumPmsConstants.SUCCESS_FETCH_MSG);
			return new ResponseEntity<>(resultMap, HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			Map<String, Object> resultMap = new HashMap<>(1);
			resultMap.put(YumPmsConstants.STATUS_MSG, e.getMessage());
			return new ResponseEntity<>(resultMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/saveRevisedSalaryEffectiveDuration", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Object>> saveRevisedSalaryEffectiveDuration(@RequestBody Map<String, Object> requestMap) {
		
		try {
			requestMap = compService.saveRevisedSalaryEffectiveDuration(requestMap);
			requestMap.put(YumPmsConstants.STATUS_MSG, YumPmsConstants.SUCCESS_SAVE_MSG);
			return new ResponseEntity<>(requestMap, HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			requestMap.put(YumPmsConstants.STATUS_MSG, e.getMessage());
			return new ResponseEntity<>(requestMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/getDesgWiseVpayPercentage", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DesgWiseVpay> getDesgWiseVpayPercentage(
			@RequestParam(value = YumPmsConstants.APPR_ID, required = false) Integer apprId) {
		
		DesgWiseVpay desgWiseVpay = null;
		try {
			desgWiseVpay = compService.getDesgWiseVpayPercentage(apprId);
			desgWiseVpay.setStatusMsg(YumPmsConstants.SUCCESS_FETCH_MSG);
			return new ResponseEntity<>(desgWiseVpay, HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			desgWiseVpay = new DesgWiseVpay(apprId);
			desgWiseVpay.setStatusMsg(e.getMessage());
			return new ResponseEntity<>(desgWiseVpay, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/saveDesgWiseVpayPercentage", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DesgWiseVpay> saveDesgWiseVpayPercentage(@RequestBody DesgWiseVpay desgWiseVpay) {
		
		try {
			desgWiseVpay = compService.saveDesgWiseVpayPercentage(desgWiseVpay);
			desgWiseVpay.setStatusMsg(YumPmsConstants.SUCCESS_SAVE_MSG);
			return new ResponseEntity<>(desgWiseVpay, HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			desgWiseVpay.setStatusMsg(e.getMessage());
			return new ResponseEntity<>(desgWiseVpay, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/getGeneralConfiguration", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GeneralConfigData> getGeneralConfiguration(
			@RequestParam(value = YumPmsConstants.APPR_ID, required = false) Integer apprId) {
		
		try {
			GeneralConfigData rsponse = compService.getGeneralConfiguration(apprId);
			rsponse.setStatusMsg(YumPmsConstants.SUCCESS_FETCH_MSG);
			return new ResponseEntity<>(rsponse, HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			GeneralConfigData rsponse = new GeneralConfigData(apprId, Collections.emptyList());
			rsponse.setStatusMsg(e.getMessage());
			return new ResponseEntity<>(rsponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/getAllEmpsDetailsForCompLetter", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CompLetterUiData> getAllEmpsDetailsForLetter(
			@RequestParam(value = YumPmsConstants.APPR_ID, required = false) Integer apprId,
			@RequestParam(value = YumPmsConstants.SALARY_TYPE, required = false) String salaryType){
		
		try {
			CompLetterUiData rsponse = compService.getAllEmpsDetailsForCompLetter(apprId, salaryType);
			rsponse.setStatusMsg(YumPmsConstants.SUCCESS_FETCH_MSG);
			return new ResponseEntity<>(rsponse, HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			CompLetterUiData rsponse = new CompLetterUiData(apprId, Collections.emptyList());
			rsponse.setStatusMsg(e.getMessage());
			return new ResponseEntity<>(rsponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/getCompensationLetterHtml", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CompensationLetter> getCompensationLetterHtml(@RequestParam(YumPmsConstants.EMP_ID) Integer empId,
			@RequestParam(YumPmsConstants.APPR_ID) Integer apprId, 
			@RequestParam(value = YumPmsConstants.SALARY_TYPE, required = false) String salaryType) {
		
		LOGGER.info("getCompensationLetterInHtml: params are: apprid = {} and empid = {}", apprId, empId);
		try {
			return new ResponseEntity<>(compService.getCompensationLetter(empId, apprId, salaryType, false), HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(new CompensationLetter(empId, apprId), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/saveGeneralConfiguration", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<GeneralConfigData> saveGeneralConfiguration(@RequestBody GeneralConfigData requestConfig) {
		
		try {
			GeneralConfigData rsponse = compService.saveGeneralConfiguration(requestConfig);
			rsponse.setStatusMsg(YumPmsConstants.SUCCESS_SAVE_MSG);
			return new ResponseEntity<>(rsponse, HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			GeneralConfigData rsponse = new GeneralConfigData(-1, Collections.emptyList());
			rsponse.setStatusMsg(e.getMessage());
			return new ResponseEntity<>(rsponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/getCompLetterTemplate", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LetterTemplateConfig> getCompLetterTemplate(
			@RequestParam(value = YumPmsConstants.APPR_ID, required = false) Integer apprId) {
		
		try {
			return new ResponseEntity<>(compService.getCompLetterTemplate(apprId), HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			LetterTemplateConfig template = new LetterTemplateConfig();
			template.setStatusMessage(ex.getMessage());
			template.setLetterTemplateList(Collections.emptyList());
			return new ResponseEntity<>(template, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/saveCompLetterTemplate", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LetterTemplateConfig> saveCompLetterTemplate(@RequestBody LetterTemplateConfig compLetterConfig) {
		
		try {
			return new ResponseEntity<>(compService.saveCompLetterTemplate(compLetterConfig), HttpStatus.OK);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			LetterTemplateConfig template = new LetterTemplateConfig();
			template.setStatusMessage(ex.getMessage());
			template.setLetterTemplateList(Collections.emptyList());
			return new ResponseEntity<>(template, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	

// -----------------------------------single with bulk submit ---------------------------------	
	/**
	 * @param revisedSalry
	 * @return
	 */
	@RequestMapping(value = "/editSaveRevisedSalary", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CompUiRevisedSalary> editSaveRevisedSalary(@RequestBody CompUiRevisedSalary revisedSalry) {
		CompUiRevisedSalary revisedSal = null;
		try {
			revisedSal = compService.saveEditReviesSalaryNew(revisedSalry); 
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(revisedSal, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(revisedSal, HttpStatus.OK);
	}
	
}
