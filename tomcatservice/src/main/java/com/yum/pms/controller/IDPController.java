package com.yum.pms.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.yum.idp.domain.EmployeeIdpSheetStatusHistory;
import com.yum.idp.domain.IdpUIConfig;
import com.yum.idp.domain.LtsRatingDetails;
import com.yum.idp.service.IIDPService;
import com.yum.pms.dao.IDP_SectionMaster;
import com.yum.pms.domain.StatusWorkFlow;
import com.yum.pms.utils.YumPmsConstants;

/**
* This Controller created for accepting all requests,
* which comes from IDP section.
*
* @author  Jayanta Biswas
* @version 1.0
* @since   21-12-2017
*/

@CrossOrigin(origins = YumPmsConstants.ANGULAR_DEV_URL)
@Controller
public class IDPController {
	
	@Autowired
	private IIDPService idpService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IDPController.class);
	
	/**
	 * @param Integer empId
	 * @param Integer appraisalId
	 * @return ResponseEntity(Json)
	 */
	@RequestMapping(value = "/getIdpDetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<IdpUIConfig> getIdpDetails(
			@RequestParam(YumPmsConstants.PARAM_EMP_ID) Integer empId, 
			@RequestParam(YumPmsConstants.PARAM_APPR_ID) Integer appraisalId, 
			Boolean isReportee) {
		
		LOGGER.info("Start getPlanDetails>>>>>>>... empId = {} and apprId = {}", empId, appraisalId);
		IdpUIConfig idpUIConfig = new IdpUIConfig();
		try {
			idpUIConfig = idpService.getIdpUi(empId,appraisalId, isReportee);
			return new ResponseEntity<>(idpUIConfig, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(idpUIConfig, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * @param Integer empId
	 * @param Integer appraisalId
	 * @return ResponseEntity(Json)
	 */
	@RequestMapping(value = "/getPrevOpportunity", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<IdpUIConfig> getPrevOpportunity(
			@RequestParam(YumPmsConstants.PARAM_EMP_ID) Integer empId, 
			@RequestParam(YumPmsConstants.PARAM_APPR_ID) Integer appraisalId, 
			Boolean isReportee) {
		
		LOGGER.info("Start getPrevOpportunity>>>>>>>... empId = {} and apprId = {}", empId, appraisalId);
		IdpUIConfig idpUIConfig = new IdpUIConfig();
		try {
			idpUIConfig = idpService.getPrevOpportunity(empId,appraisalId);
			return new ResponseEntity<>(idpUIConfig, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(idpUIConfig, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * @param Integer empId
	 * @param Integer appraisalId
	 * @return ResponseEntity(Json)
	 */
	@RequestMapping(value = "/getIdpSuggestedGoalSection", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<IdpUIConfig> getIdpSuggestedGoalSection(
			@RequestParam(YumPmsConstants.PARAM_EMP_ID) Integer empId, 
			@RequestParam(YumPmsConstants.PARAM_APPR_ID) Integer appraisalId, 
			Boolean isReportee) {
		
		LOGGER.info("Start getIdpSuggestedGoalSection>>>>>>>... empId = {} and apprId = {}", empId, appraisalId);
		IdpUIConfig idpUIConfig = new IdpUIConfig();
		try {
			idpUIConfig = idpService.getIdpUiSuggestedGoalSection(empId,appraisalId, isReportee);
			return new ResponseEntity<>(idpUIConfig, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(idpUIConfig, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * @param IdpUIConfig saveObject
	 * @return ResponseEntity(Json)
	 */
	@RequestMapping(value = "/saveIdpSheet", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<IdpUIConfig> saveIdpSheet(@RequestBody IdpUIConfig saveObject) {
		LOGGER.info("Start getPlanDetails>>>>>>>...: Request Body = {}", saveObject);
		IdpUIConfig idpUIConfig = new IdpUIConfig();
		try {
			idpUIConfig = idpService.saveIdpForm(saveObject);
			return new ResponseEntity<>(idpUIConfig, HttpStatus.OK);
		}
		catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(idpUIConfig, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
		
	/**
	 * Call service for get category list for How To Lead Section
	 * @return ResponseEntity<Map<String, Object>> sectionList
	 */
	@RequestMapping(value = "/getsectionlist", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<IDP_SectionMaster>> getsectionlist() {
		
		List<IDP_SectionMaster> sectionList = new ArrayList<>();
		try {
			sectionList = idpService.getSectionList();
			return new ResponseEntity<>(sectionList, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(sectionList, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
	/**
	 * @param Integer idpSheetId
	 * @return List<EmployeeIdpSheetStatusHistory> workflowHistory
	 */
	@RequestMapping(value = "/getWorkflowStatusForIdpSheet", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<EmployeeIdpSheetStatusHistory>> getWorkflowStatusForIdpSheet(@RequestParam("idpsheetid") Integer idpSheetId) {  
		List<EmployeeIdpSheetStatusHistory> workflowHistory = null;
		try { 
			workflowHistory = idpService.getWorkFlowHistoryStatus(idpSheetId); 
		}catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(workflowHistory, HttpStatus.INTERNAL_SERVER_ERROR);		
		}
		return new ResponseEntity<>(workflowHistory, HttpStatus.OK);		
	}

	/**
	 * @param Integer idpSheetId
	 * @return StatusWorkFlow nextStatus
	 */
	@RequestMapping(value = "/getNextWorkStatusByIdpSheetId/{idpSheetId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StatusWorkFlow> getNextWorkStatusByGoalSheetId(@PathVariable Integer idpSheetId) {
		LOGGER.info("getNextWorkStatusByGoalSheetId>>>>>..Param is>>>>.. idpSheetId: {}", idpSheetId);  
		StatusWorkFlow nextStatus = null;
		try { 
			nextStatus =  idpService.getWorkStatusByIdpSheetId(idpSheetId, false);
		}catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(nextStatus, HttpStatus.INTERNAL_SERVER_ERROR);
		}	

		return new ResponseEntity<>(nextStatus, HttpStatus.OK);
	}
	
	/**
	 * @param Map<String, Object> params
	 * @return StatusWorkFlow nextStatus
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(value = "/UpdateIdpSheetStatus", method = RequestMethod.POST, 
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StatusWorkFlow> updateGoalSheetStatus(@RequestBody Map<String, Object> params) { 

		int idpSheetId = (int) params.get(YumPmsConstants.IDP_SHEET_ID_PARAM);
		int statusId = (int) params.get(YumPmsConstants.IDP_STATUS_PARAM);
		int ratingStatusId = (int) params.get(YumPmsConstants.IDP_RATING_STATUS_ID);
		String createdBy = (String) params.get(YumPmsConstants.CREATED_BY);
		boolean offDiscussedAndAgreed = null != params.get(YumPmsConstants.DISCUSSED_AND_AGREED) ? 
				(boolean) params.get("offDiscussedAndAgreed") : false;

		StatusWorkFlow nextStatus = null;		
		LOGGER.info("UpdateGoalSheetStatus>>>>..Param is>>>>.. goalSheetId: {}; statusId: {}", idpSheetId, statusId);  
		try {
			if(statusId == 1005) {
				nextStatus = offDiscussedAndAgreed ? 
						idpService.updateIdpSheetStatus(idpSheetId, statusId, createdBy, ratingStatusId) : new StatusWorkFlow() ; 
			} else {
				nextStatus =  idpService.updateIdpSheetStatus(idpSheetId, statusId, createdBy, ratingStatusId) ; 
			}
			nextStatus.setMessage(null == nextStatus.getMessage() ? YumPmsConstants.UPDATED_SUCESSFULLY : nextStatus.getMessage()); 
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(nextStatus, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(nextStatus, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getNextRatingStatusByIdpSheetId/{idSheetId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StatusWorkFlow> getNextRatingStatusByIdpSheetId(@PathVariable Integer idSheetId) {

		LOGGER.info("getNextWorkStatusByIdpSheetId>>>>>..Param is>>>>.. idpId: {}", idSheetId);  

		StatusWorkFlow nextStatus = null;
		try { 
			nextStatus =  idpService.getWorkStatusByIdpSheetId(idSheetId, true);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(nextStatus, HttpStatus.INTERNAL_SERVER_ERROR);
		}	

		return new ResponseEntity<>(nextStatus, HttpStatus.OK);
	}
	
	/**
	 * @param empId
	 * @param appraisalId
	 * @return
	 */
	@RequestMapping(value = "/getIdpLTsRatingDetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<LtsRatingDetails>> getIdpLTsRatingDetails(
			@RequestParam(YumPmsConstants.PARAM_EMP_ID) Integer empId, 
			@RequestParam(YumPmsConstants.PARAM_APPR_ID) Integer appraisalId) {
		
		LOGGER.error("Start getPlanDetails>>>>>>>...empid = {} and apprid = {}", empId, appraisalId);
		
		List<LtsRatingDetails> ltsRatingDetailsList = null;
		try {
			ltsRatingDetailsList = idpService.getLtsRatingDetails(empId,appraisalId);
			if(CollectionUtils.isNotEmpty(ltsRatingDetailsList)) {
				ltsRatingDetailsList.forEach(ltsRating -> ltsRating.setLtsRatingSup(null));
			}
			return new ResponseEntity<>(ltsRatingDetailsList, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(ltsRatingDetailsList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/saveBulkOfIdpLTsRatingDetails", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<LtsRatingDetails>> saveBulkOfIdpLTsRatingDetails(@RequestBody List<LtsRatingDetails> saveObjectList) {
		
		LOGGER.info("Start getPlanDetails>>>>>>>...: {}", saveObjectList);
		List<LtsRatingDetails> ltsRatingDetailsList = null;
		try {
			ltsRatingDetailsList = idpService.saveBulkLTsRatingDetails(saveObjectList);
			return new ResponseEntity<>(ltsRatingDetailsList, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(ltsRatingDetailsList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * @param saveObject
	 * @return
	 */
	@RequestMapping(value = "/saveIdpLTsRatingDetails", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LtsRatingDetails> saveIdpLTsRatingDetails(@RequestBody LtsRatingDetails saveObject) {
		
		LOGGER.info("Start getPlanDetails>>>>>>>...saveObject = {}", saveObject);
		LtsRatingDetails ltRatingDetails = null;
		try {
			ltRatingDetails = idpService.saveLTsRatingDetails(saveObject);
			return new ResponseEntity<>(ltRatingDetails, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(ltRatingDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
