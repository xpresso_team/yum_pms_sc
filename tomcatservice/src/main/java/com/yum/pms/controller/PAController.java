package com.yum.pms.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.yum.pa.domain.PASheetData;
import com.yum.pa.service.IPAService;
import com.yum.pms.dao.PA_ConfigRatingBellCurveIFRange;
import com.yum.pms.domain.StatusWorkFlow;
import com.yum.pms.utils.YumPmsConstants;
import com.yum.pms.utils.YumPmsUtils;

@CrossOrigin(origins = YumPmsConstants.ANGULAR_DEV_URL)
@Controller
public class PAController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PAController.class);

	@Autowired
	private IPAService paService;

	/**
	 * @param empId
	 * @param apprId
	 * @param role
	 * @return List of Pa Sheet Data
	 */
	@RequestMapping(value = "/getPASheetData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<PASheetData>> getPASheetData(
			@RequestParam(YumPmsConstants.EMP_ID) String empId, 
			@RequestParam(YumPmsConstants.APPR_ID) String apprId, 
			@RequestParam(YumPmsConstants.ROLE) String role) {
		List<PASheetData> paSheetDataList = null; 
		try {
			paSheetDataList = paService.getPASheetData(NumberUtils.toInt(empId), NumberUtils.toInt(apprId), role);
			return new ResponseEntity<>(paSheetDataList, HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(paSheetDataList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @param saveObjectList
	 * @return the saved list of pa sheet data
	 */
	@RequestMapping(value = "/savePaSheet", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<PASheetData>> savePaSheet(@RequestBody List<PASheetData> saveObjectList) {
		List<PASheetData> objectListAfterSave = null;
		try {
			objectListAfterSave = paService.savePASheetData(saveObjectList);    
			return new ResponseEntity<>(objectListAfterSave, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(objectListAfterSave, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * get Rating BellCurve IFRange value
	 * @param apprId
	 * @return
	 */
	@RequestMapping(value = "/getRatingBellCurveIFRange", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<PA_ConfigRatingBellCurveIFRange>> getRatingBellCurveIFRange(@RequestParam(YumPmsConstants.APPR_ID) String apprId) {
		List<PA_ConfigRatingBellCurveIFRange> bellCurveIFRange = null;
		try {
			bellCurveIFRange = paService.getIfRange(NumberUtils.toInt(apprId));
			return new ResponseEntity<>(bellCurveIFRange, HttpStatus.OK);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(bellCurveIFRange, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * get Next WorkFlow Status
	 * @param paSheetId
	 * @return
	 */
	@RequestMapping(value = "/getNextWorkFlowStatusByPaSheetId/{paSheetId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StatusWorkFlow> getNextWorkStatusByGoalSheetId(@PathVariable Integer paSheetId) {
		LOGGER.info("getNextWorkFlowStatusByPaSheetId>>>>>..Param is>>>>.. paSheetId: {}", paSheetId);  
		StatusWorkFlow nextStatus = null;
		try { 
			nextStatus =  paService.getWorkStatusByPaSheetId(paSheetId);
		}catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(nextStatus, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(nextStatus, HttpStatus.OK);
	}

	/**
	 * update PaSheetStatus table
	 * @param params
	 * @return
	 */
	@RequestMapping(value = "/updatePaSheetStatus", method = RequestMethod.POST, 
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StatusWorkFlow> updateGoalSheetStatus(@RequestBody Map<String, String> params) { 
		
		int goalSheetId = NumberUtils.toInt(params.get(YumPmsConstants.PA_SHEET_ID));
		int statusId = NumberUtils.toInt(params.get(YumPmsConstants.PA_STATUS));
		String createdBy = params.get(YumPmsConstants.CREATED_BY);
		boolean offDiscussedAndAgreed = BooleanUtils.toBoolean(params.get(YumPmsConstants.DISCUSSED_AND_AGREED));
		
		StatusWorkFlow nextStatus = null;		
		try {
			if(YumPmsConstants.PA_SHEET_DISCUSS_AND_AGREED_STATUS_ID.equals(statusId)) {
				nextStatus = offDiscussedAndAgreed ? 
						paService.updatePaSheetStatus(goalSheetId, statusId, createdBy) 
						: new StatusWorkFlow(); 
			} else {
				nextStatus = paService.updatePaSheetStatus(goalSheetId, statusId, createdBy); 
			}
			if(nextStatus != null) {
				nextStatus.setMessage(StringUtils.isEmpty(nextStatus.getMessage()) ? YumPmsConstants.UPDATED_SUCESSFULLY : nextStatus.getMessage()); 
			}
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(nextStatus, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(nextStatus, HttpStatus.OK);
	}
	
	
	@RequestMapping(value = "/promotionEligibilityCheck", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<PASheetData>> promotionEligibilityCheck(@RequestBody List<PASheetData> uiObjectList) {
		List<PASheetData> promotionEligibleList = null;
		try {
			promotionEligibleList = paService.promotionEligibilityCheck(uiObjectList);
			return new ResponseEntity<>(promotionEligibleList, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(promotionEligibleList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/getPromotionPercentIndicator", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Object>> getPromotionPercentIndicator(@RequestParam(YumPmsConstants.APPR_ID) Integer apprId) {
		
		Map<String, Object> response = new HashMap<>();
		response.put(YumPmsConstants.APPR_ID, apprId);
		try {
			Double percentage = paService.getPromotionPercentage(apprId);
			response.put(YumPmsConstants.PROMOTION_PERCENTAGE, percentage);
			response.put(YumPmsConstants.STATUS_MSG, YumPmsConstants.SUCCESS_FETCH_MSG);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			response.put(YumPmsConstants.PROMOTION_PERCENTAGE, null);
			response.put(YumPmsConstants.STATUS_MSG, ex.getMessage());
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/savePromotionPercentIndicator", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, Object>> savePromotionPercentIndicator(@RequestBody Map<String, Object> request) {
		
		Map<String, Object> response = new HashMap<>();
		try {
			Integer apprId = YumPmsUtils.castToInt(request.get(YumPmsConstants.APPR_ID));
			response.put(YumPmsConstants.APPR_ID, apprId);
			Double percentage = YumPmsUtils.castToDouble(request.get(YumPmsConstants.PROMOTION_PERCENTAGE));
			String loginUser = YumPmsUtils.toString(request.get(YumPmsConstants.LOGON_USER));
			
			Double savedPercentage = 
					paService.savePromotionPercentage(apprId, percentage, loginUser);
			
			response.put(YumPmsConstants.PROMOTION_PERCENTAGE, savedPercentage);
			response.put(YumPmsConstants.STATUS_MSG, YumPmsConstants.SUCCESS_FETCH_MSG);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			response.put(YumPmsConstants.PROMOTION_PERCENTAGE, null);
			response.put(YumPmsConstants.STATUS_MSG, ex.getMessage());
			return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
