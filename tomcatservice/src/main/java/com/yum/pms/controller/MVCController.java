/******************************************************************** 
 * Spring Controller for main module 
 * 
 * http://www.abzooba.com
 * ------------------- 
 * Created By: sudipta.chandra
 * Last Updated By: sudipta.chandra
 * Last Updated On: 23-Sep-2017 (11:34:34 AM) 
 *
 * The software and related user documentation are protected under 
 * copyright laws and remain the sole property of Abzooba. 
 * 
 * Technical support is available via the Abzooba website at 
 * http://www.abzooba.com
 **************************************************************************/

package com.yum.pms.controller;

import java.sql.SQLException;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.yum.pms.dao.AppraisalPeriod;
import com.yum.pms.dao.PmsRole;
import com.yum.pms.domain.CascadedDetails;
import com.yum.pms.domain.ClientEmployee;
import com.yum.pms.domain.EmployeeCurrentDetails;
import com.yum.pms.domain.EmployeeGoalHistory;
import com.yum.pms.domain.EmployeeGoalSheet;
import com.yum.pms.domain.GoalSectionSupRemarks;
import com.yum.pms.domain.ReporteeCascadedEmp;
import com.yum.pms.domain.ReporteeTaggedEmp;
import com.yum.pms.domain.StatusWorkFlow;
import com.yum.pms.domain.TaggedDetails;
import com.yum.pms.service.IYumHibernateService;
import com.yum.pms.utils.YumPmsConstants;

@CrossOrigin(origins = YumPmsConstants.ANGULAR_DEV_URL)
@Controller
public class MVCController {

	private static final Logger LOGGER = LoggerFactory.getLogger(MVCController.class);

	@Autowired
	private IYumHibernateService service;

	@RequestMapping(value = "/getdropdownlist/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String,Object>> getUser(@PathVariable String name) {

		LOGGER.info("DropDown For>>>>.. {}", name);   
		Map<String, Object> dropDownList = null;
		try { 
			dropDownList = service.getDropDownList(name);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(dropDownList, HttpStatus.INTERNAL_SERVER_ERROR);			
		}
		return new ResponseEntity<>(dropDownList, HttpStatus.OK);		
	}

	@RequestMapping(value = "/getemployeelist", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<Integer, String>> getEmployeeList(@RequestParam(YumPmsConstants.PARAM_USER_ID) String userid, 
			@RequestParam(value = YumPmsConstants.PARAM_APPR_ID, required = false) Integer apprId,
			@RequestParam(value = YumPmsConstants.PARAM_NON_DR, required = false) Boolean nonDr) {

		Map<Integer, String> employeeListWithId = null;
		try { 
			String userId =  new String(Base64.getDecoder().decode(userid));
			LOGGER.info(YumPmsConstants.LOG_USER_ID, userId);
			employeeListWithId = service.getEmployeeListByManagerId(userId, apprId, nonDr); 
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(employeeListWithId, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(employeeListWithId, HttpStatus.OK);		
	}

	@RequestMapping(value = "/getemployeedetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EmployeeCurrentDetails> getEmployeeDetails(@RequestParam(YumPmsConstants.PARAM_USER_ID) String userid, 
			@RequestParam(value = YumPmsConstants.PARAM_APPR_ID, required = false) Integer apprId) {  

		EmployeeCurrentDetails employee = null;
		try { 
			String userId =  new String(Base64.getDecoder().decode(userid));
			LOGGER.info(YumPmsConstants.LOG_USER_ID, userId); 
			employee = service.getEmployeeDetailsByUserId(userId, apprId, YumPmsConstants.USER_NAME);
			if(!BooleanUtils.toBoolean(employee.getActive())) {
				EmployeeCurrentDetails tempEmp = new EmployeeCurrentDetails();
				tempEmp.setEmpId(employee.getEmpId());
				tempEmp.setEmail(employee.getEmail());
				tempEmp.setEmpName(employee.getEmpName());
				employee = tempEmp;
			}
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(employee, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(employee, HttpStatus.OK);		
	}

	@RequestMapping(value = "/getroledetailsbyroleId/{roleid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PmsRole> getRoleDetailsByRoleId(@PathVariable String roleid) {

		LOGGER.info("role id id is>>>>.. {}", roleid);   
		PmsRole pmsRole = null;
		try { 
			pmsRole = service.getRoleDetailsByRoleId(roleid);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(pmsRole, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(pmsRole, HttpStatus.OK);		
	}

	@RequestMapping(value = "/getemployeegoalsheetbyid", params = {YumPmsConstants.PARAM_EMP_ID, YumPmsConstants.PARAM_APPR_ID}, 
			method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EmployeeGoalSheet> getEmployeeGoalSheetById(@RequestParam(YumPmsConstants.PARAM_EMP_ID) Integer empId, 
			@RequestParam(value = YumPmsConstants.PARAM_APPR_ID, required = false) Integer appraisalId) {

		LOGGER.info("Param is>>>>.. empId:{}; appraisalId: {}", empId, appraisalId);  
		EmployeeGoalSheet employeeGoalSheet = null;
		try { 
			employeeGoalSheet =  service.getGoalSheetDetailsByEmployeeId(empId, appraisalId, false); 
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(employeeGoalSheet, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(employeeGoalSheet, HttpStatus.OK);
	}

	@RequestMapping(value = "/getAllEmployeeList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ClientEmployee>> getAllEmployeeList(@RequestParam(YumPmsConstants.EMP_ID) Integer empId, 
			@RequestParam(value = YumPmsConstants.APPR_ID, required = false) Integer apprId, 
			@RequestParam(value = YumPmsConstants.IS_DEACTIVE, required = false) Boolean isDeActive) {

		LOGGER.info("get all Employee List without empId>>>>.. {} and isDeactive:>>>>>>>>>.. {}", empId, isDeActive);   
		List<ClientEmployee> employeeListWithId = null;
		try { 
			employeeListWithId = service.getAllEmployeeList(empId, apprId, isDeActive); 
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(employeeListWithId, HttpStatus.INTERNAL_SERVER_ERROR);	
		}
		return new ResponseEntity<>(employeeListWithId, HttpStatus.OK);		
	}

	@RequestMapping(value = "/saveGoalSheetData", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<EmployeeGoalSheet> saveGoalSheetData(@RequestBody EmployeeGoalSheet employeeGoalSheet) {

		LOGGER.info("saveGoalSheetData >>>> goal sheet id >>>>.. {}",employeeGoalSheet.getGoal_sheet_id());
		try {
			employeeGoalSheet = service.saveGoalSheet(employeeGoalSheet);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(employeeGoalSheet, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(employeeGoalSheet, HttpStatus.OK);
	}

	@RequestMapping(value = "/getTaggedInfo/{goalSectionId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<TaggedDetails>> gettaggedInfo(@PathVariable Integer goalSectionId) {

		LOGGER.info("Param is>>>>.. goalSectionId: {}", goalSectionId);  
		List<TaggedDetails> taggedDetailsList = null;
		try { 
			taggedDetailsList =  service.getTaggedInfo(goalSectionId);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(taggedDetailsList, HttpStatus.INTERNAL_SERVER_ERROR);
		}	

		return new ResponseEntity<>(taggedDetailsList, HttpStatus.OK);
	}

	@RequestMapping(value = "/getEmpTaggedInfo", params = {"tagid", "reporteeid"}, method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ReporteeTaggedEmp> getemployeetaggedInfo(@RequestParam(YumPmsConstants.PARAM_TAGID) Integer tagId, 
			@RequestParam(YumPmsConstants.PARAM_REPORTEEID) Integer reporteeId){

		LOGGER.info("Param is>>>>.. tagId: {} ....reporteeId {} ", tagId, reporteeId);  
		ReporteeTaggedEmp reporteeTaggedEmp = null;
		try { 
			reporteeTaggedEmp =  service.getEmpTaggedInfo(tagId,reporteeId);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(reporteeTaggedEmp, HttpStatus.INTERNAL_SERVER_ERROR);			
		}	
		return new ResponseEntity<>(reporteeTaggedEmp, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getEmpCascadedInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ReporteeCascadedEmp> getemployeecascadedInfo(@RequestParam("cascadeid") Integer cascadeId, 
			@RequestParam(YumPmsConstants.PARAM_REPORTEEID) Integer reporteeId){

		LOGGER.info("Param is>>>>.. cascadeId: {} ....reporteeId {}", cascadeId, reporteeId);  
		ReporteeCascadedEmp reporteeCascadedEmp = null;
		try { 
			reporteeCascadedEmp =  service.getEmpCascadedInfo(cascadeId,reporteeId);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(reporteeCascadedEmp, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(reporteeCascadedEmp, HttpStatus.OK);
	}

	@RequestMapping(value = "/updateTaskRemarks", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Properties> updateTaskRemarks(@RequestBody GoalSectionSupRemarks params) {

		LOGGER.info("Param is>>>>.. goalSectionId: {}.. goalSectionSupRemarks: {}", params.getGoalSectionId(), params.getGoalSectionSupRemarks());  
		Properties prop = new Properties();
		try { 
			service.updateTaskRemarks(params.getGoalSectionId(), params.getGoalSectionSupRemarks(), params.getCreatedBy());
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			prop.put("msg", "Failure");
			return new ResponseEntity<>(prop, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		prop.put("msg", "Done");
		return new ResponseEntity<>(prop, HttpStatus.OK);
	}

	@RequestMapping(value = "/getCascadedInfo/{goalSectionId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<CascadedDetails>> getCascadedInfo(@PathVariable Integer goalSectionId) {

		LOGGER.info("Param is>>>>.. goalSectionId: {}", goalSectionId);  
		List<CascadedDetails> cascadedDetailsList = null;
		try { 
			cascadedDetailsList =  service.getCascadedInfo(goalSectionId);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(cascadedDetailsList, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(cascadedDetailsList, HttpStatus.OK);
	}

	@RequestMapping(value = "/saveTagData", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TaggedDetails> saveTagData(@RequestBody TaggedDetails taggedDetails) {

		LOGGER.info("Param is>>>>.. tagged Id: {}", taggedDetails.getTagId());
		try {
			taggedDetails = service.saveTaggedData(taggedDetails);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(taggedDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(taggedDetails, HttpStatus.OK);
	}

	@RequestMapping(value = "/saveCascadedData", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<CascadedDetails> saveCascadedData(@RequestBody CascadedDetails cascadedDetails) {

		LOGGER.info("Param is>>>>.. Cascade Id: {}", cascadedDetails.getCascadeId());
		try {
			cascadedDetails = service.saveCascadedData(cascadedDetails);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(cascadedDetails, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(cascadedDetails, HttpStatus.OK);
	}

	@RequestMapping(value = "/deleteTag", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> deleteTag(@RequestParam(YumPmsConstants.PARAM_TAGID) Integer tagid) {

		LOGGER.info("Removing tag: {}", tagid);
		try {
			service.deleteTag(tagid);
			return new ResponseEntity<>(true, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(false, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/deleteCascade", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> deleteCascade(@RequestParam(YumPmsConstants.PARAM_CASCADEID) Integer cascadeid) {

		LOGGER.info("Removing cascadeid: {}", cascadeid);
		try {
			service.deleteCascade(cascadeid);
			return new ResponseEntity<>(true, HttpStatus.OK);
		} catch (Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(false, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/getNextWorkStatusByGoalSheetId/{goalSheetId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StatusWorkFlow> getNextWorkStatusByGoalSheetId(@PathVariable Integer goalSheetId) {

		LOGGER.info("getNextWorkStatusByGoalSheetId>>>>>..Param is>>>>.. goalSheetId: {}", goalSheetId);  
		StatusWorkFlow nextStatus = null;
		try { 
			nextStatus =  service.getWorkStatusByGoalSheetId(goalSheetId);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(nextStatus, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(nextStatus, HttpStatus.OK);
	}

	@RequestMapping(value = "/UpdateGoalSheetStatus", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, 
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<StatusWorkFlow> updateGoalSheetStatus(@RequestBody Map<String, Object> params) { 

		int goalSheetId = (int) params.get(YumPmsConstants.PARAM_GOAL_SHEET_ID);
		int statusId = (int) params.get(YumPmsConstants.DROP_DOWN_GOAL_STATUS);
		String createdBy = (String) params.get(YumPmsConstants.CREATED_BY);
		boolean offDiscussedAndAgreed = null != params.get(YumPmsConstants.DISCUSSED_AND_AGREED) ? 
				(boolean) params.get(YumPmsConstants.DISCUSSED_AND_AGREED) : false;
		StatusWorkFlow nextStatus = null;		
		LOGGER.info("UpdateGoalSheetStatus>>>>..Param is>>>>.. goalSheetId:{}; statusId: {}", goalSheetId, statusId);  
		try {
			if(statusId == 1005) {
				nextStatus = offDiscussedAndAgreed ? service.updateGoalSheetStatus(goalSheetId, statusId, createdBy) : new StatusWorkFlow() ; 
			} else {
				nextStatus =  service.updateGoalSheetStatus(goalSheetId, statusId, createdBy); 
			}
			nextStatus.setMessage(null == nextStatus.getMessage() ? YumPmsConstants.UPDATED_SUCESSFULLY : nextStatus.getMessage()); 
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(nextStatus, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(nextStatus, HttpStatus.OK);
	}

	@RequestMapping(value = "/getdefappperiod", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AppraisalPeriod> getDefaultAppraisalPeriod() {  

		AppraisalPeriod appraisalPeriod = null;
		try { 
			appraisalPeriod = service.getDefaultAppraisalPeriod();
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(appraisalPeriod, HttpStatus.INTERNAL_SERVER_ERROR);		
		}
		return new ResponseEntity<>(appraisalPeriod, HttpStatus.OK);		
	}

	@RequestMapping(value = "/getRolelist", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Map<String, Object>>> getUser() {

		LOGGER.info("getRolelist >>>>.. from {}", "admin");   
		List<Map<String, Object>> dropDownList = null;
		try { 
			dropDownList = service.getUserRollList();
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(dropDownList, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(dropDownList, HttpStatus.OK);		
	}

	@RequestMapping(value = "/getworkflowstatusforgoalsheet", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<EmployeeGoalHistory>> getWorkflowStatusForGoalSheet(
			@RequestParam(YumPmsConstants.PARAM_GOALSHEETID) Integer goalSheetId) {  

		List<EmployeeGoalHistory> workflowHistory = null;
		try { 
			workflowHistory = service.getLastWorkFlowHistoryStatus(goalSheetId);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(workflowHistory, HttpStatus.INTERNAL_SERVER_ERROR);		
		}
		return new ResponseEntity<>(workflowHistory, HttpStatus.OK);		
	}

	@RequestMapping(value = "/getServerSessionTimeOut", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Properties> getSessionTimeOut(HttpSession httpSession) {
		Properties prop = new Properties();
		try {
			httpSession.setMaxInactiveInterval(Integer.parseInt(service.getServerTimeOutTime())); // 2 hours set as per client request 7200  420 for 2 minuts
			prop.put("timeout", String.valueOf(httpSession.getMaxInactiveInterval()));
		}catch (SQLException e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return new ResponseEntity<>(new Properties(), HttpStatus.INTERNAL_SERVER_ERROR);	
		}
		return new ResponseEntity<>(prop, HttpStatus.OK);
	}

	/**
	 * @return All help text from database
	 */
	@RequestMapping(value = "/getTooltipText", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Map<String, String>> getTooltipText(@RequestParam(YumPmsConstants.PARAM_EMP_GRADE) String empGrade) {

		Map<String, String> tooltipMap = null;
		try {
			tooltipMap = service.getAllTooltips(empGrade);
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(tooltipMap, HttpStatus.INTERNAL_SERVER_ERROR);	
		}
		return new ResponseEntity<>(tooltipMap, HttpStatus.OK);
	}
	
	/**
	 method name: getAppraisalPeriordList
	 * @return List of AppraisalPeriord model class
	 */
	@RequestMapping(value = "/getAppraisalPeriord", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<AppraisalPeriod>> getAppraisalPeriord() {

		List<AppraisalPeriod> appraisalPeriodList = null;
		try { 
			appraisalPeriodList =  service.getApprPeriodList();
		} catch(Exception ex) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, ex);
			return new ResponseEntity<>(appraisalPeriodList, HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		return new ResponseEntity<>(appraisalPeriodList, HttpStatus.OK);
	}

}

