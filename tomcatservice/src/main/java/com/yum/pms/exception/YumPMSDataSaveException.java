/******************************************************************** 
* Exception Handler for Failure in Saving Hibernate Objects 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: sudipta.chandra
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (1:39:30 PM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.exception;

public class YumPMSDataSaveException extends Exception {


	private static final long serialVersionUID = 1L;

	public YumPMSDataSaveException() {
	}

	public YumPMSDataSaveException(String message) {
		super(message);
	}
	
	public YumPMSDataSaveException(String message, String tableName) {
		super(message + " " + tableName);
	}

	public YumPMSDataSaveException(Throwable cause) {
		super(cause);
	}

	public YumPMSDataSaveException(String message, Throwable cause) {
		super(message, cause);
	}

	public YumPMSDataSaveException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
