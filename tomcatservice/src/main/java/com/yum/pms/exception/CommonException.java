package com.yum.pms.exception;

/**
 * This is a common unchecked exception class used to wrap all exception
 * @author dipak.swain
 */
public class CommonException extends RuntimeException {

	private static final long serialVersionUID = -2131041343387592235L;

	public CommonException() {
		super();
	}
	
	public CommonException(String msg) {
		super(msg);
	}
	
	public CommonException(String msg, String arg) {
		super(new StringBuilder(msg).append(arg).toString());
	}
	
	public CommonException(Throwable ex) {
		super(ex);
	}
	
}
