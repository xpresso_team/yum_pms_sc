package com.yum.pms.exception;

/**
 * A Custom unchecked exception which deals with class path resources
 * @author dipak.swain
 */
public class ResourceNotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = 287701901861768267L;

	public ResourceNotFoundException() {
		super("Resource is not available in class path");
	}
	
	public ResourceNotFoundException(String msg) {
		super(msg);
	}
	
	public ResourceNotFoundException(Throwable ex) {
		super(ex);
	}
	
	public ResourceNotFoundException(String property, String fileName) {
		super(new StringBuilder(property).append(" is not available in ").append(fileName).toString());
	}
	
}
