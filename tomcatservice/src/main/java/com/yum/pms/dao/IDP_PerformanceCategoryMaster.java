package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreType;

/**
 * @author jayanta.biswas
 * @comment This is a pojo class for data sending
 * @version 1.0
 * @since   24-01-2018
 */
@Entity
@Table(name = "idp_performance_category_mstr")
@JsonIgnoreType
public class IDP_PerformanceCategoryMaster implements Serializable {

	private static final long serialVersionUID = 2334468201776963685L;

	@Id
	@Column(name="idp_performance_category_id", unique = true, nullable = false)
	private Integer idpPerformanceCategoryId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="idp_performance_id")
	private IDP_PerformanceMaster idpPerformanceMaster;
	
	@Column(name="idp_performance_category_desc")
	private String idpPerformanceCategoryDesc;
	
	@Column(name="CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate")
	private Date createdDate;
	
	@Column(name="UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UpdatedDate")
	private Date updatedDate;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "idpPerformanceCategoryMaster")
	private Set<IDP_PerformanceRating> performanceRating = new HashSet<>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "idpPerformanceCategoryMaster")
	private Set<IDP_PerformanceUIConfig> idpPerformanceUIConfig = new HashSet<>(0);
	
	public IDP_PerformanceCategoryMaster() {
	}
	
	public IDP_PerformanceCategoryMaster(Integer idpPerformanceCategoryId) {
		this.idpPerformanceCategoryId = idpPerformanceCategoryId;
	}

	public Integer getIdpPerformanceCategoryId() {
		return idpPerformanceCategoryId;
	}

	public void setIdpPerformanceCategoryId(Integer idpPerformanceCategoryId) {
		this.idpPerformanceCategoryId = idpPerformanceCategoryId;
	}

	public IDP_PerformanceMaster getIdpPerformanceMaster() {
		return idpPerformanceMaster;
	}

	public void setIdpPerformanceMaster(IDP_PerformanceMaster idpPerformanceMaster) {
		this.idpPerformanceMaster = idpPerformanceMaster;
	}

	public String getIdpPerformanceCategoryDesc() {
		return idpPerformanceCategoryDesc;
	}

	public void setIdpPerformanceCategoryDesc(String idpPerformanceCategoryDesc) {
		this.idpPerformanceCategoryDesc = idpPerformanceCategoryDesc;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Set<IDP_PerformanceRating> getPerformanceRating() {
		return performanceRating;
	}

	public void setPerformanceRating(Set<IDP_PerformanceRating> performanceRating) {
		this.performanceRating = performanceRating;
	}

	public Set<IDP_PerformanceUIConfig> getIdpPerformanceUIConfig() {
		return idpPerformanceUIConfig;
	}

	public void setIdpPerformanceUIConfig(Set<IDP_PerformanceUIConfig> idpPerformanceUIConfig) {
		this.idpPerformanceUIConfig = idpPerformanceUIConfig;
	}

}
