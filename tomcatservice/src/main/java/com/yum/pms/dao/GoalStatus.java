/******************************************************************** 
* Hibernate DAO Object Mapper for goal_sheet_status table 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.dao;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreType;

@Entity
@Table(name = "goal_sheet_status")
@JsonIgnoreType
public class GoalStatus implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="goal_sheet_status_id")
	private Integer goalStatusId;
	
	@Column(name="goal_sheet_status_name")
	private String goalStatusName;
	
	@Column(name="active")
	private Boolean active = true;
	
	@Column(name="CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate", length = 23)
	private Date createdDate;
	
	@Column(name = "UpdatedBy", length = 100)
	private String updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate", length = 23)
	private Date updatedDate;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "goalStatus")
	private Set<GoalSheet> goalSheet = new HashSet<>(0);
	

	public GoalStatus() {
	}
	
	public GoalStatus(Integer goalStatusId) {
		this.goalStatusId = goalStatusId;
	}

	public Integer getGoalStatusId() {
		return this.goalStatusId;
	}

	public void setGoalStatusId(Integer goalStatusId) {
		this.goalStatusId = goalStatusId;
	}

	public String getGoalStatusName() {
		return this.goalStatusName;
	}

	public void setGoalStatusName(String goalStatusName) {
		this.goalStatusName = goalStatusName;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Set<GoalSheet> getGoalSheet() {
		return goalSheet;
	}

	public void setGoalSheet(Set<GoalSheet> goalSheet) {
		this.goalSheet = goalSheet;
	}


}
