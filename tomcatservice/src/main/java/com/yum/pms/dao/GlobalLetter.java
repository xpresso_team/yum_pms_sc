package com.yum.pms.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * @author dipak.swain
 * This is a model class for 'global_letters' table
 */
@Entity
@Table(name = "global_letter")
public class GlobalLetter implements java.io.Serializable {

	private static final long serialVersionUID = -7243264425038391912L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "global_letter_id", unique = true, nullable = false)
	private Integer globalLettersId;

	@ManyToOne
	@JoinColumn(name = "emp_id")
	private EmployeeNew employee;
	
	@ManyToOne
	@JoinColumn(name = "appr_id")
	private AppraisalPeriod appraisalPeriod;
	
	@Column(name = "letter_file_name")
	private String letterFileName;
	
	@Column(name = "letter_file_path")
	private String letterFilePath;
	
	@Column(name = "letter_desc")
	private String letterDesc;
	
	@Column(name = "CreatedBy")
	private String createdBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Transient
	private String statusMsg;
	
	@Transient
	private Integer apprId;
	
	@Transient
	private Integer empId;
	
	public GlobalLetter() {
	}

	public GlobalLetter(Integer apprId, Integer empId, String fileDesc) {
		this.apprId = apprId;
		this.empId = empId;
		this.appraisalPeriod = new AppraisalPeriod(apprId);
		this.employee = new EmployeeNew(empId);
		this.letterDesc = fileDesc;
	}

	public Integer getGlobalLettersId() {
		return globalLettersId;
	}

	public void setGlobalLettersId(Integer globalLettersId) {
		this.globalLettersId = globalLettersId;
	}

	public EmployeeNew getEmployee() {
		return employee;
	}

	public void setEmployee(EmployeeNew employee) {
		this.employee = employee;
	}

	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	public Integer getApprId() {
		return apprId;
	}

	public void setApprId(Integer apprId) {
		this.apprId = apprId;
	}

	public String getLetterFileName() {
		return letterFileName;
	}

	public void setLetterFileName(String letterFileName) {
		this.letterFileName = letterFileName;
	}

	public String getLetterFilePath() {
		return letterFilePath;
	}

	public void setLetterFilePath(String letterFilePath) {
		this.letterFilePath = letterFilePath;
	}

	public String getLetterDesc() {
		return letterDesc;
	}

	public void setLetterDesc(String letterDesc) {
		this.letterDesc = letterDesc;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
}
