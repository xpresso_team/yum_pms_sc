package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Entity
@Table(name = "comp_yearwise_multiplier")
public class CompYearWiseMultiplier implements Serializable {

	private static final long serialVersionUID = 3549313134093840475L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "yearwise_multiplier_id", unique = true, nullable = false)
	private Integer yearWiseMultiplierId;
	
	@ManyToOne
	@JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@ManyToOne
    @JoinColumn(name = "rating_id")
	private PA_RatingMaster rating;
	
	@Column(name = "multiplier")
	private Float multiplier;
	
	@Column(name = "IsActive")
	private Boolean active = true;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	@Transient
	private Integer apprId;// As we are ignoring appraisalPeriod property; Hence we have added this field in order to used in JSON
	
	public CompYearWiseMultiplier() {
	}
	
	public CompYearWiseMultiplier(PA_RatingMaster rating, Integer apprId) {
		this.rating = rating;
		this.apprId = apprId;
	}

	public CompYearWiseMultiplier(Integer yearWiseMultiplierId) {
		this.yearWiseMultiplierId = yearWiseMultiplierId;
	}

	public Integer getYearWiseMultiplierId() {
		return yearWiseMultiplierId;
	}

	public void setYearWiseMultiplierId(Integer yearWiseMultiplierId) {
		this.yearWiseMultiplierId = yearWiseMultiplierId;
	}

	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	public PA_RatingMaster getRating() {
		return rating;
	}

	public void setRating(PA_RatingMaster rating) {
		this.rating = rating;
	}

	public Float getMultiplier() {
		return multiplier;
	}

	public void setMultiplier(Float multiplier) {
		this.multiplier = multiplier;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	public Integer getApprId() {
		return apprId;
	}

	public void setApprId(Integer apprId) {
		this.apprId = apprId;
	}
	
}
