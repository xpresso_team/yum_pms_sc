package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.yum.pms.utils.YumPmsConstants;

/**
 * @author Ashis.Nayak
 * Table representation of model class
 */
@Entity
@Table(name = "help_tip_document_mstr")
public class HelpTipDocumentMaster implements Serializable {
	
	private static final long serialVersionUID = 1293846442041626941L;

	@Id
	@Column(name = "help_tip_document_id")
	private Integer helpTipDocumentId;
	
	@Column(name = "appr_id")
	private Integer apprId;

	@Column(name= "help_tip_document_url")
	private String helpTipDocumentUrl;
	
	@Column(name= "is_manager")
	private Integer isManager;
	
	@Column(name= "page_type")
	private String pageType;
	
	@Column(name= "is_active")
	private String isActive;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
	
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	@Column(name= "is_default")
	private boolean isDefault;
	
	public HelpTipDocumentMaster() {
		// Not being specified yet
	}
	
	public Integer getHelpTipDocumentId() {
		return helpTipDocumentId;
	}

	public void setHelpTipDocumentId(Integer helpTipDocumentId) {
		this.helpTipDocumentId = helpTipDocumentId;
	}

	public Integer getApprId() {
		return apprId;
	}

	public void setApprId(Integer apprId) {
		this.apprId = apprId;
	}

	public String getHelpTipDocumentUrl() {
		return helpTipDocumentUrl;
	}

	public void setHelpTipDocumentUrl(String helpTipDocumentUrl) {
		this.helpTipDocumentUrl = helpTipDocumentUrl;
	}

	public Integer getIsManager() {
		return isManager;
	}

	public void setIsManager(Integer isManager) {
		this.isManager = isManager;
	}

	public String getPageType() {
		return pageType;
	}

	public void setPageType(String pageType) {
		this.pageType = pageType;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}


		
}
