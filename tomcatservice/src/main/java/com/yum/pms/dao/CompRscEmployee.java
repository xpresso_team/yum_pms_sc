package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "comp_rsc_move_employee")
public class CompRscEmployee implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer rscId;
	
	@ManyToOne
	@JoinColumn(name = "emp_id")
	private EmployeeNew employee;
	
	@ManyToOne
	@JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="rsc_move_date")
	private Date rscMoveDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="rsc_leave_date")
	private Date rscLeaveDate;
	
	public CompRscEmployee() {
	}
	
	public CompRscEmployee(Integer rscId) {
		this.rscId = rscId;
	}

	public Integer getRscId() {
		return rscId;
	}

	public void setRscId(Integer rscId) {
		this.rscId = rscId;
	}

	public EmployeeNew getEmployee() {
		return employee;
	}

	public void setEmployee(EmployeeNew employee) {
		this.employee = employee;
	}

	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	public Date getRscMoveDate() {
		return rscMoveDate;
	}

	public void setRscMoveDate(Date rscMoveDate) {
		this.rscMoveDate = rscMoveDate;
	}

	public Date getRscLeaveDate() {
		return rscLeaveDate;
	}

	public void setRscLeaveDate(Date rscLeaveDate) {
		this.rscLeaveDate = rscLeaveDate;
	}
	
}
