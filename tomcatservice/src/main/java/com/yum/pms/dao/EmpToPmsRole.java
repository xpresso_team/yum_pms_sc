/******************************************************************** 
* Hibernate DAO Object Mapper for emp_to_pms_role table 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.dao;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@Table(name = "emp_to_pms_role")
//@JsonIgnoreType
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
public class EmpToPmsRole implements java.io.Serializable {

	private static final long serialVersionUID = -6504087611573710776L;

	@EmbeddedId
	@AttributeOverrides({ @AttributeOverride(name = "empId", column = @Column(name = "emp_id", nullable = false)),
	@AttributeOverride(name = "pmsRoleId", column = @Column(name = "pms_role_id", nullable = false)) })
	private EmpToPmsRoleId id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "emp_id", nullable = false, insertable = false, updatable = false)
	private EmployeeNew employee;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pms_role_id", nullable = false, insertable = false, updatable = false)
	private PmsRole pmsRole;
	
	@Column(name = "CreatedBy", length = 100)
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate", length = 23)
	private Date createdDate;
	
	@Column(name = "UpdatedBy", length = 100)
	private String updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate", length = 23)
	private Date updatedDate;

	public EmpToPmsRole() {
	}
	
	public EmpToPmsRole(EmpToPmsRoleId id) {
		this.id = id;
	}
	
	public EmpToPmsRoleId getId() {
		return this.id;
	}

	public void setId(EmpToPmsRoleId id) {
		this.id = id;
	}


	public EmployeeNew getEmployee() {
		return this.employee;
	}

	public void setEmployee(EmployeeNew employee) {
		this.employee = employee;
	}


	public PmsRole getPmsRole() {
		return this.pmsRole;
	}

	public void setPmsRole(PmsRole pmsRole) {
		this.pmsRole = pmsRole;
	}

	
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
