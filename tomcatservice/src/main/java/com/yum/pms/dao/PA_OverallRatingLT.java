package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author dipak.swain
 * This is a model class for 'pa_overall_rating_LT' table
 */
@Entity
@Table(name = "pa_overall_rating_LT")
@JsonIgnoreType
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@paOverallRatingLTId")
public class PA_OverallRatingLT implements Serializable {
	
	private static final long serialVersionUID = 7724632949132607312L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pa_overall_rating_LT_id")
	private Integer paOverallRatingLTId;
	
	@ManyToOne
    @JoinColumn(name = "pa_sheet_id")
	private PA_SheetMaster paSheet;
	
	@ManyToOne
    @JoinColumn(name = "pa_rating_id")
	private PA_RatingMaster paRating;
	
	@ManyToOne
    @JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@ManyToOne
    @JoinColumn(name = "emp_id")
	private EmployeeNew emp;
	
	@ManyToOne
    @JoinColumn(name = "LT_emp_id")
	private EmployeeNew ltEmp;
	
	@Column(name = "LT_remark")
	private String ltRemark;
	
	@Column(name = "LT_promo_nom")
	private Boolean ltPromoNom;
	
	@Column(name = "LT_IF")
	private Double ltIF;
	
	@Column(name = "LT_additional_increase_nom")
	private Boolean additionalIncreaseNom;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	@Column(name = "is_eligable")
	private Boolean isEligable;
	
	@Column(name = "promotion_criteria_id")
	private Integer promotionCriteriaId;
	
	@Column(name = "LT_promo_nom_aug")
	private Boolean ltPromoNomAug = false;
	
	@Column(name = "dev_check_in")
	private Boolean devCheckIn;
	
	@Column(name = "dev_check_in_remarks")
	private String devCheckInRremarks;
	
	@ManyToOne
    @JoinColumn(name = "culture_rating_id")
	private PaCultureRatingMstr cultureRatingMstr;
	

	public Integer getPaOverallRatingLTId() {
		return paOverallRatingLTId;
	}

	public void setPaOverallRatingLTId(Integer paOverallRatingLTId) {
		this.paOverallRatingLTId = paOverallRatingLTId;
	}

	public PA_SheetMaster getPaSheet() {
		return paSheet;
	}

	public void setPaSheet(PA_SheetMaster paSheet) {
		this.paSheet = paSheet;
	}

	public PA_RatingMaster getPaRating() {
		return paRating;
	}

	public void setPaRating(PA_RatingMaster paRating) {
		this.paRating = paRating;
	}

	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	public EmployeeNew getEmp() {
		return emp;
	}

	public void setEmp(EmployeeNew emp) {
		this.emp = emp;
	}

	public EmployeeNew getLtEmp() {
		return ltEmp;
	}

	public void setLtEmp(EmployeeNew ltEmp) {
		this.ltEmp = ltEmp;
	}

	public String getLtRemark() {
		return ltRemark;
	}

	public void setLtRemark(String ltRemark) {
		this.ltRemark = ltRemark;
	}

	public Boolean getLtPromoNom() {
		return ltPromoNom;
	}

	public void setLtPromoNom(Boolean ltPromoNom) {
		this.ltPromoNom = ltPromoNom;
	}

	public Double getLtIF() {
		return ltIF;
	}

	public void setLtIF(Double ltIF) {
		this.ltIF = ltIF;
	}
	
	public Boolean getAdditionalIncreaseNom() {
		return additionalIncreaseNom;
	}
	
	public void setAdditionalIncreaseNom(Boolean additionalIncreaseNom) {
		this.additionalIncreaseNom = additionalIncreaseNom;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	/**
	 * @return the isEligable
	 */
	public Boolean getIsEligable() {
		return isEligable;
	}

	/**
	 * @param isEligable the isEligable to set
	 */
	public void setIsEligable(Boolean isEligable) {
		this.isEligable = isEligable;
	}

	/**
	 * @return the promotionCriteriaId
	 */
	public Integer getPromotionCriteriaId() {
		return promotionCriteriaId;
	}

	/**
	 * @param promotionCriteriaId the promotionCriteriaId to set
	 */
	public void setPromotionCriteriaId(Integer promotionCriteriaId) {
		this.promotionCriteriaId = promotionCriteriaId;
	}

	public Boolean getLtPromoNomAug() {
		return ltPromoNomAug;
	}

	public void setLtPromoNomAug(Boolean ltPromoNomAug) {
		this.ltPromoNomAug = ltPromoNomAug;
	}

	/**
	 * @return the devCheckIn
	 */
	public Boolean getDevCheckIn() {
		return devCheckIn;
	}

	/**
	 * @param devCheckIn the devCheckIn to set
	 */
	public void setDevCheckIn(Boolean devCheckIn) {
		this.devCheckIn = devCheckIn;
	}

	/**
	 * @return the devCheckInRremarks
	 */
	public String getDevCheckInRremarks() {
		return devCheckInRremarks;
	}

	/**
	 * @param devCheckInRremarks the devCheckInRremarks to set
	 */
	public void setDevCheckInRremarks(String devCheckInRremarks) {
		this.devCheckInRremarks = devCheckInRremarks;
	}

	/**
	 * @return the cultureRatingMstr
	 */
	public PaCultureRatingMstr getCultureRatingMstr() {
		return cultureRatingMstr;
	}

	/**
	 * @param cultureRatingMstr the cultureRatingMstr to set
	 */
	public void setCultureRatingMstr(PaCultureRatingMstr cultureRatingMstr) {
		this.cultureRatingMstr = cultureRatingMstr;
	}

	@Override
	public String toString() {
		return "PA_OverallRatingLT [paOverallRatingLTId=" + paOverallRatingLTId + ", paSheet=" + paSheet + ", paRating="
				+ paRating + ", appraisalPeriod=" + appraisalPeriod + ", emp=" + emp + ", ltEmp=" + ltEmp
				+ ", ltRemark=" + ltRemark + ", ltPromoNom=" + ltPromoNom + ", ltIF=" + ltIF + "]";
	}
	
}
