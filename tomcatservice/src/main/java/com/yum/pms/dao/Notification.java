/******************************************************************** 
* Hibernate DAO Object Mapper for notification table 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreType;


@Entity
@Table(name = "notification")
@JsonIgnoreType
public class Notification implements java.io.Serializable {

	private static final long serialVersionUID = 8291650561351969405L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="notification_id")
	private short notificationId;
	
	@Column(name="notification_name")
	private String notificationName;
	
	@Column(name="settings_value")
	private String settingsValue;
	
	@Column(name="active")
	private Boolean active = true;
	
	@Column(name = "CreatedBy")
	private String CreatedBy;
	
	@Column(name = "UpdatedBy")
	private String UpdatedBy;

	public Notification() {
	}
	
	public Notification(short notificationId) {
		this.notificationId = notificationId;
	}

	public short getNotificationId() {
		return this.notificationId;
	}

	public void setNotificationId(short notificationId) {
		this.notificationId = notificationId;
	}

	public String getNotificationName() {
		return notificationName;
	}

	public void setNotificationName(String notificationName) {
		this.notificationName = notificationName;
	}

	public String getSettingsValue() {
		return settingsValue;
	}

	public void setSettingsValue(String settingsValue) {
		this.settingsValue = settingsValue;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}

	public String getUpdatedBy() {
		return UpdatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		UpdatedBy = updatedBy;
	}

	@Override
	public String toString() {
		return "Notification [notificationId=" + notificationId + ", notificationName=" + notificationName
				+ ", settingsValue=" + settingsValue + ", active=" + active + "]";
	}

}
