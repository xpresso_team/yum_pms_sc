package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author dipak.swain
 * This is a model class for 'pa_overall_rating_sup2' table
 */
@Entity
@Table(name = "pa_overall_rating_sup2")
@JsonIgnoreType
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@paOverallRatingSup2Id")
public class PA_OverallRatingSup2 implements Serializable {

	private static final long serialVersionUID = -4185957252843552966L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pa_overall_rating_sup2_id")
	private Integer paOverallRatingSup2Id;
	
	@ManyToOne
    @JoinColumn(name = "pa_sheet_id")
	private PA_SheetMaster paSheet;
	
	@ManyToOne
    @JoinColumn(name = "pa_rating_id")
	private PA_RatingMaster paRating;
	
	@ManyToOne
    @JoinColumn(name = "sup2_emp_id")
	private EmployeeNew sup2Emp;
	
	@Column(name = "sup2_remark")
	private String sup2Remark;
	
	@Column(name = "sup2_promo_nom")
	private Boolean sup2PromoNom;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;

	public Integer getPaOverallRatingSup2Id() {
		return paOverallRatingSup2Id;
	}
	
	public void setPaOverallRatingSup2Id(Integer paOverallRatingSup2Id) {
		this.paOverallRatingSup2Id = paOverallRatingSup2Id;
	}

	public PA_SheetMaster getPaSheet() {
		return paSheet;
	}

	public void setPaSheet(PA_SheetMaster paSheet) {
		this.paSheet = paSheet;
	}

	public PA_RatingMaster getPaRating() {
		return paRating;
	}

	public void setPaRating(PA_RatingMaster paRating) {
		this.paRating = paRating;
	}

	public EmployeeNew getSup2Emp() {
		return sup2Emp;
	}

	public void setSup2Emp(EmployeeNew sup2Emp) {
		this.sup2Emp = sup2Emp;
	}

	public String getSup2Remark() {
		return sup2Remark;
	}

	public void setSup2Remark(String sup2Remark) {
		this.sup2Remark = sup2Remark;
	}

	public Boolean getSup2PromoNom() {
		return sup2PromoNom;
	}

	public void setSup2PromoNom(Boolean sup2PromoNom) {
		this.sup2PromoNom = sup2PromoNom;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
}
