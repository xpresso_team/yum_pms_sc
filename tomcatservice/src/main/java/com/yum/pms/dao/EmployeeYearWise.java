package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author dipak.swain
 * Table representation of model class
 */
@Entity
@Table(name = "employee_yearwise")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property="@empYearWiseId")
public class EmployeeYearWise implements Serializable {
	
	private static final long serialVersionUID = -7048866574958828515L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "employee_yearwise_id", unique = true, nullable = false)
	private Integer empYearWiseId;
	
	@ManyToOne(cascade = CascadeType.REMOVE) // This caccade remove is used when there are no goal sheet created for this employee
	@JoinColumn(name = "emp_id")
	private EmployeeNew employee;
	
	@ManyToOne
	@JoinColumn(name = "manager_id")
	private EmployeeNew manager;
	
	@ManyToOne
	@JoinColumn(name = "Functonal_LT")
	private EmployeeNew functionalLt;
	
	@Column(name = "IsActive")
	private Boolean active = true;
	
	@ManyToOne
	@JoinColumn(name = "grade_id")
	private EmployeeGradeMaster gradeMaster;
	
	@ManyToOne
	@JoinColumn(name = "designation_id")
	private EmployeeDesignationMaster desgMaster;
	
	@ManyToOne
	@JoinColumn(name = "function_id")
	private EmployeeFunctionMaster functionMaster;
	
	@ManyToOne
	@JoinColumn(name = "sub_function_id")
	private EmployeeSubFunctionMaster subFunctionMaster;
	
	@ManyToOne
	@JoinColumn(name = "category_id")
	private EmployeeCategoryMaster categoryMaster;
	
	@ManyToOne
	@JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdateDate")
	private Date updatedDate;
	
	@Column(name = "hasreportee")
	private Boolean hasReportee;
	
	@Column(name = "KFC_allocation")
	private Integer kfcAllocation;
	
	@Column(name = "PH_allocation")
	private Integer phAllocation;
	
	@Column(name = "TB_allocation")
	private Integer tbAllocation;
	
	public EmployeeYearWise() {
	}
	
	public EmployeeYearWise(boolean active) {
		this.active = active;
	}

	public EmployeeYearWise(Integer empYearWiseId) {
		this.empYearWiseId = empYearWiseId;
	}

	public Integer getEmpYearWiseId() {
		return empYearWiseId;
	}

	public void setEmpYearWiseId(Integer empYearWiseId) {
		this.empYearWiseId = empYearWiseId;
	}

	public EmployeeNew getEmployee() {
		return employee;
	}

	public void setEmployee(EmployeeNew employee) {
		this.employee = employee;
	}

	public EmployeeNew getManager() {
		return manager;
	}

	public void setManager(EmployeeNew manager) {
		this.manager = manager;
	}

	public EmployeeNew getFunctionalLt() {
		return functionalLt;
	}

	public void setFunctionalLt(EmployeeNew functionalLt) {
		this.functionalLt = functionalLt;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public EmployeeGradeMaster getGradeMaster() {
		return gradeMaster;
	}

	public void setGradeMaster(EmployeeGradeMaster gradeMaster) {
		this.gradeMaster = gradeMaster;
	}

	public EmployeeDesignationMaster getDesgMaster() {
		return desgMaster;
	}

	public void setDesgMaster(EmployeeDesignationMaster desgMaster) {
		this.desgMaster = desgMaster;
	}

	public EmployeeFunctionMaster getFunctionMaster() {
		return functionMaster;
	}

	public void setFunctionMaster(EmployeeFunctionMaster functionMaster) {
		this.functionMaster = functionMaster;
	}

	public EmployeeSubFunctionMaster getSubFunctionMaster() {
		return subFunctionMaster;
	}

	public void setSubFunctionMaster(EmployeeSubFunctionMaster subFunctionMaster) {
		this.subFunctionMaster = subFunctionMaster;
	}

	public EmployeeCategoryMaster getCategoryMaster() {
		return categoryMaster;
	}

	public void setCategoryMaster(EmployeeCategoryMaster categoryMaster) {
		this.categoryMaster = categoryMaster;
	}

	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	public Boolean getHasReportee() {
		return hasReportee;
	}

	public void setHasReportee(Boolean hasReportee) {
		this.hasReportee = hasReportee;
	}
	
	public Integer getKfcAllocation() {
		return kfcAllocation;
	}

	public void setKfcAllocation(Integer kfcAllocation) {
		this.kfcAllocation = kfcAllocation;
	}

	public Integer getPhAllocation() {
		return phAllocation;
	}

	public void setPhAllocation(Integer phAllocation) {
		this.phAllocation = phAllocation;
	}

	public Integer getTbAllocation() {
		return tbAllocation;
	}

	public void setTbAllocation(Integer tbAllocation) {
		this.tbAllocation = tbAllocation;
	}
}
