/******************************************************************** 
* Hibernate DAO Object Mapper for goal_org table 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.dao;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@Table(name = "goal_org")
@JsonIgnoreType
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@orgGoalId")
public class GoalOrg implements java.io.Serializable {

	private static final long serialVersionUID = 845165481519838301L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "org_goal_id", unique = true, nullable = false)
	private int orgGoalId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@Column(name = "org_goal_desc", length = 4000)
	private String orgGoalDesc;
	
	@Column(name = "active")
	private Boolean active = true;
	
	@Column(name = "YLB_flag")
	private Boolean ylbFlag;
	
	@Column(name = "weightage", length = 10)
	private String weightage;
	
	@Column(name = "target", length = 50)
	private String target;
	
	@Column(name = "CreatedBy", length = 100)
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate", length = 23)
	private Date createdDate;
	
	@Column(name = "UpdatedBy", length = 100)
	private String updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate", length = 23)
	private Date updatedDate;
	
	@OneToOne(fetch = FetchType.LAZY, mappedBy = "goalOrg")
	private GoalCeo goalCeo;

	public GoalOrg() {
	}
	
	public GoalOrg(int orgGoalId) {
		this.orgGoalId = orgGoalId;
	}

	public int getOrgGoalId() {
		return this.orgGoalId;
	}

	public void setOrgGoalId(int orgGoalId) {
		this.orgGoalId = orgGoalId;
	}

	
	public AppraisalPeriod getAppraisalPeriod() {
		return this.appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	
	public String getOrgGoalDesc() {
		return this.orgGoalDesc;
	}

	public void setOrgGoalDesc(String orgGoalDesc) {
		this.orgGoalDesc = orgGoalDesc;
	}

	
	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	
	public Boolean getYlbFlag() {
		return this.ylbFlag;
	}

	public void setYlbFlag(Boolean ylbFlag) {
		this.ylbFlag = ylbFlag;
	}

	
	public String getWeightage() {
		return this.weightage;
	}

	public void setWeightage(String weightage) {
		this.weightage = weightage;
	}

	
	public String getTarget() {
		return this.target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	
	public GoalCeo getGoalCeo() {
		return this.goalCeo;
	}

	public void setGoalCeo(GoalCeo goalCeo) {
		this.goalCeo = goalCeo;
	}

}
