package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.yum.pms.domain.EmployeeCurrentDetails;
import com.yum.pms.utils.DateTimeUtils;

/**
 * @author dipak.swain
 * Table representation of model class
 */
@Entity
@Table(name = "employee_yearwise_not_effective")
public class EmployeeYearWiseNotEffective implements Serializable {
	
	private static final long serialVersionUID = -8337488838248358569L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private Integer empYearWiseId;
	
	@Column(name = "emp_id")
	private Integer empId;
	
	@Column(name = "manager_id")
	private Integer managerId;
	
	@Column(name = "Functional_LT")
	private Integer functionalLtsId;
	
	@Column(name = "IsActive")
	private Boolean active = true;
	
	@Column(name = "grade_id")
	private Integer gradeId;
	
	@Column(name = "designation_id")
	private Integer designationId;
	
	@Column(name = "function_id")
	private Integer functionId;
	
	@Column(name = "sub_function_id")
	private Integer subFunctionId;
	
	@Column(name = "category_id")
	private Integer categoryId;
	
	@Column(name = "appraisal_period_id")
	private Integer apprPeriodId;
	
	@Column(name = "KFC_allocation")
	private Integer kfcAllocation;
	
	@Column(name = "PH_allocation")
	private Integer phAllocation;
	
	@Column(name = "TB_allocation")
	private Integer tbAllocation;
	
	@Column(name = "effective_date")
	private Date effectiveDate;
	
	@Column(name = "createdby")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createddate")
	private Date createdDate;
	
	@Column(name = "updatedby")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updateddate")
	private Date updatedDate;
	
	@Column(name = "is_effective")
	private Boolean effective = false;
	
	@ManyToOne
	@JoinColumn(name = "effective_date_id")
	private CompRevisedSalaryEffectiveDuration effectiveDateId;
	
	public EmployeeYearWiseNotEffective() {
	}

	public EmployeeYearWiseNotEffective(EmployeeCurrentDetails employeeDetails) {
		this.empId = employeeDetails.getEmpId();
		this.managerId = employeeDetails.getManagerId();
		this.functionalLtsId = employeeDetails.getFunctionalLtsId();
		this.gradeId = employeeDetails.getGradeId();
		this.designationId = employeeDetails.getDesignationId();
		this.functionId = employeeDetails.getFunctionId();
		this.subFunctionId = employeeDetails.getSubFunctionId();
		this.categoryId = employeeDetails.getCategoryId();
		this.apprPeriodId = employeeDetails.getApprPeriodId();
		this.kfcAllocation = employeeDetails.getKfcAllocation();
		this.phAllocation = employeeDetails.getPhAllocation();
		this.tbAllocation = employeeDetails.getTbAllocation();
		this.effectiveDate = employeeDetails.getEffectiveDate();
		this.createdDate = employeeDetails.getCreatedDate() == null ? DateTimeUtils.today() : employeeDetails.getCreatedDate();
		this.createdBy = employeeDetails.getCreatedBy();
		this.effective = false;
		this.effectiveDateId = employeeDetails.getEffectiveDuration();
	}

	public Integer getEmpYearWiseId() {
		return empYearWiseId;
	}

	public void setEmpYearWiseId(Integer empYearWiseId) {
		this.empYearWiseId = empYearWiseId;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public Integer getManagerId() {
		return managerId;
	}

	public void setManagerId(Integer managerId) {
		this.managerId = managerId;
	}

	public Integer getFunctionalLtsId() {
		return functionalLtsId;
	}

	public void setFunctionalLtsId(Integer functionalLtsId) {
		this.functionalLtsId = functionalLtsId;
	}

	public Integer getApprPeriodId() {
		return apprPeriodId;
	}

	public void setApprPeriodId(Integer apprPeriodId) {
		this.apprPeriodId = apprPeriodId;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Integer getGradeId() {
		return gradeId;
	}

	public void setGradeId(Integer gradeId) {
		this.gradeId = gradeId;
	}

	public Integer getDesignationId() {
		return designationId;
	}

	public void setDesignationId(Integer designationId) {
		this.designationId = designationId;
	}

	public Integer getFunctionId() {
		return functionId;
	}

	public void setFunctionId(Integer functionId) {
		this.functionId = functionId;
	}

	public Integer getSubFunctionId() {
		return subFunctionId;
	}

	public void setSubFunctionId(Integer subFunctionId) {
		this.subFunctionId = subFunctionId;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public Integer getKfcAllocation() {
		return kfcAllocation;
	}

	public void setKfcAllocation(Integer kfcAllocation) {
		this.kfcAllocation = kfcAllocation;
	}

	public Integer getPhAllocation() {
		return phAllocation;
	}

	public void setPhAllocation(Integer phAllocation) {
		this.phAllocation = phAllocation;
	}

	public Integer getTbAllocation() {
		return tbAllocation;
	}

	public void setTbAllocation(Integer tbAllocation) {
		this.tbAllocation = tbAllocation;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Boolean getEffective() {
		return effective;
	}

	public void setEffective(Boolean effective) {
		this.effective = effective;
	}

	/**
	 * @return the effectiveDateId
	 */
	public CompRevisedSalaryEffectiveDuration getEffectiveDateId() {
		return effectiveDateId;
	}

	/**
	 * @param effectiveDateId the effectiveDateId to set
	 */
	public void setEffectiveDateId(CompRevisedSalaryEffectiveDuration effectiveDateId) {
		this.effectiveDateId = effectiveDateId;
	}
}
