/******************************************************************** 
* Hibernate DAO Object Mapper for goal_section table 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.dao;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@Table(name = "goal_section")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@goalSectionId")
public class GoalSection implements java.io.Serializable {

	private static final long serialVersionUID = 4307359163588550305L;

	@Id
	//@GeneratedValue(strategy=GenerationType.AUTO)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "goal_section_id", unique = true, nullable = false)
	private int goalSectionId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "goal_id", nullable = false)
	private Goal goal;
	
	@Column(name = "goal_section_summary", nullable = false)
	private String goalSectionSummary;
	
	@Column(name = "goal_section_sup_remarks")
	private String goalSectionSupRemarks;
	
	
	@Temporal(TemporalType.DATE)
	@Column(name = "goal_section_timeline")
	private Date goalSectionTimeline;
	
	@Column(name = "CreatedBy", length = 100)
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate", length = 23)
	private Date createdDate;
	
	@Column(name = "UpdatedBy", length = 100)
	private String updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate", length = 23)
	private Date updatedDate;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "goalSection")
	private Set<Tagged> taggeds = new HashSet<>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "goalSection")	
	private Set<TaggedEmp> taggedTo = new HashSet<>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "goalSection")
	private Set<Cascaded> cascadeds = new HashSet<>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "goalSection")	
	private Set<CasecadeEmp> casecadedTo = new HashSet<>(0);

	public GoalSection() {
	}
	
	public GoalSection(int goalSectionId) {
		this.goalSectionId=goalSectionId;
	}
	
	public int getGoalSectionId() {
		return this.goalSectionId;
	}

	public void setGoalSectionId(int goalSectionId) {
		this.goalSectionId = goalSectionId;
	}

	
	public Goal getGoal() {
		return this.goal;
	}

	public void setGoal(Goal goal) {
		this.goal = goal;
	}
	
	public String getGoalSectionSummary() {
		return this.goalSectionSummary;
	}

	public void setGoalSectionSummary(String goalSectionSummary) {
		this.goalSectionSummary = goalSectionSummary;
	}

	
	public String getGoalSectionSupRemarks() {
		return this.goalSectionSupRemarks;
	}

	public void setGoalSectionSupRemarks(String goalSectionSupRemarks) {
		this.goalSectionSupRemarks = goalSectionSupRemarks;
	}

	
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	public Date getGoalSectionTimeline() {
		return goalSectionTimeline;
	}

	public Set<TaggedEmp> getTaggedTo() {
		return taggedTo;
	}

	public void setTaggedTo(Set<TaggedEmp> taggedTo) {
		this.taggedTo = taggedTo;
	}

	public void setGoalSectionTimeline(Date goalSectionTimeline) {
		this.goalSectionTimeline = goalSectionTimeline;
	}

	public Set<Tagged> getTaggeds() {
		return taggeds;
	}

	public void setTaggeds(Set<Tagged> taggeds) {
		this.taggeds = taggeds;
	}

	public Set<Cascaded> getCascadeds() {
		return cascadeds;
	}

	public void setCascadeds(Set<Cascaded> cascadeds) {
		this.cascadeds = cascadeds;
	}	
	
	public Set<CasecadeEmp> getCasecadedTo() {
		return casecadedTo;
	}

	public void setCasecadedTo(Set<CasecadeEmp> casecadedTo) {
		this.casecadedTo = casecadedTo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + goalSectionId;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
	    if(obj instanceof GoalSection)
	    {
	    	GoalSection temp = (GoalSection) obj;
	        if(this.goalSectionId == temp.goalSectionId )
	            return true;
	    }
	    return false;

	}
}
