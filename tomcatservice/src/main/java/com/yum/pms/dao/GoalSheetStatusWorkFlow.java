/******************************************************************** 
* Hibernate DAO Object Mapper for goal_sheet_status_workflow table 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.dao;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "goal_sheet_status_workflow")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@goalSheetStsWorkflowId")
public class GoalSheetStatusWorkFlow implements Serializable{

	private static final long serialVersionUID = 1700330012415188300L;

	@Id
	@Column(name="goal_sheet_sts_workflow_id")
	private Integer goalSheetStsWorkflowId;
	
	@Column(name="goal_sheet_status_id")
	private Integer goalSheetStatusId;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="next_possible_goal_sheet_status_id")
	private GoalStatus nextPossibleGoalSheetStatus;
	
	@Column(name="WebServiceURL")
	private String webServiceURL;

	public Integer getGoalSheetStsWorkflowId() {
		return goalSheetStsWorkflowId;
	}

	public void setGoalSheetStsWorkflowId(Integer goalSheetStsWorkflowId) {
		this.goalSheetStsWorkflowId = goalSheetStsWorkflowId;
	}

	public Integer getGoalSheetStatusId() {
		return goalSheetStatusId;
	}

	public void setGoalSheetStatusId(Integer goalSheetStatusId) {
		this.goalSheetStatusId = goalSheetStatusId;
	}

	public GoalStatus getNextPossibleGoalSheetStatus() {
		return nextPossibleGoalSheetStatus;
	}

	public void setNextPossibleGoalSheetStatus(GoalStatus nextPossibleGoalSheetStatus) {
		this.nextPossibleGoalSheetStatus = nextPossibleGoalSheetStatus;
	}

	public String getWebServiceURL() {
		return webServiceURL;
	}

	public void setWebServiceURL(String webServiceURL) {
		this.webServiceURL = webServiceURL;
	}
	
	

}
