/******************************************************************** 
* Hibernate DAO Object Mapper for goal table 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.dao;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@Table(name = "goal")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@goalId")
public class Goal implements java.io.Serializable {

	private static final long serialVersionUID = 6593252828710139398L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "goal_id", unique = true, nullable = false)
	private int goalId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "goal_sheet_id")
	private GoalSheet goalSheet;
	
	@Column(name = "ceo_goal_id")
	private Integer ceoGoalId;
	
	@Column(name = "org_goal_id")
	private Integer orgGoalId;
	
	@Column(name = "CreatedBy", length = 100)
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate", length = 23)
	private Date createdDate;
	
	@Column(name = "UpdatedBy", length = 100)
	private String updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate", length = 23)
	private Date updatedDate;
	
	//@Transient
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "goal")
	private Set<GoalSection> goalSections = new HashSet<>(0);
	
	public Goal() {
	}
	
	public Goal(int goalId) {
		this.goalId = goalId;
	}

	public int getGoalId() {
		return goalId;
	}

	public void setGoalId(int goalId) {
		this.goalId = goalId;
	}

	public GoalSheet getGoalSheet() {
		return goalSheet;
	}

	public void setGoalSheet(GoalSheet goalSheet) {
		this.goalSheet = goalSheet;
	}

	public Integer getOrgGoalId() {
		return orgGoalId;
	}

	public void setOrgGoalId(Integer orgGoalId) {
		this.orgGoalId = orgGoalId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Set<GoalSection> getGoalSections() {
		return goalSections;
	}

	public void setGoalSections(Set<GoalSection> goalSections) {
		this.goalSections = goalSections;
	}

	public Integer getCeoGoalId() {
		return ceoGoalId;
	}

	public void setCeoGoalId(Integer ceoGoalId) {
		this.ceoGoalId = ceoGoalId;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + goalId;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
	    if(obj instanceof Goal)
	    {
	    	Goal temp = (Goal) obj;
	        if(this.goalId == temp.goalId )
	            return true;
	    }
	    return false;

	}

}
