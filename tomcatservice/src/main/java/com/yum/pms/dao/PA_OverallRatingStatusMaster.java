package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author dipak.swain
 * This is a model class for 'pa_overall_rating_status_mstr' table
 */
@Entity
@Table(name = "pa_overall_rating_status_mstr")
@JsonIgnoreType
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@paOverallRatingStatusId")
public class PA_OverallRatingStatusMaster implements Serializable {
	
	private static final long serialVersionUID = 1627751195019251071L;

	@Id
	@Column(name = "pa_overall_rating_status_id")
	private Integer paOverallRatingStatusId;
	
	@Column(name = "pa_overall_rating_status_desc")
	private String paOverallRatingStatusDesc;
	
	@Column(name = "IsActive")
	private Boolean active = true;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	public PA_OverallRatingStatusMaster() {
	}

	public PA_OverallRatingStatusMaster(Integer paOverallRatingStatusId) {
		this.paOverallRatingStatusId = paOverallRatingStatusId;
	}

	public Integer getPaOverallRatingStatusId() {
		return paOverallRatingStatusId;
	}

	public void setPaOverallRatingStatusId(Integer paOverallRatingStatusId) {
		this.paOverallRatingStatusId = paOverallRatingStatusId;
	}

	public String getPaOverallRatingStatusDesc() {
		return paOverallRatingStatusDesc;
	}

	public void setPaOverallRatingStatusDesc(String paOverallRatingStatusDesc) {
		this.paOverallRatingStatusDesc = paOverallRatingStatusDesc;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public String toString() {
		return "PA_OverallRatingStatusMaster [paOverallRatingStatusId=" + paOverallRatingStatusId
				+ ", paOverallRatingStatusDesc=" + paOverallRatingStatusDesc + "]";
	}
}
