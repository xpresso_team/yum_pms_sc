/******************************************************************** 
* Hibernate DAO Object Mapper - identity class for EmpToPmsRole 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.dao;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Embeddable
//@JsonIgnoreType
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@empId, @pmsRoleId")
public class EmpToPmsRoleId implements java.io.Serializable {

	private static final long serialVersionUID = -8362952122578165244L;
	
	@Column(name = "emp_id", nullable = false)
	private int empId;
	
	@Column(name = "pms_role_id", nullable = false)
	private short pmsRoleId;

	public EmpToPmsRoleId() {
	}
	
	public EmpToPmsRoleId(int empId, short pmsRoleId) {
		this.empId = empId;
		this.pmsRoleId = pmsRoleId;
	}
	
	public int getEmpId() {
		return this.empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	
	public short getPmsRoleId() {
		return this.pmsRoleId;
	}

	public void setPmsRoleId(short pmsRoleId) {
		this.pmsRoleId = pmsRoleId;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof EmpToPmsRoleId))
			return false;
		EmpToPmsRoleId castOther = (EmpToPmsRoleId) other;

		return (this.getEmpId() == castOther.getEmpId()) && (this.getPmsRoleId() == castOther.getPmsRoleId());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getEmpId();
		result = 37 * result + this.getPmsRoleId();
		return result;
	}

}
