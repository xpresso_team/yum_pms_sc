package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author dipak.swain
 * This is a model class for 'pa_config_promotion_percent_indicator' table
 */
@Entity
@Table(name = "pa_config_promotion_percent_indicator")
@JsonIgnoreType
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@paConfigPromoPercentId")
public class PA_ConfigPromotionPercentIndicator implements Serializable {
	
	private static final long serialVersionUID = 4799797299510356367L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer paConfigPromoPercentId;
	
	@ManyToOne
	@JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@Column(name = "promotion_percent_indicator")
	private Double promotionPercentIndicator;
	
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;

	/**
	 * @return the paConfigPromoPercentId
	 */
	public Integer getPaConfigPromoPercentId() {
		return paConfigPromoPercentId;
	}

	/**
	 * @param paConfigPromoPercentId the paConfigPromoPercentId to set
	 */
	public void setPaConfigPromoPercentId(Integer paConfigPromoPercentId) {
		this.paConfigPromoPercentId = paConfigPromoPercentId;
	}

	/**
	 * @return the appraisalPeriod
	 */
	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	/**
	 * @param appraisalPeriod the appraisalPeriod to set
	 */
	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	/**
	 * @return the promotionPercentIndicator
	 */
	public Double getPromotionPercentIndicator() {
		return promotionPercentIndicator;
	}

	/**
	 * @param promotionPercentIndicator the promotionPercentIndicator to set
	 */
	public void setPromotionPercentIndicator(Double promotionPercentIndicator) {
		this.promotionPercentIndicator = promotionPercentIndicator;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the updatedBy
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the updatedDate
	 */
	public Date getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * @param updatedDate the updatedDate to set
	 */
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
}
