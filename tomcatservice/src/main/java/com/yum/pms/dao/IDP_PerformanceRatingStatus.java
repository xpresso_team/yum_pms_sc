/**
 * 
 */
package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author jayanta.biswas
 *
 */

@Entity
@Table(name = "idp_performance_rating_LTS_workflow_status_mstr")
@JsonIgnoreType
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@idpPerformanceRatingLTSWorkflowStatusId")
public class IDP_PerformanceRatingStatus implements Serializable {
	
	private static final long serialVersionUID = -419371292778260048L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idp_performance_rating_LTS_workflow_status_id", unique = true, nullable = false)
	private Integer idpPerformanceRatingLTSWorkflowStatusId;
	
	@Column(name="idp_performance_rating_LTS_workflow_status_desc")
	private String idpPerformanceRatingLTSWorkflowStatusDesc;
	
	@Column(name="is_active")
	private String isActive;
	
	@Column(name="CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate")
	private Date createdDate;
	
	@Column(name="UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UpdatedDate")
	private Date updatedDate;
	
	public IDP_PerformanceRatingStatus() {
	}
	
	public IDP_PerformanceRatingStatus(Integer idpPerformanceRatingLTSWorkflowStatusId) {
		this.idpPerformanceRatingLTSWorkflowStatusId = idpPerformanceRatingLTSWorkflowStatusId;
	}

	/**
	 * @return the idpPerformanceRatingLTSWorkflowStatusId
	 */
	public Integer getIdpPerformanceRatingLTSWorkflowStatusId() {
		return idpPerformanceRatingLTSWorkflowStatusId;
	}

	/**
	 * @param idpPerformanceRatingLTSWorkflowStatusId the idpPerformanceRatingLTSWorkflowStatusId to set
	 */
	public void setIdpPerformanceRatingLTSWorkflowStatusId(Integer idpPerformanceRatingLTSWorkflowStatusId) {
		this.idpPerformanceRatingLTSWorkflowStatusId = idpPerformanceRatingLTSWorkflowStatusId;
	}

	/**
	 * @return the idpPerformanceRatingLTSWorkflowStatusDesc
	 */
	public String getIdpPerformanceRatingLTSWorkflowStatusDesc() {
		return idpPerformanceRatingLTSWorkflowStatusDesc;
	}

	/**
	 * @param idpPerformanceRatingLTSWorkflowStatusDesc the idpPerformanceRatingLTSWorkflowStatusDesc to set
	 */
	public void setIdpPerformanceRatingLTSWorkflowStatusDesc(String idpPerformanceRatingLTSWorkflowStatusDesc) {
		this.idpPerformanceRatingLTSWorkflowStatusDesc = idpPerformanceRatingLTSWorkflowStatusDesc;
	}

	/**
	 * @return the isActive
	 */
	public String getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the updatedBy
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the updatedDate
	 */
	public Date getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * @param updatedDate the updatedDate to set
	 */
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
