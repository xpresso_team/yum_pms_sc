package com.yum.pms.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author dipak.swain
 * This is a model class for 'pa_sheet_status_workflow' table
 */
@Entity
@Table(name = "pa_sheet_status_workflow")
@JsonIgnoreType
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@paSheetStatusWorkflowId")
public class PA_SheetStatusWorkFlow implements Serializable {
 
	private static final long serialVersionUID = 1596029890380860261L;

	@Id
	@Column(name = "pa_sheet_status_workflow_id")
	private Integer paSheetStatusWorkflowId;
	
	@Column(name = "pa_sheet_status_id")
	private Integer paSheetStatusId;
	
	@Column(name = "next_possible_pa_sheet_status_id")
	private Integer nextPossiblePaSheetStatusId;
	
	@Column(name = "WebServiceURL")
	private String webServiceURL;

	public Integer getPaSheetStatusWorkflowId() {
		return paSheetStatusWorkflowId;
	}

	public void setPaSheetStatusWorkflowId(Integer paSheetStatusWorkflowId) {
		this.paSheetStatusWorkflowId = paSheetStatusWorkflowId;
	}

	public Integer getPaSheetStatusId() {
		return paSheetStatusId;
	}

	public void setPaSheetStatusId(Integer paSheetStatusId) {
		this.paSheetStatusId = paSheetStatusId;
	}

	public Integer getNextPossiblePaSheetStatusId() {
		return nextPossiblePaSheetStatusId;
	}

	public void setNextPossiblePaSheetStatusId(Integer nextPossiblePaSheetStatusId) {
		this.nextPossiblePaSheetStatusId = nextPossiblePaSheetStatusId;
	}

	public String getWebServiceURL() {
		return webServiceURL;
	}

	public void setWebServiceURL(String webServiceURL) {
		this.webServiceURL = webServiceURL;
	}

	@Override
	public String toString() {
		return "PA_SheetStatusWorkFlow [paSheetStatusWorkflowId=" + paSheetStatusWorkflowId + ", paSheetStatusId="
				+ paSheetStatusId + ", nextPossiblePaSheetStatusId=" + nextPossiblePaSheetStatusId + "]";
	}

}
