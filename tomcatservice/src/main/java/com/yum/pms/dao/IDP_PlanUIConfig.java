package com.yum.pms.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreType;

/**
 * @author jayanta.biswas
 * @comment This is a pojo class for data sending
 * @version 1.0
 * @since   26-12-2017
 */
@Entity
@Table(name = "idp_plan_ui_config")
@JsonIgnoreType
public class IDP_PlanUIConfig implements Serializable {

	private static final long serialVersionUID = 3101961135436746587L;

	@Id
	@Column(name="idp_plan_UI_Config_id", unique = true, nullable = false)
	private Integer idpPlanUIConfig_id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="idp_plan_id")
	private IDP_PlanMaster idpPlanMaster;		
	
	@Column(name="UI_control_type")
	private String UI_control_type;
	
	@Column(name="UI_subcontrol_label")
	private String uiControlLabel;
	
	@Column(name="UI_subcontrol_value")
	private String uiControlValue;
	
	@Column(name="IsActive")
	private Boolean isActive;

	public Integer getIdpPlanUIConfig_id() {
		return idpPlanUIConfig_id;
	}

	public void setIdpPlanUIConfig_id(Integer idpPlanUIConfigId) {
		this.idpPlanUIConfig_id = idpPlanUIConfigId;
	}

	public IDP_PlanMaster getIdpPlanMaster() {
		return idpPlanMaster;
	}

	public void setIdpPlanMaster(IDP_PlanMaster idpPlanMaster) {
		this.idpPlanMaster = idpPlanMaster;
	}

	public String getUI_control_type() {
		return UI_control_type;
	}

	public void setUI_control_type(String uiControlType) {
		UI_control_type = uiControlType;
	}

	public String getUiControlLabel() {
		return uiControlLabel;
	}

	public void setUiControlLabel(String uiControlLabel) {
		this.uiControlLabel = uiControlLabel;
	}

	public String getUiControlValue() {
		return uiControlValue;
	}

	public void setUiControlValue(String uiControlValue) {
		this.uiControlValue = uiControlValue;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	

}
