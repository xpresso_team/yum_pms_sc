package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "idp_section_dtl")
@JsonIgnoreType
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@idpSectionDtlId")
public class IDP_SectionDtl implements Serializable {

	private static final long serialVersionUID = -6463601022499434123L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idp_section_dtl_id", unique = true, nullable = false)
	private Integer idpSectionDtlId ;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idp_section_id")
	private IDP_SectionMaster idpSectionMaster;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idp_sheet_id")
	private IDP_SheetMaster idpSheetMaster;
	
	@Column(name="idp_section_dtl_desc")
	private String idpSectionDtlDesc;
	
	@Column(name="idp_section_type_flag")
	private String idpSectionTypeFlag;
	
	@Column(name="is_active")
	private Boolean isActive=true;
	
	@Column(name="CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate")
	private Date createdDate;
	
	@Column(name="UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UpdatedDate")
	private Date updatedDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idp_sub_section_id")
	private IDP_SubSectionMaster idpSubSectionMaster;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idp_sub_sub_section_id")
	private IDP_SubSubSectionMaster idpSubSubSectionMaster;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idp_sub_sub_sub_section_id")
	private IDP_SubSubSubSectionMaster idpSubSubSubSectionMaster;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idp_suggested_goal_section_id")
	private IDP_SuggestedGoalMaster idpSuggestedGoalSectionMaster;

	public IDP_SuggestedGoalMaster getIdpSuggestedGoalSectionMaster() {
		return idpSuggestedGoalSectionMaster;
	}

	public void setIdpSuggestedGoalSectionId(
			IDP_SuggestedGoalMaster idpSuggestedGoalSectionMaster) {
		this.idpSuggestedGoalSectionMaster = idpSuggestedGoalSectionMaster;
	}

	public Integer getIdpSectionDtlId() {
		return idpSectionDtlId;
	}

	public void setIdpSectionDtlId(Integer idpSectionDtlId) {
		this.idpSectionDtlId = idpSectionDtlId;
	}

	public IDP_SectionMaster getIdpSectionMaster() {
		return idpSectionMaster;
	}

	public void setIdpSectionMaster(IDP_SectionMaster idpSectionMaster) {
		this.idpSectionMaster = idpSectionMaster;
	}

	public IDP_SheetMaster getIdpSheetMaster() {
		return idpSheetMaster;
	}

	public void setIdpSheetMaster(IDP_SheetMaster idpSheetMaster) {
		this.idpSheetMaster = idpSheetMaster;
	}

	public String getIdpSectionDtlDesc() {
		return idpSectionDtlDesc;
	}

	public void setIdpSectionDtlDesc(String idpSectionDtlDesc) {
		this.idpSectionDtlDesc = idpSectionDtlDesc;
	}

	public String getIdpSectionTypeFlag() {
		return idpSectionTypeFlag;
	}

	public void setIdpSectionTypeFlag(String idpSectionTypeFlag) {
		this.idpSectionTypeFlag = idpSectionTypeFlag;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public IDP_SubSectionMaster getIdpSubSectionMaster() {
		return idpSubSectionMaster;
	}

	public void setIdpSubSectionMaster(IDP_SubSectionMaster idpSubSectionMaster) {
		this.idpSubSectionMaster = idpSubSectionMaster;
	}

	public IDP_SubSubSectionMaster getIdpSubSubSectionMaster() {
		return idpSubSubSectionMaster;
	}

	public void setIdpSubSubSectionMaster(IDP_SubSubSectionMaster idpSubSubSectionMaster) {
		this.idpSubSubSectionMaster = idpSubSubSectionMaster;
	}

	public IDP_SubSubSubSectionMaster getIdpSubSubSubSectionMaster() {
		return idpSubSubSubSectionMaster;
	}

	public void setIdpSubSubSubSectionMaster(IDP_SubSubSubSectionMaster idpSubSubSubSectionMaster) {
		this.idpSubSubSubSectionMaster = idpSubSubSubSectionMaster;
	}

}
