package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author dipak.swain
 * This is a model class for 'comp_letter_status' table
 */
@Entity
@Table(name = "comp_letter_status")
@JsonIgnoreProperties(value = { "createdBy", "createdDate", "updatedBy", "updatedDate" })
public class CompLetterStatus implements Serializable {
	
	private static final long serialVersionUID = -2379981372359228012L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "letter_status_id", unique = true, nullable = false)
	private Integer letterStatusId;

	@Column(name = "emp_id")
	private Integer empId;
	
	@Column(name = "appr_id")
	private Integer apprId;
	
	@Column(name = "letter_type")
	private String letterType;
	
	@Column(name = "letter_email_sending_status")
	private Boolean letterEmailSendingStatus;
	
	@Column(name = "letter_download_permission")
	private Boolean letterDownloadPermission;
	
	@Column(name = "comp_letter_acceptance")
	private Boolean letterAcceptance;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "comp_letter_acceptance_date")
	private Date letterAcceptanceDate;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;

	public Integer getLetterStatusId() {
		return letterStatusId;
	}

	public void setLetterStatusId(Integer letterStatusId) {
		this.letterStatusId = letterStatusId;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public Integer getApprId() {
		return apprId;
	}

	public void setApprId(Integer apprId) {
		this.apprId = apprId;
	}
	
	public String getLetterType() {
		return letterType;
	}
	
	public void setLetterType(String letterType) {
		this.letterType = letterType;
	}

	public Boolean getLetterEmailSendingStatus() {
		return letterEmailSendingStatus;
	}

	public void setLetterEmailSendingStatus(Boolean letterEmailSendingStatus) {
		this.letterEmailSendingStatus = letterEmailSendingStatus;
	}

	public Boolean getLetterDownloadPermission() {
		return letterDownloadPermission;
	}

	public void setLetterDownloadPermission(Boolean letterDownloadPermission) {
		this.letterDownloadPermission = letterDownloadPermission;
	}

	public Boolean getLetterAcceptance() {
		return letterAcceptance;
	}

	public void setLetterAcceptance(Boolean letterAcceptance) {
		this.letterAcceptance = letterAcceptance;
	}

	public Date getLetterAcceptanceDate() {
		return letterAcceptanceDate;
	}
	
	public void setLetterAcceptanceDate(Date letterAcceptanceDate) {
		this.letterAcceptanceDate = letterAcceptanceDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
}
