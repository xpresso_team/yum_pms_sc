package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "idp_plan_dtl")
@JsonIgnoreType
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@idp_plan_dtl_id")
public class IDP_PlanDetails implements Serializable {

	private static final long serialVersionUID = -5894402191634374003L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idp_plan_dtl_id", unique = true, nullable = false)
	private Integer idp_plan_dtl_id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idp_sheet_id")
	private IDP_SheetMaster idpSheetMaster;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="idp_plan_id")
	private IDP_PlanMaster idpPlanMaster;	
	
	@Column(name="idp_plan_dtl_desc")
	private String idp_plan_dtl_desc;
	
	@Column(name="CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate")
	private Date createdDate;
	
	@Column(name="UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UpdatedDate")
	private Date updatedDate;

	public Integer getIdp_plan_dtl_id() {
		return idp_plan_dtl_id;
	}

	public void setIdp_plan_dtl_id(Integer idpPlanDtlId) {
		this.idp_plan_dtl_id = idpPlanDtlId;
	}

	public IDP_SheetMaster getIdpSheetMaster() {
		return idpSheetMaster;
	}

	public void setIdpSheetMaster(IDP_SheetMaster idpSheetMaster) {
		this.idpSheetMaster = idpSheetMaster;
	}

	public IDP_PlanMaster getIdpPlanMaster() {
		return idpPlanMaster;
	}

	public void setIdpPlanMaster(IDP_PlanMaster idpPlanMaster) {
		this.idpPlanMaster = idpPlanMaster;
	}

	public String getIdp_plan_dtl_desc() {
		return idp_plan_dtl_desc;
	}

	public void setIdp_plan_dtl_desc(String idpPlanDtlDesc) {
		this.idp_plan_dtl_desc = idpPlanDtlDesc;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
	
}
