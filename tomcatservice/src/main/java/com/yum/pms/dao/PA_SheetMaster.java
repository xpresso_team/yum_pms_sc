package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.BooleanUtils;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author dipak.swain
 * This is a model class for 'pa_sheet_mstr' table
 */
@Entity
@Table(name = "pa_sheet_mstr")
@JsonIgnoreType
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@paSheetId")
public class PA_SheetMaster implements Serializable {
	
	private static final long serialVersionUID = -2379981372359228012L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pa_sheet_id", unique = true, nullable = false)
	private Integer paSheetId;

	@ManyToOne
	@JoinColumn(name = "emp_id")
	private EmployeeNew employee = new EmployeeNew();
	
	@ManyToOne
	@JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@ManyToOne
	@JoinColumn(name = "pa_sheet_status_id")
	private PA_SheetStatusMaster paSheetStatus;
	
	@ManyToOne
	@JoinColumn(name = "pa_overall_rating_status_id")
	private PA_OverallRatingStatusMaster paOverallRatingStatus;
	
	@Column(name = "is_active")
	private Boolean isActive = true;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	@Column(name = "letter_email_sending_status")
	private Boolean letterEmailSendingStatus;
	
	@Column(name = "letter_download_permission")
	private Boolean letterDownloadPermission;
	
	@Column(name = "comp_letter_acceptance")
	private Boolean letterAcceptance;
	
	public PA_SheetMaster() {
	}
	
	public PA_SheetMaster(Integer paSheetId) {
		this.paSheetId = paSheetId;
	}
	
	public PA_SheetMaster(Integer paSheetId, EmployeeNew employee, AppraisalPeriod appraisalPeriod) {
		this.paSheetId = paSheetId;
		this.employee = employee;
		this.appraisalPeriod = appraisalPeriod;
	}

	public Integer getPaSheetId() {
		return paSheetId;
	}

	public void setPaSheetId(Integer paSheetId) {
		this.paSheetId = paSheetId;
	}

	public EmployeeNew getEmployee() {
		return employee;
	}

	public void setEmployee(EmployeeNew employee) {
		this.employee = employee;
	}

	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	public PA_SheetStatusMaster getPaSheetStatus() {
		return paSheetStatus;
	}

	public void setPaSheetStatus(PA_SheetStatusMaster paSheetStatus) {
		this.paSheetStatus = paSheetStatus;
	}

	public PA_OverallRatingStatusMaster getPaOverallRatingStatus() {
		return paOverallRatingStatus;
	}

	public void setPaOverallRatingStatus(PA_OverallRatingStatusMaster paOverallRatingStatus) {
		this.paOverallRatingStatus = paOverallRatingStatus;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Boolean getLetterEmailSendingStatus() {
		return letterEmailSendingStatus;
	}

	public void setLetterEmailSendingStatus(Boolean letterEmailSendingStatus) {
		this.letterEmailSendingStatus = letterEmailSendingStatus;
	}

	public Boolean getLetterDownloadPermission() {
		return letterDownloadPermission;
	}

	public void setLetterDownloadPermission(Boolean letterDownloadPermission) {
		this.letterDownloadPermission = letterDownloadPermission;
	}

	public Boolean getLetterAcceptance() {
		return letterAcceptance;
	}

	public void setLetterAcceptance(Boolean letterAcceptance) {
		this.letterAcceptance = letterAcceptance;
	}

	@Override
	public String toString() {
		return "PA_SheetMaster [paSheetId=" + paSheetId + ", employee=" + employee + ", appraisalPeriod="
				+ appraisalPeriod + ", paSheetStatus=" + paSheetStatus + ", paOverallRatingStatus="
				+ paOverallRatingStatus + ", letterDownloadPermission=" + BooleanUtils.toBooleanDefaultIfNull(letterDownloadPermission, false) 
				+ ", letterAcceptance="+ BooleanUtils.toBooleanDefaultIfNull(letterAcceptance, false) + "]";
	}
	
}
