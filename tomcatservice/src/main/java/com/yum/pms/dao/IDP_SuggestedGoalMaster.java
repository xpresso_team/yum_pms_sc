package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.yum.idp.domain.IdpSuggestedGoal;

@Entity
@Table(name = "idp_suggested_goal_mstr")
@JsonIgnoreType
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@idpSuggestedGoalSectionId")
public class IDP_SuggestedGoalMaster implements Serializable {
	
	private static final long serialVersionUID = -5553335093431048935L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idp_suggested_goal_section_id", unique = true, nullable = false)
	private Integer idpSuggestedGoalSectionId;
	
	@Column(name="idp_suggested_goal_section_desc")
	private String idpSuggestedGoalSectionDesc;
	
	@Column(name="is_active")
	private Boolean isActive;
	
	@Column(name="CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate", updatable = false)
	private Date createdDate;
	
	@Column(name="UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UpdatedDate", insertable = false)
	private Date updatedDate;
	
	@ManyToOne
	@JoinColumn(name="idp_sub_sub_section_id")
	private IDP_SubSubSectionMaster subSubSectionMaster;
	
	@ManyToOne
	@JoinColumn(name="idp_sub_section_id")
	private IDP_SubSectionMaster subSectionMaster;
	
	@ManyToOne
	@JoinColumn(name="idp_section_id")
	private IDP_SectionMaster sectionMaster;
	
	@Transient
	private boolean removable = true;
	
	@Column(name="grade_range_lower")
	private String gradeRangeLower = "0";
	
	@Column(name="grade_range_upper")
	private String gradeRangeUpper = "99";
	
	public IDP_SuggestedGoalMaster() {
	}
	
	public IDP_SuggestedGoalMaster(Integer idpSuggestedGoalSectionId) {
		this.idpSuggestedGoalSectionId = idpSuggestedGoalSectionId;
	}

	public IDP_SuggestedGoalMaster(IdpSuggestedGoal suggestedGoalSection) {
		this.idpSuggestedGoalSectionId = suggestedGoalSection.getIdpSuggestedGoalSectionId();
		this.idpSuggestedGoalSectionDesc = suggestedGoalSection.getIdpSuggestedGoalSectionDesc();
		this.isActive = suggestedGoalSection.getIsActive();
		this.createdBy = suggestedGoalSection.getCreatedBy();
		this.updatedBy = suggestedGoalSection.getUpdatedBy();
		this.removable = suggestedGoalSection.isRemovable();
		this.gradeRangeLower = suggestedGoalSection.getGradeRangeLower();
		this.gradeRangeUpper = suggestedGoalSection.getGradeRangeUpper();
		this.sectionMaster = new IDP_SectionMaster(suggestedGoalSection.getIdpSectionId());
		this.subSectionMaster = new IDP_SubSectionMaster(suggestedGoalSection.getIdpSubSectionId());
		this.subSubSectionMaster = new IDP_SubSubSectionMaster(suggestedGoalSection.getIdpSubSubSectionId());
	}

	public Integer getIdpSuggestedGoalSectionId() {
		return idpSuggestedGoalSectionId;
	}

	public void setIdpSuggestedGoalSectionId(Integer idpSuggestedGoalSectionId) {
		this.idpSuggestedGoalSectionId = idpSuggestedGoalSectionId;
	}

	public String getIdpSuggestedGoalSectionDesc() {
		return idpSuggestedGoalSectionDesc;
	}

	public void setIdpSuggestedGoalSectionDesc(String idpSuggestedGoalSectionDesc) {
		this.idpSuggestedGoalSectionDesc = idpSuggestedGoalSectionDesc;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public IDP_SubSubSectionMaster getSubSubSectionMaster() {
		return subSubSectionMaster;
	}

	public void setSubSubSectionMaster(IDP_SubSubSectionMaster subSubSectionMaster) {
		this.subSubSectionMaster = subSubSectionMaster;
	}

	public IDP_SubSectionMaster getSubSectionMaster() {
		return subSectionMaster;
	}

	public void setSubSectionMaster(IDP_SubSectionMaster subSectionMaster) {
		this.subSectionMaster = subSectionMaster;
	}

	public IDP_SectionMaster getSectionMaster() {
		return sectionMaster;
	}

	public void setSectionMaster(IDP_SectionMaster sectionMaster) {
		this.sectionMaster = sectionMaster;
	}

	public boolean isRemovable() {
		return removable;
	}

	public void setRemovable(boolean removable) {
		this.removable = removable;
	}

	public String getGradeRangeLower() {
		return gradeRangeLower;
	}

	public void setGradeRangeLower(String gradeRangeLower) {
		this.gradeRangeLower = gradeRangeLower;
	}

	public String getGradeRangeUpper() {
		return gradeRangeUpper;
	}

	public void setGradeRangeUpper(String gradeRangeUpper) {
		this.gradeRangeUpper = gradeRangeUpper;
	}
	
}
