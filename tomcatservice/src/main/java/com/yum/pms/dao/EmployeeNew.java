package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.yum.pms.utils.YumPmsConstants;

/**
 * @author dipak.swain
 * Table representation of model class
 */
@Entity
@Table(name = YumPmsConstants.EMPLOYEE_TABLE)
//@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@empId")
public class EmployeeNew implements Serializable {
	
	private static final long serialVersionUID = -665672689109009185L;

	@Id
	@Column(name = "emp_id")
	private Integer empId;
	
	@Column(name = "emp_name")
	private String empName;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
	
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	@Column(name = "email")
	private String email;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "DOJ_formatted")
	private Date doj;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "DOL_formatted")
	private Date dol;
	
	@Column(name = "signature_image_path")
	private String signatureImagePath;
	
	@Column(name = "BMU_Franchise")
	private String bmuFranchise;
	
	public EmployeeNew() {
	}

	public EmployeeNew(Integer empId) {
		this.empId = empId;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getDoj() {
		return doj;
	}

	public void setDoj(Date doj) {
		this.doj = doj;
	}

	public Date getDol() {
		return dol;
	}

	public void setDol(Date dol) {
		this.dol = dol;
	}

	public String getSignatureImagePath() {
		return signatureImagePath;
	}

	public void setSignatureImagePath(String signatureImagePath) {
		this.signatureImagePath = signatureImagePath;
	}

	public String getBmuFranchise() {
		return bmuFranchise;
	}

	public void setBmuFranchise(String bmuFranchise) {
		this.bmuFranchise = bmuFranchise;
	}
	
}
