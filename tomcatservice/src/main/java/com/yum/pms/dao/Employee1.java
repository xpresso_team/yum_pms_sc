/******************************************************************** 
* Hibernate DAO Object Mapper for employee table 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.dao;
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "employee")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@empId")
public class Employee1 implements java.io.Serializable {

	private static final long serialVersionUID = -4590769181144062362L;

	@Id
	@Column(name="emp_id")
	private int empId;
	
	@Column(name="emp_name")
	private String empName;
	
	@Column(name="email")
	private String email;
	
	@Column(name="manager_id")
	private Integer managerId;	
	
	@Column(name="hasreportee")
	private Boolean hasreportee;
	
	@Column(name="CreatedDate")
	private Date dateCreated;
	
	@Column(name="UpdatedDate")
	private Date dateModified;
	
	@Column(name="designation")
	private String designation;
	
	@Column(name="emp_function")
	private String empFunction;
	
	@Column(name="emp_sub_function")
	private String subFunction;
	
	@Column(name="CreatedBy")
	private String CreatedBy;
	
	@Column(name="UpdatedBy")
	private String UpdatedBy;
	
	@Column(name="employee_category")
	private String category;
	
	@Column(name="Functional_LT")
	private Integer functionalLTsID;
	
	@Temporal(TemporalType.DATE)
	@Column(name="DOJ_formatted")
	private Date doj;
	
	@Temporal(TemporalType.DATE)
	@Column(name="DOL_formatted")
	private Date dol;
	
	@Column(name="IsActive")
	private Boolean isActive = true;
	
	@Column(name="Grade")
	private String grade;
	
	@Transient
	private String managerName;
	
	@Transient
	private String functionalLTsName;
	
	@Transient
	private String operationMessage;
	
	@Transient
	private Boolean isAdmin = false;
	
	@Transient
	private Boolean isNew = false;
	
	@Transient
	private Boolean operationFlag = false;
	
	@Transient
	private Boolean isActiveLt = false;
	
	

	public Employee1() {
	}

	public Employee1(int empId) {
		this.empId = empId;
	}

	public int getEmpId() {
		return this.empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return this.empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getManagerId() {
		return managerId;
	}

	public void setManagerId(Integer managerId) {
		this.managerId = managerId;
	}

	public Boolean getHasreportee() {
		return this.hasreportee;
	}

	public void setHasreportee(Boolean hasreportee) {
		this.hasreportee = hasreportee;
	}

	public Date getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateModified() {
		return this.dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	public String getDesignation() {
		return this.designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getEmpFunction() {
		return empFunction;
	}

	public void setEmpFunction(String empFunction) {
		this.empFunction = empFunction;
	}

	public String getSubFunction() {
		return this.subFunction;
	}

	public void setSubFunction(String subFunction) {
		this.subFunction = subFunction;
	}

	public String getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}

	public String getUpdatedBy() {
		return UpdatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		UpdatedBy = updatedBy;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public Boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public Integer getFunctionalLTsID() {
		return functionalLTsID;
	}

	public void setFunctionalLTsID(Integer functionalLTsID) {
		this.functionalLTsID = functionalLTsID;
	}

	public String getFunctionalLTsName() {
		return functionalLTsName;
	}

	public void setFunctionalLTsName(String functionalLTsName) {
		this.functionalLTsName = functionalLTsName;
	}

	public Boolean getIsNew() {
		return isNew;
	}

	public void setIsNew(Boolean isNew) {
		this.isNew = isNew;
	}

	public String getOperationMessage() {
		return operationMessage;
	}

	public void setOperationMessage(String operationMessage) {
		this.operationMessage = operationMessage;
	}

	public Date getDoj() {
		return doj;
	}

	public void setDoj(Date doj) {
		this.doj = doj;
	}

	public Date getDol() {
		return dol;
	}

	public void setDol(Date dol) {
		this.dol = dol;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getOperationFlag() {
		return operationFlag;
	}

	public void setOperationFlag(Boolean operationFlag) {
		this.operationFlag = operationFlag;
	}

	public Boolean getIsActiveLt() {
		return isActiveLt;
	}

	public void setIsActiveLt(Boolean isActiveLt) {
		this.isActiveLt = isActiveLt;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	
	
}

