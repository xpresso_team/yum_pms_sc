package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author dipak.swain
 * Table representation of model class
 */
@Entity
@Table(name = "pa_config_promotion_criteria")
@JsonIgnoreType
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@paPromotionCriteriaId")
public class PA_ConfigPromotionCriteria implements Serializable {
	
	private static final long serialVersionUID = 8157083880058541205L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer paPromotionCriteriaId;
	
	@Column(name = "criteria_desc")
	private String criteriaDesc;
	
	@Column(name = "grade_from")
	private Integer gradeFrom;
	
	@Column(name = "grade_to")
	private Integer gradeTo;
	
	@Column(name = "tenure_months_range_lower")
	private Integer tenureMonthsRangeLower;
	
	@Column(name = "tenure_months_range_higher")
	private Integer tenureMonthsRangeHigher;
	
	@ManyToOne
	@JoinColumn(name = "pa_rating_id_CY")
	private PA_RatingMaster paRatingIdCurrent;
	
	@ManyToOne
	@JoinColumn(name = "pa_rating_id_PY")
	private PA_RatingMaster paRatingIdPrevious;
	
	@Column(name = "IF_min_CY")
	private Double ifMinCurrent;
	
	@Column(name = "IF_check_logic")
	private String ifCheckLogic;
	
	@Column(name = "LTS_IDP_rating_CY_min_level")
	private String ltsIdpRatingCurrentMinLevel;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	@Column(name = "IsActive")
	private Boolean active = true;
	
	@ManyToOne
    @JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	public PA_ConfigPromotionCriteria() {
	}

	public PA_ConfigPromotionCriteria(Integer apprId) {
		this.appraisalPeriod = new AppraisalPeriod(apprId);
	}

	public Integer getPaPromotionCriteriaId() {
		return paPromotionCriteriaId;
	}

	public void setPaPromotionCriteriaId(Integer paPromotionCriteriaId) {
		this.paPromotionCriteriaId = paPromotionCriteriaId;
	}

	public String getCriteriaDesc() {
		return criteriaDesc;
	}

	public void setCriteriaDesc(String criteriaDesc) {
		this.criteriaDesc = criteriaDesc;
	}

	public Integer getGradeFrom() {
		return gradeFrom;
	}

	public void setGradeFrom(Integer gradeFrom) {
		this.gradeFrom = gradeFrom;
	}

	public Integer getGradeTo() {
		return gradeTo;
	}

	public void setGradeTo(Integer gradeTo) {
		this.gradeTo = gradeTo;
	}

	public Integer getTenureMonthsRangeLower() {
		return tenureMonthsRangeLower;
	}

	public void setTenureMonthsRangeLower(Integer tenureMonthsRangeLower) {
		this.tenureMonthsRangeLower = tenureMonthsRangeLower;
	}

	public Integer getTenureMonthsRangeHigher() {
		return tenureMonthsRangeHigher;
	}

	public void setTenureMonthsRangeHigher(Integer tenureMonthsRangeHigher) {
		this.tenureMonthsRangeHigher = tenureMonthsRangeHigher;
	}

	public PA_RatingMaster getPaRatingIdCurrent() {
		return paRatingIdCurrent;
	}

	public void setPaRatingIdCurrent(PA_RatingMaster paRatingIdCurrent) {
		this.paRatingIdCurrent = paRatingIdCurrent;
	}

	public PA_RatingMaster getPaRatingIdPrevious() {
		return paRatingIdPrevious;
	}

	public void setPaRatingIdPrevious(PA_RatingMaster paRatingIdPrevious) {
		this.paRatingIdPrevious = paRatingIdPrevious;
	}

	public Double getIfMinCurrent() {
		return ifMinCurrent;
	}

	public void setIfMinCurrent(Double ifMinCurrent) {
		this.ifMinCurrent = ifMinCurrent;
	}

	public String getIfCheckLogic() {
		return ifCheckLogic;
	}

	public void setIfCheckLogic(String ifCheckLogic) {
		this.ifCheckLogic = ifCheckLogic;
	}

	public String getLtsIdpRatingCurrentMinLevel() {
		return ltsIdpRatingCurrentMinLevel;
	}

	public void setLtsIdpRatingCurrentMinLevel(String ltsIdpRatingCurrentMinLevel) {
		this.ltsIdpRatingCurrentMinLevel = ltsIdpRatingCurrentMinLevel;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}
	
}
