package com.yum.pms.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@hoverId")
public class HoverConfig implements java.io.Serializable {
	
	private static final long serialVersionUID = -136224157850126197L;

	@Id
	@Column(name="hover_id")
	private Integer hoverId;
	
	@Column(name="hover_name")
	private String hoverName;
	
	@Column(name="hover_value")
	private String hoverValue;
	
	public HoverConfig() {
	}
	
	public HoverConfig(Integer hoverId, String hoverName, String hoverValue) {
		this.hoverId = hoverId;
		this.hoverName = hoverName;
		this.hoverValue = hoverValue;
	}

	public Integer getHoverId() {
		return hoverId;
	}

	public void setHoverId(Integer hoverId) {
		this.hoverId = hoverId;
	}

	public String getHoverName() {
		return hoverName;
	}

	public void setHoverName(String hoverName) {
		this.hoverName = hoverName;
	}

	public String getHoverValue() {
		return hoverValue;
	}

	public void setHoverValue(String hoverValue) {
		this.hoverValue = hoverValue;
	}

}
