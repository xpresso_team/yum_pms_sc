/******************************************************************** 
* Hibernate DAO Object Mapper for GoalCeo table 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@Table(name = "goal_CEO")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@ceoGoalId")
public class GoalCeo implements java.io.Serializable {

	private static final long serialVersionUID = 7935964623735548409L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ceo_goal_id", unique = true, nullable = false)
	private int ceoGoalId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "org_goal_id")
	private GoalOrg goalOrg;
	
	@Column(name = "ceo_goal_desc", length = 4000)
	private String ceoGoalDesc;
	
	@Column(name = "active")
	private Boolean active = true;
	
	@Column(name = "CreatedBy", length = 100)
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate", length = 23)
	private Date createdDate;
	
	@Column(name = "UpdatedBy", length = 100)
	private String updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate", length = 23)
	private Date updatedDate;
	
	public GoalCeo() {
	}
	
	public GoalCeo(int ceoGoalId) {
		this.ceoGoalId = ceoGoalId;
	}

	public int getCeoGoalId() {
		return this.ceoGoalId;
	}

	public void setCeoGoalId(int ceoGoalId) {
		this.ceoGoalId = ceoGoalId;
	}

	
	public AppraisalPeriod getAppraisalPeriod() {
		return this.appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	
	public GoalOrg getGoalOrg() {
		return this.goalOrg;
	}

	public void setGoalOrg(GoalOrg goalOrg) {
		this.goalOrg = goalOrg;
	}

	
	public String getCeoGoalDesc() {
		return this.ceoGoalDesc;
	}

	public void setCeoGoalDesc(String ceoGoalDesc) {
		this.ceoGoalDesc = ceoGoalDesc;
	}

	
	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
