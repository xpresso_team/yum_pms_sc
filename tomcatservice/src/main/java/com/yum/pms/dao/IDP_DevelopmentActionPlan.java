package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author jayanta.biswas
 * @comment This is a pojo class for data sending
 * @version 1.0
 * @since   21-12-2017
 */
@Entity
@Table(name = "idp_developmentactionplan_dtl")
@JsonIgnoreType
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@idpDevelopmentActionPlanDtlId")
public class IDP_DevelopmentActionPlan implements Serializable {

	private static final long serialVersionUID = -2156733535547636558L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idp_development_action_plan_dtl_id", unique = true, nullable = false)
	private Integer idpDevelopmentActionPlanDtlId ;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idp_sheet_id")
	private IDP_SheetMaster idpSheetMaster;
	
	@Column(name="idp_development_action_plan_dtl_desc")
	private String developmentActionPlanDes;
	
	@Column(name="idp_development_action_plan_dtl_timeline")
	private String developmentActionTimeLine; 
	
	@Column(name="CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate")
	private Date createdDate;
	
	@Column(name="UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UpdatedDate")
	private Date updatedDate;
	
	@Column(name="is_ongoing")
	private Boolean isOngoing;
	
	@Column(name="is_active")
	private Boolean isActive=true;
	
	@Column(name="idp_suggested_goal_section_id")
	private Integer idpSuggestedGoalSectionId;
	
	@Column(name="idp_suggested_development_plan_section_id")
	private Integer idpSuggestedDevelopmentPlanSectionId;

	public Integer getIdpDevelopmentActionPlanDtlId() {
		return idpDevelopmentActionPlanDtlId;
	}

	public void setIdpDevelopmentActionPlanDtlId(Integer idpDevelopmentActionPlanDtlId) {
		this.idpDevelopmentActionPlanDtlId = idpDevelopmentActionPlanDtlId;
	}

	public IDP_SheetMaster getIdpSheetMaster() {
		return idpSheetMaster;
	}

	public void setIdpSheetMaster(IDP_SheetMaster idpSheetMaster) {
		this.idpSheetMaster = idpSheetMaster;
	}

	public String getDevelopmentActionPlanDes() {
		return developmentActionPlanDes;
	}

	public void setDevelopmentActionPlanDes(String developmentActionPlanDes) {
		this.developmentActionPlanDes = developmentActionPlanDes;
	}

	public String getDevelopmentActionTimeLine() {
		return developmentActionTimeLine;
	}

	public void setDevelopmentActionTimeLine(String developmentActionTimeLine) {
		this.developmentActionTimeLine = developmentActionTimeLine;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Boolean getIsOngoing() {
		return isOngoing;
	}

	public void setIsOngoing(Boolean isOngoing) {
		this.isOngoing = isOngoing;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getIdpSuggestedGoalSectionId() {
		return idpSuggestedGoalSectionId;
	}

	public void setIdpSuggestedGoalSectionId(Integer idpSuggestedGoalSectionId) {
		this.idpSuggestedGoalSectionId = idpSuggestedGoalSectionId;
	}
	
	public Integer getIdpSuggestedDevelopmentPlanSectionId() {
		return idpSuggestedDevelopmentPlanSectionId;
	}

	public void setIdpSuggestedDevelopmentPlanSectionId(
			Integer idpSuggestedDevelopmentPlanSectionId) {
		this.idpSuggestedDevelopmentPlanSectionId = idpSuggestedDevelopmentPlanSectionId;
	}

}
