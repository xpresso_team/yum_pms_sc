/******************************************************************** 
* Hibernate DAO Object Mapper for cascaded_emp table 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "cascaded_emp")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@casecadedEmpEventId")
public class CasecadeEmp implements Serializable {
	
	private static final long serialVersionUID = 6498042721541186253L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cascaded_emp_event_id", unique = true, nullable = false)
	private Integer casecadedEmpEventId;
	
	//@ManyToOne(fetch = FetchType.LAZY)
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cascade_to_empid", nullable = false)
	private EmployeeNew employee;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cascaded_to_goal_section_id")
	private GoalSection goalSection;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cascade_id", nullable = false)
	private Cascaded casecaded;
	
	@Column(name = "CreatedBy", length = 1000)
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate", length = 23)
	private Date createdDate;
	
	@Column(name = "UpdatedBy", length = 100)
	private String updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate", length = 23)
	private Date updatedDate;
	

	public Integer getCasecadedEmpEventId() {
		return casecadedEmpEventId;
	}

	public void setCasecadedEmpEventId(Integer casecadedEmpEventId) {
		this.casecadedEmpEventId = casecadedEmpEventId;
	}

	public EmployeeNew getEmployee() {
		return employee;
	}

	public void setEmployee(EmployeeNew employee) {
		this.employee = employee;
	}

	public GoalSection getGoalSection() {
		return goalSection;
	}

	public void setGoalSection(GoalSection goalSection) {
		this.goalSection = goalSection;
	}

	public Cascaded getCasecaded() {
		return casecaded;
	}

	public void setCasecaded(Cascaded casecaded) {
		this.casecaded = casecaded;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	

	


}
