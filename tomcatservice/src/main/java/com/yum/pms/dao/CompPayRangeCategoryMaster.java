package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
@Table(name = "comp_payrange_category_mstr")
public class CompPayRangeCategoryMaster implements Serializable, Comparable<CompPayRangeCategoryMaster> {

	private static final long serialVersionUID = -1184804182342363251L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "payrange_category_id", unique = true, nullable = false)
	private Integer payRangeCategoryId;
	
	@ManyToOne
	@JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@Column(name = "payrange_category_name")
	private String payRangeCategoryName;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	@Column(name = "display_order")
	private Integer displayOrder;
	
	@Column(name = "display_name")
	private String displayName;
	
	public CompPayRangeCategoryMaster() {
	}
	
	public CompPayRangeCategoryMaster(Integer payRangeCategoryId) {
		this.payRangeCategoryId = payRangeCategoryId;
	}

	@Override
	public String toString() {
		return "CompPayRangeCategoryMaster [payRangeCategoryId=" + payRangeCategoryId + ", appraisalPeriod="
				+ appraisalPeriod + ", payRangeCategoryName=" + payRangeCategoryName + ", displayOrder=" + displayOrder
				+ "]";
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(this.getPayRangeCategoryId()).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		
		if(!(obj instanceof CompPayRangeCategoryMaster)) {
			return false;
		}

		if (getClass() != obj.getClass()) {
			return false;
		}

		if (this == obj) {
			return true;
		}
		CompPayRangeCategoryMaster other = (CompPayRangeCategoryMaster) obj;
		
		if(this.getPayRangeCategoryId() == null || other.getPayRangeCategoryId() == null) {
			return new EqualsBuilder()
					.append(this.getDisplayOrder(), other.getDisplayOrder())
					.isEquals();
		}
		return new EqualsBuilder()
				.append(this.getPayRangeCategoryId(), other.getPayRangeCategoryId())
				.isEquals();
	}

	@Override
	public int compareTo(CompPayRangeCategoryMaster payRangeCategory) {
		return this.displayOrder.compareTo(payRangeCategory.getDisplayOrder());
	}

	public Integer getPayRangeCategoryId() {
		return payRangeCategoryId;
	}

	public void setPayRangeCategoryId(Integer payRangeCategoryId) {
		this.payRangeCategoryId = payRangeCategoryId;
	}

	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	public String getPayRangeCategoryName() {
		return payRangeCategoryName;
	}

	public void setPayRangeCategoryName(String payRangeCategoryName) {
		this.payRangeCategoryName = payRangeCategoryName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

}
