package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreType;

@Entity
@Table(name = "idp_plan_mstr")
@JsonIgnoreType
public class IDP_PlanMaster implements Serializable {

	private static final long serialVersionUID = -5992322545480387797L;

	@Id
	@Column(name="idp_plan_id", unique = true, nullable = false)
	private Integer idpPlanId;

	@Column(name="idp_plan_desc")
	private String idpPlanDesc;
	
	@Column(name="is_active")
	private Boolean isActive;
	
	@Column(name="CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate")
	private Date createdDate;
	
	@Column(name="UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UpdatedDate")
	private Date updatedDate;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "idpPlanMaster")
	private Set<IDP_PlanDetails> idpPlan = new HashSet<>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "idpPlanMaster")
	private Set<IDP_PlanUIConfig> idpPlanUIConfig = new HashSet<>(0);

	public Integer getIdpPlanId() {
		return idpPlanId;
	}

	public void setIdpPlanId(Integer idpPlanId) {
		this.idpPlanId = idpPlanId;
	}

	public String getIdpPlanDesc() {
		return idpPlanDesc;
	}

	public void setIdpPlanDesc(String idpPlanDesc) {
		this.idpPlanDesc = idpPlanDesc;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Set<IDP_PlanDetails> getIdpPlan() {
		return idpPlan;
	}

	public void setIdpPlan(Set<IDP_PlanDetails> idpPlan) {
		this.idpPlan = idpPlan;
	}

	public Set<IDP_PlanUIConfig> getIdpPlanUIConfig() {
		return idpPlanUIConfig;
	}

	public void setIdpPlanUIConfig(Set<IDP_PlanUIConfig> idpPlanUIConfig) {
		this.idpPlanUIConfig = idpPlanUIConfig;
	}
	
}
