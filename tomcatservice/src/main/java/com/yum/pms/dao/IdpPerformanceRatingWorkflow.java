/**
 * 
 */
package com.yum.pms.dao;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author jayanta.biswas  
 *
 */

@Entity
@Table(name = "idp_performance_rating_LTS_workflow")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@idpPerformanceRatingLTSWorkflowId")
public class IdpPerformanceRatingWorkflow implements Serializable {

	private static final long serialVersionUID = 723663329471124117L;

	@Id
	@Column(name="idp_performance_rating_LTS_workflow_id")
	private Integer idpPerformanceRatingLTSWorkflowId;
	
	@Column(name="idp_performance_rating_LTS_workflow_status_id")
	private Integer idpPerformanceRatingLTSWorkflowStatusId;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="next_possible_idp_performance_rating_LTS_workflow_status_id")
	private IDP_PerformanceRatingStatus nextPossibleIdpSheetRatingStatus;
	
	@Column(name="WebServiceUrl")
	private String webServiceURL;

	/**
	 * @return the idpPerformanceRatingLTSWorkflowId
	 */
	public Integer getIdpPerformanceRatingLTSWorkflowId() {
		return idpPerformanceRatingLTSWorkflowId;
	}

	/**
	 * @param idpPerformanceRatingLTSWorkflowId the idpPerformanceRatingLTSWorkflowId to set
	 */
	public void setIdpPerformanceRatingLTSWorkflowId(Integer idpPerformanceRatingLTSWorkflowId) {
		this.idpPerformanceRatingLTSWorkflowId = idpPerformanceRatingLTSWorkflowId;
	}

	/**
	 * @return the idpPerformanceRatingLTSWorkflowStatusId
	 */
	public Integer getIdpPerformanceRatingLTSWorkflowStatusId() {
		return idpPerformanceRatingLTSWorkflowStatusId;
	}

	/**
	 * @param idpPerformanceRatingLTSWorkflowStatusId the idpPerformanceRatingLTSWorkflowStatusId to set
	 */
	public void setIdpPerformanceRatingLTSWorkflowStatusId(Integer idpPerformanceRatingLTSWorkflowStatusId) {
		this.idpPerformanceRatingLTSWorkflowStatusId = idpPerformanceRatingLTSWorkflowStatusId;
	}

	/**
	 * @return the nextPossibleIdpSheetRatingStatus
	 */
	public IDP_PerformanceRatingStatus getNextPossibleIdpSheetRatingStatus() {
		return nextPossibleIdpSheetRatingStatus;
	}

	/**
	 * @param nextPossibleIdpSheetRatingStatus the nextPossibleIdpSheetRatingStatus to set
	 */
	public void setNextPossibleIdpSheetRatingStatus(IDP_PerformanceRatingStatus nextPossibleIdpSheetRatingStatus) {
		this.nextPossibleIdpSheetRatingStatus = nextPossibleIdpSheetRatingStatus;
	}

	/**
	 * @return the webServiceURL
	 */
	public String getWebServiceURL() {
		return webServiceURL;
	}

	/**
	 * @param webServiceURL the webServiceURL to set
	 */
	public void setWebServiceURL(String webServiceURL) {
		this.webServiceURL = webServiceURL;
	}
	
	

}
