package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.yum.idp.domain.IdpSubCategory;

@Entity
@Table(name = "idp_sub_section_mstr")
@JsonIgnoreType
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@idpSubSectionId")
public class IDP_SubSectionMaster implements Serializable {
	
	private static final long serialVersionUID = 5645221926072252884L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idp_sub_section_id", unique = true, nullable = false)
	private Integer idpSubSectionId;
	
	@Column(name="idp_sub_section_desc")
	private String idpSubSectionDesc;
	
	@Column(name="is_active")
	private Boolean isActive;
	
	@Column(name="CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate", updatable = false)
	private Date createdDate;
	
	@Column(name="UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UpdatedDate", insertable = false)
	private Date updatedDate;
	
	@ManyToOne
	@JoinColumn(name="idp_section_id")
	private IDP_SectionMaster sectionMaster;
	
	@Transient
	private boolean removable = true;
	
	@Column(name="grade_range_lower")
	private String gradeRangeLower = "0";
	
	@Column(name="grade_range_upper")
	private String gradeRangeUpper = "99";
	
	@Column(name="appr_id")
	private Integer apprId;
	
	public Integer getApprId() {
		return apprId;
	}

	public void setApprId(Integer apprId) {
		this.apprId = apprId;
	}

	public IDP_SubSectionMaster() {
	}

	public IDP_SubSectionMaster(Integer subCategoryId) {
		this.idpSubSectionId = subCategoryId;
	}

	public IDP_SubSectionMaster(IdpSubCategory subCategory) {
		this.idpSubSectionId = subCategory.getIdpSubSectionId();
		this.idpSubSectionDesc = subCategory.getIdpSubSectionDesc();
		this.isActive = subCategory.getIsActive();
		this.createdBy = subCategory.getCreatedBy();
		this.updatedBy = subCategory.getUpdatedBy();
		this.removable = subCategory.isRemovable();
		this.gradeRangeLower = subCategory.getGradeRangeLower();
		this.gradeRangeUpper = subCategory.getGradeRangeUpper();
		this.sectionMaster = new IDP_SectionMaster(subCategory.getIdpSectionId());
		this.apprId = subCategory.getApprId();
	}

	public Integer getIdpSubSectionId() {
		return idpSubSectionId;
	}

	public void setIdpSubSectionId(Integer idpSubSectionId) {
		this.idpSubSectionId = idpSubSectionId;
	}

	public String getIdpSubSectionDesc() {
		return idpSubSectionDesc;
	}

	public void setIdpSubSectionDesc(String idpSubSectionDesc) {
		this.idpSubSectionDesc = idpSubSectionDesc;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public IDP_SectionMaster getSectionMaster() {
		return sectionMaster;
	}

	public void setSectionMaster(IDP_SectionMaster sectionMaster) {
		this.sectionMaster = sectionMaster;
	}

	public boolean isRemovable() {
		return removable;
	}

	public void setRemovable(boolean removable) {
		this.removable = removable;
	}
	
	public String getGradeRangeLower() {
		return gradeRangeLower;
	}
	
	public void setGradeRangeLower(String gradeRangeLower) {
		this.gradeRangeLower = gradeRangeLower;
	}
	
	public String getGradeRangeUpper() {
		return gradeRangeUpper;
	}
	
	public void setGradeRangeUpper(String gradeRangeUpper) {
		this.gradeRangeUpper = gradeRangeUpper;
	}
	
}
