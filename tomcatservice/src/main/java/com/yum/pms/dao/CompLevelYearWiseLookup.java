package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
@Table(name = "comp_level_year_wise_lookup_table")
public class CompLevelYearWiseLookup implements Serializable {
	
	private static final long serialVersionUID = -2277688587861125234L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "lookup_id", unique = true, nullable = false)
	private Integer lookupId;
	
	@ManyToOne
	@JoinColumn(name = "appraisal_year_id")
	private AppraisalPeriod appraisalPeriod;
	
	@ManyToOne
    @JoinColumn(name = "grade_id")
	private EmployeeGradeMaster gradeMaster;
	
	@ManyToOne
    @JoinColumn(name = "salary_head_id")
	private CompLookupSalaryHeads salaryHeads;
	
	@Column(name = "head_value")
	private Double headValue;
	
	@Column(name = "IsActive")
	private Boolean active = true;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	public CompLevelYearWiseLookup() {
	}

	public CompLevelYearWiseLookup(Integer lookupId) {
		this.lookupId = lookupId;
	}

	public Integer getLookupId() {
		return lookupId;
	}

	public void setLookupId(Integer lookupId) {
		this.lookupId = lookupId;
	}

	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	public EmployeeGradeMaster getGradeMaster() {
		return gradeMaster;
	}

	public void setGradeMaster(EmployeeGradeMaster gradeMaster) {
		this.gradeMaster = gradeMaster;
	}

	public CompLookupSalaryHeads getSalaryHeads() {
		return salaryHeads;
	}

	public void setSalaryHeads(CompLookupSalaryHeads salaryHeads) {
		this.salaryHeads = salaryHeads;
	}

	public Double getHeadValue() {
		return headValue;
	}

	public void setHeadValue(Double headValue) {
		this.headValue = headValue;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public String toString() {
		return "CompLevelYearWiseLookup [lookupId=" + lookupId + ", appraisal Period Id=" 
				+ (appraisalPeriod != null ? appraisalPeriod.getAppraisalPeriodId() : 0)
				+ ", grade ID=" + (gradeMaster != null ? gradeMaster.getGradeId() : 0) 
				+ ", salaryHeads=" + (salaryHeads != null ? salaryHeads : "null") + ", headValue=" + headValue + "]";
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(this.getLookupId()).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		
		if(!(obj instanceof CompLevelYearWiseLookup)) {
			return false;
		}

		if (getClass() != obj.getClass()) {
			return false;
		}

		if (this == obj) {
			return true;
		}
		CompLevelYearWiseLookup other = (CompLevelYearWiseLookup) obj;
		return new EqualsBuilder()
				.append(this.getLookupId(), other.getLookupId())
				.isEquals();
	}
}
