/**
 * 
 */
package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author dipak.swain
 * This is a model class for 'pa_config_IF' table
 */
@Entity
@Table(name = "pa_config_IF")
@JsonIgnoreType
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@paConfigIFId")
public class PA_ConfigIF implements Serializable {

	private static final long serialVersionUID = 5466840211521588514L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer paConfigIFId;
	
	@Column(name = "IF_value_weighted_avg_indicator_percent")
	private Double ifValueWeightedAvgIndicatorPercent;
	
	@ManyToOne
    @JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;

	/**
	 * @return the paConfigIFId
	 */
	public Integer getPaConfigIFId() {
		return paConfigIFId;
	}

	/**
	 * @param paConfigIFId the paConfigIFId to set
	 */
	public void setPaConfigIFId(Integer paConfigIFId) {
		this.paConfigIFId = paConfigIFId;
	}

	/**
	 * @return the tfValueWeightedAvgIndicatorPercent
	 */
	public Double getIfValueWeightedAvgIndicatorPercent() {
		return ifValueWeightedAvgIndicatorPercent;
	}

	/**
	 * @param ifValueWeightedAvgIndicatorPercent the tfValueWeightedAvgIndicatorPercent to set
	 */
	public void setIfValueWeightedAvgIndicatorPercent(Double ifValueWeightedAvgIndicatorPercent) {
		this.ifValueWeightedAvgIndicatorPercent = ifValueWeightedAvgIndicatorPercent;
	}

	/**
	 * @return the appraisalPeriod
	 */
	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	/**
	 * @param appraisalPeriod the appraisalPeriod to set
	 */
	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the updatedBy
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the updatedDate
	 */
	public Date getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * @param updatedDate the updatedDate to set
	 */
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
}
