package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.yum.pms.utils.YumPmsConstants;

/**
 * @author jayanta.biswas
 * @comment This is a pojo class for data sending
 * @version 1.0
 * @since   10-01-2018
 */
@Entity
@Table(name = "idp_performance_rating")
@JsonIgnoreType
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@idpPerformanceRatingId")
public class IDP_PerformanceRating implements Serializable {

	private static final long serialVersionUID = -5713414167924885131L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idp_performance_rating_id", unique = true, nullable = false)
	private Integer idpPerformanceRatingId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="idp_performance_category_id")
	private IDP_PerformanceCategoryMaster idpPerformanceCategoryMaster;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idp_sheet_id")
	private IDP_SheetMaster idpSheetMaster;
	
	@Column(name="rating_desc")
	private String ratingDesc;
	
	@Column(name="CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate")
	private Date createdDate;
	
	@Column(name="UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UpdatedDate")
	private Date updatedDate;
	
	@Column(name="remarks_curr")
	private String remarksCurr;
	
	@Column(name = YumPmsConstants.IS_ACTION_PENDING)
	private Boolean actionPending;

	/**
	 * @return
	 */
	public Integer getIdpPerformanceRatingId() {
		return idpPerformanceRatingId;
	}

	/**
	 * @param idpPerformanceRatingId
	 */
	public void setIdpPerformanceRatingId(Integer idpPerformanceRatingId) {
		this.idpPerformanceRatingId = idpPerformanceRatingId;
	}

	/**
	 * @return
	 */
	public IDP_PerformanceCategoryMaster getIdpPerformanceCategoryMaster() {
		return idpPerformanceCategoryMaster;
	}

	/**
	 * @param idpPerformanceCategoryMaster
	 */
	public void setIdpPerformanceCategoryMaster(IDP_PerformanceCategoryMaster idpPerformanceCategoryMaster) {
		this.idpPerformanceCategoryMaster = idpPerformanceCategoryMaster;
	}

	/**
	 * @return
	 */
	public IDP_SheetMaster getIdpSheetMaster() {
		return idpSheetMaster;
	}

	/**
	 * @param idpSheetMaster
	 */
	public void setIdpSheetMaster(IDP_SheetMaster idpSheetMaster) {
		this.idpSheetMaster = idpSheetMaster;
	}

	/**
	 * @return
	 */
	public String getRatingDesc() {
		return ratingDesc;
	}

	/**
	 * @param ratingDesc
	 */
	public void setRatingDesc(String ratingDesc) {
		this.ratingDesc = ratingDesc;
	}

	/**
	 * @return
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return
	 */
	public Date getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * @param updatedDate
	 */
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	/**
	 * @return
	 */
	public String getRemarksCurr() {
		return remarksCurr;
	}

	/**
	 * @param remarksCurr
	 */
	public void setRemarksCurr(String remarksCurr) {
		this.remarksCurr = remarksCurr;
	}

	/**
	 * @return the actionPending
	 */
	public Boolean getActionPending() {
		return actionPending;
	}

	/**
	 * @param actionPending the actionPending to set
	 */
	public void setActionPending(Boolean actionPending) {
		this.actionPending = actionPending;
	}

}
