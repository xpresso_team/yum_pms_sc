package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author dipak.swain
 * This is a model class for 'pa_config_rating_BellCurve_IF_Range' table
 */
@Entity
@Table(name = "pa_config_rating_BellCurve_IF_Range")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@paConfigRatingBellCurveId")
public class PA_ConfigRatingBellCurveIFRange implements Serializable {
	
	private static final long serialVersionUID = -7368995016447858196L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer paConfigRatingBellCurveId;
	
	@ManyToOne
	@JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@ManyToOne
	@JoinColumn(name = "pa_rating_id")
	private PA_RatingMaster paRating;
	
	@Column(name = "bell_curve_value_percent")
	private Double bellCurveValuePercent;
	
	@Column(name = "IF_range_value_lower")
	private Double ifRangeValueLower;
	
	@Column(name = "IF_range_value_upper")
	private Double ifRangeValueUpper;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;

	/**
	 * @return the paConfigRatingBellCurveId
	 */
	public Integer getPaConfigRatingBellCurveId() {
		return paConfigRatingBellCurveId;
	}

	/**
	 * @param paConfigRatingBellCurveId the paConfigRatingBellCurveId to set
	 */
	public void setPaConfigRatingBellCurveId(Integer paConfigRatingBellCurveId) {
		this.paConfigRatingBellCurveId = paConfigRatingBellCurveId;
	}

	/**
	 * @return the appraisalPeriod
	 */
	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	/**
	 * @param appraisalPeriod the appraisalPeriod to set
	 */
	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	/**
	 * @return the paRating
	 */
	public PA_RatingMaster getPaRating() {
		return paRating;
	}

	/**
	 * @param paRating the paRating to set
	 */
	public void setPaRating(PA_RatingMaster paRating) {
		this.paRating = paRating;
	}

	/**
	 * @return the bellCurveValuePercent
	 */
	public Double getBellCurveValuePercent() {
		return bellCurveValuePercent;
	}

	/**
	 * @param bellCurveValuePercent the bellCurveValuePercent to set
	 */
	public void setBellCurveValuePercent(Double bellCurveValuePercent) {
		this.bellCurveValuePercent = bellCurveValuePercent;
	}

	/**
	 * @return the ifRangeValueLower
	 */
	public Double getIfRangeValueLower() {
		return ifRangeValueLower;
	}

	/**
	 * @param ifRangeValueLower the ifRangeValueLower to set
	 */
	public void setIfRangeValueLower(Double ifRangeValueLower) {
		this.ifRangeValueLower = ifRangeValueLower;
	}

	/**
	 * @return the ifRangeValueUpper
	 */
	public Double getIfRangeValueUpper() {
		return ifRangeValueUpper;
	}

	/**
	 * @param ifRangeValueUpper the ifRangeValueUpper to set
	 */
	public void setIfRangeValueUpper(Double ifRangeValueUpper) {
		this.ifRangeValueUpper = ifRangeValueUpper;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the updatedBy
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the updatedDate
	 */
	public Date getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * @param updatedDate the updatedDate to set
	 */
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
}
