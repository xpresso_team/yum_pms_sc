package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author dipak.swain
 * This is a model class for 'pa_config_TF' table
 */
@Entity
@Table(name = "pa_config_TF")
@JsonIgnoreType
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@paConfigTFId")
public class PA_ConfigTF implements Serializable {

	private static final long serialVersionUID = -9138153463830129783L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer paConfigTFId;
	
	@Column(name = "TF_value_percent")
	private Double tfValuePercent;
	
	@ManyToOne
    @JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	@Column(name = "TF_value_tb_percent")
	private Double tfValueTbPercent;
	
	@Column(name = "TF_value_ph_percent")
	private Double tfValuePhPercent;

	/**
	 * @return the paConfigTFId
	 */
	public Integer getPaConfigTFId() {
		return paConfigTFId;
	}

	/**
	 * @param paConfigTFId the paConfigTFId to set
	 */
	public void setPaConfigTFId(Integer paConfigTFId) {
		this.paConfigTFId = paConfigTFId;
	}

	/**
	 * @return the tfValuePercent
	 */
	public Double getTfValuePercent() {
		return tfValuePercent;
	}

	/**
	 * @param tfValuePercent the tfValuePercent to set
	 */
	public void setTfValuePercent(Double tfValuePercent) {
		this.tfValuePercent = tfValuePercent;
	}

	/**
	 * @return the appraisalPeriod
	 */
	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	/**
	 * @param appraisalPeriod the appraisalPeriod to set
	 */
	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the updatedBy
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the updatedDate
	 */
	public Date getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * @param updatedDate the updatedDate to set
	 */
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	/**
	 * @return the tfValueTbPercent
	 */
	public Double getTfValueTbPercent() {
		return tfValueTbPercent;
	}

	/**
	 * @param tfValueTbPercent the tfValueTbPercent to set
	 */
	public void setTfValueTbPercent(Double tfValueTbPercent) {
		this.tfValueTbPercent = tfValueTbPercent;
	}

	/**
	 * @return the tfValuePhPercent
	 */
	public Double getTfValuePhPercent() {
		return tfValuePhPercent;
	}

	/**
	 * @param tfValuePhPercent the tfValuePhPercent to set
	 */
	public void setTfValuePhPercent(Double tfValuePhPercent) {
		this.tfValuePhPercent = tfValuePhPercent;
	}

}
