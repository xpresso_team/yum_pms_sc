package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author dipak.swain
 * This is a model class for 'pa_goal_rating' table
 */
@Entity
@Table(name = "pa_goal_rating")
@JsonIgnoreType
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@paGoalRatingId")
public class PA_GoalRating implements Serializable {

	private static final long serialVersionUID = 987200386952481412L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pa_goal_rating_id")
	private Integer paGoalRatingId;
	
	@ManyToOne
    @JoinColumn(name = "pa_sheet_id")
	private PA_SheetMaster paSheet;
	
	@ManyToOne
	@JoinColumn(name = "goal_id")
	private Goal goal;
	
	@ManyToOne
	@JoinColumn(name = "org_goal_id")
	private GoalOrg goalOrg;
	
	@ManyToOne
    @JoinColumn(name = "pa_rating_id_emp")
	private PA_RatingMaster paRatingEmp;
	
	@ManyToOne
    @JoinColumn(name = "pa_rating_id_sup")
	private PA_RatingMaster paRatingSup;
	
	@ManyToOne
    @JoinColumn(name = "sup_emp_id")
	private EmployeeNew supEmp;
	
	@Column(name = "sup_remark")
	private String supRemark;
	
	@ManyToOne
    @JoinColumn(name = "emp_id")
	private EmployeeNew emp;
	
	@ManyToOne
    @JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@ManyToOne
    @JoinColumn(name = "goal_section_id")
	private GoalSection goalSection;
	
	@Column(name="is_active")
	private Boolean active = true;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	@Column(name = "self_remark")
	private String selfRemark;

	public Integer getPaGoalRatingId() {
		return paGoalRatingId;
	}

	public void setPaGoalRatingId(Integer paGoalRatingId) {
		this.paGoalRatingId = paGoalRatingId;
	}

	public PA_SheetMaster getPaSheet() {
		return paSheet;
	}

	public void setPaSheet(PA_SheetMaster paSheet) {
		this.paSheet = paSheet;
	}

	public Goal getGoal() {
		return goal;
	}

	public void setGoal(Goal goal) {
		this.goal = goal;
	}

	public GoalOrg getGoalOrg() {
		return goalOrg;
	}

	public void setGoalOrg(GoalOrg goalOrg) {
		this.goalOrg = goalOrg;
	}

	public PA_RatingMaster getPaRatingEmp() {
		return paRatingEmp;
	}

	public void setPaRatingEmp(PA_RatingMaster paRatingEmp) {
		this.paRatingEmp = paRatingEmp;
	}

	public PA_RatingMaster getPaRatingSup() {
		return paRatingSup;
	}

	public void setPaRatingSup(PA_RatingMaster paRatingSup) {
		this.paRatingSup = paRatingSup;
	}

	public EmployeeNew getSupEmp() {
		return supEmp;
	}

	public void setSupEmp(EmployeeNew supEmp) {
		this.supEmp = supEmp;
	}

	public String getSupRemark() {
		return supRemark;
	}

	public void setSupRemark(String supRemark) {
		this.supRemark = supRemark;
	}

	public EmployeeNew getEmp() {
		return emp;
	}

	public void setEmp(EmployeeNew emp) {
		this.emp = emp;
	}

	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public GoalSection getGoalSection() {
		return goalSection;
	}

	public void setGoalSection(GoalSection goalSection) {
		this.goalSection = goalSection;
	}

	public String getSelfRemark() {
		return selfRemark;
	}

	public void setSelfRemark(String selfRemark) {
		this.selfRemark = selfRemark;
	}

	@Override
	public String toString() {
		return "PA_GoalRating [paGoalRatingId=" + paGoalRatingId + ", paSheet=" + paSheet + ", goal=" + goal
				+ ", goalOrg=" + goalOrg + ", paRatingEmp=" + paRatingEmp + ", supEmp=" + supEmp + ", supRemark="
				+ supRemark + ", emp=" + emp + ", appraisalPeriod=" + appraisalPeriod + "]";
	}
	
}
