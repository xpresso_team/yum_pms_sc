package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
@Table(name = "comp_lookup_salary_heads")
public class CompLookupSalaryHeads implements Serializable, Comparable<CompLookupSalaryHeads> {
	
	private static final long serialVersionUID = 5812896536824218989L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private Integer headId;
	
	@Column(name = "head_name")
	private String headName;
	
	@ManyToOne
	@JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "display_name")
	private String displayName;
	
	@Column(name = "display_order")
	private Integer displayOrder;
	
	public CompLookupSalaryHeads() {
	}

	public CompLookupSalaryHeads(Integer headId) {
		this.headId = headId;
	}

	public CompLookupSalaryHeads(Integer headId, String headName) {
		this.headId = headId;
		this.headName = headName;
	}

	public Integer getHeadId() {
		return headId;
	}

	public void setHeadId(Integer headId) {
		this.headId = headId;
	}

	public String getHeadName() {
		return headName;
	}

	public void setHeadName(String headName) {
		this.headName = headName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	@Override
	public String toString() {
		return "LookupSalaryHeads [headId=" + headId + ", headName=" + headName + ", appraisal Period Id=" 
				+ (appraisalPeriod != null ? appraisalPeriod.getAppraisalPeriodId() : 0)
				+ ", displayName=" + displayName + ", displayOrder=" + displayOrder + "]";
	}

	@Override
	public int compareTo(CompLookupSalaryHeads salaryHeads) {
		return this.displayOrder.compareTo(salaryHeads.getDisplayOrder());
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(this.getHeadId()).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		
		if(!(obj instanceof CompLookupSalaryHeads)) {
			return false;
		}

		if (getClass() != obj.getClass()) {
			return false;
		}

		if (this == obj) {
			return true;
		}
		CompLookupSalaryHeads other = (CompLookupSalaryHeads) obj;
		return new EqualsBuilder()
				.append(this.getHeadId(), other.getHeadId())
				.isEquals();
	}
}
