package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author dipak.swain
 * This is a model class for 'pa_overall_rating_sup' table
 */
@Entity
@Table(name = "pa_overall_rating_sup")
@JsonIgnoreType
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@paOverallRatingSupId")
public class PA_OverallRatingSup implements Serializable {
	
	private static final long serialVersionUID = 320198585390799689L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pa_overall_rating_sup_id")
	private Integer paOverallRatingSupId;
	
	@ManyToOne
    @JoinColumn(name = "pa_sheet_id")
	private PA_SheetMaster paSheet;
	
	@ManyToOne
    @JoinColumn(name = "pa_rating_id")
	private PA_RatingMaster paRating;
	
	@ManyToOne
    @JoinColumn(name = "sup_emp_id")
	private EmployeeNew supEmp;
	
	@Column(name = "sup_remark")
	private String supRemark;
	
	@Column(name = "sup_promo_nom")
	private Boolean supPromoNom;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	@Column(name = "sup_IF")
	private Double supIf;
	
	@Column(name = "sup_promo_nom_aug")
	private Boolean supPromoNomAug = false;
	
	@Column(name = "dev_check_in")
	private Boolean devCheckIn;
	
	@Column(name = "dev_check_in_remarks")
	private String devCheckInRremarks;
	
	@ManyToOne
    @JoinColumn(name = "culture_rating_id")
	private PaCultureRatingMstr cultureRatingMstr;

	public Integer getPaOverallRatingSupId() {
		return paOverallRatingSupId;
	}

	public void setPaOverallRatingSupId(Integer paOverallRatingSupId) {
		this.paOverallRatingSupId = paOverallRatingSupId;
	}

	public PA_SheetMaster getPaSheet() {
		return paSheet;
	}

	public void setPaSheet(PA_SheetMaster paSheet) {
		this.paSheet = paSheet;
	}

	public PA_RatingMaster getPaRating() {
		return paRating;
	}

	public void setPaRating(PA_RatingMaster paRating) {
		this.paRating = paRating;
	}

	public EmployeeNew getSupEmp() {
		return supEmp;
	}

	public void setSupEmp(EmployeeNew supEmp) {
		this.supEmp = supEmp;
	}

	public String getSupRemark() {
		return supRemark;
	}

	public void setSupRemark(String supRemark) {
		this.supRemark = supRemark;
	}

	public Boolean getSupPromoNom() {
		return supPromoNom;
	}

	public void setSupPromoNom(Boolean supPromoNom) {
		this.supPromoNom = supPromoNom;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Double getSupIf() {
		return supIf;
	}

	public void setSupIf(Double supIf) {
		this.supIf = supIf;
	}

	public Boolean getSupPromoNomAug() {
		return supPromoNomAug;
	}

	public void setSupPromoNomAug(Boolean supPromoNomAug) {
		this.supPromoNomAug = supPromoNomAug;
	}

	/**
	 * @return the devCheckIn
	 */
	public Boolean getDevCheckIn() {
		return devCheckIn;
	}

	/**
	 * @param devCheckIn the devCheckIn to set
	 */
	public void setDevCheckIn(Boolean devCheckIn) {
		this.devCheckIn = devCheckIn;
	}

	/**
	 * @return the devCheckInRremarks
	 */
	public String getDevCheckInRremarks() {
		return devCheckInRremarks;
	}

	/**
	 * @param devCheckInRremarks the devCheckInRremarks to set
	 */
	public void setDevCheckInRremarks(String devCheckInRremarks) {
		this.devCheckInRremarks = devCheckInRremarks;
	}

	/**
	 * @return the cultureRatingMstr
	 */
	public PaCultureRatingMstr getCultureRatingMstr() {
		return cultureRatingMstr;
	}

	/**
	 * @param cultureRatingMstr the cultureRatingMstr to set
	 */
	public void setCultureRatingMstr(PaCultureRatingMstr cultureRatingMstr) {
		this.cultureRatingMstr = cultureRatingMstr;
	}

}
