package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "idp_selected_suggested_goal_dtl")
public class IDP_SelectedSuggestedGoalDtl implements Serializable {

	private static final long serialVersionUID = -2276367836564109906L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idp_selected_suggested_goal_dtl_id", unique = true, nullable = false)
	private Integer idpSelectedSuggestedGoalDtlId;

	@ManyToOne
	@JoinColumn(name = "idp_sheet_id" )
	private IDP_SheetMaster idpSheetMaster;
	
	@Column(name="idp_selected_suggested_goal_id")
	private Integer idpSelectedSuggestedGoalId;
	
	@Column(name="idp_selected_suggested_goal_others_desc")
	private String idpSelectedSuggestedGoalOthersDesc;
	
	@Column(name="is_ongoing")
	private Boolean isOnGoing;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="timeline")
	private Date timeline;
	

	@Column(name="is_active")
	private Boolean isActive;
	
	@Column(name="CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate")
	private Date createdDate;
	
	@Column(name="UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UpdatedDate")
	private Date updatedDate;
	
	@Column(name="idp_selected_suggested_goal_desc")
	private String idpSelectedSuggestedGoalDesc;
	

	public Integer getIdpSelectedSuggestedGoalDtlId() {
		return idpSelectedSuggestedGoalDtlId;
	}

	public void setIdpSelectedSuggestedGoalDtlId(
			Integer idpSelectedSuggestedGoalDtlId) {
		this.idpSelectedSuggestedGoalDtlId = idpSelectedSuggestedGoalDtlId;
	}

	public IDP_SheetMaster getIdpSheetMaster() {
		return idpSheetMaster;
	}

	public void setIdpSheetMaster(IDP_SheetMaster idpSheetMaster) {
		this.idpSheetMaster = idpSheetMaster;
	}

	public Integer getIdpSelectedSuggestedGoalId() {
		return idpSelectedSuggestedGoalId;
	}

	public void setIdpSelectedSuggestedGoalId(Integer idpSelectedSuggestedGoalId) {
		this.idpSelectedSuggestedGoalId = idpSelectedSuggestedGoalId;
	}

	public String getIdpSelectedSuggestedGoalOthersDesc() {
		return idpSelectedSuggestedGoalOthersDesc;
	}

	public void setIdpSelectedSuggestedGoalOthersDesc(
			String idpSelectedSuggestedGoalOthersDesc) {
		this.idpSelectedSuggestedGoalOthersDesc = idpSelectedSuggestedGoalOthersDesc;
	}

	public Boolean getIsOnGoing() {
		return isOnGoing;
	}

	public void setIsOnGoing(Boolean isOnGoing) {
		this.isOnGoing = isOnGoing;
	}

	public Date getTimeline() {
		return timeline;
	}

	public void setTimeline(Date timeline) {
		this.timeline = timeline;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getIdpSelectedSuggestedGoalDesc() {
		return idpSelectedSuggestedGoalDesc;
	}

	public void setIdpSelectedSuggestedGoalDesc(String idpSelectedSuggestedGoalDesc) {
		this.idpSelectedSuggestedGoalDesc = idpSelectedSuggestedGoalDesc;
	}
	
	
}
