package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "idp_BalanceYearGoals")
@JsonIgnoreType
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@idpBalanceYearGoalsId")
public class IDP_BalanceYearGoal implements Serializable {
	
	private static final long serialVersionUID = -4350274433373146463L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idp_BalanceYearGoals_id", unique = true, nullable = false)
	private Integer idpBalanceYearGoalsId ;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idp_sheet_id")
	private IDP_SheetMaster idpSheetMaster;
	
	@Column(name="goal_section_id")
	private Integer goalSectionId;
	
	@Column(name="is_active")
	private Boolean isActive; 
	
	@Column(name="CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate")
	private Date createdDate;
	
	@Column(name="UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UpdatedDate")
	private Date updatedDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="goal_section_timeline")
	private Date goalSectionTimeline;
	
	@Column(name="BalanceYearGoals_desc")//BalanceYearGoals_desc
	private String balanceYearGoalsDesc;

	public Integer getIdpBalanceYearGoalsId() {
		return idpBalanceYearGoalsId;
	}

	public void setIdpBalanceYearGoalsId(Integer idpBalanceYearGoalsId) {
		this.idpBalanceYearGoalsId = idpBalanceYearGoalsId;
	}

	public IDP_SheetMaster getIdpSheetMaster() {
		return idpSheetMaster;
	}

	public void setIdpSheetMaster(IDP_SheetMaster idpSheetMaster) {
		this.idpSheetMaster = idpSheetMaster;
	}

	public Integer getGoalSectionId() {
		return goalSectionId;
	}

	public void setGoalSectionId(Integer goalSectionId) {
		this.goalSectionId = goalSectionId;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	public String getBalanceYearGoalsDesc() {
		return balanceYearGoalsDesc;
	}

	public void setBalanceYearGoalsDesc(String balanceYearGoalsDesc) {
		this.balanceYearGoalsDesc = balanceYearGoalsDesc;
	}

	public Date getGoalSectionTimeline() {
		return goalSectionTimeline;
	}

	public void setGoalSectionTimeline(Date goalSectionTimeline) {
		this.goalSectionTimeline = goalSectionTimeline;
	}
	

}
