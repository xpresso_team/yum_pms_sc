package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "comp_letter_template")
public class CompLetterTemplate implements Serializable {

	private static final long serialVersionUID = -3507274957487194055L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "letter_id", unique = true, nullable = false)
	private Integer letterId;
	
	@Column(name = "letter_name")
	private String letterName;
	
	@Column(name = "letter_description")
	private String letterDescription;
	
	@Column(name = "is_default")
	private Boolean defaultLetter;
	
	@Column(name = "default_letter")
	private String defaultLetterBody;
	
	@Column(name = "custom_letter")
	private String customLetterBody;
	
	@Column(name = "IsActive")
	private Boolean active = true;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	@ManyToOne
	@JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	public CompLetterTemplate() {
	}

	public CompLetterTemplate(Integer letterId) {
		this.letterId = letterId;
	}

	public CompLetterTemplate(Integer letterId, String letterName) {
		this.letterId = letterId;
		this.letterName = letterName;
	}

	public Integer getLetterId() {
		return letterId;
	}

	public void setLetterId(Integer letterId) {
		this.letterId = letterId;
	}

	public String getLetterName() {
		return letterName;
	}

	public void setLetterName(String letterName) {
		this.letterName = letterName;
	}

	public String getLetterDescription() {
		return letterDescription;
	}

	public void setLetterDescription(String letterDescription) {
		this.letterDescription = letterDescription;
	}

	public Boolean getDefaultLetter() {
		return defaultLetter;
	}

	public void setDefaultLetter(Boolean defaultLetter) {
		this.defaultLetter = defaultLetter;
	}

	public String getDefaultLetterBody() {
		return defaultLetterBody;
	}

	public void setDefaultLetterBody(String defaultLetterBody) {
		this.defaultLetterBody = defaultLetterBody;
	}

	public String getCustomLetterBody() {
		return customLetterBody;
	}

	public void setCustomLetterBody(String customLetterBody) {
		this.customLetterBody = customLetterBody;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}
	
}
