package com.yum.pms.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "idp_sheet_mstr")
@JsonIgnoreType
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@idpSheetId")
public class IDP_SheetMaster implements Serializable {

	private static final long serialVersionUID = -6282161029181333510L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idp_sheet_id", unique = true, nullable = false)
	private Integer idpSheetId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="emp_id")
	private EmployeeNew employee = new EmployeeNew();
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod = new AppraisalPeriod();
	
	@Column(name="is_active")
	private Boolean isActive;
	
	@Column(name="CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate")
	private Date createdDate;
	
	@Column(name="UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UpdatedDate")
	private Date updatedDate;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idp_sheet_status_id")
	private IDP_SheetStatus idpStatus;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idp_performance_rating_LTS_workflow_status_id")
	private IDP_PerformanceRatingStatus idpPerformanceRatingLTSWorkflowStatusId;
	

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "idpSheetMaster")
	private List<IDP_SelectedSuggestedGoalDtl> idpSelectedSuggestedGoalList = new ArrayList<>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "idpSheetMaster")
	private Set<IDP_DevelopmentActionPlan> idpSheetDevelopmentPaln = new HashSet<>(0);
	
	public IDP_SheetMaster() {
	}
	
	public IDP_SheetMaster(Integer idpSheetId) {
		this.idpSheetId = idpSheetId;
	}

	public List<IDP_SelectedSuggestedGoalDtl> getIdpSelectedSuggestedGoalList() {
		return idpSelectedSuggestedGoalList;
	}

	public void setIdpSelectedSuggestedGoalList(
			List<IDP_SelectedSuggestedGoalDtl> idpSelectedSuggestedGoalList) {
		this.idpSelectedSuggestedGoalList = idpSelectedSuggestedGoalList;
	}

	public Integer getIdpSheetId() {
		return idpSheetId;
	}

	public void setIdpSheetId(Integer idpSheetId) {
		this.idpSheetId = idpSheetId;
	}

	public EmployeeNew getEmployee() {
		return employee;
	}

	public void setEmployee(EmployeeNew employee) {
		this.employee = employee;
	}

	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Set<IDP_DevelopmentActionPlan> getIdpSheetDevelopmentPaln() {
		return idpSheetDevelopmentPaln;
	}

	public void setIdpSheetDevelopmentPaln(Set<IDP_DevelopmentActionPlan> idpSheetDevelopmentPaln) {
		this.idpSheetDevelopmentPaln = idpSheetDevelopmentPaln;
	}

	public IDP_SheetStatus getIdpStatus() {
		return idpStatus;
	}

	public void setIdpStatus(IDP_SheetStatus idpStatus) {
		this.idpStatus = idpStatus;
	}

	public IDP_PerformanceRatingStatus getIdpPerformanceRatingLTSWorkflowStatusId() {
		return idpPerformanceRatingLTSWorkflowStatusId;
	}

	public void setIdpPerformanceRatingLTSWorkflowStatusId(
			IDP_PerformanceRatingStatus idpPerformanceRatingLTSWorkflowStatusId) {
		this.idpPerformanceRatingLTSWorkflowStatusId = idpPerformanceRatingLTSWorkflowStatusId;
	}

	
}
