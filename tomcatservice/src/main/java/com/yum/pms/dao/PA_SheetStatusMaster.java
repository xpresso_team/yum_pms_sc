package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author dipak.swain
 * This is a model class for 'pa_sheet_status_mstr' table
 */
@Entity
@Table(name = "pa_sheet_status_mstr")
@JsonIgnoreType
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@paSheetStatusId")
public class PA_SheetStatusMaster implements Serializable {
	
	private static final long serialVersionUID = -1196723894152932544L;

	@Id
	@Column(name = "pa_sheet_status_id")
	private Integer paSheetStatusId;
	
	@Column(name = "pa_sheet_status_desc")
	private String paSheetStatusDesc;
	
	@Column(name = "IsActive")
	private Boolean active = true;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	public PA_SheetStatusMaster() {

	}

	public PA_SheetStatusMaster(Integer paSheetStatusId) {
		this.paSheetStatusId = paSheetStatusId;
	}
	
	public Integer getPaSheetStatusId() {
		return paSheetStatusId;
	}

	public void setPaSheetStatusId(Integer paSheetStatusId) {
		this.paSheetStatusId = paSheetStatusId;
	}

	public String getPaSheetStatusDesc() {
		return paSheetStatusDesc;
	}

	public void setPaSheetStatusDesc(String paSheetStatusDesc) {
		this.paSheetStatusDesc = paSheetStatusDesc;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public String toString() {
		return "PA_SheetStatusMaster [paSheetStatusId=" + paSheetStatusId + ", paSheetStatusDesc=" + paSheetStatusDesc
				+ "]";
	}
	
}
