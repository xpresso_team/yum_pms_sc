/******************************************************************** 
* Hibernate DAO Object Mapper for tagged table 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.dao;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "Tagged")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@tagId")
public class Tagged implements java.io.Serializable {

	private static final long serialVersionUID = 545225045338557508L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tag_id", unique = true, nullable = false)
	private Integer tagId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tag_by_empid")
	private EmployeeNew employee;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tag_by_goal_section_id")
	private GoalSection goalSection;
	
	@Column(name = "edited_gs_summary_to_emp", length = 2000)
	private String editedGsSummaryToEmp;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "tagged_timeline")
	private Date taggedTimeline;
	
	@Column(name = "CreatedBy", length = 100)
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate", length = 23)
	private Date createdDate;
	
	@Column(name = "UpdatedBy", length = 100)
	private String updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate", length = 23)
	private Date updatedDate;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "tagged")
	private Set<TaggedEmp> taggedEmp = new HashSet<>(0);

	public Tagged() {
	}
	
	public Tagged(Integer tagId) {
		this.tagId = tagId;
	}

	public Integer getTagId() {
		return this.tagId;
	}

	public void setTagId(Integer tagId) {
		this.tagId = tagId;
	}

	
	public AppraisalPeriod getAppraisalPeriod() {
		return this.appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}
	
	public EmployeeNew getEmployee() {
		return this.employee;
	}

	public void setEmployee(EmployeeNew employee) {
		this.employee = employee;
	}

	
	public GoalSection getGoalSection() {
		return this.goalSection;
	}

	public void setGoalSection(GoalSection goalSection) {
		this.goalSection = goalSection;
	}
	
	public String getEditedGsSummaryToEmp() {
		return editedGsSummaryToEmp;
	}

	public void setEditedGsSummaryToEmp(String editedGsSummaryToEmp) {
		this.editedGsSummaryToEmp = editedGsSummaryToEmp;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Set<TaggedEmp> getTaggedEmp() {
		return taggedEmp;
	}

	public void setTaggedEmp(Set<TaggedEmp> taggedEmp) {
		this.taggedEmp = taggedEmp;
	}

	public Date getTaggedTimeline() {
		return taggedTimeline;
	}

	public void setTaggedTimeline(Date taggedTimeline) {
		this.taggedTimeline = taggedTimeline;
	}	

}
