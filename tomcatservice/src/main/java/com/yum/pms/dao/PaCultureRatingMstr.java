package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "pa_culture_rating_mstr")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@cultureRatingId")
public class PaCultureRatingMstr implements Serializable {

	private static final long serialVersionUID = 7692629511885356640L;

	@Id
	@Column(name = "culture_rating_id")
	private Integer cultureRatingId;
	
	@Column(name = "culture_rating_desc")
	private String cultureRatingDesc;
	
	@Column(name = "pa_cultural_rating_short_desc")
	private String paCulturalRatingShortDesc;
	
	@Column(name = "is_active")
	private Boolean active = true;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	public PaCultureRatingMstr() {
	}

	public PaCultureRatingMstr(Integer cultureRatingId) {
		this.cultureRatingId = cultureRatingId;
	}

	/**
	 * @return the cultureRatingId
	 */
	public Integer getCultureRatingId() {
		return cultureRatingId;
	}

	/**
	 * @param cultureRatingId the cultureRatingId to set
	 */
	public void setCultureRatingId(Integer cultureRatingId) {
		this.cultureRatingId = cultureRatingId;
	}

	/**
	 * @return the cultureRatingDesc
	 */
	public String getCultureRatingDesc() {
		return cultureRatingDesc;
	}

	/**
	 * @param cultureRatingDesc the cultureRatingDesc to set
	 */
	public void setCultureRatingDesc(String cultureRatingDesc) {
		this.cultureRatingDesc = cultureRatingDesc;
	}

	/**
	 * @return the paCulturalRatingShortDesc
	 */
	public String getPaCulturalRatingShortDesc() {
		return paCulturalRatingShortDesc;
	}

	/**
	 * @param paCulturalRatingShortDesc the paCulturalRatingShortDesc to set
	 */
	public void setPaCulturalRatingShortDesc(String paCulturalRatingShortDesc) {
		this.paCulturalRatingShortDesc = paCulturalRatingShortDesc;
	}

	/**
	 * @return the isActive
	 */
	public Boolean getIsActive() {
		return active;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(Boolean active) {
		this.active = active;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the updatedBy
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the updatedDate
	 */
	public Date getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * @param updatedDate the updatedDate to set
	 */
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
