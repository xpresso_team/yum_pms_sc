package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreType;

/**
 * @author jayanta.biswas
 * @comment This is a pojo class for data sending
 * @version 1.0
 * @since   24-01-2018
 */
@Entity
@Table(name = "idp_performance_mstr")
@JsonIgnoreType
public class IDP_PerformanceMaster implements Serializable {

	private static final long serialVersionUID = 4714106805935788871L;

	@Id
	@Column(name="idp_performance_id", unique = true, nullable = false)
	private Integer idpPerformanceId;

	@Column(name="idp_performance_desc")
	private String idpPerformanceDesc;
	
	@Column(name="is_active")
	private Boolean isActive;
	
	@Column(name="CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate")
	private Date createdDate;
	
	@Column(name="UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UpdatedDate")
	private Date updatedDate;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "idpPerformanceMaster")
	private Set<IDP_PerformanceCategoryMaster> performanceCategoryMaster = new HashSet<>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "idpPerformanceMaster")
	private Set<IDP_PerformanceUIConfig> idpPerformanceUIConfig = new HashSet<>(0);

	public Integer getIdpPerformanceId() {
		return idpPerformanceId;
	}

	public void setIdpPerformanceId(Integer idpPerformanceId) {
		this.idpPerformanceId = idpPerformanceId;
	}

	public String getIdpPerformanceDesc() {
		return idpPerformanceDesc;
	}

	public void setIdpPerformanceDesc(String idpPerformanceDesc) {
		this.idpPerformanceDesc = idpPerformanceDesc;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Set<IDP_PerformanceCategoryMaster> getPerformanceCategoryMaster() {
		return performanceCategoryMaster;
	}

	public void setPerformanceCategoryMaster(Set<IDP_PerformanceCategoryMaster> performanceCategoryMaster) {
		this.performanceCategoryMaster = performanceCategoryMaster;
	}

	public Set<IDP_PerformanceUIConfig> getIdpPerformanceUIConfig() {
		return idpPerformanceUIConfig;
	}

	public void setIdpPerformanceUIConfig(Set<IDP_PerformanceUIConfig> idpPerformanceUIConfig) {
		this.idpPerformanceUIConfig = idpPerformanceUIConfig;
	}

}
