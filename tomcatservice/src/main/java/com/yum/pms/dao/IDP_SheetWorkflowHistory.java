/**
 * 
 */
package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author jayanta.biswas
 *
 */
@Entity
@Table(name = "idp_sheet_workflow_history")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@idpWorkflowHistId")
public class IDP_SheetWorkflowHistory implements Serializable {

	private static final long serialVersionUID = -3343321614765558644L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idp_workflow_hist_id", unique = true, nullable = false)
	private long idpWorkflowHistId;
	
	@Column(name = "idp_sheet_id")
	private int idpSheetId;
	
	@Column(name = "idp_sheet_status_id")
	private int idpStatusId;
	
	@Column(name = "appraisal_period_id")
	private int appraisalPeriodId;
	
	@Column(name = "emp_id")
	private int empId;
	
	@Column(name = "CreatedBy", length = 100)
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate", length = 23)
	private Date createdDate;

	public long getIdpWorkflowHistId() {
		return idpWorkflowHistId;
	}

	public void setIdpWorkflowHistId(long idpWorkflowHistId) {
		this.idpWorkflowHistId = idpWorkflowHistId;
	}

	public int getIdpSheetId() {
		return idpSheetId;
	}

	public void setIdpSheetId(int idpSheetId) {
		this.idpSheetId = idpSheetId;
	}

	public int getIdpStatusId() {
		return idpStatusId;
	}

	public void setIdpStatusId(int idpStatusId) {
		this.idpStatusId = idpStatusId;
	}

	public int getAppraisalPeriodId() {
		return appraisalPeriodId;
	}

	public void setAppraisalPeriodId(int appraisalPeriodId) {
		this.appraisalPeriodId = appraisalPeriodId;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
}
