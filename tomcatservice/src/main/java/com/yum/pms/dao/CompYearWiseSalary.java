package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "comp_yearwise_salary")
public class CompYearWiseSalary implements Serializable {
	
	private static final long serialVersionUID = -1094489366971363211L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer yearWiseSalaryId;
	
	@ManyToOne
	@JoinColumn(name = "emp_id")
	private EmployeeNew employee;
	
	@ManyToOne
	@JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@ManyToOne
	@JoinColumn(name = "salary_field_id")
	private CompSalaryFileds salaryFileds;
	
	@Column(name = "amount")
	private Double amount;
	
	@Column(name = "IsActive")
	private Boolean active = true;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	@Column(name = "status")
	private String status;
	
	public CompYearWiseSalary() {
	}

	public CompYearWiseSalary(Integer yearWiseSalaryId) {
		this.yearWiseSalaryId = yearWiseSalaryId;
	}

	public Integer getYearWiseSalaryId() {
		return yearWiseSalaryId;
	}

	public void setYearWiseSalaryId(Integer yearWiseSalaryId) {
		this.yearWiseSalaryId = yearWiseSalaryId;
	}

	public EmployeeNew getEmployee() {
		return employee;
	}

	public void setEmployee(EmployeeNew employee) {
		this.employee = employee;
	}

	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	public CompSalaryFileds getSalaryFileds() {
		return salaryFileds;
	}

	public void setSalaryFileds(CompSalaryFileds salaryFileds) {
		this.salaryFileds = salaryFileds;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "CompYearWiseSalary [yearWiseSalaryId=" + yearWiseSalaryId + ", employee=" + employee
				+ ", appraisalPeriodId=" + (appraisalPeriod != null ? appraisalPeriod.getAppraisalPeriodId() : 0) 
				+ ", salaryFileds=" + (salaryFileds != null ? salaryFileds.getSalaryFieldId() : 0) + ", amount=" + amount
				+ "]";
	}
}
