/**
 * 
 */
package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author jayanta.biswas
 *
 */
@Entity
@Table(name = "idp_remarks")
@JsonIgnoreType
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@idpRemarksId")
public class IDP_Remarks implements Serializable {

	private static final long serialVersionUID = 2069820563073504426L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idp_remarks_id", unique = true, nullable = false)
	private Integer idpRemarksId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idp_sheet_id")
	private IDP_SheetMaster idpSheetMaster;
	
	@Column(name="HowILead_appreciate_remarks")
	private String howILeadAppreciateRemarks;
	
	@Column(name="HowILead_effectiveness_remarks")
	private String howILeadEffectivenessRemarks;
	
	@Column(name="HowILead_growth_remarks")
	private String howILeadGrowthRemarks;
	
	@Column(name="MyPlan_remarks")
	private String myPlanRemarks;
	
	@Column(name="DevelopmentActionPlan_remarks")
	private String developmentActionPlanRemarks;
	
	@Column(name="PerformanceCheckIn_goals_remarks")
	private String performanceCheckInGoalsRemarks;
	
	@Column(name="BalanceYearGoals_remarks")
	private String balanceYearGoalsRemarks;
	
	@Column(name="CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate", updatable = false)
	private Date createdDate;
	
	@Column(name="UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UpdatedDate")
	private Date updatedDate;

	/**
	 * @return the idpRemarksId
	 */
	public Integer getIdpRemarksId() {
		return idpRemarksId;
	}

	/**
	 * @param idpRemarksId the idpRemarksId to set
	 */
	public void setIdpRemarksId(Integer idpRemarksId) {
		this.idpRemarksId = idpRemarksId;
	}

	/**
	 * @return the idpSheetMaster
	 */
	public IDP_SheetMaster getIdpSheetMaster() {
		return idpSheetMaster;
	}

	/**
	 * @param idpSheetMaster the idpSheetMaster to set
	 */
	public void setIdpSheetMaster(IDP_SheetMaster idpSheetMaster) {
		this.idpSheetMaster = idpSheetMaster;
	}

	/**
	 * @return the howILeadAppreciateRemarks
	 */
	public String getHowILeadAppreciateRemarks() {
		return howILeadAppreciateRemarks;
	}

	/**
	 * @param howILeadAppreciateRemarks the howILeadAppreciateRemarks to set
	 */
	public void setHowILeadAppreciateRemarks(String howILeadAppreciateRemarks) {
		this.howILeadAppreciateRemarks = howILeadAppreciateRemarks;
	}

	/**
	 * @return the howILeadEffectivenessRemarks
	 */
	public String getHowILeadEffectivenessRemarks() {
		return howILeadEffectivenessRemarks;
	}

	/**
	 * @param howILeadEffectivenessRemarks the howILeadEffectivenessRemarks to set
	 */
	public void setHowILeadEffectivenessRemarks(String howILeadEffectivenessRemarks) {
		this.howILeadEffectivenessRemarks = howILeadEffectivenessRemarks;
	}

	/**
	 * @return the howILeadGrowthRemarks
	 */
	public String getHowILeadGrowthRemarks() {
		return howILeadGrowthRemarks;
	}

	/**
	 * @param howILeadGrowthRemarks the howILeadGrowthRemarks to set
	 */
	public void setHowILeadGrowthRemarks(String howILeadGrowthRemarks) {
		this.howILeadGrowthRemarks = howILeadGrowthRemarks;
	}

	/**
	 * @return the myPlanRemarks
	 */
	public String getMyPlanRemarks() {
		return myPlanRemarks;
	}

	/**
	 * @param myPlanRemarks the myPlanRemarks to set
	 */
	public void setMyPlanRemarks(String myPlanRemarks) {
		this.myPlanRemarks = myPlanRemarks;
	}

	/**
	 * @return the developmentActionPlanRemarks
	 */
	public String getDevelopmentActionPlanRemarks() {
		return developmentActionPlanRemarks;
	}

	/**
	 * @param developmentActionPlanRemarks the developmentActionPlanRemarks to set
	 */
	public void setDevelopmentActionPlanRemarks(String developmentActionPlanRemarks) {
		this.developmentActionPlanRemarks = developmentActionPlanRemarks;
	}

	/**
	 * @return the performanceCheckInGoalsRemarks
	 */
	public String getPerformanceCheckInGoalsRemarks() {
		return performanceCheckInGoalsRemarks;
	}

	/**
	 * @param performanceCheckInGoalsRemarks the performanceCheckInGoalsRemarks to set
	 */
	public void setPerformanceCheckInGoalsRemarks(String performanceCheckInGoalsRemarks) {
		this.performanceCheckInGoalsRemarks = performanceCheckInGoalsRemarks;
	}

	/**
	 * @return the balanceYearGoalsRemarks
	 */
	public String getBalanceYearGoalsRemarks() {
		return balanceYearGoalsRemarks;
	}

	/**
	 * @param balanceYearGoalsRemarks the balanceYearGoalsRemarks to set
	 */
	public void setBalanceYearGoalsRemarks(String balanceYearGoalsRemarks) {
		this.balanceYearGoalsRemarks = balanceYearGoalsRemarks;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the updatedBy
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the updatedDate
	 */
	public Date getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * @param updatedDate the updatedDate to set
	 */
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
}
