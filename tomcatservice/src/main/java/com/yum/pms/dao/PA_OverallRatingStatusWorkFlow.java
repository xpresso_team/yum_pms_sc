package com.yum.pms.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author dipak.swain
 * This is a model class for 'pa_overall_rating_status_workflow' table
 */
@Entity
@Table(name = "pa_overall_rating_status_workflow")
@JsonIgnoreType
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@paOverallRatingStatusWorkflowId")
public class PA_OverallRatingStatusWorkFlow implements Serializable {

	private static final long serialVersionUID = -3815691350090540520L;

	@Id
	@Column(name = "pa_overall_rating_status_workflow_id")
	private Integer paOverallRatingStatusWorkflowId;
	
	@Column(name = "pa_overall_rating_status_id")
	private Integer paOverallRatingStatusId;
	
	@Column(name = "next_possible_pa_overall_rating_status_id")
	private Integer nextPossiblePaOverallRatingStatusId;
	
	@Column(name = "WebServiceURL")
	private String webServiceURL;

	public Integer getPaOverallRatingStatusWorkflowId() {
		return paOverallRatingStatusWorkflowId;
	}

	public void setPaOverallRatingStatusWorkflowId(Integer paOverallRatingStatusWorkflowId) {
		this.paOverallRatingStatusWorkflowId = paOverallRatingStatusWorkflowId;
	}

	public Integer getPaOverallRatingStatusId() {
		return paOverallRatingStatusId;
	}

	public void setPaOverallRatingStatusId(Integer paOverallRatingStatusId) {
		this.paOverallRatingStatusId = paOverallRatingStatusId;
	}

	public Integer getNextPossiblePaOverallRatingStatusId() {
		return nextPossiblePaOverallRatingStatusId;
	}
	
	public void setNextPossiblePaOverallRatingStatusId(Integer nextPossiblePaOverallRatingStatusId) {
		this.nextPossiblePaOverallRatingStatusId = nextPossiblePaOverallRatingStatusId;
	}

	public String getWebServiceURL() {
		return webServiceURL;
	}

	public void setWebServiceURL(String webServiceURL) {
		this.webServiceURL = webServiceURL;
	}

	@Override
	public String toString() {
		return "PA_OverallRatingStatusWorkFlow [paOverallRatingStatusWorkflowId=" + paOverallRatingStatusWorkflowId
				+ ", paOverallRatingStatusId=" + paOverallRatingStatusId + ", nextPossiblePaOverallRatingStatusId="
				+ nextPossiblePaOverallRatingStatusId + "]";
	}
	
}
