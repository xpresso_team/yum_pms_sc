package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "comp_yearwise_payrange")
public class CompYearWisePayRange implements Serializable {
	
	private static final long serialVersionUID = -5653754860038575495L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "yearwise_payrange_id", unique = true, nullable = false)
	private Integer yearWisePayRangeId;
	
	@ManyToOne
	@JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@ManyToOne
	@JoinColumn(name = "grade_id")
	private EmployeeGradeMaster gradeMaster;
	
	@Column(name = "min_range")
	private Double minRange;
	
	@Column(name = "max_range")
	private Double maxRange;
	
	@ManyToOne
	@JoinColumn(name = "payrange_category_id")
	private CompPayRangeCategoryMaster payRangeCategory;
	
	@Column(name = "increase_percentage")
	private Double increasePercentage;
	
	@Column(name = "IsActive")
	private Boolean active = true;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	public CompYearWisePayRange() {
	}

	public CompYearWisePayRange(Integer yearWisePayRangeId) {
		this.yearWisePayRangeId = yearWisePayRangeId;
	}

	public CompYearWisePayRange(boolean active) {
		this.active = active;
	}

	public Integer getYearWisePayRangeId() {
		return yearWisePayRangeId;
	}

	public void setYearWisePayRangeId(Integer yearWisePayRangeId) {
		this.yearWisePayRangeId = yearWisePayRangeId;
	}

	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	public EmployeeGradeMaster getGradeMaster() {
		return gradeMaster;
	}

	public void setGradeMaster(EmployeeGradeMaster gradeMaster) {
		this.gradeMaster = gradeMaster;
	}

	public Double getMinRange() {
		return minRange;
	}

	public void setMinRange(Double minRange) {
		this.minRange = minRange;
	}

	public Double getMaxRange() {
		return maxRange;
	}

	public void setMaxRange(Double maxRange) {
		this.maxRange = maxRange;
	}

	public CompPayRangeCategoryMaster getPayRangeCategory() {
		return payRangeCategory;
	}

	public void setPayRangeCategory(CompPayRangeCategoryMaster payRangeCategory) {
		this.payRangeCategory = payRangeCategory;
	}

	public Double getIncreasePercentage() {
		return increasePercentage;
	}

	public void setIncreasePercentage(Double increasePercentage) {
		this.increasePercentage = increasePercentage;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public String toString() {
		return "CompYearWisePayRange [Key=" + yearWisePayRangeId + ", apprId=" 
				+ (appraisalPeriod != null ? String.valueOf(appraisalPeriod.getAppraisalPeriodId()) : 0)
				+ ", gradeId=" + (gradeMaster != null ? String.valueOf(gradeMaster.getGradeId()) : 0) 
				+ ", minRange=" + minRange + ", maxRange=" + maxRange
				+ ", payRangeCategory=" + payRangeCategory + ", increasePercentage=" + increasePercentage + "]";
	}
	
}
