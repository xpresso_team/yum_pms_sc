/******************************************************************** 
* Hibernate DAO Object Mapper for pms_role table 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@Table(name = "pms_role")
//@JsonIgnoreType
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@pmsRoleId")
public class PmsRole implements java.io.Serializable {

	private static final long serialVersionUID = 2130295064543306294L;
	
	@Id
	@Column(name="pms_role_id")
	private Integer pmsRoleId;
	
	@Column(name="pms_role_name")
	private String pmsRoleName;
	
	@Column(name="CreatedBy")
	private String createdBy;
	
	@Column(name="UpdatedBy")
	private String updatedBy;
	
	@Column(name="CreatedDate")
	private Date createdDate;
	
	@Column(name="UpdatedDate")
	private Date updatedDate;

	public PmsRole() {
	}
	
	public PmsRole(Integer pmsRoleId) {
		this.pmsRoleId = pmsRoleId;
	}
	
	public Integer getPmsRoleId() {
		return pmsRoleId;
	}

	public void setPmsRoleId(Integer pmsRoleId) {
		this.pmsRoleId = pmsRoleId;
	}

	public String getPmsRoleName() {
		return pmsRoleName;
	}

	public void setPmsRoleName(String pmsRoleName) {
		this.pmsRoleName = pmsRoleName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
