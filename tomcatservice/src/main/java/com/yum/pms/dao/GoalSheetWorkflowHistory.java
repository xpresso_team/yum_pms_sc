/******************************************************************** 
* Hibernate DAO Object Mapper for goal_sheet_workflow_history table 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: sudipta.chandra
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@Table(name = "goal_sheet_workflow_history")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@gsWorkflowHistId")
public class GoalSheetWorkflowHistory implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "gs_workflow_hist_id", unique = true, nullable = false)
	private long gsWorkflowHistId;
	
	@Column(name = "goal_sheet_id")
	private int goalSheetId;
	
	@Column(name = "goal_sheet_status_id")
	private int goalStatusId;
	
	@Column(name = "appraisal_period_id")
	private int appraisalPeriodId;
	
	@Column(name = "emp_id")
	private int empId;
	
	@Column(name = "CreatedBy", length = 100)
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate", length = 23)
	private Date createdDate;
	
	public GoalSheetWorkflowHistory() {
	}
	
	public GoalSheetWorkflowHistory(long gsWorkflowHistId) {
		this.gsWorkflowHistId = gsWorkflowHistId;
	}

	public long getGsWorkflowHistId() {
		return gsWorkflowHistId;
	}

	public void setGsWorkflowHistId(long gsWorkflowHistId) {
		this.gsWorkflowHistId = gsWorkflowHistId;
	}

	public int getGoalSheetId() {
		return goalSheetId;
	}

	public void setGoalSheetId(int goalSheetId) {
		this.goalSheetId = goalSheetId;
	}

	public int getGoalStatusId() {
		return goalStatusId;
	}

	public void setGoalStatusId(int goalStatusId) {
		this.goalStatusId = goalStatusId;
	}

	public int getAppraisalPeriodId() {
		return appraisalPeriodId;
	}

	public void setAppraisalPeriodId(int appraisalPeriodId) {
		this.appraisalPeriodId = appraisalPeriodId;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	

}
