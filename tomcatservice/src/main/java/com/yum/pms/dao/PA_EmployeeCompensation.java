package com.yum.pms.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author dipak.swain
 * This is a model class for 'pa_emp_compensation' table
 */
@Entity
@Table(name = "pa_emp_compensation")
@JsonIgnoreType
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@paEmpCompensationId")
public class PA_EmployeeCompensation implements Serializable {
	
	private static final long serialVersionUID = -35914124787366712L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer paEmpCompensationId;
	
	@ManyToOne
    @JoinColumn(name = "emp_id")
	private EmployeeNew employee;
	
	@ManyToOne
    @JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@Column(name = "Vpay")
	private Double variablePay;

	/**
	 * @return the paEmpCompensationId
	 */
	public Integer getPaEmpCompensationId() {
		return paEmpCompensationId;
	}

	/**
	 * @param paEmpCompensationId the paEmpCompensationId to set
	 */
	public void setPaEmpCompensationId(Integer paEmpCompensationId) {
		this.paEmpCompensationId = paEmpCompensationId;
	}

	/**
	 * @return the employee
	 */
	public EmployeeNew getEmployee() {
		return employee;
	}

	/**
	 * @param employee the employee to set
	 */
	public void setEmployee(EmployeeNew employee) {
		this.employee = employee;
	}

	/**
	 * @return the appraisalPeriod
	 */
	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	/**
	 * @param appraisalPeriod the appraisalPeriod to set
	 */
	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	/**
	 * @return the variablePay
	 */
	public Double getVariablePay() {
		return variablePay;
	}

	/**
	 * @param variablePay the variablePay to set
	 */
	public void setVariablePay(Double variablePay) {
		this.variablePay = variablePay;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PA_EmployeeCompensation [paEmpCompensationId=" + paEmpCompensationId + ", employee=" + employee
				+ ", appraisalPeriod=" + appraisalPeriod + ", variablePay=" + variablePay + "]";
	}
	
}
