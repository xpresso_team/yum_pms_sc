package com.yum.pms.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreType;

@Entity
@Table(name = "idp_performance_category_ui_config")
@JsonIgnoreType
public class IDP_PerformanceUIConfig implements Serializable {
	
	private static final long serialVersionUID = 5580190670471422650L;

	@Id
	@Column(name="idp_performance_category_UI_config_id", unique = true, nullable = false)
	private Integer idpPerformanceUIConfig_id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="idp_performance_id")
	private IDP_PerformanceMaster idpPerformanceMaster;	
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="idp_performance_category_id")
	private IDP_PerformanceCategoryMaster idpPerformanceCategoryMaster;	
	
	@Column(name="UI_control_type")
	private String UI_control_type;
	
	@Column(name="UI_control_label")
	private String uiControlLabel;
	
	@Column(name="UI_control_value")
	private String uiControlValue;
	
	@Column(name="is_active")
	private Boolean isActive;

	public Integer getIdpPerformanceUIConfig_id() {
		return idpPerformanceUIConfig_id;
	}

	public void setIdpPerformanceUIConfig_id(Integer idpPerformanceUIConfigId) {
		this.idpPerformanceUIConfig_id = idpPerformanceUIConfigId;
	}

	public IDP_PerformanceMaster getIdpPerformanceMaster() {
		return idpPerformanceMaster;
	}

	public void setIdpPerformanceMaster(IDP_PerformanceMaster idpPerformanceMaster) {
		this.idpPerformanceMaster = idpPerformanceMaster;
	}

	public IDP_PerformanceCategoryMaster getIdpPerformanceCategoryMaster() {
		return idpPerformanceCategoryMaster;
	}

	public void setIdpPerformanceCategoryMaster(IDP_PerformanceCategoryMaster idpPerformanceCategoryMaster) {
		this.idpPerformanceCategoryMaster = idpPerformanceCategoryMaster;
	}

	public String getUI_control_type() {
		return UI_control_type;
	}

	public void setUI_control_type(String uiControlType) {
		UI_control_type = uiControlType;
	}

	public String getUiControlLabel() {
		return uiControlLabel;
	}

	public void setUiControlLabel(String uiControlLabel) {
		this.uiControlLabel = uiControlLabel;
	}

	public String getUiControlValue() {
		return uiControlValue;
	}

	public void setUiControlValue(String uiControlValue) {
		this.uiControlValue = uiControlValue;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}
