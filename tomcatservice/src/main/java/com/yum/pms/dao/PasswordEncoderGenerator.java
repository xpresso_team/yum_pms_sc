/******************************************************************** 
 * Password encoder 
 * 
 * http://www.abzooba.com
 * ------------------- 
 * Created By: keshav.kumar
 * Last Updated By: sudipta.chandra
 * Last Updated On: 23-Sep-2017 (11:34:34 AM) 
 *
 * The software and related user documentation are protected under 
 * copyright laws and remain the sole property of Abzooba. 
 * 
 * Technical support is available via the Abzooba website at 
 * http://www.abzooba.com
 **************************************************************************/

package com.yum.pms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.yum.pms.utils.DbConnection;
import com.yum.pms.utils.YumPmsConstants;

public class PasswordEncoderGenerator {

	private static final Logger LOGGER = LoggerFactory.getLogger(PasswordEncoderGenerator.class);

	public static void main(String[] args) {

		DbConnection obj = new DbConnection();
		Connection conn = obj.getConnection();

		String selectSQL = "SELECT username, password FROM emp_login";
		try(
				PreparedStatement preparedStatement = conn.prepareStatement(selectSQL);
				ResultSet rs = preparedStatement.executeQuery();
				PreparedStatement ps = conn.prepareStatement("UPDATE emp_login SET password = ? WHERE username = ?")) {
			while (rs.next()) {
				String password = rs.getString("password");
				String username = rs.getString("username");
				BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
				String hashedPassword = passwordEncoder.encode(password);
				LOGGER.info("Password encrypted for username = {}", username);
				// set the prepared statement parameters
				ps.setString(1,hashedPassword);
				ps.setString(2,username);
				// call executeUpdate to execute our sql update statement
				ps.executeUpdate();
			}
		} catch (SQLException e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
		}

	}

}
