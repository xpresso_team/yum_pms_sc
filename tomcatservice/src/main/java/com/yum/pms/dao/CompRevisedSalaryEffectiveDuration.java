package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "comp_revised_salary_effective_duration")
public class CompRevisedSalaryEffectiveDuration implements Serializable {

	private static final long serialVersionUID = -8466251832813512459L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@Column(name = "salary_duration")
	private Date salaryDuration;
	
	@Column(name = "IsActive")
	private Boolean active = true;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	public CompRevisedSalaryEffectiveDuration() {
	}

	public CompRevisedSalaryEffectiveDuration(Integer id) {
		this.id = id;
	}

	public CompRevisedSalaryEffectiveDuration(Integer id, Integer apprId) {
		this.id = id;
		this.appraisalPeriod = new AppraisalPeriod(apprId);
	}

	public CompRevisedSalaryEffectiveDuration(Integer id, Integer apprId, Date salaryDuration) {
		this.id = id;
		this.appraisalPeriod = new AppraisalPeriod(apprId);
		this.salaryDuration = salaryDuration;
	}
	
	public CompRevisedSalaryEffectiveDuration(Integer apprId, Date salaryDuration) {
		this.appraisalPeriod = new AppraisalPeriod(apprId);
		this.salaryDuration = salaryDuration;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	public Date getSalaryDuration() {
		return salaryDuration;
	}

	public void setSalaryDuration(Date salaryDuration) {
		this.salaryDuration = salaryDuration;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
