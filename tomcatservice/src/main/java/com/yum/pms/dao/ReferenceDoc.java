package com.yum.pms.dao;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreType;

@Entity
@Table(name = "reference_doc")
@JsonIgnoreType
public class ReferenceDoc implements java.io.Serializable {
	
	private static final long serialVersionUID = -766085234497772832L;

	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="FileID", unique = true, nullable = false)
	private String fileId;
	
	@Column(name="appraisal_period_id")
	private Integer appraisalPeriodId;
	
	@Column(name="emp_id")
	private Integer empId;
	
	@Column(name="filename")
	private String fileName;
	
	@Column(name="filedesc")
	private String fileDesc;
	
	@Column(name="filepath")
	private String filePath;
	
	@Column(name="IsAdminDoc")
	private Boolean isAdminDoc;
	
	@Column(name="CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate", length = 23)
	private Date createdDate;
	
	@Transient
	private String operationMassage;

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public Integer getAppraisalPeriodId() {
		return appraisalPeriodId;
	}

	public void setAppraisalPeriodId(Integer appraisalPeriodId) {
		this.appraisalPeriodId = appraisalPeriodId;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileDesc() {
		return fileDesc;
	}

	public void setFileDesc(String fileDesc) {
		this.fileDesc = fileDesc;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Boolean getIsAdminDoc() {
		return isAdminDoc;
	}

	public void setIsAdminDoc(Boolean isAdminDoc) {
		this.isAdminDoc = isAdminDoc;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getOperationMassage() {
		return operationMassage;
	}

	public void setOperationMassage(String operationMassage) {
		this.operationMassage = operationMassage;
	}
	
	


}
