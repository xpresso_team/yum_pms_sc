/**
 * 
 */
package com.yum.pms.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.yum.pms.utils.YumPmsConstants;

/**
 * @author jayanta.biswas
 *
 */

@Entity
@Table(name = "idp_performance_rating_reporting")
@JsonIgnoreType
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@idpPerformanceRatingReportingId")
public class Idp_PerformanceRatingReporting implements Serializable {
	
	private static final long serialVersionUID = -5384418433800647586L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idp_performance_rating_reporting_id", unique = true, nullable = false)
	private Integer idpPerformanceRatingReportingId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="emp_id")
	private EmployeeNew employee;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="appraisal_period_id")
	private AppraisalPeriod appraisalPeriord;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idp_sheet_id")
	private IDP_SheetMaster idpSheetMaster;
	
	@Column(name="rating_LTS_by_sup")
	private String ratingLTSbySup;
	
	@Column(name="rating_LTS_by_LT")
	private String ratingLTSbyLT;
	
	@Column(name="rating_LTS_by_HR")
	private String ratingLTSbyHR;
	
	@Column(name="rating_culture_by_sup")
	private String ratingCulturebySup;
	
	@Column(name="rating_culture_by_LT")
	private String ratingCultureByLT;
	
	@Column(name="rating_culture_by_HR")
	private String ratingCulturebyHR;
	
	@Column(name="remarks_sup")
	private String remarksSup;
	
	@Column(name="remarks_LT")
	private String remarksLT;
	
	@Column(name="remarks_HR")
	private String remarksHR;
	
	@Column(name="is_active")
	private Boolean isActive = true;
	
	@Column(name = YumPmsConstants.RATING_GOAL_BY_SUP)
	private String ratingGoalBySup;
	
	@Column(name = YumPmsConstants.RATING_GOAL_BY_LT)
	private String ratingGoalByLt;

	/**
	 * @return
	 */
	public Integer getIdpPerformanceRatingReportingId() {
		return idpPerformanceRatingReportingId;
	}

	/**
	 * @param idpPerformanceRatingReportingId
	 */
	public void setIdpPerformanceRatingReportingId(Integer idpPerformanceRatingReportingId) {
		this.idpPerformanceRatingReportingId = idpPerformanceRatingReportingId;
	}

	/**
	 * @return
	 */
	public EmployeeNew getEmployee() {
		return employee;
	}

	/**
	 * @param employee
	 */
	public void setEmployee(EmployeeNew employee) {
		this.employee = employee;
	}

	/**
	 * @return
	 */
	public AppraisalPeriod getAppraisalPeriord() {
		return appraisalPeriord;
	}

	/**
	 * @param appraisalPeriord
	 */
	public void setAppraisalPeriord(AppraisalPeriod appraisalPeriord) {
		this.appraisalPeriord = appraisalPeriord;
	}

	/**
	 * @return
	 */
	public IDP_SheetMaster getIdpSheetMaster() {
		return idpSheetMaster;
	}

	/**
	 * @param idpSheetMaster
	 */
	public void setIdpSheetMaster(IDP_SheetMaster idpSheetMaster) {
		this.idpSheetMaster = idpSheetMaster;
	}

	/**
	 * @return
	 */
	public String getRatingLTSbySup() {
		return ratingLTSbySup;
	}

	/**
	 * @param ratingLTSbySup
	 */
	public void setRatingLTSbySup(String ratingLTSbySup) {
		this.ratingLTSbySup = ratingLTSbySup;
	}

	/**
	 * @return
	 */
	public String getRatingLTSbyLT() {
		return ratingLTSbyLT;
	}

	/**
	 * @param ratingLTSbyLT
	 */
	public void setRatingLTSbyLT(String ratingLTSbyLT) {
		this.ratingLTSbyLT = ratingLTSbyLT;
	}

	/**
	 * @return
	 */
	public String getRatingLTSbyHR() {
		return ratingLTSbyHR;
	}

	/**
	 * @param ratingLTSbyHR
	 */
	public void setRatingLTSbyHR(String ratingLTSbyHR) {
		this.ratingLTSbyHR = ratingLTSbyHR;
	}

	/**
	 * @return
	 */
	public String getRatingCulturebySup() {
		return ratingCulturebySup;
	}

	/**
	 * @param ratingCulturebySup
	 */
	public void setRatingCulturebySup(String ratingCulturebySup) {
		this.ratingCulturebySup = ratingCulturebySup;
	}

	/**
	 * @return
	 */
	public String getRatingCultureByLT() {
		return ratingCultureByLT;
	}

	/**
	 * @param ratingCultureByLT
	 */
	public void setRatingCultureByLT(String ratingCultureByLT) {
		this.ratingCultureByLT = ratingCultureByLT;
	}

	/**
	 * @return
	 */
	public String getRatingCulturebyHR() {
		return ratingCulturebyHR;
	}

	/**
	 * @param ratingCulturebyHR
	 */
	public void setRatingCulturebyHR(String ratingCulturebyHR) {
		this.ratingCulturebyHR = ratingCulturebyHR;
	}

	/**
	 * @return
	 */
	public String getRemarksSup() {
		return remarksSup;
	}

	/**
	 * @param remarksSup
	 */
	public void setRemarksSup(String remarksSup) {
		this.remarksSup = remarksSup;
	}

	/**
	 * @return
	 */
	public String getRemarksLT() {
		return remarksLT;
	}

	/**
	 * @param remarksLT
	 */
	public void setRemarksLT(String remarksLT) {
		this.remarksLT = remarksLT;
	}

	/**
	 * @return
	 */
	public String getRemarksHR() {
		return remarksHR;
	}

	/**
	 * @param remarksHR
	 */
	public void setRemarksHR(String remarksHR) {
		this.remarksHR = remarksHR;
	}

	/**
	 * @return the isActive
	 */
	public Boolean getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	/**
	 * @return the ratingGoalBySup
	 */
	public String getRatingGoalBySup() {
		return ratingGoalBySup;
	}

	/**
	 * @param ratingGoalBySup the ratingGoalBySup to set
	 */
	public void setRatingGoalBySup(String ratingGoalBySup) {
		this.ratingGoalBySup = ratingGoalBySup;
	}

	/**
	 * @return the ratingGoalByLt
	 */
	public String getRatingGoalByLt() {
		return ratingGoalByLt;
	}

	/**
	 * @param ratingGoalByLt the ratingGoalByLt to set
	 */
	public void setRatingGoalByLt(String ratingGoalByLt) {
		this.ratingGoalByLt = ratingGoalByLt;
	}

}
