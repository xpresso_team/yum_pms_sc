/******************************************************************** 
 * Hibernate DAO Object Mapper for tagged_emp table 
 * 
 * http://www.abzooba.com
 * ------------------- 
 * Created By: jayanta.biswas
 * Last Updated By: sudipta.chandra
 * Last Updated On: 23-Sep-2017 (11:34:34 AM) 
 *
 * The software and related user documentation are protected under 
 * copyright laws and remain the sole property of Abzooba. 
 * 
 * Technical support is available via the Abzooba website at 
 * http://www.abzooba.com
 **************************************************************************/

package com.yum.pms.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@Table(name = "tagged_emp")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@taggedEmpEventId")
public class TaggedEmp implements java.io.Serializable {

	private static final long serialVersionUID = -1492673148357086898L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tagged_emp_event_id", unique = true, nullable = false)
	private Integer taggedEmpEventId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tag_to_empid", nullable = false)
	private EmployeeNew employee;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tag_to_goal_section_id")
	private GoalSection goalSection;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tag_id", nullable = false)
	private Tagged tagged;

	@Column(name = "tag_rejection_desc", length = 2000)
	private String tagRejactionDesc;

	@Column(name = "tag_accept_flag")
	private boolean tagStatus;

	@Column(name = "CreatedBy")
	private String CreatedBy;

	@Column(name = "UpdatedBy")
	private String UpdatedBy;

	public TaggedEmp() {
	}

	public TaggedEmp(Integer taggedEmpEventId) {
		this.taggedEmpEventId = taggedEmpEventId;
	}

	public Integer getTaggedEmpEventId() {
		return taggedEmpEventId;
	}

	public void setTaggedEmpEventId(Integer taggedEmpEventId) {
		this.taggedEmpEventId = taggedEmpEventId;
	}

	public String getTagRejactionDesc() {
		return tagRejactionDesc;
	}

	public void setTagRejactionDesc(String tagRejactionDesc) {
		this.tagRejactionDesc = tagRejactionDesc;
	}

	public Boolean getTagStatus() {
		return tagStatus;
	}

	public void setTagStatus(Boolean tagStatus) {
		this.tagStatus = tagStatus;
	}

	public void setTagStatus(boolean tagStatus) {
		this.tagStatus = tagStatus;
	}

	public EmployeeNew getEmployee() {
		return this.employee;
	}

	public void setEmployee(EmployeeNew employee) {
		this.employee = employee;
	}


	public GoalSection getGoalSection() {
		return this.goalSection;
	}

	public void setGoalSection(GoalSection goalSection) {
		this.goalSection = goalSection;
	}


	public Tagged getTagged() {
		return this.tagged;
	}

	public void setTagged(Tagged tagged) {
		this.tagged = tagged;
	}
	public String getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}

	public String getUpdatedBy() {
		return UpdatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		UpdatedBy = updatedBy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + taggedEmpEventId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof TaggedEmp) {
			TaggedEmp temp = (TaggedEmp) obj;
			if(this.taggedEmpEventId == temp.taggedEmpEventId) {
				return true;
			}
		}
		return false;
	}

}
