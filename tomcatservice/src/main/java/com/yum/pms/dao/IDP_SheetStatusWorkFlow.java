/**
 * 
 */
package com.yum.pms.dao;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author jayanta.biswas
 *
 */
@Entity
@Table(name = "idp_sheet_status_workflow")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@idpSheetStsWorkflowId")
public class IDP_SheetStatusWorkFlow implements Serializable {
	
	private static final long serialVersionUID = 8042455435859443622L;

	@Id
	@Column(name="idp_sheet_sts_workflow_id")
	private Integer idpSheetStsWorkflowId;
	
	@Column(name="idp_sheet_status_id")
	private Integer idpSheetStatusId;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="next_possible_idp_sheet_status_id")
	private IDP_SheetStatus nextPossibleIdpSheetStatus;
	
	@Column(name="WebServiceURL")
	private String webServiceURL;

	public Integer getIdpSheetStsWorkflowId() {
		return idpSheetStsWorkflowId;
	}

	public void setIdpSheetStsWorkflowId(Integer idpSheetStsWorkflowId) {
		this.idpSheetStsWorkflowId = idpSheetStsWorkflowId;
	}

	public Integer getIdpSheetStatusId() {
		return idpSheetStatusId;
	}

	public void setIdpSheetStatusId(Integer idpSheetStatusId) {
		this.idpSheetStatusId = idpSheetStatusId;
	}

	public IDP_SheetStatus getNextPossibleIdpSheetStatus() {
		return nextPossibleIdpSheetStatus;
	}

	public void setNextPossibleIdpSheetStatus(IDP_SheetStatus nextPossibleIdpSheetStatus) {
		this.nextPossibleIdpSheetStatus = nextPossibleIdpSheetStatus;
	}

	public String getWebServiceURL() {
		return webServiceURL;
	}

	public void setWebServiceURL(String webServiceURL) {
		this.webServiceURL = webServiceURL;
	}
	
	

}
