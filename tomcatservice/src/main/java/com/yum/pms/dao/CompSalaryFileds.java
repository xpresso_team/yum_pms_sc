package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "comp_salary_fileds")
public class CompSalaryFileds implements Serializable {

	private static final long serialVersionUID = 93011617649684308L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer salaryFieldId;
	
	@Column(name = "head_name")
	private String headName;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Column(name = "salary_sheet_display_name")
	private String salarySheetDisplayName;
	
	@ManyToOne
	@JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@Column(name = "display_order")
	private Integer displayOrder;
	
	@Column(name = "is_displayed")
	private Boolean isDisplayed;
	
	@Column(name = "is_mandatory")
	private Boolean isMandatory;
	
	@Column(name = "new_employee_salary_head_display_name")
	private String newEmployeeSalarySheetDisplayName;
	
	public CompSalaryFileds() {
	}

	public CompSalaryFileds(Integer salaryFieldId) {
		this.salaryFieldId = salaryFieldId;
	}

	public CompSalaryFileds(Integer salaryFieldId, String headName) {
		this.salaryFieldId = salaryFieldId;
		this.headName = headName;
	}

	public Integer getSalaryFieldId() {
		return salaryFieldId;
	}

	public void setSalaryFieldId(Integer salaryFieldId) {
		this.salaryFieldId = salaryFieldId;
	}

	public String getHeadName() {
		return headName;
	}

	public void setHeadName(String headName) {
		this.headName = headName;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getSalarySheetDisplayName() {
		return salarySheetDisplayName;
	}

	public void setSalarySheetDisplayName(String salarySheetDisplayName) {
		this.salarySheetDisplayName = salarySheetDisplayName;
	}

	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public Boolean getIsDisplayed() {
		return isDisplayed;
	}

	public void setIsDisplayed(Boolean isDisplayed) {
		this.isDisplayed = isDisplayed;
	}

	public Boolean getIsMandatory() {
		return isMandatory;
	}

	public void setIsMandatory(Boolean isMandatory) {
		this.isMandatory = isMandatory;
	}

	public String getNewEmployeeSalarySheetDisplayName() {
		return newEmployeeSalarySheetDisplayName;
	}

	public void setNewEmployeeSalarySheetDisplayName(String newEmployeeSalarySheetDisplayName) {
		this.newEmployeeSalarySheetDisplayName = newEmployeeSalarySheetDisplayName;
	}

	@Override
	public String toString() {
		return "CompSalaryFileds [salaryFieldId=" + salaryFieldId + ", headName=" + headName
				+ ", salarySheetDisplayName=" + salarySheetDisplayName + ", appraisalPeriodId=" 
				+ (appraisalPeriod != null ? appraisalPeriod.getAppraisalPeriodId() : 0)
				+ ", displayOrder=" + displayOrder + "]";
	}
}
