package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author dipak.swain
 * This is a model class for 'pa_emp_current_level_since' table
 */
@Entity
@Table(name = "pa_emp_current_level_since")
@JsonIgnoreType
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@employee")
public class PA_EmployeeCurrentLevelSince implements Serializable {
	
	private static final long serialVersionUID = 346833908488735700L;

	@Id
    @OneToOne
    @JoinColumn(name = "emp_id")
	private EmployeeNew employee;
	
	@Column(name = "in_current_level_since")
	private Date inCurrentLevelSince;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	@Column(name = "Current_or_Future_grade")
	private Integer currentOrFutureGrade;
	
	public PA_EmployeeCurrentLevelSince() {
	}

	public PA_EmployeeCurrentLevelSince(int empId) {
		this.employee = new EmployeeNew(empId);
	}

	/**
	 * @return the employee
	 */
	public EmployeeNew getEmployee() {
		return employee;
	}

	/**
	 * @param employee the employee to set
	 */
	public void setEmployee(EmployeeNew employee) {
		this.employee = employee;
	}

	/**
	 * @return the inCurrentLevelSince
	 */
	public Date getInCurrentLevelSince() {
		return inCurrentLevelSince;
	}

	/**
	 * @param inCurrentLevelSince the inCurrentLevelSince to set
	 */
	public void setInCurrentLevelSince(Date inCurrentLevelSince) {
		this.inCurrentLevelSince = inCurrentLevelSince;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the updatedBy
	 */
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the updatedDate
	 */
	public Date getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * @param updatedDate the updatedDate to set
	 */
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	/**
	 * @return the currentOrFutureGrade
	 */
	public Integer getCurrentOrFutureGrade() {
		return currentOrFutureGrade;
	}

	/**
	 * @param currentOrFutureGrade the currentOrFutureGrade to set
	 */
	public void setCurrentOrFutureGrade(Integer currentOrFutureGrade) {
		this.currentOrFutureGrade = currentOrFutureGrade;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PA_EmployeeCurrentLevelSince [employee=" + employee + ", inCurrentLevelSince=" + inCurrentLevelSince
				+ ", currentOrFutureGrade=" + currentOrFutureGrade + "]";
	}
	
}
