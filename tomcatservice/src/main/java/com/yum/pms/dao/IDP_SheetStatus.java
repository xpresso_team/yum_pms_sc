/**
 * 
 */
package com.yum.pms.dao;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreType;

/**
 * @author jayanta.biswas
 *
 */
@Entity
@Table(name = "idp_sheet_status")
@JsonIgnoreType
public class IDP_SheetStatus implements Serializable {

	private static final long serialVersionUID = 324041344272997203L;

	@Id
	@Column(name="idp_sheet_status_id")
	private Integer idpSheetStatusId;
	
	@Column(name="idp_sheet_status_name")
	private String idpSheetStatusName;
	
	@Column(name="active")
	private Boolean active = true;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "idpStatus")
	private Set<IDP_SheetMaster> idpSheet = new HashSet<>(0);

	public Integer getIdpSheetStatusId() {
		return idpSheetStatusId;
	}

	public void setIdpSheetStatusId(Integer idpSheetStatusId) {
		this.idpSheetStatusId = idpSheetStatusId;
	}

	public String getIdpSheetStatusName() {
		return idpSheetStatusName;
	}

	public void setIdpSheetStatusName(String idpSheetStatusName) {
		this.idpSheetStatusName = idpSheetStatusName;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Set<IDP_SheetMaster> getIdpSheet() {
		return idpSheet;
	}

	public void setIdpSheet(Set<IDP_SheetMaster> idpSheet) {
		this.idpSheet = idpSheet;
	}
	
	

}
