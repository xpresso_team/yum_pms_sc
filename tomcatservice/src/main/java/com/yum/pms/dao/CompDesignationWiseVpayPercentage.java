package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "comp_designation_wise_vpay_percentage")
public class CompDesignationWiseVpayPercentage implements Serializable {

	private static final long serialVersionUID = -3125739211111790090L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@ManyToOne
	@JoinColumn(name = "designation_id")
	private EmployeeDesignationMaster desgMaster;
	
	@Column(name = "vpay_value")
	private Double vPayValue;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	public CompDesignationWiseVpayPercentage() {
	}
	
	public CompDesignationWiseVpayPercentage(Integer id) {
		this.id = id;
	}
	
	public CompDesignationWiseVpayPercentage(Integer id, Integer apprId, Double vPayValue, Integer degsId) {
		this.id = id;
		this.appraisalPeriod = new AppraisalPeriod(apprId);
		this.vPayValue = vPayValue;
		this.desgMaster = new EmployeeDesignationMaster(degsId);
	}
	
	public CompDesignationWiseVpayPercentage(Integer apprId, Double vPayValue, Integer degsId) {
		this.appraisalPeriod = new AppraisalPeriod(apprId);
		this.vPayValue = vPayValue;
		this.desgMaster = new EmployeeDesignationMaster(degsId);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	public EmployeeDesignationMaster getDesgMaster() {
		return desgMaster;
	}

	public void setDesgMaster(EmployeeDesignationMaster desgMaster) {
		this.desgMaster = desgMaster;
	}

	public Double getvPayValue() {
		return vPayValue;
	}

	public void setvPayValue(Double vPayValue) {
		this.vPayValue = vPayValue;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public String toString() {
		return "CompDesignationWiseVpayPercentage [id=" + id 
				+ ", appraisal Period Id =" + (appraisalPeriod != null ? appraisalPeriod.getAppraisalPeriodId() : 0) 
				+ ", desgMaster=" + desgMaster + ", vPayValue=" + vPayValue + "]";
	}
}
