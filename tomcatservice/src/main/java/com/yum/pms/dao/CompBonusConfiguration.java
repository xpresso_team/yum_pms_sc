package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "bonusConfiguraton")
@JsonIgnoreProperties(value = { "createdBy", "createdDate", "updatedBy", "updatedDate" })
public class CompBonusConfiguration implements Serializable {

	private static final long serialVersionUID = -4346392899194218501L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private Integer bonusConfigId;
	
	@ManyToOne
	@JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@Column(name = "bonus_start_date")
	private Date bonusStartDate;
	
	@Column(name = "bonus_end_date")
	private Date bonusEndDate;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "updatedby")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updatedDate")
	private Date updatedDate;
	
	private transient String statusMsg;
	
	private transient Integer apprId;
	
	private transient String logonUser;

	public Integer getBonusConfigId() {
		return bonusConfigId;
	}

	public void setBonusConfigId(Integer bonusConfigId) {
		this.bonusConfigId = bonusConfigId;
	}

	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	public Date getBonusStartDate() {
		return bonusStartDate;
	}

	public void setBonusStartDate(Date bonusStartDate) {
		this.bonusStartDate = bonusStartDate;
	}

	public Date getBonusEndDate() {
		return bonusEndDate;
	}

	public void setBonusEndDate(Date bonusEndDate) {
		this.bonusEndDate = bonusEndDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	public Integer getApprId() {
		return apprId;
	}

	public void setApprId(Integer apprId) {
		this.apprId = apprId;
	}

	public String getLogonUser() {
		return logonUser;
	}

	public void setLogonUser(String logonUser) {
		this.logonUser = logonUser;
	}
}
