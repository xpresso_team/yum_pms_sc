package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author dipak.swain
 * This is a model class for 'pa_overall_rating_Admin' table
 */
@Entity
@Table(name = "pa_overall_rating_Admin")
@JsonIgnoreType
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@paOverallRatingAdminId")
public class PA_OverallRatingAdmin implements Serializable {
	
	private static final long serialVersionUID = 8257446333674268994L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pa_overall_rating_Admin_id")
	private Integer paOverallRatingAdminId;
	
	@ManyToOne
    @JoinColumn(name = "pa_sheet_id")
	private PA_SheetMaster paSheet;
	
	@ManyToOne
    @JoinColumn(name = "pa_rating_id")
	private PA_RatingMaster paRating;
	
	@ManyToOne
    @JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@ManyToOne
    @JoinColumn(name = "emp_id")
	private EmployeeNew emp;
	
	@ManyToOne
    @JoinColumn(name = "Admin_emp_id")
	private EmployeeNew adminEmp;
	
	@Column(name = "Admin_remark")
	private String adminRemark;
	
	@Column(name = "Admin_promo_nom")
	private Boolean adminPromoNom;
	
	@Column(name = "additional_increase_nom")
	private Boolean additionalIncreaseNom;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	@Column(name = "promotion_criteria_id")
	private Integer promotionCriteriaId;
	
	@Column(name = "admin_if_value")
	private Double adminIfValue;
	
	@Column(name = "is_eligable")
	private Boolean isEligable;
	
	@Column(name = "Admin_promo_nom_aug")
	private Boolean adminPromoNomAug = false;
	
	@Column(name = "dev_check_in")
	private Boolean devCheckIn;
	
	@Column(name = "dev_check_in_remarks")
	private String devCheckInRremarks;
	
	@ManyToOne
    @JoinColumn(name = "culture_rating_id")
	private PaCultureRatingMstr cultureRatingMstr;

	public Integer getPaOverallRatingAdminId() {
		return paOverallRatingAdminId;
	}

	public void setPaOverallRatingAdminId(Integer paOverallRatingAdminId) {
		this.paOverallRatingAdminId = paOverallRatingAdminId;
	}

	public PA_SheetMaster getPaSheet() {
		return paSheet;
	}

	public void setPaSheet(PA_SheetMaster paSheet) {
		this.paSheet = paSheet;
	}

	public PA_RatingMaster getPaRating() {
		return paRating;
	}

	public void setPaRating(PA_RatingMaster paRating) {
		this.paRating = paRating;
	}

	public AppraisalPeriod getAppraisalPeriod() {
		return appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	public EmployeeNew getEmp() {
		return emp;
	}

	public void setEmp(EmployeeNew emp) {
		this.emp = emp;
	}

	public EmployeeNew getAdminEmp() {
		return adminEmp;
	}

	public void setAdminEmp(EmployeeNew adminEmp) {
		this.adminEmp = adminEmp;
	}

	public String getAdminRemark() {
		return adminRemark;
	}

	public void setAdminRemark(String adminRemark) {
		this.adminRemark = adminRemark;
	}

	public Boolean getAdminPromoNom() {
		return adminPromoNom;
	}

	public void setAdminPromoNom(Boolean adminPromoNom) {
		this.adminPromoNom = adminPromoNom;
	}
	
	public Boolean getAdditionalIncreaseNom() {
		return additionalIncreaseNom;
	}
	
	public void setAdditionalIncreaseNom(Boolean additionalIncreaseNom) {
		this.additionalIncreaseNom = additionalIncreaseNom;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	public Integer getPromotionCriteriaId() {
		return promotionCriteriaId;
	}

	public void setPromotionCriteriaId(Integer promotionCriteriaId) {
		this.promotionCriteriaId = promotionCriteriaId;
	}

	public Double getAdminIfValue() {
		return adminIfValue;
	}

	public void setAdminIfValue(Double adminIfValue) {
		this.adminIfValue = adminIfValue;
	}

	/**
	 * @return the isEligable
	 */
	public Boolean getIsEligable() {
		return isEligable;
	}

	/**
	 * @param isEligable the isEligable to set
	 */
	public void setIsEligable(Boolean isEligable) {
		this.isEligable = isEligable;
	}

	public Boolean getAdminPromoNomAug() {
		return adminPromoNomAug;
	}

	public void setAdminPromoNomAug(Boolean adminPromoNomAug) {
		this.adminPromoNomAug = adminPromoNomAug;
	}

	/**
	 * @return the cultureRatingMstr
	 */
	public PaCultureRatingMstr getCultureRatingMstr() {
		return cultureRatingMstr;
	}

	/**
	 * @return the devCheckIn
	 */
	public Boolean getDevCheckIn() {
		return devCheckIn;
	}

	/**
	 * @param devCheckIn the devCheckIn to set
	 */
	public void setDevCheckIn(Boolean devCheckIn) {
		this.devCheckIn = devCheckIn;
	}

	/**
	 * @return the devCheckInRremarks
	 */
	public String getDevCheckInRremarks() {
		return devCheckInRremarks;
	}

	/**
	 * @param devCheckInRremarks the devCheckInRremarks to set
	 */
	public void setDevCheckInRremarks(String devCheckInRremarks) {
		this.devCheckInRremarks = devCheckInRremarks;
	}

	/**
	 * @param cultureRatingMstr the cultureRatingMstr to set
	 */
	public void setCultureRatingMstr(PaCultureRatingMstr cultureRatingMstr) {
		this.cultureRatingMstr = cultureRatingMstr;
	}

	@Override
	public String toString() {
		return "PA_OverallRatingAdmin [paOverallRatingAdminId=" + paOverallRatingAdminId + ", paSheet=" + paSheet
				+ ", paRating=" + paRating + ", appraisalPeriod=" + appraisalPeriod + ", emp=" + emp + ", adminEmp="
				+ adminEmp + ", adminRemark=" + adminRemark + ", adminPromoNom=" + adminPromoNom + "]";
	}
	
}
