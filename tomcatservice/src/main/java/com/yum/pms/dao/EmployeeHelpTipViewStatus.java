package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.yum.pms.utils.YumPmsConstants;

/**
 * @author Ashis.Nayak
 * Table representation of model class
 */
@Entity
@Table(name = "employee_help_tip_view_status")
public class EmployeeHelpTipViewStatus implements Serializable {
	
	private static final long serialVersionUID = -2660004073096601699L;

	@Id
	@Column(name = "employee_help_tip_view_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer employeeHelpTipViewId;
	
	@Column(name = "emp_id")
	private Integer empid;
	
	@Column(name = "appr_id")
	private Integer apprId;

	@Column(name= "is_viewed")
	private Integer isViewed;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
	
	@Column(name = "UpdatedDate")
	private Date updatedDate;	
	public EmployeeHelpTipViewStatus() {
	}

	public EmployeeHelpTipViewStatus(Integer empid) {
		this.empid = empid;
	}

	public Integer getEmpid() {
		return empid;
	}

	public void setEmpId(Integer empid) {
		this.empid = empid;
	}

	public Integer getEmployeeHelpTipViewId() {
		return employeeHelpTipViewId;
	}

	public void setEmployeeHelpTipViewId(Integer employeeHelpTipViewId) {
		this.employeeHelpTipViewId = employeeHelpTipViewId;
	}

	public Integer getApprId() {
		return apprId;
	}

	public void setApprId(Integer apprId) {
		this.apprId = apprId;
	}

	public Integer getIsViewed() {
		return isViewed;
	}

	public void setIsViewed(Integer isViewed) {
		this.isViewed = isViewed;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
}
