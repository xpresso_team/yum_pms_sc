package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * @author dipak.swain
 * This is a model class for 'pa_rating_mstr' table
 */
@Entity
@Table(name = "pa_rating_mstr")
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@paRatingId")
public class PA_RatingMaster implements Serializable {
	
	private static final long serialVersionUID = 7025903274675446922L;

	@Id
	@Column(name = "pa_rating_id")
	private Integer paRatingId;
	
	@Column(name = "pa_rating_desc")
	private String paRatingDesc;
	
	@Column(name = "pa_rating_short_desc")
	private String paRatingShortDesc;
	
	@Column(name = "IsActive")
	private Boolean active = true;
	
	@Column(name = "CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate")
	private Date createdDate;
	
	@Column(name = "UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate")
	private Date updatedDate;
	
	public PA_RatingMaster() {
	}

	public PA_RatingMaster(Integer paRatingId) {
		this.paRatingId = paRatingId;
	}

	public Integer getPaRatingId() {
		return paRatingId;
	}

	public void setPaRatingId(Integer paRatingId) {
		this.paRatingId = paRatingId;
	}

	public String getPaRatingDesc() {
		return paRatingDesc;
	}

	public void setPaRatingDesc(String paRatingDesc) {
		this.paRatingDesc = paRatingDesc;
	}

	public String getPaRatingShortDesc() {
		return paRatingShortDesc;
	}

	public void setPaRatingShortDesc(String paRatingShortDesc) {
		this.paRatingShortDesc = paRatingShortDesc;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public String toString() {
		return "PA_RatingMaster [paRatingId=" + paRatingId + ", paRatingDesc=" + paRatingDesc + ", paRatingShortDesc="
				+ paRatingShortDesc + "]";
	}
	
}
