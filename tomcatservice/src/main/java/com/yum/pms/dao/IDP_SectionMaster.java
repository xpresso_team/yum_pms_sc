/**
 * 
 */
package com.yum.pms.dao;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.yum.idp.domain.IdpCategory;

/**
 * @author jayanta.biswas
 * @comment This is a pojo class for data sending
 * @version 1.0
 * @since   24-01-2018
 */
@Entity
@Table(name = "idp_section_mstr")
@JsonIgnoreType
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@idpSectionId")
public class IDP_SectionMaster implements Serializable {

	private static final long serialVersionUID = -5355853247000075113L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idp_section_id", unique = true, nullable = false)
	private Integer idpSectionId;

	@Column(name="idp_section_desc")
	private String idpSectionDesc;
	
	@Column(name="is_active")
	private Boolean isActive;
	
	@Column(name="CreatedBy")
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CreatedDate", updatable = false)
	private Date createdDate;
	
	@Column(name="UpdatedBy")
	private String updatedBy;
		
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UpdatedDate", insertable = false)
	private Date updatedDate;
	
	@Column(name="idp_section_hover_desc")
	private String idpSectionHoverDesc;
	
	@Column(name="grade_range_lower")
	private String gradeRangeLower = "0";
	
	@Column(name="grade_range_upper")
	private String gradeRangeUpper = "99";
	
	@Column(name="appr_id")
	private Integer apprId;
	
	@Column(name="idp_section_type_flag")
	private String idpSectionTypeFlag;
	
	public String getIdpSectionTypeFlag() {
		return idpSectionTypeFlag;
	}

	public void setIdpSectionTypeFlag(String idpSectionTypeFlag) {
		this.idpSectionTypeFlag = idpSectionTypeFlag;
	}

	public Integer getApprId() {
		return apprId;
	}

	public void setApprId(Integer apprId) {
		this.apprId = apprId;
	}

	public IDP_SectionMaster() {
		
	}
	
	public IDP_SectionMaster(Integer idpSectionId) {
		this.idpSectionId = idpSectionId;
	}

	public IDP_SectionMaster(IdpCategory category) {
		this.idpSectionId = category.getIdpSectionId();
		this.idpSectionDesc = category.getIdpSectionDesc();
		this.idpSectionHoverDesc = category.getIdpSectionHoverDesc();
		this.isActive = category.getIsActive();
		this.createdBy = category.getCreatedBy();
		this.updatedBy = category.getUpdatedBy();
		this.gradeRangeLower = category.getGradeRangeLower();
		this.gradeRangeUpper = category.getGradeRangeUpper();
		this.apprId = category.getApprId();
		this.idpSectionTypeFlag = category.getIdpSectionTypeFlag();
	}

	@Transient
	private boolean removable = true;

	public boolean isRemovable() {
		return removable;
	}

	public void setRemovable(boolean removable) {
		this.removable = removable;
	}

	public Integer getIdpSectionId() {
		return idpSectionId;
	}

	public void setIdpSectionId(Integer idpSectionId) {
		this.idpSectionId = idpSectionId;
	}

	public String getIdpSectionDesc() {
		return idpSectionDesc;
	}

	public void setIdpSectionDesc(String idpSectionDesc) {
		this.idpSectionDesc = idpSectionDesc;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getIdpSectionHoverDesc() {
		return idpSectionHoverDesc;
	}

	public void setIdpSectionHoverDesc(String idpSectionHoverDesc) {
		this.idpSectionHoverDesc = idpSectionHoverDesc;
	}
	
	public String getGradeRangeLower() {
		return gradeRangeLower;
	}
	
	public void setGradeRangeLower(String gradeRangeLower) {
		this.gradeRangeLower = gradeRangeLower;
	}
	
	public String getGradeRangeUpper() {
		return gradeRangeUpper;
	}
	
	public void setGradeRangeUpper(String gradeRangeUpper) {
		this.gradeRangeUpper = gradeRangeUpper;
	}

	
}
