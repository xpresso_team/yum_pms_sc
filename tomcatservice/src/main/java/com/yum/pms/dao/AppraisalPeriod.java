/******************************************************************** 
* Hibernate DAO Object Mapper for appraisal_period table 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.dao;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "appraisal_period")
@JsonIgnoreType
public class AppraisalPeriod implements java.io.Serializable {

	private static final long serialVersionUID = 4878999063950354385L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="appraisal_period_id", unique = true, nullable = false)
	private Integer appraisalPeriodId;
	
	@Column(name="period_start_yyyymm")
	private Integer periodStartYyyymm;
	
	@Column(name="period_end_yyyymm")
	private Integer periodEndYyyymm;
	
	@Column(name="period_desc")
	private String periodDesc;
	
	@Column(name="active")
	private Boolean active = true;
	
	@Column(name = "CreatedBy", length = 100)
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate", length = 23)
	private Date createdDate;
	
	@Column(name = "UpdatedBy", length = 100)
	private String updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate", length = 23)
	private Date updatedDate;
	
	@Column(name = "Is_Default")
	private Boolean isDefault;
	
	@Column(name="goal_label")
	private String goalLabel;
	
	@Column(name="is_configured")
	private Boolean isConfigured = true;
	
	@Column(name = "pa_eligibility_date")
	private Date paEligibilityDate;
	
	@JsonProperty("isNewYearConfigured")
	@Column(name = "is_new_year_configured")
	private Boolean newYearConfigured;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "appraisalPeriod")
	private Set<GoalSheet> goalSheet = new HashSet<>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "appraisalPeriod")
	private Set<Cascaded> cascadeds = new HashSet<>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "appraisalPeriod")
	private Set<GoalOrg> goalOrgs = new HashSet<>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "appraisalPeriod")
	private Set<Tagged> taggeds = new HashSet<>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "appraisalPeriod")
	private Set<GoalCeo> goalCeos = new HashSet<>(0); 

	@Transient
	private Boolean isRemovable = false;	
	
	@Transient
	private Date newYearConfigDate;

	public AppraisalPeriod() {
	}

	public AppraisalPeriod(Integer appraisalPeriodId) {
		this.appraisalPeriodId = appraisalPeriodId;
	}
	
	public AppraisalPeriod(Integer appraisalPeriodId, String periodDesc) {
		this.appraisalPeriodId = appraisalPeriodId;
		this.periodDesc = periodDesc;
	}

	public Integer getAppraisalPeriodId() {
		return this.appraisalPeriodId;
	}

	public void setAppraisalPeriodId(Integer appraisalPeriodId) {
		this.appraisalPeriodId = appraisalPeriodId;
	}

	public Integer getPeriodStartYyyymm() {
		return this.periodStartYyyymm;
	}

	public void setPeriodStartYyyymm(Integer periodStartYyyymm) {
		this.periodStartYyyymm = periodStartYyyymm;
	}

	public Integer getPeriodEndYyyymm() {
		return this.periodEndYyyymm;
	}

	public void setPeriodEndYyyymm(Integer periodEndYyyymm) {
		this.periodEndYyyymm = periodEndYyyymm;
	}

	public String getPeriodDesc() {
		return this.periodDesc;
	}

	public void setPeriodDesc(String periodDesc) {
		this.periodDesc = periodDesc;
	}

	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Boolean getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Boolean isDefault) {
		this.isDefault = isDefault;
	}

	public Set<GoalSheet> getGoalSheet() {
		return goalSheet;
	}

	public void setGoalSheet(Set<GoalSheet> goalSheet) {
		this.goalSheet = goalSheet;
	}

	public Set<Cascaded> getCascadeds() {
		return cascadeds;
	}

	public void setCascadeds(Set<Cascaded> cascadeds) {
		this.cascadeds = cascadeds;
	}

	public Set<GoalOrg> getGoalOrgs() {
		return goalOrgs;
	}

	public void setGoalOrgs(Set<GoalOrg> goalOrgs) {
		this.goalOrgs = goalOrgs;
	}

	public Set<Tagged> getTaggeds() {
		return taggeds;
	}

	public void setTaggeds(Set<Tagged> taggeds) {
		this.taggeds = taggeds;
	}

	public Set<GoalCeo> getGoalCeos() {
		return goalCeos;
	}

	public void setGoalCeos(Set<GoalCeo> goalCeos) {
		this.goalCeos = goalCeos;
	}

	
	public Boolean getIsRemovable() {
		return isRemovable;
	}

	public void setIsRemovable(Boolean isRemovable) {
		this.isRemovable = isRemovable;
	}

	public String getGoalLabel() {
		return goalLabel;
	}

	public void setGoalLabel(String goalLabel) {
		this.goalLabel = goalLabel;
	}

	/**
	 * @return the isConfigured
	 */
	public Boolean getIsConfigured() {
		return isConfigured;
	}

	/**
	 * @param isConfigured the isConfigured to set
	 */
	public void setIsConfigured(Boolean isConfigured) {
		this.isConfigured = isConfigured;
	}

	public Date getPaEligibilityDate() {
		return paEligibilityDate;
	}

	public void setPaEligibilityDate(Date paEligibilityDate) {
		this.paEligibilityDate = paEligibilityDate;
	}

	public Boolean getNewYearConfigured() {
		return newYearConfigured;
	}

	public void setNewYearConfigured(Boolean newYearConfigured) {
		this.newYearConfigured = newYearConfigured;
	}

	public Date getNewYearConfigDate() {
		return newYearConfigDate;
	}

	public void setNewYearConfigDate(Date newYearConfigDate) {
		this.newYearConfigDate = newYearConfigDate;
	}
	
	
}
