/******************************************************************** 
* Hibernate DAO Object Mapper for goal_sheet table 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.dao;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@Table(name = "goal_sheet")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@goalSheetId")
public class GoalSheet implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "goal_sheet_id", unique = true, nullable = false)
	private int goalSheetId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "appraisal_period_id")
	private AppraisalPeriod appraisalPeriod;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "emp_id", nullable = false)
	private EmployeeNew employee;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "goal_sheet_status_id")
	private GoalStatus goalStatus;
	
	@Column(name = "active")
	private Boolean active = true;
	
	@Column(name = "CreatedBy", length = 1000)
	private String createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreatedDate", length = 23)
	private Date createdDate;
	
	@Column(name = "UpdatedBy", length = 100)
	private String updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UpdatedDate", length = 23)
	private Date updatedDate;
	
	//@Transient
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "goalSheet")
	private Set<Goal> goals = new HashSet<>(0);

	public GoalSheet() {
	}
	
	public GoalSheet(int goalSheetId) {
		this.goalSheetId = goalSheetId;
	}
	
	public int getGoalSheetId() {
		return this.goalSheetId;
	}

	public void setGoalSheetId(int goalSheetId) {
		this.goalSheetId = goalSheetId;
	}

	
	public AppraisalPeriod getAppraisalPeriod() {
		return this.appraisalPeriod;
	}

	public void setAppraisalPeriod(AppraisalPeriod appraisalPeriod) {
		this.appraisalPeriod = appraisalPeriod;
	}

	
	public EmployeeNew getEmployee() {
		return this.employee;
	}

	public void setEmployee(EmployeeNew employee) {
		this.employee = employee;
	}

	
	public GoalStatus getGoalStatus() {
		return this.goalStatus;
	}

	public void setGoalStatus(GoalStatus goalStatus) {
		this.goalStatus = goalStatus;
	}

	
	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	
	public Set<Goal> getGoals() {
		return this.goals;
	}

	public void setGoals(Set<Goal> goals) {
		this.goals = goals;
	}

}
