/******************************************************************** 
 * Spring Security Configuration 
 * 
 * http://www.abzooba.com
 * ------------------- 
 * Created By: sudipta.chandra
 * Last Updated By: sudipta.chandra
 * Last Updated On: 23-Sep-2017 (11:34:34 AM) 
 *
 * The software and related user documentation are protected under 
 * copyright laws and remain the sole property of Abzooba. 
 * 
 * Technical support is available via the Abzooba website at 
 * http://www.abzooba.com
 **************************************************************************/

package com.yum.pms.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.yum.pms.utils.ResourceUtils;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private SimpleAuthenticationSuccessHandler successHandler;

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		if(ResourceUtils.getPropertyValue("ldap.enabled").equals("true")) {
			http
			.authorizeRequests()
			.antMatchers("/static/**").permitAll()
			.antMatchers("/forgotPassword").permitAll()
			.antMatchers("/setForgotPass").permitAll()
			.antMatchers("/sendMailForNewJoinee").permitAll()
			.antMatchers("/sendMailForExistingJoinee").permitAll()
			.antMatchers("/sendMailForLT").permitAll()
			.antMatchers("/sendMailForLTDetail").permitAll()
			.antMatchers("/sendMailToManagerForGoalSheetSubmission").permitAll()
			.antMatchers("/sendMailToDiscussedAndAgreedMonthlyQuarterly").permitAll()
			.antMatchers("/approveOrRejectionFromMailByLt").permitAll()
			.antMatchers("/sendMailToAllPerposeThoughBatch").permitAll()
			.antMatchers("/sendMailToNotDiscussedAndAgreedAndSubmitted").permitAll()
			//.antMatchers("/changePass").permitAll()
			.anyRequest().authenticated()
			.and()
			.formLogin().successHandler(successHandler)
			.loginPage("/login")
			.permitAll()
			.usernameParameter("username")
			.passwordParameter("password")
			.and()
			.httpBasic()
			.and()
			.logout().logoutSuccessUrl("/login?logout")
			.permitAll()
			.and().csrf().disable();
		} else {
			http.authorizeRequests().antMatchers("/").permitAll().and().csrf().disable();
		}
	}

	@Override
	protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {

		authenticationManagerBuilder.ldapAuthentication()
		.contextSource()
		.url(ResourceUtils.getPropertyValue("ldap.url"))
		.managerDn(ResourceUtils.getPropertyValue("ldap.managerDn")).managerPassword(ResourceUtils.getPropertyValue("ldap.managerPassword"))
		.and()
		//.userSearchBase("ou=users")
		.userSearchBase(ResourceUtils.getPropertyValue("ldap.userSearchBase"))

		//.userSearchFilter("(mail=*{0}*)")
		.userSearchFilter(ResourceUtils.getPropertyValue("ldap.userSearchFilter"))
		.groupSearchFilter(ResourceUtils.getPropertyValue("ldap.groupSearchFilter"))
		// .groupSearchBase("OU=IT,DC=int,DC=tgr,DC=net")
		//.groupSearchBase("OU=IT,DC=int,DC=tgr,DC=net")
		.groupSearchBase(ResourceUtils.getPropertyValue("ldap.groupSearchBase"))
		.userDetailsContextMapper(successHandler);
	}

}