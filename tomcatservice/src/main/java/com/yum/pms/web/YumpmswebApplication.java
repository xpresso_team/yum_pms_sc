/******************************************************************** 
* Spring Boot Entry Class 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: sudipta.chandra
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration
@ComponentScan({"com.yum.pms.controller","com.yum.pms.service","com.yum.idp.service","com.yum.pms.dao","com.yum.idp.dao","com.yum.pms.web", 
	"com.yum.pa", "com.yum.comp", "com.yum.pms.utils"})
//@EnableAutoConfiguration
public class YumpmswebApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		
		SpringApplication.run(YumpmswebApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		setRegisterErrorPageFilter(false); 
		return application.sources(applicationClass);
	}

	private static Class<YumpmswebApplication> applicationClass = YumpmswebApplication.class;
}

