/******************************************************************** 
 * Spring Security Auth Configuration 
 * 
 * http://www.abzooba.com
 * ------------------- 
 * Created By: keshav.kumar
 * Last Updated By: sudipta.chandra
 * Last Updated On: 23-Sep-2017 (11:34:34 AM) 
 *
 * The software and related user documentation are protected under 
 * copyright laws and remain the sole property of Abzooba. 
 * 
 * Technical support is available via the Abzooba website at 
 * http://www.abzooba.com
 **************************************************************************/

package com.yum.pms.web;

import java.io.IOException;
import java.util.Collection;

import javax.naming.directory.Attributes;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.userdetails.UserDetailsContextMapper;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class SimpleAuthenticationSuccessHandler implements AuthenticationSuccessHandler,UserDetailsContextMapper {

	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleAuthenticationSuccessHandler.class);

	private String email = StringUtils.EMPTY;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest arg0, HttpServletResponse arg1, Authentication authentication)
			throws IOException, ServletException {

		LOGGER.info("In success handler ... email id is {}", email);
		//if request is not from HttpServletRequest, you should do a type cast before
		HttpSession session = arg0.getSession(false);
		//save message in session
		session.setAttribute("userEmail",email);
		arg1.sendRedirect("/yumpmsweb/");
		/* Veracode suggested to invalidate the session, but doing these user are not able to login to the application: session.invalidate() */ 
	}

	/**
	 * 
	 * @param email
	 * @return flag whether use has reset initial pass or not
	 */
	@Override
	public UserDetails mapUserFromContext(DirContextOperations ctx, String username, Collection<? extends GrantedAuthority> authorities) {

		Attributes attributes = ctx.getAttributes();
		try {
			email = (String) attributes.get("mail").get();
			LOGGER.info("email in context mapper function is {}", email);
		} catch (Exception e) {
			LOGGER.error("Couldn't fetch email ids", e);
		} 
		return new CustomUserDetails(username, StringUtils.EMPTY, authorities, email);
	}

	@Override
	public void mapUserToContext(UserDetails arg0, DirContextAdapter arg1) {
		LOGGER.info("Blank implementation of {} method", "mapUserToContext()");
	}

}