package com.yum.pms.web;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class CustomUserDetails extends User {

	private static final long serialVersionUID = 1416132138315457558L;

	// extra instance variables
	private final String fullname;
	private final String  email;
	private final String title;

	public CustomUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities, String email) {

		super(username, password, true, true, true, true, authorities);
		this.fullname = StringUtils.EMPTY;
		this.email = email;
		this.title = StringUtils.EMPTY;        
	}

	public String getFullname() {
		return this.fullname;
	}

	public String getEmail() {
		return this.email;
	}

	public String getTitle() {
		return this.title;
	}

	@Override
	public int hashCode() {

		return new HashCodeBuilder(17, 37)
				.append(StringUtils.trimToEmpty(this.getUsername()))
				.append(StringUtils.trimToEmpty(this.getEmail()))
				.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {

		if(!(obj instanceof CustomUserDetails)) {
			return false;
		}

		if (getClass() != obj.getClass()) {
			return false;
		}

		if (this == obj) {
			return true;
		}

		CustomUserDetails other = (CustomUserDetails) obj;
		return new EqualsBuilder()
				.append(this.getUsername(), other.getUsername())
				.append(this.getEmail(), other.getEmail())
				.isEquals();
	}
	
}
