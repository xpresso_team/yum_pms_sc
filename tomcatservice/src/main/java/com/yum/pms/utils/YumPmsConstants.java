package com.yum.pms.utils;

/**
 * This class contains all the application constants. Developer can add their constants in this file,
 * but before adding make sure that this file does not contains that constant value before
 * @author dipak.swain
 */
public final class YumPmsConstants {
	
	private YumPmsConstants() {
		throw new UnsupportedOperationException("Can not be instantiated");
	}
	// Angular development url
	public static final String ANGULAR_DEV_URL = "*";//"http://localhost:4200";
	
	// Java Model class field(column) name
	public static final String RATING_GOAL_BY_SUP = "rating_goal_by_sup";
	public static final String RATING_GOAL_BY_LT = "rating_goal_by_LT";
	public static final String IS_ACTION_PENDING = "IsActionPending";
	
	public static final String IDP_SHEET_MASTER = "idpSheetMaster";
	public static final String IDP_PERFORMANCE_CATEGORY_MASTER = "idpPerformanceCategoryMaster";
	public static final String IDP_SHEET_ID = "idpSheetId";
	public static final String IS_ACTIVE = "isActive";
	public static final String SELECTED_SUGGESTED_GOAL_ID = "idp_suggested_goal_section_id";
	public static final String IDP_SECTION_DESC = "idpSectionDesc";
	public static final String ERROR_MSG_SAVE_UPDATE = "Failed to save or update ";
	public static final String IDP_BALANCE_YEAR_GOAL = "IDP_BalanceYearGoal";
	public static final String IDP_SUGGESTED_GOAL_DETAIL = "IDP_SelectedSuggestedGoalDtl";
	public static final String IDP_SECTION_DETAIL = "IDP_SectionDtl";
	
	// These are used to pass the parameter name in store procedure
	public static final String PARAM_1 = "param1";
	public static final String PARAM_2 = "param2";
	public static final String PARAM_3 = "param3";
	public static final String PARAM_4 = "param4";
	public static final String PARAM_5 = "param5";
	public static final String PARAM_6 = "param6";
	public static final String ID = "id";
	public static final String TABLE_NAME = "tableName";
	
	public static final String ADMIN = "ADMIN";
	public static final String SUPERVISOR = "SUPERVISOR";
	public static final String SUPERVISOR_LT = "SUPERVISOR-LT";
	public static final String SUPERVISOR_OF_SUPERVISOR = "SupervisorOfSupervisor";
	public static final String LT = "LT";
	public static final String HR = "HR";
	public static final String EMPLOYEE_ID = "EMPLOYEE_ID";
	public static final String SUPERVISOR_ID = "SUPERVISOR_ID";
	public static final String EMPLOYEE = "EMPLOYEE";
	public static final String EMP_ID = "empId";
	public static final String EMP_NAME = "empName";
	public static final String IDP_PERFORMANCE_RATING_REPORTING = "Idp_PerformanceRatingReporting";
	public static final String IDP_PERFORMANCE_RATING = "IDP_PerformanceRating";
	public static final String IDP_REMARKS = "IDP_Remarks";
	public static final String IDP_DEVELOPMENT_ACTION_PLAN = "IDP_DevelopmentActionPlan";
	public static final String IDP_PLAN_DETAILS = "IDP_PlanDetails";
	public static final String APPR_ID = "apprId";
	public static final String IDP_SECTION_TYPE_FLAG = "idpSectionTypeFlag";
	public static final String EFFECTIVE = "effective";
	public static final String IS_DEACTIVE = "isDeActive";
	public static final String PATH_OF_UPLOAD_FILES = "path_of_upload_files";
	public static final String FILE_TYPE = "file_type";
	
	// User roles used in PA module
	public static final String ROLE = "role";
	public static final String ROLE_SELF = "self";
	public static final String ROLE_SUPERVISOR = "supervisor";
	public static final String ROLE_SUPERVISOR_2 = "supervisor2";
	public static final String ROLE_LT = "lt";
	public static final String ROLE_ADMIN = "admin";
	
	// Time-line date format
	public static final String TIME_LINE_DATE_FORMAT = "MMM, yyyy";
	public static final String DATE_TIME_FROMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String DATE_FORMAT_YYYY_MM_DD = "yyyy-MM-dd";
	
	// Time-line label 'By: '
	public static final String TIME_LINE_LABEL_BY = "By: ";
	
	public static final String APPRAISAL = "appraisal";
	public static final String GOAL = "goal";
	public static final String GOAL_SHEET = "goalSheet";
	public static final String PA_SHEET = "paSheet";
	public static final String EMPLOYEE_MODEL = "employee";
	public static final String APPRAISAL_PERIOD = "appraisalPeriod";
	public static final String IS_MANAGER = "isManager";
	public static final String PA_RATING = "paRating";
	public static final String ORG_GOAL_IDS = "orgGoalIds";
	public static final String ACTIVE = "active";
	public static final String UPDATED_SUCESSFULLY = "Updated Sucessfully";
	public static final String ERROR_PA_SHEET_MASTER = "PA_SheetMaster is empty, paSheetId = {}";
	public static final String ERROR_PA_OVERALLRATINGADMIN = "PA_OverallRatingAdmin is empty, appraisalPeriod = {}";
	public static final String ERROR_GOAL_SHEET = "GoalSheet is empty, goalSheetId = {}";
	public static final String ERROR_FAIL_TO_FETCH = "Failed to fetch {} due to reason : {}";
	public static final String GOAL_SHEET_TABLE = "GoalSheet";
	public static final String ARGUMENT_CAN_NOT_NULL = "Parameter can't be null";
	public static final String IDENTIFIER_CAN_NOT_NULL = "ID can't be null";
	public static final String GIVEN_ID_NOT_AVAILABLE = "The provided ID is not available in %s";
	public static final String ERROR_FAIL_TO_SAVE = "Failed to save {}";
	public static final String ERROR_FAIL_TO_UPDATE = "Failed to update : ";
	public static final String ERROR_FAIL_TO_DELETE = "Failed to delete : ";
	public static final String ERROR_DUE_TO_GET_PA_RATING_ADMIN = "empId or appraisalPeriordId is empty {}";
	
	public static final String INFO_ABOUT_FUNCTION_PARAMITER = "Rating and IF value should not null or 0, while poromotion eligibility check function call";
	
	// These are pa module's model class
	public static final String PA_GOAL_RATING = "PA_GoalRating";
	public static final String PA_OVERALL_RATING_SUP = "PA_OverallRatingSup";
	public static final String PA_OVERALL_RATING_LT = "PA_OverallRatingLT";
	public static final String PA_SHEET_MASTER = "PA_SheetMaster";
	public static final String PA_CONFIG_IF = "PA_ConfigIF";
	public static final String PA_CONFIG_TF = "PA_ConfigTF";
	public static final String PA_CONFIG_RATING_BELL_CURVE_IF_RANGE = "PA_ConfigRatingBellCurveIFRange";
	public static final String PA_CONFIG_PROMOTION_CRITERIA = "PA_ConfigPromotionCriteria";
	public static final String GRADE_ID = "gradeId";
	public static final String GRADE_DESCRIPTION = "gradeDesc";
	public static final String APPRAISAL_PERIOD_DESCRIPTION = "periodDesc";
	public static final String PROMOTION_PERCENTAGE = "promotionPercentage";
	public static final String LOGON_USER = "logonUser";
	
	public static final String GOAL_STATUS_ID = "goalStatusId";
	public static final String GOAL_STATUS_NAME = "goalStatusName";
	public static final String PMS_ROLE_NAME = "pmsRoleName";
	public static final String PERIOD_DESC = "periodDesc";
	public static final String GOAL_ORG = "goalOrg";
	public static final String CEO_GOAL_ID = "ceoGoalId";
	public static final String NOTIFICATION_ID = "notificationId";
	public static final String ORG_GOAL_ID = "orgGoalId";
	public static final String APPRAISAL_PERIOD_ID = "appraisalPeriodId";
	public static final String CURRENT_APPR_ID = "currentApprId";
	public static final String NEXT_APPR_ID = "nextApprId";
	public static final String IS_DEFAULT = "isDefault";
	public static final String PMS_ROLE_ID = "pmsRoleId";
	public static final String GROWTH_IN_UPPER_CASE = "GROWTH";
	public static final String GROWTH_IN_LOWER_CASE = "growth";
	public static final String LOGIN_ID = "loginId";
	public static final String STATUS_MSG = "statusMsg";
	public static final String GRADE_LOOKUP_DATA = "lookupData";
	public static final String GRADE_WISE_PAY_RANGE_LIST = "gradeWisePayRangeList";
	public static final String GRADE_MASTER = "gradeMaster";
	public static final String DISPLAY_ORDER = "displayOrder";
	public static final String SQL_PARAM_PASS = "password";
	public static final String EMAIL = "email";
	public static final String FUNCTIONAL_LTS_ID = "functionalLTsID";
	public static final String EMP = "emp";
	public static final String PMS = "pms";
	public static final String PMS_ROLE = "pmsRole";
	public static final String IDP_SECTION_IDS = "sectionIds";
	public static final String IDP_SUB_SECTION_IDS = "subSectionIds";
	public static final String IDP_SUB_SUB_SECTION_IDS = "subSubSectionIds";
	public static final String IDP_SUB_SUB_SUB_SECTION_IDS = "subSubSubSectionIds";
	public static final String PARAM_SELECTED_SUGGESTED_GOAL_IDS = "suggestedGoalIds";
	public static final String PARAM_SELECTED_SUGGESTED_DEV_PLAN_IDS = "suggestedDevPlanIds";
	public static final String IDP_SECTION_LIST = "sectionList";
	public static final String IDP_SUB_SECTION_LIST = "subSectionList";
	public static final String IDP_SUB_SUB_SECTION_LIST = "subSubSectionList";
	public static final String IDP_SUB_SUB_SUB_SECTION_LIST = "subSubSubSectionList";
	public static final String IDP_SUGGESTED_GOAL_LIST = "suggestedGoalList";
	public static final String IDP_SUGGESTED_DEV_PLAN = "developmentPlanList";
	public static final String GOAL_SHEET_ID = "goalSheetId";
	public static final String GOAL_ID = "goalId";
	public static final String GOAL_SECTION_ID = "goalSectionId";
	public static final String REMARKS = "REMARKS";
	public static final String IDP_STATUS = "idpStatus";
	public static final String IDP_SHEET_STATUS_ID = "idpSheetStatusId";
	public static final String IDP_PERFORMANCE_RATING_LTS_WORKFLOW_STATUS_ID = "idpPerformanceRatingLTSWorkflowStatusId";
	public static final String IDP_PLAN_MASTER = "idpPlanMaster";
	public static final String PLAN = "plan";
	public static final String ACTION_PENDING = "actionPending";
	public static final String IDP_PERFORMANCE_CATEGORY_DESC = "idpPerformanceCategoryDesc";
	public static final String APPRECIATE = "appreciate";
	public static final String EFFECTIVENESS = "effectiveness";
	public static final String CONTROLE = "controll_";
	public static final String EMP_FUNCTION = "empFunction";
	public static final String SUB_FUNCTION = "subFunction";
	public static final String CATEGORY = "category";
	public static final String MANAGER_ID = "managerId";
	public static final String IDP_SHEET_ID_PARAM = "idp_sheet_id";
	public static final String IDP_STATUS_PARAM = "idp_status";
	public static final String IDP_RATING_STATUS_ID = "idp_ratingStatusId";
	public static final String RESET = "reset";
	public static final String FAILURE = "failure";
	public static final String CHANGE_PASS = "changePassword";
	public static final String PASS_CHANGED = "passwordChanged";
	public static final String TRUE = "true";
	public static final String MAIL_PROCESSED_FOR = "Mail Processed For {}";
	public static final String MAIL_SEND_SUCCESSFULLY = "Mail Successfully send";
	public static final String CHECK_BOX_PATH = "reports/images/checked.png";
	public static final String UNCHECK_BOX_PATH = "reports/images/unchecked.png";
	public static final String UNDERSCORE_12 = "____________";
	public static final String INDIVIDUAL = "Individual";
	public static final String DATE = "Date";
	public static final String COACH = "Coach";
	public static final String DROP_DOWN_APPRAISAL_PERIOD = "appraisal_period";
	public static final String DROP_DOWN_GOAL_STATUS = "goal_status";
	public static final String DROP_DOWN_USER_ROLE = "user_role";
	public static final String TAG_ID = "tagId";
	public static final String TAGGED = "tagged";
	public static final String GOAL_SECTION = "goalSection";
	public static final String CASCADE_ID = "cascadeId";
	public static final String CASECADED = "casecaded";
	public static final String IS_ADMIN_DOC = "isAdminDoc";
	public static final String PARAM_GOAL_SHEET_ID = "goal_sheet_id";
	public static final String DEAR = "Dear ";
	public static final String END_TD_TAG_2 = "</td></tr>";
	
	// work-flow status Ids
	public static final Integer WORK_FLOW_INITIAL_STATUS_ID = 1002;
	public static final Integer OVER_ALL_RATING_INITIAL_STATUS_ID = 1001;
	public static final Integer GOAL_SHEET_DISCUSS_AND_AGREED_STATUS_ID = 1005;
	
	// PA Sheet status
	public static final Integer PA_WORK_FLOW_INITIAL_STATUS_ID = 1002;
	public static final Integer PA_OVER_ALL_RATING_INITIAL_STATUS_ID = 1001;
	public static final Integer PA_SHEET_DISCUSS_AND_AGREED_STATUS_ID = 1005;
	
	// PA status descriptions, can be used for Goal and IDP also
	public static final String WORK_FLOW_STATUS_DESC_SUBMITTED = "SUBMITTED";
	public static final String WORK_FLOW_STATUS_DESC_ROLLEDBACK = "ROLLEDBACK";
	public static final String WORK_FLOW_STATUS_DESC_APPROVED = "APPROVED";
	public static final String WORK_FLOW_STATUS_DESC_RECALLED = "RECALLED";
	public static final String EMAIL_FOR_GOAL_RATING_E = "exceptional";
	public static final String EMAIL_FOR_GOAL_RATING_O = "opportunity";
	public static final String EMAIL_FOR_GOAL_RATING_S = "successful";
	
	//PA module form parameters
	public static final String PA_SHEET_STATUS_ID = "paSheetStatusId";
	public static final String PA_SHEET_ID = "pa_sheet_id";
	public static final String PA_STATUS = "pa_status";
	public static final String CREATED_BY = "createdBy";
	public static final String DISCUSSED_AND_AGREED = "offDiscussedAndAgreed";
	public static final String APPROVAL_MAIL_TO_LT = "Aproval_Mail_To_LT";
	public static final String REJECTION_MAIL_TO_SUPERVISOR = "Rejection_Mail_To_Supervisor";
	public static final String PA_PROMOTION_CRITERIA_DESC = "Promotion criteria for L";
	public static final String TAG_REJECT = "TAGREJECT";
	public static final String TAG_ACCEPT = "TAGACCEPT";
	public static final String TAG = "TAG";
	public static final String CASCADE = "CASCADE";
	
	public static final String SUCCESS_FETCH_MSG = "Successful Fetched";
	public static final String SUCCESS_SAVE_MSG = "Successful Saved";
	public static final String SUCCESS_DELETE_MSG = "Successfully Deleted";
	public static final String ERROR_MSG_TRY_AGAIN = "Sorry Try Again";
	public static final String MSG = "msg";
	public static final String SAVED = "Saved";
	public static final String FAILED = "failed";
	public static final String SUCCESS = "success";
	
	// Email parameters : To whom the mail will be deliver
	public static final String NEW_JOINEE = "NEW_JOINEE";
	public static final String EXISTING_JOINEE = "EXISTING_JOINEE";
	public static final String GOAL_SHEET_SUBMIT = "GOAL_SHEET_SUBMIT";
	public static final String LT_SUMMARIZED = "LT_SUMMARIZED";
	public static final String LT_DETAIL = "LT_DETAIL";
	public static final String DISCUSSED_AND_AGREED_MONTHLY_QUARTERLY = "DISCUSSED_AND_AGREED_MONTHLY_QUARTERLY";
	public static final String COMP_LETTER_DOWNLOAD_PERMISSION = "permissions";
	
	public static final String UNKNOWN_ERROR = "Unknown Error";
	// Used in catch block to log the error
	public static final String ERROR_STACK_TRACE = "Stacktrace : {}";
	public static final String ERROR_EMAIL_CAN_NOT_SEND = "Mail can't send, due to tecnical error";
	public static final String LOG_USER_ID = "userid is = {}";
	
	// Used in IDP and PA module reportee screen
	public static final String ERROR_ALIGNING_SUP_2 = "You needs to check the aligning supervisor's supervisor flag before submitting";
	public static final String ERROR_PROMOTION_CRITERIAS_BLANK = "Promotion Criteria can not be empty";
	
	// Procedure name with parameter placeholder
	public static final String SP_IDP_EMAIL_TO_LT_ON_EXCEPTIONAL_RATING = "exec sp_EmailNotification_IDP_ToLT_OnExceptionalRatingBySup :%s";
	public static final String SP_IDP_EMAIL_TO_SUP_ON_RATING_REJECTION = "exec sp_EmailNotification_IDP_ToSup_OnExceptionalRatingRejectedByLT :%s";
	public static final String SP_IDP_EMAIL_TO_SUP_2 = "exec sp_EmailNotification_IDP_ToSup2_OnLTSubmission :%s";
	public static final String SP_PA_EMAIL_TO_SUP_2 = "exec sp_EmailNotification_PA_ToSup2_OnLTSubmission :%s";
	public static final String SP_IDP_EMAIL_STATUS_CHANGE = "exec sp_EmailNotification_ByIDP_java :%s, :%s";
	public static final String SP_IDP_VERSION_HISTORY = "exec sp_VersionHistory_idp :%s";
	public static final String SP_GOAL_VERSION_HISTORY = "exec sp_VersionHistory_GoalSheet :%s";
	public static final String SP_GOAL_EMAIL_STATUS_CHANGE = "exec sp_EmailNotification_ByGoalSheet_java :%s, :%s";
	
	public static final String SP_EMAIL_NOTIFICATION_TAG_CASCADE = "exec sp_EmailNotification_TagCascade_java :param1, :param2, :param3, :param4";
	
	public static final String FUN_PA_PROMOTION_ELIGIBILITY = "{ ? = call fn_PA_PromotionEligibilityCheck(?,?,?,?) }";
	public static final String FUN_PA_PROMOTION_CRITERIA_ID = "{ ? = call fn_PA_PromotionEligibilityCriteriaCheck(?,?,?,?) }";
	
	// HTTP Headers 
	public static final String HTTP_HEADER_CONTENT_TYPE = "Content-Type";
	public static final String HTTP_HEADER_CONTENT_DISPOSITION = "Content-Disposition";
	public static final String HTTP_HEADER_VALUE_FORCE_DOWNLOAD = "application/force-download";
	public static final String HTTP_HEADER_ATTACHMENT = "attachment; filename=";
	
	public static final String HTML_CONTENT_TYPE = "text/html";
	
	// These fields are used in PA administration Configuration page 
	public static final String PA_IF_TF_SECTION = "IF_TF";
	public static final String PA_BELL_CURVE_SECTION = "BELL_CURVE";
	
	// PDF Report Parameters
	public static final String SUPERVISOR_NAME = "supervisorName";
	public static final String FUNCTION_NAME = "functionName";
	public static final String SUB_FUNCTION_NAME = "subFunctionName";
	public static final String CATEGORY_NAME = "categoryName";
	public static final String GRADE_DESC = "gradeDesc";
	public static final String FUNCTION_LT_NAME= "functionLTName";
	public static final String PA_SHEET_STATUS = "paSheetStatus";
	public static final String DESIGNATION = "designation";
	public static final String YEAR = "year";
	public static final String REPORT_LOGO_PATH= "reportLogoPath";
	public static final String PA_SELFGOAL_DATASET = "paSelfGoalDataset";
	
	public static final String GOAL_SHEET_STATUS = "goalSheetStatus";
	public static final String GOAL_SHEET_DATASET = "goalSheetDataset";
	
	public static final String PLUS = "+";
	public static final String HYPHEN = "-";
	public static final String SEMICOLON = ";";
	public static final String COMMA = ",";
	public static final String DOT = ".";
	public static final String UPPER_CASE_ALPHABET_L = "L";
	public static final String UPPER_CASE_ALPHABET_A = "A";
	public static final String UPPER_CASE_ALPHABET_R = "R";
	public static final String SPACE = " ";
	public static final String UNDERSCORE = "_";
	public static final String IDP_SHEET_PDF_NAME = "_IDP_Sheet_";
	public static final String PA_SHEET_PDF_NAME = "_PA_Sheet_";
	public static final String GOAL_SHEET_PDF_NAME = "_Goal_Sheet_";
	public static final String PDF_FILE_EXTENSION = ".pdf";
	public static final String GOAL_SHEET_PDF_FILE = "goal";
	public static final String IDP_SHEET_PDF_FILE = "idp";
	public static final String PA_SHEET_PDF_FILE = "pa";
	public static final String COMP_LETTER_FILE = "comp";
	public static final String COMP_LETTER_PDF_NAME = "_Compensation_Letter_";
	
	public static final String PA_ZIP_FILE = "zip_file";
	public static final String PA_COMP_ZIP_NAME = "_COMP_ZIP_FOLDER";
	
	// Some HTML elements used in PDF report
	public static final String HTML_BOLD_START_TAG = "<b>";
	public static final String HTML_BOLD_END_TAG = "</b>";
	public static final String HTML_BREAK_TAG = "<br>";
	
	public static final String HTML_DIV_START_TAG = "<div>";
	public static final String HTML_DIV_END_TAG = "</div>";
	public static final String FUNCTION_LT_SIGNATURE = "[[Function_LT_Signature]]";
	
	public static final String PARAM_EMP_ID = "empid";
	public static final String PARAM_APPRAISAL_ID = "appraisalid";
	public static final String APPR_PERIOD_ID = "apprPeriodId";
	public static final String PARAM_APPR_ID = "appid";
	public static final String PARAM_SELECTED_SUGGESTED_GOAL_ID = "selectedSuggestedGoalId";
	public static final String PARAM_RATING = "rating";
	public static final String PARAM_FILEDESC = "fileDesc";
	public static final String PARAM_UPLOADFILE = "uploadfile";
	public static final String PARAM_IS_ADMIN = "isadmin";
	public static final String PARAM_FILEID = "fileid";
	public static final String PARAM_TAGID = "tagid";
	public static final String PARAM_REPORTEEID = "reporteeid";
	public static final String PARAM_CASCADEID = "cascadeid";
	public static final String PARAM_GOALSHEETID = "goalsheetid";
	public static final String PARAM_EMP_GRADE = "empGrade";
	public static final String PARAM_USER_ID = "userid";
	public static final String PARAM_NON_DR = "nondr";
	
	public static final String ORG_GOAL = "ORG_GOAL";
	public static final String CEO_GOAL = "CEO_GOAL";
	public static final String APPRAISAL_IN_UPPER_CASE = "APPRAISAL";
	public static final String PROMOTION_CRITERIA = "PROMOTION_CRITERIA";
	
	//Admin Module
	public static final String USER_NAME = "USER_NAME";
	public static final int QUADRANT_COUNT = 5;
	
	public static final String SP_YEAR_CONFIG_FOR_BRAND_GOAL = "EXEC sp_yearConfigurationForBrandGoal %s";
	public static final String STATUS = "status";
	public static final String EMP_DESIGNATION_UPPER_CASE = "EMP_DESIGNATION";
	public static final String EMP_FUNCTION_UPPER_CASE = "EMP_FUNCTION";
	public static final String EMP_SUB_FUNCTION_UPPER_CASE = "EMP_SUB_FUNCTION";
	public static final String EMP_CATEGORY_UPPER_CASE = "EMP_CATEGORY";
	public static final String EMP_GRADE_UPPER_CASE = "EMP_GRADE";
	public static final String CREATED_DATE = "createdDate";
	public static final String FUNCTIONAL_LT = "functionalLt";
	public static final String MANAGER = "manager";
	public static final String NOT_DISCUSSED_AND_AGREED_IDP = "NOTDISCUSSEDANDAGREEDANDSUBMITED";
	
	public static final String EMPLOYEE_TABLE = "employee_new";
	
	// Compensation Module-------
	public static final String COMP_LETTER_ACCEPT = "comp_letter_accept";
	public static final String ZIP_FILE_EXTENSION = ".zip";
	public static final String SIGNATURE_IMAGE = "signatureImage";
	public static final String EMPLOYEE_JSON = "employeeJson";
	public static final String USER_SIGNATURE = "YumUserSignature";
	public static final String GLOBAL_LETTER = "YumGlobalLetter";
	public static final String SP_COMP_DISPLAY_LETTER = "sp_comp_display_letter";
	public static final String SP_COMP_EMAIL_LETTER = "sp_EmailNotification_CompensationLetter";
	public static final String COLUMN_NAME = "columnName";
	public static final String COLUMN_VALUE = "columnValue";
	public static final String COLUMN_EMP_NAME = "employee_name";
	public static final String COLUMN_LETTER_BODY = "letter_body";
	public static final String COLUMN_ANNEXTURE_BODY = "annexture_body";
	public static final String HTML_HR_ELEMENT = "<hr/>";
	public static final String INVALID_SP_NAME = "Invalid Stored Procedure Name";
	public static final String INVALID_REQUEST_DATA = "Invalid Request Data, Can not save";
	public static final String SALARY_DURATION = "salaryDuration";
	public static final String CURRENT_YEAR = "current_year";
	public static final String NEXT_YEAR = "next_year";
	public static final String ACTION = "action";
	public static final String REVISED_SAL = "RevisedSalary";
	public static final String VIEW = "View";
	public static final String YES = "yes";
	public static final String SAVE ="save";
	public static final String SP_COMP_LETTER = "sp_comp_PA_letter_generation";
	public static final String SP_REVISE_SALARY = "exec sp_comp_RevisedSalaryCalculation_" ;
	public static final String SP_PROMOTION_ELIGIBILITY = "sp_PA_PromotionEligibilityCheck";
	public static final String SALARY_TYPE = "purpose";

	public static final String SP_BONUS_CALCULATION = "exec sp_comp_RevisedBonusCalculation_";
	public static final String SP_SPECIAL_SALARY_HANDEL = "sp_comp_special_salary_handle_%s_%s";
	public static final String SP_NEW_YEAR_CONFIGURATION = "sp_New_Year_Setup %s, %s";
	public static final String PURPOSE = "purpose";
	public static final String DATA_LIST = "dataList";
	
	public static final String SP_REVISE_SALARY_PARAM = " ?, ?, ?, ?";
	public static final char UNDER_SCORE = '_' ;
	public static final String SINGLE_CODE = "'";
	public static final char HASH = '#';
	public static final char DOLLER = '$';
	public static final String STR_DOLLAR = "$";
	public static final String STR_EXCLAMATION = "!";
	public static final String EDITABLE = "$editable_";
	public static final String DISPLAY = "$display_";
	public static final String TOP_PROPERTY = "$top_value_";
	
	public static final String SP_PARAM_CUR_YEAR = "curYear";
	public static final String SP_PARAM_NEXT_YEAR = "nextYear";	
	public static final String SP_REVISED_SALARY_PARAM_EMP_ID = "empId";
	public static final String SP_REVISED_SALARY_PARAM_ADDITIONAL_PERCENTAGE = "additionPercentage";
	public static final String SP_REVISED_SALARY_PARAM_DESIGNATION_ID = "newDesignationId";
	
	public static final String FLAG = "flag";
	public static final String MESSAGE = "status_message";
	
	public static final String SP_NAME = "spName";
	public static final String PARAMITER_MAIL = "byParamiterMail";  
	public static final String EXECUTE = "exec %s"; 
	
	public static final String QUERY_FOR_GET_VPAY_WITH_EMP_ID = "SELECT T1.emp_id, T1.amount FROM comp_yearwise_salary T1"
	+ " JOIN comp_salary_fileds T2 ON T1.salary_field_id = T2.ID AND T1.appraisal_period_id = T2.appraisal_period_id " 
	+ " WHERE T1.appraisal_period_id = :%s AND T1.emp_id IN (:%s)"
	+ " AND T1.IsActive = 1 AND LOWER(T2.head_name) = 'vpayamount'";

	public static final double DEFAULT_IF_VALUE = 100.00;
	
	public static final String INSERT_QUERY_FOR_PARAMITER_OF_REVISED_SALARY = "insert into comp_revised_salary_changed_value(emp_id, additional_correctional_percentage, promoted_designation) values(%s, %s, '%s')";
	public static final String DELETE_QUERY_FOR_PARAMITER_OF_REVISED_SALARY = "delete comp_revised_salary_changed_value";
	
	public static final String INSERT_QUERY_FOR_MOBILITY_ALLOWANCE = "INSERT INTO compmobilityallowance_change_value(emp_id, allowance) values(%s, %s)";
	public static final String DELETE_QUERY_FOR_MOBILITY_ALLOWANCE = "DELETE compmobilityallowance_change_value";
	
	public static final String SP_EMPLOYEE_DESIGNATION_TUNERE = "sp_compGetEmployeeTenureDesignatioin";
	public static final String VALUE_AREA_COACH = "Area Coach";
	public static final String VALUE_SR_AREA_COACH = "Senior Area Coach";
	public static final String OUT_PARAM_DAYS = "emp_tenure";
	public static final String OUT_PARAM_VPAY = "emp_vpay_amount";
	public static final String OUT_PARAM_BUDGET = "Alocated_Budget";
	public static final String OUT_PARAM_CTC = "emp_ctc";
	public static final String OUT_PARAM_DESIGNATION = "emp_designation";
	
	public static final String SERVER_TIME_OUT_QUERY = "SELECT Value FROM dbconfig WHERE Name = 'session_timeout'";
	
	public static final String COMP_LETTER_HEADER_IMAGE = "reports/images/letter-header.jpg";
	public static final String COMP_LETTER_FOOTER_IMAGE = "reports/images/letter-footer.jpg";
	
	public static final String SP_COMP_LETTER_STATUS = "sp_comp_letter_status";
	public static final String DOWNLOAD = "download";
	public static final String GLOBAL_LETTER_ID = "globalLetterId";
	public static final String INVALID_GLOBAL_LETTER_ID = "Invalid global letter id";
	public static final String GLOBAL_LETTER_JSON = "globalLetterJson";
	public static final String MB = "MB";
	public static final String KB = "KB";
	public static final String LETTER_DOWNLOAD_PERMISSION = "letterDownloadPermission";
	public static final String COMP_LETTER_ACCEPTANCE_EMAIL = "sp_EmailNotification_comp_letter_acceptance";
	public static final String CAPITAL_LETTER_Y = "Y";
	public static final String CAPITAL_LETTER_N = "N";

	
}
