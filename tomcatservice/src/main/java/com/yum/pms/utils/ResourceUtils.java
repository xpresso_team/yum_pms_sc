package com.yum.pms.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yum.pms.exception.CommonException;
import com.yum.pms.exception.ResourceNotFoundException;

/**
 * This is a utility class which will help us to read the application.properties file from class path and return each property value
 * This can be also used to 
 *  - read/write/delete the files
 *  - create and delete the temporary files/folders  
 * @author dipak.swain
 */
public class ResourceUtils {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ResourceUtils.class);
	
	private static final String APP_RESOURCE_FILE = "application.properties";
	private static Properties applicationProperties = getResourceAsProperties(APP_RESOURCE_FILE);
	public static final String HOME = System.getProperty("user.home");
	
	/**
	 * Load the specified file from class path and return as Properties
	 * @param classPath
	 * @return
	 */
	public static Properties getResourceAsProperties(String classPath) {
		
		if(StringUtils.isEmpty(classPath)) {
			throw new IllegalArgumentException(YumPmsConstants.ARGUMENT_CAN_NOT_NULL);
		}
		Properties properties = new Properties();
		try {
			properties.load(getResourceAsStream(classPath));
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
			throw new ResourceNotFoundException(e.getMessage());
		}
		return properties;
	}
	
	/**
	 * Load the specified file from class path and return as InputStream 
	 * @param classPath
	 * @return
	 */
	public static InputStream getResourceAsStream(String classPath) {
		
		if(StringUtils.isEmpty(classPath)) {
			throw new IllegalArgumentException(YumPmsConstants.ARGUMENT_CAN_NOT_NULL);
		}
		return Thread.currentThread().getContextClassLoader().getResourceAsStream(classPath);
	}
	
	/**
	 * Load application.properties from the class path if not loaded
	 */
	private static void load() {
		if(applicationProperties == null) { // Ideally this should not be null
			applicationProperties = getResourceAsProperties(APP_RESOURCE_FILE);
		}
	}
	
	/**
	 * Get all supported signature image types
	 * @return Set<String>
	 */
	public static Set<String> getEmpSignatureImageTypes() {
		return new HashSet<>(Arrays.asList(getPropertyValue("SIGNATURE_IMAGE_TYPE").split(YumPmsConstants.COMMA)));
	}
	
	/**
	 * Get maximum signature image size
	 * @return long value in KB
	 */
	public static long getMaxSignatureImageSIze() {
		return FileUtils.ONE_KB * NumberUtils.toInt(getPropertyValue("SIGNATURE_IMAGE_SIZE"));
	}
	
	/**
	 * Get maximum file upload image size
	 * @return long value in KB
	 */
	public static long getMaxFileSIze() {
		
		String fileSizeStr = getPropertyValue("spring.http.multipart.max-file-size");
		if(StringUtils.endsWithIgnoreCase(fileSizeStr, YumPmsConstants.MB)) {
			return FileUtils.ONE_MB * NumberUtils.toInt(StringUtils.substringBefore(fileSizeStr, YumPmsConstants.MB));
		} else if(StringUtils.endsWithIgnoreCase(fileSizeStr, YumPmsConstants.KB)) {
			return FileUtils.ONE_KB * NumberUtils.toInt(StringUtils.substringBefore(fileSizeStr, YumPmsConstants.KB));
		}
		return -1;
	}
	
	/**
	 * Get the configured signature image width
	 * @return width in integer
	 */
	public static int getSignatureImageWidth() {
		return NumberUtils.toInt(getPropertyValue("SIGNATURE_IMAGE_WIDTH"));
	}
	
	/**
	 * Get the configured signature image height
	 * @return height in integer
	 */
	public static int getSignatureImageHeight() {
		return NumberUtils.toInt(getPropertyValue("SIGNATURE_IMAGE_HEIGHT"));
	}
	
	/**
	 * Get the absolute path of the PDF logo
	 * @return
	 */
	public static String getReportLogo() {
		load();
		return applicationProperties.getProperty("path_of_PDF_logo");
	}
	
	/**
	 * @param key - property file's key
	 * @return - The values from Property file
	 */
	public static String getPropertyValue(String key) {
		load();
		return applicationProperties.getProperty(key);
	}
	
	/**
	 * Create a directory in user home directory
	 * @param folderName
	 * @return Absolute path of the created directory 
	 */
	public static String createTempFolder(String folderName) {
		try {
			Path path = Paths.get(
					new StringBuilder(HOME)
					.append(File.separator)
					.append(StringUtils.defaultString(folderName, "YumPMSAttachment"))
					.toString());
			if(!path.toFile().exists()) {
				Path tempDir = Files.createDirectory(path);
				return tempDir.toFile().getAbsolutePath();
			}
			return path.toFile().getAbsolutePath();
		} catch (IOException e) {
			LOGGER.error(e.getMessage());
			throw new ResourceNotFoundException(e.getMessage());
		}
	}
	
	/**
	 * create the BufferedImage object from the given path
	 * @param imgPath - Path of the image file
	 * @return BufferedImage of the given path
	 */
	public static BufferedImage createImageFromPath(Path imgPath) {
		return createImageFromFile(imgPath.toFile());
	}
	
	/**
	 * Create the BufferedImage object from the given image file
	 * @param imgFile - File object of the image
	 * @return BufferedImage of the given file
	 */
	public static BufferedImage createImageFromFile(File imgFile) {
		
		try {
	        return ImageIO.read(imgFile);
	    } catch (IOException e) {
	    	LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
	        throw new CommonException(e);
	    }
	}
	
	/**
	 * Create the BufferedImage object from the given byte data
	 * @param imageData
	 * @return BufferedImage of the given byte data
	 */
	public static BufferedImage createImageFromBytes(byte[] imageData) {
	    
	    return createImageFromStream(new ByteArrayInputStream(imageData));
	}
	
	/**
	 * Create the BufferedImage object from the given InputStream object
	 * @param inputStream
	 * @return BufferedImage of the given byte data
	 */
	public static BufferedImage createImageFromStream(InputStream inputStream) {
		try {
	        return ImageIO.read(inputStream);
	    } catch (IOException e) {
	    	LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
	        throw new CommonException(e);
	    }
	}
	
	/**
	 * Create byte content from the BufferedImage object with the given file extension
	 * @param src
	 * @param fileExtension
	 * @return byte content of the image
	 */
	public static byte[] createBytesFromImage(BufferedImage src, String fileExtension) {
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			ImageIO.write(src, fileExtension, baos);
		} catch (IOException e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			throw new CommonException(e);
		}
		return baos.toByteArray();
	}
	
	/**
	 * crop the provided image according to width and height
	 * @param src - provided image file in form of BufferedImage
	 * @param width - required width in pixel
	 * @param height - required height in pixel
	 * @return the cropped image
	 */
	public static BufferedImage cropImage(BufferedImage src, int width, int height) {
		
		int originalWidth = src.getWidth();
		int originalHeight = src.getHeight();
		int x = 0;
		int y = 0;
		if(originalWidth > width) {
			x = (originalWidth - width) / 2;
		} else {
			width = originalWidth;
		}
		if(originalHeight > height) {
			y = (originalHeight - height) / 2;
		} else {
			height = originalHeight;
		}
		return src.getSubimage(x, y, width, height); 
	}
	
	/**
	 * Crop the image file according to width and height parameters.
	 * @param imgFile - provided image file
	 * @param width - required width in pixel
	 * @param height - required height in pixel
	 * @return new cropped content
	 */
	public static byte[] cropImageFromFile(File imgFile, int width, int height) {
		
		if(imgFile == null || !imgFile.isFile()) {
			return new byte[0];
		}
		BufferedImage originalImage = createImageFromFile(imgFile);
		BufferedImage croppedImage = cropImage(originalImage, width, height);
		String extension = FilenameUtils.getExtension(imgFile.getAbsolutePath());
		return createBytesFromImage(croppedImage, extension);
	}
	
	/**
	 * Crop the content image according to width and height parameters.
	 * @param content - image content
	 * @param imgExtension - image extension
	 * @param width - required width in pixel
	 * @param height - required height in pixel
	 * @return new cropped content
	 */
	public static byte[] cropImageFromBytes(byte[] content, String imgExtension, int width, int height) {
		
		BufferedImage originalImage = createImageFromBytes(content);
		BufferedImage croppedImage = cropImage(originalImage, width, height);
		return createBytesFromImage(croppedImage, imgExtension);
	}
	
	/**
	 * Create folder if not exist in user home folder and place the specified file with given content.
	 * @param folderName
	 * @param fileName
	 * @param content
	 * @return the created file path(absolute path)
	 */
	public static String createFileInFolder(String folderName, String fileName, byte[] content) {
		
		StringBuilder folder = new StringBuilder(HOME).append(File.separator).append(StringUtils.defaultString(folderName, "YumPMS"));
		Path folderPath = Paths.get(folder.toString());
		Path filePath = Paths.get(folder.append(File.separator).append(fileName).toString());
		try {
			if(!folderPath.toFile().exists()) {
				Files.createDirectories(folderPath);
			}
			Files.deleteIfExists(filePath);
			Files.write(filePath, content);
			return filePath.toFile().getAbsolutePath();
		} catch (IOException e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			throw new ResourceNotFoundException(e.getMessage());
		}		
	}
	
	public static String createUserSignature(String fileName, byte[] content) {
		
		String signaturePath = getUserSignaturePath();
		Path signatureFilePath = Paths.get(signaturePath + File.separator + fileName);
		createFolderIfNotExist(signaturePath);
		try {
			Files.deleteIfExists(signatureFilePath);
			Files.write(signatureFilePath, content);
			return File.separator + FilenameUtils.getName(signaturePath) + File.separator + fileName;
		} catch (IOException e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			throw new ResourceNotFoundException(e.getMessage());
		}
	}
	
	public static String createFile(String fileName, String folderPath, byte[] content) {
		
		Path filePath = Paths.get(folderPath + File.separator + fileName);
		createFolderIfNotExist(folderPath);
		try {
			Files.deleteIfExists(filePath);
			Files.write(filePath, content);
			return File.separator + FilenameUtils.getName(folderPath) + File.separator + fileName;
		} catch (IOException e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			throw new ResourceNotFoundException(e.getMessage());
		}
	}
	
	public static void createFolderIfNotExist(String strPath) {
		
		Path path = Paths.get(strPath);
		if(!path.toFile().exists()) {
			try {
				Files.createDirectories(path);
			} catch (IOException e) {
				LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
				throw new ResourceNotFoundException(e.getMessage());
			}
		}
	}
	
	public static String getUserSignaturePath() {
		return getYumRootPath() + File.separator + YumPmsConstants.USER_SIGNATURE;
	}
	
	public static String getGlobalLetterPath() {
		return getYumRootPath() + File.separator + YumPmsConstants.GLOBAL_LETTER;
	}
	
	public static String getYumRootPath() {
		return System.getProperty("catalina.home");
	}
	
	/**
	 * Create a file in user home directory
	 * @param fileName
	 * @param content
	 * @return Absolute path of the created file 
	 */
	public static String createTempFile(String fileName, byte[] content) {	
		Path path = Paths.get(new StringBuilder(HOME).append(File.separator).append(fileName).toString());
		try {
			File file = path.toFile();
			Files.write(path, content);
			file.deleteOnExit();
			return file.getAbsolutePath();
		} catch (IOException e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			throw new ResourceNotFoundException(e.getMessage());
		}
	}
	
	public static String createZip(String zipFileName, Map<String, byte[]> map) {
		
		if(!StringUtils.endsWith(zipFileName, YumPmsConstants.ZIP_FILE_EXTENSION)) {
			zipFileName = zipFileName + YumPmsConstants.ZIP_FILE_EXTENSION;
		}
		Path zipFilePath = Paths.get(getYumRootPath() + File.separator + zipFileName);
		try(ZipOutputStream zipOut = new ZipOutputStream(Files.newOutputStream(zipFilePath))) {
			for(Entry<String, byte[]> entry : map.entrySet()) {
				ZipEntry zipEntry = new ZipEntry(entry.getKey());
		        zipOut.putNextEntry(zipEntry);
		        zipOut.write(entry.getValue());
			}
			return zipFilePath.toFile().getAbsolutePath();
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			throw new CommonException(e);
		}
	}
	
	public static byte[] getZipAsBytes(String zipFileName, Map<String, byte[]> map) {
		
		String zipFilePath = createZip(zipFileName, map);
		byte[] bytes = new byte[0];
		try {
			bytes = Files.readAllBytes(Paths.get(zipFilePath));
		} catch (IOException e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			throw new CommonException(e);
		}
		return bytes;
	}
	
	/**
	 * Delete the file from provided absolute path
	 * @param file
	 */
	public static void delete(String file) {
		delete(Paths.get(file));
	}
	
	/**
	 * Delete the file from provided absolute path(java.nio.file.Path)
	 * @param path
	 */
	public static void delete(Path path) {
		try {
			Files.deleteIfExists(path);
		} catch (IOException e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			throw new ResourceNotFoundException(e.getMessage());
		}
	}
	
	/**
	 * Delete the file/directory from provided file(java.io.File)
	 * @param file
	 */
	public static void delete(File file) {
		if(file != null) {
			delete(file.toPath());
		}
	}
	
	public static URL getResourceUrlFromClassPath(String classPathResource) {
		
		if(StringUtils.isEmpty(classPathResource)) {
			throw new IllegalArgumentException(YumPmsConstants.ARGUMENT_CAN_NOT_NULL);
		}
		return Thread.currentThread().getContextClassLoader().getResource(classPathResource);
	}
	
	//Test cases
	public static void main(String[] args) {
		String file = createTempFile(
				new StringBuilder("DpkTmp").append(UUID.randomUUID().toString()).append(".txt").toString(), 
				"This is a temporary file".getBytes(StandardCharsets.UTF_8)
				);
		LOGGER.info("Successfully create the file : {}", file);
		
		// Image crop test case
		String origionalFile = "C:\\Users\\dipak.swain\\Downloads\\image.jpg";
		byte[] content = cropImageFromFile(new File(origionalFile), 500, 300);
		createFileInFolder(null, "croppedImage.jpg", content);
		
		// zip file creation test
		try {
			createZipTestCase();
		} catch (IOException e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
		}
	}
	
	private static void createZipTestCase() throws IOException {
		
		Path sourcePath = Paths.get("C:\\Users\\dipak.swain\\Desktop\\pdf2.pdf");
		Path sourcePath2 = Paths.get("C:\\Users\\dipak.swain\\Desktop\\Screen1.PNG");
		Map<String, byte[]> map = new HashMap<>();
		map.put(sourcePath.toFile().getName(), Files.readAllBytes(sourcePath));
		map.put(sourcePath2.toFile().getName(), Files.readAllBytes(sourcePath2));
		String absolutePath = createZip("compressed.zip", map);
		LOGGER.info("Successfully create the file : {}", absolutePath);
	}
	

}
