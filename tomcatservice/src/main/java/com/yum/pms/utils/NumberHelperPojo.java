package com.yum.pms.utils;

import java.io.Serializable;

public class NumberHelperPojo implements Serializable {
	
	private static final long serialVersionUID = -7409087783379418248L;
	
	private Integer value;
	
	public NumberHelperPojo() {
	}
	
	public NumberHelperPojo(Integer value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}
	
	public void setValue(Integer value) {
		this.value = value;
	}
}
