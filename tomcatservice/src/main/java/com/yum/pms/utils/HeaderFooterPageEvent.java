package com.yum.pms.utils;

import java.io.IOException;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

public class HeaderFooterPageEvent extends PdfPageEventHelper {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(HeaderFooterPageEvent.class);
	
	public static final HeaderFooterPageEvent INSTANCE = new HeaderFooterPageEvent();
	private URL headerUrl;
	private URL footerUrl;
	
	private HeaderFooterPageEvent() {
		headerUrl = ResourceUtils.getResourceUrlFromClassPath(YumPmsConstants.COMP_LETTER_HEADER_IMAGE);
		footerUrl = ResourceUtils.getResourceUrlFromClassPath(YumPmsConstants.COMP_LETTER_FOOTER_IMAGE);
	}

	@Override
    public void onStartPage(PdfWriter writer, Document document) {
		
	    try {
	    	Image image = Image.getInstance(headerUrl);
	        image.setAlignment(Element.ALIGN_CENTER);
	        image.setAbsolutePosition(0, 750);
	        image.scalePercent(24f);
	        writer.getDirectContent().addImage(image, true);
	    } catch (IOException | DocumentException e) {
	    	LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
	    }
    }

	@Override
    public void onEndPage(PdfWriter writer, Document document) {
		
	    try {
	    	Image image = Image.getInstance(footerUrl);
	        image.setAlignment(Element.ALIGN_CENTER);
	        image.setAbsolutePosition(0, 10);
	        image.scalePercent(24f);
	        writer.getDirectContent().addImage(image, true);
	    } catch (IOException | DocumentException e) {
	    	LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
	    }
    }

}
