/******************************************************************** 
 * Database Connection Manager
 * 
 * http://www.abzooba.com
 * ------------------- 
 * Created By: jayanta.biswas
 * Last Updated By: sudipta.chandra
 * Last Updated On: 23-Sep-2017 (11:34:34 AM) 
 *
 * The software and related user documentation are protected under 
 * copyright laws and remain the sole property of Abzooba. 
 * 
 * Technical support is available via the Abzooba website at 
 * http://www.abzooba.com
 **************************************************************************/

package com.yum.pms.utils;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


//@PropertySource(value = { "classpath:application.properties" })
public class DbConnection {

	private static final Logger LOGGER = LoggerFactory.getLogger(DbConnection.class);

	public Connection getConnection()  {

		String driver = ResourceUtils.getPropertyValue("jdbc.driverClassName");
		String url = ResourceUtils.getPropertyValue("jdbc.url");
		String username = ResourceUtils.getPropertyValue("jdbc.username");
		String password = ResourceUtils.getPropertyValue("jdbc.password");
		try {
			Class.forName(driver);

		} catch (ClassNotFoundException e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
		}
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url, username, password);
		} catch (SQLException e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
		}
		return conn;
	}

}
