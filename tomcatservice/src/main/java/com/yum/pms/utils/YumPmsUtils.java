/******************************************************************** 
* Util Methods 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.utils;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.yum.comp.domain.CompensationLetter;
import com.yum.idp.domain.BalanceOfGoal;
import com.yum.idp.domain.DevelopmentActionPlan;
import com.yum.idp.domain.HowLeadSubData;
import com.yum.idp.domain.IdpUIConfig;
import com.yum.idp.domain.UiControll;
import com.yum.pa.domain.MyGoal;
import com.yum.pa.domain.PASheetData;
import com.yum.pa.domain.Rating;
import com.yum.pa.domain.SelfSheetData;
import com.yum.pms.dao.Goal;
import com.yum.pms.dao.GoalCeo;
import com.yum.pms.dao.GoalSection;
import com.yum.pms.domain.EmployeeGoalMatrix;
import com.yum.pms.domain.EmployeeGoalSection;
import com.yum.pms.domain.EmployeeGoalSheet;
import com.yum.pms.domain.GeneralDomain;
import com.yum.pms.exception.CommonException;
import com.yum.pms.report.GoalSheetReportData;
import com.yum.pms.report.PaGoalSectionReport;
import com.yum.pms.report.PaReportData;
import com.yum.pms.report.ReportModel;
import com.yum.pms.report.ReportName;
import com.yum.pms.report.ReportUtils;

import be.quodlibet.boxable.BaseTable;
import be.quodlibet.boxable.Cell;
import be.quodlibet.boxable.HorizontalAlignment;
import be.quodlibet.boxable.Row;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class YumPmsUtils {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(YumPmsConstants.class);
	private static final ConcurrentMap<String, Object> objectMap = new ConcurrentHashMap<>();
	// https://www.owasp.org/index.php/OWASP_Validation_Regex_Repository
	private static final String EMAIL_REGEX = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
	public static final Pattern EMIAL_PATTERN = Pattern.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE);
	
	private YumPmsUtils() {
		throw new UnsupportedOperationException("Can not be instantiated");
	}
	
	public static boolean isValidEmail(String email) {
		
		if(StringUtils.isBlank(email)) {
			return false;
		}
		return EMIAL_PATTERN.matcher(email).matches();
	}
	
	public static boolean isInValidEmail(String email) {
		return !isValidEmail(email);
	}
	
	public static Boolean isExist(List<Goal> goalList, List<GoalCeo> ceoGoalList, int orgGoalId) {
		boolean has = false;
		for(Goal goal : goalList) {
			if(orgGoalId == goal.getOrgGoalId()) {
				has = true;
				break;
			}
		}	
		if(!has) {
			for(GoalCeo ceoGoal : ceoGoalList) {
				if(orgGoalId == ceoGoal.getGoalOrg().getOrgGoalId()) {
					has = true;
					break;
				}
			}	
		}
		return has;
	}
	
	public static int castToInt(Object obj) {
		try {
			if(obj != null && obj instanceof String) {
				return NumberUtils.toInt(obj.toString());
			}
			return (int)obj;
		} catch(Exception e) {
			return 0;
		}
	}
	
	public static boolean toBoolean(Object obj) {
		
		if(obj == null) {
			return false;
		}
		if(!(obj instanceof Boolean)) {
			return false;
		}
		try {
			return (boolean)obj;
		} catch(Exception e) {
			return false;
		}
	}
	
	public static double castToDouble(Object obj) {
		double value = 0.0;
		try {
			if(obj instanceof BigDecimal) {
				value = ((BigDecimal) obj).doubleValue();
	        } else if(obj instanceof Number) {
	        	value = ((Number)obj).doubleValue();
	        } 
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			value = 0.0;
		}
		return value;
	}
	
	public static BigDecimal doubleToBigDecimal(Double val) {
		
		try {
			return BigDecimal.valueOf(val);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return BigDecimal.ZERO;
		}
	}
	
	public static double multiply(Double val1, Double val2) {

		try {
			return val1 * val2;
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return 0.0;
		}
	}
	
	public static BigDecimal getRoundedValue(BigDecimal var1) {
		
		if(var1 == null) {
			var1 = BigDecimal.ZERO;
		}
		return var1.setScale(2, RoundingMode.HALF_UP);
	}
	
	public static BigDecimal multiply(BigDecimal var1, BigDecimal var2) {
		
		BigDecimal result = BigDecimal.ZERO;
		try {
			result = var1.multiply(var2);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
		}
		return result;
	}
	
	public static BigDecimal divide(BigDecimal var1, BigDecimal var2) {
		
		BigDecimal result = BigDecimal.ZERO;
		try {
			result = var1.divide(var2, 2, RoundingMode.HALF_UP);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
		}
		return result;
	}
	
	public static BigDecimal multiply(BigDecimal var1, BigDecimal var2, boolean roundingFlag) {
		
		if(roundingFlag) {
			return getRoundedValue(multiply(var1, var2));
		}
		return multiply(var1, var2);
	}
	
	public static BigDecimal divide(BigDecimal var1, BigDecimal var2, boolean roundingFlag) {
		
		if(roundingFlag) {
			return getRoundedValue(divide(var1, var2));
		}
		return divide(var1, var2);
	}
	
	public static BigDecimal calculatePercentage(BigDecimal value, double percentage) {
		return multiply(value, BigDecimal.valueOf(percentage)).divide(BigDecimal.valueOf(100));
	}
	
	public static BigDecimal calculatePercentage(BigDecimal value, double percentage, boolean roundingFlag) {
		if(roundingFlag) {
			return getRoundedValue(calculatePercentage(value, percentage));
		}
		return calculatePercentage(value, percentage);
	}

	public static boolean isNullOrEmpty(String text) {
		return text == null || text.isEmpty();
	}

	public static String toStringorNull(Object object) {
		if(object == null)
			return null;
		return object.toString();
	}
	
	public static String toString(Object object) {
		if(object == null)
			return StringUtils.EMPTY;
		return object.toString();
	}
	
	public static String intToStr(Integer num) {
		try {
			return num.toString();
		} catch(Exception e) {
			return StringUtils.EMPTY;
		}
	}
	
	public static double toDouble(Double number) {
		
		try {
			return number.doubleValue();
		} catch(Exception e) {
			return 0;
		}
	}
	
	public static NumberHelperPojo toInteger(String num) {
		
		NumberHelperPojo pojo = new NumberHelperPojo();
		try {
			pojo.setValue(Integer.parseInt(num));
		} catch(Exception e) {
			return pojo;
		}
		return pojo;
	}
	
	/**
	 * This method check the provided parameter is null or zero
	 * @param number: This accepts integer, float, double, long, short
	 * @return true/false
	 */
	public static boolean isNullOrZero(Number number) {
		return number == null || number.intValue() == 0;
	}
	
	/**
	 * This method check the provided parameter is null and not zero
	 * @param number: This accepts integer, float, double, long, short
	 * @return true/false
	 */
	public static boolean isNotNullAndNonZero(Number number) {
		return number != null && number.intValue() != 0;
	}
	
	/**
	 * This method check the provided parameter is null and greater zero
	 * @param number: This accepts integer, float, double, long, short
	 * @return true/false
	 */
	public static boolean isGreaterThanZero(Number number) {
		return number != null && number.intValue() > 0;
	}
	
	/**
	 * This method check the provided parameter is null and less than zero
	 * @param number: This accepts integer, float, double, long, short
	 * @return true/false
	 */
	public static boolean isLessThanZero(Number number) {
		return number != null && number.intValue() < 0;
	}
	
	/**
	 * This method check the provided parameter is null and less than or equal zero
	 * @param number: This accepts integer, float, double, long, short
	 * @return true/false
	 */
	public static boolean isLessThanOrEqual(Number number) {
		return number != null && number.intValue() <= 0;
	}
	
	public static String getTagStatus(Boolean tagStatus) {
		return BooleanUtils.toBoolean(tagStatus) ? YumPmsConstants.UPPER_CASE_ALPHABET_A : YumPmsConstants.UPPER_CASE_ALPHABET_R;
	}
	
	public static String getTagStatusDesc(String tagStatus) {
		return StringUtils.equalsIgnoreCase(tagStatus, YumPmsConstants.UPPER_CASE_ALPHABET_R) ? 
				YumPmsConstants.TAG_REJECT : YumPmsConstants.TAG_ACCEPT;
	}
	
	/**
	 * This method check the provided parameter is null and greater than and equal to zero
	 * @param number: This accepts integer, float, double, long, short
	 * @return true/false
	 */
	public static boolean isGreaterThanEqualToZero(Number number) {
		return number != null && number.intValue() >= 0;
	}
	
	// ============================================PDF Creation Method===========================================================
	
	public static ByteArrayOutputStream getPDF(EmployeeGoalSheet employeeGoalSheet, Map<String, Object> tagReplaceMap) throws IOException {

		ByteArrayOutputStream output = new ByteArrayOutputStream();

		String[][] headerDescriptionContent = {
				{"Associate Name:","<b>" + employeeGoalSheet.getEmpName() +"</b>","Designation:","<b>" + 
						employeeGoalSheet.getEmpDesignation() + "</b>"},
				{"Supervisor Name:","<b>" + employeeGoalSheet.getSupervisor_name() + "</b>","Year:","<b>" + 
						employeeGoalSheet.getPeriodDesc() + "</b>"},
				{"Function Name:","<b>" + employeeGoalSheet.getFunctionName()+"</b>","Function LT:","<b>" + 
						employeeGoalSheet.getFunctionalLtsName() + "</b>" },
				{"Goal Sheet Status:","<b>" + employeeGoalSheet.getGoalStatus().getGoalStatusName() + "</b>"}
		};
		//Creating PDF document object 
		PDDocument doc = new PDDocument();
		//Adding the blank page to the document
		doc.addPage(new PDPage());
		PDPage page = doc.getPage(0);

		PDPageContentStream contentStream = new PDPageContentStream(doc, page);
		YumPmsUtils.drawPDFHeader(doc, contentStream);

		//Dummy Table
		float margin = 50;
		// starting y position is whole page height subtracted by top and bottom margin
		float yStartNewPage = page.getMediaBox().getHeight() - (2 * margin);
		// we want table across whole page width (subtracted by left and right margin ofcourse)
		float tableWidth = page.getMediaBox().getWidth() - (2 * margin);

		boolean drawContent = true;
		float bottomMargin = 70;
		// y position is your coordinate of top left corner of the table
		final float yPosition = 700;

		BaseTable headerTable = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, margin, doc, page, false, drawContent);	
		YumPmsUtils.drawPDFDescription(headerTable, headerDescriptionContent);
		BaseTable table = new BaseTable(yPosition-100, yStartNewPage, bottomMargin, tableWidth, margin, doc, page, true, drawContent);
		// Get goal sheet pdf content and draw into pdf table
		YumPmsUtils.drawPDFContent(table, getGoalSheetPdfContent(employeeGoalSheet, tagReplaceMap));

		//------------------------------------------------------------
		doc.addPage( new PDPage() );
		int totalPages = doc.getNumberOfPages();
		page = doc.getPage(totalPages-1);
		float footerTableWidth = page.getMediaBox().getWidth() - (25);
		BaseTable footerTable1 = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, margin, doc, page, true, drawContent);	
		BaseTable footerTable2 = new BaseTable(yPosition-250, yStartNewPage, bottomMargin, tableWidth, margin, doc, page, true, drawContent);
		BaseTable footerTable3 = new BaseTable(yPosition-275, yStartNewPage, bottomMargin, tableWidth, margin, doc, page, false, drawContent);
		BaseTable footerTable4 = new BaseTable(yPosition-350, yStartNewPage, bottomMargin, footerTableWidth, margin, doc, page, true, drawContent);

		PDPageContentStream contentStream1 = new PDPageContentStream(doc, page);
		YumPmsUtils.drawPDFFooter(contentStream1);	

		YumPmsUtils.drawPDFFooterTable(footerTable1, 1);
		YumPmsUtils.drawPDFFooterTable(footerTable2, 2);
		YumPmsUtils.drawPDFFooterTable(footerTable3, 3);
		YumPmsUtils.drawPDFFooterTable(footerTable4, 4);
		contentStream1.close();
		//------------------------------------------------------------		
		contentStream.close();
		doc.save(output);
		//Closing the document  
		doc.close();

		return output;
	}

	private static ArrayList<ArrayList<String>> getGoalSheetPdfContent(EmployeeGoalSheet employeeGoalSheet, Map<String, Object> tagReplaceMap) {
		
		ArrayList<ArrayList<String>> goals = new ArrayList<>();
		for(EmployeeGoalMatrix empGoal : employeeGoalSheet.getEmpGoalMatrix()) {
			
			ArrayList<String> goal = new ArrayList<>();
			StringBuilder goalContent = new StringBuilder();
			goal.add("<p>" + empGoal.getOrgGoal().getOrgGoalDesc() + "</p>");

			List<EmployeeGoalSection> employeeGoalSections = empGoal.getMyGoal().getEmployeeGoalSectionList();
			Iterator<EmployeeGoalSection> it = employeeGoalSections.iterator();
			while(it.hasNext()) {
				EmployeeGoalSection employeeGoalSection = it.next();
				String gs = employeeGoalSection.getGoalSectionSummary();
				LOGGER.info("Goal Section = {}", gs);
				LOGGER.info("Level 1 Goal Section = {}", gs);
				Document thing = Jsoup.parse(gs);
				for (Element elem : thing.getElementsByTag("span")) {
					LOGGER.info("element = {}", elem);
					elem.parent().insertChildren(elem.siblingIndex(), elem.childNodes());
					elem.remove();
				}
				thing.outputSettings().indentAmount(0).prettyPrint(false);
				gs = thing.body().html();
				gs = gs.replaceAll("\\t", "    ");
				gs = gs.replaceAll("\\n", "<br>");
				for (Map.Entry<String, Object> entry : tagReplaceMap.entrySet()) {
					String key = entry.getKey();
					String value = entry.getValue().toString();
					gs = gs.replaceAll(key, value);
					LOGGER.info("Key = {} and Value = {}", key, value);
				}
				LOGGER.info("UpdatedGoal Section = {}", gs);
				goalContent.append("<li>").append(gs);
				if(StringUtils.isNotEmpty(employeeGoalSection.getGoalSectionTimeLine())) {
					goalContent.append("<br><br>      <b>By: ").append(format(employeeGoalSection.getGoalSectionTimeLine())).append("</b>");
				}
				goalContent.append("</li>");
				if(it.hasNext()) {
					goalContent.append("<br><br>");
				}
			}
			goal.add(goalContent.toString());
			LOGGER.info("Goal Content = {}", goalContent);
			goals.add(goal);
		}
		return goals;
	}

	private static String format(String gsDate) {
		
		String newdate = StringUtils.EMPTY;
		try {
			Date result = new SimpleDateFormat(YumPmsConstants.DATE_FORMAT_YYYY_MM_DD, Locale.ENGLISH).parse(gsDate);
			newdate = new SimpleDateFormat(YumPmsConstants.TIME_LINE_DATE_FORMAT).format(result);
		} catch (ParseException e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
		}
		return newdate;
	}
	
	public static void drawPDFHeader(PDDocument doc, PDPageContentStream contentStream) throws IOException {
		
		PDImageXObject pdImage = PDImageXObject.createFromFile(ResourceUtils.getReportLogo(), doc);
		contentStream.drawImage(pdImage, 50, 720);
		//Begin the Content stream 
		contentStream.beginText();
		contentStream.setLeading(1.5*16f);
		contentStream.newLineAtOffset(90, 735);
		//Setting the font to the Content stream
		contentStream.setFont( PDType1Font.HELVETICA, 16 );
		contentStream. showText("Performance Management System");
		//Ending the content stream
		contentStream.endText();
	}
	
	public static void drawPDFDescription(BaseTable headerTable, String[][] content) throws IOException {
		
		LOGGER.info("content length = {}", content.length);
		for(int i=0; i<content.length; i++) {
			Row<PDPage> headerRow = headerTable.createRow(15f);
			for(int j=0; j<content[i].length; j += 2) {
				Cell<PDPage> cell = headerRow.createCell(20, content[i][j]);
				cell.setFont(PDType1Font.HELVETICA);
				cell.setFontSize(8);
				LOGGER.info("content : {}", content[i][j]);
				cell = headerRow.createCell(30, content[i][j+1]);
				cell.setFont(PDType1Font.HELVETICA);
				cell.setFontSize(8);
			}
			headerTable.addHeaderRow(headerRow);
		}
		headerTable.draw();
	}
	
	public static void drawPDFContent(BaseTable table, List<ArrayList<String>> goals) throws IOException {
		
		Color fillColor = new Color(245, 245, 245);
		Row<PDPage> headerRow = table.createRow(15f);
		Cell<PDPage> cell = headerRow.createCell(20, "Goal Category");
		cell.setFont(PDType1Font.HELVETICA);
		cell.setFontSize(8);
		cell = headerRow.createCell(50, "My Goal");
		cell.setFont(PDType1Font.HELVETICA);
		cell.setFontSize(8);
		cell = headerRow.createCell(15, "Self Evaluation");
		cell.setFont(PDType1Font.HELVETICA);
		cell.setFontSize(8);
		cell.setFillColor(fillColor); 
		cell = headerRow.createCell(15, "Coach Evaluation");
		cell.setFont(PDType1Font.HELVETICA);
		cell.setFontSize(8);
		cell.setFillColor(fillColor); 
		table.addHeaderRow(headerRow);
		
		for(int i=0; i<goals.size(); i++) {
			ArrayList<String> goal=goals.get(i);
			Row<PDPage> row = table.createRow(12);
			cell = row.createCell(20, goal.get(0));
			cell.setFont(PDType1Font.HELVETICA);
			cell.setFontSize(8);
			cell = row.createCell(50, goal.get(1));
			cell.setFont(PDType1Font.HELVETICA);
			cell.setFontSize(8);
			cell = row.createCell(15, "");
			cell.setFont(PDType1Font.HELVETICA);
			cell.setFontSize(8);
			cell.setFillColor(fillColor); 
			cell = row.createCell(15, "");
			cell.setFont(PDType1Font.HELVETICA);
			cell.setFontSize(8);
			cell.setFillColor(fillColor); 
		}
		table.draw();	
	}
	
	public static void drawPDFFooterTable(BaseTable footerTable, int x) throws IOException {
		
		float height = (float)(x == 1 ? 160.00 : 60.00);
		String header = x == 1 ? "<b>RESULTS</b><br><i>Coach summary of your performance." : "<b>SIGNATURE</b>";

		if(x == 1 || x == 2) {
			Row<PDPage> headerRow = footerTable.createRow(15f);	
			Cell<PDPage> cell = headerRow.createCell(100, header);
			cell.setFont(PDType1Font.HELVETICA);
			cell.setFontSize(8);
			cell.setFillColor(new Color(245, 245, 245));
			footerTable.addHeaderRow(headerRow);
			
			Row<PDPage> row = footerTable.createRow(12);
			cell = row.createCell(100, "");
			cell.setFont(PDType1Font.HELVETICA);
			cell.setFontSize(10);
			cell.setHeight(height); 
			footerTable.addHeaderRow(row);
			
		} else if(x == 3) {			// Create table third time-----------------
			Row<PDPage> row = footerTable.createRow(12);
			Cell<PDPage> cell = row.createCell(25, StringUtils.EMPTY);
			cell.setFont(PDType1Font.HELVETICA);
			cell.setFontSize(10);
			cell.setAlign(HorizontalAlignment.CENTER); 
			
			cell = row.createCell(25, StringUtils.EMPTY);
			cell.setFont(PDType1Font.HELVETICA);
			cell.setFontSize(10);
			cell.setAlign(HorizontalAlignment.CENTER); 
			
			cell = row.createCell(25, StringUtils.EMPTY);
			cell.setFont(PDType1Font.HELVETICA);
			cell.setFontSize(10);
			cell.setAlign(HorizontalAlignment.CENTER); 
			
			cell = row.createCell(25, StringUtils.EMPTY);
			cell.setFont(PDType1Font.HELVETICA);
			cell.setFontSize(10);
			cell.setAlign(HorizontalAlignment.CENTER); 
			footerTable.addHeaderRow(row);
			
			Row<PDPage> headerRow = footerTable.createRow(12);
			cell = headerRow.createCell(25, YumPmsConstants.UNDERSCORE_12);
			cell.setFont(PDType1Font.COURIER);
			cell.setFontSize(10);
			cell.setAlign(HorizontalAlignment.CENTER); 
			
			cell = headerRow.createCell(25, YumPmsConstants.UNDERSCORE_12);
			cell.setFont(PDType1Font.COURIER);
			cell.setFontSize(10);
			cell.setAlign(HorizontalAlignment.CENTER); 
			
			cell = headerRow.createCell(25, YumPmsConstants.UNDERSCORE_12);
			cell.setFont(PDType1Font.COURIER);
			cell.setFontSize(10);
			cell.setAlign(HorizontalAlignment.CENTER); 
			
			cell = headerRow.createCell(25, YumPmsConstants.UNDERSCORE_12);
			cell.setFont(PDType1Font.COURIER);
			cell.setFontSize(10);
			cell.setAlign(HorizontalAlignment.CENTER); 
			footerTable.addHeaderRow(headerRow);	// Header Complete-----------------
			
			row = footerTable.createRow(12);
			cell = row.createCell(25, YumPmsConstants.INDIVIDUAL);
			cell.setFont(PDType1Font.HELVETICA);
			cell.setFontSize(10);
			cell.setAlign(HorizontalAlignment.CENTER);			
			
			cell = row.createCell(25, YumPmsConstants.DATE);
			cell.setFont(PDType1Font.HELVETICA);
			cell.setFontSize(10);
			cell.setAlign(HorizontalAlignment.CENTER); 
			
			cell = row.createCell(25, YumPmsConstants.COACH);
			cell.setFont(PDType1Font.HELVETICA);
			cell.setFontSize(10);
			cell.setAlign(HorizontalAlignment.CENTER); 
			
			cell = row.createCell(25, YumPmsConstants.DATE);
			cell.setFont(PDType1Font.HELVETICA);
			cell.setFontSize(10);
			cell.setAlign(HorizontalAlignment.CENTER);
			footerTable.addHeaderRow(row);
		} else if(x == 4) {
			Row<PDPage> row = footerTable.createRow(2);
			Cell<PDPage> cell = row.createCell(2, StringUtils.EMPTY);
			cell.setFont(PDType1Font.HELVETICA);
			cell.setFontSize(10);
			footerTable.addHeaderRow(row);
		}
		footerTable.draw();		
	}
	
	public static void drawPDFFooter(PDPageContentStream contentStream) throws IOException {	
		//Begin the Content stream 
		contentStream.beginText();
		contentStream.setLeading(1.5*16f);
		contentStream.newLineAtOffset(80, 345);
		//Setting the font to the Content stream
		contentStream.setFont( PDType1Font.HELVETICA, 8 );
		contentStream.showText("DEVELOPMENT CHECK-IN");
		contentStream.newLineAtOffset(0, -10);
		contentStream.showText("Refer back to your IDP Development Plan actions and discuss progress. Update if neccessary.");
		//Ending the content stream
		contentStream.endText();
	}
	
	public static boolean isNumber(String str) {  
		
		try {  
			Integer.parseInt(str);  
		} catch(Exception e) {  
			return false;  
		}  
		return true;  
	}
	
	public static byte[] getIDPSheetReportData(IdpUIConfig idpUIConfig) {
		return ReportUtils.getReportContent(ReportName.IDP_SHEET_REPORT, getIDPSheetParams(idpUIConfig));
	}
	
	public static Map<String, Object> getIDPSheetParams(IdpUIConfig idpUIConfig) {
		
		Map<String, Object> params = new HashMap<>();
		params.put("empName", idpUIConfig.getEmpName());
		params.put("supervisorName", idpUIConfig.getSupervisorName());
		params.put("functionName", idpUIConfig.getFunctionName());
		params.put("functionLTName", idpUIConfig.getFunctionLTName());
		params.put("goalSheetStatus", idpUIConfig.getIdpSheetStatus());
		params.put("designation", idpUIConfig.getEmpDesignation());
		params.put("year", idpUIConfig.getAppraisalDescription());
		params.put("reportLogoPath", ResourceUtils.getReportLogo());
		
		List<UiControll> performenceList = idpUIConfig.getPerformenceList();
		
		if(CollectionUtils.isNotEmpty(performenceList)) {
			
			params.put("performanceGoal", StringUtils.trimToNull(performenceList.get(0).getLabel()));
			params.put("performanceGoalValue", StringUtils.trimToNull(performenceList.get(0).getValue()));
			
			if(performenceList.size() > 1) {
				params.put("performanceCulture", StringUtils.trimToNull(performenceList.get(1).getLabel()));
				params.put("performanceCultureValue", StringUtils.trimToNull(performenceList.get(1).getValue()));
			}
		}
		
		List<ReportModel> planList = getPlanList(idpUIConfig);
		if(planList != null && planList.size() > 7) {
			
			params.put("careerAspiration", planList.get(0).getLabel());
			params.put("careerAspirationValue", planList.get(0).getDesc());

			params.put("nextJob", planList.get(1).getLabel());
			params.put("nextJobValue", planList.get(1).getDesc());

			params.put("careerPotential", planList.get(2).getLabel());
			params.put("careerPotentialValue", planList.get(2).getDesc());
			
			String relocateValue = StringUtils.trimToEmpty(planList.get(3).getDesc());
			
			params.put("relocate", planList.get(3).getLabel());
			params.put("relocateValue", relocateValue);
			
			params.put("relocateYes", "Y".equalsIgnoreCase(relocateValue) ? YumPmsConstants.CHECK_BOX_PATH : YumPmsConstants.UNCHECK_BOX_PATH);
			params.put("relocateNo", "N".equalsIgnoreCase(relocateValue) ? YumPmsConstants.CHECK_BOX_PATH : YumPmsConstants.UNCHECK_BOX_PATH);
			params.put("relocateInternational", "I".equalsIgnoreCase(relocateValue) ? 
					YumPmsConstants.CHECK_BOX_PATH : YumPmsConstants.UNCHECK_BOX_PATH);
			
			params.put("languages", planList.get(4).getLabel());
			params.put("languagesValue", planList.get(4).getDesc());

			params.put("citizenship", planList.get(5).getLabel());
			params.put("citizenshipValue", planList.get(5).getDesc());

			params.put("preferences", planList.get(6).getLabel());
			params.put("preferencesValue", planList.get(6).getDesc());

			params.put("developmentGoal", planList.get(7).getLabel());
			params.put("developmentGoalValue", planList.get(7).getDesc());
		}
		
		params.put("developmentPlanDataset", new JRBeanCollectionDataSource(getDevelopmentActionPlanList(idpUIConfig)));
		params.put("balanceGoalDataset", new JRBeanCollectionDataSource(getBalanceGoalList(idpUIConfig)));
		params.put("howILeadDataset", new JRBeanCollectionDataSource(getHowLeadAppreciate(idpUIConfig)));
		params.put("howILeadDataset2", new JRBeanCollectionDataSource(getHowLeadEffectiveness(idpUIConfig)));
		params.put("howILeadGoal", idpUIConfig.getHowLead().getGrowth().getDescription());
		
		return params;
	}
	
	private static List<ReportModel> getPlanList(IdpUIConfig idpUIConfig) {

		List<UiControll> planList = idpUIConfig.getPlanList();
		List<ReportModel> result = null;
		if(CollectionUtils.isNotEmpty(planList)) {
			result = planList.stream().map(uiControll -> new ReportModel(uiControll.getLabel(), uiControll.getValue())).collect(Collectors.toList());
		}
		return getEmptyListIfNull(result);
	}
	
	private static List<ReportModel> getDevelopmentActionPlanList(IdpUIConfig idpUIConfig) {
		
		List<DevelopmentActionPlan> devActPlanList = idpUIConfig.getDevelopmentActionPlanList();
		List<ReportModel> result = null;
		if(CollectionUtils.isNotEmpty(devActPlanList)) {
			result = devActPlanList.stream().map(ReportModel :: new).collect(Collectors.toList());
		}
		return getEmptyListIfNull(result);
	}
	
	private static List<ReportModel> getBalanceGoalList(IdpUIConfig idpUIConfig) {
		
		List<BalanceOfGoal> balanceGoals = idpUIConfig.getBalanceGoal();
		List<ReportModel> result = null;
		if(CollectionUtils.isNotEmpty(balanceGoals)) {
			result = balanceGoals.stream().map(ReportModel :: new).collect(Collectors.toList());
		}
		return getEmptyListIfNull(result);
	}
	
	private static List<ReportModel> getHowLeadAppreciate(IdpUIConfig idpUIConfig) {
		
		List<HowLeadSubData> appreciateList = idpUIConfig.getHowLead().getAppreciate();
		List<ReportModel> result = null;
		if(CollectionUtils.isNotEmpty(appreciateList)) {
			result = appreciateList.stream().map(ReportModel :: new).collect(Collectors.toList());
		}
		return getEmptyListIfNull(result);
	}
	
	private static List<ReportModel> getHowLeadEffectiveness(IdpUIConfig idpUIConfig) {
		
		List<HowLeadSubData> appreciateList = idpUIConfig.getHowLead().getEffectiveness();
		List<ReportModel> result = null;
		if(CollectionUtils.isNotEmpty(appreciateList)) {
			result = appreciateList.stream().map(ReportModel :: new).collect(Collectors.toList());
		}
		return getEmptyListIfNull(result);
	}
	
	public static void csvCreation(List<GeneralDomain> domainList) {
		
		try (Writer writer = Files.newBufferedWriter(Paths.get(ResourceUtils.getPropertyValue("PATH_OF_CSV_FILE")))) {
			
			StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer)
					.withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
					.build();
			for(GeneralDomain generalDomain : domainList) {
				domainList.add(generalDomain);
			}
			beanToCsv.write(domainList);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			throw new CommonException(e);
		}
	}
	
	public static final <T> T newInstance(Class<T> clazz) {
		
		T obj = null;
		if(clazz == null) {
			throw new IllegalArgumentException(YumPmsConstants.ARGUMENT_CAN_NOT_NULL);
		}
		String className = clazz.getSimpleName();
		try {
			obj = (T) objectMap.get(className);
			if(obj == null) {
				obj = clazz.newInstance();
				objectMap.putIfAbsent(className, obj);
			}
		} catch (InstantiationException | IllegalAccessException e) {
			LOGGER.error(e.getMessage());
		}
		return obj;
	}
	
	public static Integer getGoalIdFromGoalSection(GoalSection goalSection) {
		
		if(goalSection == null || goalSection.getGoal() == null) {
			return 0;
		}
		return goalSection.getGoal().getGoalId();
	}
	
	/**
	 * Get the PA Sheet PDF data 
	 * @param sheetData
	 * @return
	 */
	public static byte[] getPaSheetReportData(PASheetData sheetData) {
		
		return ReportUtils.getReportContent(ReportName.PA_SHEET_REPORT, getPaSheetParams(sheetData));
	}

	/**
	 * Get the Jasper report parameters for PA Sheet PDF
	 * @param sheetData
	 * @return
	 */
	private static Map<String, Object> getPaSheetParams(PASheetData sheetData) {
		
		if(sheetData == null || isNullOrZero(sheetData.getPaSheetId())) {
			return Collections.emptyMap();
		}
		Map<String, Object> params = new HashMap<>();
		params.put(YumPmsConstants.EMP_NAME, sheetData.getEmpName());
		params.put(YumPmsConstants.SUPERVISOR_NAME, sheetData.getSupervisorName());
		params.put(YumPmsConstants.FUNCTION_NAME, sheetData.getFunction());
		params.put(YumPmsConstants.FUNCTION_LT_NAME, sheetData.getLtName());
		params.put(YumPmsConstants.PA_SHEET_STATUS, sheetData.getPaSheetStatusDesc());
		params.put(YumPmsConstants.DESIGNATION, sheetData.getEmpDesignation());
		params.put(YumPmsConstants.YEAR, sheetData.getApprDesc());
		params.put(YumPmsConstants.REPORT_LOGO_PATH, ResourceUtils.getReportLogo());
		List<PaGoalSectionReport> goalSectionsList = new ArrayList<>();
		List<SelfSheetData> selfSheetList = sheetData.getSelfSheetList();
		if(CollectionUtils.isNotEmpty(selfSheetList)) {
			List<Rating> ratingsList = selfSheetList.get(0).getRatingsList();
			Map<Integer, String> ratingMap = ratingsList.stream().collect(Collectors.toMap(Rating :: getRatingId, Rating :: getRatingDesc));
			for (SelfSheetData selfSheet : selfSheetList) {
				List<MyGoal> myGoalSectionList = selfSheet.getMyGoalSectionList();
				if(CollectionUtils.isEmpty(myGoalSectionList)) {
					continue;
				}
				List<PaGoalSectionReport> list = myGoalSectionList.stream().map(PaGoalSectionReport :: new).collect(Collectors.toList());
				list.forEach(goalSection -> {
					goalSection.setRating(StringUtils.trimToEmpty(ratingMap.get(goalSection.getRatingId())));
					goalSection.setGoalDesc(selfSheet.getOrgGoalDesc());
				});
				goalSectionsList.addAll(list);
			}
		}
		params.put(YumPmsConstants.PA_SELFGOAL_DATASET, new JRBeanCollectionDataSource(goalSectionsList));
		return params;
	}
	
	/**
	 * Prepare the data set for paSheet PDF report
	 * @param selfSheetList
	 * @return
	 */
	public static List<PaReportData> getSelfGoalList(List<SelfSheetData> selfSheetList) {
		
		if(CollectionUtils.isEmpty(selfSheetList)) {
			return Collections.emptyList();
		}
		return selfSheetList.stream().map(PaReportData :: new).collect(Collectors.toList());
	}
	
	/**
	 * Get the Goal Sheet PDF data 
	 * @param empGoalSheet
	 * @return report content
	 */
	public static byte[] getGoalSheetReportData(EmployeeGoalSheet empGoalSheet) {
		
		return ReportUtils.getReportContent(ReportName.GOAL_SHEET_REPORT, geGoalSheetParams(empGoalSheet));
	}

	/**
	 * Get the Jasper report parameters for Goal Sheet PDF
	 * @param empGoalSheet
	 * @return report parameter data
	 */
	private static Map<String, Object> geGoalSheetParams(EmployeeGoalSheet empGoalSheet) {
		
		if(empGoalSheet == null || isNullOrZero(empGoalSheet.getGoal_sheet_id())) {
			return Collections.emptyMap();
		}
		Map<String, Object> params = new HashMap<>();
		params.put(YumPmsConstants.EMP_NAME, empGoalSheet.getEmpName());
		params.put(YumPmsConstants.SUPERVISOR_NAME, empGoalSheet.getSupervisor_name());
		params.put(YumPmsConstants.FUNCTION_NAME, empGoalSheet.getFunctionName());
		params.put(YumPmsConstants.FUNCTION_LT_NAME, empGoalSheet.getFunctionalLtsName());
		params.put(YumPmsConstants.GOAL_SHEET_STATUS, empGoalSheet.getGoalStatus().getGoalStatusName());
		params.put(YumPmsConstants.DESIGNATION, empGoalSheet.getEmpDesignation());
		params.put(YumPmsConstants.YEAR, empGoalSheet.getPeriodDesc());
		params.put(YumPmsConstants.REPORT_LOGO_PATH, ResourceUtils.getReportLogo());
		
		List<GoalSheetReportData> reportDataset = new LinkedList<>();
		StringBuilder goalSections = new StringBuilder();
		for (EmployeeGoalMatrix empGoalMatrix : empGoalSheet.getEmpGoalMatrix()) {
			for (EmployeeGoalSection empGoalSection : empGoalMatrix.getMyGoal().getEmployeeGoalSectionList()) {
				goalSections
					.append(StringUtils.trimToEmpty(empGoalSection.getGoalSectionSummary()))
					.append(YumPmsConstants.HTML_BREAK_TAG)
					.append(YumPmsConstants.HTML_BOLD_START_TAG)
					.append(getGoalSectionTimeline(empGoalSection.getGoalSectionTimeLine()))
					.append(YumPmsConstants.HTML_BOLD_END_TAG)
					.append(YumPmsConstants.HTML_BREAK_TAG);
			}
			reportDataset.add(new GoalSheetReportData(empGoalMatrix.getOrgGoal().getOrgGoalDesc(), StringUtils.trim(goalSections.toString())));
			goalSections.setLength(0);
		}
		params.put(YumPmsConstants.GOAL_SHEET_DATASET, new JRBeanCollectionDataSource(reportDataset));
		return params;
	}
	
	private static String getGoalSectionTimeline(String timeline) {
		
		String formattedTimeline = DateTimeUtils.format(timeline, YumPmsConstants.TIME_LINE_DATE_FORMAT);
		return StringUtils.isEmpty(formattedTimeline) ? StringUtils.EMPTY : YumPmsConstants.TIME_LINE_LABEL_BY + formattedTimeline;
	}
	
	public static byte[] getCompensationLetterContent(CompensationLetter letter) {
		
		String content = letter.getHtmlReportContent();
		LOGGER.info("HTML Report Content = {}", content);
		if(StringUtils.isBlank(content)) {
			return new byte[0];
		}
		String[] pages = content.split(YumPmsConstants.HTML_HR_ELEMENT);
		String pageBreak = pages.length > 1 ? "found" : "not found";
		LOGGER.info("page break <hr/> element {}", pageBreak);
		return ReportUtils.getHtmlToPdfContent(pages);
	}
	
	/**
	 * Get the size from bits.
	 * Example: 
	 * 	param :1024 = 1 KB
	 * @param size
	 * @return
	 */
	public static String getSize(long size) {
		
		double kb = size/1024.0;
		double mb = size/1048576.0;
		double gb = size/1073741824.0;

		DecimalFormat dec = new DecimalFormat("0.00");
		String strSize = null;
		if (gb > 1) {
			strSize = dec.format(gb).concat(" GB");
		} else if (mb > 1) {
			strSize = dec.format(mb).concat(" MB");
		} else if(kb > 1) {
			strSize = dec.format(kb).concat(" KB");
		} else {
			strSize = dec.format(size/8.0).concat(" Bytes");
		}
		return StringUtils.isEmpty(strSize) ? StringUtils.EMPTY : strSize;
	}
	
	/**
	 * Used to create Compensation letter PDF file name
	 * @param empName
	 * @param apprId
	 * @param letterType
	 * @return
	 */
	public static String getCompLetterFileName(String empName, String apprId, String letterType) {
		
		String letterSuffix = YumPmsConstants.COMP_LETTER_FILE;
		if(StringUtils.equalsIgnoreCase(letterType, "bonus")) {
			letterSuffix = "_Bonus_Letter_";
		} else if(StringUtils.equalsIgnoreCase(letterType, "lumpsum")) {
			letterSuffix = "_Retention_Bonus_Letter_";
		}
		return getPdfFileName(empName, apprId, letterSuffix);
	}
	
	/**
	 * Used to create IDP/Goal/PA PDF file name
	 * @param empName
	 * @param apprId
	 * @return
	 */
	public static String getPdfFileName(String empName, String apprId, String whichSheet) {
		
		String name = null;
		switch(whichSheet) {
		case YumPmsConstants.GOAL_SHEET_PDF_FILE:
			name = YumPmsConstants.GOAL_SHEET_PDF_NAME;
			break;
		case YumPmsConstants.IDP_SHEET_PDF_FILE:
			name = YumPmsConstants.IDP_SHEET_PDF_NAME;
			break;
		case YumPmsConstants.PA_SHEET_PDF_FILE:
			name = YumPmsConstants.PA_SHEET_PDF_NAME;
			break;
		case YumPmsConstants.COMP_LETTER_FILE:
			name = YumPmsConstants.COMP_LETTER_PDF_NAME;
			break;
		case YumPmsConstants.PA_ZIP_FILE:
			name = YumPmsConstants.PA_COMP_ZIP_NAME;
			break;	
		default:
			name = StringUtils.trimToEmpty(whichSheet);
			break;
		}
		return new StringBuilder(StringUtils.replace(empName, YumPmsConstants.SPACE, YumPmsConstants.UNDERSCORE))
				.append(name)
				.append(apprId)
				.append(YumPmsConstants.UNDERSCORE)
				.append(System.nanoTime())
				.append(whichSheet.equalsIgnoreCase(YumPmsConstants.PA_ZIP_FILE)?YumPmsConstants.ZIP_FILE_EXTENSION : YumPmsConstants.PDF_FILE_EXTENSION)
				.toString();
	}
	
	public static String getExceptionMessage(Throwable ex) {
		
		String msg = null;
		if(StringUtils.isEmpty(ex.getMessage())) {
			msg = ex.toString();
		} else {
			int index = ex.getMessage().indexOf(YumPmsConstants.SEMICOLON);
			msg = index > 0 ? ex.getMessage().substring(0, index) : ex.getMessage();
		}
		return StringUtils.isEmpty(msg) ? YumPmsConstants.UNKNOWN_ERROR : msg;
	}
	
	/**
	 * Return an empty list if the parameter is null
	 * @param list
	 * @return
	 */
	public static <E> List<E> getEmptyListIfNull(List<E> list) {
		return CollectionUtils.isEmpty(list) ? Collections.emptyList() : list;
	}
	
	/**
	 * Return an empty map if the parameter is null
	 * @param map
	 * @return
	 */
	public static <E, T> Map<E, T> getEmptyMapIfNull(Map<E, T> map) {
		return MapUtils.isEmpty(map) ? Collections.emptyMap() : map;
	}
	
	/**
	 * Sort the list as per provided comparator
	 * @param c
	 * @return
	 */
	public static <T> Collector<T, Object, List<T>> toSortedList(Comparator<T> c) {
	    return Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(c)), ArrayList :: new);
	}
	
	/**
	 * Handle NullPointerException; If NPE occurs then this will return empty
	 * @param resolver
	 * @return Optional<T>
	 */
	public static <T> Optional<T> resolve(Supplier<T> resolver) {
	    try {
	        T result = resolver.get();
	        return Optional.ofNullable(result);
	    } catch (NullPointerException e) {
	        return Optional.empty();
	    }
	}
	
	/**
	 * Get the Optional List Value is present
	 * @param optional
	 * @return
	 */
	public static <T> List<T> getOptionalListValue(Optional<List<T>> optional) {
		
		return optional.orElse(Collections.emptyList());
	}
	
	/**
	 * Used to know a particular property is available in bean or not 
	 * @param bean
	 * @param property
	 * @return
	 */
	public static <T> boolean isPropertyExists(T bean, String property) {
	    
		if(bean == null || StringUtils.isBlank(property)) {
			return false;
		}
		return PropertyUtils.isReadable(bean, property) && PropertyUtils.isWriteable(bean, property); 
	}
	
	/**
	 * Create object using no argument constructor
	 * @param clazz
	 * @return
	 */
	public static <T> T createObject(Class<T> clazz) {
		
		T obj = null;
		if(clazz == null) {
			throw new IllegalArgumentException(YumPmsConstants.ARGUMENT_CAN_NOT_NULL);
		}
		try {
			obj = clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e.getMessage());
			throw new CommonException(e);
		}
		return obj;
	}
	
	public static String buildSignatureFileName(String empName, Integer empId, String originalFilename) {

		return 
				new StringBuilder(StringUtils.replace(empName, YumPmsConstants.SPACE, YumPmsConstants.UNDERSCORE))
				.append(YumPmsConstants.UNDERSCORE)
				.append(empId)
				.append(YumPmsConstants.STR_EXCLAMATION)
				.append(originalFilename)
				.toString();
	}
	
	/**
	 * Set the provided value using the bean's setter method. 
	 * The bean should follow the java bean standard
	 * @param bean
	 * @param propertyName
	 * @param value
	 */
	public static <T> void setProperty(T bean, String propertyName, Object value) {
		
		if(bean == null) {
			return;
		}
		if(!isPropertyExists(bean, propertyName)) {
			return;
		}
		try {
			PropertyUtils.setProperty(bean, propertyName, value);
			//Will remove later BeanUtils.setProperty(bean, propertyName, value); later
		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e.getMessage());
		}
	}
	
	public static <T> Object getProperty(T bean, String propertyName) {
		
		if(bean == null || !isPropertyExists(bean, propertyName)) {
			return null;
		}
		try {
			return PropertyUtils.getProperty(bean, propertyName);
		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e.getMessage());
		}
		return null;
	}
	
}
