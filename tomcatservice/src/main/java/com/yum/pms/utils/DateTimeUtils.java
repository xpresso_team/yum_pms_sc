package com.yum.pms.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.ResolverStyle;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yum.pms.exception.CommonException;

/**
 * @author dipak.swain
 *
 */
public class DateTimeUtils {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DateTimeUtils.class);
	
	private static final DateFormat globalDateFormatter = getGlobalDateFormat();
	
	private DateTimeUtils() {}
	
	public static DateFormat getGlobalDateFormat() {
		
		if(globalDateFormatter == null) {
			return new SimpleDateFormat(YumPmsConstants.DATE_FORMAT_YYYY_MM_DD);
		}
		return globalDateFormatter;
	}
	
	public static Timestamp today() {
		return Timestamp.valueOf(LocalDate.now().atStartOfDay());
	}
	
	public static Timestamp now() {
		return Timestamp.valueOf(LocalDateTime.now());
	}
	
	public static String format(Date date, String pattern) {
		
		if(date == null) {
			return StringUtils.EMPTY;
		}
		java.sql.Date sqlDate = new java.sql.Date(date.getTime());
		LocalDate localDate = sqlDate.toLocalDate();
		return localDate.format(DateTimeFormatter.ofPattern(pattern));
	}
	
	public static String format(Timestamp timestamp, String pattern) {
		
		if(timestamp == null) {
			return StringUtils.EMPTY;
		}
		LocalDateTime localDateTime = timestamp.toLocalDateTime();
		return localDateTime.format(DateTimeFormatter.ofPattern(pattern));
	}
	
	public static String format(String dateStr, String toPattern) {
		if(StringUtils.isBlank(dateStr)) {
			return StringUtils.EMPTY;
		}
		LocalDate localDateTime = LocalDate.parse(dateStr, getStrictFormatter("uuuu-MM-dd"));
		return localDateTime.format(getFormatter(toPattern));
	}
	
	public static Date parse(String dateStr) {
		// We need to check the null or empty before parsing.
		// This function should return null if the parameter is null or empty as per business logic
		if(StringUtils.isEmpty(dateStr)) {
			return null;
		}
		return parse(dateStr, StringUtils.EMPTY);
	}
	
	public static Date parseWithSuppressException(String date) {
		if(StringUtils.isEmpty(date)) {
			return null;
		}
		try {
			return globalDateFormatter.parse(date);
		} catch (ParseException e) {
			throw new CommonException(e);
		}
	}
	
	public static String format(Date date) {
		return date == null ? StringUtils.EMPTY : globalDateFormatter.format(date);
	}
	
	public static Date parse(String dateStr, String toPattern) {
		
		if(StringUtils.isEmpty(dateStr)) {
			throw new CommonException("Parameter date can not be null");
		}
		try {
			if(StringUtils.isNotEmpty(toPattern)) {
				return new SimpleDateFormat(toPattern).parse(dateStr);
			}
			return globalDateFormatter.parse(dateStr);
		} catch (ParseException e) {
			throw new CommonException(e);
		}
	}
	
	public static DateTimeFormatter getFormatter(String pattern) {
		return DateTimeFormatter.ofPattern(pattern);
	}
	
	public static DateTimeFormatter getStrictFormatter(String pattern) {
		return new DateTimeFormatterBuilder()
				.parseStrict()
				.appendPattern(pattern)
				.toFormatter()
				.withResolverStyle(ResolverStyle.STRICT);
	}
	
	public static Date currentDate(String format) {
		
		if(StringUtils.isEmpty(format)) {
			return today();
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String currentDate = sdf.format(Calendar.getInstance().getTime());
		Date date2 = null;
		try {
			date2 = sdf.parse(currentDate);
		} catch (ParseException e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
		}
		return date2 == null ? today() : date2;
	}
	
	public static String currentDateStr(String format) {
		
		if(StringUtils.isEmpty(format)) {
			return StringUtils.EMPTY;
		}
		String strDate = new SimpleDateFormat(format).format(Calendar.getInstance().getTime());
		return StringUtils.isEmpty(strDate) ? StringUtils.EMPTY : strDate;
	}
	
	public static LocalDate toLocalDate(long longValue) {
		
		return Instant.ofEpochMilli(longValue).atZone(ZoneId.systemDefault()).toLocalDate();
	}
	
	public static LocalDate toLocalDate(Date date) {
		
		if(date == null) {
			date = now();
		}
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}
	
	public static Date toDate(LocalDate date) {
		
		return Date.from(date.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}
	
	public static Date toDate(long longValue) {
		
		return toDate(toLocalDate(longValue));
	}
	
	/**
	 * If date1 is before date2 ====> -1
	 * If date1 is after date2  ====> 1
	 * If date1 is equal date2  ====> 0
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static int isEqualOrAfterOrBefore(Date date1, Date date2) {
		
		LocalDate localDate1 = toLocalDate(date1);
		LocalDate localDate2 = toLocalDate(date2);
		if(localDate1.isBefore(localDate2)) {
			return -1;
		} else if(localDate1.isAfter(localDate2)) {
			return 1;
		}
		return 0;
	}
	
	/**
	 * If <b>date1</b> is after <b>date2</b>  return <b>true</b>
	 * Otherwise return <b>false</b><br/>
	 * Note : This function consider only date not time
	 * @param date1
	 * @param date2
	 * @return true/false
	 */
	public static boolean isAfter(Date date1, Date date2) {
		
		return toLocalDate(date1).isAfter(toLocalDate(date2));
	}
	
	/**
	 * If <b>date1</b> is before <b>date2</b>  return <b>true</b>
	 * Otherwise return <b>false</b><br/>
	 * Note : This function consider only date not time
	 * @param date1
	 * @param date2
	 * @return true/false
	 */
	public static boolean isBefore(Date date1, Date date2) {
		return toLocalDate(date1).isBefore(toLocalDate(date2));
	}
	
	/**
	 * If <b>date1</b> is equal <b>date2</b>  return <b>true</b>
	 * Otherwise return <b>false</b><br/>
	 * Note : This function consider only date not time
	 * @param date1
	 * @param date2
	 * @return true/false
	 */
	public static boolean isEqual(Date date1, Date date2) {
		return toLocalDate(date1).isEqual(toLocalDate(date2));
	}
	
	public static boolean isToday(Date date) {
		return toLocalDate(date).isEqual(toLocalDate(today()));
	}
	
	public static boolean isPastDate(Date date) {
		return toLocalDate(date).isBefore(toLocalDate(today()));
	}
	
	public static boolean isFutureDate(Date date) {
		return toLocalDate(date).isAfter(toLocalDate(today()));
	}
	
	public static int getYear(Date date) {
		return toLocalDate(date).getYear();
	}
	
	public static void main(String[] args) {
		
		LOGGER.info("Today = {}", today());
		
		LOGGER.info("Now = {}", now());
		
		LOGGER.info("Parse from String date to util date = {}", parse("2018-02-20", "yyyy-MM-dd"));
	}

}
