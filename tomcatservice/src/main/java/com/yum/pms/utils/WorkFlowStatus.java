package com.yum.pms.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dipak.swain
 * Now it is used to represent the PA Sheet Status, we can use this enum for other modules
 */
public enum WorkFlowStatus {
	
	
	NOT_INITIATED(1001), IN_DRAFT(1002), SUBMITTED(1003), ROLLED_BACK(1004), DISCUSS_AND_AGREED(1005), RECALLED(1006), UNKNOWN(-1);
	
	private Integer statusCode;
	private static Map<Integer, WorkFlowStatus> map = new HashMap<>();
	
	private WorkFlowStatus(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
	static {
        for (WorkFlowStatus statusType : WorkFlowStatus.values()) {
            map.put(statusType.statusCode, statusType);
        }
    }
	
	public static WorkFlowStatus toValue(Integer statusCode) {
		
		WorkFlowStatus status = map.get(statusCode);
		if(status == null) {
			status = UNKNOWN;
		}
		return status;
	}
	
	public Integer getStatusCode() {
		return statusCode;
	}
}
