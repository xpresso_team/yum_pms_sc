/******************************************************************** 
 * Email Sender 
 * 
 * http://www.abzooba.com
 * ------------------- 
 * Created By: keshav.kumar
 * Last Updated By: sudipta.chandra
 * Last Updated On: 23-Sep-2017 (11:34:34 AM) 
 *
 * The software and related user documentation are protected under 
 * copyright laws and remain the sole property of Abzooba. 
 * 
 * Technical support is available via the Abzooba website at 
 * http://www.abzooba.com
 **************************************************************************/

package com.yum.pms.utils;

import java.net.URI;
import java.net.URISyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.yum.pms.exception.CommonException;

import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.enumeration.property.BodyType;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.property.complex.EmailAddress;
import microsoft.exchange.webservices.data.property.complex.MessageBody;

@Component
public class SendMail {

	private static final Logger LOGGER = LoggerFactory.getLogger(SendMail.class);

	public void sendMail(String toMail, String ccMail, String subject, String body) {
		sendMail(toMail, ccMail, subject, body, null);		
	}

	public void sendMail(String toMail, String ccMail, String subject, String body, String pathOfFileToAttach) {

		try {
			ExchangeService emailService = getExchangeServiceObj(
					ResourceUtils.getPropertyValue("email_user"), 
					ResourceUtils.getPropertyValue("email_pass"), 
					ResourceUtils.getPropertyValue("ews_server"));

			if(!StringUtils.hasText(toMail)) { 
				toMail = ccMail;
			}
			EmailMessage emessage = new EmailMessage(emailService);
			LOGGER.info("Sending email {}", "messages");
			// Add properties to the email message.
			emessage.setSubject(subject);
			LOGGER.info("Subject: {}", subject);
			emessage.setFrom(new EmailAddress(ResourceUtils.getPropertyValue("email_address")));
			String strBodyMessage = body;
			strBodyMessage = strBodyMessage + "<br /><br />";
			LOGGER.info("Body: {} ", body);
			MessageBody msg = new MessageBody(BodyType.HTML, strBodyMessage);
			emessage.setBody(msg);

			if (org.apache.commons.lang.StringUtils.isNotEmpty(pathOfFileToAttach)) {
				emessage.getAttachments().addFileAttachment(pathOfFileToAttach);
			}
			//list of recepients 
			if(ResourceUtils.getPropertyValue("app_env").equals("dev_No")) {
				emessage.getToRecipients().add("jayanta.biswas@abzooba.com");
				emessage.getToRecipients().add("debottam.dattagupta@abzooba.com");
				emessage.getCcRecipients().add("sudipta.chandra@abzooba.com");
				emessage.getCcRecipients().add("rajiv.ranjan@abzooba.com");
			} else {
				LOGGER.info("In Else>>>>>>>>>.. To mail: {} ", toMail);
				if (toMail == null) {
					LOGGER.error("To Mail Not Specified {}", toMail	);
					throw new CommonException("SendMail: To Mail Not Specified.");
				}
				for (String to_mail_split : toMail.split(";")) {
					emessage.getToRecipients().add(to_mail_split.trim());
				}
				if (ccMail != null) {
					for (String cc_mail_split : ccMail.split(";")) {
						emessage.getCcRecipients().add(cc_mail_split.trim());					
					}
				}
			}
			emessage.sendAndSaveCopy();
			LOGGER.info("Email send {}", "sucessfully");
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			throw new CommonException(e);
		}
	}

	public ExchangeService getExchangeServiceObj(String userName, String password, String emailServerURI) throws URISyntaxException {

		ExchangeService service = null;
		LOGGER.debug("getExchangeServiceObj() {}.", "starts");
		if(!YumPmsUtils.isNullOrEmpty(userName) && !YumPmsUtils.isNullOrEmpty(password) && !YumPmsUtils.isNullOrEmpty(emailServerURI)) {
			if (ResourceUtils.getPropertyValue("exchange-server-type").equalsIgnoreCase("SP1")){
				service = new ExchangeService(ExchangeVersion.Exchange2010_SP1);
			} else if (ResourceUtils.getPropertyValue("exchange-server-type").equalsIgnoreCase("SP2")){
				service = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
			}
			if(service != null) {
				service.setCredentials(new WebCredentials(userName,password));
				service.setUrl(new URI(emailServerURI));   
			}
		} else {
			LOGGER.error("Email -->> Login credenitials are null or empty; userName = {}", userName);
		}
		LOGGER.debug("getExchangeServiceObj() {}.", "ends");
		return service;
	}

}



