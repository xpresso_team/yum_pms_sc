/******************************************************************** 
 * Hibernate Services for Workflow 
 * 
 * http://www.abzooba.com
 * ------------------- 
 * Created By: jayanta.biswas
 * Last Updated By: sudipta.chandra
 * Last Updated On: 23-Sep-2017 (11:34:34 AM) 
 *
 * The software and related user documentation are protected under 
 * copyright laws and remain the sole property of Abzooba. 
 * 
 * Technical support is available via the Abzooba website at 
 * http://www.abzooba.com
 **************************************************************************/
package com.yum.pms.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.yum.pms.dao.GoalSheet;
import com.yum.pms.dao.GoalSheetStatusWorkFlow;
import com.yum.pms.dao.GoalStatus;
import com.yum.pms.exception.YumPMSDataSaveException;
import com.yum.pms.utils.YumPmsConstants;

@Service
@SuppressWarnings("unchecked")
public class StatusWorkFlowService {

	private static final Logger LOGGER = LoggerFactory.getLogger(StatusWorkFlowService.class);

	public List<GoalSheetStatusWorkFlow> getWorkStatusByGoalSheetId(Integer goalSheetStatusId, SessionFactory sessionFactory) {

		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(GoalSheetStatusWorkFlow.class)
				.add(Restrictions.eq("goalSheetStatusId", goalSheetStatusId))
				.list();  
	}

	public GoalStatus getGoalStatusIdByGoalSheetId(Integer goalSheetId, SessionFactory sessionFactory) {

		GoalStatus goalSheetStatus = 
				(GoalStatus)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(GoalSheet.class)
				.setProjection(Projections.projectionList().add(Projections.property("goalStatus")))
				.add(Restrictions.eq(YumPmsConstants.GOAL_SHEET_ID, goalSheetId))
				.uniqueResult();  
		return null == goalSheetStatus ? new GoalStatus() : goalSheetStatus;
	}

	public String updateGoalSheetStatus(Integer goalSheetId, Integer statusId, String createdBy, SessionFactory sessionFactory) throws YumPMSDataSaveException {

		String msg = StringUtils.EMPTY;
		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
		GoalSheet passingGoalSheet = session.get(GoalSheet.class, goalSheetId);
		passingGoalSheet.setGoalStatus(new GoalStatus(statusId));  
		passingGoalSheet.setUpdatedBy(createdBy);
		try {
			session.merge(passingGoalSheet);
			YumHibernateUtilServices.commitTransaction(session);
			msg = YumPmsConstants.UPDATED_SUCESSFULLY;
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException("Failed to update Goal Sheet Status Values", e);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return msg;
	}

}
