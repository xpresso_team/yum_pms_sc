/******************************************************************** 
* Hibernate Services for Appraisal Period 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/


package com.yum.pms.service;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yum.pms.dao.AppraisalPeriod;
import com.yum.pms.exception.YumPMSDataSaveException;
import com.yum.pms.utils.YumPmsConstants;

@SuppressWarnings("unchecked")
@Service
public class AppraisalService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AppraisalService.class);
	
	@Autowired
	private SessionFactory sessionFactory;

	public AppraisalPeriod getDefaultAppraisalService() {

		AppraisalPeriod appraisalPeriod = 
				(AppraisalPeriod)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(AppraisalPeriod.class)
				.add(Restrictions.eq(YumPmsConstants.IS_DEFAULT, true))
				.uniqueResult();  

		return new AppraisalPeriod(appraisalPeriod.getAppraisalPeriodId(), appraisalPeriod.getPeriodDesc());
	}

	public AppraisalPeriod getAppraisalPeriodbyId(Integer appraisalPeriodId) {
		
		AppraisalPeriod appraisalPeriod = 
				(AppraisalPeriod)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(AppraisalPeriod.class)
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD_ID, appraisalPeriodId))
				.uniqueResult();  
		return appraisalPeriod == null ? new AppraisalPeriod() : appraisalPeriod;
	}

	public AppraisalPeriod saveAppraisalPeriod(AppraisalPeriod appraisalPeriod) throws YumPMSDataSaveException {
		
		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			appraisalPeriod = (AppraisalPeriod) session.merge(appraisalPeriod);
			YumHibernateUtilServices.commitTransaction(session);     
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException("Failed to save Appraisal Period", e);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return appraisalPeriod;
	}
	
	/**
	 * method name: getAppraisalPeriordList
	 * @return List of AppraisalPeriord model class
	 */
	public List<AppraisalPeriod> getAppraisalPeriordList() {		

		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(AppraisalPeriod.class)
				.add(Restrictions.eq(YumPmsConstants.ACTIVE, true))
				.list(); 
	}

	public AppraisalPeriod getAppraisalPeriodbyDescription(String appraisalPeriodDescription) {

		AppraisalPeriod appraisalPeriod = 
				(AppraisalPeriod)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(AppraisalPeriod.class)
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD_DESCRIPTION, appraisalPeriodDescription))
				.add(Restrictions.eq(YumPmsConstants.ACTIVE, true))
				.uniqueResult();  
		return appraisalPeriod == null ? new AppraisalPeriod() : appraisalPeriod;
	}
	
	public List<Object[]> yearConfigurationForBrandGoal(Integer apprId) {
		
		String sql = String.format(YumPmsConstants.SP_YEAR_CONFIG_FOR_BRAND_GOAL, apprId);
		SQLQuery query = YumHibernateUtilServices.getSession(sessionFactory).createSQLQuery(sql);
		return query.list();
	}

}
