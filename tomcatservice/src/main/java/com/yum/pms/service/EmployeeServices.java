/******************************************************************** 
* Hibernate Services for Employee 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.service;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yum.comp.service.CompensationDao;
import com.yum.pms.dao.AppraisalPeriod;
import com.yum.pms.dao.CompRscEmployee;
import com.yum.pms.dao.EmpToPmsRole;
import com.yum.pms.dao.EmployeeCategoryMaster;
import com.yum.pms.dao.EmployeeDesignationMaster;
import com.yum.pms.dao.EmployeeFunctionMaster;
import com.yum.pms.dao.EmployeeGradeMaster;
import com.yum.pms.dao.EmployeeHelpTipViewStatus;
import com.yum.pms.dao.EmployeeNew;
import com.yum.pms.dao.EmployeeSubFunctionMaster;
import com.yum.pms.dao.EmployeeYearWise;
import com.yum.pms.dao.EmployeeYearWiseNotEffective;
import com.yum.pms.dao.HelpTipDocumentMaster;
import com.yum.pms.domain.ClientEmployee;
import com.yum.pms.domain.EmployeeCurrentDetails;
import com.yum.pms.exception.CommonException;
import com.yum.pms.exception.YumPMSDataSaveException;
import com.yum.pms.utils.SendMail;
import com.yum.pms.utils.YumPmsConstants;
import com.yum.pms.utils.YumPmsUtils;

@Service
@SuppressWarnings("unchecked")
public class EmployeeServices {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeServices.class);

	@Autowired
	private SendMail mailSender;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private AppraisalService apprService;
	
	@Autowired
	private YumHibernateUtilServices util;
	
	@Autowired
	private CompensationDao compDao;
	
	private static final String ACTIVE_EMP_SQL = "SELECT DISTINCT e.emp_id FROM employee_yearwise e, (SELECT emp_id, MAX(CreatedDate) AS CreatedDate "
			+ "FROM employee_yearwise GROUP BY emp_id) u WHERE e.CreatedDate = u.CreatedDate AND u.emp_id = e.emp_id AND e.IsActive = 1 ";
	
	private static final String ACTIVE_EMP_IN_APPR_SQL = "SELECT DISTINCT e.emp_id FROM employee_yearwise e, "
			+ "(SELECT emp_id, MAX(CreatedDate) AS CreatedDate "
			+ "FROM employee_yearwise WHERE appraisal_period_id = :apprId GROUP BY emp_id) u "
			+ "WHERE e.CreatedDate = u.CreatedDate AND u.emp_id = e.emp_id AND e.IsActive = 1 ";

	// get List of employee ID and name against any manager ID
	@Transactional
	public Map<Integer, String> getEmployeeListByManagerId(Integer managerId, Integer apprId) {

		if(YumPmsUtils.isNullOrZero(apprId)) {
			apprId = apprService.getDefaultAppraisalService().getAppraisalPeriodId();
		}
		String sql = ACTIVE_EMP_IN_APPR_SQL + "AND e.manager_id = :managerId";
		List<Integer> empIds = YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(sql)
				.setInteger(YumPmsConstants.MANAGER_ID, managerId)
				.setInteger(YumPmsConstants.APPR_ID, apprId)
				.list();
		return createEmpIdNameMap(empIds);
	}

	public Map<Integer, String> getEmployeeListByManagerIdForNonDR(Integer managerId, Integer apprId)  {
		
		if(YumPmsUtils.isNullOrZero(apprId)) {
			apprId = apprService.getDefaultAppraisalService().getAppraisalPeriodId();
		}
		String sql = new StringBuffer("WITH t1(emp_id) AS (SELECT emp_id FROM employee_new WHERE emp_id = ")
				.append(managerId).append(" UNION ALL SELECT  t2.emp_id FROM ")
				.append("(SELECT *, ROW_NUMBER() OVER(PARTITION BY appraisal_period_id, emp_id ORDER BY CreatedDate DESC) AS RowID ")
				.append("FROM employee_yearwise WHERE appraisal_period_id = ").append(apprId)
				.append(") AS t2, t1 WHERE t1.emp_id = t2.manager_id AND t2.IsActive <> 0 AND t2.RowID = 1) ")
				.append("SELECT DISTINCT t1.emp_id FROM t1 WHERE t1.emp_id <> ").append(managerId).toString();
		List<Integer> empIds = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(sql)
				.list();
		return createEmpIdNameMap(empIds);
	}
	
	/**
	 * @param managerId
	 * @param sessionFactory
	 * @return
	 */
	public List<Integer> getNonDrEmpIdListByLtsId(Integer managerId, Integer apprId) {
		
		List<Integer> empIds = null;
		if(YumPmsUtils.isGreaterThanZero(managerId)) {
			empIds = getAllActiveEmpIdsByFunLtIdAndApprId(managerId, apprId);
		} else {
			empIds = getAllActiveEmpIdsByApprId(apprId);
		}
		return YumPmsUtils.getEmptyListIfNull(empIds);
	}
	
	// get Employee details against his user ID		
	@SuppressWarnings("null")
	public EmployeeCurrentDetails getEmployeeDetailsByUserId(String userId, Integer apprId,  String conditionType) {
		
		LOGGER.info("getEmployeeDetailsByUserId >>> userId = {} and apprId = {} and conditionType = {}", userId, apprId, conditionType);
		EmployeeCurrentDetails employee = new EmployeeCurrentDetails();
		EmployeeYearWise empYearWiseModel = new EmployeeYearWise();
		EmployeeNew empModel = new EmployeeNew();
		EmployeeHelpTipViewStatus employeeHelpTipViewStatusModel= null;
		HelpTipDocumentMaster helpTipDocMasterModel = null;
		CompRscEmployee employeeRscData =null;
		
		if(conditionType.equalsIgnoreCase(YumPmsConstants.USER_NAME)) {
			empModel = getEmployeeByEmail(userId);  
			if(empModel != null) {
				empYearWiseModel = getEmpYearWiseData(empModel.getEmpId(), apprId);
			}
		} else if(conditionType.equalsIgnoreCase(YumPmsConstants.EMPLOYEE_ID)) {
			empYearWiseModel = getEmpYearWiseData(Integer.parseInt(userId), apprId);
			empModel = getEmployeeById(Integer.parseInt(userId));
			
			employeeRscData = getEmpRscData(Integer.parseInt(userId), apprId);
			//Check and Set value when employee has no RSC date available in DB or not
			if(employeeRscData != null){
				employee.setRscId(employeeRscData.getRscId());
				employee.setRld(employeeRscData.getRscLeaveDate());
				employee.setRmd(employeeRscData.getRscMoveDate());
			}
		}
		
		if(null != empYearWiseModel.getHasReportee()){
			helpTipDocMasterModel = getHelpDocURL(apprId,empYearWiseModel.getHasReportee());
			if(helpTipDocMasterModel !=null){
				employee.setHelpTipDocumentMaster(helpTipDocMasterModel);
			}
			
			if(empYearWiseModel.getHasReportee()){
				employee.setNonMgrhelpTipDocumentMaster(getNonMgrHelpDocURL(apprId,empYearWiseModel.getHasReportee()));
			} 
		}
		
		
		//Get Employee data from employee_help_tip_view_status
		if(empModel!=null)
		employeeHelpTipViewStatusModel = getHelpTipViewStatus(empModel.getEmpId(),apprId);
		
		if(employeeHelpTipViewStatusModel != null){
			employee.setEmployeeHelpTipViewStatus(employeeHelpTipViewStatusModel);
		} else {
			EmployeeHelpTipViewStatus employeeHelpTipViewStatusData = new EmployeeHelpTipViewStatus();
			employeeHelpTipViewStatusData.setApprId(apprId);
			
			if(empModel!=null)
			employeeHelpTipViewStatusData.setEmpId(empModel.getEmpId());
			
			employeeHelpTipViewStatusData.setCreatedBy("sa");
			employeeHelpTipViewStatusData.setIsViewed(0);
			try {
				employeeHelpTipViewStatusData = addOrUpdateEmployeeViewRecord(employeeHelpTipViewStatusData);
				employee.setEmployeeHelpTipViewStatus(employeeHelpTipViewStatusData);
			} catch (YumPMSDataSaveException e) {
				LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
				throw new CommonException(e);
			}
		}
		
		
		if(empModel != null) {
			employee.setEmpId(empModel.getEmpId());
			employee.setEmpName(empModel.getEmpName());
			employee.setEmail(empModel.getEmail());
			employee.setDoj(empModel.getDoj());
			employee.setDol(empModel.getDol());
			employee.setSignatureFileName(StringUtils.substringAfter(empModel.getSignatureImagePath(), YumPmsConstants.STR_EXCLAMATION));
			employee.setIsActiveLt(isActiveLt(employee.getEmpId(), apprId));
			Integer roleId = getEmpRoleId(employee.getEmpId());
			employee.setIsAdmin(roleId != null && roleId.equals(1001));
		}
		if(empYearWiseModel != null) {
			setEmpYearWiseData(employee, empYearWiseModel);
		}
		EmployeeYearWiseNotEffective notEffectiveEmpData = getNotEffectiveEmpData(employee.getEmpId());
		if(notEffectiveEmpData != null && YumPmsUtils.isGreaterThanZero(notEffectiveEmpData.getEmpYearWiseId())) {
			employee.setNotEffectiveData(notEffectiveEmpData);
			Optional<Date> optional = YumPmsUtils.resolve(() -> notEffectiveEmpData.getEffectiveDateId().getSalaryDuration());
			employee.setEffectiveDate(optional.orElse(notEffectiveEmpData.getEffectiveDate()));
		}
		return employee;
	}

	
	public EmployeeHelpTipViewStatus addOrUpdateEmployeeViewRecord(EmployeeHelpTipViewStatus employeeHelpTipViewStatusModel) throws YumPMSDataSaveException {
		
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			employeeHelpTipViewStatusModel = (EmployeeHelpTipViewStatus) currentSession.merge(employeeHelpTipViewStatusModel);
			YumHibernateUtilServices.commitTransaction(currentSession);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to insert EmployeeYearWiseNotEffective", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return employeeHelpTipViewStatusModel;	
	}

	private EmployeeHelpTipViewStatus getHelpTipViewStatus(int empId,
			Integer apprId) {
		return 
				(EmployeeHelpTipViewStatus)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(EmployeeHelpTipViewStatus.class)
				.add(Restrictions.eq(YumPmsConstants.PARAM_EMP_ID, empId))
				.add(Restrictions.eq(YumPmsConstants.APPR_ID, apprId))
				.uniqueResult();
	}

	private HelpTipDocumentMaster getHelpDocURL(Integer apprId,
			Boolean hasReportee) {
		Criteria criteria = YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(HelpTipDocumentMaster.class);
		if(hasReportee) {
			criteria.add(Restrictions.eq(YumPmsConstants.IS_MANAGER, 1));
			criteria.add(Restrictions.eq(YumPmsConstants.IS_DEFAULT, true));
		} else {
			criteria.add(Restrictions.eq(YumPmsConstants.IS_MANAGER, 0));
			criteria.add(Restrictions.eq(YumPmsConstants.IS_DEFAULT, true));
		}
		criteria.add(Restrictions.eq(YumPmsConstants.APPR_ID, apprId));
		
		return (HelpTipDocumentMaster) criteria.uniqueResult();
	}
	
	private HelpTipDocumentMaster getNonMgrHelpDocURL(Integer apprId,
			Boolean hasReportee) {
		Criteria criteria = YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(HelpTipDocumentMaster.class);
		
			criteria.add(Restrictions.eq(YumPmsConstants.IS_MANAGER, 0));
			criteria.add(Restrictions.eq(YumPmsConstants.IS_DEFAULT, true));
			criteria.add(Restrictions.eq(YumPmsConstants.APPR_ID, apprId));
		
			return (HelpTipDocumentMaster) criteria.uniqueResult();
	}

	private CompRscEmployee getEmpRscData(Integer empId, Integer apprId) {
		LOGGER.info("empId = {} and apprId = {}", empId, apprId);
		return 
				(CompRscEmployee)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(CompRscEmployee.class)
				.add(Restrictions.eq(YumPmsConstants.EMPLOYEE_MODEL, new EmployeeNew(empId)))
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, new AppraisalPeriod(apprId)))
				.uniqueResult();
		
	}

	private void setEmpYearWiseData(EmployeeCurrentDetails employee, EmployeeYearWise empYearWiseModel) {
		
		employee.setHasReportee(empYearWiseModel.getHasReportee());
		EmployeeNew managerEmp = empYearWiseModel.getManager();
		if(managerEmp != null) {
			employee.setManagerId(managerEmp.getEmpId());
			employee.setManagerName(managerEmp.getEmpName());
		}
		EmployeeNew functionalLtEmp = empYearWiseModel.getFunctionalLt();
		if(functionalLtEmp != null) {
			employee.setFunctionalLtsId(functionalLtEmp.getEmpId());
			employee.setFunctionalLtsName(functionalLtEmp.getEmpName());
		}
		EmployeeGradeMaster empGrade = empYearWiseModel.getGradeMaster();
		if(empGrade != null) {
			employee.setGradeId(empGrade.getGradeId());
			employee.setGrade(empGrade.getGradeDesc());
		}
		EmployeeDesignationMaster empDesg = empYearWiseModel.getDesgMaster();
		if(empDesg != null) {
			employee.setDesignationId(empDesg.getDesignationId());
			employee.setDesignation(empDesg.getDesignation());
		}
		EmployeeFunctionMaster empFunction = empYearWiseModel.getFunctionMaster();
		if(empFunction != null) {
			employee.setFunctionId(empFunction.getFunctionId());
			employee.setFunctionName(empFunction.getFunctionName());
		}
		EmployeeSubFunctionMaster empSubFunction = empYearWiseModel.getSubFunctionMaster();
		if(empSubFunction != null) {
			employee.setSubFunctionId(empSubFunction.getSubFunctionId());
			employee.setSubFunctionName(empSubFunction.getSubFunctionName());
		}
		EmployeeCategoryMaster empCategory = empYearWiseModel.getCategoryMaster();
		if(empCategory != null) {
			employee.setCategoryId(empCategory.getCategoryId());
			employee.setCategoryName(empCategory.getCategoryName());
		}
		AppraisalPeriod appraisalPeriod = empYearWiseModel.getAppraisalPeriod();
		if(appraisalPeriod != null) {
			employee.setApprPeriodId(appraisalPeriod.getAppraisalPeriodId());
		}
		employee.setKfcAllocation(empYearWiseModel.getKfcAllocation());
		employee.setPhAllocation(empYearWiseModel.getPhAllocation());
		employee.setTbAllocation(empYearWiseModel.getTbAllocation());
		employee.setActive(empYearWiseModel.getActive());
	}
	
	public EmployeeYearWise getEmpYearWiseData(Integer empId, Integer apprId) {
		
		LOGGER.info("empId = {} and apprId = {}", empId, apprId);
		Criteria criteria = YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(EmployeeYearWise.class)
				.add(Restrictions.eq(YumPmsConstants.EMPLOYEE_MODEL, new EmployeeNew(empId)));
		if(YumPmsUtils.isGreaterThanZero(apprId)) {
			criteria.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, new AppraisalPeriod(apprId)));
		}
		List<EmployeeYearWise> empYserWiseList =
				criteria.addOrder(Order.desc(YumPmsConstants.CREATED_DATE))
				.list();
		if(CollectionUtils.isEmpty(empYserWiseList)) {
			return new EmployeeYearWise(false);
		}
		return empYserWiseList.get(0);
	}
	
	public EmployeeYearWiseNotEffective getNotEffectiveEmpData(Integer empId, Integer apprId) {

		LOGGER.info("getNotEffectiveEmpData() =====> empId = {} and apprId = {}", empId, apprId);
		return 
				(EmployeeYearWiseNotEffective)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(EmployeeYearWiseNotEffective.class)
				.add(Restrictions.eq(YumPmsConstants.EMP_ID, empId))
				.add(Restrictions.eq(YumPmsConstants.APPR_PERIOD_ID, apprId))
				.add(Restrictions.eq(YumPmsConstants.ACTIVE, true))
				.add(Restrictions.eq(YumPmsConstants.EFFECTIVE, false))
				.uniqueResult();
	}
	
	public EmployeeYearWiseNotEffective getNotEffectiveEmpData(Integer empId) {

		LOGGER.info("getNotEffectiveEmpData() =====> empId = {}", empId);
		EmployeeYearWiseNotEffective notEffectiveData = 
				(EmployeeYearWiseNotEffective)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(EmployeeYearWiseNotEffective.class)
				.add(Restrictions.eq(YumPmsConstants.EMP_ID, empId))
				.add(Restrictions.eq(YumPmsConstants.ACTIVE, true))
				.add(Restrictions.eq(YumPmsConstants.EFFECTIVE, false))
				.uniqueResult();
		return notEffectiveData == null ? new EmployeeYearWiseNotEffective() : notEffectiveData;
	}
	
	public EmployeeNew getEmployeeByEmail(String email) {

		if(StringUtils.isBlank(email)) {
			return new EmployeeNew();
		}
		EmployeeNew employee =  
				(EmployeeNew)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(EmployeeNew.class)
				.add(Restrictions.eq(YumPmsConstants.EMAIL, email))
				.uniqueResult(); 
		return employee == null ? new EmployeeNew() : employee;
	}
	
	public EmployeeNew getEmployeeById(Integer empId) {
		
		if(YumPmsUtils.isNullOrZero(empId)) {
			return new EmployeeNew();
		}
		EmployeeNew employee = YumHibernateUtilServices.getSession(sessionFactory).get(EmployeeNew.class, empId);
		return employee == null ? new EmployeeNew() : employee;
	}
	
	public boolean isActiveLt(Integer functionLtId, Integer apprId) {

		LOGGER.info("functionLtId = {} and apprId = {}", functionLtId, apprId);
		if(YumPmsUtils.isNullOrZero(functionLtId) || YumPmsUtils.isNullOrZero(apprId)) {
			return false;
		}
		String sql = ACTIVE_EMP_IN_APPR_SQL + "AND e.Functonal_LT = :functionalLTsID";
		List<Integer> empIds = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(sql)
				.setInteger(YumPmsConstants.APPR_ID, apprId)
				.setInteger(YumPmsConstants.FUNCTIONAL_LTS_ID, functionLtId)
				.list();
		return CollectionUtils.isNotEmpty(empIds);
	}
	
	public Integer getEmpRoleId(Integer empId) {

		return 
				(Integer) YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(EmpToPmsRole.class)
				.createAlias(YumPmsConstants.PMS_ROLE, YumPmsConstants.PMS)
				.createAlias(YumPmsConstants.EMPLOYEE_MODEL, YumPmsConstants.EMP)
				.setProjection(Projections.projectionList().add(Projections.property("pms.pmsRoleId")))
				.add(Restrictions.eq("emp.empId", empId))
				.uniqueResult();
	}


	public List<ClientEmployee> getAllEmployeeList(Integer empId, Integer apprId, Boolean isDeActive) {
		
		List<Integer> allEmpIds = null;
		if(!BooleanUtils.toBoolean(isDeActive)) {
			String sql = ACTIVE_EMP_IN_APPR_SQL + "AND e.emp_id <> :empId";
			allEmpIds = YumHibernateUtilServices.getSession(sessionFactory)
					.createSQLQuery(sql)
					.setInteger(YumPmsConstants.EMP_ID, empId)
					.setInteger(YumPmsConstants.APPR_ID, apprId)
					.list();
		} else {
			String sql = "SELECT DISTINCT emp_id FROM employee_yearwise WHERE emp_id <> :empId AND appraisal_period_id = :apprId AND IsActive <> 1";
			allEmpIds = YumHibernateUtilServices.getSession(sessionFactory)
					.createSQLQuery(sql)
					.setInteger(YumPmsConstants.EMP_ID, empId)
					.setInteger(YumPmsConstants.APPR_ID, apprId)
					.list();
		}
		if(CollectionUtils.isEmpty(allEmpIds)) {
			return Collections.emptyList();
		}
		return 
				createEmpIdNameMap(allEmpIds)
				.entrySet()
				.stream()
				.map(ClientEmployee :: new)
				.collect(Collectors.toList());
	}
	
	public List<Integer> getAllActiveEmpIdsByApprId(Integer apprId) {
		
		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(ACTIVE_EMP_IN_APPR_SQL)
				.setInteger(YumPmsConstants.APPR_ID, apprId)
				.list();
	}
	
	public List<Integer> getAllActiveEmpIdsByFunLtIdAndApprId(Integer functionalLTsID, Integer apprId) {
		
		String sql = ACTIVE_EMP_IN_APPR_SQL + "AND e.Functonal_LT = :functionalLTsID AND e.emp_id <> :functionalLTsID";
		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(sql)
				.setInteger(YumPmsConstants.APPR_ID, apprId)
				.setInteger(YumPmsConstants.FUNCTIONAL_LTS_ID, functionalLTsID)
				.list();
	}
	
	public List<Integer> getAllActiveEmpIds() {
		
		return YumHibernateUtilServices.getSession(sessionFactory).createSQLQuery(ACTIVE_EMP_SQL.trim()).list();
	}

	public List<EmployeeNew> getAllEmpList() {
		
		List<Integer> activeEmpIds = getAllActiveEmpIds();
		if(CollectionUtils.isEmpty(activeEmpIds)) {
			return Collections.emptyList();
		}
		List<EmployeeNew> allEmpList = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createQuery("SELECT e FROM EmployeeNew e WHERE e.empId IN(:empId)")
				.setParameterList(YumPmsConstants.EMP_ID, activeEmpIds)
				.list();
		return YumPmsUtils.getEmptyListIfNull(allEmpList);
	}

	//	=========================================Employee Management Services======================================================
	
	public List<Integer> getAllLtEmpIdsByApprId(Integer apprId) {
		
		String sql = ACTIVE_EMP_IN_APPR_SQL + "AND e.category_id = "
				+ "(SELECT category_id FROM employee_category_mstr WHERE category_name = 'LT')";
		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(sql)
				.setInteger(YumPmsConstants.APPR_ID, apprId)
				.list();
	}
	
	public Map<Integer, String> getFunctionLTList(Integer apprId) {
		
		List<Integer> ltEmpIds = getAllLtEmpIdsByApprId(apprId);
		if(CollectionUtils.isEmpty(ltEmpIds)) {
			return Collections.emptyMap();
		}
		return createEmpIdNameMap(ltEmpIds);
	}
	
	public Map<Integer, String> getManagerList(Integer apprId) {
		
		String sql = "SELECT CAST(emp_id AS INT) AS empId, CAST(emp_name AS VARCHAR(500)) AS empName "
				+ "FROM employee_new WHERE emp_id IN(SELECT DISTINCT e.manager_id FROM employee_yearwise e, "
				+ "(SELECT emp_id, MAX(CreatedDate) AS CreatedDate FROM employee_yearwise WHERE appraisal_period_id = :apprId GROUP BY emp_id) u "
				+ "WHERE e.CreatedDate = u.CreatedDate AND u.emp_id = e.emp_id AND e.IsActive = 1 AND e.manager_id IS NOT NULL)";
		List<Object[]> emps = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(sql)
				.setInteger(YumPmsConstants.APPR_ID, apprId)
				.list();
		Map<Integer, String> empIdNameMap = new HashMap<>();
		for(Object[] emp : emps) {
			int empId = YumPmsUtils.castToInt(emp[0]);
			String empName = YumPmsUtils.toString(emp[1]);
			empIdNameMap.put(empId, empName + " (" + empId + ")");
		}
		return empIdNameMap;
	}
	
	public Map<Integer, String> createEmpIdNameMap(Collection<Integer> empIds) {
		
		if(CollectionUtils.isEmpty(empIds)) {
			return Collections.emptyMap();
		}
		List<Object[]> emps = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createQuery("SELECT T.empId, T.empName FROM EmployeeNew T WHERE T.empId IN (:empId)")
				.setParameterList(YumPmsConstants.EMP_ID, empIds)
				.list();
		Map<Integer, String> empIdNameMap = new HashMap<>();
		for(Object[] emp : emps){
			int empId = YumPmsUtils.castToInt(emp[0]);
			String empName = YumPmsUtils.toString(emp[1]);
			empIdNameMap.put(empId, empName + " (" + empId + ")");
		}
		return empIdNameMap;
	}

	public EmployeeNew saveOrUpdateEmployeeDetails(EmployeeNew employee) throws YumPMSDataSaveException {

		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			employee = (EmployeeNew) currentSession.merge(employee);
			YumHibernateUtilServices.commitTransaction(currentSession);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to save or update Employee", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return employee;				
	}
	
	public EmployeeYearWiseNotEffective insertOrUpdateNotEffectiveEmpData(EmployeeYearWiseNotEffective empYearWise) throws YumPMSDataSaveException {
		
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			empYearWise = (EmployeeYearWiseNotEffective) currentSession.merge(empYearWise);
			YumHibernateUtilServices.commitTransaction(currentSession);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to insert EmployeeYearWiseNotEffective", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return empYearWise;	
	}
	
	public EmployeeYearWise insertEmployeeYearWiseData(EmployeeYearWise empYearWise) throws YumPMSDataSaveException {
		
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			Integer empYearWiseId = (Integer)currentSession.save(empYearWise);
			YumHibernateUtilServices.commitTransaction(currentSession);
			empYearWise.setEmpYearWiseId(empYearWiseId);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to insert EmployeeYearWise", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return empYearWise;	
	}

	public Boolean isDuplicateRecord(Integer empId, String emailId, SessionFactory sessionFactory) {

		Integer count = (Integer)YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery("SELECT COUNT(emp_id) FROM " + YumPmsConstants.EMPLOYEE_TABLE + " WHERE emp_id = :empId OR email = :email")
				.setInteger(YumPmsConstants.EMP_ID, empId)
				.setString(YumPmsConstants.EMAIL, StringUtils.trimToEmpty(emailId))
				.uniqueResult();
		return YumPmsUtils.isGreaterThanZero(count);
	}

	public boolean hasEmployeeGoalSheet(Integer empId) {

		Integer count = (Integer)YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery("SELECT COUNT(goal_sheet_id) FROM goal_sheet WHERE emp_id = :empId")
				.setInteger(YumPmsConstants.EMP_ID, empId)
				.uniqueResult();
		return YumPmsUtils.isGreaterThanZero(count);
	}

	public Boolean sendMailForNewJoinee(Integer empId, SessionFactory sessionFactory) {
		if(util.isActiveSp(sessionFactory, "sp_EmailNotification_EmpAdd_java")) {
		List<Object[]> results = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery("exec sp_EmailNotification_EmpAdd_java :param1")
				.setString(YumPmsConstants.PARAM_1, Integer.toString(empId))
				.list();
		return sendEmail(results, "new joinee");
		}else {
			return false;
		}
	}

	public Boolean sendMailForDesignationChange(Integer empId, String previousRoll, String presentRoll) {
		if(util.isActiveSp(sessionFactory, "sp_EmailNotification_EmpRoleChange_java")) {
			List<Object[]> results = 
					YumHibernateUtilServices.getSession(sessionFactory)
					.createSQLQuery("exec sp_EmailNotification_EmpRoleChange_java :param1, :param2, :param3, :param4")
					.setString(YumPmsConstants.PARAM_1, Integer.toString(empId))
					.setString(YumPmsConstants.PARAM_2, "designation")
					.setString(YumPmsConstants.PARAM_3, previousRoll)
					.setString(YumPmsConstants.PARAM_4, presentRoll)
					.list();
			return sendEmail(results, "Employee's Designation change");
		}else {
			return false;
		}
	}
	
	private boolean sendEmail(List<Object[]> results, String reason) {

		if(CollectionUtils.isEmpty(results)) {
			return false;
		}
		try {
			for (Object[] tuples : results) {
				String toMail = YumPmsUtils.toStringorNull(tuples[0]);
				String ccMail = YumPmsUtils.toStringorNull(tuples[1]);
				String subject = YumPmsUtils.toStringorNull(tuples[2]);
				String body = YumPmsUtils.toStringorNull(tuples[3]);										
				mailSender.sendMail(toMail, ccMail, subject, body);
				LOGGER.info("Mail content for {}>>>>>...: toMail : {} ccMail : {} subject : {} body : {}", reason, toMail, ccMail, subject, body);
			}
		} catch(Exception e) {
			LOGGER.error("Mail could not be sent for {}. Error :{}", reason, e.getMessage());
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return false;
		}
		return true;
	}
	
	public boolean deleteEmployee(Integer empId) {

		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
		try {
			String hql = "DELETE FROM EmployeeYearWise WHERE employee.empId = :empId";
			session.createQuery(hql).setInteger(YumPmsConstants.EMP_ID, empId).executeUpdate();
			YumHibernateUtilServices.commitTransaction(session);
			return true;
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(session);
			return false;
		}
	}
	
	/**
	 * @param gradeId
	 * @return
	 */
	public EmployeeGradeMaster getGradeMstrById(Integer gradeId) { 
		EmployeeGradeMaster gradeMstr = 
				(EmployeeGradeMaster)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(EmployeeGradeMaster.class)
				.add(Restrictions.eq(YumPmsConstants.GRADE_ID, gradeId))
				.uniqueResult();  
		return gradeMstr == null ? new EmployeeGradeMaster() : gradeMstr;
	}
	
	/**
	 * @param description
	 * @return
	 */
	public EmployeeGradeMaster getGradeMstrByDescription(String description) { 
		EmployeeGradeMaster gradeMstr = 
				(EmployeeGradeMaster)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(EmployeeGradeMaster.class)
				.add(Restrictions.eq(YumPmsConstants.GRADE_DESCRIPTION, description))
				.add(Restrictions.eq(YumPmsConstants.ACTIVE, true))
				.uniqueResult();  
		return gradeMstr == null ? new EmployeeGradeMaster() : gradeMstr;
	}
	
	public int getCurrentGradeDesc(Integer empId, Integer apprId) {

		String sql = "SELECT grade_description FROM employee_grade_mstr WHERE IsActive = 1 AND grade_id = "
				+ "(SELECT TOP 1 grade_id FROM employee_yearwise WHERE emp_id = :empId AND appraisal_period_id = :apprId ORDER BY CreatedDate DESC)";
		Object result = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(sql)
				.setInteger(YumPmsConstants.EMP_ID, empId)
				.setInteger(YumPmsConstants.APPR_ID, apprId)
				.uniqueResult();
		return YumPmsUtils.castToInt(result);
	}

}
