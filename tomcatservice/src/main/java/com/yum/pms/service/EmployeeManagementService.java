package com.yum.pms.service;

import java.util.Map;

import com.yum.pms.dao.EmployeeHelpTipViewStatus;
import com.yum.pms.domain.EmployeeCurrentDetails;
import com.yum.pms.exception.YumPMSDataSaveException;

public interface EmployeeManagementService {
	
	public Map<String, Object> getDropDownList(Integer apprId); 
	
	public EmployeeCurrentDetails saveOrUpdateEmployeeDetails(EmployeeCurrentDetails employee) throws YumPMSDataSaveException;
	
	public EmployeeCurrentDetails deleteEmployee(EmployeeCurrentDetails employee) throws YumPMSDataSaveException;
	
	public Map<Integer, String> getMasterTableDropdown(String dropdownName);
	
	public Map<Integer, String> saveEmployeeMasterData(String dropdownName, Map<Integer, String> requestData) throws YumPMSDataSaveException;
	
	public String deleteEmployeeMasterData(String dropdownName, Integer id);

	public EmployeeHelpTipViewStatus addOrUpdateEmployeeViewRecord(
			EmployeeHelpTipViewStatus employeeHelpTipViewData);
}
