/******************************************************************** 
* Hibernate Services for Employee Roles 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/


package com.yum.pms.service;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import com.yum.pms.dao.PmsRole;
import com.yum.pms.utils.YumPmsConstants;

//This class have all method of roles and permission 
@Service
public class PmsRoleServices {

	//	get Employee details against his user ID		
	public PmsRole getRoleDetailsByRoleId(String roleId, SessionFactory sessionFactory) {
		return 
				(PmsRole)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(PmsRole.class)
				.add(Restrictions.eq(YumPmsConstants.PMS_ROLE_ID, Integer.parseInt(roleId)))
				.uniqueResult();  
	}

}
 