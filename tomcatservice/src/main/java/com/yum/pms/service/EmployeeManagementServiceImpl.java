package com.yum.pms.service;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.yum.comp.service.CompensationDao;
import com.yum.pms.dao.AppraisalPeriod;
import com.yum.pms.dao.CompRscEmployee;
import com.yum.pms.dao.EmployeeCategoryMaster;
import com.yum.pms.dao.EmployeeDesignationMaster;
import com.yum.pms.dao.EmployeeFunctionMaster;
import com.yum.pms.dao.EmployeeGradeMaster;
import com.yum.pms.dao.EmployeeHelpTipViewStatus;
import com.yum.pms.dao.EmployeeNew;
import com.yum.pms.dao.EmployeeSubFunctionMaster;
import com.yum.pms.dao.EmployeeYearWise;
import com.yum.pms.dao.EmployeeYearWiseNotEffective;
import com.yum.pms.domain.EmployeeCurrentDetails;
import com.yum.pms.exception.CommonException;
import com.yum.pms.exception.YumPMSDataSaveException;
import com.yum.pms.utils.DateTimeUtils;
import com.yum.pms.utils.ResourceUtils;
import com.yum.pms.utils.YumPmsConstants;
import com.yum.pms.utils.YumPmsUtils;

@Service
public class EmployeeManagementServiceImpl implements EmployeeManagementService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeManagementServiceImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private EmployeeServices employeeService;
	
	@Autowired
	private YumHibernateUtilServices utilService;
	
	@Autowired
	private AppraisalService apprService;
	
	@Autowired
	private CompensationDao compDao;


	@Override
	public Map<String, Object> getDropDownList(Integer apprId) {
		
		Map<Integer, String> empFunctionMap = utilService.getMasterTableMap(EmployeeFunctionMaster.class, YumPmsConstants.FUNCTION_NAME);
		Map<Integer, String> empDesgMap = utilService.getMasterTableMap(EmployeeDesignationMaster.class, YumPmsConstants.DESIGNATION);
		Map<Integer, String> empCategoryMap = utilService.getMasterTableMap(EmployeeCategoryMaster.class, YumPmsConstants.CATEGORY_NAME);
		Map<Integer, String> empSubFunctionMap = utilService.getMasterTableMap(EmployeeSubFunctionMaster.class, YumPmsConstants.SUB_FUNCTION_NAME);
		Map<Integer, String> empGradeList = utilService.getMasterTableMap(EmployeeGradeMaster.class, YumPmsConstants.GRADE_DESC);
		if(YumPmsUtils.isNullOrZero(apprId)) {
			apprId = apprService.getDefaultAppraisalService().getAppraisalPeriodId();
		}
		Map<Integer, String> functionLTList = employeeService.getFunctionLTList(apprId);
		Map<Integer, String> managerList = employeeService.getManagerList(apprId);
		
		Map<String, Object> dropDownBox = new HashMap<>(7); 
		dropDownBox.put("functionLTList", functionLTList);
		dropDownBox.put("managerList", managerList);
		dropDownBox.put("catagoryList", empCategoryMap);
		dropDownBox.put("functionList", empFunctionMap);
		dropDownBox.put("subFunctionList", empSubFunctionMap);
		dropDownBox.put("designationList", empDesgMap);
		dropDownBox.put("gradeList", empGradeList);
		
		return dropDownBox;
	}
	
	@Override
	public EmployeeCurrentDetails saveOrUpdateEmployeeDetails(EmployeeCurrentDetails employeeDetails) throws YumPMSDataSaveException {
		
		Integer empId = employeeDetails.getEmpId();
		if(employeeDetails.getIsNew()) {
			if(employeeService.isDuplicateRecord(empId, employeeDetails.getEmail(), sessionFactory)) {
				employeeDetails.setOperationMessage("This is duplicate record. Please change your Employee-Id or Email-Id");
				employeeDetails.setOperationFlag(false);
				return employeeDetails;
			}
			EmployeeNew employee = new EmployeeNew(empId);
			employee.setEmpName(employeeDetails.getEmpName());
			employee.setEmail(employeeDetails.getEmail());
			employee.setDoj(employeeDetails.getDoj());
			employee.setDol(employeeDetails.getDol());
			employee.setCreatedBy(employeeDetails.getCreatedBy());
			employee.setCreatedDate(employeeDetails.getCreatedDate());
			employee.setBmuFranchise(YumPmsConstants.CAPITAL_LETTER_N);
			storeSignatureInServerPath(employee, employeeDetails);
			employeeService.saveOrUpdateEmployeeDetails(employee);
			
			insertYearWiseEmployee(employeeDetails, true);
			employeeDetails.setOperationMessage("New Employee join successfully"); 
			employeeDetails.setOperationFlag(true);
			employeeService.sendMailForNewJoinee(empId, sessionFactory);
		} else {
			EmployeeNew tempEmployee = employeeService.getEmployeeById(empId);
			tempEmployee.setEmpName(employeeDetails.getEmpName());
			tempEmployee.setEmail(employeeDetails.getEmail());
			tempEmployee.setDoj(employeeDetails.getDoj());
			tempEmployee.setDol(employeeDetails.getDol());
			tempEmployee.setUpdatedBy(employeeDetails.getUpdatedBy());
			tempEmployee.setUpdatedDate(employeeDetails.getUpdatedDate());
			// BMU Franchise operation start
			if(StringUtils.equalsIgnoreCase(employeeDetails.getBmuFranchise(), YumPmsConstants.CAPITAL_LETTER_Y) && tempEmployee.getDol() == null) {
				employeeDetails.setOperationMessage("BMU Franchise can not set to \"Yes\" without Date Of Leaving");
				employeeDetails.setOperationFlag(false);
				return employeeDetails;
			}
			checkSetBMUFranchise(employeeDetails, tempEmployee);
			// BMU Franchise operation end
			storeSignatureInServerPath(tempEmployee, employeeDetails);
			employeeService.saveOrUpdateEmployeeDetails(tempEmployee);
			
			EmployeeYearWise empYearWise = employeeService.getEmpYearWiseData(empId, employeeDetails.getApprPeriodId());
			int prevDesgId = 0;
			String prevDesg = StringUtils.EMPTY;
			if(empYearWise.getDesgMaster() != null) {
				prevDesgId = YumPmsUtils.castToInt(empYearWise.getDesgMaster().getDesignationId());
				prevDesg = StringUtils.trimToEmpty(empYearWise.getDesgMaster().getDesignation());
			}
			int currentDesgId = employeeDetails.getDesignationId() != null ? employeeDetails.getDesignationId().intValue() : 0;
			
			insertYearWiseEmployee(employeeDetails, false);
			employeeDetails.setOperationMessage("Record updated sucessfully"); 
			employeeDetails.setOperationFlag(true);
			
			if(prevDesgId != currentDesgId) {
				employeeService.sendMailForDesignationChange(empId, prevDesg, employeeDetails.getDesignation());
			}
		}
		EmployeeCurrentDetails result = 
				employeeService.getEmployeeDetailsByUserId(empId.toString(), employeeDetails.getApprPeriodId(), YumPmsConstants.EMPLOYEE_ID);
		result.setOperationMessage(employeeDetails.getOperationMessage());
		result.setOperationFlag(employeeDetails.getOperationFlag());
		
		//Save or Update RSC value in DB
		CompRscEmployee employeeUpdatedData = saveRscDate(employeeDetails);
		result.setRmd(employeeUpdatedData.getRscMoveDate());
		result.setRld(employeeUpdatedData.getRscLeaveDate());
		
		return result;
	}

	private void checkSetBMUFranchise(EmployeeCurrentDetails employeeDetails,
			EmployeeNew tempEmployee) {
		if(StringUtils.equalsIgnoreCase(employeeDetails.getBmuFranchise(), YumPmsConstants.CAPITAL_LETTER_Y) || 
				StringUtils.equalsIgnoreCase(employeeDetails.getBmuFranchise(), YumPmsConstants.CAPITAL_LETTER_N)) {
			tempEmployee.setBmuFranchise(StringUtils.upperCase(employeeDetails.getBmuFranchise()));
		} else {
			tempEmployee.setBmuFranchise(YumPmsConstants.CAPITAL_LETTER_N);
		}
	}

	@Override
	public EmployeeCurrentDetails deleteEmployee(EmployeeCurrentDetails employeeDetails) throws YumPMSDataSaveException {
		
		boolean delFlag = false;
		Integer empId = employeeDetails.getEmpId();
		boolean isGoalSheetExist = employeeService.hasEmployeeGoalSheet(empId);
		if(!isGoalSheetExist) {
			// Hard delete from employee table and employee_yearwise table
			delFlag = employeeService.deleteEmployee(empId);
			employeeDetails.setOperationFlag(delFlag);
			employeeDetails.setOperationMessage(delFlag ? "Employee Deleted Sucessfully" : StringUtils.EMPTY);
		} else {
			try {
				EmployeeNew employee = employeeService.getEmployeeById(empId);
				employee.setDol(employeeDetails.getDol());
				employee.setUpdatedBy(employeeDetails.getUpdatedBy());
				employee.setUpdatedDate(employeeDetails.getUpdatedDate());
				if(StringUtils.equalsIgnoreCase(employeeDetails.getBmuFranchise(), YumPmsConstants.CAPITAL_LETTER_Y) && employee.getDol() == null) {
					employeeDetails.setOperationMessage("BMU Franchise can not set to \"Yes\" without Date Of Leaving");
					employeeDetails.setOperationFlag(delFlag);
					return employeeDetails;
				}
				checkSetBMUFranchise(employeeDetails, employee);
				employeeService.saveOrUpdateEmployeeDetails(employee);
				
				EmployeeYearWise empYearWise = employeeService.getEmpYearWiseData(empId, employeeDetails.getApprPeriodId());
				empYearWise.setActive(false);
				empYearWise.setCreatedDate(DateTimeUtils.now());
				empYearWise.setCreatedBy(employeeDetails.getUpdatedBy());
				employeeService.insertEmployeeYearWiseData(empYearWise); 
				delFlag = true;
				employeeDetails.setOperationMessage("Employee Deactivated Sucessfully"); 
			} catch(Exception e) {
				delFlag = false;
			}
			employeeDetails.setOperationFlag(delFlag);
		}
		return employeeDetails;
	}

	@Override
	public Map<Integer, String> getMasterTableDropdown(String dropdownName) {

		Class<?> clazz = null;
		String descFieldName = null;
		switch(StringUtils.upperCase(StringUtils.trimToEmpty(dropdownName))) {
		case YumPmsConstants.EMP_DESIGNATION_UPPER_CASE:
			clazz = EmployeeDesignationMaster.class;
			descFieldName = YumPmsConstants.DESIGNATION;
			break;
		case YumPmsConstants.EMP_FUNCTION_UPPER_CASE:
			clazz = EmployeeFunctionMaster.class;
			descFieldName = YumPmsConstants.FUNCTION_NAME;
			break;
		case YumPmsConstants.EMP_SUB_FUNCTION_UPPER_CASE:
			clazz = EmployeeSubFunctionMaster.class;
			descFieldName = YumPmsConstants.SUB_FUNCTION_NAME;
			break;
		case YumPmsConstants.EMP_CATEGORY_UPPER_CASE:
			clazz = EmployeeCategoryMaster.class;
			descFieldName = YumPmsConstants.CATEGORY_NAME;
			break;
		default:
			// default for "EMP_GRADE" case
			clazz = EmployeeGradeMaster.class;
			descFieldName = YumPmsConstants.GRADE_DESC;
			break;
		}
		Map<Integer, String> result = utilService.getMasterTableMap(clazz, descFieldName);
		return MapUtils.isEmpty(result) ? Collections.emptyMap() : result;
	}
	
	/**
	 * Create a folder is not exist in user home directory with name 'UserSignature' and place the uploaded file in it.
	 * Validate the file type and size according to the configuration in 'application.properties' file.
	 * The uploaded file rename using certain criteria in order to make this name as unique.
	 * Set two fields signatureImagePath and signatureImageName on employee object.
	 * @param employee
	 * @param employeeDetails
	 */
	private void storeSignatureInServerPath(EmployeeNew employee, EmployeeCurrentDetails employeeDetails) {

		MultipartFile signatureImage = employeeDetails.getSignatureImage();
		if(signatureImage == null || signatureImage.isEmpty()) {
			return;
		}
		String originalFilename = signatureImage.getOriginalFilename();
		Set<String> imageTypes = ResourceUtils.getEmpSignatureImageTypes();
		String extension = FilenameUtils.getExtension(originalFilename);
		if(CollectionUtils.isNotEmpty(imageTypes) && !imageTypes.contains(extension)) {
			throw new CommonException("Unsupported Signature Image File Extension");
		}
		try {
			byte[] content = employeeDetails.getSignatureImage().getBytes();
			String size = FileUtils.byteCountToDisplaySize(content.length);
			LOGGER.info("Original Uploded File Size = {}", size);
			long maxSignImgSize = ResourceUtils.getMaxSignatureImageSIze();
			if(content.length > maxSignImgSize) {
				throw new CommonException("Maximum File Size Limit Exceed. You can provide maximun " + maxSignImgSize + " KB");
			}
			
			String uniqueFileName = YumPmsUtils.buildSignatureFileName(employeeDetails.getEmpName(), employeeDetails.getEmpId(), originalFilename);
			employee.setSignatureImagePath(ResourceUtils.createUserSignature(uniqueFileName, content));
			employeeDetails.setSignatureFileName(originalFilename);
		} catch (IOException e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			throw new CommonException(e);
		}
	}
	
	/**
	 * Insert or update into "employee_yearwise" or "employee_yearwise_not_effective" table
	 * @param employeeDetails
	 * @param isNewEmp
	 * @throws YumPMSDataSaveException
	 */
	private void insertYearWiseEmployee(EmployeeCurrentDetails employeeDetails, boolean isNewEmp) throws YumPMSDataSaveException {
		
		EmployeeYearWise employeeYearWise = prepareEmpYearWise(employeeDetails);
		Date doj = employeeDetails.getDoj();
		Date effectiveDate = employeeDetails.getEffectiveDate();
		if(DateTimeUtils.isFutureDate(doj) || DateTimeUtils.isFutureDate(effectiveDate)) { // If it is a future entry, 
			// Then it will save in 'employee_yearwise_not_effective' table 
			employeeService.insertOrUpdateNotEffectiveEmpData(prepareEmpYearWiseNotEffective(employeeDetails, employeeYearWise));
		} else { 
			if(DateTimeUtils.isPastDate(doj) && isNewEmp) {//If user is inserting a new employee after the original DOJ(back date entry), 
				// Then CreatedDate = DOJ in employee_yearwise table
				employeeYearWise.setCreatedDate(employeeDetails.getDoj());
			}
			employeeService.insertEmployeeYearWiseData(employeeYearWise);
		}
	}
	
	/**
	 * prepare EmployeeYearWiseNotEffective object from EmployeeCurrentDetails object which is getting from client side
	 * @param employeeDetails
	 * @return EmployeeYearWiseNotEffective
	 */
	private EmployeeYearWiseNotEffective prepareEmpYearWiseNotEffective(EmployeeCurrentDetails employeeDetails, EmployeeYearWise employeeYearWise) {
		
		EmployeeYearWiseNotEffective notEffectiveData = employeeDetails.getNotEffectiveData();
		if(notEffectiveData == null || YumPmsUtils.isLessThanZero(notEffectiveData.getEmpId()) 
				|| YumPmsUtils.isLessThanZero(notEffectiveData.getApprPeriodId())) {
			throw new CommonException(YumPmsConstants.INVALID_REQUEST_DATA);
		}
		EmployeeYearWiseNotEffective savedNotEffectiveData = employeeService.getNotEffectiveEmpData(notEffectiveData.getEmpId());
		if(savedNotEffectiveData.getEmpYearWiseId() == null) {
			savedNotEffectiveData.setCreatedDate(DateTimeUtils.now());
			savedNotEffectiveData.setCreatedBy(employeeDetails.getCreatedBy());
		}
		savedNotEffectiveData.setUpdatedDate(DateTimeUtils.now());
		savedNotEffectiveData.setUpdatedBy(employeeDetails.getCreatedBy());
		savedNotEffectiveData.setEmpId(notEffectiveData.getEmpId());
		savedNotEffectiveData.setManagerId(notEffectiveData.getManagerId());
		savedNotEffectiveData.setFunctionalLtsId(notEffectiveData.getFunctionalLtsId());
		savedNotEffectiveData.setGradeId(YumPmsUtils.isGreaterThanZero(notEffectiveData.getGradeId()) 
				? notEffectiveData.getGradeId() : employeeYearWise.getGradeMaster().getGradeId());
		savedNotEffectiveData.setDesignationId(YumPmsUtils.isGreaterThanZero(notEffectiveData.getDesignationId()) 
				? notEffectiveData.getDesignationId() : employeeYearWise.getDesgMaster().getDesignationId());
		savedNotEffectiveData.setFunctionId(YumPmsUtils.isGreaterThanZero(notEffectiveData.getFunctionId()) 
				? notEffectiveData.getFunctionId() : employeeYearWise.getFunctionMaster().getFunctionId());
		savedNotEffectiveData.setSubFunctionId(YumPmsUtils.isGreaterThanZero(notEffectiveData.getSubFunctionId()) 
				? notEffectiveData.getSubFunctionId() : employeeYearWise.getSubFunctionMaster().getSubFunctionId());
		savedNotEffectiveData.setCategoryId(YumPmsUtils.isGreaterThanZero(notEffectiveData.getCategoryId()) 
				? notEffectiveData.getCategoryId() : employeeYearWise.getCategoryMaster().getCategoryId());
		AppraisalPeriod apprPeriod = 
				apprService.getAppraisalPeriodbyDescription(String.valueOf(DateTimeUtils.getYear(employeeDetails.getEffectiveDate())));
		savedNotEffectiveData.setApprPeriodId(apprPeriod.getAppraisalPeriodId() != null 
				? apprPeriod.getAppraisalPeriodId() : notEffectiveData.getApprPeriodId());
		savedNotEffectiveData.setKfcAllocation(notEffectiveData.getKfcAllocation());
		savedNotEffectiveData.setPhAllocation(notEffectiveData.getPhAllocation());
		savedNotEffectiveData.setTbAllocation(notEffectiveData.getTbAllocation());
		savedNotEffectiveData.setEffectiveDate(employeeDetails.getEffectiveDate());
		return savedNotEffectiveData;
	}
	
	/**
	 * Prepare EmployeeYearWise object from EmployeeCurrentDetails object which is getting from client
	 * @param empDetails
	 * @return EmployeeYearWise
	 * @throws YumPMSDataSaveException
	 */
	private EmployeeYearWise prepareEmpYearWise(EmployeeCurrentDetails empDetails) throws YumPMSDataSaveException {
		
		EmployeeYearWise empYearWise = new EmployeeYearWise();
		empYearWise.setHasReportee(empDetails.getHasReportee());
		empYearWise.setEmployee(new EmployeeNew(empDetails.getEmpId()));
		empYearWise.setCreatedBy(empDetails.getCreatedBy());
		empYearWise.setCreatedDate(DateTimeUtils.now());
		if(YumPmsUtils.isGreaterThanZero(empDetails.getApprPeriodId())) {
			empYearWise.setAppraisalPeriod(new AppraisalPeriod(empDetails.getApprPeriodId()));
		} else {
			empYearWise.setAppraisalPeriod(new AppraisalPeriod(apprService.getDefaultAppraisalService().getAppraisalPeriodId()));
		}
		if(YumPmsUtils.isGreaterThanZero(empDetails.getCategoryId())) {
			empYearWise.setCategoryMaster(new EmployeeCategoryMaster(empDetails.getCategoryId()));
		} else if(StringUtils.isNotBlank(empDetails.getCategoryName())) {
			empYearWise.setCategoryMaster(utilService.saveOrUpdate(new EmployeeCategoryMaster(empDetails.getCategoryName())));
		}
		if(YumPmsUtils.isGreaterThanZero(empDetails.getDesignationId())) {
			empYearWise.setDesgMaster(new EmployeeDesignationMaster(empDetails.getDesignationId()));
		} else if(StringUtils.isNotBlank(empDetails.getDesignation())) {
			empYearWise.setDesgMaster(utilService.saveOrUpdate(new EmployeeDesignationMaster(empDetails.getDesignation())));
		}
		if(YumPmsUtils.isGreaterThanZero(empDetails.getFunctionalLtsId())) {
			empYearWise.setFunctionalLt(new EmployeeNew(empDetails.getFunctionalLtsId()));
		}
		if(YumPmsUtils.isGreaterThanZero(empDetails.getFunctionId())) {
			empYearWise.setFunctionMaster(new EmployeeFunctionMaster(empDetails.getFunctionId()));
		} else if(StringUtils.isNotBlank(empDetails.getFunctionName())) {
			empYearWise.setFunctionMaster(utilService.saveOrUpdate(new EmployeeFunctionMaster(empDetails.getFunctionName())));
		}
		if(YumPmsUtils.isGreaterThanZero(empDetails.getSubFunctionId())) {
			empYearWise.setSubFunctionMaster(new EmployeeSubFunctionMaster(empDetails.getSubFunctionId()));
		} else if(StringUtils.isNotBlank(empDetails.getSubFunctionName())) {
			empYearWise.setSubFunctionMaster(utilService.saveOrUpdate(new EmployeeSubFunctionMaster(empDetails.getSubFunctionName())));
		}
		if(YumPmsUtils.isGreaterThanZero(empDetails.getGradeId())) {
			empYearWise.setGradeMaster(new EmployeeGradeMaster(empDetails.getGradeId()));
		} else if(StringUtils.isNotBlank(empDetails.getGrade())) {
			empYearWise.setGradeMaster(utilService.saveOrUpdate(new EmployeeGradeMaster(empDetails.getGrade())));
		}
		if(YumPmsUtils.isGreaterThanZero(empDetails.getManagerId())) {
			empYearWise.setManager(new EmployeeNew(empDetails.getManagerId()));
		}
		empYearWise.setKfcAllocation(empDetails.getKfcAllocation());
		empYearWise.setPhAllocation(empDetails.getPhAllocation());
		empYearWise.setTbAllocation(empDetails.getTbAllocation());
		return empYearWise;
	}
	
	/* save Employee Master table Data like desg, function, sub-function, category and grade
	 * @param dropdownName - like "emp_designation", "emp_grade", "emp_function", "emp_sub_function" and "emp_category" 
	 * @param requestData - The request data should be in this { "key1" : "value1", "key2" : "value3" } format
	 */
	@Override
	public Map<Integer, String> saveEmployeeMasterData(String dropdownName, Map<Integer, String> requestData) throws YumPMSDataSaveException {
		
		if(StringUtils.isBlank(dropdownName) || MapUtils.isEmpty(requestData)) {
			throw new CommonException("Invalid Request data. For this request both dropdown name and dropdown value are required");
		}
		Class<?> clazz = null;
		String descFieldName = null;
		switch(StringUtils.upperCase(StringUtils.trimToEmpty(dropdownName))) {
		case YumPmsConstants.EMP_DESIGNATION_UPPER_CASE:
			clazz = EmployeeDesignationMaster.class;
			descFieldName = YumPmsConstants.DESIGNATION;
			break;
		case YumPmsConstants.EMP_FUNCTION_UPPER_CASE:
			clazz = EmployeeFunctionMaster.class;
			descFieldName = YumPmsConstants.FUNCTION_NAME;
			break;
		case YumPmsConstants.EMP_SUB_FUNCTION_UPPER_CASE:
			clazz = EmployeeSubFunctionMaster.class;
			descFieldName = YumPmsConstants.SUB_FUNCTION_NAME;
			break;
		case YumPmsConstants.EMP_CATEGORY_UPPER_CASE:
			clazz = EmployeeCategoryMaster.class;
			descFieldName = YumPmsConstants.CATEGORY_NAME;
			break;
		case YumPmsConstants.EMP_GRADE_UPPER_CASE:
			clazz = EmployeeGradeMaster.class;
			descFieldName = YumPmsConstants.GRADE_DESC;
			break;
		default:
			break;
		}
		return utilService.saveMasterTableMap(clazz, descFieldName, requestData);
	}
	
	/* Delete an employee master table data by it's primary key
	 * @param dropdownName - like "emp_designation", "emp_grade", "emp_function", "emp_sub_function" and "emp_category"
	 * @param id - primary key
	 */
	@Override
	public String deleteEmployeeMasterData(String dropdownName, Integer id) {
		
		if(StringUtils.isBlank(dropdownName) || YumPmsUtils.isLessThanOrEqual(id)) {
			throw new CommonException("Invalid Request data. For this request both dropdown name and id value are required");
		}
		Class<?> clazz = null;
		switch(StringUtils.upperCase(StringUtils.trimToEmpty(dropdownName))) {
		case YumPmsConstants.EMP_DESIGNATION_UPPER_CASE:
			clazz = EmployeeDesignationMaster.class;
			break;
		case YumPmsConstants.EMP_FUNCTION_UPPER_CASE:
			clazz = EmployeeFunctionMaster.class;
			break;
		case YumPmsConstants.EMP_SUB_FUNCTION_UPPER_CASE:
			clazz = EmployeeSubFunctionMaster.class;
			break;
		case YumPmsConstants.EMP_CATEGORY_UPPER_CASE:
			clazz = EmployeeCategoryMaster.class;
			break;
		case YumPmsConstants.EMP_GRADE_UPPER_CASE:
			clazz = EmployeeGradeMaster.class;
			break;
		default:
			break;
		}
		boolean isDelete = utilService.deleteMasterTableById(clazz, id);
		return isDelete ? YumPmsConstants.SUCCESS_DELETE_MSG : YumPmsConstants.ERROR_MSG_TRY_AGAIN;
	}
	
	private CompRscEmployee saveRscDate(EmployeeCurrentDetails employeeDetails) throws YumPMSDataSaveException {
		CompRscEmployee compRscEmployee = null;
		if(YumPmsUtils.isGreaterThanZero(employeeDetails.getRscId())) {
			Optional<CompRscEmployee> optional = compDao.findById(CompRscEmployee.class, employeeDetails.getRscId());
			if(optional.isPresent()) {
				compRscEmployee = optional.get();
			}
		}
		if(compRscEmployee == null) {
			compRscEmployee = new CompRscEmployee();
		}
		compRscEmployee.setRscMoveDate(employeeDetails.getRmd());
		compRscEmployee.setRscLeaveDate(employeeDetails.getRld());
		compRscEmployee.setEmployee(new EmployeeNew(employeeDetails.getEmpId()));
		compRscEmployee.setAppraisalPeriod(new AppraisalPeriod(employeeDetails.getApprPeriodId()));
		CompRscEmployee rscEmp = compDao.saveOrUpdate(compRscEmployee)
				.orElseThrow(() -> new CommonException("Unable to save or update CompRscEmployee data"));
		
		employeeDetails.setRscId(rscEmp.getRscId());
		
		return rscEmp;
		
	}

	@Override
	public EmployeeHelpTipViewStatus addOrUpdateEmployeeViewRecord(
			EmployeeHelpTipViewStatus employeeHelpTipViewData) {
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			employeeHelpTipViewData = (EmployeeHelpTipViewStatus) currentSession.merge(employeeHelpTipViewData);
			YumHibernateUtilServices.commitTransaction(currentSession);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			try {
				throw new YumPMSDataSaveException("Failed to insert EmployeeYearWiseNotEffective", e);
			} catch (YumPMSDataSaveException e1) {
				LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e1);
				throw new CommonException(e1);
			}
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return employeeHelpTipViewData;
	}

}
