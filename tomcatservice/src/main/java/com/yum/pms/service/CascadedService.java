/******************************************************************** 
* Hibernate Services for Cascade 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/


package com.yum.pms.service;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.yum.pms.dao.Cascaded;
import com.yum.pms.dao.CasecadeEmp;
import com.yum.pms.dao.EmployeeNew;
import com.yum.pms.dao.GoalSection;
import com.yum.pms.exception.YumPMSDataSaveException;
import com.yum.pms.utils.YumPmsConstants;

@Service
@SuppressWarnings("unchecked")
public class CascadedService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CascadedService.class);

	public List<Cascaded> getCascadedDetailsByGoalSection(GoalSection goalSection, SessionFactory sessionFactory) {
		
		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(Cascaded.class)
				.add(Restrictions.eq(YumPmsConstants.GOAL_SECTION, goalSection))
				.list();
	}

	public Cascaded getCascadedById(int cascadeId, SessionFactory sessionFactory) {
		
		Cascaded cascaded = 
				(Cascaded)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(Cascaded.class)
				.add(Restrictions.eq(YumPmsConstants.CASCADE_ID, cascadeId))
				.uniqueResult();
		return cascaded == null ? new Cascaded() : cascaded;
	}
	
	public CasecadeEmp getCascadedEmpById(Cascaded cascaded, EmployeeNew employee, SessionFactory sessionFactory) {
		
		CasecadeEmp cascadedEmp = 
				(CasecadeEmp)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(CasecadeEmp.class)
				.add(Restrictions.eq(YumPmsConstants.CASECADED, cascaded))
				.add(Restrictions.eq(YumPmsConstants.EMPLOYEE_MODEL, employee))
				.uniqueResult();
		return cascadedEmp == null ? new CasecadeEmp() : cascadedEmp;
	}

	public List<CasecadeEmp> getCascadedEmpListbyCascadeId(Cascaded cascaded, SessionFactory sessionFactory) {

		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(CasecadeEmp.class)
				.add(Restrictions.eq(YumPmsConstants.CASECADED, cascaded))
				.list(); 
	}
	
	public Cascaded saveCascadedData(Cascaded cascaded, SessionFactory sessionFactory) throws YumPMSDataSaveException {
		
		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		  try{
		    cascaded = (Cascaded) session.merge(cascaded);
		    YumHibernateUtilServices.commitTransaction(session);
		  } catch (Exception e) {
		    LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
		    YumHibernateUtilServices.rollbackTransaction(session);
		    throw new YumPMSDataSaveException("Failed to save Cascade", e);
		  } finally {
			  YumHibernateUtilServices.clearSession(session);
		  }
		return cascaded;
	}

	public CasecadeEmp saveCascadedEmp(CasecadeEmp cascadedEmp, SessionFactory sessionFactory) throws YumPMSDataSaveException {

		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try{
			cascadedEmp = (CasecadeEmp) session.merge(cascadedEmp);
			YumHibernateUtilServices.commitTransaction(session);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException("Failed to save Employee Cascade", e);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return cascadedEmp;
	}

	public void deleteCascadedEmps(List<CasecadeEmp> deletedCascadedEmpList, SessionFactory sessionFactory) throws YumPMSDataSaveException {
		
		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			for(CasecadeEmp deleteCascadedEmp : deletedCascadedEmpList) {			    
				deleteById(CasecadeEmp.class, deleteCascadedEmp.getCasecadedEmpEventId(), session);
			}
			YumHibernateUtilServices.commitTransaction(session);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException("Failed to Remove Cascaded Employee", e);
		} finally {
			YumHibernateUtilServices.clearSession(session);		    
		}
	}

	public void deleteCascade(Integer cascadeid, SessionFactory sessionFactory) throws YumPMSDataSaveException {


		Cascaded cascaded = 
				(Cascaded)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(Cascaded.class)
				.add(Restrictions.eq(YumPmsConstants.CASCADE_ID, cascadeid))
				.uniqueResult();

		List<CasecadeEmp> cascadedEmpList = getCascadedEmpListbyCascadeId(cascaded, sessionFactory);
		deleteCascadedEmps(cascadedEmpList, sessionFactory);

		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			deleteById(Cascaded.class, cascadeid, session);
			YumHibernateUtilServices.commitTransaction(session);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException("Failed to Remove Cascade", e);
		} finally {
			YumHibernateUtilServices.clearSession(session);		    
		}
	}
	
	private <T> boolean deleteById(Class<T> type, Serializable id, Session session) {
	    
		T persistentInstance = session.load(type, id);
	    if (persistentInstance != null) {
	        session.delete(persistentInstance);
	        return true;
	    }
	    return false;
	}

	public CasecadeEmp getReporteeCascadedInfoByCascadeId(Cascaded cascaded, EmployeeNew reporteeEmployee, SessionFactory sessionFactory) {

		return 
				(CasecadeEmp)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(CasecadeEmp.class)
				.add(Restrictions.eq(YumPmsConstants.CASECADED, cascaded))
				.add(Restrictions.eq(YumPmsConstants.EMPLOYEE_MODEL, reporteeEmployee))
				.uniqueResult(); 
	}

	public GoalSection getGoalSectionByCascadedID(int cascadeId, SessionFactory sessionFactory) {
		
		return 
				(GoalSection)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(Cascaded.class)
				.setProjection(Projections.projectionList()
						.add(Projections.property(YumPmsConstants.GOAL_SECTION)))
				.add(Restrictions.eq(YumPmsConstants.CASCADE_ID, cascadeId))
				.uniqueResult(); 
	}
}
