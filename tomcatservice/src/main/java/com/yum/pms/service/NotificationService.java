/******************************************************************** 
* Hibernate Notification Services 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.service;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.yum.pms.dao.Notification;
import com.yum.pms.exception.YumPMSDataSaveException;
import com.yum.pms.utils.YumPmsConstants;
import com.yum.pms.utils.YumPmsUtils;

@Service
@SuppressWarnings("unchecked")
public class NotificationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationService.class);

	public List<Notification> getNotificationList(SessionFactory sessionFactory) {

		List<Notification> notificationList = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(Notification.class)
				.list();
		return YumPmsUtils.getEmptyListIfNull(notificationList);
	}

	public Notification saveNotificationSettingValue(Notification notificationSetting, SessionFactory sessionFactory) throws YumPMSDataSaveException {

		LOGGER.info("incoming NotificationSettings======>>>>> {}, {}, {}", notificationSetting.getNotificationId(), 
				notificationSetting.getSettingsValue(), notificationSetting.getNotificationName());
		Notification notification  = 
				(Notification)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(Notification.class)
				.add(Restrictions.eq(YumPmsConstants.NOTIFICATION_ID, notificationSetting.getNotificationId()))
				.uniqueResult();
		notification.setSettingsValue(notificationSetting.getSettingsValue());
		if(StringUtils.hasText(notificationSetting.getUpdatedBy())) {
			notification.setUpdatedBy(notificationSetting.getUpdatedBy());
		} else {
			notification.setCreatedBy(notificationSetting.getCreatedBy());
		}
		Session session = null;
		try {
			session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
			notificationSetting = (Notification) session.merge(notification);
			YumHibernateUtilServices.commitTransaction(session);     
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException("Failed to save Notification Settings");
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return notificationSetting;
	}

}
