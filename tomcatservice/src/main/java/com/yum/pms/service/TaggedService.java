/******************************************************************** 
* Hibernate Services for Tagging 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.service;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.yum.pms.dao.EmployeeNew;
import com.yum.pms.dao.GoalSection;
import com.yum.pms.dao.Tagged;
import com.yum.pms.dao.TaggedEmp;
import com.yum.pms.exception.YumPMSDataSaveException;
import com.yum.pms.utils.YumPmsConstants;

@Service
@SuppressWarnings("unchecked")
public class TaggedService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TaggedService.class);
	
	public List<Tagged> getTaggedDetailsByGoalSection(GoalSection goalSection, SessionFactory sessionFactory) {		
		
		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(Tagged.class)
				.add(Restrictions.eq(YumPmsConstants.GOAL_SECTION, goalSection))
				.list(); 
	}
	
	public TaggedEmp getTaggedToEmpById(Tagged tagged, EmployeeNew employee, SessionFactory sessionFactory) {	

		TaggedEmp taggedEmp = 
				(TaggedEmp)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(TaggedEmp.class)
				.add(Restrictions.eq(YumPmsConstants.TAGGED, tagged))
				.add(Restrictions.eq(YumPmsConstants.EMPLOYEE_MODEL, employee))
				.uniqueResult(); 
		return taggedEmp == null ? new TaggedEmp() : taggedEmp;
	}
	 
	public TaggedEmp saveTagRejectionDescription(TaggedEmp taggedEmp, SessionFactory sessionFactory) throws YumPMSDataSaveException {
		
		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			taggedEmp = (TaggedEmp) session.merge(taggedEmp);
			YumHibernateUtilServices.commitTransaction(session); 
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException("Failed to save Tag Rejection Description", e);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return taggedEmp;
	}

	public Tagged getTaggedById(int tagId, SessionFactory sessionFactory) {
		Tagged tagged = 
				(Tagged)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(Tagged.class)
				.add(Restrictions.eq(YumPmsConstants.TAG_ID, tagId))
				.uniqueResult(); 
		return tagged == null ? new Tagged() : tagged;
	}

	public List<TaggedEmp> getTaggedEmpListbyTagId(Tagged tagged, SessionFactory sessionFactory) {		
		
		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(TaggedEmp.class)
				.add(Restrictions.eq(YumPmsConstants.TAGGED, tagged))
				.list(); 
	}

	public Tagged saveTagData(Tagged tagged, SessionFactory sessionFactory) throws YumPMSDataSaveException {

		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			tagged = (Tagged)session.merge(tagged);
			YumHibernateUtilServices.commitTransaction(session);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException("Failed to save Tag", e);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		LOGGER.info("New tag id: {}", tagged.getTagId());
		return tagged;
	}

	public TaggedEmp saveTaggedEmp(TaggedEmp taggedEmp, SessionFactory sessionFactory) throws YumPMSDataSaveException {

		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try{
			taggedEmp = (TaggedEmp) session.merge(taggedEmp);
			YumHibernateUtilServices.commitTransaction(session);    
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException("Failed to save Employee for Tag", e);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return taggedEmp;
	}

	public void deleteTaggedEmps(List<TaggedEmp> deletedTaggedEmpList, SessionFactory sessionFactory) throws YumPMSDataSaveException {

		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try{
			for(TaggedEmp deleteTaggedEmp : deletedTaggedEmpList) {			    
				deleteById(TaggedEmp.class, deleteTaggedEmp.getTaggedEmpEventId(), session);
			}
			YumHibernateUtilServices.commitTransaction(session);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException("Failed to Remove Tagged Employee", e);
		} finally {
			YumHibernateUtilServices.clearSession(session);		    
		}
		return ;	
	}

	public void deleteTag(Integer tagid, SessionFactory sessionFactory) throws YumPMSDataSaveException {

		Tagged tagged = 
				(Tagged)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(Tagged.class)
				.add(Restrictions.eq(YumPmsConstants.TAG_ID, tagid))
				.uniqueResult();

		deleteTaggedEmps(getTaggedEmpListbyTagId(tagged, sessionFactory), sessionFactory);
		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
		try {
			deleteById(Tagged.class, tagid, session);
			YumHibernateUtilServices.commitTransaction(session); 
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			session.getTransaction().rollback(); 
			throw new YumPMSDataSaveException("Failed to Delete Tag", e);
		} finally {
			YumHibernateUtilServices.clearSession(session);	    
		}
	}
	
	private <T> boolean deleteById(Class<T> type, Serializable id, Session session) {
	    
		T persistentInstance = session.load(type, id);
	    if (persistentInstance != null) {
	        session.delete(persistentInstance);
	        return true;
	    }
	    return false;
	}
	
	public GoalSection getGoalSectionByTaggedID(Integer tagId, SessionFactory sessionFactory) {		

		return 
				(GoalSection)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(Tagged.class)
				.setProjection(Projections.projectionList()
						.add(Projections.property(YumPmsConstants.GOAL_SECTION)))
				.add(Restrictions.eq(YumPmsConstants.TAG_ID, tagId))
				.uniqueResult(); 
	}
	
	public TaggedEmp getReporteeTaggedInfoByTagId(Tagged tagged, EmployeeNew employee, SessionFactory sessionFactory) {

		return 
				(TaggedEmp)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(TaggedEmp.class)
				.add(Restrictions.eq(YumPmsConstants.TAGGED, tagged))
				.add(Restrictions.eq(YumPmsConstants.EMPLOYEE_MODEL, employee))
				.uniqueResult(); 
	}
}
