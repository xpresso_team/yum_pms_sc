/******************************************************************** 
* Services Interface for all service methods 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import com.yum.idp.domain.IdpCategory;
import com.yum.idp.domain.IdpSubCategory;
import com.yum.idp.domain.IdpSubSubCategory;
import com.yum.idp.domain.IdpSubSubSubCategory;
import com.yum.idp.domain.IdpSuggestedDevelopmentActionPlan;
import com.yum.idp.domain.IdpSuggestedGoal;
import com.yum.pa.domain.AdminPaConfig;
import com.yum.pa.domain.PromotionConfig;
import com.yum.pms.dao.AppraisalPeriod;
import com.yum.pms.dao.GlobalLetter;
import com.yum.pms.dao.IDP_SectionMaster;
import com.yum.pms.dao.PmsRole;
import com.yum.pms.dao.ReferenceDoc;
import com.yum.pms.domain.CascadedDetails;
import com.yum.pms.domain.ClientEmployee;
import com.yum.pms.domain.EmployeeCurrentDetails;
import com.yum.pms.domain.EmployeeGoalHistory;
import com.yum.pms.domain.EmployeeGoalSheet;
import com.yum.pms.domain.EmployeeGoalStatus;
import com.yum.pms.domain.EmployeeOrgGoal;
import com.yum.pms.domain.EmployeeOrgGoalCeoGoal;
import com.yum.pms.domain.EmployeeRoleDetails;
import com.yum.pms.domain.NotificationSettings;
import com.yum.pms.domain.ReporteeCascadedEmp;
import com.yum.pms.domain.ReporteeTaggedEmp;
import com.yum.pms.domain.StatusWorkFlow;
import com.yum.pms.domain.TaggedDetails;
import com.yum.pms.exception.YumPMSDataAccessException;
import com.yum.pms.exception.YumPMSDataSaveException;

public interface IYumHibernateService {
	
	public Map<String, Object> getDropDownList(String item); 
	
	public Map<Integer, String> getEmployeeListByManagerId(String userId, Integer apprId, Boolean nonDr);
	
	public EmployeeCurrentDetails getEmployeeDetailsByUserId(String userId, Integer apprId, String conditionType);
	
	public PmsRole getRoleDetailsByRoleId(String roleId);
	
	public EmployeeGoalSheet getGoalSheetDetailsByEmployeeId(Integer empId, Integer appraisalId, boolean isFromPDF) throws YumPMSDataAccessException;
	
	public List<ClientEmployee> getAllEmployeeList(Integer empId, Integer apprId, Boolean isDeactive);
	
	public EmployeeGoalSheet saveGoalSheet(EmployeeGoalSheet employeeGoalSheet)throws YumPMSDataSaveException;

	public List<EmployeeGoalStatus> getGoalSheetStatusValues();

	public List<TaggedDetails> getTaggedInfo(Integer goalSecId);
	
	public void updateTaskRemarks(Integer goalSecId,String goalSectionRemarks,String createdBy) throws YumPMSDataSaveException;

	public boolean saveGoalSheetStatusValues(EmployeeGoalStatus status) throws YumPMSDataSaveException;
	
	public List<CascadedDetails> getCascadedInfo(Integer goalSectionId);

	public TaggedDetails saveTaggedData(TaggedDetails taggedDetails) throws YumPMSDataSaveException;

	public CascadedDetails saveCascadedData(CascadedDetails cascadedDetails) throws YumPMSDataSaveException;

	public ReporteeTaggedEmp getEmpTaggedInfo(Integer tagId, Integer reporteeId);
	
	public Boolean deleteTag(Integer tagid) throws YumPMSDataSaveException;
	
	public Boolean deleteCascade(Integer cascadeid) throws YumPMSDataSaveException;

	public ReporteeCascadedEmp getEmpCascadedInfo(Integer cascadeId, Integer reporteeId);
	
	public StatusWorkFlow getWorkStatusByGoalSheetId(Integer goalSheetId);

	public List<EmployeeOrgGoal> getOrgGoals(Integer appraisalPeriodId);

	public List<EmployeeOrgGoalCeoGoal> getCeoOrgGoals(Integer appraisalPeriodId);

	public List<NotificationSettings> getNotificationSettings();
	
	public StatusWorkFlow updateGoalSheetStatus(Integer goalSheetId,Integer statusId, String createdBy)throws YumPMSDataSaveException;

	public List<NotificationSettings> saveNotificationSetting(List<NotificationSettings> listNotificationSetting) throws YumPMSDataSaveException;

	public List<AppraisalPeriod> getApprPeriodList();
	
	public List<EmployeeOrgGoal> saveOrgGoals(List<EmployeeOrgGoal> employeeOrgGoalList) throws YumPMSDataSaveException;
	
	public List<EmployeeOrgGoalCeoGoal> saveCeoGoals(List<EmployeeOrgGoalCeoGoal> employeeCeoGoalList) throws YumPMSDataSaveException;
	
	public String deleteById(Integer id, String tableName);
	
	public String deleteById(String id, String tableName) throws YumPMSDataSaveException;

	public List<EmployeeRoleDetails> getEmpRoleList();

	public AppraisalPeriod getDefaultAppraisalPeriod();

	public List<AppraisalPeriod> saveAppraisalPeriods(List<AppraisalPeriod> appraisalPeriodList);

	public List<EmployeeRoleDetails> saveEmployeeRoles(List<EmployeeRoleDetails> employeeRoleList) throws YumPMSDataSaveException;
	
	public List<Map<String, Object>> getUserRollList();

	public List<EmployeeGoalHistory> getLastWorkFlowHistoryStatus(Integer goalSheetId); 
	
	public String savePassword(String loginId) throws YumPMSDataSaveException;
	
	public void sendMail(String type);
	
	public void sendMail(String type, String spName) throws SQLException;
	
	public void sendMail(String type, Integer idpSheetId);
	
	public ReferenceDoc fileUpload(MultipartFile uploadFile, ReferenceDoc referenceDoc) throws YumPMSDataSaveException;
	
	public GlobalLetter uploadGlobalLetter(MultipartFile uploadFile, GlobalLetter globalLetter) throws YumPMSDataSaveException;
	
	public List<GlobalLetter> getGlobalLetterDetails(Integer apprId, Integer empId);
	
	public void deleteGlobalLetterDetails(Integer globalLetterId);
	
	public Map<String, byte[]> downloadGlobalLetter(Integer globalLetterId);
	
	public List<ReferenceDoc> getfilesdetailsList(ReferenceDoc referenceDoc);
	
	public String fileDownload(String fileId, HttpServletRequest request, HttpServletResponse response) throws YumPMSDataAccessException ;
	
	public Map<String, Object> getPdfTagReplacementDetails(); 
	
	public Map<String,Object> getAllSections(Integer apprId, String categoryFlag);
	
	public List<IdpCategory> saveIdpCategoryList(List<IdpCategory> sectionList);
	
	public List<IdpSubCategory> saveIdpSubCategoryList(List<IdpSubCategory> subSectionList) throws YumPMSDataSaveException;

	public List<IdpSubSubCategory> saveIdpSubSubCategoryList(List<IdpSubSubCategory> subSubSectionList) throws YumPMSDataSaveException;

	public List<IdpSubSubSubCategory> saveIdpSubSubSubCategoryList(List<IdpSubSubSubCategory> subSubSubSectionList) throws YumPMSDataSaveException;
	
	public List<IdpSuggestedGoal> saveIdpSuggestedGoalList(List<IdpSuggestedGoal> suggestedGoalList) throws YumPMSDataSaveException;
	
	public List<IdpSuggestedDevelopmentActionPlan> saveIdpDevelopmentPlanList(List<IdpSuggestedDevelopmentActionPlan> developmentPlanList) throws YumPMSDataSaveException;
	
	public List<IDP_SectionMaster> getIdpCategoryList();

	public Map<String, String> getAllTooltips(String empGrade);

	public String deleteIdpSection(Integer id, String tableName);

	AdminPaConfig savePaConfigData(AdminPaConfig adminPaConfig) throws YumPMSDataSaveException;

	AdminPaConfig getPaConfigData(Integer apprId);

	void savePromotionConfigData(PromotionConfig promotionConfig) throws YumPMSDataSaveException;

	public PromotionConfig getPromotionConfigData(Integer apprId);
	
	public List<AppraisalPeriod> getAppraisalPeriod();
	
	public String getServerTimeOutTime() throws SQLException;
	
	public Map<String, Object> yearConfigurationForBrandGoal(Integer apprId);
	
}
