/******************************************************************** 
 * Hibernate Services for Goalsheet and related data
 * 
 * http://www.abzooba.com
 * ------------------- 
 * Created By: jayanta.biswas
 * Last Updated By: sudipta.chandra
 * Last Updated On: 23-Sep-2017 (11:34:34 AM) 
 *
 * The software and related user documentation are protected under 
 * copyright laws and remain the sole property of Abzooba. 
 * 
 * Technical support is available via the Abzooba website at 
 * http://www.abzooba.com
 **************************************************************************/

package com.yum.pms.service;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yum.pms.dao.AppraisalPeriod;
import com.yum.pms.dao.EmployeeNew;
import com.yum.pms.dao.Goal;
import com.yum.pms.dao.GoalCeo;
import com.yum.pms.dao.GoalOrg;
import com.yum.pms.dao.GoalSection;
import com.yum.pms.dao.GoalSheet;
import com.yum.pms.dao.GoalSheetWorkflowHistory;
import com.yum.pms.dao.GoalStatus;
import com.yum.pms.exception.YumPMSDataSaveException;
import com.yum.pms.utils.SendMail;
import com.yum.pms.utils.YumPmsConstants;
import com.yum.pms.utils.YumPmsUtils;

@Service
@SuppressWarnings("unchecked")
public class GoalServices {

	private static final Logger LOGGER = LoggerFactory.getLogger(GoalServices.class);
	
	@Autowired
	private YumHibernateUtilServices util;

	@Autowired
	private SendMail mailSender;

	public GoalSheet getGoalSheetbyEmpId(EmployeeNew employee, AppraisalPeriod appraisalPeriod, SessionFactory sessionFactory) {		

		GoalSheet goalSheet =  
				(GoalSheet) YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(GoalSheet.class)
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, appraisalPeriod))
				.add(Restrictions.eq(YumPmsConstants.EMPLOYEE_MODEL, employee))
				.uniqueResult(); 
		return goalSheet == null ? new GoalSheet() : goalSheet;
	}


	public List<GoalOrg> getOrgGoalList(Integer appraisalPeriodId, SessionFactory sessionFactory) {

		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(GoalOrg.class)
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, new AppraisalPeriod(appraisalPeriodId)))
				.list();
	}

	public Goal getGoalByOrgCeoGoal(Integer apprId, Integer orgGoalId, Integer ceoGoalId, EmployeeNew employee, SessionFactory sessionFactory) {

		Criteria cr = YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(Goal.class)
				.createAlias(YumPmsConstants.GOAL_SHEET, "gs")
				.add(Restrictions.eq("gs.appraisalPeriod", new AppraisalPeriod(apprId)))
				.add(Restrictions.eq("gs.employee", employee));

		if(orgGoalId != null && orgGoalId.intValue() != 0) {
			cr.add(Restrictions.eq(YumPmsConstants.ORG_GOAL_ID, orgGoalId));
		}
		if(ceoGoalId != null && ceoGoalId.intValue() != 0) {
			cr.add(Restrictions.eq(YumPmsConstants.CEO_GOAL_ID, ceoGoalId));
		}
		return (Goal) cr.uniqueResult();

	}

	public GoalSheet getGoalSheetbyId(Integer goalSheetId, SessionFactory sessionFactory) {

		GoalSheet goalSheet = 
				(GoalSheet)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(GoalSheet.class)
				.add(Restrictions.eq(YumPmsConstants.GOAL_SHEET_ID, goalSheetId))
				.uniqueResult(); 
		return goalSheet == null ? new GoalSheet() : goalSheet;
	}

	public AppraisalPeriod getDefaultAppraisalPeriod(SessionFactory sessionFactory) {

		return 
				(AppraisalPeriod) YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(AppraisalPeriod.class)
				.add(Restrictions.eq(YumPmsConstants.IS_DEFAULT, true))
				.uniqueResult();
	}

	public GoalStatus getGoalStatus(Integer goalStatusID, SessionFactory sessionFactory) {
		
		return 
				(GoalStatus)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(GoalStatus.class)
				.add(Restrictions.eq(YumPmsConstants.GOAL_STATUS_ID, goalStatusID))
				.uniqueResult(); 
	}

	public GoalSheet saveGoalSheet(GoalSheet goalSheet, SessionFactory sessionFactory) throws YumPMSDataSaveException {
		
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			goalSheet = (GoalSheet) currentSession.merge(goalSheet);
			YumHibernateUtilServices.commitTransaction(currentSession);    
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to save Goal Sheet", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return goalSheet;
	}

	public void updateRemarks(Integer goalSecId, String goalSectionSupRemarks, String createdBy, SessionFactory sessionFactory) 
			throws YumPMSDataSaveException {

		GoalSection goalSection = 
				(GoalSection)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(GoalSection.class)
				.add(Restrictions.eq(YumPmsConstants.GOAL_SECTION_ID, goalSecId))
				.uniqueResult();
		goalSection.setGoalSectionSupRemarks(goalSectionSupRemarks);
		goalSection.setUpdatedBy(createdBy);

		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
		try {
			currentSession.merge(goalSection);
			YumHibernateUtilServices.commitTransaction(currentSession);       
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to Update Supervisor Remarks", e);
		}
		//send email
		try {
			if(util.isActiveSp(sessionFactory, "sp_EmailNotification_ByGoalSheet_java")) {
				List<Object[]> results = 
						YumHibernateUtilServices.getSession(sessionFactory)
						.createSQLQuery("exec sp_EmailNotification_ByGoalSheet_java :param1, :param2")
						.setString(YumPmsConstants.PARAM_1, Integer.toString(goalSection.getGoal().getGoalSheet().getGoalSheetId()))
						.setString(YumPmsConstants.PARAM_2, YumPmsConstants.REMARKS)
						.list();
				for (Object[] tuples : results) {
					mailSender.sendMail(YumPmsUtils.toStringorNull(tuples[0]), 
							YumPmsUtils.toStringorNull(tuples[1]), 
							YumPmsUtils.toStringorNull(tuples[2]), 
							YumPmsUtils.toStringorNull(tuples[3]));
				}
			}
		} catch (Exception e) {
			LOGGER.error("Mail could not be sent for Supervisor Remarks Addition. Error: {}", e.getMessage());
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
		}
	}

	public Goal saveGoal(Goal goal, SessionFactory sessionFactory) throws YumPMSDataSaveException {

		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			goal = (Goal) currentSession.merge(goal);
			YumHibernateUtilServices.commitTransaction(currentSession);     
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to save Goal", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return goal;
	}

	public GoalSection saveGoalSection(GoalSection section, SessionFactory sessionFactory) throws YumPMSDataSaveException {

		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			section = (GoalSection) currentSession.merge(section);
			YumHibernateUtilServices.commitTransaction(currentSession);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to save Task", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return section;
	}

	public GoalSection getGoalSectionById(int sectionId, SessionFactory sessionFactory) {		
		
		GoalSection goalSection = 
				(GoalSection) YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(GoalSection.class)
				.add(Restrictions.eq(YumPmsConstants.GOAL_SECTION_ID, sectionId))
				.uniqueResult(); 
		return goalSection == null ? new GoalSection() : goalSection;
	}

	public Goal getGoalbyId(Integer goalId, SessionFactory sessionFactory) {
		
		Goal goal =  
				(Goal)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(Goal.class)
				.add(Restrictions.eq(YumPmsConstants.GOAL_ID, goalId))
				.uniqueResult(); 
		return goal == null ? new Goal() : goal;
	}

	public List<Goal> getGoalListbyGoalSheetId(GoalSheet goalSheet, SessionFactory sessionFactory) {

		List<Goal> goalList = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(Goal.class)
				.add(Restrictions.eq(YumPmsConstants.GOAL_SHEET, goalSheet))
				.list(); 
		return YumPmsUtils.getEmptyListIfNull(goalList);
	}

	public List<GoalSection> getGoalSectionListbyGoalId(Goal goal, SessionFactory sessionFactory) {
		
		List<GoalSection> goalSectionList =  
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(GoalSection.class)
				.add(Restrictions.eq(YumPmsConstants.GOAL, goal))
				.list(); 
		return YumPmsUtils.getEmptyListIfNull(goalSectionList);
	}

	public void deleteGoalSection(List<GoalSection> deletedGoalSectionList, SessionFactory sessionFactory) throws YumPMSDataSaveException {
		Session currentSession = null; 
		try {
			for(GoalSection deleteSection : deletedGoalSectionList) {
				GoalSection gsCheck = getGoalSectionById(deleteSection.getGoalSectionId(), sessionFactory);
				if(gsCheck.getGoalSectionId() == 0) {
					return;
				}
				currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
				deleteById(GoalSection.class, deleteSection.getGoalSectionId(), currentSession); 
				YumHibernateUtilServices.commitTransaction(currentSession);
			}
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to Remove Task", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);		    
		}
	}

	public void deleteGoal(List<Goal> deletedGoalList, SessionFactory sessionFactory) throws YumPMSDataSaveException {
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			for(Goal deleteGoal : deletedGoalList) {			    
				currentSession.delete(deleteGoal);
			}
			YumHibernateUtilServices.commitTransaction(currentSession);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to remove Goal", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);		    
		}
	}

	public GoalStatus getDefaultGoalStatus(SessionFactory sessionFactory) {
		return 
				(GoalStatus)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(GoalStatus.class)
				.add(Restrictions.eq(YumPmsConstants.GOAL_STATUS_ID, 1001))
				.uniqueResult(); 
	}

	private boolean deleteById(Class<?> type, Serializable id, Session session) {
		Object persistentInstance = session.load(type, id);
		if (persistentInstance != null) {
			session.delete(persistentInstance);
			return true;
		}
		return false;
	}

	public Goal getGoalByGoalID(Integer goalId, SessionFactory sessionFactory) {		
		return 
				(Goal)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(Goal.class)
				.add(Restrictions.eq(YumPmsConstants.GOAL_ID, goalId))
				.uniqueResult(); 
	}

	public List<GoalOrg> getOrgGoalsbyAppraisalPeriod(AppraisalPeriod appraisalPeriod, SessionFactory sessionFactory) {

		List<GoalOrg> orgGoalList = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(GoalOrg.class)
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, appraisalPeriod))
				.list(); 
		return YumPmsUtils.getEmptyListIfNull(orgGoalList);
	}

	public GoalCeo getCeoGoalByOrgGoal(GoalOrg orgGoal, SessionFactory sessionFactory) {

		GoalCeo ceoGoal = 
				(GoalCeo) YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(GoalCeo.class)
				.add(Restrictions.eq(YumPmsConstants.GOAL_ORG, orgGoal))
				.uniqueResult(); 
		return ceoGoal == null ? new GoalCeo() : ceoGoal;
	}

	public List<Goal> getGoalList(SessionFactory sessionFactory) {

		List<Goal> goalList = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(Goal.class)
				.list(); 
		return YumPmsUtils.getEmptyListIfNull(goalList);
	}

	public GoalOrg getOrgGoalsbyId(Integer orgGoalId, SessionFactory sessionFactory) {
		GoalOrg orgGoal =  
				(GoalOrg) YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(GoalOrg.class)
				.add(Restrictions.eq(YumPmsConstants.ORG_GOAL_ID, orgGoalId))
				.uniqueResult(); 
		return orgGoal == null ? new GoalOrg() : orgGoal;
	}

	public GoalOrg saveOrgGoal(GoalOrg orgGoal, SessionFactory sessionFactory) throws YumPMSDataSaveException {

		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			orgGoal = (GoalOrg) currentSession.merge(orgGoal);
			YumHibernateUtilServices.commitTransaction(currentSession); 
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to save Org Goal", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return orgGoal;

	}

	public List<GoalCeo> getCeoGoalList(SessionFactory sessionFactory) {

		List<GoalCeo> ceoGoalList = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(GoalCeo.class)
				.list(); 
		return YumPmsUtils.getEmptyListIfNull(ceoGoalList);
	}

	public GoalCeo getCeoGoalbyId(Integer ceoGoalId, SessionFactory sessionFactory) {

		GoalCeo ceoGoal = YumHibernateUtilServices.getSession(sessionFactory).get(GoalCeo.class, ceoGoalId);
		return ceoGoal == null ? new GoalCeo() : ceoGoal;
	}

	public GoalCeo saveCeoGoal(GoalCeo ceogGoal, SessionFactory sessionFactory) throws YumPMSDataSaveException {

		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			ceogGoal = (GoalCeo) currentSession.merge(ceogGoal);
			YumHibernateUtilServices.commitTransaction(currentSession);     
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to save Ceo Goal", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return ceogGoal;

	}

	public AppraisalPeriod getAppraisalPeriodFromGoalSection(GoalSection goalSection, SessionFactory sessionFactory) {

		Goal goal = 
				(Goal) YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(Goal.class)
				.add(Restrictions.eq(YumPmsConstants.GOAL_ID, goalSection.getGoal().getGoalId()))
				.uniqueResult();
		
		int goalSheetId = goal != null && goal.getGoalSheet() != null ? goal.getGoalSheet().getGoalSheetId() : 0;
		GoalSheet goalSheet = 
				(GoalSheet)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(GoalSheet.class)
				.add(Restrictions.eq(YumPmsConstants.GOAL_SHEET_ID, goalSheetId))
				.uniqueResult();

		return goalSheet != null ? goalSheet.getAppraisalPeriod() : new AppraisalPeriod();
	}

	public List<Goal> getCustomGoalList(EmployeeNew emp, AppraisalPeriod appraisalperiod, SessionFactory sessionFactory) {
		List<Goal> customGoalList = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(Goal.class).createAlias(YumPmsConstants.GOAL_SHEET, "g_sheet")
				.add(Restrictions.eq("g_sheet.employee", emp))
				.add(Restrictions.eq("g_sheet.appraisalPeriod", appraisalperiod))
				.add(Restrictions.eq(YumPmsConstants.ORG_GOAL_ID, 0))
				.list(); 
		return YumPmsUtils.getEmptyListIfNull(customGoalList);
	}

	public GoalSheetWorkflowHistory saveGoalSheetHistory(GoalSheetWorkflowHistory gsWkflwHistory, SessionFactory sessionFactory) 
			throws YumPMSDataSaveException {
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try{
			gsWkflwHistory = (GoalSheetWorkflowHistory) currentSession.merge(gsWkflwHistory);
			YumHibernateUtilServices.commitTransaction(currentSession);  
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to save Goal Sheet History", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return gsWkflwHistory;
	}

	public List<GoalSheetWorkflowHistory> getWorkFlowHistoryStatus(Integer goalSheetId, SessionFactory sessionFactory) {

		Query query = YumHibernateUtilServices.getSession(sessionFactory)
				.createQuery("from GoalSheetWorkflowHistory gswfw where gswfw.goalSheetId = :goalSheetId order by createdDate desc")
				.setParameter(YumPmsConstants.GOAL_SHEET_ID, goalSheetId);

		List<GoalSheetWorkflowHistory> lGoalSheetHistory = null;
		try {
			lGoalSheetHistory = query.list();
		} catch (Exception e) {
			//no action required
		}
		return YumPmsUtils.getEmptyListIfNull(lGoalSheetHistory);
	}

	public Integer getGoalByOrgGoalGoalSheetID(Integer orgGoalId, GoalSheet goalSheet, SessionFactory sessionFactory) {		
		
		LOGGER.info("orgGoalId = {} and goalSheet_id = {}", orgGoalId, goalSheet != null ? goalSheet.getGoalSheetId() : 0);
		Goal goal = 
				(Goal) YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(Goal.class)
				.add(Restrictions.eq(YumPmsConstants.ORG_GOAL_ID, orgGoalId))
				.add(Restrictions.eq(YumPmsConstants.GOAL_SHEET, goalSheet))
				.uniqueResult();  
		return goal != null ? goal.getGoalId() : 0;
	}

	/**
	 * execute Goal Version History Procedure
	 * @param goalSheetId
	 * @param sessionFactory
	 */
	public void executeGoalVersionHistoryProcedure(Integer goalSheetId, SessionFactory sessionFactory) {

		YumHibernateUtilServices.getSession(sessionFactory)
		.createSQLQuery(String.format(YumPmsConstants.SP_GOAL_VERSION_HISTORY, YumPmsConstants.PARAM_1))
		.setInteger(YumPmsConstants.PARAM_1, goalSheetId)
		.executeUpdate();
	}

	/**
	 * Used to get the email notification parameter while changing the goal sheet status
	 * @param goalSheetId
	 * @param status
	 * @return the email parameter list
	 */
	public List<Object[]> getGoalStatusChangeEmailParams(String goalSheetId, String status, SessionFactory sessionFactory) {
		if(util.isActiveSp(sessionFactory, "sp_EmailNotification_ByGoalSheet_java")) {
			List<Object[]> results = 
					YumHibernateUtilServices.getSession(sessionFactory)
					.createSQLQuery(String.format(YumPmsConstants.SP_GOAL_EMAIL_STATUS_CHANGE, YumPmsConstants.PARAM_1, YumPmsConstants.PARAM_2))
					.setString(YumPmsConstants.PARAM_1, goalSheetId)
					.setString(YumPmsConstants.PARAM_2, status)
					.list();
			return YumPmsUtils.getEmptyListIfNull(results);
		}else {
			return null;
		}
	}
}
