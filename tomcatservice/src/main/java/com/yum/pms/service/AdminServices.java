/******************************************************************** 
 * Hibernate Services for Admin section  
 * 
 * http://www.abzooba.com
 * ------------------- 
 * Created By: sudipta.chandra
 * Last Updated By: sudipta.chandra
 * Last Updated On: 23-Sep-2017 (11:34:34 AM) 
 *
 * The software and related user documentation are protected under 
 * copyright laws and remain the sole property of Abzooba. 
 * 
 * Technical support is available via the Abzooba website at 
 * http://www.abzooba.com
 **************************************************************************/


package com.yum.pms.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yum.pms.dao.AppraisalPeriod;
import com.yum.pms.dao.EmpToPmsRole;
import com.yum.pms.dao.EmpToPmsRoleId;
import com.yum.pms.dao.EmployeeNew;
import com.yum.pms.dao.Goal;
import com.yum.pms.dao.GoalCeo;
import com.yum.pms.dao.GoalOrg;
import com.yum.pms.dao.GoalSheet;
import com.yum.pms.dao.GoalStatus;
import com.yum.pms.dao.IDP_SectionMaster;
import com.yum.pms.dao.IDP_SubSectionMaster;
import com.yum.pms.dao.IDP_SubSubSectionMaster;
import com.yum.pms.dao.IDP_SubSubSubSectionMaster;
import com.yum.pms.dao.IDP_SuggestedDevelopmentPlanMaster;
import com.yum.pms.dao.IDP_SuggestedGoalMaster;
import com.yum.pms.dao.PmsRole;
import com.yum.pms.domain.EmployeeGoalStatus;
import com.yum.pms.exception.CommonException;
import com.yum.pms.exception.YumPMSDataSaveException;
import com.yum.pms.utils.YumPmsConstants;
import com.yum.pms.utils.YumPmsUtils;

@Service
@SuppressWarnings("unchecked")
public class AdminServices {

	private static final Logger LOGGER = LoggerFactory.getLogger(AdminServices.class);

	@Autowired
	private SessionFactory sessionFactory;

	private boolean deleteById(Class<?> type, Serializable id, Session session) {
		Object persistentInstance = session.load(type, id);
		if (persistentInstance != null) {
			session.delete(persistentInstance);
			return true;
		}
		return false;
	}

	public List<EmployeeGoalStatus> getGoalSheetStatusValues(SessionFactory sessionFactory) {

		List<Object[]> tuples = YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(GoalStatus.class)
				.setProjection(Projections.projectionList()
						.add(Projections.property(YumPmsConstants.GOAL_STATUS_ID))
						.add(Projections.property(YumPmsConstants.GOAL_STATUS_NAME))
						.add(Projections.property(YumPmsConstants.ACTIVE)))
				.list();
		if(CollectionUtils.isEmpty(tuples)) {
			return Collections.emptyList();
		}
		List<EmployeeGoalStatus> statusList = new ArrayList<>(tuples.size());
		for (Object[] tuple : tuples) {
			EmployeeGoalStatus status = new EmployeeGoalStatus();
			status.setActive((Boolean)tuple[2]);
			status.setGoalStatusId((Integer)tuple[0]);
			status.setGoalStatusName((String)tuple[1]);
			statusList.add(status);
		}
		return statusList;
	}

	public boolean saveGoalSheetStatusValues(EmployeeGoalStatus status, SessionFactory sessionFactory) throws YumPMSDataSaveException {

		LOGGER.info("incoming goalStatus======>>>>> {}, {}, {}", status.getGoalStatusId(), status.getGoalStatusName(), status.getActive());
		GoalStatus goalStatus  = 
				(GoalStatus) YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(GoalStatus.class)
				.add(Restrictions.eq(YumPmsConstants.GOAL_STATUS_ID, status.getGoalStatusId()))
				.uniqueResult();
		goalStatus.setGoalStatusName(status.getGoalStatusName());
		goalStatus.setActive(status.getActive());
		if(goalStatus.getGoalStatusId() > 0)
			goalStatus.setCreatedBy(status.getCreatedBy());
		else
			goalStatus.setUpdatedBy(status.getCreatedBy());
		Session currentSession = null;
		try {
			currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
			currentSession.merge(goalStatus);
			YumHibernateUtilServices.commitTransaction(currentSession);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to save Goal Sheet Status Values", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}

		return true;
	}

	public List<AppraisalPeriod> getApprPeriodList(SessionFactory sessionFactory) {

		return YumHibernateUtilServices.getSession(sessionFactory).createCriteria(AppraisalPeriod.class).list();
	}
	// ==============================================All Delete Method===================================================
	public Boolean deleteOrgGoal(Integer id, SessionFactory sessionFactory) {
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		boolean deleteFlag = deleteById(GoalOrg.class, id, currentSession); 		
		YumHibernateUtilServices.commitTransaction(currentSession);
		return deleteFlag;
	}
	public Boolean deleteCeoGoal(Integer id, SessionFactory sessionFactory) {
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);  
		currentSession.getTransaction().begin();   
		boolean deleteFlag = deleteById(GoalCeo.class, id, currentSession); 		
		YumHibernateUtilServices.commitTransaction(currentSession); 
		return deleteFlag;
	}

	public Boolean deleteAppraisal(Integer id, SessionFactory sessionFactory) {
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		currentSession.getTransaction().begin();   
		boolean deleteFlag = deleteById(AppraisalPeriod.class, id, currentSession); 		
		YumHibernateUtilServices.commitTransaction(currentSession);  
		return deleteFlag;
	}

	public Boolean deleteEmployee(Integer id, SessionFactory sessionFactory) {
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		boolean deleteFlag = deleteById(EmployeeNew.class, id, currentSession); 		
		YumHibernateUtilServices.commitTransaction(currentSession);  
		return deleteFlag;
	}

	// ===================================================================================================================
	public List<EmpToPmsRole> getEmpRoleList(SessionFactory sessionFactory) {

		List<EmpToPmsRole> empPMSRoleList = YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(EmpToPmsRole.class)
				.list();		
		return YumPmsUtils.getEmptyListIfNull(empPMSRoleList);
	}

	public EmpToPmsRole getEmpPMSRole(EmpToPmsRoleId pmsRoleId, EmployeeNew employee, SessionFactory sessionFactory) {

		return 
				(EmpToPmsRole)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(EmpToPmsRole.class)
				.add(Restrictions.eq(YumPmsConstants.ID, pmsRoleId))
				.add(Restrictions.eq(YumPmsConstants.EMPLOYEE_MODEL, employee))
				.uniqueResult();  
	}

	public PmsRole getPMSRoleById(Integer pmsRoleId, SessionFactory sessionFactory) {

		return
				(PmsRole)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(PmsRole.class)
				.add(Restrictions.eq(YumPmsConstants.PMS_ROLE_ID, pmsRoleId))
				.uniqueResult();  
	}

	public EmpToPmsRole saveEmpPMSRole(EmpToPmsRole dbEmpToPMSRole, SessionFactory sessionFactory) throws YumPMSDataSaveException {
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			dbEmpToPMSRole = (EmpToPmsRole) currentSession.merge(dbEmpToPMSRole);
			YumHibernateUtilServices.commitTransaction(currentSession);   
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to save Employee Role Values", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return dbEmpToPMSRole;
	}

	public void deleteEmptoPMSRoleIdById(int empId, SessionFactory sessionFactory) throws YumPMSDataSaveException {

		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);			
		EmpToPmsRole empPMSRole = 
				(EmpToPmsRole)currentSession.createCriteria(EmpToPmsRole.class)
				.add(Restrictions.eq(YumPmsConstants.EMPLOYEE_MODEL, new EmployeeNew(empId)))
				.uniqueResult();
		if(empPMSRole != null) {
			try {
				currentSession.delete(empPMSRole);
				YumHibernateUtilServices.commitTransaction(currentSession);
			} catch (Exception e) {
				LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
				YumHibernateUtilServices.rollbackTransaction(currentSession);
				throw new YumPMSDataSaveException("Failed to delete Goal Sheet Status Values", e);
			} finally {
				YumHibernateUtilServices.clearSession(currentSession);
			}
		}
	}

	public Boolean getAppraisalPeriodIsRemovable(AppraisalPeriod appraisalPeriodFromDB, SessionFactory sessionFactory) {


		Session localSession = YumHibernateUtilServices.getSession(sessionFactory);			

		//check org goals for linkage
		List<GoalOrg> orgGoalList = 
				localSession.createCriteria(GoalOrg.class)
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, appraisalPeriodFromDB))
				.list();  
		if(CollectionUtils.isNotEmpty(orgGoalList))
			return false;

		//check Ceo goals for linkage
		List<GoalCeo> ceoGoalList = 
				localSession.createCriteria(GoalCeo.class)
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, appraisalPeriodFromDB))
				.list();  
		if(CollectionUtils.isNotEmpty(ceoGoalList))
			return false;

		//check for goal sheet link
		List<GoalSheet> goalSheetList = 
				localSession.createCriteria(GoalSheet.class)
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, appraisalPeriodFromDB))
				.list(); 
		if(CollectionUtils.isNotEmpty(goalSheetList))
			return false;

		return true;
	}

	public void savePassword(String loginId, String password, SessionFactory sessionFactory) throws YumPMSDataSaveException {
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			String sql = String.format("UPDATE emp_login SET dashboard_tmp_pass = :%s WHERE username = :%s", 
					YumPmsConstants.SQL_PARAM_PASS, YumPmsConstants.LOGIN_ID);
			int result = 
					currentSession.createSQLQuery(sql)
					.setString(YumPmsConstants.SQL_PARAM_PASS, password)
					.setString(YumPmsConstants.LOGIN_ID, loginId)
					.executeUpdate();
			LOGGER.info("Result = {}", result);
			YumHibernateUtilServices.commitTransaction(currentSession);     
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to save Employee Role Values", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
	}

	public void insertUserRole(String loginId, List<String> roleValue, SessionFactory sessionFactory) throws YumPMSDataSaveException {
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 		
		try {
			List<Integer> fetchingRows = 
					currentSession.createSQLQuery(String.format("SELECT COUNT(*) FROM user_roles WHERE username = :%s", YumPmsConstants.LOGIN_ID))
					.setParameter(YumPmsConstants.LOGIN_ID, loginId)
					.list();

			if(fetchingRows.get(0) == 0) {
				String sql = String.format("INSERT INTO user_roles (username, role_name) VALUES(:%s, :%s)", 
						YumPmsConstants.LOGIN_ID, YumPmsConstants.ROLE);
				for(int index=0; index<roleValue.size(); index++) {
					SQLQuery insertQuery = currentSession.createSQLQuery(sql);
					insertQuery.setString(YumPmsConstants.LOGIN_ID, loginId);
					insertQuery.setString(YumPmsConstants.ROLE, roleValue.get(index));
					insertQuery.executeUpdate();
				}
			}
			YumHibernateUtilServices.commitTransaction(currentSession);
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to insert user_roles table", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
	}

	public void deleteUserRole(String loginId, SessionFactory sessionFactory) throws YumPMSDataSaveException {
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 		
		try {
			currentSession.createSQLQuery(String.format("DELETE FROM user_roles WHERE username = :%s", YumPmsConstants.LOGIN_ID))
			.setString(YumPmsConstants.LOGIN_ID, loginId)
			.executeUpdate();
			YumHibernateUtilServices.commitTransaction(currentSession);     
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to delete user_roles table", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return ;
	}

	public List<Goal> getGoalByCeoGoal(GoalCeo ceoGoal, SessionFactory sessionFactory) {

		List<Goal> goalList = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(Goal.class)
				.add(Restrictions.eq(YumPmsConstants.CEO_GOAL_ID, ceoGoal.getCeoGoalId()))
				.list(); 
		return YumPmsUtils.getEmptyListIfNull(goalList);
	}

	private Session getSession(SessionFactory sessionFactory) {
		return YumHibernateUtilServices.getSession(sessionFactory);
	}

	/**
	 * @param SessionFactory sessionFactory
	 * @return List<IDP_SectionMaster> list
	 * @throws YumPMSDataSaveException
	 */
	public List<IDP_SectionMaster> getIdpCategoryList(SessionFactory sessionFactory) {
		return getSession(sessionFactory)
				.createCriteria(IDP_SectionMaster.class)
				.add(Restrictions.ne(YumPmsConstants.IDP_SECTION_DESC, YumPmsConstants.GROWTH_IN_UPPER_CASE))
				.list();
	}

	/**
	 * @param section
	 * @param sessionFactory
	 * @return : Get saved IDP category object
	 */
	public IDP_SectionMaster saveIdpSectionMasterData(IDP_SectionMaster section) {
		try {
			Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
			IDP_SectionMaster savedSection = (IDP_SectionMaster)currentSession.merge(section);
			YumHibernateUtilServices.commitTransaction(currentSession);
			YumHibernateUtilServices.clearSession(currentSession);
			return savedSection;
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			throw new CommonException(e);
		}
	}

	/**
	 * @param id
	 * @return Delete IDP category and return true/false as result
	 * @throws Exception
	 */
	public boolean deleteIdpSection(Integer id, String tableName) {
		try {
			Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
			String sql = String.format("exec sp_logic_idp_CategoryDeletion :%s, :%s", YumPmsConstants.TABLE_NAME, YumPmsConstants.ID);
			SQLQuery query = currentSession.createSQLQuery(sql);
			query.setString(YumPmsConstants.TABLE_NAME, tableName);
			query.setInteger(YumPmsConstants.ID, id);
			query.executeUpdate();
			YumHibernateUtilServices.commitTransaction(currentSession);
			YumHibernateUtilServices.clearSession(currentSession);
			return true;
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			throw new CommonException(e);
		}
	}

	/**
	 * @param apprId 
	 * @param SessionFactory sessionFactory
	 * @return Map<String, Object> mapOfItem
	 * @throws YumPMSDataSaveException
	 */
	public List<IDP_SectionMaster> getSectionList(Integer apprId, String sectionFlag) {
		
		Criteria criteria = getSession(sessionFactory).createCriteria(IDP_SectionMaster.class);
		
		criteria.add(Restrictions.eq(YumPmsConstants.IS_ACTIVE, true))
				.add(Restrictions.eq(YumPmsConstants.APPR_ID, apprId));
		if(StringUtils.isNotBlank(sectionFlag)) {
			criteria.add(Restrictions.eq(YumPmsConstants.IDP_SECTION_TYPE_FLAG, sectionFlag));
		}
		return
				criteria.add(Restrictions.ne(YumPmsConstants.IDP_SECTION_DESC, YumPmsConstants.GROWTH_IN_UPPER_CASE))
				.list();
	}
	
	/**
	 * @param apprId 
	 * @param SessionFactory sessionFactory
	 * @return Map<String, Object> mapOfItem
	 * @throws YumPMSDataSaveException
	 */
	public List<IDP_SectionMaster> getSectionListPrevious(Integer apprId) {
		return 
				getSession(sessionFactory)
				.createCriteria(IDP_SectionMaster.class)
				.add(Restrictions.eq(YumPmsConstants.IS_ACTIVE, true))
				.add(Restrictions.eq(YumPmsConstants.APPR_ID, apprId))
				.add(Restrictions.ne(YumPmsConstants.IDP_SECTION_DESC, YumPmsConstants.GROWTH_IN_UPPER_CASE))
				.list();
	}

	/**
	 * @param apprId 
	 * @param categoryFlag 
	 * @param SessionFactory sessionFactory
	 * @return List<IDP_SubSectionMaster> listOfSubSectionMaster
	 */
	public List<IDP_SubSectionMaster> getSubSectionList(Integer apprId, String categoryFlag) {
		return getSession(sessionFactory)
				.createCriteria(IDP_SubSectionMaster.class)
				.add(Restrictions.eq(YumPmsConstants.IS_ACTIVE, true))
				.add(Restrictions.eq(YumPmsConstants.APPR_ID, apprId))
				.list();
	}

	/**
	 * @param apprId 
	 * @param SessionFactory sessionFactory
	 * @return List<IDP_SubSubSectionMaster> listOfSubSubSectionMaster
	 */
	public List<IDP_SubSubSectionMaster> getSubSubSectionList(Integer apprId) {
		return getSession(sessionFactory)
				.createCriteria(IDP_SubSubSectionMaster.class)
				.add(Restrictions.eq(YumPmsConstants.IS_ACTIVE, true))
				.add(Restrictions.eq(YumPmsConstants.APPR_ID, apprId))
				.list();
	}

	/**
	 * @param SessionFactory sessionFactory
	 * @return List<IDP_SubSubSectionMaster> listOfSubSubSubSectionMaster
	 */
	public List<IDP_SubSubSubSectionMaster> getSubSubSubSectionList() {
		return getSession(sessionFactory)
				.createCriteria(IDP_SubSubSubSectionMaster.class)
				.add(Restrictions.eq(YumPmsConstants.IS_ACTIVE, true))
				.list();		
	}

	public IDP_SubSectionMaster saveIdpSubCategory(IDP_SubSectionMaster subSection) throws YumPMSDataSaveException {
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			subSection = (IDP_SubSectionMaster)currentSession.merge(subSection);
			YumHibernateUtilServices.commitTransaction(currentSession);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to save or update IDP_SubSectionMaster", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return subSection;
	}

	public IDP_SubSubSectionMaster saveIdpSubSubCategory(IDP_SubSubSectionMaster subSubSection) throws YumPMSDataSaveException {
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			subSubSection = (IDP_SubSubSectionMaster)currentSession.merge(subSubSection);
			YumHibernateUtilServices.commitTransaction(currentSession);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession);
			throw new YumPMSDataSaveException("Failed to save or update IDP_SubSubSectionMaster", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return subSubSection;
	}

	public IDP_SubSubSubSectionMaster saveIdpSubSubSubCategory(IDP_SubSubSubSectionMaster subSubSubSection) throws YumPMSDataSaveException {
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			subSubSubSection = (IDP_SubSubSubSectionMaster)currentSession.merge(subSubSubSection);
			YumHibernateUtilServices.commitTransaction(currentSession);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession); 
			throw new YumPMSDataSaveException("Failed to save or update IDP_SubSubSubSectionMaster", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return subSubSubSection;
	}
	
	public List<IDP_SuggestedGoalMaster> getSuggestedGoalList() {
		return getSession(sessionFactory)
				.createCriteria(IDP_SuggestedGoalMaster.class)
				.add(Restrictions.eq(YumPmsConstants.IS_ACTIVE, true))
				.list();		
	}
	
	public List<IDP_SuggestedDevelopmentPlanMaster> getSuggestedDevelopmentPlanList() {
		return getSession(sessionFactory)
				.createCriteria(IDP_SuggestedDevelopmentPlanMaster.class)
				.add(Restrictions.eq(YumPmsConstants.IS_ACTIVE, true))
				.list();		
	}
	
	public IDP_SuggestedGoalMaster saveIdpSuggestedGaol(IDP_SuggestedGoalMaster subSubSubSection) throws YumPMSDataSaveException {
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			subSubSubSection = (IDP_SuggestedGoalMaster)currentSession.merge(subSubSubSection);
			YumHibernateUtilServices.commitTransaction(currentSession);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession); 
			throw new YumPMSDataSaveException("Failed to save or update IDP_SubSubSubSectionMaster", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return subSubSubSection;
	}
	
	public IDP_SuggestedDevelopmentPlanMaster saveIdpDevelopmentPlanList(IDP_SuggestedDevelopmentPlanMaster suggestedDevelopmentPlan) throws YumPMSDataSaveException {
		Session currentSession = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try {
			suggestedDevelopmentPlan = (IDP_SuggestedDevelopmentPlanMaster)currentSession.merge(suggestedDevelopmentPlan);
			YumHibernateUtilServices.commitTransaction(currentSession);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(currentSession); 
			throw new YumPMSDataSaveException("Failed to save or update IDP_SuggestedDevelopmentPlanMaster", e);
		} finally {
			YumHibernateUtilServices.clearSession(currentSession);
		}
		return suggestedDevelopmentPlan;
	}

	public Set<Integer> getRemovableCategoryIds(List<Integer> sectionIds) {
		sectionIds = new ArrayList<>(sectionIds);
		String sql = String.format("SELECT DISTINCT IDP_SECTION_ID FROM IDP_SECTION_DTL WHERE IDP_SECTION_ID IN (:%s)", 
				YumPmsConstants.IDP_SECTION_IDS);
		List<Integer> resultId = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(sql)
				.setParameterList(YumPmsConstants.IDP_SECTION_IDS, sectionIds)
				.list();
		sectionIds.removeAll(resultId);
		return new HashSet<>(sectionIds);
	}

	public Set<Integer> getRemovableSubCategoryIds(List<Integer> subSectionIds) {
		subSectionIds = new ArrayList<>(subSectionIds);
		String sql = String.format("SELECT DISTINCT IDP_SUB_SECTION_ID FROM IDP_SECTION_DTL WHERE IDP_SUB_SECTION_ID IN (:%s)", 
				YumPmsConstants.IDP_SUB_SECTION_IDS);
		List<Integer> resultId = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(sql)
				.setParameterList(YumPmsConstants.IDP_SUB_SECTION_IDS, subSectionIds)
				.list();
		subSectionIds.removeAll(resultId);
		return new HashSet<>(subSectionIds);
	}

	public Set<Integer> getRemovableSubSubCategoryIds(List<Integer> subSubSectionIds) {
		subSubSectionIds = new ArrayList<>(subSubSectionIds);
		String sql = String.format("SELECT DISTINCT IDP_SUB_SUB_SECTION_ID FROM IDP_SECTION_DTL WHERE IDP_SUB_SUB_SECTION_ID IN (:%s)", 
				YumPmsConstants.IDP_SUB_SUB_SECTION_IDS);
		List<Integer> resultId = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(sql)
				.setParameterList(YumPmsConstants.IDP_SUB_SUB_SECTION_IDS, subSubSectionIds)
				.list();
		subSubSectionIds.removeAll(resultId);
		return new HashSet<>(subSubSectionIds);
	}

	public Set<Integer> getRemovableSubSubSubCategoryIds(List<Integer> subSubSubSectionIds) {
		subSubSubSectionIds = new ArrayList<>(subSubSubSectionIds);
		String sql = String.format("SELECT DISTINCT IDP_SUB_SUB_SUB_SECTION_ID FROM IDP_SECTION_DTL WHERE IDP_SUB_SUB_SUB_SECTION_ID IN (:%s)", 
				YumPmsConstants.IDP_SUB_SUB_SUB_SECTION_IDS);
		List<Integer> resultId = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(sql)
				.setParameterList(YumPmsConstants.IDP_SUB_SUB_SUB_SECTION_IDS, subSubSubSectionIds)
				.list();
		subSubSubSectionIds.removeAll(resultId);
		return new HashSet<>(subSubSubSectionIds);
	}
	
	public Set<Integer> getRemovableSelectedSuggestedGoalIds(List<Integer> suggestedGoalIds) {
		suggestedGoalIds = new ArrayList<>(suggestedGoalIds);
		String sql = String.format("SELECT DISTINCT IDP_SELECTED_SUGGESTED_GOAL_ID FROM IDP_SELECTED_SUGGESTED_GOAL_DTL WHERE IDP_SELECTED_SUGGESTED_GOAL_ID IN (:%s)", 
				YumPmsConstants.PARAM_SELECTED_SUGGESTED_GOAL_IDS);
		List<Integer> resultId = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(sql)
				.setParameterList(YumPmsConstants.PARAM_SELECTED_SUGGESTED_GOAL_IDS, suggestedGoalIds)
				.list();
		suggestedGoalIds.removeAll(resultId);
		return new HashSet<>(suggestedGoalIds);
	}
	
	public Set<Integer> getRemovableSelectedSuggestedDevGoalIds(List<Integer> suggestedDevelopmentIds) {
		suggestedDevelopmentIds = new ArrayList<>(suggestedDevelopmentIds);
		String sql = String.format("SELECT DISTINCT IDP_SUGGESTED_DEVELOPMENT_PLAN_SECTION_ID FROM idp_developmentactionplan_dtl WHERE IDP_SUGGESTED_DEVELOPMENT_PLAN_SECTION_ID IN (:%s)", 
				YumPmsConstants.PARAM_SELECTED_SUGGESTED_DEV_PLAN_IDS);
		List<Integer> resultId = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(sql)
				.setParameterList(YumPmsConstants.PARAM_SELECTED_SUGGESTED_DEV_PLAN_IDS, suggestedDevelopmentIds)
				.list();
		suggestedDevelopmentIds.removeAll(resultId);
		return new HashSet<>(suggestedDevelopmentIds);
	}
}