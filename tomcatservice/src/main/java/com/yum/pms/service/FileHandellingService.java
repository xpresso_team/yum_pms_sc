package com.yum.pms.service;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.yum.pms.dao.AppraisalPeriod;
import com.yum.pms.dao.EmployeeNew;
import com.yum.pms.dao.GlobalLetter;
import com.yum.pms.dao.ReferenceDoc;
import com.yum.pms.exception.YumPMSDataSaveException;
import com.yum.pms.utils.YumPmsConstants;
import com.yum.pms.utils.YumPmsUtils;

@Service
@SuppressWarnings("unchecked")
public class FileHandellingService {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileHandellingService.class);

	private <T> boolean deleteById(Class<T> type, Serializable id, Session session) {

		T persistentInstance = session.load(type, id);
		if (persistentInstance != null) {
			session.delete(persistentInstance);
			return true;
		}
		return false;
	}

	public ReferenceDoc saveFileDetails(ReferenceDoc referenceDoc, SessionFactory sessionFactory) throws YumPMSDataSaveException {

		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		try{
			referenceDoc = (ReferenceDoc) session.merge(referenceDoc);
			YumHibernateUtilServices.commitTransaction(session);     
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException("Failed to save File", e);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return referenceDoc;
	}

	public List<ReferenceDoc> getUserfilesdetailsList(ReferenceDoc referenceDoc, SessionFactory sessionFactory) {

		return
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(ReferenceDoc.class)
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD_ID, referenceDoc.getAppraisalPeriodId()))
				.add(Restrictions.or(Restrictions.eq(YumPmsConstants.IS_ADMIN_DOC, true), 
						Restrictions.eq(YumPmsConstants.EMP_ID, referenceDoc.getEmpId())))
				.list();  
	}

	public List<ReferenceDoc> getAdminfilesdetailsList(ReferenceDoc referenceDoc, SessionFactory sessionFactory) {

		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(ReferenceDoc.class)
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD_ID, referenceDoc.getAppraisalPeriodId()))
				.add(Restrictions.eq(YumPmsConstants.IS_ADMIN_DOC, true))
				.list();  
	}

	public ReferenceDoc getFileDetailsById(String fileId, SessionFactory sessionFactory) {

		return 
				(ReferenceDoc)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(ReferenceDoc.class)
				.add(Restrictions.eq("fileId", fileId))
				.uniqueResult();  
	}

	public Boolean deleteFileById(String id, SessionFactory sessionFactory) {

		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		boolean deleteFlag = deleteById(ReferenceDoc.class, id, session); 		
		YumHibernateUtilServices.commitTransaction(session);
		return deleteFlag;
	}
	
	public List<GlobalLetter> getGlobalLetter(Integer apprId, Integer empId, SessionFactory sessionFactory) {

		Criteria criteria = YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(GlobalLetter.class)
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, new AppraisalPeriod(apprId)));
		if(YumPmsUtils.isGreaterThanZero(empId)) {
			criteria.add(Restrictions.eq(YumPmsConstants.EMPLOYEE_MODEL, new EmployeeNew(empId)));
		}
		return YumPmsUtils.getEmptyListIfNull(criteria.list());
	}

}
