/******************************************************************** 
 * Service class Controller - implements IYumHibernateService 
 * All Hibernate Service calls to be invoked from methods in this class
 * 
 * http://www.abzooba.com
 * ------------------- 
 * Created By: jayanta.biswas
 * Last Updated By: sudipta.chandra
 * Last Updated On: 23-Sep-2017 (11:34:34 AM) 
 *
 * The software and related user documentation are protected under 
 * copyright laws and remain the sole property of Abzooba. 
 * 
 * Technical support is available via the Abzooba website at 
 * http://www.abzooba.com
 **************************************************************************/


package com.yum.pms.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.yum.idp.domain.IdpCategory;
import com.yum.idp.domain.IdpSubCategory;
import com.yum.idp.domain.IdpSubSubCategory;
import com.yum.idp.domain.IdpSubSubSubCategory;
import com.yum.idp.domain.IdpSuggestedDevelopmentActionPlan;
import com.yum.idp.domain.IdpSuggestedGoal;
import com.yum.idp.service.IDPUtilService;
import com.yum.pa.domain.AdminPaConfig;
import com.yum.pa.domain.PromotionConfig;
import com.yum.pa.domain.PromotionCriteria;
import com.yum.pa.service.PADaoImpl;
import com.yum.pms.dao.AppraisalPeriod;
import com.yum.pms.dao.Cascaded;
import com.yum.pms.dao.CasecadeEmp;
import com.yum.pms.dao.CompRevisedSalaryEffectiveDuration;
import com.yum.pms.dao.EmpToPmsRole;
import com.yum.pms.dao.EmpToPmsRoleId;
import com.yum.pms.dao.EmployeeGradeMaster;
import com.yum.pms.dao.EmployeeNew;
import com.yum.pms.dao.EmployeeYearWise;
import com.yum.pms.dao.GlobalLetter;
import com.yum.pms.dao.Goal;
import com.yum.pms.dao.GoalCeo;
import com.yum.pms.dao.GoalOrg;
import com.yum.pms.dao.GoalSection;
import com.yum.pms.dao.GoalSheet;
import com.yum.pms.dao.GoalSheetStatusWorkFlow;
import com.yum.pms.dao.GoalSheetWorkflowHistory;
import com.yum.pms.dao.GoalStatus;
import com.yum.pms.dao.IDP_SectionMaster;
import com.yum.pms.dao.IDP_SubSectionMaster;
import com.yum.pms.dao.IDP_SubSubSectionMaster;
import com.yum.pms.dao.IDP_SubSubSubSectionMaster;
import com.yum.pms.dao.IDP_SuggestedDevelopmentPlanMaster;
import com.yum.pms.dao.IDP_SuggestedGoalMaster;
import com.yum.pms.dao.Notification;
import com.yum.pms.dao.PA_ConfigIF;
import com.yum.pms.dao.PA_ConfigPromotionCriteria;
import com.yum.pms.dao.PA_ConfigRatingBellCurveIFRange;
import com.yum.pms.dao.PA_ConfigTF;
import com.yum.pms.dao.PA_RatingMaster;
import com.yum.pms.dao.PmsRole;
import com.yum.pms.dao.ReferenceDoc;
import com.yum.pms.dao.Tagged;
import com.yum.pms.dao.TaggedEmp;
import com.yum.pms.domain.CascadedDetails;
import com.yum.pms.domain.ClientEmployee;
import com.yum.pms.domain.EmailDomain;
import com.yum.pms.domain.EmployeeCeoGoal;
import com.yum.pms.domain.EmployeeCurrentDetails;
import com.yum.pms.domain.EmployeeGoal;
import com.yum.pms.domain.EmployeeGoalHistory;
import com.yum.pms.domain.EmployeeGoalMatrix;
import com.yum.pms.domain.EmployeeGoalSection;
import com.yum.pms.domain.EmployeeGoalSheet;
import com.yum.pms.domain.EmployeeGoalStatus;
import com.yum.pms.domain.EmployeeOrgGoal;
import com.yum.pms.domain.EmployeeOrgGoalCeoGoal;
import com.yum.pms.domain.EmployeeRoleDetails;
import com.yum.pms.domain.NotificationSettings;
import com.yum.pms.domain.ReporteeCascadedEmp;
import com.yum.pms.domain.ReporteeTaggedEmp;
import com.yum.pms.domain.StatusWorkFlow;
import com.yum.pms.domain.TaggedDetails;
import com.yum.pms.exception.CommonException;
import com.yum.pms.exception.YumPMSDataAccessException;
import com.yum.pms.exception.YumPMSDataSaveException;
import com.yum.pms.utils.DateTimeUtils;
import com.yum.pms.utils.ResourceUtils;
import com.yum.pms.utils.SendMail;
import com.yum.pms.utils.YumPmsConstants;
import com.yum.pms.utils.YumPmsUtils;

@Service
@SuppressWarnings("unchecked")
public class YumHibernateServiceImpl implements IYumHibernateService {

	@Autowired
	private GoalServices goalService;	
	
	@Autowired
	private AdminServices adminService;	
	
	@Autowired
	private PmsRoleServices roleService;
	
	@Autowired
	private TaggedService taggedService;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private YumHibernateUtilServices util;
	
	@Autowired
	private CascadedService cascadedService;
	
	@Autowired
	private EmployeeServices employeeService;	
	
	@Autowired
	private NotificationService notificationService;
	
	@Autowired
	private StatusWorkFlowService statusWorkFlowService;	
	
	@Autowired
	private AppraisalService appraisalService;
	
	@Autowired
	private FileHandellingService fileHandellingService;
	
	@Autowired
	private IDPUtilService idpUtil;
	
	@Autowired
	private PADaoImpl paDao;

	@Autowired
	private SendMail mailSender;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(YumHibernateServiceImpl.class);


	@Override
	public Map<String, Object> getDropDownList(String item) {
		return util.getDropDownList(item, sessionFactory);
	}

	@Override
	public Map<Integer, String> getEmployeeListByManagerId(String emailID, Integer apprId, Boolean nonDr) {
		
		EmployeeNew empMgr = employeeService.getEmployeeByEmail(emailID);
		Map<Integer, String> employeeListWithId = null;
		if(null == nonDr ||  !nonDr) {
			employeeListWithId = employeeService.getEmployeeListByManagerId(empMgr.getEmpId(), apprId); 
		} else {
			employeeListWithId = employeeService.getEmployeeListByManagerIdForNonDR(empMgr.getEmpId(), apprId);
		}
		return employeeListWithId;
	}

	@Override
	public EmployeeCurrentDetails getEmployeeDetailsByUserId(String userId, Integer apprId, String conditionType) {
		
		if(!YumPmsUtils.isGreaterThanZero(apprId)) {
			apprId = appraisalService.getDefaultAppraisalService().getAppraisalPeriodId();
		}
		return employeeService.getEmployeeDetailsByUserId(userId, apprId, conditionType); 
	}

	@Override
	public PmsRole getRoleDetailsByRoleId(String roleId) {
		return roleService.getRoleDetailsByRoleId(roleId, sessionFactory);
	}
	
	@Override
	public EmployeeGoalSheet getGoalSheetDetailsByEmployeeId(Integer empId, Integer apprId, boolean isFromPDF) throws YumPMSDataAccessException {

		EmployeeNew employee = employeeService.getEmployeeById(empId);
		AppraisalPeriod apprPeriod = getAppraisalPeriod(apprId);
		EmployeeYearWise empYearWise = employeeService.getEmpYearWiseData(empId, apprId);
		EmployeeNew supervisor = getSupervisor(empYearWise);
		Integer supervisorId = supervisor.getEmpId();
		String supervisorName = supervisor.getEmpName();
		String supervisorDesig = StringUtils.EMPTY;
		if(YumPmsUtils.isGreaterThanZero(supervisorId)) {
			EmployeeYearWise supEmpYearWise = employeeService.getEmpYearWiseData(supervisorId, apprId);
			if(supEmpYearWise.getDesgMaster() != null) {
				supervisorDesig = supEmpYearWise.getDesgMaster().getDesignation();
			}
		}
		GoalSheet goalSheet = updateGoalSheet(employee, apprPeriod);
		EmployeeGoalSheet empGoalSheet = new EmployeeGoalSheet();
		empGoalSheet.setAppraisal_id(apprPeriod.getAppraisalPeriodId());
		empGoalSheet.setPeriodDesc(apprPeriod.getPeriodDesc());
		empGoalSheet.setAppStartDate(apprPeriod.getPeriodStartYyyymm());
		empGoalSheet.setAppEndDate(apprPeriod.getPeriodEndYyyymm());
		empGoalSheet.setGoalLabel(apprPeriod.getGoalLabel());
		empGoalSheet.setEmp_id(empId);
		empGoalSheet.setGoal_sheet_id(goalSheet.getGoalSheetId());
		empGoalSheet.setGoalStatus(new EmployeeGoalStatus(getGoalStatus(goalSheet)));
		empGoalSheet.setSupervisor_id(supervisorId);
		empGoalSheet.setSupervisor_name(supervisorName);
		empGoalSheet.setSupervisor_designation(supervisorDesig);
		empGoalSheet.setEmpName(employee.getEmpName());
		empGoalSheet.setEmpDesignation(getEmployeeFieldValue(empYearWise, YumPmsConstants.DESIGNATION));
		empGoalSheet.setFunctionName(getEmployeeFieldValue(empYearWise, YumPmsConstants.FUNCTION_NAME));
		empGoalSheet.setFunctionalLtsId(YumPmsUtils.toInteger(getEmployeeFieldValue(empYearWise, YumPmsConstants.FUNCTIONAL_LTS_ID)).getValue());
		empGoalSheet.setFunctionalLtsName(getEmployeeFieldValue(empYearWise, YumPmsConstants.FUNCTION_LT_NAME));
		//--------get Org-goal		
		List<GoalOrg> orgGoalList = goalService.getOrgGoalList(apprPeriod.getAppraisalPeriodId(), sessionFactory);
		for (GoalOrg goalOrg : orgGoalList) {
			
			EmployeeGoalMatrix empGoalMatrix = new EmployeeGoalMatrix();
			
			EmployeeOrgGoal employeeOrgGoal = new EmployeeOrgGoal(goalOrg.getOrgGoalId());
			employeeOrgGoal.setOrgGoalDesc(goalOrg.getOrgGoalDesc());
			employeeOrgGoal.setTarget(goalOrg.getTarget());
			employeeOrgGoal.setWeightage(goalOrg.getWeightage());
			employeeOrgGoal.setAppraisalPeriodId(goalOrg.getAppraisalPeriod().getAppraisalPeriodId());
			employeeOrgGoal.setYlbFlag(goalOrg.getYlbFlag());
			employeeOrgGoal.setActive(goalOrg.getActive());

			empGoalMatrix.setOrgGoal(employeeOrgGoal);

			//--------get Ceo-goal
			GoalCeo goalCeo = goalOrg.getGoalCeo() != null ? goalOrg.getGoalCeo() : new GoalCeo();			
			empGoalMatrix.setCeoGoal(new EmployeeCeoGoal(goalCeo.getCeoGoalId(), goalCeo.getCeoGoalDesc(), goalCeo.getActive()));

			//--------get supervisor goal
			EmployeeGoal supGoal = populateSupervisorGoal(supervisor, supervisorId, apprPeriod.getAppraisalPeriodId(), 
					goalOrg.getOrgGoalId(), goalCeo.getCeoGoalId());
			empGoalMatrix.setSupGoal(supGoal);	
			//--------get Functional goal
			if(empYearWise != null && empYearWise.getFunctionalLt() != null) {
				EmployeeGoal funGoal = populateFunctionLtGoal(empYearWise.getFunctionalLt().getEmpId(), apprPeriod.getAppraisalPeriodId(), 
						goalOrg.getOrgGoalId(), goalCeo.getCeoGoalId());
				empGoalMatrix.setFuntionalGoal(funGoal);
			}
			//--------------- get My Goal
			EmployeeGoal empGoal = populateEmpGoal(isFromPDF, employee, apprPeriod.getAppraisalPeriodId(), 
					goalOrg.getOrgGoalId(), goalCeo.getCeoGoalId());
			empGoalMatrix.setMyGoal(empGoal);
			
			empGoalSheet.getEmpGoalMatrix().add(empGoalMatrix);
		}
		//------get Custom-Goal		----------------------------------------------------------------------------------
		List<Goal> customGoalList = goalService.getCustomGoalList(employee, apprPeriod, sessionFactory);
		for(Goal customGoal : customGoalList) {
			
			EmployeeGoalMatrix matrix = new EmployeeGoalMatrix();
			matrix.setMyGoal(new EmployeeGoal(customGoal.getGoalId(), populateCustomGoal(customGoal)));
			
			empGoalSheet.getEmpGoalMatrix().add(matrix);
		}
		return empGoalSheet;
	}
	
	private String getEmployeeFieldValue(EmployeeYearWise empYearWise, String fieldName) {
		
		if(empYearWise == null || StringUtils.isBlank(fieldName)) {
			return StringUtils.EMPTY;
		}
		String returnValue = null;
		switch(fieldName) {
		case YumPmsConstants.DESIGNATION:
			returnValue = empYearWise.getDesgMaster() != null ? empYearWise.getDesgMaster().getDesignation() : null;
			break;
		case YumPmsConstants.FUNCTION_NAME:
			returnValue = empYearWise.getFunctionMaster() != null ? empYearWise.getFunctionMaster().getFunctionName() : null;
			break;
		case YumPmsConstants.FUNCTIONAL_LTS_ID:
			returnValue = empYearWise.getFunctionalLt() != null ? YumPmsUtils.toString(empYearWise.getFunctionalLt().getEmpId()) : null;
			break;
		case YumPmsConstants.FUNCTION_LT_NAME:
			returnValue = empYearWise.getFunctionalLt() != null ? empYearWise.getFunctionalLt().getEmpName() : null;
			break;
		default:
			returnValue = null;
			break;
		}
		return StringUtils.trimToEmpty(returnValue);
	}

	@Override
	public List<ClientEmployee> getAllEmployeeList(Integer empId, Integer apprId, Boolean isDeActive) {
		
		if(YumPmsUtils.isNullOrZero(apprId)) {
			apprId = appraisalService.getDefaultAppraisalService().getAppraisalPeriodId();
		}
		return employeeService.getAllEmployeeList(empId, apprId, isDeActive);
	}

	@Override
	public EmployeeGoalSheet saveGoalSheet(EmployeeGoalSheet employeeGoalSheet) throws YumPMSDataSaveException {		

		boolean bGoalSheetChanged = false;
		GoalSheet goalSheet;
		if(YumPmsUtils.isGreaterThanZero(employeeGoalSheet.getGoal_sheet_id())) {
			goalSheet = goalService.getGoalSheetbyId(employeeGoalSheet.getGoal_sheet_id(), sessionFactory);
			goalSheet.setUpdatedBy(employeeGoalSheet.getCreatedBy());
		} else {
			bGoalSheetChanged = true;
			goalSheet = new GoalSheet();
			goalSheet.setEmployee(new EmployeeNew(employeeGoalSheet.getEmp_id()));
			goalSheet.setCreatedBy(employeeGoalSheet.getCreatedBy());
		}
		GoalStatus goalStatus = new GoalStatus(employeeGoalSheet.getGoalStatus().getGoalStatusId());
		goalSheet.setGoalStatus(goalStatus);
		//Save and get saving object(GoalSheet).
		goalSheet = goalService.saveGoalSheet(goalSheet, sessionFactory); 

		//update goal sheet status history
		GoalSheetWorkflowHistory gsWkflwHistory = new GoalSheetWorkflowHistory();
		gsWkflwHistory.setGoalSheetId(goalSheet.getGoalSheetId());
		gsWkflwHistory.setAppraisalPeriodId(employeeGoalSheet.getAppraisal_id());
		gsWkflwHistory.setGoalStatusId(goalStatus.getGoalStatusId());
		gsWkflwHistory.setEmpId(employeeGoalSheet.getEmp_id());
		gsWkflwHistory.setCreatedDate(new Date());
		gsWkflwHistory.setCreatedBy(employeeService.getEmployeeById(employeeGoalSheet.getEmp_id()).getEmpName());
		goalService.saveGoalSheetHistory(gsWkflwHistory, sessionFactory);

		// 	Populate Sending Object==============================================================
		EmployeeGoalSheet sendingGoalSheet = new EmployeeGoalSheet();
		sendingGoalSheet.setGoal_sheet_id(goalSheet.getGoalSheetId());
		sendingGoalSheet.setEmp_id(employeeGoalSheet.getEmp_id());
		sendingGoalSheet.setSupervisor_id(employeeGoalSheet.getSupervisor_id());
		sendingGoalSheet.setAppraisal_id(employeeGoalSheet.getAppraisal_id());
		sendingGoalSheet.setGoalLabel(employeeGoalSheet.getGoalLabel());
		sendingGoalSheet.setAppStartDate(employeeGoalSheet.getAppStartDate());
		sendingGoalSheet.setAppEndDate(employeeGoalSheet.getAppEndDate());

		sendingGoalSheet.setSupervisor_name(employeeGoalSheet.getSupervisor_name());
		sendingGoalSheet.setSupervisor_designation(employeeGoalSheet.getSupervisor_designation());
		sendingGoalSheet.setGoalStatus(employeeGoalSheet.getGoalStatus());
		//============================================================================================	
		if(CollectionUtils.isEmpty(employeeGoalSheet.getEmpGoalMatrix())) {
			return sendingGoalSheet;
		}
		List<EmployeeGoalMatrix> empGoalMatrix = 
				employeeGoalSheet.getEmpGoalMatrix().stream()
				.filter(matrix -> matrix.getMyGoal() != null)
				.collect(Collectors.toList());
		empGoalMatrix = CollectionUtils.isEmpty(empGoalMatrix) ? Collections.emptyList() : empGoalMatrix;
		for(EmployeeGoalMatrix matrix : empGoalMatrix) {
			
			EmployeeGoalMatrix sendingGoalMatrix = new EmployeeGoalMatrix();
			List<GoalSection> dbGoalSectionList = null;
			EmployeeGoal myGoal = matrix.getMyGoal();
			LOGGER.debug("Processing Goal: {}", myGoal.getGoalId());	
			EmployeeGoal sendingMyGoal = new EmployeeGoal(); //SC21112017
			Goal goal;
			Integer tempGoalId = goalService.getGoalByOrgGoalGoalSheetID(matrix.getOrgGoal().getOrgGoalId(), goalSheet, sessionFactory);
			if(YumPmsUtils.isGreaterThanZero(tempGoalId)) {					
				goal = goalService.getGoalbyId(tempGoalId, sessionFactory);
				// fetching db goalSectionList against goal_id for deletion
				dbGoalSectionList = goalService.getGoalSectionListbyGoalId(goal, sessionFactory);		
				goal.setUpdatedBy(employeeGoalSheet.getCreatedBy());
			} else { 
				//eoc SC21112017
				bGoalSheetChanged = true;
				goal = createNewGoal(employeeGoalSheet.getCreatedBy(), goalSheet, matrix, myGoal);
			}
			for(EmployeeGoalSection section : myGoal.getEmployeeGoalSectionList()) {
				GoalSection goalsection;
				LOGGER.debug("Processing Goal Section: {}", section.getGoalSectionId());
				if(YumPmsUtils.isGreaterThanZero(section.getGoalSectionId())) {
					goalsection = goalService.getGoalSectionById(section.getGoalSectionId(), sessionFactory);	
					goalsection.setUpdatedBy(employeeGoalSheet.getCreatedBy());
					bGoalSheetChanged = isGoalSheetChanged(bGoalSheetChanged, section, goalsection);
				} else {
					bGoalSheetChanged = true;
					goalsection = new GoalSection();
					goalsection.setCreatedBy(employeeGoalSheet.getCreatedBy());
				}
				String previousSummary = StringUtils.trimToEmpty(goalsection.getGoalSectionSummary());
				String previousTimeLine = StringUtils.trimToEmpty(DateTimeUtils.format(goalsection.getGoalSectionTimeline()));
				goalsection.setGoalSectionSummary(section.getGoalSectionSummary());
				goalsection.setGoalSectionSupRemarks(section.getSupervisorRemarks());
				goalsection.setGoalSectionTimeline(DateTimeUtils.parse(section.getGoalSectionTimeLine()));	
				goalsection.setGoal(goal);
				goalsection = goalService.saveGoalSection(goalsection, sessionFactory); //Save and get saving object(GoalSection)
				// Send email for Tag and Cascade edit
				proceedToSendEmail(section, previousSummary, previousTimeLine);
				setEmpGoalSectionList(employeeGoalSheet, sendingMyGoal, section, goalsection);
			}				
			sendingMyGoal.setGoalId(goal.getGoalId());
			// Populate Sending Object==============================================================				
			sendingGoalMatrix.setCeoGoal(matrix.getCeoGoal());
			sendingGoalMatrix.setOrgGoal(matrix.getOrgGoal());
			sendingGoalMatrix.setSupGoal(matrix.getSupGoal()); 
			sendingGoalMatrix.setFuntionalGoal(matrix.getFuntionalGoal());
			sendingGoalMatrix.setMyGoal(sendingMyGoal);			
			sendingGoalSheet.getEmpGoalMatrix().add(sendingGoalMatrix);			
			//  Delete process of Goal sections
			deleteGoalSections(dbGoalSectionList, myGoal);
		
		}
		return sendingGoalSheet;
	}

	@Override
	public List<TaggedDetails> getTaggedInfo(Integer goalSectionId) {

		List<TaggedDetails> taggedDetalsList = new ArrayList<>();
		List<Tagged> taggedList = taggedService.getTaggedDetailsByGoalSection(new GoalSection(goalSectionId), sessionFactory);
		if(CollectionUtils.isNotEmpty(taggedList))	{	 
			for(Tagged tagged: taggedList) {
				TaggedDetails taggedDetails = new TaggedDetails();

				Set<TaggedEmp> empSet = tagged.getTaggedEmp();
				for(TaggedEmp empTagged: empSet) {	
					Integer empId = empTagged.getEmployee().getEmpId();
					String empName = StringUtils.trimToEmpty(empTagged.getEmployee().getEmpName());
					taggedDetails.getTagToEmployeeMap().put(empId, empName + " (" + empId + ")");
				}			

				taggedDetails.setTagId(tagged.getTagId());
				taggedDetails.setGoalSectionId(tagged.getGoalSection().getGoalSectionId());
				taggedDetails.setEditOfGoalSectionSummary(tagged.getEditedGsSummaryToEmp());
				taggedDetails.setTaggedTimeline(DateTimeUtils.format(tagged.getTaggedTimeline()));
				taggedDetalsList.add(taggedDetails);			 
			}
		} else{
			TaggedDetails taggedDetails = new TaggedDetails();
			taggedDetails.setMessage("No Data Found");
			taggedDetalsList.add(taggedDetails);
		}
		return taggedDetalsList;
	}

	@Override
	public void updateTaskRemarks(Integer goalSecId, String goalSectionSupRemarks, String createdBy) throws YumPMSDataSaveException {
		goalService.updateRemarks(goalSecId, goalSectionSupRemarks, createdBy, sessionFactory);

	}

	@Override
	public List<CascadedDetails> getCascadedInfo(Integer goalSectionId) {

		List<CascadedDetails> cascadedDetailsList = new ArrayList<>();
		GoalSection goalSection = new GoalSection();
		goalSection.setGoalSectionId(goalSectionId);

		List<Cascaded> cascadedList = cascadedService.getCascadedDetailsByGoalSection(goalSection, sessionFactory);
		if(CollectionUtils.isNotEmpty(cascadedList))	{	 
			for(Cascaded cascaded: cascadedList) {
				CascadedDetails cascadedDetails = new CascadedDetails();

				Set<CasecadeEmp> empSet = cascaded.getCasecadedEmp();
				for(CasecadeEmp empCascaded: empSet) {				
					cascadedDetails.getCascadeToEmployeeMap().put(empCascaded.getEmployee().getEmpId(), empCascaded.getEmployee().getEmpName());
				}			

				cascadedDetails.setCascadeId(cascaded.getCascadeId());
				cascadedDetails.setGoalSectionId(cascaded.getGoalSection().getGoalSectionId());
				cascadedDetails.setEditedGsSummaryToEmp(cascaded.getEditedGsSummaryToEmp());
				cascadedDetails.setCascadedTimeline(DateTimeUtils.format(cascaded.getCascadedTimeline()));
				cascadedDetailsList.add(cascadedDetails);			 
			}
		} else{
			CascadedDetails cascadedDetails = new CascadedDetails();
			cascadedDetails.setMessage("No Data Found");
			cascadedDetailsList.add(cascadedDetails);
		}
		return cascadedDetailsList;
	}

	@Override
	public boolean saveGoalSheetStatusValues(EmployeeGoalStatus status) throws YumPMSDataSaveException {
		return adminService.saveGoalSheetStatusValues(status,sessionFactory);
	}

	@Override
	public TaggedDetails saveTaggedData(TaggedDetails taggedDetails) throws YumPMSDataSaveException {

		TaggedDetails sendingTaggedData = new TaggedDetails();
		Tagged tagged;
		List<TaggedEmp> dbTaggedEmpList = null;
		boolean isNewTag = false;

		//Save Tagged Details
		if (taggedDetails.getTagId() < 1) {
			LOGGER.debug("Creating new tag >>> Tag Id : {}", taggedDetails.getTagId());
			tagged = new Tagged();	
			tagged.setCreatedBy(taggedDetails.getCreatedBy());
			isNewTag = true;
		} else {
			LOGGER.debug("Updating existing tag >>> Tag id : {}", taggedDetails.getTagId());
			tagged = taggedService.getTaggedById(taggedDetails.getTagId(), sessionFactory);
			tagged.setUpdatedBy(taggedDetails.getCreatedBy());
			//pre-populate for processing deleted entities later
			dbTaggedEmpList = taggedService.getTaggedEmpListbyTagId(tagged, sessionFactory);  // fetching db taggedEmpList for tags
		}
		GoalSection goalSection = goalService.getGoalSectionById(taggedDetails.getGoalSectionId(), sessionFactory);
		EmployeeNew emp = goalSection.getGoal().getGoalSheet().getEmployee(); 			
		tagged.setEmployee(emp);

		goalSection.setGoalSectionId(taggedDetails.getGoalSectionId());				
		tagged.setGoalSection(goalSection);
		tagged.setEditedGsSummaryToEmp(taggedDetails.getEditOfGoalSectionSummary());
		tagged.setTaggedTimeline(DateTimeUtils.parse(taggedDetails.getTaggedTimeline()));
		tagged.setAppraisalPeriod(goalService.getAppraisalPeriodFromGoalSection(goalSection, sessionFactory));

		tagged = taggedService.saveTagData(tagged, sessionFactory); //Save and get saving object(Tagged). 

		//Populate Sending Object======================================================
		sendingTaggedData.setTagId(tagged.getTagId().intValue());
		sendingTaggedData.setEditOfGoalSectionSummary(tagged.getEditedGsSummaryToEmp());
		sendingTaggedData.setTaggedTimeline(DateTimeUtils.format(tagged.getTaggedTimeline()));
		sendingTaggedData.setGoalSectionId(goalSection.getGoalSectionId());
		//Save TaggedEmp Details
		List<TaggedEmp> taggedEmpList = new ArrayList<>();	
		for(Integer taggedEmpID : taggedDetails.getTagToEmployeeMap().keySet()) {
			boolean isNewTaggedEmp = false;
			EmployeeNew toEmployee = new EmployeeNew(taggedEmpID);
			String taggedEmpName = taggedDetails.getTagToEmployeeMap().get(taggedEmpID);

			LOGGER.debug("Saving TaggedEmp: {} & : {}", taggedEmpID, taggedEmpName);
			TaggedEmp taggedEmp = taggedService.getTaggedToEmpById(tagged, toEmployee, sessionFactory);
			if (taggedEmp.getTaggedEmpEventId() == null) {
				isNewTaggedEmp = true;
			}
			taggedEmp.setTagged(tagged);	
			taggedEmp.setEmployee(toEmployee);	
			taggedEmp.setTagStatus(true);
			if(isNewTag || isNewTaggedEmp) {
				taggedEmp.setCreatedBy(taggedDetails.getCreatedBy());
			} else {
				taggedEmp.setUpdatedBy(taggedDetails.getCreatedBy()); 
			}
			taggedEmpList.add(taggedEmp);	
			taggedEmp = taggedService.saveTaggedEmp(taggedEmp, sessionFactory);	//Save and get saving object(TaggedEmp)

			// save Goal and GoalSection after tagging==========
			if(isNewTag || isNewTaggedEmp) {
				saveGoalSectionAfterTagging(sendingTaggedData, taggedEmpID);
			} else {
				int goalSectionIdToUpdate = taggedEmp.getGoalSection().getGoalSectionId();
				GoalSection goalSectionToUpdate = goalService.getGoalSectionById(goalSectionIdToUpdate, sessionFactory);
				goalSectionToUpdate.setGoalSectionSummary(sendingTaggedData.getEditOfGoalSectionSummary());
				goalSectionToUpdate.setGoalSectionTimeline(DateTimeUtils.parse(sendingTaggedData.getTaggedTimeline()));
				goalService.saveGoalSection(goalSectionToUpdate, sessionFactory);
			}
			//Populate Sending Object================================================				
			sendingTaggedData.getTagToEmployeeMap().put(taggedEmpID, taggedEmpName);	
		}	
		//mail sending while tagged to some employee============================
		String taggedToEmps = StringUtils.join(taggedDetails.getTagToEmployeeMap().keySet(), YumPmsConstants.COMMA);
		sendEmailForTagAndCascade(YumPmsConstants.TAG, emp.getEmpId(), taggedToEmps, tagged.getTagId());
		//Delete Tagged Employees
		if (CollectionUtils.isNotEmpty(dbTaggedEmpList)) {
			deleteTaggedEmps(dbTaggedEmpList, taggedEmpList);
		}	
		return sendingTaggedData;		
	}

	@Override
	public List<EmployeeGoalStatus> getGoalSheetStatusValues() {
		return adminService.getGoalSheetStatusValues(sessionFactory);
	}

	@Override
	public CascadedDetails saveCascadedData(CascadedDetails cascadedDetails) throws YumPMSDataSaveException {

		List<CasecadeEmp> dbCascadedEmpList = null;
		boolean isNewCascade = false;
		Cascaded cascaded = null;
		//Save Cascaded Details
		if (cascadedDetails.getCascadeId() < 1) {
			cascaded = new Cascaded();
			cascaded.setCreatedBy(cascadedDetails.getCreatedBy());
			isNewCascade = true;
		} else {
			cascaded = cascadedService.getCascadedById(cascadedDetails.getCascadeId(), sessionFactory);
			cascaded.setUpdatedBy(cascadedDetails.getCreatedBy());
			//pre populate for processing deleted entities later
			dbCascadedEmpList = cascadedService.getCascadedEmpListbyCascadeId(cascaded, sessionFactory);  // fetching db cascadeEmpList for deletion
		}
		GoalSection goalSection = goalService.getGoalSectionById(cascadedDetails.getGoalSectionId(), sessionFactory);
		AppraisalPeriod appraisalPeriod = goalService.getAppraisalPeriodFromGoalSection(goalSection, sessionFactory);

		EmployeeNew emp = goalSection.getGoal().getGoalSheet().getEmployee(); 			
		cascaded.setEmployee(emp);

		goalSection.setGoalSectionId(cascadedDetails.getGoalSectionId());
		cascaded.setGoalSection(goalSection);
		cascaded.setEditedGsSummaryToEmp(cascadedDetails.getEditedGsSummaryToEmp());
		cascaded.setCascadedTimeline(DateTimeUtils.parse(cascadedDetails.getCascadedTimeline()));
		cascaded.setAppraisalPeriod(appraisalPeriod);

		cascaded = cascadedService.saveCascadedData(cascaded, sessionFactory); //Save and get saving object(Cascaded). 

		//Populate Sending Object======================================================
		CascadedDetails sendingCascadedData = new CascadedDetails();
		sendingCascadedData.setCascadeId(cascaded.getCascadeId());
		sendingCascadedData.setEditedGsSummaryToEmp(cascaded.getEditedGsSummaryToEmp());
		sendingCascadedData.setCascadedTimeline(DateTimeUtils.format(cascaded.getCascadedTimeline()));
		sendingCascadedData.setGoalSectionId(goalSection.getGoalSectionId());
		//=============================================================================
		//Save CascadedEmp Details
		List<CasecadeEmp> cascadedEmpList = new ArrayList<>();
		Set<Integer> cascadedEmpIds = cascadedDetails.getCascadeToEmployeeMap().keySet();
		for(Integer cascadedEmpID : cascadedEmpIds) {

			EmployeeNew toEmployee = new EmployeeNew(cascadedEmpID);
			boolean isNewCascadedEmp = false;
			CasecadeEmp cascadedEmp = cascadedService.getCascadedEmpById(cascaded, toEmployee, sessionFactory);
			if(cascadedEmp.getCasecadedEmpEventId() == null) {
				isNewCascadedEmp = true;
			}
			cascadedEmp.setCasecaded(cascaded);			
			cascadedEmp.setEmployee(toEmployee);
			if(isNewCascade || isNewCascadedEmp) {
				cascadedEmp.setCreatedBy(cascadedDetails.getCreatedBy());
			} else {
				cascadedEmp.setUpdatedBy(cascadedDetails.getCreatedBy()); 
			}
			cascadedEmpList.add(cascadedEmp);	
			cascadedEmp = cascadedService.saveCascadedEmp(cascadedEmp, sessionFactory);	//Save and get saving object(CascadedEmp)	

			// save Goal and GoalSection after cascading==========
			if(isNewCascade || isNewCascadedEmp) {
				saveGoalSectionAfterCascading(sendingCascadedData, cascadedEmpID);
			} else {
				GoalSection goalSectionToUpdate = goalService.getGoalSectionById(cascadedEmp.getGoalSection().getGoalSectionId(), sessionFactory);
				goalSectionToUpdate.setGoalSectionSummary(sendingCascadedData.getEditedGsSummaryToEmp());
				goalSectionToUpdate.setGoalSectionTimeline(DateTimeUtils.parse(sendingCascadedData.getCascadedTimeline()));
				goalService.saveGoalSection(goalSectionToUpdate, sessionFactory);
			}
			//Populate Sending Object================================================				
			sendingCascadedData.getCascadeToEmployeeMap().put(cascadedEmp.getEmployee().getEmpId(), cascadedEmp.getEmployee().getEmpName());
		}	
		//mail sending while cascaded to some employee
		String strCascadedEmpIds = StringUtils.join(cascadedEmpIds, YumPmsConstants.COMMA);
		sendEmailForTagAndCascade(YumPmsConstants.CASCADE, emp.getEmpId(), strCascadedEmpIds, cascaded.getCascadeId()); 
		//Delete Cascaded Employees
		if(CollectionUtils.isNotEmpty(dbCascadedEmpList)) {
			deleteCascadedEmps(dbCascadedEmpList, cascadedEmpList);
		}
		return sendingCascadedData;
	}

	@Override
	public Boolean deleteTag(Integer tagid) throws YumPMSDataSaveException {

		try {
			taggedService.deleteTag(tagid, sessionFactory);
		} catch (Exception e) {
			return false;
		}		
		return true;
	}

	@Override
	public Boolean deleteCascade(Integer cascadeid) throws YumPMSDataSaveException {
		
		try {
			cascadedService.deleteCascade(cascadeid, sessionFactory);
		} catch (Exception e) {
			return false;
		}		
		return true;
	}

	public void saveGoalSectionAfterTagging(TaggedDetails taggedDetails, Integer empId) throws YumPMSDataSaveException {

		GoalSection mgrGoalSection = taggedService.getGoalSectionByTaggedID(taggedDetails.getTagId(), sessionFactory);
		Goal mgrGoal  = mgrGoalSection.getGoal();

		//get reportee org and ceo goals from mgr goal - they should be same
		Integer reporteeCeoGoalId = mgrGoal.getCeoGoalId();
		Integer reporteeOrgGoalId = mgrGoal.getOrgGoalId();
		GoalOrg reporteeOrgGoal = new GoalOrg();
		reporteeOrgGoal.setOrgGoalId(mgrGoal.getOrgGoalId());

		AppraisalPeriod appraisalPeriod = mgrGoal.getGoalSheet().getAppraisalPeriod();
		EmployeeNew reporteeEmp = employeeService.getEmployeeById(empId);
		GoalSheet reporteeGoalSheet = goalService.getGoalSheetbyEmpId(reporteeEmp, appraisalPeriod, sessionFactory);

		//if goal sheet not present
		if(reporteeGoalSheet == null || reporteeGoalSheet.getGoalSheetId() == 0) {	
			reporteeGoalSheet = new GoalSheet();
			reporteeGoalSheet.setAppraisalPeriod(appraisalPeriod);
			reporteeGoalSheet.setEmployee(reporteeEmp);
			GoalStatus goalStatus = mgrGoal.getGoalSheet().getGoalStatus();
			reporteeGoalSheet.setGoalStatus(goalStatus);
			reporteeGoalSheet.setActive(true);
			reporteeGoalSheet.setCreatedBy(taggedDetails.getCreatedBy());

			reporteeGoalSheet = goalService.saveGoalSheet(reporteeGoalSheet, sessionFactory);
		}

		//check for existence of goal - if not present then add	
		Goal reporteeGoal = null;
		if(reporteeOrgGoalId != null && reporteeOrgGoalId != 0 )
			reporteeGoal = goalService.getGoalByOrgCeoGoal(appraisalPeriod.getAppraisalPeriodId(), 
					reporteeOrgGoalId, reporteeCeoGoalId, reporteeEmp, sessionFactory);
		if(reporteeGoal == null) {
			reporteeGoal = new Goal();
			reporteeGoal.setCeoGoalId(reporteeCeoGoalId);
			reporteeGoal.setGoalSheet(reporteeGoalSheet);
			reporteeGoal.setOrgGoalId(reporteeOrgGoal.getOrgGoalId()>0?reporteeOrgGoal.getOrgGoalId():0);
			reporteeGoal.setCreatedBy(taggedDetails.getCreatedBy());
			//save reportee goal
			reporteeGoal = goalService.saveGoal(reporteeGoal, sessionFactory);
		}
		//add goalsection and get id
		GoalSection newReporteeGoalSection = new GoalSection();
		newReporteeGoalSection.setGoal(reporteeGoal);
		newReporteeGoalSection.setGoalSectionSummary(taggedDetails.getEditOfGoalSectionSummary());
		newReporteeGoalSection.setGoalSectionTimeline(DateTimeUtils.parse(taggedDetails.getTaggedTimeline()));
		newReporteeGoalSection.setCreatedBy(taggedDetails.getCreatedBy());
		newReporteeGoalSection = goalService.saveGoalSection(newReporteeGoalSection, sessionFactory);

		//update tagged to emp table
		Tagged tagged = taggedService.getTaggedById(taggedDetails.getTagId(), sessionFactory);
		TaggedEmp mgrTagEmp = taggedService.getTaggedToEmpById(tagged, reporteeEmp, sessionFactory);

		mgrTagEmp.setGoalSection(newReporteeGoalSection);
		mgrTagEmp.setUpdatedBy(taggedDetails.getCreatedBy());

		taggedService.saveTaggedEmp(mgrTagEmp, sessionFactory);
	}

	@Override
	public ReporteeTaggedEmp getEmpTaggedInfo(Integer tagId, Integer reporteeId) {

		Tagged tagged = taggedService.getTaggedById(tagId, sessionFactory);
		EmployeeNew reporteeEmployee = employeeService.getEmployeeById(reporteeId);
		TaggedEmp taggedEmp = taggedService.getReporteeTaggedInfoByTagId(tagged, reporteeEmployee, sessionFactory);
		ReporteeTaggedEmp reporteeTaggedEmp = new ReporteeTaggedEmp();
		reporteeTaggedEmp.setReporteeEmpId(reporteeId);
		reporteeTaggedEmp.setReporteeEmpName(reporteeEmployee.getEmpName());
		reporteeTaggedEmp.setTaggedByEmpId(tagged.getEmployee().getEmpId());
		reporteeTaggedEmp.setTaggedByEmpName(tagged.getEmployee().getEmpName());
		if(taggedEmp != null) {
			Boolean tagStatus = taggedEmp.getTagStatus();
			if(tagStatus != null)
				reporteeTaggedEmp.setTagAccepted(taggedEmp.getTagStatus());
			reporteeTaggedEmp.setTagRejectedDesc(taggedEmp.getTagRejactionDesc());
			reporteeTaggedEmp.setTagToGoalSectionId(taggedEmp.getGoalSection().getGoalSectionId());
			reporteeTaggedEmp.setTaggedGoalSectionTimeline(DateTimeUtils.format(taggedEmp.getGoalSection().getGoalSectionTimeline()));
			AppraisalPeriod appPeriod = taggedEmp.getTagged().getAppraisalPeriod();
			if (appPeriod!=null)
				reporteeTaggedEmp.setTaggedAppPeriod(appPeriod.getPeriodDesc());
			reporteeTaggedEmp.setTaggedGoalSectionSummary(taggedEmp.getGoalSection().getGoalSectionSummary());
		}
		if(tagged.getGoalSection() != null)
			reporteeTaggedEmp.setOriginalGoalSectionSummary(tagged.getGoalSection().getGoalSectionSummary());
		return reporteeTaggedEmp;
	}

	@Override
	public ReporteeCascadedEmp getEmpCascadedInfo(Integer cascadeId, Integer reporteeId) {

		Cascaded cascaded = cascadedService.getCascadedById(cascadeId, sessionFactory);
		EmployeeNew reporteeEmployee = employeeService.getEmployeeById(reporteeId);
		CasecadeEmp cascadedEmp = cascadedService.getReporteeCascadedInfoByCascadeId(cascaded, reporteeEmployee, sessionFactory);
		ReporteeCascadedEmp reporteeCascadedEmp = new ReporteeCascadedEmp();
		reporteeCascadedEmp.setReporteeEmpId(reporteeId);
		reporteeCascadedEmp.setReporteeEmpName(reporteeEmployee.getEmpName());
		reporteeCascadedEmp.setCascadedByEmpId(cascaded.getEmployee().getEmpId());
		reporteeCascadedEmp.setCascadedByEmpName(cascaded.getEmployee().getEmpName());
		if(cascadedEmp!=null) {
			reporteeCascadedEmp.setCascadeToGoalSectionId(cascadedEmp.getGoalSection().getGoalSectionId());
			reporteeCascadedEmp.setCascadedGoalSectionTimeline(DateTimeUtils.format(cascadedEmp.getGoalSection().getGoalSectionTimeline()));
			AppraisalPeriod appPeriod = cascadedEmp.getCasecaded().getAppraisalPeriod();
			if (appPeriod!=null)
				reporteeCascadedEmp.setCascadedAppPeriod(appPeriod.getPeriodDesc());
			reporteeCascadedEmp.setCascadedGoalSectionSummary(cascadedEmp.getGoalSection().getGoalSectionSummary());
		}
		if(cascaded.getGoalSection()!=null)
			reporteeCascadedEmp.setOriginalGoalSectionSummary(cascaded.getGoalSection().getGoalSectionSummary());
		return reporteeCascadedEmp;
	}

	@Override

	public StatusWorkFlow getWorkStatusByGoalSheetId(Integer goalSheetId) {
		
		GoalStatus currentGoalStatus = statusWorkFlowService.getGoalStatusIdByGoalSheetId(goalSheetId, sessionFactory);
		List<GoalSheetStatusWorkFlow> goalSheetStatusWorkFlowList = 
				statusWorkFlowService.getWorkStatusByGoalSheetId(currentGoalStatus.getGoalStatusId(), sessionFactory);

		StatusWorkFlow statusWorkFlow = new StatusWorkFlow();
		statusWorkFlow.setCurrentStatusId(currentGoalStatus.getGoalStatusId());
		statusWorkFlow.setCurrentStatusName(currentGoalStatus.getGoalStatusName());
		if(CollectionUtils.isNotEmpty(goalSheetStatusWorkFlowList)) {
			for(GoalSheetStatusWorkFlow workFlow : goalSheetStatusWorkFlowList) {
				List<String> nextStatusNameAndUrl = new ArrayList<>();
				nextStatusNameAndUrl.add(workFlow.getNextPossibleGoalSheetStatus().getGoalStatusName());
				nextStatusNameAndUrl.add(workFlow.getWebServiceURL());
				statusWorkFlow.getPossibleNextStutsNameAndUrl()
					.put(workFlow.getNextPossibleGoalSheetStatus().getGoalStatusId(), nextStatusNameAndUrl);
			}
		} else {
			statusWorkFlow.setMessage("Goal Shet Does Not Exist In The Database"); 
		}
		return statusWorkFlow;
	}

	@Override
	public List<EmployeeOrgGoal> getOrgGoals(Integer appraisalPeriodId) {
		List<EmployeeOrgGoal> empOrgGoalList = new ArrayList<>();

		AppraisalPeriod appraisalPeriod = new AppraisalPeriod();
		appraisalPeriod.setAppraisalPeriodId(appraisalPeriodId);
		List<Goal> goalList = goalService.getGoalList(sessionFactory);
		List<GoalCeo> ceoGoalList = goalService.getCeoGoalList(sessionFactory);

		List<GoalOrg> orgGoalList = goalService.getOrgGoalsbyAppraisalPeriod(appraisalPeriod, sessionFactory);
		if(CollectionUtils.isNotEmpty(orgGoalList))	{	 
			for(GoalOrg orgGoal: orgGoalList) {
				EmployeeOrgGoal empOrgGoal = new EmployeeOrgGoal();

				empOrgGoal.setAppraisalPeriodId(appraisalPeriodId);  
				empOrgGoal.setOrgGoalDesc(orgGoal.getOrgGoalDesc());
				empOrgGoal.setOrgGoalId(orgGoal.getOrgGoalId());
				empOrgGoal.setTarget(orgGoal.getTarget());
				empOrgGoal.setWeightage(orgGoal.getWeightage());
				empOrgGoal.setYlbFlag(orgGoal.getYlbFlag());
				empOrgGoal.setIsDeletable(YumPmsUtils.isExist(goalList, ceoGoalList, orgGoal.getOrgGoalId())? false : true);

				empOrgGoalList.add(empOrgGoal);			 
			}
		}
		return empOrgGoalList;
	}

	@Override
	public List<EmployeeOrgGoalCeoGoal> getCeoOrgGoals(Integer appraisalPeriodId) {
		List<EmployeeOrgGoalCeoGoal> empOrgCeoGoalList = new ArrayList<>();

		AppraisalPeriod appraisalPeriod = new AppraisalPeriod();
		appraisalPeriod.setAppraisalPeriodId(appraisalPeriodId);

		List<GoalOrg> orgGoalList = goalService.getOrgGoalsbyAppraisalPeriod(appraisalPeriod, sessionFactory);
		if(CollectionUtils.isNotEmpty(orgGoalList))	{	 
			for(GoalOrg orgGoal: orgGoalList) {

				GoalCeo ceoGoal = goalService.getCeoGoalByOrgGoal(orgGoal, sessionFactory);
				EmployeeOrgGoalCeoGoal empOrgCeoGoal = new EmployeeOrgGoalCeoGoal();

				EmployeeCeoGoal empCeoGoal = new EmployeeCeoGoal();
				if(ceoGoal != null) {
					empCeoGoal.setCeoGoalId(ceoGoal.getCeoGoalId());
					empCeoGoal.setCeoGoalDesc(ceoGoal.getCeoGoalDesc());
					empCeoGoal.setAppraisalPeriodId(appraisalPeriodId);

					List<Goal> goalList = adminService.getGoalByCeoGoal(ceoGoal, sessionFactory); 
					empOrgCeoGoal.setIsDeletable(CollectionUtils.isNotEmpty(goalList) ? false : true); 	
				}				
				empOrgCeoGoal.setCeoGoal(empCeoGoal);
				empOrgCeoGoal.setAppraisalPeriodId(appraisalPeriodId);
				empOrgCeoGoal.setOrgGoalDesc(orgGoal.getOrgGoalDesc());
				empOrgCeoGoal.setOrgGoalId(orgGoal.getOrgGoalId());
				empOrgCeoGoal.setTarget(orgGoal.getTarget());
				empOrgCeoGoal.setWeightage(orgGoal.getWeightage());
				empOrgCeoGoal.setYlbFlag(orgGoal.getYlbFlag());

				empOrgCeoGoalList.add(empOrgCeoGoal);			 
			}
		}
		return empOrgCeoGoalList;
	}

	@Override
	public List<NotificationSettings> getNotificationSettings() {

		List<Notification> notificationList = notificationService.getNotificationList(sessionFactory);
		List<NotificationSettings> notificationSettingsList = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(notificationList)) {	 
			for(Notification notification: notificationList) {
				NotificationSettings notificationSettings = new NotificationSettings();

				notificationSettings.setSettingsValue(notification.getSettingsValue());
				notificationSettings.setNotificationId(notification.getNotificationId());
				notificationSettings.setNotificationName(notification.getNotificationName());
				notificationSettingsList.add(notificationSettings);			 
			}
		}
		return notificationSettingsList;
	}

	@Override
	public StatusWorkFlow updateGoalSheetStatus(Integer goalSheetId, Integer statusId, String createdBy) throws YumPMSDataSaveException {

		String msg = statusWorkFlowService.updateGoalSheetStatus(goalSheetId, statusId, createdBy, sessionFactory);
		StatusWorkFlow nextStatus = getWorkStatusByGoalSheetId(goalSheetId);
		if(nextStatus != null && !nextStatus.getCurrentStatusId().equals(1002)){
			try {
				int actionStatusId = nextStatus.getCurrentStatusId() == null ? 0 : nextStatus.getCurrentStatusId();
				String param2 = StringUtils.EMPTY;
				if(actionStatusId == 1003) {
					param2 = YumPmsConstants.WORK_FLOW_STATUS_DESC_SUBMITTED;
				} else if(actionStatusId == 1004) {
					param2 = YumPmsConstants.WORK_FLOW_STATUS_DESC_ROLLEDBACK;
				} else if(actionStatusId == 1005) {
					param2 = YumPmsConstants.WORK_FLOW_STATUS_DESC_APPROVED;
					goalService.executeGoalVersionHistoryProcedure(goalSheetId, sessionFactory);
				} else if(actionStatusId == 1006) {
					param2 = YumPmsConstants.WORK_FLOW_STATUS_DESC_RECALLED;
				}
				if(YumPmsConstants.UPDATED_SUCESSFULLY.equalsIgnoreCase(msg)) {	
					sendMail(goalSheetId, actionStatusId, param2);
				}		
			} catch(Exception e) {
				LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			} 
		}
		//update goal sheet status history
		GoalSheetWorkflowHistory gsWkflwHistory = new GoalSheetWorkflowHistory();
		gsWkflwHistory.setGoalSheetId(goalSheetId);

		GoalSheet goalSheet = goalService.getGoalSheetbyId(goalSheetId, sessionFactory);
		gsWkflwHistory.setAppraisalPeriodId(goalSheet.getAppraisalPeriod().getAppraisalPeriodId());
		gsWkflwHistory.setGoalStatusId(statusId);
		gsWkflwHistory.setEmpId(goalSheet.getEmployee().getEmpId());
		gsWkflwHistory.setCreatedDate(new Date());
		gsWkflwHistory.setCreatedBy(goalSheet.getEmployee().getEmpName());
		goalService.saveGoalSheetHistory(gsWkflwHistory, sessionFactory);
		return null == nextStatus ? new StatusWorkFlow() : nextStatus;
	}


	public List<NotificationSettings> saveNotificationSetting(List<NotificationSettings> listNotificationSetting) throws YumPMSDataSaveException {

		List<NotificationSettings> newNotificationList = new ArrayList<>();
		for(NotificationSettings notificationSetting : listNotificationSetting) {
			Notification notification =  new Notification();
			notification.setNotificationId(notificationSetting.getNotificationId());
			notification.setNotificationName(notificationSetting.getNotificationName());
			notification.setSettingsValue(notificationSetting.getSettingsValue());
			notification.setActive(notificationSetting.getActive());

			if(notificationSetting.getNotificationId() > 0) {
				notification.setUpdatedBy(notificationSetting.getCreatedBy());
			} else {
				notification.setCreatedBy(notificationSetting.getCreatedBy());
			}
			notificationService.saveNotificationSettingValue(notification, sessionFactory);
			newNotificationList.add(notificationSetting);
		}
		return newNotificationList;
	}

	@Override
	public List<AppraisalPeriod> getApprPeriodList() {

		List<AppraisalPeriod> appPeriodListFromDb = adminService.getApprPeriodList(sessionFactory); 
		if(CollectionUtils.isEmpty(appPeriodListFromDb)) {
			return Collections.emptyList();
		}
		Map<Integer, Date> salaryEffectiveDayMap = paDao.findAll(CompRevisedSalaryEffectiveDuration.class).stream()
				.collect(Collectors.toMap(obj -> obj.getAppraisalPeriod().getAppraisalPeriodId(),
						CompRevisedSalaryEffectiveDuration :: getSalaryDuration));
		
		List<AppraisalPeriod> appPeriodList = new ArrayList<>();
		for (AppraisalPeriod appraisalPeriodFromDB : appPeriodListFromDb) {
			AppraisalPeriod period = new AppraisalPeriod();
			period.setAppraisalPeriodId(appraisalPeriodFromDB.getAppraisalPeriodId());
			period.setPeriodDesc(appraisalPeriodFromDB.getPeriodDesc());
			period.setPeriodStartYyyymm(appraisalPeriodFromDB.getPeriodStartYyyymm());
			period.setPeriodEndYyyymm(appraisalPeriodFromDB.getPeriodEndYyyymm());
			period.setIsDefault(appraisalPeriodFromDB.getIsDefault());
			period.setActive(appraisalPeriodFromDB.getActive());
			period.setGoalLabel(appraisalPeriodFromDB.getGoalLabel());
			period.setIsConfigured(appraisalPeriodFromDB.getIsConfigured());
			period.setNewYearConfigured(BooleanUtils.toBoolean(appraisalPeriodFromDB.getNewYearConfigured()));

			Boolean bRemovable = adminService.getAppraisalPeriodIsRemovable(appraisalPeriodFromDB, sessionFactory);
			if(bRemovable) {
				period.setIsRemovable(true);
			}
			if(period.getNewYearConfigured()) {
				period.setNewYearConfigDate(salaryEffectiveDayMap.get(period.getAppraisalPeriodId()));
			}
			appPeriodList.add(period);
		}
		return appPeriodList;
	}

	@Override
	public List<EmployeeOrgGoal> saveOrgGoals(List<EmployeeOrgGoal> employeeOrgGoalList) throws YumPMSDataSaveException {
		List<EmployeeOrgGoal> sendinEmpOrgGoalList = new ArrayList<>();
		for(EmployeeOrgGoal empOrgGoal : employeeOrgGoalList) {
			GoalOrg orgGoal = null;
			if(empOrgGoal.getOrgGoalId() > 0) {
				orgGoal = goalService.getOrgGoalsbyId(empOrgGoal.getOrgGoalId(), sessionFactory);	
				orgGoal.setUpdatedBy(empOrgGoal.getCreatedBy());
			} else { 
				orgGoal = new GoalOrg();
				orgGoal.setCreatedBy(empOrgGoal.getCreatedBy());
			}
			AppraisalPeriod appraisalPeriod = new AppraisalPeriod();
			appraisalPeriod.setAppraisalPeriodId(empOrgGoal.getAppraisalPeriodId());
			orgGoal.setAppraisalPeriod(appraisalPeriod);			
			orgGoal.setOrgGoalDesc(empOrgGoal.getOrgGoalDesc());		
			orgGoal.setYlbFlag(empOrgGoal.getYlbFlag());
			orgGoal.setWeightage(empOrgGoal.getWeightage());
			orgGoal.setTarget(empOrgGoal.getTarget());
			try {
				orgGoal = goalService.saveOrgGoal(orgGoal, sessionFactory);
				//populated sending object=====================
				EmployeeOrgGoal sendingEmpOrgGoal = empOrgGoal;
				sendingEmpOrgGoal.setOrgGoalId(orgGoal.getOrgGoalId());
				sendinEmpOrgGoalList.add(sendingEmpOrgGoal);
			} catch (Exception e) {
				LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			}
		}
		return sendinEmpOrgGoalList;
	}

	@Override
	public AppraisalPeriod getDefaultAppraisalPeriod() {
		return appraisalService.getDefaultAppraisalService();

	}

	public List<EmployeeOrgGoalCeoGoal> saveCeoGoals(List<EmployeeOrgGoalCeoGoal> employeeCeoGoalList) throws YumPMSDataSaveException {

		List<EmployeeOrgGoalCeoGoal> sendinEmpCeoGoalList = new ArrayList<>();
		for(EmployeeOrgGoalCeoGoal empCeoGoal : employeeCeoGoalList) {
			if(empCeoGoal != null && empCeoGoal.getCeoGoal() != null && empCeoGoal.getCeoGoal().getCeoGoalId() != 0) {
				GoalCeo ceoGoal = null;
				if(empCeoGoal.getCeoGoal().getCeoGoalId() > 0) {
					ceoGoal = goalService.getCeoGoalbyId(empCeoGoal.getCeoGoal().getCeoGoalId(), sessionFactory);	
					ceoGoal.setUpdatedBy(empCeoGoal.getCeoGoal().getCreatedBy());
				} else { 
					ceoGoal = new GoalCeo();
					ceoGoal.setCreatedBy(empCeoGoal.getCeoGoal().getCreatedBy()); 
				}
				AppraisalPeriod appraisalPeriod = new AppraisalPeriod();
				appraisalPeriod.setAppraisalPeriodId(empCeoGoal.getCeoGoal().getAppraisalPeriodId());
				ceoGoal.setAppraisalPeriod(appraisalPeriod);			
				ceoGoal.setCeoGoalDesc(empCeoGoal.getCeoGoal().getCeoGoalDesc());		

				GoalOrg orgGoal = new GoalOrg();
				orgGoal.setOrgGoalId(empCeoGoal.getOrgGoalId());
				ceoGoal.setGoalOrg(orgGoal);
				try {
					ceoGoal = goalService.saveCeoGoal(ceoGoal, sessionFactory);
				} catch (Exception e) {
					LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
				}
				empCeoGoal.getCeoGoal().setCeoGoalId(ceoGoal.getCeoGoalId());
			}
			// populated sending object=====================
			sendinEmpCeoGoalList.add(empCeoGoal);
		}
		return sendinEmpCeoGoalList;
	}

	@Override
	public String deleteById(Integer id, String tableName) {

		boolean isDelete = false;
		switch(StringUtils.upperCase(tableName)) {
		case YumPmsConstants.ORG_GOAL:
			isDelete = adminService.deleteOrgGoal(id, sessionFactory);
			break;
		case YumPmsConstants.CEO_GOAL:
			isDelete = adminService.deleteCeoGoal(id, sessionFactory);
			break;
		case YumPmsConstants.APPRAISAL_IN_UPPER_CASE:
			isDelete = adminService.deleteAppraisal(id, sessionFactory);
			break;
		case YumPmsConstants.PROMOTION_CRITERIA:
			isDelete = paDao.deleteById(PA_ConfigPromotionCriteria.class, id);
			break;
		default :
			isDelete = false;
			break;
		}
		return isDelete ? YumPmsConstants.SUCCESS_DELETE_MSG : YumPmsConstants.ERROR_MSG_TRY_AGAIN;
	}



	@Override
	public String deleteById(String id, String tableName) throws YumPMSDataSaveException {
		
		return fileHandellingService.deleteFileById(id, sessionFactory) ? YumPmsConstants.SUCCESS_DELETE_MSG : YumPmsConstants.ERROR_MSG_TRY_AGAIN;
	}

	@Override
	public List<EmployeeRoleDetails> getEmpRoleList() {

		List<EmpToPmsRole> empToPMSRoleList = adminService.getEmpRoleList(sessionFactory);
		List<EmployeeNew> allEmpList = employeeService.getAllEmpList();

		List<EmployeeRoleDetails> empRoleDetailList = new ArrayList<>();
		for(EmployeeNew emp: allEmpList) {
			EmployeeRoleDetails empRoleDetails = new EmployeeRoleDetails();
			boolean bRoleSet = false;
			for (EmpToPmsRole empToPmsRole : empToPMSRoleList) {
				if (emp.getEmpId().equals(empToPmsRole.getEmployee().getEmpId())) {
					empRoleDetails.setEmpId(empToPmsRole.getEmployee().getEmpId());
					empRoleDetails.setEmpName(empToPmsRole.getEmployee().getEmpName());
					empRoleDetails.setPmsRoleId(empToPmsRole.getPmsRole().getPmsRoleId());
					empRoleDetails.setPmsRoleName(empToPmsRole.getPmsRole().getPmsRoleName());
					bRoleSet = true;
				}
				if(bRoleSet) {
					break;
				}
			}
			if(!bRoleSet) {
				empRoleDetails.setEmpId(emp.getEmpId());
				empRoleDetails.setEmpName(emp.getEmpName());
			}
			empRoleDetailList.add(empRoleDetails);										
		}
		return empRoleDetailList;
	}

	private void saveGoalSectionAfterCascading(CascadedDetails cascadedDetails, Integer empId) throws YumPMSDataSaveException {
		GoalSection mgrGoalSection = cascadedService.getGoalSectionByCascadedID(cascadedDetails.getCascadeId(), sessionFactory);
		Goal mgrGoal  = mgrGoalSection.getGoal();

		//get reportee org and ceo goals from mgr goal - they should be same
		Integer reporteeCeoGoalId = mgrGoal.getCeoGoalId();
		Integer reporteeOrgGoalId = mgrGoal.getOrgGoalId();
		GoalOrg reporteeOrgGoal = new GoalOrg();
		reporteeOrgGoal.setOrgGoalId(mgrGoal.getOrgGoalId());

		AppraisalPeriod appraisalPeriod = mgrGoal.getGoalSheet().getAppraisalPeriod();
		EmployeeNew reporteeEmp = employeeService.getEmployeeById(empId);
		GoalSheet reporteeGoalSheet = goalService.getGoalSheetbyEmpId(reporteeEmp, appraisalPeriod, sessionFactory);

		//if goal sheet not present
		if(reporteeGoalSheet == null || reporteeGoalSheet.getGoalSheetId() == 0) {	
			reporteeGoalSheet = new GoalSheet();
			reporteeGoalSheet.setAppraisalPeriod(appraisalPeriod);
			reporteeGoalSheet.setEmployee(reporteeEmp);
			GoalStatus goalStatus = mgrGoal.getGoalSheet().getGoalStatus();
			reporteeGoalSheet.setGoalStatus(goalStatus);
			reporteeGoalSheet.setActive(true);
			reporteeGoalSheet.setCreatedBy(cascadedDetails.getCreatedBy());
			reporteeGoalSheet = goalService.saveGoalSheet(reporteeGoalSheet, sessionFactory);
		}

		//check for existence of goal - if not present then add	
		Goal reporteeGoal = null;
		if(reporteeOrgGoalId != null && reporteeOrgGoalId != 0)		
			reporteeGoal = goalService.getGoalByOrgCeoGoal(appraisalPeriod.getAppraisalPeriodId(), reporteeOrgGoalId, 
					reporteeCeoGoalId, reporteeEmp, sessionFactory);
		if(reporteeGoal == null) {
			reporteeGoal = new Goal();
			if (reporteeCeoGoalId != null) {
				reporteeGoal.setCeoGoalId(reporteeCeoGoalId);
			}
			reporteeGoal.setGoalSheet(reporteeGoalSheet);
			reporteeGoal.setOrgGoalId(reporteeOrgGoal.getOrgGoalId()>0?reporteeOrgGoal.getOrgGoalId():0);
			reporteeGoal.setCreatedBy(cascadedDetails.getCreatedBy());
			//save reportee goal
			reporteeGoal = goalService.saveGoal(reporteeGoal, sessionFactory);
		}

		//add goalsection and get id
		GoalSection newReporteeGoalSection = new GoalSection();
		newReporteeGoalSection.setGoal(reporteeGoal);
		newReporteeGoalSection.setGoalSectionSummary(cascadedDetails.getEditedGsSummaryToEmp());
		newReporteeGoalSection.setGoalSectionTimeline(DateTimeUtils.parse(cascadedDetails.getCascadedTimeline()));
		newReporteeGoalSection.setCreatedBy(cascadedDetails.getCreatedBy());
		newReporteeGoalSection = goalService.saveGoalSection(newReporteeGoalSection, sessionFactory);

		//update cascaded to emp table
		Cascaded cascaded = cascadedService.getCascadedById(cascadedDetails.getCascadeId(), sessionFactory);
		CasecadeEmp mgrCascadedEmp = cascadedService.getCascadedEmpById(cascaded, reporteeEmp, sessionFactory);
		mgrCascadedEmp.setGoalSection(newReporteeGoalSection);

		cascadedService.saveCascadedEmp(mgrCascadedEmp, sessionFactory);
	}

	@Override
	public List<AppraisalPeriod> saveAppraisalPeriods(List<AppraisalPeriod> appraisalPeriodList) {

		for(AppraisalPeriod appraisalPeriod : appraisalPeriodList) {
			if(appraisalPeriod == null) {
				continue;
			}
			AppraisalPeriod dbAppraisalPeriod = null;
			if(appraisalPeriod.getAppraisalPeriodId() != null && appraisalPeriod.getAppraisalPeriodId() > 0) {
				dbAppraisalPeriod = appraisalService.getAppraisalPeriodbyId(appraisalPeriod.getAppraisalPeriodId());
				dbAppraisalPeriod.setUpdatedBy(appraisalPeriod.getCreatedBy());

			} else { 
				dbAppraisalPeriod = new AppraisalPeriod();
				dbAppraisalPeriod.setCreatedBy(appraisalPeriod.getCreatedBy());
			}
			dbAppraisalPeriod.setActive(appraisalPeriod.getActive());
			dbAppraisalPeriod.setIsDefault(appraisalPeriod.getIsDefault());
			dbAppraisalPeriod.setPeriodDesc(appraisalPeriod.getPeriodDesc());
			dbAppraisalPeriod.setPeriodEndYyyymm(appraisalPeriod.getPeriodEndYyyymm());
			dbAppraisalPeriod.setPeriodStartYyyymm(appraisalPeriod.getPeriodStartYyyymm());
			dbAppraisalPeriod.setGoalLabel(appraisalPeriod.getGoalLabel());
			try {
				appraisalService.saveAppraisalPeriod(dbAppraisalPeriod);			
			} catch (Exception e) {
				LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			}
		}
		return getApprPeriodList();
	}

	@Override
	public List<EmployeeRoleDetails> saveEmployeeRoles(List<EmployeeRoleDetails> employeeRoleList) throws YumPMSDataSaveException {

		String loginId = StringUtils.EMPTY;
		int roleId = 0;
		for(EmployeeRoleDetails empPMSRole : employeeRoleList) {
			if(empPMSRole != null && empPMSRole.getPmsRoleId() != null && empPMSRole.getPmsRoleId()>0 ) {
				EmployeeNew employee = employeeService.getEmployeeById(empPMSRole.getEmpId());
				loginId = employee.getEmail();
				adminService.deleteEmptoPMSRoleIdById(empPMSRole.getEmpId(), sessionFactory);

				EmpToPmsRole dbEmpToPMSRole = new EmpToPmsRole();
				EmpToPmsRoleId empToPmsRoleId = new EmpToPmsRoleId();

				empToPmsRoleId.setEmpId(empPMSRole.getEmpId());
				empToPmsRoleId.setPmsRoleId(empPMSRole.getPmsRoleId().shortValue());
				dbEmpToPMSRole.setId(empToPmsRoleId);				
				dbEmpToPMSRole.setEmployee(employee);

				PmsRole pmsRole = adminService.getPMSRoleById(empPMSRole.getPmsRoleId(), sessionFactory);
				roleId = pmsRole.getPmsRoleId();
				dbEmpToPMSRole.setPmsRole(pmsRole);
				dbEmpToPMSRole.setCreatedBy(empPMSRole.getCreatedBy());
				try {
					adminService.saveEmpPMSRole(dbEmpToPMSRole, sessionFactory);
				} catch (Exception e) {
					LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
				}				
			}
			updatePermission(loginId, roleId);
		}
		return YumPmsUtils.getEmptyListIfNull(getEmpRoleList());
	}

	public void updatePermission(String loginId, int roleId) throws YumPMSDataSaveException {

		if(roleId == 1001) {
			adminService.insertUserRole(loginId, Arrays.asList("admin", "app_manager", "uba_user"), sessionFactory);
		} else {
			EmployeeNew employee = employeeService.getEmployeeByEmail(loginId);
			if(employee != null && !employeeService.isActiveLt(employee.getEmpId(), -1)) {
				adminService.deleteUserRole(loginId, sessionFactory); 
			}
		}
	}

	@Override
	public List<Map<String, Object>> getUserRollList() {

		List<Map<String, Object>> userRoleList = new ArrayList<>();
		List<PmsRole> pmsRoleList = util.getUserRollList(sessionFactory);	
		if(CollectionUtils.isNotEmpty(pmsRoleList)) {
			for(PmsRole userRole : pmsRoleList) {
				Map<String, Object> mapOfItem = new HashMap<>();
				mapOfItem.put(YumPmsConstants.ID, userRole.getPmsRoleId()); 
				mapOfItem.put(YumPmsConstants.ROLE, userRole.getPmsRoleName());
				userRoleList.add(mapOfItem);
			}	
		}
		return userRoleList;
	}

	@Override
	public List<EmployeeGoalHistory> getLastWorkFlowHistoryStatus(Integer goalSheetId) {

		List<GoalSheetWorkflowHistory> gsWorkFlowDetailsList = goalService.getWorkFlowHistoryStatus(goalSheetId, sessionFactory);
		List<EmployeeGoalHistory> listWorkFlowHistory = new ArrayList<>();
		if (gsWorkFlowDetailsList != null) {

			for (GoalSheetWorkflowHistory gsWorkFlowDetails : gsWorkFlowDetailsList) {		

				EmployeeGoalHistory lastWorkFlowDetails = new EmployeeGoalHistory();
				lastWorkFlowDetails.setGoalSheetId(goalSheetId);

				GoalStatus status = goalService.getGoalStatus(gsWorkFlowDetails.getGoalStatusId(), sessionFactory);
				lastWorkFlowDetails.setGoalStatusName(status.getGoalStatusName());

				lastWorkFlowDetails.setUpdatedByEmpName(employeeService.getEmployeeById(gsWorkFlowDetails.getEmpId()).getEmpName());

				lastWorkFlowDetails.setUpdateDate(gsWorkFlowDetails.getCreatedDate());
				listWorkFlowHistory.add(lastWorkFlowDetails);
			}
		}
		return listWorkFlowHistory;
	}

	@Override
	public String savePassword(String loginId) throws YumPMSDataSaveException {

		int range = 99999999 - 10000000;
		Random random = new Random();
		int rValue = random.nextInt();
		String password = ((Integer)(Math.abs(range * rValue))).toString();
		adminService.savePassword(loginId, password, sessionFactory);
		return password;
	}

	//============================================Mail & File Handling Part========================================================
	@Override
	public void sendMail(String type) {

		List<EmailDomain> listOfMail = null;

		switch(StringUtils.upperCase(type)) {
		case YumPmsConstants.NEW_JOINEE:
			listOfMail = util.getNewJoineeMailDetails(sessionFactory);
			break;
		case YumPmsConstants.EXISTING_JOINEE:
			listOfMail = util.getExistingJoineeMailDetails(sessionFactory);
			break;
		case YumPmsConstants.GOAL_SHEET_SUBMIT:
			listOfMail = util.getGoalSheetSubmitedMailDetails(sessionFactory);
			break;
		case YumPmsConstants.LT_SUMMARIZED:
			listOfMail = util.getLTMailSummarized(sessionFactory);
			break;
		case YumPmsConstants.LT_DETAIL:
			listOfMail = util.getLTMailDetails(sessionFactory);
			break;
		case YumPmsConstants.DISCUSSED_AND_AGREED_MONTHLY_QUARTERLY:
			listOfMail = util.getDiscussedAndAgreedMailDetails(sessionFactory);
			break;
		case YumPmsConstants.NOT_DISCUSSED_AND_AGREED_IDP:
			listOfMail = util.getNotDiscussedAndAgreedOfIdpMailDetails(sessionFactory);
			break;		
		default:
			listOfMail = Collections.emptyList();
			break;
		}
		if(CollectionUtils.isNotEmpty(listOfMail)) {
			for(EmailDomain mailDetails : listOfMail) {
				try {
					mailSender.sendMail(mailDetails.getToMailId(), mailDetails.getCcMailId(), mailDetails.getSubject(), mailDetails.getBody());
				} catch (Exception e) {
					LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
					throw new CommonException(e);
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.yum.pms.service.IYumHibernateService#sendMail(java.lang.String, java.lang.String)
	 */
	@Override
	public void sendMail(String type, String spName) {
		List<EmailDomain> listOfMail = null;
		try {
			if(type.equalsIgnoreCase(YumPmsConstants.PARAMITER_MAIL)) {
				listOfMail = util.getGeneralMailDetails(sessionFactory, spName);
			}else {
				listOfMail = Collections.emptyList();
			}
			if(CollectionUtils.isNotEmpty(listOfMail)) {
				for(EmailDomain mailDetails : listOfMail) {
					
						mailSender.sendMail(mailDetails.getToMailId(), mailDetails.getCcMailId(), mailDetails.getSubject(), mailDetails.getBody());				
				}
			}
		} catch (Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			throw new CommonException(e);
		}
	}

	/* (non-Javadoc)
	 * sendMail this method send mail while Overall Rating save
	 * @see com.yum.pms.service.IYumHibernateService#sendMail(java.lang.String, java.lang.Integer)
	 */
	@Override
	public void sendMail(String type, Integer idpSheetId) {

		List<EmailDomain> listOfMail = new ArrayList<>(1);
		EmailDomain email = new EmailDomain();
		listOfMail.add(email); 
		List<Object[]> results = new ArrayList<>();
		if(YumPmsConstants.SUPERVISOR_OF_SUPERVISOR.equalsIgnoreCase(type)) {
			results = idpUtil.getSup2EmailParams(idpSheetId);
		}else if(YumPmsConstants.REJECTION_MAIL_TO_SUPERVISOR.equalsIgnoreCase(type)) {
			results = idpUtil.getEmailParamsOnRejectionExceptionalRating(idpSheetId);
		}else if(YumPmsConstants.APPROVAL_MAIL_TO_LT.equalsIgnoreCase(type)){
			results = idpUtil.getEmailParamsOnExceptionalRatingBySup(idpSheetId);
		}
		if(CollectionUtils.isNotEmpty(results)) {
			Object[] tuples = results.get(0);
			listOfMail.get(0).setToMailId(YumPmsUtils.toStringorNull(tuples[0])); 
			listOfMail.get(0).setCcMailId(YumPmsUtils.toStringorNull(tuples[1])); 
			listOfMail.get(0).setSubject(YumPmsUtils.toStringorNull(tuples[2])); 
			listOfMail.get(0).setBody(YumPmsUtils.toStringorNull(tuples[3])); 
		}
		if(CollectionUtils.isNotEmpty(listOfMail)) {
			for(EmailDomain mailDetails : listOfMail) {
				if(null != mailDetails.getToMailId())
					try {
						mailSender.sendMail(mailDetails.getToMailId(), mailDetails.getCcMailId(), mailDetails.getSubject(), mailDetails.getBody());
					} catch (Exception e) {
						LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
						throw new CommonException(e);
					}
			}
		}
	}
	
	/**
	 * Get the list of global letter details in a particular appraisal period
	 * @param apprId
	 * @param empId - this parameter is optional, if it is found then get letter for this employee only
	 */
	@Override
	public List<GlobalLetter> getGlobalLetterDetails(Integer apprId, Integer empId) {
		
		if(!YumPmsUtils.isGreaterThanZero(apprId)) {
			apprId = getDefaultAppraisalPeriod().getAppraisalPeriodId();
		}
		List<GlobalLetter> globalLetters = fileHandellingService.getGlobalLetter(apprId, empId, sessionFactory);
		globalLetters.forEach(globalLetter -> globalLetter.setApprId(globalLetter.getAppraisalPeriod().getAppraisalPeriodId()));
		return globalLetters;
	}
	
	/**
	 * upload a global letter
	 * @param uploadFile - letter 
	 * @param globalLetter - to whom it will assigned
	 */
	@Override
	public GlobalLetter uploadGlobalLetter(MultipartFile uploadFile, GlobalLetter globalLetter) throws YumPMSDataSaveException {
		
		if(uploadFile == null || uploadFile.isEmpty()) {
			throw new CommonException("Upload file is mandatory");
		}
		if(!YumPmsUtils.isGreaterThanZero(globalLetter.getApprId()) || !YumPmsUtils.isGreaterThanZero(globalLetter.getEmployee().getEmpId())) {
			throw new CommonException("Both appraisal id and employee id are mandatory");
		}
		String originalFilename = StringUtils.replace(uploadFile.getOriginalFilename(), YumPmsConstants.SPACE, YumPmsConstants.UNDERSCORE);
		String[] fileTypeList = ResourceUtils.getPropertyValue(YumPmsConstants.FILE_TYPE).split(YumPmsConstants.COMMA);
		String extension = FilenameUtils.getExtension(originalFilename);
		if(!ArrayUtils.contains(fileTypeList, extension.toLowerCase())) {	
			throw new CommonException("Unsupported File Extension");
		}
		try {
			byte[] content = uploadFile.getBytes();
			String size = FileUtils.byteCountToDisplaySize(content.length);
			LOGGER.info("Original Uploded File Size = {}", size);
			long maxSignImgSize = ResourceUtils.getMaxFileSIze();
			if(content.length > maxSignImgSize) {
				throw new CommonException("Maximum File Size Limit Exceed. You can provide maximun " + maxSignImgSize + " KB");
			}
			String uniqueFileName = 
					new StringBuilder(StringUtils.EMPTY).append(globalLetter.getApprId()).append(YumPmsConstants.UNDERSCORE)
					.append(globalLetter.getEmployee().getEmpId()).append(YumPmsConstants.UNDERSCORE)
					.append(System.nanoTime()).append(YumPmsConstants.UNDERSCORE)
					.append(FilenameUtils.getBaseName(originalFilename)).append(YumPmsConstants.DOT).append(extension).toString();
			globalLetter.setLetterFilePath(ResourceUtils.createFile(uniqueFileName, ResourceUtils.getGlobalLetterPath(), content));
			globalLetter.setLetterFileName(originalFilename);
			globalLetter.setCreatedDate(DateTimeUtils.now());
			return paDao.saveOrUpdate(globalLetter).orElseThrow(() -> new CommonException("Unable to save global letter"));
		} catch (IOException e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			throw new CommonException(e);
		}
	}
	
	/**
	 * delete a particular global letter
	 * @param globalLetterId - letter ID 
	 */
	@Override
	public void deleteGlobalLetterDetails(Integer globalLetterId) {
		
		if(!YumPmsUtils.isGreaterThanZero(globalLetterId)) {
			throw new CommonException(YumPmsConstants.INVALID_GLOBAL_LETTER_ID);
		}
		GlobalLetter globalLetter = paDao.findById(GlobalLetter.class, globalLetterId)
				.orElseThrow(() -> new CommonException(YumPmsConstants.INVALID_GLOBAL_LETTER_ID));
		boolean flag = paDao.deleteById(GlobalLetter.class, globalLetterId);
		if(!flag) {
			throw new CommonException("Unable to delete global letter");	
		}
		if(StringUtils.isNotEmpty(globalLetter.getLetterFilePath())) {
			String letterPath = ResourceUtils.getYumRootPath() + globalLetter.getLetterFilePath();
			LOGGER.info("Letter path to delete: {}", letterPath);
			ResourceUtils.delete(letterPath);
		}
	}
	
	/**
	 * download a global letter
	 * @param globalLetterId - letter ID 
	 * @return letter name and content in byte array and wrap into a map
	 */
	@Override
	public Map<String, byte[]> downloadGlobalLetter(Integer globalLetterId) {
		
		if(YumPmsUtils.isGreaterThanZero(globalLetterId)) {
			GlobalLetter globalLetter = paDao.findById(GlobalLetter.class, globalLetterId)
					.orElseThrow(() -> new CommonException(YumPmsConstants.INVALID_GLOBAL_LETTER_ID));
			if(StringUtils.isEmpty(globalLetter.getLetterFilePath())) {
				throw new CommonException("Global letter record does not exist in data base against the id : " + globalLetterId);
			}
			String letterPath = ResourceUtils.getYumRootPath() + globalLetter.getLetterFilePath();
			LOGGER.info("Letter path to download: {}", letterPath);
			try {
				Map<String, byte[]> map = new HashMap<>(1);
				map.put(globalLetter.getLetterFileName(), Files.readAllBytes(Paths.get(letterPath)));
				return map;
			} catch (IOException e) {
				throw new CommonException(e);
			}
		}
		throw new CommonException(YumPmsConstants.INVALID_GLOBAL_LETTER_ID);
	}

	@Override
	public ReferenceDoc fileUpload(MultipartFile uploadFile, ReferenceDoc referenceDoc) throws YumPMSDataSaveException {
		
		int fileNo = 0;
		String fileName = uploadFile.getOriginalFilename();
		String fileExtetion = fileName.substring(fileName.lastIndexOf(YumPmsConstants.DOT) + 1);
		String actualFileName = fileName.substring(0, fileName.lastIndexOf(YumPmsConstants.DOT) ); 

		String path = ResourceUtils.getPropertyValue(YumPmsConstants.PATH_OF_UPLOAD_FILES);	
		String[] fileTypeList = ResourceUtils.getPropertyValue(YumPmsConstants.FILE_TYPE).split(YumPmsConstants.COMMA);
		File file = new File(path);
		if(ArrayUtils.contains(fileTypeList, fileExtetion.toLowerCase())) {		
			referenceDoc.setFileName(fileName);
			referenceDoc.setFilePath(path + fileName);
			referenceDoc.setFileId(referenceDoc.getAppraisalPeriodId() + YumPmsConstants.UNDERSCORE + 
					referenceDoc.getEmpId() + YumPmsConstants.UNDERSCORE + 
					DateTimeUtils.currentDateStr(YumPmsConstants.DATE_TIME_FROMAT).replaceAll(YumPmsConstants.SPACE, YumPmsConstants.UNDERSCORE));
			referenceDoc.setCreatedBy(referenceDoc.getEmpId().toString());
			referenceDoc.setCreatedDate(DateTimeUtils.currentDate(YumPmsConstants.DATE_TIME_FROMAT)); 		
			if(!file.exists()) {			// Folder creation, if does not exist--------
				file.mkdir();
			}
			File f = new File(path + fileName);
			if(f.exists() && !f.isDirectory()) {
				while(f.exists()){
					try {
						fileNo++;
						uploadFile.transferTo(new File(path + actualFileName + "(" + fileNo + ")." + fileExtetion)); 
					} catch (Exception e) {
						fileNo--;
						referenceDoc.setFileName(actualFileName + "(" + fileNo + ")." + fileExtetion);
						referenceDoc.setFilePath(path + actualFileName + "(" + fileNo + ")." + fileExtetion); 
						referenceDoc = fileHandellingService.saveFileDetails(referenceDoc, sessionFactory);
						return referenceDoc;
					}
				}
				referenceDoc.setFileName(actualFileName + "(" + fileNo + ")");
				referenceDoc.setFilePath(path + actualFileName + "(" + fileNo + ")." + fileExtetion); 
			} else {
				sendToPath(uploadFile, path + fileName); 
			}
			referenceDoc = fileHandellingService.saveFileDetails(referenceDoc, sessionFactory);
		} else {
			referenceDoc.setOperationMassage("Your File Type Is Not Support, Please Contact To Admin ");
		}
		return referenceDoc;
	}

	private void sendToPath(MultipartFile uploadFile, String fileName) {
		try {
			uploadFile.transferTo(new File(fileName));
		} catch (IllegalStateException | IOException e) {
			throw new CommonException(e);
		}
	}

	@Override
	public List<ReferenceDoc> getfilesdetailsList(ReferenceDoc referenceDoc) {

		return BooleanUtils.toBoolean(referenceDoc.getIsAdminDoc()) ? 
				fileHandellingService.getAdminfilesdetailsList(referenceDoc, sessionFactory)
				: fileHandellingService.getUserfilesdetailsList(referenceDoc, sessionFactory);
	}

	@Override
	public String fileDownload(String fileId, HttpServletRequest request, HttpServletResponse response) {

		String filename = fileHandellingService.getFileDetailsById(fileId, sessionFactory).getFileName();     
		response.setContentType(YumPmsConstants.HTML_CONTENT_TYPE);  
		response.setContentType("APPLICATION/OCTET-STREAM");   
		response.setHeader("Content-Disposition","attachment; filename=\"" + filename + "\"");  

		try(
			PrintWriter out = response.getWriter();
			InputStream inputStream = new FileInputStream(ResourceUtils.getPropertyValue(YumPmsConstants.PATH_OF_UPLOAD_FILES) + filename)) {  
			int i;   
			while ((i = inputStream.read()) != -1) {  
				out.write(i);   
			}   
		} catch(Exception e) {
			LOGGER.info("Download Failed>>>>>>...{}", e);
			return YumPmsConstants.FAILED;
		}
		return YumPmsConstants.SUCCESS;
	}

	@Override
	public Map<String, Object> getPdfTagReplacementDetails() {

		return util.getTagDetailsOfPdf(sessionFactory);
	}

	/** 
	 * @see com.yum.pms.service.IYumHibernateService#getIdpCategoryList()
	 * Get all IDP category list 
	 */
	@Override
	public List<IDP_SectionMaster> getIdpCategoryList() {

		return adminService.getIdpCategoryList(sessionFactory);
	}

	/** 
	 * @see com.yum.pms.service.IYumHibernateService#saveIdpCategoryList(java.util.List)
	 * Save all IDP category list
	 */
	@Override
	public List<IdpCategory> saveIdpCategoryList(List<IdpCategory> sectionList) {
		for (IdpCategory category : sectionList) {
			adminService.saveIdpSectionMasterData(new IDP_SectionMaster(category));
		}
		
		List<IdpCategory> idpSectionList = new ArrayList<>();
		for (IdpCategory category : sectionList) {
			
			List<IdpCategory> collect = 
					getSectionList(category.getApprId(), category.getIdpSectionTypeFlag())
					.stream()
					.map(IdpCategory :: new)
					.collect(Collectors.toList());
			idpSectionList.addAll(collect);
			
		}
		return idpSectionList;
	}

	/* (Fetch all categories)
	 * @see com.yum.idp.service.IIDPService#getAllSections()
	 */
	@Override
	public Map<String,Object> getAllSections(Integer apprId, String categoryFlag) {
		Map<String,Object> sectionMap = new HashMap<>(6);
		List<IDP_SectionMaster> sectionList = getSectionList(apprId, categoryFlag);
		if(sectionList != null) {
			List<IdpCategory> categoryList = sectionList.stream().map(IdpCategory :: new).collect(Collectors.toList());
			sectionMap.put(YumPmsConstants.IDP_SECTION_LIST, categoryList);
		}
		List<IDP_SubSectionMaster> subSectionList = getSubSectionList(apprId, categoryFlag);
		if(subSectionList != null) {
			List<IdpSubCategory> subCategoryList = subSectionList.stream().map(IdpSubCategory :: new).collect(Collectors.toList());
			sectionMap.put(YumPmsConstants.IDP_SUB_SECTION_LIST, subCategoryList);
		}
		List<IDP_SubSubSectionMaster> subSubSectionList = getSubSubSectionList(apprId);
		if(subSubSectionList != null) {
			List<IdpSubSubCategory> subSubCategoryList = subSubSectionList.stream().map(IdpSubSubCategory :: new).collect(Collectors.toList());
			sectionMap.put(YumPmsConstants.IDP_SUB_SUB_SECTION_LIST, subSubCategoryList);
		}
		List<IDP_SubSubSubSectionMaster> subSubSubSectionList = getSubSubSubSectionList();
		if(subSubSubSectionList != null) {
			List<IdpSubSubSubCategory> subSubSubCategoryList = subSubSubSectionList.stream()
					.map(IdpSubSubSubCategory :: new).collect(Collectors.toList());
			sectionMap.put(YumPmsConstants.IDP_SUB_SUB_SUB_SECTION_LIST, subSubSubCategoryList);
		}
		List<IDP_SuggestedGoalMaster> idpSuggestedGoalList = getSuggestedGoalList();
		if(idpSuggestedGoalList != null) {
			List<IdpSuggestedGoal> suggestedGoalList = idpSuggestedGoalList.stream()
					.map(IdpSuggestedGoal :: new).collect(Collectors.toList());
			sectionMap.put(YumPmsConstants.IDP_SUGGESTED_GOAL_LIST, suggestedGoalList);
		}
		List<IDP_SuggestedDevelopmentPlanMaster> idpSuggestedDevPlan = getSuggestedDevelopmentPlanList();
		if(idpSuggestedDevPlan != null) {
			List<IdpSuggestedDevelopmentActionPlan> suggestedGoalList = idpSuggestedDevPlan.stream()
					.map(IdpSuggestedDevelopmentActionPlan :: new).collect(Collectors.toList());
			sectionMap.put(YumPmsConstants.IDP_SUGGESTED_DEV_PLAN, suggestedGoalList);
		}
		return sectionMap; 
	}
	
	@Override
	public Map<String, Object> yearConfigurationForBrandGoal(Integer apprId) {
		
		List<Object[]> result = appraisalService.yearConfigurationForBrandGoal(apprId);
		if(CollectionUtils.isEmpty(result)) {
			return Collections.emptyMap();
		}
		Object[] cols = result.get(0);
		Map<String, Object> map = new HashMap<>(2);
		if(ArrayUtils.getLength(cols) > 1) {
			map.put(YumPmsConstants.STATUS, cols[0]);
			map.put(YumPmsConstants.MSG, cols[1]);
		}
		return map;
	}

	/* (Fetch all category list for How To Lead section)
	 * @see com.yum.idp.service.IIDPService#getSectionList()
	 */
	private List<IDP_SectionMaster> getSectionList(Integer apprId, String categoryFlag) {
		
		List<IDP_SectionMaster> sectionList = adminService.getSectionList(apprId,categoryFlag); 
		
		if(CollectionUtils.isEmpty(sectionList)) {
			return Collections.emptyList();
		}
		List<Integer> categoryIds = sectionList.stream().map(IDP_SectionMaster :: getIdpSectionId).collect(Collectors.toList());
		if(CollectionUtils.isNotEmpty(categoryIds)) {
			Set<Integer> removableIds = adminService.getRemovableCategoryIds(categoryIds);
			sectionList.forEach(category -> category.setRemovable(removableIds.contains(category.getIdpSectionId())));
		}

		return sectionList;
	}

	/* (Fetch all sub-category list for How To Lead section)
	 * @see com.yum.idp.service.IIDPService#getSubSectionList()
	 */
	private List<IDP_SubSectionMaster> getSubSectionList(Integer apprId, String categoryFlag) {
		List<IDP_SubSectionMaster> subSectionList = adminService.getSubSectionList(apprId, categoryFlag); 
		if(CollectionUtils.isEmpty(subSectionList)) {
			return Collections.emptyList();
		}
		List<Integer> subCategoryIds = subSectionList.stream().map(IDP_SubSectionMaster :: getIdpSubSectionId).collect(Collectors.toList());
		if(CollectionUtils.isNotEmpty(subCategoryIds)) {
			Set<Integer> removableIds = adminService.getRemovableSubCategoryIds(subCategoryIds);
			subSectionList.forEach(subCategory -> subCategory.setRemovable(removableIds.contains(subCategory.getIdpSubSectionId())));
		}
		return subSectionList;
	}
	
	private List<IDP_SuggestedGoalMaster> getSuggestedGoalList() {
		List<IDP_SuggestedGoalMaster> suggestedGoalList = adminService.getSuggestedGoalList(); 
		if(CollectionUtils.isEmpty(suggestedGoalList)) {
			return Collections.emptyList();
		}
		List<Integer> suggestedGoalIds = suggestedGoalList.stream()
				.map(IDP_SuggestedGoalMaster :: getIdpSuggestedGoalSectionId).collect(Collectors.toList());
		if(CollectionUtils.isNotEmpty(suggestedGoalIds)) {
			Set<Integer> removableIds = adminService.getRemovableSelectedSuggestedGoalIds(suggestedGoalIds);
			suggestedGoalList
			.forEach(suggestedGoalSection -> suggestedGoalSection.setRemovable(removableIds.contains(suggestedGoalSection.getIdpSuggestedGoalSectionId())));
		}
		return suggestedGoalList;
	}
	
	public List<IDP_SuggestedDevelopmentPlanMaster> getSuggestedDevelopmentPlanList() {
		List<IDP_SuggestedDevelopmentPlanMaster> developmentPlanList = adminService.getSuggestedDevelopmentPlanList(); 
		if(CollectionUtils.isEmpty(developmentPlanList)) {
			return Collections.emptyList();
		}
		List<Integer> suggestedDevelopmentIds = developmentPlanList.stream()
				.map(IDP_SuggestedDevelopmentPlanMaster ::  getIdpSuggestedDevelopmentPlanSectionId).collect(Collectors.toList());
		if(CollectionUtils.isNotEmpty(suggestedDevelopmentIds)) {
			Set<Integer> removableIds = adminService.getRemovableSelectedSuggestedDevGoalIds(suggestedDevelopmentIds);
			developmentPlanList
			.forEach(suggestedDevlopmentPlanSection -> suggestedDevlopmentPlanSection.setRemovable(removableIds.contains(suggestedDevlopmentPlanSection.getIdpSuggestedDevelopmentPlanSectionId())));
		}
		return developmentPlanList;
	}


	/* (Fetch all sub-category list for How To Lead section)
	 * @see com.yum.idp.service.IIDPService#getSubSectionList()
	 */
	private List<IDP_SubSubSectionMaster> getSubSubSectionList(Integer apprId) {
		List<IDP_SubSubSectionMaster> subSubSectionList = adminService.getSubSubSectionList(apprId); 
		if(CollectionUtils.isEmpty(subSubSectionList)) {
			return Collections.emptyList();
		}
		List<Integer> subSubSectionIds = subSubSectionList.stream()
				.map(IDP_SubSubSectionMaster :: getIdpSubSubSectionId).collect(Collectors.toList());
		if(CollectionUtils.isNotEmpty(subSubSectionIds)) {
			Set<Integer> removableIds = adminService.getRemovableSubSubCategoryIds(subSubSectionIds);
			subSubSectionList.forEach(subSubCategory -> subSubCategory.setRemovable(removableIds.contains(subSubCategory.getIdpSubSubSectionId())));
		}
		return subSubSectionList;
	}

	/* (Fetch all sub-category list for How To Lead section)
	 * @see com.yum.idp.service.IIDPService#getSubSectionList()
	 */
	private List<IDP_SubSubSubSectionMaster> getSubSubSubSectionList() {
		List<IDP_SubSubSubSectionMaster> subSubSubSectionList = adminService.getSubSubSubSectionList(); 
		if(CollectionUtils.isEmpty(subSubSubSectionList)) {
			return Collections.emptyList();
		}
		List<Integer> subSubSubSectionIds = subSubSubSectionList.stream()
				.map(IDP_SubSubSubSectionMaster :: getIdpSubSubSubSectionId).collect(Collectors.toList());
		if(CollectionUtils.isNotEmpty(subSubSubSectionIds)) {
			Set<Integer> removableIds = adminService.getRemovableSubSubSubCategoryIds(subSubSubSectionIds);
			subSubSubSectionList
			.forEach(subSubSubCategory -> subSubSubCategory.setRemovable(removableIds.contains(subSubSubCategory.getIdpSubSubSubSectionId())));
		}
		return subSubSubSectionList;
	}

	/**
	 * Save All IDP Sub-Category List
	 */
	@Override
	public List<IdpSubCategory> saveIdpSubCategoryList(List<IdpSubCategory> subCategoryList) throws YumPMSDataSaveException {
		for(IdpSubCategory subCategory : subCategoryList) {
			// Ideally this should not happen
			if(subCategory.getIdpSectionId() == null) {
				continue;
			}
			adminService.saveIdpSubCategory(new IDP_SubSectionMaster(subCategory));
		}
		
		List<IdpSubCategory> idpSubSectionList = new ArrayList<>();
		for (IdpSubCategory subCategory : subCategoryList) {
			List<IdpSubCategory> collect = 
					getSubSectionList(subCategory.getApprId(),new IDP_SectionMaster().getIdpSectionTypeFlag())
					.stream()
					.map(IdpSubCategory :: new)
					.collect(Collectors.toList());
			idpSubSectionList.addAll(collect);
		}
		return idpSubSectionList;
	}

	/**
	 * Save All IDP Sub-Sub-Category List
	 */
	@Override
	public List<IdpSubSubCategory> saveIdpSubSubCategoryList(List<IdpSubSubCategory> subSubCategoryList) throws YumPMSDataSaveException {
		for(IdpSubSubCategory subSubSection : subSubCategoryList) {
			// Ideally this should not happen
			if(subSubSection.getIdpSectionId() == null || subSubSection.getIdpSubSectionId() == null) {
				continue;
			}
			adminService.saveIdpSubSubCategory(new IDP_SubSubSectionMaster(subSubSection));
		}
		
		List<IdpSubSubCategory> idpSubSubSectionList = new ArrayList<>();
		for (IdpSubSubCategory subSubCategory : subSubCategoryList) {
			List<IdpSubSubCategory> collect = 
					getSubSubSectionList(subSubCategory.getApprId())
					.stream()
					.map(IdpSubSubCategory :: new)
					.collect(Collectors.toList());
			idpSubSubSectionList.addAll(collect);
		}
		return idpSubSubSectionList;
	}

	/**
	 * Save All IDP Sub-Sub-Sub-Category List
	 */
	@Override
	public List<IdpSubSubSubCategory> saveIdpSubSubSubCategoryList(List<IdpSubSubSubCategory> subSubSubSectionList) throws YumPMSDataSaveException {
		for(IdpSubSubSubCategory subSubSubSection : subSubSubSectionList) {
			// Ideally this should not happen
			if(subSubSubSection.getIdpSectionId() == null || subSubSubSection.getIdpSubSectionId() == null 
					|| subSubSubSection.getIdpSubSubSectionId() == null) {
				continue;
			}
			adminService.saveIdpSubSubSubCategory(new IDP_SubSubSubSectionMaster(subSubSubSection));
		}
		return getSubSubSubSectionList().stream().map(IdpSubSubSubCategory :: new).collect(Collectors.toList());
	}
	
	/**
	 * Save All IDP Suggested goal List
	 */
	@Override
	public List<IdpSuggestedGoal> saveIdpSuggestedGoalList(List<IdpSuggestedGoal> subSuggestedGoalList) throws YumPMSDataSaveException {
		for(IdpSuggestedGoal subSubSubSection : subSuggestedGoalList) {
			// Ideally this should not happen
			if(subSubSubSection.getIdpSectionId() == null || subSubSubSection.getIdpSubSectionId() == null 
					|| subSubSubSection.getIdpSubSubSectionId() == null) {
				continue;
			}
			adminService.saveIdpSuggestedGaol(new IDP_SuggestedGoalMaster(subSubSubSection));
		}
		return getSuggestedGoalList().stream().map(IdpSuggestedGoal :: new).collect(Collectors.toList());
	}
	
	/**
	 * Save All IDP Suggested Development Action Plan list
	 */
	@Override
	public List<IdpSuggestedDevelopmentActionPlan> saveIdpDevelopmentPlanList(List<IdpSuggestedDevelopmentActionPlan> developmentPlanList) throws YumPMSDataSaveException {
		for(IdpSuggestedDevelopmentActionPlan developmentPlans : developmentPlanList) {
			// Ideally this should not happen
			if(developmentPlans.getIdpSectionId() == null || developmentPlans.getIdpSubSectionId() == null 
					|| developmentPlans.getIdpSubSubSectionId() == null || developmentPlans.getIdpSuggestedGoalSectionId() == null) {
				continue;
			}
			adminService.saveIdpDevelopmentPlanList(new IDP_SuggestedDevelopmentPlanMaster(developmentPlans));
		}
		return getSuggestedDevelopmentPlanList().stream().map(IdpSuggestedDevelopmentActionPlan :: new).collect(Collectors.toList());
	}
	
	/** 
	 * @see com.yum.pms.service.IYumHibernateService#getAllTooltips()
	 * Get all the help text from database
	 */
	@Override
	public Map<String, String> getAllTooltips(String empGrade) {

		List<String> hoverNameList = util.findAllTooltips();
		Map<String, String> hoverNameHoverValue = new HashMap<>(hoverNameList.size());
		for(String hoverName : hoverNameList) {
			String description = util.getDescriptionOfHoverText(hoverName, empGrade);
			hoverNameHoverValue.put(hoverName, description);

		}		
		return hoverNameHoverValue;
	}

	@Override
	public String deleteIdpSection(Integer id, String tableName) {

		return adminService.deleteIdpSection(id, tableName) ? YumPmsConstants.SUCCESS_DELETE_MSG : YumPmsConstants.ERROR_MSG_TRY_AGAIN;
	}

	@Override
	public AdminPaConfig getPaConfigData(Integer apprId) {

		AdminPaConfig adminPaConfig = new AdminPaConfig(apprId);
		adminPaConfig.setIfValue(paDao.getIfValuePercentByApprId(apprId));
		PA_ConfigTF paConfigTf = paDao.getPaConfigTfByApprId(apprId);
		adminPaConfig.setTfValue(paConfigTf.getTfValuePercent());
		adminPaConfig.setTfValueTb(paConfigTf.getTfValueTbPercent());
		adminPaConfig.setTfValuePh(paConfigTf.getTfValuePhPercent());
		List<PA_ConfigRatingBellCurveIFRange> bellCurveIfRangeList = paDao.getRatingBellCurveIFRange(apprId); 
		if(CollectionUtils.isEmpty(bellCurveIfRangeList)) {

			List<PA_RatingMaster> paRatingMaster = paDao.getPARatingMaster();
			if(CollectionUtils.isNotEmpty(paRatingMaster)) {

				paRatingMaster.forEach(ratingMaster -> {
					PA_ConfigRatingBellCurveIFRange bellCurveIfRange = new PA_ConfigRatingBellCurveIFRange();
					bellCurveIfRange.setAppraisalPeriod(new AppraisalPeriod(apprId));
					bellCurveIfRange.setPaRating(ratingMaster);
					adminPaConfig.getBellCurveIfRange().add(bellCurveIfRange);
				});
			}
		} else {
			adminPaConfig.setBellCurveIfRange(bellCurveIfRangeList);
		}
		return adminPaConfig;
	}

	@Override
	public AdminPaConfig savePaConfigData(AdminPaConfig adminPaConfig) throws YumPMSDataSaveException {

		if(adminPaConfig == null || YumPmsUtils.isNullOrZero(adminPaConfig.getApprId())) {
			throw new YumPMSDataSaveException(YumPmsConstants.ARGUMENT_CAN_NOT_NULL);
		}
		if(StringUtils.equalsIgnoreCase(YumPmsConstants.PA_IF_TF_SECTION, adminPaConfig.getSectionName()) 
				&& adminPaConfig.getIfValue() != null && adminPaConfig.getTfValue() != null) {
			// Save pa_config_IF data
			savePaConfigIfData(adminPaConfig);
			// Save pa_config_TF data
			savePaConfigTfData(adminPaConfig);
		} else if(StringUtils.equalsIgnoreCase(YumPmsConstants.PA_BELL_CURVE_SECTION, adminPaConfig.getSectionName()) 
				&& CollectionUtils.isNotEmpty(adminPaConfig.getBellCurveIfRange())) {
			// Save PA_ConfigRatingBellCurveIFRange List
			savePaConfigBellCurveIfRangeData(adminPaConfig);
		}
		return adminPaConfig;
	}

	@Override
	public PromotionConfig getPromotionConfigData(Integer apprId) {

		if(YumPmsUtils.isNullOrZero(apprId)) {
			return new PromotionConfig(YumPmsConstants.ARGUMENT_CAN_NOT_NULL);
		}

		PromotionConfig promotionConfig = new PromotionConfig(apprId);
		List<EmployeeGradeMaster> gradeList = paDao.fetchAll(EmployeeGradeMaster.class);
		if(CollectionUtils.isNotEmpty(gradeList)) {
			promotionConfig.setGradeList(gradeList.stream()
					.filter(grade -> BooleanUtils.toBoolean(grade.getActive()))
					.collect(Collectors.toMap(EmployeeGradeMaster :: getGradeId, EmployeeGradeMaster :: getGradeDesc)));
		}
		List<PA_RatingMaster> ratingList = paDao.getPARatingMaster();
		if(CollectionUtils.isNotEmpty(ratingList)) {
			promotionConfig.setRatingList(ratingList.stream()
					.collect(Collectors.toMap(PA_RatingMaster :: getPaRatingId, PA_RatingMaster :: getPaRatingDesc)));
		}
		List<String> allIfLogic = paDao.getAllIfLogic(); 
		if(CollectionUtils.isNotEmpty(allIfLogic)) {
			promotionConfig.setIfLogicList(allIfLogic.stream().collect(Collectors.toMap(Function.identity(), Function.identity())));
		}
		List<PA_ConfigPromotionCriteria> criterias = paDao.getConfigPromotionCriteria(apprId);
		if(CollectionUtils.isNotEmpty(criterias)) {
			Set<Integer> promotionCriterias = new HashSet<>(paDao.getAllPromotionCriteriaByApprId(apprId));
			List<PromotionCriteria> promotionCriteriaList = criterias.stream().map(PromotionCriteria :: new).collect(Collectors.toList());
			promotionCriteriaList.forEach(criteria -> criteria.setDisable(promotionCriterias.contains(criteria.getCriteriaId())));
			promotionConfig.setPromotionCriterias(promotionCriteriaList);
		}
		promotionConfig.setLtsIdpOptions(paDao.getLtsIdpOptions());
		return promotionConfig;
	}

	@Override
	public void savePromotionConfigData(PromotionConfig promotionConfig) throws YumPMSDataSaveException {

		if(promotionConfig == null || YumPmsUtils.isNullOrZero(promotionConfig.getApprId())) {
			throw new YumPMSDataSaveException(YumPmsConstants.ARGUMENT_CAN_NOT_NULL);
		}

		List<PromotionCriteria> promotionCriterias = promotionConfig.getPromotionCriterias();
		if(CollectionUtils.isEmpty(promotionCriterias)) {
			throw new YumPMSDataSaveException(YumPmsConstants.ERROR_PROMOTION_CRITERIAS_BLANK);
		}

		List<PromotionCriteria> newCriterias = promotionCriterias.stream().filter(this :: isNewCriteria).collect(Collectors.toList());
		if(CollectionUtils.isNotEmpty(newCriterias)) {
			insertConfigPromotionCriteria(newCriterias, promotionConfig.getApprId(), promotionConfig.getLogonUser());
		} 

		List<PromotionCriteria> existingCriterias = promotionCriterias.stream().filter(this :: isExistingCriteria).collect(Collectors.toList());
		if(CollectionUtils.isNotEmpty(existingCriterias)) {
			updateConfigPromotionCriteria(existingCriterias, promotionConfig.getApprId(), promotionConfig.getLogonUser());
		}
	}
	
	/* (non-Javadoc)
	 * @see com.yum.pms.service.IYumHibernateService#getServerTimeOutTime()
	 */
	@Override
	public String getServerTimeOutTime()throws SQLException {
		return util.getServerTimeOutTime();
	}

	// *********************************** All private methods goes here *******************************************************

	

	/**
	 * Return true if the parameter criteria is a new else return false
	 * @param criteria
	 * @return true/false
	 */
	private boolean isNewCriteria(PromotionCriteria criteria) {
		return criteria.getCriteriaId() == null || criteria.getCriteriaId() <= 0;
	}

	/**
	 * Return false if the parameter criteria is an existing criteria else return true
	 * @param criteria
	 * @return
	 */
	private boolean isExistingCriteria(PromotionCriteria criteria) {
		return criteria.getCriteriaId() != null && criteria.getCriteriaId() > 0;
	}
	/**
	 * Insert the ConfigPromotionCriteria objects
	 * @param criterias
	 * @param apprId
	 * @param logonUser
	 * @throws YumPMSDataSaveException
	 */
	private void insertConfigPromotionCriteria(List<PromotionCriteria> criterias, Integer apprId, String logonUser) throws YumPMSDataSaveException {

		List<PA_ConfigPromotionCriteria> saveCriterias = new ArrayList<>(criterias.size());
		StringBuilder criteriaDesc = new StringBuilder(YumPmsConstants.PA_PROMOTION_CRITERIA_DESC);
		int length = criteriaDesc.length();
		for(PromotionCriteria clientCriteria : criterias) {
			PA_ConfigPromotionCriteria saveCriteria = new PA_ConfigPromotionCriteria(apprId);
			objectMapper(clientCriteria, saveCriteria);

			if(saveCriteria.getGradeFrom() != null && saveCriteria.getGradeTo() != null) {
				criteriaDesc.append(saveCriteria.getGradeFrom());
				if(saveCriteria.getGradeTo().equals(99)) {
					criteriaDesc.append(YumPmsConstants.PLUS);
				} else {
					criteriaDesc.append(YumPmsConstants.HYPHEN).append(YumPmsConstants.UPPER_CASE_ALPHABET_L).append(saveCriteria.getGradeTo());
				}
				saveCriteria.setCriteriaDesc(criteriaDesc.toString());
				criteriaDesc.setLength(length);
			}

			saveCriteria.setCreatedBy(logonUser);
			saveCriteria.setUpdatedBy(logonUser);
			saveCriterias.add(saveCriteria);
		}
		paDao.saveConfigPromotionCriteria(saveCriterias);
	}

	/**
	 * Update the ConfigPromotionCriteria objects
	 * @param criterias
	 * @param apprId
	 * @param logonUser
	 * @throws YumPMSDataSaveException
	 */
	private void updateConfigPromotionCriteria(List<PromotionCriteria> criterias, Integer apprId, String logonUser) throws YumPMSDataSaveException {

		List<PA_ConfigPromotionCriteria> saveCriteriaList = paDao.getConfigPromotionCriteria(apprId);
		if(CollectionUtils.isNotEmpty(saveCriteriaList)) {
			Map<Integer, PromotionCriteria> map = criterias.stream()
					.collect(Collectors.toMap(PromotionCriteria :: getCriteriaId, Function.identity()));
			for(PA_ConfigPromotionCriteria saveCriteria : saveCriteriaList) {
				PromotionCriteria clientCriteria = map.get(saveCriteria.getPaPromotionCriteriaId());
				if(clientCriteria != null) {
					objectMapper(clientCriteria, saveCriteria);
				}
				saveCriteria.setUpdatedBy(logonUser);
			}
			paDao.saveConfigPromotionCriteria(saveCriteriaList);
		}
	}

	/**
	 * Used to map between client JSON object to persistence model object
	 * @param fromCriteria
	 * @param toCriteria
	 */
	private void objectMapper(PromotionCriteria fromCriteria, PA_ConfigPromotionCriteria toCriteria) {

		toCriteria.setGradeFrom(fromCriteria.getGradeFrom());
		toCriteria.setGradeTo(fromCriteria.getGradeTo());
		toCriteria.setTenureMonthsRangeLower(fromCriteria.getTenureMonthsLower());
		toCriteria.setTenureMonthsRangeHigher(fromCriteria.getTenureMonthsHigher());
		toCriteria.setPaRatingIdCurrent(new PA_RatingMaster(fromCriteria.getPaRatingCurrent()));
		toCriteria.setPaRatingIdPrevious(new PA_RatingMaster(fromCriteria.getPaRatingPrevious()));
		toCriteria.setIfMinCurrent(fromCriteria.getIfMinCurrent());
		toCriteria.setIfCheckLogic(fromCriteria.getIfCheckLogic());
		toCriteria.setLtsIdpRatingCurrentMinLevel(fromCriteria.getLtsIdp());
	}


	/**
	 * This method purpose is to save/update data in PA_ConfigRatingBellCurveIFRange table.
	 * @param adminPaConfig : We have already checked the not null on this parameter in parent method
	 * @throws YumPMSDataSaveException
	 */
	private void savePaConfigBellCurveIfRangeData(AdminPaConfig adminPaConfig) throws YumPMSDataSaveException {

		List<PA_ConfigRatingBellCurveIFRange> bellCurveIFRangeList = adminPaConfig.getBellCurveIfRange();
		List<PA_ConfigRatingBellCurveIFRange> savedBellCurveIFRangeList = paDao.getRatingBellCurveIFRange(adminPaConfig.getApprId());
		if(CollectionUtils.isEmpty(savedBellCurveIFRangeList)) {
			// For first time bell curve IF range data insert	
			for(PA_ConfigRatingBellCurveIFRange bellCurveIFRange : bellCurveIFRangeList) {
				if(bellCurveIFRange.getPaConfigRatingBellCurveId() == null) {
					bellCurveIFRange.setCreatedBy(adminPaConfig.getLogonUser());
					bellCurveIFRange.setUpdatedBy(adminPaConfig.getLogonUser());
				}
				bellCurveIFRange.setAppraisalPeriod(new AppraisalPeriod(adminPaConfig.getApprId()));
			}
			adminPaConfig.setBellCurveIfRange(paDao.savePaBellCurveIfRange(bellCurveIFRangeList));
		} else {
			// For updating bell curve IF range data
			for(int i=0; i<savedBellCurveIFRangeList.size(); i++) {
				PA_ConfigRatingBellCurveIFRange bellCurveIFRange = savedBellCurveIFRangeList.get(i);
				bellCurveIFRange.setUpdatedBy(adminPaConfig.getLogonUser());
				if(bellCurveIFRangeList.size() < i + 1) {
					continue;
				}
				PA_ConfigRatingBellCurveIFRange userProvidedBellCurveIFRange = bellCurveIFRangeList.get(i);
				if(bellCurveIFRange.getPaConfigRatingBellCurveId().equals(userProvidedBellCurveIFRange.getPaConfigRatingBellCurveId())) {
					bellCurveIFRange.setBellCurveValuePercent(userProvidedBellCurveIFRange.getBellCurveValuePercent());
					bellCurveIFRange.setIfRangeValueLower(userProvidedBellCurveIFRange.getIfRangeValueLower());
					bellCurveIFRange.setIfRangeValueUpper(userProvidedBellCurveIFRange.getIfRangeValueUpper());
				}
			}
			adminPaConfig.setBellCurveIfRange(paDao.savePaBellCurveIfRange(savedBellCurveIFRangeList));
		}
	}

	/**
	 * This method purpose is to save/update data in PA_ConfigTF table.
	 * @param adminPaConfig : We have already checked the not null on this parameter in parent method
	 * @throws YumPMSDataSaveException
	 */
	private void savePaConfigTfData(AdminPaConfig adminPaConfig) throws YumPMSDataSaveException {

		PA_ConfigTF savedPaConfigTf = paDao.getPaConfigTf(adminPaConfig.getApprId());
		if(savedPaConfigTf == null) {
			savedPaConfigTf = new PA_ConfigTF();
			savedPaConfigTf.setCreatedBy(adminPaConfig.getLogonUser());
			savedPaConfigTf.setUpdatedBy(adminPaConfig.getLogonUser());
		} else {
			savedPaConfigTf.setUpdatedBy(adminPaConfig.getLogonUser());
		}
		savedPaConfigTf.setTfValuePercent(adminPaConfig.getTfValue());
		savedPaConfigTf.setTfValueTbPercent(adminPaConfig.getTfValueTb());
		savedPaConfigTf.setTfValuePhPercent(adminPaConfig.getTfValuePh());
		paDao.savePaConfigTf(savedPaConfigTf);
	}

	/**
	 * This method purpose is to save/update data in PA_ConfigIF table.
	 * @param adminPaConfig : We have already checked the not null on this parameter in parent method
	 * @throws YumPMSDataSaveException
	 */
	private void savePaConfigIfData(AdminPaConfig adminPaConfig) throws YumPMSDataSaveException {

		PA_ConfigIF savedPaConfigIf = paDao.getPaConfigIf(adminPaConfig.getApprId());
		if(savedPaConfigIf == null) {
			savedPaConfigIf = new PA_ConfigIF();
			savedPaConfigIf.setCreatedBy(adminPaConfig.getLogonUser());
			savedPaConfigIf.setUpdatedBy(adminPaConfig.getLogonUser());
		} else {
			savedPaConfigIf.setUpdatedBy(adminPaConfig.getLogonUser());
		}
		savedPaConfigIf.setIfValueWeightedAvgIndicatorPercent(adminPaConfig.getIfValue());
		paDao.savePaConfigIf(savedPaConfigIf);
	}

	/**
	 * Send email about the Goal sheet status and also attached the Goal Sheet PDF if the status id is 1005
	 * @param idpSheetId
	 * @param statusId
	 * @param param2
	 * @throws Exception
	 */
	private void sendMail(Integer goalSheetId, int statusId, String param2) throws YumPMSDataAccessException {

		List<Object[]> result = goalService.getGoalStatusChangeEmailParams(goalSheetId.toString(), param2, sessionFactory);
		if(CollectionUtils.isEmpty(result)) {
			return;
		}
		for (Object[] tuples : result) {
			String attachmentPath = StringUtils.EMPTY;
			if(statusId == 1005) {
				attachmentPath = createGoalSheetPdfPath(goalSheetId);
			}
			mailSender.sendMail(YumPmsUtils.toStringorNull(tuples[0]), 
					YumPmsUtils.toStringorNull(tuples[1]), 
					YumPmsUtils.toStringorNull(tuples[2]), 
					YumPmsUtils.toStringorNull(tuples[3]), 
					attachmentPath);
			if(StringUtils.isNotBlank(attachmentPath)) {
				ResourceUtils.delete(attachmentPath);
			}
		}
	}

	/**
	 * Create goalSheet PDF file, store in a folder and return the path 
	 * @param goalSheetId
	 * @return
	 * @throws Exception
	 */
	private String createGoalSheetPdfPath(int goalSheetId) throws YumPMSDataAccessException {

		GoalSheet goalSheet = goalService.getGoalSheetbyId(goalSheetId, sessionFactory);
		Integer apprId = goalSheet.getAppraisalPeriod().getAppraisalPeriodId();
		EmployeeGoalSheet employeeGoalSheet = getGoalSheetDetailsByEmployeeId(goalSheet.getEmployee().getEmpId(), apprId, false);
		String fileName = YumPmsUtils.getPdfFileName(goalSheet.getEmployee().getEmpName(), String.valueOf(apprId), 
				YumPmsConstants.GOAL_SHEET_PDF_FILE);
		return ResourceUtils.createTempFile(fileName, YumPmsUtils.getGoalSheetReportData(employeeGoalSheet));
	}

	/**
	 * Email notification for Tag and Cascade
	 * @param cascadeId
	 * @param empId
	 * @param cascadedToEmpStringOfList
	 */
	private void sendEmailForTagAndCascade(String param1Value, Integer empId, String cascadedEmpIds, Integer cascadeId) {

		Session session = YumHibernateUtilServices.getSession(sessionFactory); 	
		try {
			List<Object[]> results = null;
			if(util.isActiveSp(sessionFactory, "sp_EmailNotification_TagCascade_java")) {
				results = 
						session.createSQLQuery(YumPmsConstants.SP_EMAIL_NOTIFICATION_TAG_CASCADE)
						.setString(YumPmsConstants.PARAM_1, param1Value) 
						.setString(YumPmsConstants.PARAM_2,  Integer.toString(empId))
						.setString(YumPmsConstants.PARAM_3, cascadedEmpIds)
						.setString(YumPmsConstants.PARAM_4, Integer.toString(cascadeId))
						.list();
			}
			if(CollectionUtils.isNotEmpty(results)) {
				for (Object[] tuples : results) {
					mailSender.sendMail(YumPmsUtils.toStringorNull(tuples[0]), 
							YumPmsUtils.toStringorNull(tuples[1]), 
							YumPmsUtils.toStringorNull(tuples[2]), 
							YumPmsUtils.toStringorNull(tuples[3]));
				}
			}
		} catch(Exception e) {
			LOGGER.error("Mail could not be sent for Goal creation or edit. Error: {}", e.getMessage());
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
		}
	}

	/**
	 * send Email notification For Tag and Cascade edit
	 * @param goalSectionId
	 */
	private void sendEmailForTagAndCascadeEdit(Integer goalSectionId) {

		Session session = YumHibernateUtilServices.getSession(sessionFactory); 							
		try {	
			List<Object[]> results = null;
			if(util.isActiveSp(sessionFactory, "sp_EmailNotification_TagCascadeEdit_java")) {
				results = 
						session.createSQLQuery("exec sp_EmailNotification_TagCascadeEdit_java :param1")
						.setInteger(YumPmsConstants.PARAM_1, goalSectionId)
						.list();
			}
			if(CollectionUtils.isNotEmpty(results)) {
				for (Object[] tuples : results) {
					mailSender.sendMail(YumPmsUtils.toStringorNull(tuples[0]), 
							YumPmsUtils.toStringorNull(tuples[1]), 
							YumPmsUtils.toStringorNull(tuples[2]), 
							YumPmsUtils.toStringorNull(tuples[3]));
				}
			}
		} catch(Exception e) {
			LOGGER.error("Mail could not be sent for Goal creation or edit. Error: {}", e.getMessage());
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, YumPmsUtils.getExceptionMessage(e));
		}
	}

	/**
	 * delete the Cascaded Employees
	 * @param dbCascadedEmpList
	 * @param cascadedEmpList
	 * @throws YumPMSDataSaveException
	 */
	private void deleteCascadedEmps(List<CasecadeEmp> dbCascadedEmpList, List<CasecadeEmp> cascadedEmpList) throws YumPMSDataSaveException {

		List<CasecadeEmp> deletedCascadedEmpList = new ArrayList<>();
		for(CasecadeEmp deleteCascadedEmp : dbCascadedEmpList) {
			boolean bFound = false;
			for(CasecadeEmp cascadedEmp : cascadedEmpList) {
				if(cascadedEmp.getCasecadedEmpEventId() != null 
						&& cascadedEmp.getCasecadedEmpEventId().equals(deleteCascadedEmp.getCasecadedEmpEventId())) {
					bFound = true;						
				}
			}
			if(!bFound) {
				deletedCascadedEmpList.add(deleteCascadedEmp);
			}
		}
		// Delete CascadedEmp of Cascaded Objects === Complete=========
		cascadedService.deleteCascadedEmps(deletedCascadedEmpList, sessionFactory);			
	}

	/**
	 * delete the Cascaded Employees
	 * @param dbTaggedEmpList
	 * @param taggedEmpList
	 * @throws YumPMSDataSaveException
	 */
	private void deleteTaggedEmps(List<TaggedEmp> dbTaggedEmpList, List<TaggedEmp> taggedEmpList) throws YumPMSDataSaveException {

		List<TaggedEmp> deletedTaggedEmpList = new ArrayList<>();
		for(TaggedEmp deleteTaggedEmp : dbTaggedEmpList) {
			boolean bFound = false;
			for(TaggedEmp taggedEmp : taggedEmpList) {
				if(taggedEmp.getTaggedEmpEventId() != null 
						&& (taggedEmp.getTaggedEmpEventId().equals(deleteTaggedEmp.getTaggedEmpEventId()))) {
					bFound = true;						
				}
			}
			if(!bFound)
				deletedTaggedEmpList.add(deleteTaggedEmp);
		}
		// Delete TaggedEmp of Tagged Objects === Complete=========
		taggedService.deleteTaggedEmps(deletedTaggedEmpList, sessionFactory);	
	}

	private GoalSheet updateGoalSheet(EmployeeNew employee, AppraisalPeriod appraisalPeriod) throws YumPMSDataAccessException {
		
		GoalSheet goalSheet = goalService.getGoalSheetbyEmpId(employee, appraisalPeriod, sessionFactory);
		if(goalSheet.getGoalSheetId() == 0) {
			goalSheet.setAppraisalPeriod(appraisalPeriod);
			goalSheet.setEmployee(employee);
			goalSheet.setGoalStatus(new GoalStatus(1002));
			goalSheet.setCreatedBy(employee.getEmpName());
			goalSheet = updateGoalSheetAndStatus(goalSheet, employee.getEmpName());
		} else if(goalSheet.getGoalStatus().getGoalStatusId().equals(1001)) {
			goalSheet.setGoalStatus(new GoalStatus(1002));
			goalSheet = updateGoalSheetAndStatus(goalSheet, employee.getEmpName());
		}
		return goalSheet;
	}
	
	private GoalSheet updateGoalSheetAndStatus(GoalSheet goalSheet, String empName) throws YumPMSDataAccessException {
		
		try {
			goalSheet = goalService.saveGoalSheet(goalSheet, sessionFactory);
			updateGoalSheetStatus(goalSheet.getGoalSheetId(), 1002, empName);
		} catch(Exception e) {
			throw new YumPMSDataAccessException(e);
		}
		return goalSheet;
	}
	
	private AppraisalPeriod getAppraisalPeriod(Integer apprId) {
		
		if(apprId == null) {
			return goalService.getDefaultAppraisalPeriod(sessionFactory);
		}
		return appraisalService.getAppraisalPeriodbyId(apprId);
	}
	
	private GoalStatus getGoalStatus(GoalSheet goalSheet) {
		
		GoalStatus goalStatus = goalSheet.getGoalStatus();
		if (goalStatus == null) {
			goalStatus = goalService.getDefaultGoalStatus(sessionFactory);
		}
		return goalStatus;
	}
	
	private EmployeeGoal populateSupervisorGoal(EmployeeNew supervisor, Integer supervisorId, Integer apprId, Integer orgGoalId, Integer ceoGoalId) {
		
		EmployeeGoal supGoal = new EmployeeGoal();
		if(YumPmsUtils.isGreaterThanZero(supervisorId) /*&& BooleanUtils.toBoolean(supervisor.getIsActive())*/) {
			Goal supervisorMatchingGoal = goalService.getGoalByOrgCeoGoal(apprId, orgGoalId, ceoGoalId, supervisor, sessionFactory);	
			if(supervisorMatchingGoal != null) {
				Integer goalSheetStatusOfSupGoal = supervisorMatchingGoal.getGoalSheet().getGoalStatus().getGoalStatusId();
				if(goalSheetStatusOfSupGoal != null && goalSheetStatusOfSupGoal.equals(1005)) {
					supGoal.setGoalId(supervisorMatchingGoal.getGoalId());
					supGoal.setEmployeeGoalSectionList(
							supervisorMatchingGoal.getGoalSections()
							.stream()
							.map(EmployeeGoalSection :: new)
							.collect(Collectors.toList()));
				} else {
					supGoal.setEmployeeGoalSectionList(Collections.emptyList());
				}
			}
		}
		return supGoal;
	}

	private EmployeeGoal populateFunctionLtGoal(Integer functionalLtsId, Integer apprPeriodId, Integer orgGoalId, Integer ceoGoalId) {
		
		EmployeeNew ltEmployee = new EmployeeNew(null != functionalLtsId ? functionalLtsId : 0);
		boolean isActiveLt = false;
		if(ltEmployee.getEmpId() != 0) {
			isActiveLt = employeeService.isActiveLt(functionalLtsId, apprPeriodId);
		}
		EmployeeGoal funGoal = new EmployeeGoal();	
		if(isActiveLt) {
			Goal matchingGoal = goalService.getGoalByOrgCeoGoal(apprPeriodId, orgGoalId, ceoGoalId, ltEmployee, sessionFactory);	
			if(matchingGoal != null) {
				Integer goalSheetStatusOfThatGoal = matchingGoal.getGoalSheet().getGoalStatus().getGoalStatusId();
				if(goalSheetStatusOfThatGoal != null && goalSheetStatusOfThatGoal.equals(1005)) {
					funGoal.setGoalId(matchingGoal.getGoalId());
					funGoal.setEmployeeGoalSectionList(
							matchingGoal.getGoalSections().stream()
							.map(EmployeeGoalSection :: new)
							.collect(Collectors.toList()));
				} else {
					funGoal.setEmployeeGoalSectionList(Collections.emptyList());
				}
			}
		}
		return funGoal;
	}
	
	private EmployeeGoal populateEmpGoal(boolean isFromPDF, EmployeeNew employee, Integer apprPeriodId, Integer orgGoalId, Integer ceoGoalId) {

		Goal myGoal = goalService.getGoalByOrgCeoGoal(apprPeriodId, orgGoalId, ceoGoalId, employee, sessionFactory);
		if(myGoal == null) {
			return new EmployeeGoal(0, Collections.emptyList());
		}
		List<GoalSection> goalSectionList = new ArrayList<>(myGoal.getGoalSections());
		List<EmployeeGoalSection> employeeGoalsectionList = new ArrayList<>();
		for(GoalSection gSection : goalSectionList) {
			EmployeeGoalSection empGoalSection = new EmployeeGoalSection(gSection.getGoalSectionId());
			empGoalSection.setGoalSectionSummary(gSection.getGoalSectionSummary());
			empGoalSection.setGoalSectionTimeLine(DateTimeUtils.format(gSection.getGoalSectionTimeline()));
			empGoalSection.setSupervisorRemarks(gSection.getGoalSectionSupRemarks());
			empGoalSection.setLastUpdatedDate(gSection.getUpdatedDate());
			// set all the tagged and cascaded fields
			setTagAndCascadeValues(empGoalSection, gSection);
			// This check will work when the service is being called from PDF with Cascade reject/accept condition
			if(isFromPDF) {
				if(!YumPmsConstants.UPPER_CASE_ALPHABET_R.equalsIgnoreCase(empGoalSection.getTagStatus())) {
					employeeGoalsectionList.add(empGoalSection);
				}
			} else {
				employeeGoalsectionList.add(empGoalSection);
			}
		}
		return new EmployeeGoal(myGoal.getGoalId(), employeeGoalsectionList);
	}

	private void setTagAndCascadeValues(EmployeeGoalSection empGoalSection, GoalSection gSection) {
		
		//comes as tagged goal-section	
		Set<TaggedEmp> taggedList = gSection.getTaggedTo();
		if(CollectionUtils.isNotEmpty(taggedList)) {
			TaggedEmp taggedEmp = taggedList.iterator().next();
			Integer tagId = taggedEmp.getTagged().getTagId();
			empGoalSection.setTagStatus(taggedEmp.getTagStatus() ? 
					YumPmsConstants.UPPER_CASE_ALPHABET_A : YumPmsConstants.UPPER_CASE_ALPHABET_R);
			empGoalSection.setRejectDescription(taggedEmp.getTagRejactionDesc());
			empGoalSection.setTagFromEmpIdAndName(taggedService.getTaggedById(tagId, sessionFactory));
		}
		// goes to tagged goal_section
		if(CollectionUtils.isNotEmpty(gSection.getTaggeds())) {
			empGoalSection.setIsToTagged(true);
		}
		// comes as cascaded goal-section	
		Set<CasecadeEmp> cascadedEmpList = gSection.getCasecadedTo();
		if(CollectionUtils.isNotEmpty(cascadedEmpList)) {
			Integer cascadeId = cascadedEmpList.iterator().next().getCasecaded().getCascadeId();
			empGoalSection.setCascadedEmpIdAndName(cascadedService.getCascadedById(cascadeId, sessionFactory));
		} else {
			empGoalSection.setIsCascaded(false);
			empGoalSection.setCaseCadeId(0);		
		}
		// goes to cascaded goal_section
		if(CollectionUtils.isNotEmpty(gSection.getCascadeds())) {
			empGoalSection.setIsToCascaded(true);
		}
	}
	
	private List<EmployeeGoalSection> populateCustomGoal(Goal customGoal) {
		
		List<EmployeeGoalSection> empCustomGoalSections = new ArrayList<>();
		List<GoalSection> customGoalSectionList = new ArrayList<>(customGoal.getGoalSections());
		for(GoalSection customGoalSection : customGoalSectionList) {

			EmployeeGoalSection empCustomGoalSection = new EmployeeGoalSection(customGoalSection.getGoalSectionId());
			
			empCustomGoalSection.setGoalSectionSummary(customGoalSection.getGoalSectionSummary());
			empCustomGoalSection.setGoalSectionTimeLine(DateTimeUtils.format(customGoalSection.getGoalSectionTimeline()));
			empCustomGoalSection.setSupervisorRemarks(customGoalSection.getGoalSectionSupRemarks());
			Set<TaggedEmp> taggedList = customGoalSection.getTaggedTo();
			if(CollectionUtils.isNotEmpty(taggedList)) {
				TaggedEmp taggedEmp = taggedList.iterator().next();
				empCustomGoalSection.setTagStatus(BooleanUtils.toBoolean(taggedEmp.getTagStatus()) ? 
						YumPmsConstants.UPPER_CASE_ALPHABET_A : YumPmsConstants.UPPER_CASE_ALPHABET_R);
				empCustomGoalSection.setRejectDescription(taggedEmp.getTagRejactionDesc());
				Integer tagId = taggedEmp.getTagged().getTagId();
				empCustomGoalSection.setTagFromEmpIdAndName(taggedService.getTaggedById(tagId, sessionFactory));
			}
			Set<CasecadeEmp> cascadedEmpList = customGoalSection.getCasecadedTo();
			if(CollectionUtils.isNotEmpty(cascadedEmpList)) {
				Integer cascadeId = cascadedEmpList.iterator().next().getCasecaded().getCascadeId();
				empCustomGoalSection.setCascadedEmpIdAndName(cascadedService.getCascadedById(cascadeId, sessionFactory));
			} else {
				empCustomGoalSection.setIsCascaded(false);
				empCustomGoalSection.setCaseCadeId(0);		
			}				
			empCustomGoalSections.add(empCustomGoalSection);							
		}
		return empCustomGoalSections;
	}
	
	private boolean isGoalSheetChanged(boolean bGoalSheetChanged, EmployeeGoalSection section, GoalSection goalsection) {
		
		if(!bGoalSheetChanged) {	
			goalsection.setGoalSectionSupRemarks(StringUtils.trimToEmpty(goalsection.getGoalSectionSupRemarks()));
			goalsection.setGoalSectionSummary(StringUtils.trimToEmpty(goalsection.getGoalSectionSummary()));
			section.setSupervisorRemarks(StringUtils.trimToEmpty(section.getSupervisorRemarks()));
			section.setGoalSectionTimeLine(StringUtils.trimToEmpty(section.getGoalSectionTimeLine()));
			section.setGoalSectionSummary(StringUtils.trimToEmpty(section.getGoalSectionSummary()));
			if(!StringUtils.equals(goalsection.getGoalSectionSummary(), section.getGoalSectionSummary())) {
				bGoalSheetChanged = true;
			}
			if(!StringUtils.equals(goalsection.getGoalSectionSupRemarks(), section.getSupervisorRemarks())) {
				bGoalSheetChanged = true;
			}
			if(!StringUtils.equals(DateTimeUtils.format(goalsection.getGoalSectionTimeline()), section.getGoalSectionTimeLine())) {
				bGoalSheetChanged = true;
			}
		}
		return bGoalSheetChanged;
	}

	private Goal createNewGoal(String createdBy, GoalSheet goalSheet, EmployeeGoalMatrix matrix, EmployeeGoal myGoal) 
			throws YumPMSDataSaveException {
		
		Goal goal = new Goal();	
		goal.setCreatedBy(createdBy);
		Integer orGoalId = matrix.getOrgGoal() != null ? matrix.getOrgGoal().getOrgGoalId() : null;
		if(YumPmsUtils.isGreaterThanZero(orGoalId)) {
			goal.setOrgGoalId(orGoalId);
		}
		goal.setGoalSheet(goalSheet);
		Integer ceoGoalID = matrix.getCeoGoal() != null ? matrix.getCeoGoal().getCeoGoalId() : null;
		if (YumPmsUtils.isGreaterThanEqualToZero(ceoGoalID)) {
			goal.setCeoGoalId(ceoGoalID);
		}
		//do not process if there are no goal sections
		//SC21112017
		if(CollectionUtils.isNotEmpty(myGoal.getEmployeeGoalSectionList())) {
			goal = goalService.saveGoal(goal, sessionFactory); //Save and get saving object(Goal)
		}
		//eoc SC21112017
		return goal;
	}

	private void proceedToSendEmail(EmployeeGoalSection section, String prevSummary, String prevTimeLine) {

		if((BooleanUtils.toBoolean(section.getIsTagged()) || BooleanUtils.toBoolean(section.getIsCascaded())) 
				&& (!prevSummary.equalsIgnoreCase(section.getGoalSectionSummary()) || !prevTimeLine.equals(section.getGoalSectionTimeLine()))) {
			sendEmailForTagAndCascadeEdit(section.getGoalSectionId());
		}
	}

	private void setEmpGoalSectionList(EmployeeGoalSheet employeeGoalSheet, EmployeeGoal sendingMyGoal, 
			EmployeeGoalSection section, GoalSection goalsection) throws YumPMSDataSaveException {
		
		EmployeeGoalSection sendingMyGoalSection = new EmployeeGoalSection();
		if(BooleanUtils.toBoolean(section.getIsTagged())) {
			Tagged tempTagged = taggedService.getTaggedById(section.getTagId(), sessionFactory);
			EmployeeNew tempEmployee = employeeService.getEmployeeById(employeeGoalSheet.getEmp_id());
			TaggedEmp taggedEmp = taggedService.getTaggedToEmpById(tempTagged, tempEmployee, sessionFactory);
			boolean prevStatus = BooleanUtils.toBoolean(taggedEmp.getTagStatus());
			taggedEmp.setTagStatus(section.getTagStatus().equalsIgnoreCase(YumPmsConstants.UPPER_CASE_ALPHABET_A));
			taggedEmp.setTagRejactionDesc(section.getRejectDescription());	
			taggedEmp.setTagged(tempTagged);
			taggedEmp.setCreatedBy(employeeGoalSheet.getCreatedBy());
			//Save and get saving object(TaggedEmp)
			taggedEmp = taggedService.saveTagRejectionDescription(taggedEmp, sessionFactory);	
			// sending mail for tag rejection
			if((prevStatus && !section.getTagStatus().equalsIgnoreCase(YumPmsConstants.UPPER_CASE_ALPHABET_A)) 
					|| (!prevStatus && section.getTagStatus().equalsIgnoreCase(YumPmsConstants.UPPER_CASE_ALPHABET_A))) {
				//send email
				sendEmailForTagAndCascade(YumPmsUtils.getTagStatusDesc(section.getTagStatus()), tempTagged.getEmployee().getEmpId(), 
						Integer.toString(tempEmployee.getEmpId()), section.getTagId());
			}
			// Populate Sending Object==============================================================				
			sendingMyGoalSection.setIsTagged(true);
			sendingMyGoalSection.setTagFromEmployee(section.getTagFromEmployee());
			sendingMyGoalSection.setTagFromEmployeeName(section.getTagFromEmployeeName());
			sendingMyGoalSection.setTagId(section.getTagId());
			sendingMyGoalSection.setTagStatus(YumPmsUtils.getTagStatus(taggedEmp.getTagStatus()));
			sendingMyGoalSection.setRejectDescription(taggedEmp.getTagRejactionDesc());	
		}	
		//Populate Sending Object ==============================================================		
		sendingMyGoalSection.setIsToTagged(section.getIsToCascaded());
		sendingMyGoalSection.setIsToCascaded(section.getIsToCascaded());
		sendingMyGoalSection.setIsCascaded(section.getIsCascaded());
		//SC21112017
		sendingMyGoalSection.setLastUpdatedDate(null != goalsection.getUpdatedDate() ? 
				goalsection.getUpdatedDate() : goalsection.getCreatedDate());
		sendingMyGoalSection.setCaseCadeId(section.getIsCascaded() != null && section.getCaseCadeId() != null ? 
				section.getCaseCadeId() : 0); 
		sendingMyGoalSection.setCasecadedFromEmployee(section.getCasecadedFromEmployee());
		sendingMyGoalSection.setCasecadedFromEmployeeName(section.getCasecadedFromEmployeeName());
		sendingMyGoalSection.setGoalSectionId(goalsection.getGoalSectionId());
		sendingMyGoalSection.setGoalSectionSummary(goalsection.getGoalSectionSummary());
		sendingMyGoalSection.setGoalSectionTimeLine(DateTimeUtils.format(goalsection.getGoalSectionTimeline()));
		sendingMyGoalSection.setSupervisorRemarks(goalsection.getGoalSectionSupRemarks());
		if (!BooleanUtils.toBoolean(section.getForDeletion())) {
			sendingMyGoal.getEmployeeGoalSectionList().add(sendingMyGoalSection);
		}
	}

	private void deleteGoalSections(List<GoalSection> dbGoalSectionList, EmployeeGoal myGoal) throws YumPMSDataSaveException {

		if(CollectionUtils.isEmpty(dbGoalSectionList)) {
			return;
		}
		List<GoalSection> deletedGoalSectionList = new ArrayList<>();
		for(GoalSection deleteSection : dbGoalSectionList) {
			//do not delete if cascaded or tagged
			if(!deleteSection.getCascadeds().isEmpty() && !deleteSection.getTaggeds().isEmpty()) {
				continue;
			}
			boolean bDelete = false;
			for(EmployeeGoalSection deleteSectionCheck : myGoal.getEmployeeGoalSectionList()) {
				LOGGER.info("Processing GS:{} for deletion", deleteSectionCheck.getGoalSectionId());
				if((deleteSectionCheck.getGoalSectionId().equals(deleteSection.getGoalSectionId())) 
						&& BooleanUtils.toBoolean(deleteSectionCheck.getForDeletion())) {										
					bDelete = true;
					LOGGER.info("Marked :{} for deletion", deleteSectionCheck.getGoalSectionId());
					break;
				}
			}
			if(bDelete) {
				deletedGoalSectionList.add(deleteSection);
			}
		}
		// Delete process of Goal sections === Complete =========
		goalService.deleteGoalSection(deletedGoalSectionList, sessionFactory);	
	}
	
	private EmployeeNew getSupervisor(EmployeeYearWise empYearWise) {
		
		if(empYearWise != null && empYearWise.getManager() != null) {
			return employeeService.getEmployeeById(empYearWise.getManager().getEmpId());
		}
		return new EmployeeNew();
	}

	/* (non-Javadoc)
	 * @see com.yum.pms.service.IYumHibernateService#getAppraisalPeriod()
	 */
	@Override
	public List<AppraisalPeriod> getAppraisalPeriod() {
		List<AppraisalPeriod> appraisalPeriodsList = appraisalService.getAppraisalPeriordList();
		return CollectionUtils.isEmpty(appraisalPeriodsList)? Collections.emptyList() : appraisalPeriodsList; 
	}
	
	

}
