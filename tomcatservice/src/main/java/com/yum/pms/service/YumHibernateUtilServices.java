/******************************************************************** 
* Hibernate Util Classes 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.service;

import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.metadata.ClassMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.yum.pa.service.PADaoImpl;
import com.yum.pms.dao.AppraisalPeriod;
import com.yum.pms.dao.GoalStatus;
import com.yum.pms.dao.HoverConfig;
import com.yum.pms.dao.PA_SheetStatusMaster;
import com.yum.pms.dao.PmsRole;
import com.yum.pms.domain.EmailDomain;
import com.yum.pms.domain.GeneralDomain;
import com.yum.pms.exception.CommonException;
import com.yum.pms.exception.YumPMSDataSaveException;
import com.yum.pms.utils.YumPmsConstants;
import com.yum.pms.utils.YumPmsUtils;


/* This is the Service Class, all method which are not related with any particular domain, but uses on overall application, 
 * such method implement there */

@Service
@SuppressWarnings(value = {"unchecked"})
public class YumHibernateUtilServices {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(YumHibernateUtilServices.class);
	
	@Autowired
	private AppraisalService appraisalService;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private PADaoImpl paDao;
	
	@Autowired
	private YumHibernateUtilServices util;
	
	public static Session getSession(SessionFactory sessionFactory) {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();
		}catch(HibernateException he) {
			session = sessionFactory.openSession();
		}
		return session;
	}
	
	public static Session getSessionAndBeginTransaction(SessionFactory sessionFactory) {
		Session session = getSession(sessionFactory);
		session.getTransaction().begin();
		return session;
	}
	
	public static void commitTransaction(Session session) {
		if(session != null) {
			session.getTransaction().commit();
		}
	}
	
	public static void rollbackTransaction(Session session) {
		if(session != null) {
			session.getTransaction().rollback();
		}
	}
	
	public static void clearSession(Session session) {
		if(session != null) {
			session.flush();
			session.close();
		}
	}
	
	
//  get List of item name and item id map for all drop-down list 
	@Transactional
	public Map<String, Object> getDropDownList(String item, SessionFactory sessionFactory) {	

		Map<Integer, String> map = null;
		if(YumPmsConstants.DROP_DOWN_APPRAISAL_PERIOD.equalsIgnoreCase(item)) { 
			map = getMasterTableMap(AppraisalPeriod.class, YumPmsConstants.PERIOD_DESC);
		} else if(YumPmsConstants.DROP_DOWN_GOAL_STATUS.equalsIgnoreCase(item)) {
			map = getMasterTableMap(GoalStatus.class, YumPmsConstants.GOAL_STATUS_NAME);
		} else if(YumPmsConstants.DROP_DOWN_USER_ROLE.equalsIgnoreCase(item)) {
			map = getMasterTableMap(PmsRole.class, YumPmsConstants.PMS_ROLE_NAME);
		}
		if(map == null) {
			return Collections.emptyMap();
		}
		return map.entrySet().stream().collect(Collectors.toMap(entry -> String.valueOf(entry.getKey()), Entry :: getValue));
	}

	
	public List<PmsRole> getUserRollList(SessionFactory sessionFactory) {	
		return getSession(sessionFactory).createCriteria(PmsRole.class).list();
	}
	
	public List<EmailDomain> getNewJoineeMailDetails(SessionFactory sessionFactory) {
		List<Object[]> tuples = null;
		List<EmailDomain> emailList = new ArrayList<>();
		
		Session session = YumHibernateUtilServices.getSession(sessionFactory);
		SQLQuery sqlQuery = session.createSQLQuery("Select * from email_NewJoineeGoalCreationPending");
		tuples = sqlQuery.list();

		for (Object[] tuple : tuples) {
			EmailDomain email = new EmailDomain();
			email.setToMailId(null != tuple[0]? tuple[0].toString():null);
			email.setCcMailId(null != tuple[1]? tuple[1].toString():null);
			email.setSubject(null != tuple[2]? tuple[2].toString():null);
			email.setBody(null != tuple[3]? tuple[3].toString():null);
			
			emailList.add(email);
		}

		return emailList;
	}
	public List<EmailDomain> getExistingJoineeMailDetails(SessionFactory sessionFactory) {
		List<Object[]> tuples = null;
		List<EmailDomain> emailList = new ArrayList<>();
		
		Session session = YumHibernateUtilServices.getSession(sessionFactory);
		SQLQuery sqlQuery = session.createSQLQuery("Select * from email_ExistingEmployeeGoalSheetActionRequired");
		tuples = sqlQuery.list();

		for (Object[] tuple : tuples) {
			EmailDomain email = new EmailDomain();
			email.setToMailId(null != tuple[0]? tuple[0].toString():null);
			email.setCcMailId(null != tuple[1]? tuple[1].toString():null);
			email.setSubject(null != tuple[2]? tuple[2].toString():null);
			email.setBody(null != tuple[3]? tuple[3].toString():null);
			
			emailList.add(email);
		}

		return emailList;
	}
	
	public List<EmailDomain> getGoalSheetSubmitedMailDetails(SessionFactory sessionFactory) {
		AppraisalPeriod appraisalPeriod = appraisalService.getDefaultAppraisalService();
		List<Object[]> tuples = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(" SELECT * from email_SubmittedGoalSheetForManagers_Formatted")
				.list();

		List<EmailDomain> emailList = new ArrayList<>();
		String[] employeeArray = null;
		for (Object[] tuple : tuples) {
			EmailDomain email = new EmailDomain();
			email.setToMailId(YumPmsUtils.toStringorNull(tuple[0]));
			email.setCcMailId(YumPmsUtils.toStringorNull(tuple[1]));
			String manager = YumPmsUtils.toString(tuple[2]);
			employeeArray = null != tuple[3]? tuple[3].toString().split(YumPmsConstants.COMMA): null;

			List<String> employeeList = null != employeeArray ? Arrays.asList(employeeArray) : null;
			String employee = StringUtils.EMPTY;

			if(null != employeeList) {
				employee = "<table style='border:1px solid #ccc;border-collapse: collapse;'><tr><th style='border:1px solid #ccc;padding: 5px;'>"
						+ "Employee ID</th> <th style='border:1px solid #ccc;padding: 5px;'>Employee Name</th></tr>";
				for(String employeeWithId : employeeList) {
					employee = employee + 
							"<tr><td style='border:1px solid #ccc;padding: 5px;'>"
							+ employeeWithId.substring(employeeWithId.indexOf('(') + 1, employeeWithId.indexOf(')')) 
							+ "</td><td style='border:1px solid #ccc;padding: 5px;'> " 
							+ employeeWithId.substring(0, employeeWithId.indexOf('(')) + " </td></tr>";
				}
				employee = employee + "</table>";
			}
			email.setSubject("PMS Update - Submitted Goal Sheets for your employees for "+appraisalPeriod.getPeriodDesc());
			email.setBody(YumPmsConstants.DEAR  + manager+",  </BR></BR>Here is the list of your reportees who have submitted the goal sheet for "
					+ appraisalPeriod.getPeriodDesc() + " </BR></BR>" + employee + " </BR></BR>Warm Regards, </BR>RSC HR");    		
			emailList.add(email);
		}
		return emailList;
	}

	public List<EmailDomain> getLTMailSummarized(SessionFactory sessionFactory) {
		List<EmailDomain> emailList = new ArrayList<>();
		String lt = StringUtils.EMPTY;
		String mail = StringUtils.EMPTY;

		AppraisalPeriod appraisalPeriod = appraisalService.getDefaultAppraisalService();
		Session session = YumHibernateUtilServices.getSession(sessionFactory);
		
		List<String> goalSheetStatusNameList = 
				session.createSQLQuery("SELECT CAST(goal_sheet_status_name AS VARCHAR) FROM goal_sheet_status")
				.list();				
		List<String> deptList = 
				session.createSQLQuery("SELECT DISTINCT cast(function_name AS VARCHAR) FROM employee_function_mstr WHERE function_name != '' ")
				.list();
		
		for(String dept: deptList) {
			EmailDomain email = new EmailDomain();
			Integer toatalEmployee = 
					(Integer)session.createSQLQuery("select count(*) from email_CompletionStatusForLTs where LT_Function = :dept")
					.setString("dept", dept)
					.uniqueResult();
			toatalEmployee = toatalEmployee != null ? toatalEmployee.intValue() : 0;
			for(String status: goalSheetStatusNameList) {
				String sql = "select cast(To_email as varchar(200)) as To_mail,cast(CC_email as varchar(200)) as Cc_mail,"
						+ "cast(LT_Name as varchar)as Lt from email_CompletionStatusForLTs where LT_Function = :dept "
						+ "and goal_sheet_status = :status";
				SQLQuery sqlQuery = session.createSQLQuery(sql);
				sqlQuery.setString("dept", dept);
				sqlQuery.setString("status", status);
				List<Object[]> tuples = sqlQuery.list();
				if(CollectionUtils.isNotEmpty(tuples)) {
					Object[] tuple = tuples.get(0);
					email.setToMailId(YumPmsUtils.toStringorNull(tuple[0]));
					email.setCcMailId(YumPmsUtils.toStringorNull(tuple[1]));	
					lt = YumPmsUtils.toString(tuple[2]);
					mail = mail + "<tr><td style='border:1px solid #ccc;padding: 5px;'> " + status + ": </td>"
							+ "<td style='border:1px solid #ccc;padding: 5px;'>" + tuples.size() + YumPmsConstants.END_TD_TAG_2;
				}
			}
			email.setSubject("PMS Update - Department wise goal sheet completion status for "+appraisalPeriod.getPeriodDesc());
			email.setBody(YumPmsConstants.DEAR + lt.substring(0, lt.indexOf(' ') + 1) + ", </BR></BR> Consolidated goal sheet completion status for " 
					+ dept + ". </BR></BR> <table  style='border:1px solid #ccc;border-collapse: collapse;'><tr>"
					+ "<td style='border:1px solid #ccc;padding: 5px;'>Total number of employees:</td>"
					+ "<td style='border:1px solid #ccc;padding: 5px;'> "+
					toatalEmployee + YumPmsConstants.END_TD_TAG_2 + mail + "</table> </BR></BR>Warm Regards, </BR>RSC HR");
			if(StringUtils.isNotBlank(email.getToMailId())) {
				emailList.add(email);
			}
			createLTCSVDetails(dept); 
			mail = StringUtils.EMPTY;
		}
		return emailList;
	}
	
	//-----------------------------------------------------------------------------------------------------
	private void createLTCSVDetails(String dept) {
		List<GeneralDomain> domainList = new ArrayList<>();
		String sql = new StringBuilder("select CAST(To_email AS varchar(200)) AS To_mail, ")
				.append("CAST(CC_email AS varchar(200)) AS Cc_mail, ")
				.append("CAST(LT_Name AS varchar) AS Lt, ")
				.append("CAST(emp_name as varchar) AS employee, ")
				.append("CAST(goal_sheet_status AS varchar) AS status ")
				.append("FROM email_CompletionStatusForLTs WHERE LT_Function = :dept ORDER BY emp_name")
				.toString();
		List<Object[]> tuples = YumHibernateUtilServices.getSession(sessionFactory).createSQLQuery(sql).setString("dept", dept).list();
		for (Object[] tuple : tuples) {
			GeneralDomain domain = new GeneralDomain();
			domain.setEmployeeId(Integer.parseInt(tuple[3].toString()));  
			domain.setEmpName(tuple[4].toString());
			domainList.add(domain);
		}	
		YumPmsUtils.csvCreation(domainList); 
	}
	
	//-----------------------------------------------------------------------------------------------------

	public List<EmailDomain> getLTMailDetails(SessionFactory sessionFactory) {

		List<EmailDomain> emailList = new ArrayList<>();
		String lt = StringUtils.EMPTY;
		String mail = StringUtils.EMPTY;

		AppraisalPeriod appraisalPeriod = appraisalService.getDefaultAppraisalService();

		List<String> deptList = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery("SELECT DISTINCT cast(function_name AS VARCHAR) FROM employee_function_mstr WHERE function_name != '' ")
				.list();

		for(String dept: deptList) { 
			EmailDomain email = new EmailDomain();
			String sql = "select cast(To_email as varchar(200)) as To_mail,cast(CC_email as varchar(200)) as Cc_mail,cast(LT_Name as varchar)as Lt, "
					+ "cast(emp_name as varchar) as employee, cast(goal_sheet_status as varchar) as status from email_CompletionStatusForLTs "
					+ "where LT_Function = :dept order by emp_name ";
			SQLQuery sqlQuery = YumHibernateUtilServices.getSession(sessionFactory).createSQLQuery(sql);
			sqlQuery.setString("dept", dept);
			List<Object[]> tuples = sqlQuery.list();

			for (Object[] tuple : tuples) {
				email.setToMailId(null != tuple[0] ? tuple[0].toString(): null);
				email.setCcMailId(null != tuple[1] ? tuple[1].toString(): null);	
				lt = null != tuple[2] ? tuple[2].toString() : StringUtils.EMPTY;

				mail = mail + "<tr><td style='border:1px solid #ccc;padding: 5px;'> "+tuple[3].toString()
						+" </td><td style='border:1px solid #ccc;padding: 5px;'>"+tuple[4].toString() + YumPmsConstants.END_TD_TAG_2;
			}	
			email.setSubject("PMS Update - Employee wise goal sheet completion status for "+appraisalPeriod.getPeriodDesc());
			email.setBody(YumPmsConstants.DEAR + lt.substring(0, lt.indexOf(' ')+1)+", </BR></BR> Employee goal sheet completion status for "
					+dept+". </BR></BR> <table  style='border:1px solid #ccc;border-collapse: collapse;'> "+
					mail+"</table> </BR></BR>Warm Regards, </BR>RSC HR");
			if(StringUtils.isNotBlank(email.getToMailId())) {
				emailList.add(email);
			}	
			mail = StringUtils.EMPTY;
		}
		return emailList;
	}

	public List<EmailDomain> getDiscussedAndAgreedMailDetails(SessionFactory sessionFactory) {
		List<EmailDomain> emailList = null;
		if(util.isActiveSp(sessionFactory, "sp_EmailNotification_DiscussedAndAgreedMonthlyQuarterly_scheduled_java")) {
			List<Object[]> results = 
					YumHibernateUtilServices.getSession(sessionFactory)
					.createSQLQuery("exec sp_EmailNotification_DiscussedAndAgreedMonthlyQuarterly_scheduled_java ")
					.list();
			if(CollectionUtils.isEmpty(results)) {
				return Collections.emptyList();
			}
			emailList = new ArrayList<>(results.size());
			for (Object[] tuples : results) {
				EmailDomain email = new EmailDomain();
				email.setToMailId(YumPmsUtils.toStringorNull(tuples[8])); 
				email.setCcMailId(YumPmsUtils.toStringorNull(tuples[9]));
				email.setSubject(YumPmsUtils.toStringorNull(tuples[10]));
				email.setBody(YumPmsUtils.toStringorNull(tuples[11]));	
				emailList.add(email);
			}	
		}
		return emailList;
	}
	
	public List<EmailDomain> getNotDiscussedAndAgreedOfIdpMailDetails(SessionFactory sessionFactory) {
		List<Object[]> results = new ArrayList<>();
		if(util.isActiveSp(sessionFactory, "sp_EmailNotification_NotDiscussedAndAgreedSubmitted_IDP_scheduled_java")) {
			results = 
					YumHibernateUtilServices.getSession(sessionFactory)
					.createSQLQuery("exec sp_EmailNotification_NotDiscussedAndAgreedSubmitted_IDP_scheduled_java ")
					.list();
		}
		if(CollectionUtils.isEmpty(results)) {
			return Collections.emptyList();
		}
		List<EmailDomain> emailList = new ArrayList<>(results.size());
		for (Object[] tuples : results) {
			EmailDomain email = new EmailDomain();
			email.setToMailId(YumPmsUtils.toStringorNull(tuples[7])); 
			email.setCcMailId(YumPmsUtils.toStringorNull(tuples[8]));
			email.setSubject(YumPmsUtils.toStringorNull(tuples[9]));
			email.setBody(YumPmsUtils.toStringorNull(tuples[10]));	
			emailList.add(email);
		}		
		return emailList;
	}
	
	
	/**
	 * @param sessionFactory
	 * @param spName
	 * @return
	 */
	public List<EmailDomain> getGeneralMailDetails(SessionFactory sessionFactory, String spName)throws SQLException {
		List<Object[]> results = null;
		results = YumHibernateUtilServices.getSession(sessionFactory).createSQLQuery(String.format(YumPmsConstants.EXECUTE, spName)).list();

		List<EmailDomain> emailList = new ArrayList<>(results.size());
		for (Object[] tuples : results) {
			EmailDomain email = new EmailDomain();
			email.setToMailId(YumPmsUtils.toStringorNull(tuples[0])); 
			email.setCcMailId(YumPmsUtils.toStringorNull(tuples[1]));
			email.setSubject(YumPmsUtils.toStringorNull(tuples[2]));
			email.setBody(YumPmsUtils.toStringorNull(tuples[3]));	
			emailList.add(email);
		}		
		return emailList;
	}
	
	public Map<String, Object> getTagDetailsOfPdf(SessionFactory sessionFactory) {
		List<Object[]> results = 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery("SELECT * FROM tag_replacement")
				.list();
		if(CollectionUtils.isEmpty(results)) {
			return Collections.emptyMap();
		}
		return results.stream().collect(Collectors.toMap(tuples -> YumPmsUtils.toString(tuples[0]), tuples -> YumPmsUtils.toString(tuples[1])));
	}
	
	/**
	 * @return All the Help text from database
	 */
	public List<String> findAllTooltips() {
		return
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(HoverConfig.class)
				.setProjection(Projections.distinct(Projections.property("hoverName")))
				.list();
	}
	
	public String getDescriptionOfHoverText(String hoverName, String empGrade) {

		try {
			return YumHibernateUtilServices.getSession(sessionFactory).doReturningWork(connection -> {
				try (CallableStatement function = connection.prepareCall("{ ? = call fn_hover_value(?,?) }")) {
					function.registerOutParameter(1, Types.VARCHAR);
					function.setString(2,hoverName);
					function.setString(3,empGrade);

					function.execute();
					return function.getString(1);
				}
			});
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return StringUtils.EMPTY;
		}
		
	}
	
	/**
	 * get the PaSheet Status By PaSheetId
	 * @param paSheetId
	 * @return
	 */
	public String getPaSheetStatusByPaSheetId(Integer paSheetId) {
		
		PA_SheetStatusMaster paSheetStatus = paDao.getPaSheetStatusByPaSheetId(paSheetId);
		return paSheetStatus != null ? paSheetStatus.getPaSheetStatusDesc() : StringUtils.EMPTY;
	}
	
	/**
	 * Get the meta data object of the provided model/entity class
	 * @param clazz
	 * @return
	 */
	public <T> ClassMetadata getClassMetadata(Class<T> clazz) {
		
		if(!clazz.isAnnotationPresent(javax.persistence.Entity.class)) {
			throw new CommonException("Invalid Entity class");
		}
		return sessionFactory.getClassMetadata(clazz);
	}
	
	/**
	 * Return true if Identifier property is available else false
	 * @param clazz
	 * @return
	 */
	public <T> boolean hasIdentifierProperty(Class<T> clazz) {
		
		return getClassMetadata(clazz).hasIdentifierProperty();
	}
	
	/**
	 * Return true if Identifier property is available else false
	 * @param classMetadata
	 * @return
	 */
	public boolean hasIdentifierProperty(ClassMetadata classMetadata) {
		
		return classMetadata.hasIdentifierProperty();
	}
	
	/**
	 * Get the ID property name
	 * @param clazz
	 * @return
	 */
	public <T> String getIdentifierPropertyName(Class<T> clazz) {
		
		ClassMetadata classMetadata = getClassMetadata(clazz);
		if(hasIdentifierProperty(classMetadata)) {
			return classMetadata.getIdentifierPropertyName();
		}
		return StringUtils.EMPTY;
	}
	
	/**
	 * Is the field is exist in the table or not
	 * @param clazz
	 * @param fieldName
	 * @return
	 */
	public <T> boolean hasFieldExist(Class<T> clazz, String fieldName) {
		
		return getAllPropertyNames(clazz).contains(fieldName);
	}
	
	/**
	 * Get all the property names of the table
	 * @param clazz
	 * @return
	 */
	public <T> Set<String> getAllPropertyNames(Class<T> clazz) {
		
		return new HashSet<>(Arrays.asList(getClassMetadata(clazz).getPropertyNames()));
	}
	
	/**
	 * Get the map(id, description) of master table data 
	 * @param clazz
	 * @param descFieldName
	 * @return
	 */
	public <T> Map<Integer, String> getMasterTableMap(Class<T> clazz, String descFieldName) {
		
		// Validation logic
		if(clazz == null || StringUtils.isEmpty(clazz.getSimpleName()) || StringUtils.isEmpty(descFieldName)) {
			return Collections.emptyMap();
		}
		String idFieldName = getIdentifierPropertyName(clazz);
		if(StringUtils.isEmpty(idFieldName) || !hasFieldExist(clazz, descFieldName)) {
			return Collections.emptyMap();
		}
		// Create dynamic HQL
		StringBuilder hql = new StringBuilder("SELECT T.")
				.append(idFieldName).append(", T.").append(descFieldName)
				.append(" FROM ").append(clazz.getSimpleName()).append(" T");
		
		if(hasFieldExist(clazz, YumPmsConstants.ACTIVE)) {
			hql.append(" WHERE ").append(YumPmsConstants.ACTIVE).append(" = 1");
		}
		LOGGER.info("Dynamic HQL Generated... {}", hql);
		List<Object[]> list = YumHibernateUtilServices.getSession(sessionFactory).createQuery(hql.toString()).list();
		if(CollectionUtils.isEmpty(list)) {
			return Collections.emptyMap();
		}
		Map<Integer, String> resultMap = new HashMap<>(list.size());
		for (Object[] obj : list) {
			resultMap.put(YumPmsUtils.castToInt(obj[0]), YumPmsUtils.toString(obj[1]));
		}
		return resultMap;
	}
	
	/**
	 * Save/Update master table data by passing the following parameters
	 * @param clazz			- Model class
	 * @param descFieldName - Which column has to update or insert
	 * @param requestData   - Column id and value map
	 * @return
	 * @throws YumPMSDataSaveException
	 */
	public <T> Map<Integer, String> saveMasterTableMap(Class<T> clazz, String descFieldName, Map<Integer, String> requestData) 
			throws YumPMSDataSaveException {
		
		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
		String idFieldName = getIdentifierPropertyName(clazz);
		try {
			Map<Integer, String> resultMap = new HashMap<>(requestData.size());
			for(Map.Entry<Integer, String> entry : requestData.entrySet()) {
				Integer id = entry.getKey();
				T obj = clazz.newInstance();
				if(YumPmsUtils.isGreaterThanZero(id)) {
					obj = session.get(clazz, id);
				}
				YumPmsUtils.setProperty(obj, descFieldName, entry.getValue());
				obj = (T)session.merge(obj);
				resultMap.put(YumPmsUtils.castToInt(YumPmsUtils.getProperty(obj, idFieldName)), 
						String.valueOf(YumPmsUtils.getProperty(obj, descFieldName)));
			}
			YumHibernateUtilServices.commitTransaction(session);
			return resultMap;
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException("Failed to save or update in " + clazz.getSimpleName(), e);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		
	}
	
	/**
	 * delete a row By it's Id(PK)
	 * @param clazz : Model class
	 * @param id    : Primary key id
	 * @return true/false
	 */
	public <T> boolean deleteMasterTableById(Class<T> clazz, Number id) {

		try {
			T obj = YumHibernateUtilServices.getSession(sessionFactory).get(clazz, id);
			Optional.ofNullable(obj).ifPresent(persistentInstance -> {
				Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
				session.delete(persistentInstance);
				YumHibernateUtilServices.commitTransaction(session);
			});
			return true;
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return false;
		}
	}
	
	/**
	 * save Or Update a entity/model object
	 * @param t - Model class
	 * @return Persisted model class
	 * @throws YumPMSDataSaveException
	 */
	public <T> T saveOrUpdate(T t) throws YumPMSDataSaveException {
		
		if(t == null) {
			throw new IllegalArgumentException("Invalid Persistance Object(null)");
		}
		if(!hasIdentifierProperty(t.getClass())) {
			throw new CommonException("Invalid Operation: This object does not contains the IdentifierProperty");
		}
		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
		try {
			t = (T)session.merge(t);
			YumHibernateUtilServices.commitTransaction(session);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException("Failed to save or update in " + t.getClass().getSimpleName(), e);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return t;
	}
	
	/**
	 * Check an SP is active or not based on "email_notification_config" table value
	 * @param sessionFactory
	 * @param spName
	 * @return true/false
	 */
	public Boolean isActiveSp(SessionFactory sessionFactory, String spName) {
		Boolean isActive = 
				(Boolean) YumHibernateUtilServices.getSession(sessionFactory)
		.createSQLQuery(String.format("SELECT is_active FROM email_notification_config WHERE sp_name = '%s'", StringUtils.trimToEmpty(spName)))
		.uniqueResult();
		return BooleanUtils.toBoolean(isActive);
	}
	
	/**
	 * Get server session out time
	 * @return
	 */
	public String getServerTimeOutTime()  {
		String timeOut = 
		(String) YumHibernateUtilServices.getSession(sessionFactory)
		.createSQLQuery(YumPmsConstants.SERVER_TIME_OUT_QUERY)
		.uniqueResult();
		
		return timeOut;
	}

}
