/******************************************************************** 
* Object Mapper for Employee Roles Data Provider 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.domain;

public class EmployeeRoleDetails {

	private int empId;
	
	private String empName;
	
	private Integer pmsRoleId;
	
	private String pmsRoleName;
	
	private String createdBy;
	
	@Override
	public String toString() {
		return "EmployeeRoleDetails [empId=" + empId + ", empName=" + empName + ", pmsRoleId=" + pmsRoleId
				+ ", pmsRoleName=" + pmsRoleName + "]";
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public Integer getPmsRoleId() {
		return pmsRoleId;
	}

	public void setPmsRoleId(Integer pmsRoleId) {
		this.pmsRoleId = pmsRoleId;
	}

	public String getPmsRoleName() {
		return pmsRoleName;
	}

	public void setPmsRoleName(String pmsRoleName) {
		this.pmsRoleName = pmsRoleName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


}
