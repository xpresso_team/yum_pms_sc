/******************************************************************** 
* Object Mapper for Tag Details Data Provider 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/


package com.yum.pms.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class TaggedDetails implements Serializable {

	private static final long serialVersionUID = 680266986463755608L;

	private int tagId;	
	
	private Integer goalSectionId;
	
	private String editOfGoalSectionSummary;
	
	private Map<Integer,String> tagToEmployeeMap = new HashMap<>();
	
	private String message;
	
	private String taggedTimeline;
	
	private String createdBy;
	
	public int getTagId() {
		return this.tagId;
	}

	public void setTagId(int tagId) {
		this.tagId = tagId;
	}

	public Integer getGoalSectionId() {
		return goalSectionId;
	}

	public void setGoalSectionId(Integer goalSectionId) {
		this.goalSectionId = goalSectionId;
	}

	public String getEditOfGoalSectionSummary() {
		return editOfGoalSectionSummary;
	}

	public void setEditOfGoalSectionSummary(String editOfGoalSectionSummary) {
		this.editOfGoalSectionSummary = editOfGoalSectionSummary;
	}

	public Map<Integer,String> getTagToEmployeeMap() {
		return tagToEmployeeMap;
	}

	public void setTagToEmployeeMap(Map<Integer,String> tagToEmployeeMap) {
		this.tagToEmployeeMap = tagToEmployeeMap;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTaggedTimeline() {
		return taggedTimeline;
	}

	public void setTaggedTimeline(String taggedTimeline) {
		this.taggedTimeline = taggedTimeline;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}	

}
