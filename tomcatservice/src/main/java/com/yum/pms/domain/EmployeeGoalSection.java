/******************************************************************** 
* Object Mapper for Employee Goal Section Data Provider 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;

import com.yum.pms.dao.Cascaded;
import com.yum.pms.dao.GoalSection;
import com.yum.pms.dao.Tagged;
import com.yum.pms.dao.TaggedEmp;
import com.yum.pms.utils.DateTimeUtils;
import com.yum.pms.utils.YumPmsConstants;
import com.yum.pms.utils.YumPmsUtils;

public class EmployeeGoalSection implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer goalSectionId;
	
	private String goalSectionSummary;
	
	private String goalSectionTimeLine;
	
	private Boolean isTagged = false;
	
	private Boolean isToTagged = false;
	
	private Integer tagId;
	
	private Integer tagFromEmployee;
	
	private String tagFromEmployeeName;
	
	private String tagStatus;
	
	private String rejectDescription;
	
	private Boolean isToCascaded = false;
	
	private Boolean isCascaded = false;
	
	private Integer caseCadeId;
	
	private Integer CasecadedFromEmployee;
	
	private String CasecadedFromEmployeeName;
	
	private String supervisorRemarks;
	
	private Date lastUpdatedDate;
	
	private Boolean forDeletion = false;
	
	public EmployeeGoalSection() {
	}
	
	public EmployeeGoalSection(Integer goalSectionId) {
		this.goalSectionId = goalSectionId;
	}
	
	public EmployeeGoalSection(GoalSection gSection) {
		
		this.goalSectionId = gSection.getGoalSectionId();
		this.goalSectionSummary = gSection.getGoalSectionSummary();
		this.goalSectionTimeLine = DateTimeUtils.format(gSection.getGoalSectionTimeline());
		this.supervisorRemarks = gSection.getGoalSectionSupRemarks();
		Set<TaggedEmp> taggedList = gSection.getTaggedTo();
		if(CollectionUtils.isNotEmpty(taggedList)) {
			TaggedEmp taggedEmp = taggedList.iterator().next();
			this.isTagged = true;
			this.tagStatus = BooleanUtils.toBoolean(taggedEmp.getTagStatus()) ? 
					YumPmsConstants.UPPER_CASE_ALPHABET_A : YumPmsConstants.UPPER_CASE_ALPHABET_R;
			this.rejectDescription = taggedEmp.getTagRejactionDesc();
		}
	}
	
	public void setTagFromEmpIdAndName(Tagged tempTagged) {
		
		if(tempTagged != null) {
			if(YumPmsUtils.isGreaterThanZero(tempTagged.getTagId())) {
				this.tagFromEmployee = tempTagged.getEmployee().getEmpId();
				this.tagFromEmployeeName = tempTagged.getEmployee().getEmpName();
			}
			this.tagId = tempTagged.getTagId();
		}
		this.isTagged = true;
	}
	
	public void setCascadedEmpIdAndName(Cascaded cascaded) {
		
		if(cascaded != null && cascaded.getCascadeId() > 0) {
			this.CasecadedFromEmployee = cascaded.getEmployee().getEmpId();
			this.CasecadedFromEmployeeName = cascaded.getEmployee().getEmpName();
			this.caseCadeId = cascaded.getCascadeId();
		}
		this.isCascaded = true;
	}

	public Integer getGoalSectionId() {
		return goalSectionId;
	}

	public void setGoalSectionId(Integer goalSectionId) {
		this.goalSectionId = goalSectionId;
	}

	public String getGoalSectionSummary() {
		return goalSectionSummary;
	}

	public void setGoalSectionSummary(String goalSectionSummary) {
		this.goalSectionSummary = goalSectionSummary;
	}

	public String getGoalSectionTimeLine() {
		return goalSectionTimeLine;
	}

	public void setGoalSectionTimeLine(String goalSectionTimeLine) {
		this.goalSectionTimeLine = goalSectionTimeLine;
	}

	public Boolean getIsTagged() {
		return isTagged;
	}

	public void setIsTagged(Boolean isTagged) {
		this.isTagged = isTagged;
	}

	public Boolean getIsToTagged() {
		return isToTagged;
	}

	public void setIsToTagged(Boolean isToTagged) {
		this.isToTagged = isToTagged;
	}

	public Integer getTagId() {
		return tagId;
	}

	public void setTagId(Integer tagId) {
		this.tagId = tagId;
	}

	public Integer getTagFromEmployee() {
		return tagFromEmployee;
	}

	public void setTagFromEmployee(Integer tagFromEmployee) {
		this.tagFromEmployee = tagFromEmployee;
	}

	public String getTagFromEmployeeName() {
		return tagFromEmployeeName;
	}

	public void setTagFromEmployeeName(String tagFromEmployeeName) {
		this.tagFromEmployeeName = tagFromEmployeeName;
	}

	public String getTagStatus() {
		return tagStatus;
	}

	public void setTagStatus(String tagStatus) {
		this.tagStatus = tagStatus;
	}

	public String getRejectDescription() {
		return rejectDescription;
	}

	public void setRejectDescription(String rejectDescription) {
		this.rejectDescription = rejectDescription;
	}

	public Boolean getIsToCascaded() {
		return isToCascaded;
	}

	public void setIsToCascaded(Boolean isToCascaded) {
		this.isToCascaded = isToCascaded;
	}

	public Boolean getIsCascaded() {
	return isCascaded;
	}
	
	public void setIsCascaded(Boolean isCascaded) {
		this.isCascaded = isCascaded;
	}
	
	public Integer getCaseCadeId() {
		return caseCadeId;
	}

	public void setCaseCadeId(Integer caseCadeId) {
		this.caseCadeId = caseCadeId;
	}

	public Integer getCasecadedFromEmployee() {
		return CasecadedFromEmployee;
	}

	public void setCasecadedFromEmployee(Integer casecadedFromEmployee) {
		CasecadedFromEmployee = casecadedFromEmployee;
	}

	public String getCasecadedFromEmployeeName() {
		return CasecadedFromEmployeeName;
	}

	public void setCasecadedFromEmployeeName(String casecadedFromEmployeeName) {
		CasecadedFromEmployeeName = casecadedFromEmployeeName;
	}

	public String getSupervisorRemarks() {
		return supervisorRemarks;
	}

	public void setSupervisorRemarks(String supervisorRemarks) {
		this.supervisorRemarks = supervisorRemarks;
	}

	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public Boolean getForDeletion() {
		return forDeletion;
	}

	public void setForDeletion(Boolean forDeletion) {
		this.forDeletion = forDeletion;
	}
	
	

}
