/******************************************************************** 
* Object Mapper for Employee List Data Provider 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.domain;

import java.io.Serializable;
import java.util.Map.Entry;

public class ClientEmployee implements Serializable {
	
	private static final long serialVersionUID = 4701291131475012984L;

	private Integer id;
	
	private String text;
	
	public ClientEmployee() {
	}
	
	public ClientEmployee(Entry<Integer, String> entry) {
		this(entry.getKey(), entry.getValue());
	}

	public ClientEmployee(Integer id, String text) {
		this.id = id;
		this.text = text;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
