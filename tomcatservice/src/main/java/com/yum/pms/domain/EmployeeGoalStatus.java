/******************************************************************** 
* Object Mapper for Employee Goal Status Data Provider 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.domain;

import java.io.Serializable;

import com.yum.pms.dao.GoalStatus;

public class EmployeeGoalStatus implements Serializable {

	private static final long serialVersionUID = -4361915568623006209L;

	private Integer goalStatusId; 

	private String goalStatusName;
	
	private Boolean active;
	
	private String createdBy;
	
	public EmployeeGoalStatus() {
	}
	
	public EmployeeGoalStatus(GoalStatus goalStatus) {
		if(goalStatus != null) {
			this.goalStatusId = goalStatus.getGoalStatusId();
			this.goalStatusName = goalStatus.getGoalStatusName();
		}
	}

	@Override
	public String toString() {
		return "EmployeeGoalStatus [goalStatusId=" + goalStatusId + ", goalStatusName=" + goalStatusName + ", active="
				+ active + "]";
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Integer getGoalStatusId() {
		return goalStatusId;
	}

	public void setGoalStatusId(Integer goalStatusId) {
		this.goalStatusId = goalStatusId;
	}

	public String getGoalStatusName() {
		return goalStatusName;
	}

	public void setGoalStatusName(String goalStatusName) {
		this.goalStatusName = goalStatusName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	

}
