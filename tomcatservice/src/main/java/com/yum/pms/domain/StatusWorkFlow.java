/******************************************************************** 
* Object Mapper for Workflow Data Provider 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/


package com.yum.pms.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StatusWorkFlow implements Serializable {

	private static final long serialVersionUID = 4705868808397298779L;

	private Integer currentStatusId;
	
	private String currentStatusName;
	
	private Map<Integer, List<String>> possibleNextStutsNameAndUrl = new HashMap<Integer, List<String>>();
	
	private String message; 

	public Integer getCurrentStatusId() {
		return currentStatusId;
	}

	public void setCurrentStatusId(Integer currentStatusId) {
		this.currentStatusId = currentStatusId;
	}

	public String getCurrentStatusName() {
		return currentStatusName;
	}

	public void setCurrentStatusName(String currentStatusName) {
		this.currentStatusName = currentStatusName;
	}

	public Map<Integer, List<String>> getPossibleNextStutsNameAndUrl() {
		return possibleNextStutsNameAndUrl;
	}

	public void setPossibleNextStutsNameAndUrl(Map<Integer, List<String>> possibleNextStutsNameAndUrl) {
		this.possibleNextStutsNameAndUrl = possibleNextStutsNameAndUrl;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	

}
