/******************************************************************** 
* Object Mapper for Notification Settings Data Provider 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/


package com.yum.pms.domain;

import java.io.Serializable;

public class NotificationSettings implements Serializable {

	private static final long serialVersionUID = 872161495682715697L;

	private short notificationId;
	
	private String notificationName;
	
	private String settingsValue;
	
	private Boolean active;
	
	private String createdBy;

	public short getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(short notificationId) {
		this.notificationId = notificationId;
	}

	public String getNotificationName() {
		return notificationName;
	}

	public void setNotificationName(String notificationName) {
		this.notificationName = notificationName;
	}

	public String getSettingsValue() {
		return settingsValue;
	}

	public void setSettingsValue(String settingsValue) {
		this.settingsValue = settingsValue;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	
	
}
