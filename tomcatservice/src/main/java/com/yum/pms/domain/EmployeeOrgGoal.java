/******************************************************************** 
* Object Mapper for Org Goal Data Provider 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.domain;

import java.io.Serializable;

public class EmployeeOrgGoal implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Integer orgGoalId;
	
	private Integer appraisalPeriodId;
	
	private String orgGoalDesc;
	
	private Boolean active;
	
	private Boolean ylbFlag;
	
	private String weightage;
	
	private String target;
	
	private Boolean isDeletable;
	
	private String createdBy;
	
	public EmployeeOrgGoal() {
	}
	
	public EmployeeOrgGoal(Integer orgGoalId) {
		this.orgGoalId = orgGoalId;
	}

	public Integer getOrgGoalId() {
		return orgGoalId;
	}

	public void setOrgGoalId(int orgGoalId) {
		this.orgGoalId = orgGoalId;
	}

	public Integer getAppraisalPeriodId() {
		return appraisalPeriodId;
	}

	public void setAppraisalPeriodId(Integer appraisalPeriodId) {
		this.appraisalPeriodId = appraisalPeriodId;
	}

	public String getOrgGoalDesc() {
		return orgGoalDesc;
	}

	public void setOrgGoalDesc(String orgGoalDesc) {
		this.orgGoalDesc = orgGoalDesc;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getYlbFlag() {
		return ylbFlag;
	}

	public void setYlbFlag(Boolean ylbFlag) {
		this.ylbFlag = ylbFlag;
	}

	public String getWeightage() {
		return weightage;
	}

	public void setWeightage(String weightage) {
		this.weightage = weightage;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Boolean getIsDeletable() {
		return isDeletable;
	}

	public void setIsDeletable(Boolean isDeletable) {
		this.isDeletable = isDeletable;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	

}
