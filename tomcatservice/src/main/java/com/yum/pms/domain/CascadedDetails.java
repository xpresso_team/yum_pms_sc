/******************************************************************** 
* Object Mapper for Cascade Details Data Provider 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class CascadedDetails implements Serializable {

	private static final long serialVersionUID = 680266986463755608L;

	private int cascadeId;	
	
	private Integer goalSectionId;
	
	private String editedGsSummaryToEmp;
	
	private Map<Integer,String> cascadeToEmployeeMap = new HashMap<>();
	
	private String message;
	
	private String cascadedTimeline;
	
	private String createdBy;

	public int getCascadeId() {
		return cascadeId;
	}

	public void setCascadeId(int cascadeId) {
		this.cascadeId = cascadeId;
	}

	public Integer getGoalSectionId() {
		return goalSectionId;
	}

	public void setGoalSectionId(Integer goalSectionId) {
		this.goalSectionId = goalSectionId;
	}

	public String getEditedGsSummaryToEmp() {
		return editedGsSummaryToEmp;
	}

	public void setEditedGsSummaryToEmp(String editedGsSummaryToEmp) {
		this.editedGsSummaryToEmp = editedGsSummaryToEmp;
	}

	public Map<Integer, String> getCascadeToEmployeeMap() {
		return cascadeToEmployeeMap;
	}

	public void setCascadeToEmployeeMap(Map<Integer, String> cascadeToEmployeeMap) {
		this.cascadeToEmployeeMap = cascadeToEmployeeMap;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCascadedTimeline() {
		return cascadedTimeline;
	}

	public void setCascadedTimeline(String cascadedTimeline) {
		this.cascadedTimeline = cascadedTimeline;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}	

}
