/******************************************************************** 
* Object Mapper for Employee Goal Sheet Data Provider 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EmployeeGoalSheet implements Serializable {

	private static final long serialVersionUID = 6400638512725474921L;
	
	private Integer goal_sheet_id;
	private Integer appraisal_id;
	private Integer appStartDate;
	private Integer appEndDate;
	private Integer emp_id;	
	private Integer functionalLtsId;
	private String periodDesc;
	//for reportee tab
	private String functionName;
	private String empName;
	private String empDesignation;
	
	private String functionalLtsName;
	
	private Integer supervisor_id;
	private String supervisor_name;
	private String supervisor_designation;
	private String createdBy;
	private EmployeeGoalStatus goalStatus;
	private String goalLabel;
	private List<EmployeeGoalMatrix> empGoalMatrix = new ArrayList<>();

	
	public Integer getGoal_sheet_id() {
		return goal_sheet_id;
	}
	public void setGoal_sheet_id(Integer goalSheetId) {
		this.goal_sheet_id = goalSheetId;
	}
	public Integer getAppraisal_id() {
		return appraisal_id;
	}
	public void setAppraisal_id(Integer appraisalId) {
		this.appraisal_id = appraisalId;
	}
	public Integer getEmp_id() {
		return emp_id;
	}
	public void setEmp_id(Integer empId) {
		this.emp_id = empId;
	}
	public Integer getSupervisor_id() {
		return supervisor_id;
	}
	public void setSupervisor_id(Integer supervisorId) {
		this.supervisor_id = supervisorId;
	}	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public EmployeeGoalStatus getGoalStatus() {
		return goalStatus;
	}
	public void setGoalStatus(EmployeeGoalStatus goalStatus) {
		this.goalStatus = goalStatus;
	}
	public List<EmployeeGoalMatrix> getEmpGoalMatrix() {
		return empGoalMatrix;
	}
	public void setEmpGoalMatrix(List<EmployeeGoalMatrix> empGoalMatrix) {
		this.empGoalMatrix = empGoalMatrix;
	}
	public Integer getAppStartDate() {
		return appStartDate;
	}
	public void setAppStartDate(Integer appStartDate) {
		this.appStartDate = appStartDate;
	}
	public Integer getAppEndDate() {
		return appEndDate;
	}
	public void setAppEndDate(Integer appEndDate) {
		this.appEndDate = appEndDate;
	}
	public String getSupervisor_name() {
		return supervisor_name;
	}
	public void setSupervisor_name(String supervisorName) {
		this.supervisor_name = supervisorName;
	}
	public String getSupervisor_designation() {
		return supervisor_designation;
	}
	public void setSupervisor_designation(String supervisorDesignation) {
		this.supervisor_designation = supervisorDesignation;
	}
	
	public String getGoalLabel() {
		return goalLabel;
	}
	public Integer getFunctionalLtsId() {
		return functionalLtsId;
	}
	public void setFunctionalLtsId(Integer functionalLtsId) {
		this.functionalLtsId = functionalLtsId;
	}
	public void setGoalLabel(String goalLabel) {
		this.goalLabel = goalLabel;
	}
	public String getFunctionName() {
		return functionName;
	}
	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}
	public String getFunctionalLtsName() {
		return functionalLtsName;
	}
	public void setFunctionalLtsName(String functionalLtsName) {
		this.functionalLtsName = functionalLtsName;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmpDesignation() {
		return empDesignation;
	}
	public void setEmpDesignation(String empDesignation) {
		this.empDesignation = empDesignation;
	}
	public String getPeriodDesc() {
		return periodDesc;
	}
	public void setPeriodDesc(String periodDesc) {
		this.periodDesc = periodDesc;
	}

}
