package com.yum.pms.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.yum.pms.dao.CompRevisedSalaryEffectiveDuration;
import com.yum.pms.dao.EmployeeHelpTipViewStatus;
import com.yum.pms.dao.EmployeeYearWiseNotEffective;
import com.yum.pms.dao.HelpTipDocumentMaster;

public class EmployeeCurrentDetails implements Serializable {

	private static final long serialVersionUID = 6732962586165071496L;

	private Integer empId;

	private String empName;

	private String email;

	private Integer managerId;
	
	private String managerName;
	
	private Integer functionalLtsId;
	
	private String functionalLtsName;
	
	private Integer gradeId;
	
	private String grade;
	
	private Integer designationId;
	
	private String designation;
	
	private Integer functionId;
	
	private String functionName;
	
	private Integer subFunctionId;
	
	private String subFunctionName;
	
	private Integer categoryId;
	
	private String categoryName;
	
	private Integer apprPeriodId;

	private Boolean hasReportee;
	
	private String createdBy;
	
	private Date createdDate;
	
	private String updatedBy;

	private Date updatedDate;

	private Date doj;

	private Date dol;

	private Boolean active = true;

	private String operationMessage;

	private Boolean isAdmin = false;

	private Boolean isNew = false;

	private Boolean operationFlag = false;

	private Boolean isActiveLt = false;
	
	private Integer kfcAllocation;
	
	private Integer phAllocation;
	
	private Integer tbAllocation;
	
	private Date effectiveDate;
	
	@JsonIgnore private transient MultipartFile signatureImage;
	
	private String signatureFileName;
	
	private String bmuFranchise;
	
	private EmployeeYearWiseNotEffective notEffectiveData;
	
	private CompRevisedSalaryEffectiveDuration effectiveDuration;
	
	private Integer rscId;
	
	private Date rscMoveDate;
	
	private Date rscLeaveDate;
	
	private Date rld;
	
	private Date rmd;
	
	private HelpTipDocumentMaster helpTipDocumentMaster;
	
	private HelpTipDocumentMaster nonMgrhelpTipDocumentMaster;
	
	private EmployeeHelpTipViewStatus employeeHelpTipViewStatus;	
	
	public Date getRld() {
		return rld;
	}

	public void setRld(Date rld) {
		this.rld = rld;
	}

	public Date getRmd() {
		return rmd;
	}

	public void setRmd(Date rmd) {
		this.rmd = rmd;
	}

	public Integer getRscId() {
		return rscId;
	}

	public void setRscId(Integer rscId) {
		this.rscId = rscId;
	}

	public Date getRscMoveDate() {
		return rscMoveDate;
	}

	public void setRscMoveDate(Date rscMoveDate) {
		this.rscMoveDate = rscMoveDate;
	}

	public Date getRscLeaveDate() {
		return rscLeaveDate;
	}

	public void setRscLeaveDate(Date rscLeaveDate) {
		this.rscLeaveDate = rscLeaveDate;
	}


	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getManagerId() {
		return managerId;
	}

	public void setManagerId(Integer managerId) {
		this.managerId = managerId;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public Integer getFunctionalLtsId() {
		return functionalLtsId;
	}

	public void setFunctionalLtsId(Integer functionalLtsId) {
		this.functionalLtsId = functionalLtsId;
	}

	public String getFunctionalLtsName() {
		return functionalLtsName;
	}

	public void setFunctionalLtsName(String functionalLtsName) {
		this.functionalLtsName = functionalLtsName;
	}

	public Integer getGradeId() {
		return gradeId;
	}

	public void setGradeId(Integer gradeId) {
		this.gradeId = gradeId;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public Integer getDesignationId() {
		return designationId;
	}

	public void setDesignationId(Integer designationId) {
		this.designationId = designationId;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public Integer getFunctionId() {
		return functionId;
	}

	public void setFunctionId(Integer functionId) {
		this.functionId = functionId;
	}

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	public Integer getSubFunctionId() {
		return subFunctionId;
	}

	public void setSubFunctionId(Integer subFunctionId) {
		this.subFunctionId = subFunctionId;
	}

	public String getSubFunctionName() {
		return subFunctionName;
	}

	public void setSubFunctionName(String subFunctionName) {
		this.subFunctionName = subFunctionName;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Integer getApprPeriodId() {
		return apprPeriodId;
	}

	public void setApprPeriodId(Integer apprPeriodId) {
		this.apprPeriodId = apprPeriodId;
	}

	public Boolean getHasReportee() {
		return hasReportee;
	}

	public void setHasReportee(Boolean hasReportee) {
		this.hasReportee = hasReportee;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Date getDoj() {
		return doj;
	}

	public void setDoj(Date doj) {
		this.doj = doj;
	}

	public Date getDol() {
		return dol;
	}

	public void setDol(Date dol) {
		this.dol = dol;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getOperationMessage() {
		return operationMessage;
	}

	public void setOperationMessage(String operationMessage) {
		this.operationMessage = operationMessage;
	}

	public Boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public Boolean getIsNew() {
		return isNew;
	}

	public void setIsNew(Boolean isNew) {
		this.isNew = isNew;
	}

	public Boolean getOperationFlag() {
		return operationFlag;
	}

	public void setOperationFlag(Boolean operationFlag) {
		this.operationFlag = operationFlag;
	}

	public Boolean getIsActiveLt() {
		return isActiveLt;
	}

	public void setIsActiveLt(Boolean isActiveLt) {
		this.isActiveLt = isActiveLt;
	}

	public Integer getKfcAllocation() {
		return kfcAllocation;
	}

	public void setKfcAllocation(Integer kfcAllocation) {
		this.kfcAllocation = kfcAllocation;
	}

	public Integer getPhAllocation() {
		return phAllocation;
	}

	public void setPhAllocation(Integer phAllocation) {
		this.phAllocation = phAllocation;
	}

	public Integer getTbAllocation() {
		return tbAllocation;
	}

	public void setTbAllocation(Integer tbAllocation) {
		this.tbAllocation = tbAllocation;
	}

	public Date getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public MultipartFile getSignatureImage() {
		return signatureImage;
	}

	public void setSignatureImage(MultipartFile signatureImage) {
		this.signatureImage = signatureImage;
	}

	public String getSignatureFileName() {
		return signatureFileName;
	}
	
	public String getBmuFranchise() {
		return bmuFranchise;
	}

	public void setBmuFranchise(String bmuFranchise) {
		this.bmuFranchise = bmuFranchise;
	}

	public void setSignatureFileName(String signatureFileName) {
		this.signatureFileName = signatureFileName;
	}

	public EmployeeYearWiseNotEffective getNotEffectiveData() {
		return notEffectiveData;
	}

	public void setNotEffectiveData(EmployeeYearWiseNotEffective notEffectiveData) {
		this.notEffectiveData = notEffectiveData;
	}

	/**
	 * @return the effectiveDuration
	 */
	public CompRevisedSalaryEffectiveDuration getEffectiveDuration() {
		return effectiveDuration;
	}

	/**
	 * @param effectiveDuration the effectiveDuration to set
	 */
	public void setEffectiveDuration(CompRevisedSalaryEffectiveDuration effectiveDuration) {
		this.effectiveDuration = effectiveDuration;
	}

	public HelpTipDocumentMaster getHelpTipDocumentMaster() {
		return helpTipDocumentMaster;
	}

	public void setHelpTipDocumentMaster(HelpTipDocumentMaster helpTipDocumentMaster) {
		this.helpTipDocumentMaster = helpTipDocumentMaster;
	}

	public HelpTipDocumentMaster getNonMgrhelpTipDocumentMaster() {
		return nonMgrhelpTipDocumentMaster;
	}

	public void setNonMgrhelpTipDocumentMaster(
			HelpTipDocumentMaster nonMgrhelpTipDocumentMaster) {
		this.nonMgrhelpTipDocumentMaster = nonMgrhelpTipDocumentMaster;
	}

	public EmployeeHelpTipViewStatus getEmployeeHelpTipViewStatus() {
		return employeeHelpTipViewStatus;
	}

	public void setEmployeeHelpTipViewStatus(
			EmployeeHelpTipViewStatus employeeHelpTipViewStatus) {
		this.employeeHelpTipViewStatus = employeeHelpTipViewStatus;
	}
	
}
