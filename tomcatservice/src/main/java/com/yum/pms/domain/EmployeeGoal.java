/******************************************************************** 
* Object Mapper for Employee Goal Data Provider 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EmployeeGoal implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer goalId;
	
	private List<EmployeeGoalSection> employeeGoalSectionList = new ArrayList<>();

	public EmployeeGoal() {
	}
	
	public EmployeeGoal(Integer goalId) {
		this.goalId = goalId;
	}
	
	public EmployeeGoal(Integer goalId, List<EmployeeGoalSection> employeeGoalSectionList) {
		this.goalId = goalId;
		this.employeeGoalSectionList = employeeGoalSectionList;
	}

	public Integer getGoalId() {
		return goalId;
	}

	public void setGoalId(Integer goalId) {
		this.goalId = goalId;
	}

	public List<EmployeeGoalSection> getEmployeeGoalSectionList() {
		return employeeGoalSectionList;
	}

	public void setEmployeeGoalSectionList(List<EmployeeGoalSection> employeeGoalSectionList) {
		this.employeeGoalSectionList = employeeGoalSectionList;
	}
	
	
	

	
}
