/******************************************************************** 
* Object Mapper for Employee Ceo Goal Provider 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.domain;

import java.io.Serializable;

public class EmployeeCeoGoal implements Serializable {
	
	private static final long serialVersionUID = 2595346269242598224L;

	private int ceoGoalId;
	
	private int appraisalPeriodId;
	
	private String ceoGoalDesc;
	
	private Boolean active;
	
	private String createdBy;
	
	public EmployeeCeoGoal() {
	}
	
	public EmployeeCeoGoal(int ceoGoalId) {
		this.ceoGoalId = ceoGoalId;
	}
	
	public EmployeeCeoGoal(int ceoGoalId, String ceoGoalDesc, Boolean active) {
		this.ceoGoalId = ceoGoalId;
		this.ceoGoalDesc = ceoGoalDesc;
		this.active = active;
	}

	public Integer getCeoGoalId() {
		return ceoGoalId;
	}

	public void setCeoGoalId(int ceoGoalId) {
		this.ceoGoalId = ceoGoalId;
	}

	public Integer getAppraisalPeriodId() {
		return appraisalPeriodId;
	}

	public void setAppraisalPeriodId(int appraisalPeriodId) {
		this.appraisalPeriodId = appraisalPeriodId;
	}

	public String getCeoGoalDesc() {
		return ceoGoalDesc;
	}

	public void setCeoGoalDesc(String ceoGoalDesc) {
		this.ceoGoalDesc = ceoGoalDesc;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	

}
