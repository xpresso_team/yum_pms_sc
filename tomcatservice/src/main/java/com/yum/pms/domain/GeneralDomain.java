/**
 * 
 */
package com.yum.pms.domain;

import java.io.Serializable;

/**
 * @author jayanta.biswas
 *
 */
public class GeneralDomain implements Serializable {

	private static final long serialVersionUID = 4092267506289773559L;

	private int employeeId;
	
	private String empName;
	
	private String designation;
	
	private String function;
	
	private String superVisorName;
	
	private String ltName;
	
	private String goalSheetStatus;

	/**
	 * @return
	 */
	public int getEmployeeId() {
		return employeeId;
	}

	/**
	 * @param employeeId
	 */
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * @return
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return
	 */
	public String getFunction() {
		return function;
	}

	/**
	 * @param function
	 */
	public void setFunction(String function) {
		this.function = function;
	}

	/**
	 * @return
	 */
	public String getSuperVisorName() {
		return superVisorName;
	}

	/**
	 * @param superVisorName
	 */
	public void setSuperVisorName(String superVisorName) {
		this.superVisorName = superVisorName;
	}

	/**
	 * @return
	 */
	public String getLtName() {
		return ltName;
	}

	/**
	 * @param ltName
	 */
	public void setLtName(String ltName) {
		this.ltName = ltName;
	}

	/**
	 * @return
	 */
	public String getGoalSheetStatus() {
		return goalSheetStatus;
	}

	/**
	 * @param goalSheetStatus
	 */
	public void setGoalSheetStatus(String goalSheetStatus) {
		this.goalSheetStatus = goalSheetStatus;
	}
	
	

}
