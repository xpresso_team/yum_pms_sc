/******************************************************************** 
* Object Mapper for Supervisor Remarks on Task Data Provider 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.domain;

public class GoalSectionSupRemarks {
	
	private Integer goalSectionId;
	
	private String goalSectionSupRemarks;
	
	private String createdBy;
	
	public Integer getGoalSectionId() {
		return goalSectionId;
	}

	public void setGoalSectionId(Integer goalSectionId) {
		this.goalSectionId = goalSectionId;
	}

	public void setGoalSectionSupRemarks(String goalSectionSupRemarks) {
		this.goalSectionSupRemarks = goalSectionSupRemarks;
	}

	public String getGoalSectionSupRemarks() {
		return goalSectionSupRemarks;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

}
