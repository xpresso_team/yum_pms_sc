/******************************************************************** 
* Object Mapper for List of Employee Goal Row data provider 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.domain;

import java.io.Serializable;

public class EmployeeGoalMatrix implements Serializable {
	
	private static final long serialVersionUID = 4158970973675419797L;

	private EmployeeOrgGoal orgGoal = new EmployeeOrgGoal();
	
	private EmployeeCeoGoal ceoGoal = new EmployeeCeoGoal();
	
	private EmployeeGoal supGoal = new EmployeeGoal();
	
	private EmployeeGoal funtionalGoal = new EmployeeGoal();
	
	private EmployeeGoal myGoal = new EmployeeGoal();

	public EmployeeOrgGoal getOrgGoal() {
		return orgGoal;
	}

	public void setOrgGoal(EmployeeOrgGoal orgGoal) {
		this.orgGoal = orgGoal;
	}

	public EmployeeCeoGoal getCeoGoal() {
		return ceoGoal;
	}

	public void setCeoGoal(EmployeeCeoGoal ceoGoal) {
		this.ceoGoal = ceoGoal;
	}

	public EmployeeGoal getSupGoal() {
		return supGoal;
	}

	public void setSupGoal(EmployeeGoal supGoal) {
		this.supGoal = supGoal;
	}

	public EmployeeGoal getMyGoal() {
		return myGoal;
	}

	public void setMyGoal(EmployeeGoal myGoal) {
		this.myGoal = myGoal;
	}

	public EmployeeGoal getFuntionalGoal() {
		return funtionalGoal;
	}

	public void setFuntionalGoal(EmployeeGoal funtionalGoal) {
		this.funtionalGoal = funtionalGoal;
	}

}
