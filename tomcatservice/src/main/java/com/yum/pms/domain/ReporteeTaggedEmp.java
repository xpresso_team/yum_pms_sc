/******************************************************************** 
* Object Mapper for Tagged Reportee Data Provider 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/


package com.yum.pms.domain;

public class ReporteeTaggedEmp {

	private int reporteeEmpId;
	private String reporteeEmpName;
	private int tag_to_goalSectionId;

	private int tag_by_EmpId;
	private String tag_by_EmpName;
	
	private String goalSectionTimeline;
	
	private String apprPeriod;

	private boolean tag_acceptedFlag;
	private String tag_rejectDescription;
	private String taggedGoalSectionSummary;
	private String originalGoalSectionSummary;
	
	public int getReporteeEmpId() {
		return this.reporteeEmpId;
	}
	
	public void setReporteeEmpId(int empId) {
		this.reporteeEmpId=empId;
	}
	
	public String getReporteeEmpName() {
		return this.reporteeEmpName;
	}
	
	public void setReporteeEmpName(String empName) {
		this.reporteeEmpName=empName;
	}

	public int getTaggedByEmpId() {
		return this.tag_by_EmpId;
	}
	
	public void setTaggedByEmpId(int empId) {
		this.tag_by_EmpId=empId;
	}

	public String getTaggedByEmpName() {
		return this.tag_by_EmpName;
	}
	
	public void setTaggedByEmpName(String empName) {
		this.tag_by_EmpName=empName;
	}

	public boolean getTagAccepted() {
		return this.tag_acceptedFlag;
	}
	
	public void setTagAccepted(Boolean tagStatus) {
		this.tag_acceptedFlag=tagStatus.booleanValue();
	}

	public String getTagRejectedDesc() {
		return this.tag_rejectDescription;
	}
	
	public void setTagRejectedDesc(String tagRejactionDesc) {
		this.tag_rejectDescription=tagRejactionDesc;
	}

	public int getTagToGoalSectionId() {
		return this.tag_to_goalSectionId;
	}
	
	public void setTagToGoalSectionId(int goalSectionId) {
		this.tag_to_goalSectionId=goalSectionId;
	}

	public String getTaggedAppPeriod() {
		return this.apprPeriod;
	}
	
	public void setTaggedAppPeriod(String apprPeriod) {
		this.apprPeriod=apprPeriod;
	}
	
	public String getTaggedGoalSectionSummary() {
		return this.taggedGoalSectionSummary;
	}
	
	public void setTaggedGoalSectionSummary(String goalSectionSummary) {
		this.taggedGoalSectionSummary=goalSectionSummary;
	}
	
	public String getOriginalGoalSectionSummary() {
		return this.originalGoalSectionSummary;
	}
	
	public void setOriginalGoalSectionSummary(String goalSectionSummary) {
		this.originalGoalSectionSummary=goalSectionSummary;
	}
	
	public String getTaggedGoalSectionTimeline() {
		return this.goalSectionTimeline;
	}
	
	public void setTaggedGoalSectionTimeline(String goalSectionTimeline) {
		this.goalSectionTimeline=goalSectionTimeline;
	}

}
