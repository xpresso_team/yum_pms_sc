/******************************************************************** 
* Object Mapper for Employee Goal History Data Provider 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.domain;

import java.util.Date;

public class EmployeeGoalHistory {

	private int goalSheetId;
	
	private String updatedByEmpName; 

	private String goalStatusName;
	
	private Date updateDate;

	public String getUpdatedByEmpName() {
		return updatedByEmpName;
	}

	public void setUpdatedByEmpName(String updatedByEmpName) {
		this.updatedByEmpName = updatedByEmpName;
	}

	public String getGoalStatusName() {
		return goalStatusName;
	}

	public void setGoalStatusName(String goalStatusName) {
		this.goalStatusName = goalStatusName;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public int getGoalSheetId() {
		return goalSheetId;
	}

	public void setGoalSheetId(int goalSheetId) {
		this.goalSheetId = goalSheetId;
	}	
	
}
