/******************************************************************** 
* Object Mapper for Cascaded Reportee Data Provider 
* 
* http://www.abzooba.com
* ------------------- 
* Created By: jayanta.biswas
* Last Updated By: sudipta.chandra
* Last Updated On: 23-Sep-2017 (11:34:34 AM) 
*
* The software and related user documentation are protected under 
* copyright laws and remain the sole property of Abzooba. 
* 
* Technical support is available via the Abzooba website at 
* http://www.abzooba.com
**************************************************************************/

package com.yum.pms.domain;

public class ReporteeCascadedEmp {
	
	private int reporteeEmpId;
	private String reporteeEmpName;
	private int cascade_to_goalSectionId;

	private int cascade_by_EmpId;
	private String cascade_by_EmpName;
	
	private String goalSectionTimeline;
	
	private String apprPeriod;

	private String cascadedGoalSectionSummary;
	private String originalGoalSectionSummary;
	
	public int getReporteeEmpId() {
		return this.reporteeEmpId;
	}
	
	public void setReporteeEmpId(int empId) {
		this.reporteeEmpId=empId;
	}
	
	public String getReporteeEmpName() {
		return this.reporteeEmpName;
	}
	
	public void setReporteeEmpName(String empName) {
		this.reporteeEmpName=empName;
	}

	public int getCascadedByEmpId() {
		return this.cascade_by_EmpId;
	}
	
	public void setCascadedByEmpId(int empId) {
		this.cascade_by_EmpId=empId;
	}

	public String getCascadedByEmpName() {
		return this.cascade_by_EmpName;
	}
	
	public void setCascadedByEmpName(String empName) {
		this.cascade_by_EmpName=empName;
	}

	public int getCascadeToGoalSectionId() {
		return this.cascade_to_goalSectionId;
	}
	
	public void setCascadeToGoalSectionId(int goalSectionId) {
		this.cascade_to_goalSectionId=goalSectionId;
	}

	public String getCascadedAppPeriod() {
		return this.apprPeriod;
	}
	
	public void setCascadedAppPeriod(String apprPeriod) {
		this.apprPeriod=apprPeriod;
	}
	
	public String getCascadedGoalSectionSummary() {
		return this.cascadedGoalSectionSummary;
	}
	
	public void setCascadedGoalSectionSummary(String goalSectionSummary) {
		this.cascadedGoalSectionSummary=goalSectionSummary;
	}
	
	public String getOriginalGoalSectionSummary() {
		return this.originalGoalSectionSummary;
	}
	
	public void setOriginalGoalSectionSummary(String goalSectionSummary) {
		this.originalGoalSectionSummary=goalSectionSummary;
	}
	
	public String getCascadedGoalSectionTimeline() {
		return this.goalSectionTimeline;
	}
	
	public void setCascadedGoalSectionTimeline(String goalSectionTimeline) {
		this.goalSectionTimeline=goalSectionTimeline;
	}
}
