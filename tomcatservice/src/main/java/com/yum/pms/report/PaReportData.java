package com.yum.pms.report;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;

import com.yum.pa.domain.Rating;
import com.yum.pa.domain.SelfSheetData;

public class PaReportData implements Serializable {

	private static final long serialVersionUID = 4065413906280049742L;

	private Integer goalId;
	
	private String goalDesc;
	
	private List<PaGoalSectionReport> goalSectionList;
	
	public PaReportData() {
	}

	public PaReportData(Integer goalId) {
		this.goalId = goalId;
	}

	public PaReportData(Integer goalId, String goalDesc) {
		this.goalId = goalId;
		this.goalDesc = goalDesc;
	}
	
	public PaReportData(Integer goalId, String goalDesc, List<PaGoalSectionReport> goalSectionList) {
		this.goalId = goalId;
		this.goalDesc = goalDesc;
		this.goalSectionList = goalSectionList;
	}

	public PaReportData(SelfSheetData selfSheet) {
		
		this(selfSheet.getOrgGoalId(), selfSheet.getOrgGoalDesc());
		if(CollectionUtils.isNotEmpty(selfSheet.getMyGoalSectionList())) {
			this.goalSectionList = selfSheet.getMyGoalSectionList().stream().map(PaGoalSectionReport :: new).collect(Collectors.toList());
		} else {
			this.goalSectionList = Collections.emptyList();
		}
		// Here rating list should not be null. So no need to do null check
		Map<Integer, String> ratingMap = selfSheet.getRatingsList().stream().collect(Collectors.toMap(Rating :: getRatingId, Rating :: getRatingDesc));
		this.goalSectionList.forEach(goalSection -> goalSection.setRating(ratingMap.get(goalSection.getRatingId())));
	}

	/**
	 * @return the goalId
	 */
	public Integer getGoalId() {
		return goalId;
	}

	/**
	 * @param goalId the goalId to set
	 */
	public void setGoalId(Integer goalId) {
		this.goalId = goalId;
	}

	/**
	 * @return the goalDesc
	 */
	public String getGoalDesc() {
		return goalDesc;
	}

	/**
	 * @param goalDesc the goalDesc to set
	 */
	public void setGoalDesc(String goalDesc) {
		this.goalDesc = goalDesc;
	}

	/**
	 * @return the goalSectionList
	 */
	public List<PaGoalSectionReport> getGoalSectionList() {
		return goalSectionList;
	}

	/**
	 * @param goalSectionList the goalSectionList to set
	 */
	public void setGoalSectionList(List<PaGoalSectionReport> goalSectionList) {
		this.goalSectionList = goalSectionList;
	}
	
}
