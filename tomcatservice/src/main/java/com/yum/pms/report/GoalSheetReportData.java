package com.yum.pms.report;

import java.io.Serializable;

public class GoalSheetReportData implements Serializable {

	private static final long serialVersionUID = -5921312427658774926L;
	
	private String goalDesc;
	private String sectionDesc;
	
	public GoalSheetReportData() {
	}
	
	public GoalSheetReportData(String goalDesc, String sectionDesc) {
		this.goalDesc = goalDesc;
		this.sectionDesc = sectionDesc;
	}

	public String getGoalDesc() {
		return goalDesc;
	}
	
	public void setGoalDesc(String goalDesc) {
		this.goalDesc = goalDesc;
	}
	
	public String getSectionDesc() {
		return sectionDesc;
	}
	
	public void setSectionDesc(String sectionDesc) {
		this.sectionDesc = sectionDesc;
	}
	
}
