package com.yum.pms.report;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import com.yum.pa.domain.MyGoal;
import com.yum.pms.utils.DateTimeUtils;
import com.yum.pms.utils.YumPmsConstants;

public class PaGoalSectionReport implements Serializable {

	private static final long serialVersionUID = 3925168968778710006L;
	
	private String goalDesc;
	private Integer sectionId;
	private String sectionDesc;
	private String timeline;
	private String rating;
	private Integer ratingId;
	private String remarks;
	
	public PaGoalSectionReport() {
	}
	
	public PaGoalSectionReport(Integer sectionId) {
		this.sectionId = sectionId;
	}
	
	public PaGoalSectionReport(Integer sectionId, String sectionDesc) {
		this.sectionId = sectionId;
		this.sectionDesc = sectionDesc;
	}
	
	public PaGoalSectionReport(MyGoal myGoal) {
		this(myGoal.getGoalSecId(), myGoal.getGoalSecDesc());
		String formattedTimeline = DateTimeUtils.format(myGoal.getTimeline(), YumPmsConstants.TIME_LINE_DATE_FORMAT);
		this.timeline = StringUtils.isEmpty(formattedTimeline) ? StringUtils.EMPTY : YumPmsConstants.TIME_LINE_LABEL_BY + formattedTimeline;
		this.ratingId = NumberUtils.toInt(myGoal.getSelectedRatingEmp());
		this.remarks = myGoal.getRatingRemarks();
	}

	/**
	 * @return the sectionId
	 */
	public Integer getSectionId() {
		return sectionId;
	}

	/**
	 * @param sectionId the sectionId to set
	 */
	public void setSectionId(Integer sectionId) {
		this.sectionId = sectionId;
	}

	/**
	 * @return the sectionDesc
	 */
	public String getSectionDesc() {
		return sectionDesc;
	}

	/**
	 * @param sectionDesc the sectionDesc to set
	 */
	public void setSectionDesc(String sectionDesc) {
		this.sectionDesc = sectionDesc;
	}

	/**
	 * @return the timeline
	 */
	public String getTimeline() {
		return timeline;
	}

	/**
	 * @param timeline the timeline to set
	 */
	public void setTimeline(String timeline) {
		this.timeline = timeline;
	}

	/**
	 * @return the rating
	 */
	public String getRating() {
		return rating;
	}

	/**
	 * @param rating the rating to set
	 */
	public void setRating(String rating) {
		this.rating = rating;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the ratingId
	 */
	public Integer getRatingId() {
		return ratingId;
	}

	/**
	 * @param ratingId the ratingId to set
	 */
	public void setRatingId(Integer ratingId) {
		this.ratingId = ratingId;
	}
	
	/**
	 * @return the goalDesc
	 */
	public String getGoalDesc() {
		return goalDesc;
	}

	/**
	 * @param goalDesc the goalDesc to set
	 */
	public void setGoalDesc(String goalDesc) {
		this.goalDesc = goalDesc;
	}
	
}
