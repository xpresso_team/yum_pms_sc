package com.yum.pms.report;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.yum.pms.exception.CommonException;
import com.yum.pms.utils.HeaderFooterPageEvent;
import com.yum.pms.utils.ResourceUtils;
import com.yum.pms.utils.YumPmsConstants;
import com.yum.pms.utils.YumPmsUtils;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;

/**
 * This class handle the jasper report load, compile, fill and export to desired format
 * @author dipak.swain
 */
public class ReportUtils {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ResourceUtils.class);
	private static final String REPORTS = "reports";
	private static final String ERROR_MSG = "Can't create object of this class";
	
	private ReportUtils() {
		LOGGER.error(ERROR_MSG);
		throw new UnsupportedOperationException(ERROR_MSG);
	}
	
	public static InputStream loadFile(String fileName) {
		return ResourceUtils.getResourceAsStream(new StringBuilder(REPORTS).append(File.separator).append(fileName).toString());
	}
	
	public static JasperReport compileReport(InputStream inputStream) {
		try {
			return JasperCompileManager.compileReport(inputStream);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new CommonException(e);
		}
	}
	
	public static JasperReport compileReport(String fileName) {
		return compileReport(loadFile(fileName));
	}
	
	public static JasperPrint fillReport(JasperReport jasperReport, Map<String, Object> params, JRDataSource dataSource) {
		try {
			return JasperFillManager.fillReport(jasperReport, params, getEmptyDataSourceIfNull(dataSource));
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new CommonException(e);
		}
	}
	
	public static JasperPrint fillReport(String jrxmlFilePath, Map<String, Object> params, JRDataSource dataSource) {
		JasperReport jasperReport = compileReport(jrxmlFilePath);
		return fillReport(jasperReport, params, getEmptyDataSourceIfNull(dataSource));
	}
	
	public static JasperPrint fillReport(JasperReport jasperReport, Map<String, Object> params, DataSource dataSource) {
		try {
			return JasperFillManager.fillReport(jasperReport, params, dataSource.getConnection());
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new CommonException(e);
		}
	}
	
	public static JRDataSource getEmptyDataSourceIfNull(JRDataSource dataSource) {
		return dataSource == null ? new JREmptyDataSource() : dataSource;
	}
	
	public static byte[] getReportContent(String reportFilePath, Map<String, Object> params, JRDataSource dataSource) {
		JasperPrint jasperPrint = fillReport(reportFilePath, params, dataSource);
		try {
			return JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new CommonException(e);
		}
	}
	
	public static byte[] getReportContent(String reportFilePath, Map<String, Object> params) {
		return getReportContent(reportFilePath, params, null);
	}
	
	public static byte[] getReportContent(ReportName reportName, Map<String, Object> params) {
		return getReportContent(reportName.toString(), params, null);
	}
	
	public static byte[] getReportContent(ReportName reportName, List<Map<String, Object>> paramsList, boolean isExcelReport) {
		
		JasperReport jasperReport = compileReport(reportName.toString());
		JRDataSource dataSource = null;
		List<JasperPrint> jasperPrintList = paramsList.stream()
				.map(params -> fillReport(jasperReport, params, dataSource))
				.collect(Collectors.toList());
		
		JasperPrint[] jasperPrintArr = jasperPrintList.toArray(new JasperPrint[jasperPrintList.size()]);
		return isExcelReport ? getExcelContent(jasperPrintArr) : getPdfContent(jasperPrintArr);
	}
	
	public static synchronized byte[] getHtmlToPdfContent(String ...pages) {
		
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		int pageCount = 0;
		try {
			Document document = new Document(PageSize.A4);
			PdfWriter writer = PdfWriter.getInstance(document, output);
			writer.setPageEvent(HeaderFooterPageEvent.INSTANCE);
			document.open();
			for(int i=0; i<pages.length-1; i++) {
				XMLWorkerHelper.getInstance().parseXHtml(writer, document, new StringReader(wrapContentByDiv(pages[i]))); 
				writer.setPageEmpty(false);
				document.newPage();
			}
			XMLWorkerHelper.getInstance().parseXHtml(writer, document, new StringReader(wrapContentByDiv(pages[pages.length-1])));
			pageCount = writer.getPageNumber();
			document.close();
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			throw new CommonException(e);
		}
		LOGGER.info( "PDF content created..., Content Size = {} and Page count = {}", YumPmsUtils.getSize(output.size()), pageCount);
		return output.toByteArray();
	}
	
	private static String wrapContentByDiv(String html) {
		return YumPmsConstants.HTML_DIV_START_TAG + StringUtils.trimToEmpty(html) + YumPmsConstants.HTML_DIV_END_TAG;
	}
	
	private static byte[] getPdfContent(JasperPrint ...jasperPrintList) {
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setExporterInput(SimpleExporterInput.getInstance(Arrays.asList(jasperPrintList)));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
			exporter.setConfiguration(getPDFConfiguration());
			exporter.exportReport();
			return outputStream.toByteArray();
		} catch(JRException e) {
			LOGGER.error(e.getMessage());
			throw new CommonException(e);
		}
	}
	
	private static byte[] getExcelContent(JasperPrint ...jasperPrintList) {
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(SimpleExporterInput.getInstance(Arrays.asList(jasperPrintList)));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
			exporter.setConfiguration(getExcelConfiguration());
			exporter.exportReport();
			return outputStream.toByteArray();
		} catch(JRException e) {
			LOGGER.error(e.getMessage());
			throw new CommonException(e);
		}
	}
	
	private static SimplePdfExporterConfiguration getPDFConfiguration() {
		SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
		configuration.setCreatingBatchModeBookmarks(true);
		configuration.setMetadataAuthor("YumPMSAdmin");
		configuration.setMetadataCreator("YumPMSAdmin");
		return configuration;
	}

	private static SimpleXlsxReportConfiguration getExcelConfiguration() {
		SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
		configuration.setOnePagePerSheet(false);
		configuration.setWrapText(true);
		configuration.setRemoveEmptySpaceBetweenRows(true);
		configuration.setCollapseRowSpan(true);
		configuration.setDetectCellType(true);
		configuration.setIgnoreCellBackground(false);
		return configuration;
	}

}
