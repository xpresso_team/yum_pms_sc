package com.yum.pms.report;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;

import com.yum.idp.domain.BalanceOfGoal;
import com.yum.idp.domain.DevelopmentActionPlan;
import com.yum.idp.domain.HowLeadSubData;
import com.yum.idp.domain.UiControll;
import com.yum.pms.utils.DateTimeUtils;
import com.yum.pms.utils.YumPmsConstants;

public class ReportModel {
	
	private String label;
	private String desc;
	private boolean flag;
	
	ReportModel() {
	}
	
	public ReportModel(String label, String desc) {
		this.label = label;
		this.desc = desc;
	}

	public ReportModel(String label, String desc, boolean flag) {
		this.label = label;
		this.desc = desc;
		this.flag = flag;
	}
	
	public ReportModel(DevelopmentActionPlan devPlan) {
		this.label = getFormattedDate(devPlan);
		this.desc = devPlan.getDevActionSummary();
		this.flag = devPlan.getIsOnGoing();
	}
	
	public ReportModel(BalanceOfGoal balanceGoal) {
		if(balanceGoal.getIdpGoalSectionTimeline() != null) {
			this.label = DateTimeUtils.format(balanceGoal.getIdpGoalSectionTimeline(), YumPmsConstants.TIME_LINE_DATE_FORMAT);
		} else {
			this.label = StringUtils.EMPTY;
		}
		this.desc = balanceGoal.getBalanceGoalDesc();
	}
	
	public ReportModel(HowLeadSubData howLead) {
		this.label = howLead.getSubCategoryDesc();
		String subSubSubDesc = howLead.getSubSubSubCategoryDesc();
		String subSubDesc = howLead.getSubSubCategoryDesc();
		if(StringUtils.isNotBlank(subSubSubDesc)) {
			this.desc = subSubSubDesc;
		} else if(StringUtils.isNotBlank(subSubDesc) && StringUtils.isBlank(howLead.getDescription())) {
			this.desc = subSubDesc;
		} else {
			this.desc = howLead.getDescription();
		}
	}
	
	public ReportModel(UiControll performance) {
		this.label = performance.getLabel();
		this.desc = performance.getValue();
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	
	private static String getFormattedDate(DevelopmentActionPlan pojo) {
		try {
			if(pojo.getTimeline() == null || pojo.getTimeline().isEmpty() || BooleanUtils.toBoolean(pojo.getIsOnGoing())) {
				return pojo.getTimeline();
			}
			return DateTimeUtils.format(pojo.getTimeline(), "MMM, yyyy");
		} catch(Exception e) {
			return pojo.getTimeline();
		}
	}
	
}
