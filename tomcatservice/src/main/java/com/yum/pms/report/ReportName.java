package com.yum.pms.report;

public enum ReportName {
	
	IDP_SHEET_REPORT("IDPSheetPdfReport.jrxml"), GOAL_SHEET_REPORT("GoalSheetPdfReport.jrxml"), PA_SHEET_REPORT("PaSheetPdfReport2.jrxml");
	
	private String reportFileName;
	
	ReportName(String reportFileName) {
		this.reportFileName = reportFileName;
	}
	
	@Override
	public String toString() {
		return reportFileName;
	}

}
