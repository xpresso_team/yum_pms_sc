package com.yum.comp.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.yum.comp.domain.CompLetterUiData;
import com.yum.comp.domain.CompUiRevisedSalary;
import com.yum.comp.domain.CompensationLetter;
import com.yum.comp.domain.CompensationUiConfig;
import com.yum.comp.domain.ConfigData;
import com.yum.comp.domain.DesgWiseVpay;
import com.yum.comp.domain.DisplayDomain;
import com.yum.comp.domain.GeneralConfigData;
import com.yum.comp.domain.GradeLookupData;
import com.yum.comp.domain.GradeWisePayRange;
import com.yum.comp.domain.LetterTemplateConfig;
import com.yum.comp.domain.LookupUiConfig;
import com.yum.comp.domain.PayRangeData;
import com.yum.comp.domain.SalaryHeadData;
import com.yum.comp.domain.VpayData;
import com.yum.comp.domain.YearWisePayRangeData;
import com.yum.pa.service.PADaoImpl;
import com.yum.pms.dao.AppraisalPeriod;
import com.yum.pms.dao.CompBonusConfiguration;
import com.yum.pms.dao.CompDesignationWiseVpayPercentage;
import com.yum.pms.dao.CompGeneralConfiguration;
import com.yum.pms.dao.CompLetterStatus;
import com.yum.pms.dao.CompLetterTemplate;
import com.yum.pms.dao.CompLevelYearWiseLookup;
import com.yum.pms.dao.CompLookupSalaryHeads;
import com.yum.pms.dao.CompPayRangeCategoryMaster;
import com.yum.pms.dao.CompRevisedSalaryEffectiveDuration;
import com.yum.pms.dao.CompSalaryFileds;
import com.yum.pms.dao.CompYearWiseMultiplier;
import com.yum.pms.dao.CompYearWisePayRange;
import com.yum.pms.dao.CompYearWiseSalary;
import com.yum.pms.dao.EmployeeDesignationMaster;
import com.yum.pms.dao.EmployeeGradeMaster;
import com.yum.pms.dao.EmployeeNew;
import com.yum.pms.dao.PA_RatingMaster;
import com.yum.pms.exception.CommonException;
import com.yum.pms.exception.YumPMSDataSaveException;
import com.yum.pms.service.AppraisalService;
import com.yum.pms.service.EmployeeServices;
import com.yum.pms.utils.DateTimeUtils;
import com.yum.pms.utils.ResourceUtils;
import com.yum.pms.utils.SendMail;
import com.yum.pms.utils.YumPmsConstants;
import com.yum.pms.utils.YumPmsUtils;

@Service
public class CompensationServiceImpl implements CompensationService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CompensationServiceImpl.class);
	
	@Autowired
	private CompensationDao compDao;
	
	@Autowired
	private AppraisalService apprService;
	
	@Autowired
	private EmployeeServices employeeService;
	
	@Autowired
	private PADaoImpl paDao;
	
	@Autowired
	private SendMail mailSender;
	
	@Override
	public List<CompensationUiConfig> getReviesSalary(Integer empId, String curYear, String nextYear, String action)throws SQLException {

		List<CompensationUiConfig> reviseSalaryList = new ArrayList<>();
		CompensationUiConfig reviseSal = null;
		Map<String, String>  params = new HashMap<>();
		params.put(YumPmsConstants.SP_REVISED_SALARY_PARAM_EMP_ID, YumPmsUtils.toStringorNull(empId));
		params.put(YumPmsConstants.SP_PARAM_CUR_YEAR, curYear);
		params.put(YumPmsConstants.SP_PARAM_NEXT_YEAR, nextYear);
		params.put(YumPmsConstants.ACTION, action);
		AppraisalPeriod currentAppraisalPeriord = apprService.getAppraisalPeriodbyDescription(curYear);

		List<Map<String, String>> reviseSalData = compDao.revisedSalary(params);		
		for(Map<String, String> reviseSalary : reviseSalData) {
			reviseSal = new CompensationUiConfig();
			reviseSal.setCurYear(curYear);
			reviseSal.setNextYear(nextYear);			
			
			List<DisplayDomain> columnList = new ArrayList<>(reviseSalary.size());
			Map<String, String> topProperty = new HashMap<>();
			int count = 1 ;
			for(Map.Entry<String, String> entry : reviseSalary.entrySet()) {
				String columnName = entry.getKey();
				String columnValue =  null != entry.getValue()? entry.getValue() : "NA"; 
				DisplayDomain columnDetails = new DisplayDomain();

				columnDetails.setColumnName(columnName.substring(0, columnName.indexOf(YumPmsConstants.HASH)));  
				columnDetails.setColumnPosition(NumberUtils.toInt(columnName.substring(columnName.indexOf(YumPmsConstants.HASH)+1, columnName.indexOf(YumPmsConstants.DOLLER))));
				columnDetails.setIsEditable(columnName.substring(columnName.lastIndexOf(YumPmsConstants.EDITABLE)+10, columnName.lastIndexOf(YumPmsConstants.DISPLAY)).equalsIgnoreCase(YumPmsConstants.YES)? true : false);
				columnDetails.setIsDisplay(columnName.substring(columnName.lastIndexOf(YumPmsConstants.DISPLAY)+9, columnName.lastIndexOf(YumPmsConstants.TOP_PROPERTY)).equalsIgnoreCase(YumPmsConstants.YES)? true : false);
				columnDetails.setColumnValue(columnValue); 
				
				if(count == 2) {
					reviseSal.setEmpId(NumberUtils.toInt(columnValue));
					reviseSal.setAdditionalCorrectionEditableFlag(paDao.getOverallRatingAdminbyEmpId(NumberUtils.toInt(columnValue),
														  currentAppraisalPeriord.getAppraisalPeriodId()).getAdditionalIncreaseNom());
				}
				count++; 
				checkActionConditions(action, columnList, topProperty,
						columnName, columnValue, columnDetails);
				
			}
			reviseSal.setTopProperty(topProperty); 
			reviseSal.setColumnList(columnList); 
			reviseSalaryList.add(reviseSal);
		}
		return reviseSalaryList;
	}

	private void checkActionConditions(String action,
			List<DisplayDomain> columnList, Map<String, String> topProperty,
			String columnName, String columnValue, DisplayDomain columnDetails) {
		if(null != action && action.equalsIgnoreCase(YumPmsConstants.VIEW)) {
			if(CollectionUtils.isNotEmpty(columnList) && (columnDetails.getColumnPosition() == columnList.get(columnList.size()-1).getColumnPosition())) {
				columnList.get(columnList.size()-1).setNextColumnValue(columnDetails.getColumnValue());
			}else if((columnName.substring(columnName.lastIndexOf(YumPmsConstants.TOP_PROPERTY)+11).equalsIgnoreCase(YumPmsConstants.YES))){
				topProperty.put(columnDetails.getColumnName(), columnValue);
			}else {
				columnList.add(columnDetails);
			}
		}else {
			columnList.add(columnDetails);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.yum.comp.service.CompensationService#getBonusSheetList(java.lang.Integer, java.lang.String, java.lang.String)
	 */
	@Override
	public List<CompensationUiConfig> getBonusSheetList(Integer empId, String curYear, String nextYear) throws SQLException {

		List<CompensationUiConfig> bonusSheetList = new ArrayList<>();
		CompensationUiConfig bonusSheet = null;
		
		List<Map<String, String>> bonusData = compDao.bonusCalculation(YumPmsUtils.toStringorNull(empId), curYear, nextYear);
		for(Map<String, String> bonusMap : bonusData) {
			bonusSheet = new CompensationUiConfig();
			bonusSheet.setCurYear(curYear);
			bonusSheet.setNextYear(nextYear); 
			List<DisplayDomain> columnList = new ArrayList<>(bonusMap.size());
			int count = 1;
			for(Map.Entry<String, String> entry : bonusMap.entrySet()) {
				String columnName = entry.getKey();
				String columnValue =  entry.getValue();
				DisplayDomain columnDetails = new DisplayDomain();
				columnDetails.setColumnName(columnName.substring(0, columnName.indexOf(YumPmsConstants.HASH)));  
				columnDetails.setColumnPosition(Integer.parseInt(columnName.substring(columnName.indexOf(YumPmsConstants.HASH)+1, columnName.indexOf(YumPmsConstants.DOLLER))));
				columnDetails.setIsEditable(columnName.substring(columnName.lastIndexOf(YumPmsConstants.EDITABLE)+10, columnName.lastIndexOf(YumPmsConstants.DISPLAY)).equalsIgnoreCase(YumPmsConstants.YES)? true : false);
				columnDetails.setIsDisplay(columnName.substring(columnName.lastIndexOf(YumPmsConstants.DISPLAY)+9).equalsIgnoreCase(YumPmsConstants.YES)? true : false);
				columnDetails.setColumnValue(columnValue); 
				checkCount(bonusSheet, count, columnValue);
				count++;
				columnList.add(columnDetails);
			}
			bonusSheet.setColumnList(columnList); 
			bonusSheetList.add(bonusSheet);
		}
		return bonusSheetList;
	}

	@Override
	public YearWisePayRangeData getYearWisePayRangeData(Integer apprId) {
		
		return getPayRangeDataForGrades(getApprId(apprId), getGradeIdValueMap());
	}

	@Override
	public YearWisePayRangeData saveYearWisePayRangeData(YearWisePayRangeData userPayRangeData) throws YumPMSDataSaveException {
		
		String logonUser = userPayRangeData.getLogonUser();
		Integer apprId = userPayRangeData.getApprId();
		List<GradeWisePayRange> gradeWisePayRangeList = userPayRangeData.getGradeWisePayRangeList();
		if(YumPmsUtils.isNullOrZero(apprId) || CollectionUtils.isEmpty(gradeWisePayRangeList)) {
			throw new CommonException(YumPmsConstants.INVALID_REQUEST_DATA);
		}
		Map<Integer, Map<Integer, CompYearWisePayRange>> mapByGradeAndPayRangeCategory = getPayRangeMapByGradeAndPayRangeCategory(apprId);
		ArrayList<CompYearWisePayRange> saveObjList = new ArrayList<>();
		for(GradeWisePayRange gradeWisePayRange : gradeWisePayRangeList) {
			Integer gradeId = gradeWisePayRange.getGradeId();
			List<PayRangeData> payRangeList = YumPmsUtils.getEmptyListIfNull(gradeWisePayRange.getPayRangeList());
			for(PayRangeData payRange : payRangeList) {
				Integer categoryId = payRange.getPayRangeCategoryId();
				Optional<CompYearWisePayRange> optional = YumPmsUtils.resolve(() -> mapByGradeAndPayRangeCategory.get(gradeId).get(categoryId));
				CompYearWisePayRange saveObj = null;
				if(optional.isPresent()) {
					saveObj = optional.get();
					saveObj.setUpdatedBy(logonUser);
					saveObj.setUpdatedDate(DateTimeUtils.now());
				} else {
					saveObj = new CompYearWisePayRange();
					saveObj.setCreatedBy(logonUser);
					saveObj.setCreatedDate(DateTimeUtils.now());
					saveObj.setAppraisalPeriod(new AppraisalPeriod(apprId));
					saveObj.setGradeMaster(new EmployeeGradeMaster(gradeId));
					saveObj.setPayRangeCategory(new CompPayRangeCategoryMaster(payRange.getPayRangeCategoryId()));
				}
				saveObj.setMinRange(payRange.getMinRange());
				saveObj.setMaxRange(payRange.getMaxRange());
				saveObj.setIncreasePercentage(payRange.getIncrPercentage());
				saveObjList.add(saveObj);
			}
		}
		saveObjList.trimToSize();
		compDao.saveOrUpdateAll(saveObjList);
		// Prepare the response
		Map<Integer, String> gradeMap = 
				gradeWisePayRangeList.stream()
				.collect(Collectors.toMap(GradeWisePayRange :: getGradeId, GradeWisePayRange :: getGrade));
		return getPayRangeDataForGrades(apprId, gradeMap);
	}
	
	@Override
	public List<CompYearWiseMultiplier> getYearWiseMultiplierData(Integer apprId) {

		apprId = getApprId(apprId);
		List<CompYearWiseMultiplier> multiplierList = compDao.getYearWiseMultiplierList(apprId);
		if(CollectionUtils.isNotEmpty(multiplierList)) {
			for(CompYearWiseMultiplier multiplier : multiplierList) {
				multiplier.setApprId(apprId);
			}
			return multiplierList;
		}
		// For the scenario where appraisal period id not present in comp_yearwise_multiplier table
		List<CompYearWiseMultiplier> result = new ArrayList<>();
		int appId = apprId.intValue();
		compDao.findAll(PA_RatingMaster.class).forEach(rating -> result.add(new CompYearWiseMultiplier(rating, appId)));
		return result;
	}
	
	@Override
	public List<CompYearWiseMultiplier> saveYearWiseMultiplierData(List<CompYearWiseMultiplier> multiplierData) throws YumPMSDataSaveException {
		
		if(CollectionUtils.isEmpty(multiplierData)) {
			return Collections.emptyList();
		}
		Integer apprId = multiplierData.get(0).getApprId();
		if(!YumPmsUtils.isGreaterThanZero(apprId)) {
			throw new CommonException("Can't proceed with the apprId: " + apprId + " value");
		}
		Map<Integer, CompYearWiseMultiplier> savedMultiplierMap = getYearWiseMultiplierMapById(apprId);
		List<CompYearWiseMultiplier> savedMultiplierList = new ArrayList<>(multiplierData.size());
		for(CompYearWiseMultiplier multiplier : multiplierData) {
			CompYearWiseMultiplier data = savedMultiplierMap.get(multiplier.getYearWiseMultiplierId());
			if(data == null) {
				data = multiplier;
				data.setCreatedDate(DateTimeUtils.now());
				data.setCreatedBy(multiplier.getUpdatedBy());
				data.setUpdatedBy(null);
			} else {
				data.setMultiplier(multiplier.getMultiplier());
				data.setUpdatedDate(DateTimeUtils.now());
				data.setUpdatedBy(multiplier.getUpdatedBy());
			}
			// As we are ignoring appraisalPeriod property using Jackson API; so we have to add it manually
			data.setAppraisalPeriod(new AppraisalPeriod(apprId));
			savedMultiplierList.add(data);
		}
		List<CompYearWiseMultiplier> result = compDao.saveOrUpdateAll(savedMultiplierList);
		for(CompYearWiseMultiplier multiplier : result) {
			multiplier.setApprId(apprId); // Again add this apprId to the response
		}
		return result;
	}
	
	@Override
	public LookupUiConfig getGradeAndYearWiseLookupData(Integer apprId) {
		
		return getLookupUiConfigForGrades(getApprId(apprId), getGradeIdValueMap());
	}

	@Override
	public LookupUiConfig saveGradeAndYearWiseLookupData(LookupUiConfig lookupUiConfig) throws YumPMSDataSaveException {
		
		Integer apprId = lookupUiConfig.getApprId();
		String logonUser = lookupUiConfig.getLogonUser();
		List<GradeLookupData> lookupDataList = lookupUiConfig.getLookupDataList();
		if(YumPmsUtils.isNullOrZero(apprId) || CollectionUtils.isEmpty(lookupDataList)) {
			throw new CommonException(YumPmsConstants.INVALID_REQUEST_DATA);
		}
		Map<Integer, Map<Integer, CompLevelYearWiseLookup>> lookupDataGroupByGradeIdHeadId = getSalaryHeadIdAndValueGroupByGradeId(apprId);
		List<CompLevelYearWiseLookup> saveObjList = new ArrayList<>();
		for(GradeLookupData lookupData : lookupDataList) {
			Integer gradeId = lookupData.getGradeId();
			List<SalaryHeadData> salaryHeads = lookupData.getSalaryHead();
			for(SalaryHeadData salaryHead : salaryHeads) {
				LOGGER.debug("Is updatable Record : {} With Lookup ID : {}", 
						YumPmsUtils.isGreaterThanZero(salaryHead.getLookupId()), salaryHead.getLookupId());
				Integer headId = salaryHead.getHeadId();
				Optional<CompLevelYearWiseLookup> optional = YumPmsUtils.resolve(() -> lookupDataGroupByGradeIdHeadId.get(gradeId).get(headId));
				CompLevelYearWiseLookup saveObj = null;
				if(optional.isPresent()) {
					saveObj = optional.get();
					saveObj.setUpdatedBy(logonUser);
					saveObj.setUpdatedDate(DateTimeUtils.now());
				} else {
					saveObj = new CompLevelYearWiseLookup();
					saveObj.setCreatedBy(logonUser);
					saveObj.setCreatedDate(DateTimeUtils.now());
				}
				saveObj.setAppraisalPeriod(new AppraisalPeriod(apprId));
				saveObj.setGradeMaster(new EmployeeGradeMaster(gradeId));
				saveObj.setHeadValue(salaryHead.getHeadValue());
				saveObj.setSalaryHeads(new CompLookupSalaryHeads(headId));
				saveObjList.add(saveObj);
			}
		}
		compDao.saveOrUpdateAll(saveObjList);
		// Prepare the response
		Map<Integer, String> gradeMap = 
				lookupDataList.stream().collect(Collectors.toMap(GradeLookupData :: getGradeId, GradeLookupData :: getGradeDesc));
		return getLookupUiConfigForGrades(apprId, gradeMap);
	}
	
	@Override
	public Map<String, Object> getRevisedSalaryEffectiveDuration(Integer apprId) {
		
		Optional<CompRevisedSalaryEffectiveDuration> optional = compDao.findOneByApprId(CompRevisedSalaryEffectiveDuration.class, getApprId(apprId));
		return prepareResultMap(optional.orElse(new CompRevisedSalaryEffectiveDuration(null, apprId)));
	}
	
	@Override
	public Map<String, Object> saveRevisedSalaryEffectiveDuration(Map<String, Object> requestMap) throws YumPMSDataSaveException {
		
		int id = YumPmsUtils.castToInt(requestMap.get(YumPmsConstants.ID));
		int apprId = YumPmsUtils.castToInt(requestMap.get(YumPmsConstants.APPR_ID));
		if(apprId <= 0) {
			throw new CommonException("Invalid Appraisal Period Id");
		}
		long dateLong = (long)requestMap.get(YumPmsConstants.SALARY_DURATION);
		String logonUser = YumPmsUtils.toString(requestMap.get(YumPmsConstants.LOGIN_ID));
		LOGGER.info("apprId = {}, dateLong = {}, logonUser = {}", apprId, dateLong, logonUser);
		CompRevisedSalaryEffectiveDuration saveEffectiveDuration = null;
		if(id > 0) {
			Optional<CompRevisedSalaryEffectiveDuration> optional = compDao.findById(CompRevisedSalaryEffectiveDuration.class, id);
			if(optional.isPresent()) {
				saveEffectiveDuration = optional.get();
				saveEffectiveDuration.setSalaryDuration(DateTimeUtils.toDate(dateLong));
				saveEffectiveDuration.setUpdatedBy(logonUser);
				saveEffectiveDuration.setUpdatedDate(DateTimeUtils.now());
			}
		}
		if(saveEffectiveDuration == null) {
			saveEffectiveDuration = new CompRevisedSalaryEffectiveDuration(apprId, DateTimeUtils.toDate(dateLong));
			saveEffectiveDuration.setCreatedBy(saveEffectiveDuration.getCreatedBy());
			saveEffectiveDuration.setCreatedDate(DateTimeUtils.now());
		}
		return prepareResultMap(compDao.saveOrUpdate(saveEffectiveDuration).orElse(new CompRevisedSalaryEffectiveDuration(null, apprId)));
	}
	
	@Override
	public DesgWiseVpay getDesgWiseVpayPercentage(Integer apprId) {
		
		apprId = getApprId(apprId);
		List<VpayData> vpayDataList = 
				compDao.findAllByApprId(CompDesignationWiseVpayPercentage.class, apprId)
				.stream()
				.map(VpayData :: new)
				.collect(Collectors.toList());
		Map<Integer, String>  desgMap = compDao.getMasterTableMap(EmployeeDesignationMaster.class, YumPmsConstants.DESIGNATION);
		return new DesgWiseVpay(apprId, vpayDataList, desgMap);
	}
	
	@Override
	public DesgWiseVpay saveDesgWiseVpayPercentage(DesgWiseVpay desgWiseVpay) throws YumPMSDataSaveException {

		Integer apprId = desgWiseVpay.getApprId();
		String logonUser = desgWiseVpay.getLogonUser();
		List<VpayData> vpayDataList = desgWiseVpay.getVpayDataList();
		if(YumPmsUtils.isNullOrZero(apprId) || CollectionUtils.isEmpty(vpayDataList)) {
			throw new CommonException(YumPmsConstants.INVALID_REQUEST_DATA);
		}
		Map<Integer, CompDesignationWiseVpayPercentage> vpayPercentageMap = 
				compDao.findAllByApprId(CompDesignationWiseVpayPercentage.class, apprId)
				.stream()
				.collect(Collectors.toMap(CompDesignationWiseVpayPercentage :: getId, Function.identity()));
		List<CompDesignationWiseVpayPercentage> saveObjList = new ArrayList<>(vpayDataList.size());
		for(VpayData vpayData : vpayDataList) {
			Optional<CompDesignationWiseVpayPercentage> optional = YumPmsUtils.resolve(() -> vpayPercentageMap.get(vpayData.getVpayPercentId()));
			CompDesignationWiseVpayPercentage saveObj = null;
			if(optional.isPresent()) {
				saveObj = optional.get();
				saveObj.setvPayValue(vpayData.getVpayPercentValue());
				saveObj.setDesgMaster(new EmployeeDesignationMaster(vpayData.getDesignationId()));
				saveObj.setUpdatedBy(logonUser);
				saveObj.setUpdatedDate(DateTimeUtils.now());
			} else {
				saveObj = new CompDesignationWiseVpayPercentage(apprId, vpayData.getVpayPercentValue(), vpayData.getDesignationId());
				saveObj.setCreatedBy(logonUser);
				saveObj.setCreatedDate(DateTimeUtils.now());
			}
			saveObjList.add(saveObj);
		}
		vpayDataList = 
				compDao.saveOrUpdateAll(saveObjList).stream()
				.map(VpayData :: new)
				.collect(Collectors.toList());
		return new DesgWiseVpay(apprId, vpayDataList, Collections.emptyMap());
	}
	
	@Override
	public GeneralConfigData getGeneralConfiguration(Integer apprId) {
		
		apprId = getApprId(apprId);
		List<ConfigData> configList = 
				compDao.findAllByApprId(CompGeneralConfiguration.class, apprId).stream()
				.map(ConfigData :: new)
				.sorted(Comparator.comparingInt(ConfigData :: getDisplayOrder))
				.collect(Collectors.toList());
		return new GeneralConfigData(apprId, configList);
	}
	
	@Override
	public GeneralConfigData saveGeneralConfiguration(GeneralConfigData generalConfigData) throws YumPMSDataSaveException {
		
		Integer apprId = generalConfigData.getApprId();
		String logonUser = generalConfigData.getLogonUser();
		List<ConfigData> configList = generalConfigData.getConfigList();
		if(YumPmsUtils.isNullOrZero(apprId) || CollectionUtils.isEmpty(configList)) {
			throw new CommonException(YumPmsConstants.INVALID_REQUEST_DATA);
		}
		Map<Integer, CompGeneralConfiguration> map = 
				compDao.findAllByApprId(CompGeneralConfiguration.class, apprId).stream()
				.collect(Collectors.toMap(CompGeneralConfiguration :: getId, Function.identity()));
		List<CompGeneralConfiguration> saveObjList = new ArrayList<>(configList.size());
		for(ConfigData configData : configList) {
			Optional<CompGeneralConfiguration> optional = YumPmsUtils.resolve(() -> map.get(configData.getId()));
			if(optional.isPresent()) {
				CompGeneralConfiguration saveObj = optional.get();
				saveObj.setHeadValue(configData.getHeadValue());
				saveObj.setHeadName(configData.getHeadName());
				saveObj.setUpdatedBy(logonUser);
				saveObj.setUpdatedDate(DateTimeUtils.now());
				saveObjList.add(saveObj);
			}
		}
		List<ConfigData> responseConfigList = 
				compDao.saveOrUpdateAll(saveObjList)
				.stream()
				.map(ConfigData :: new)
				.collect(Collectors.toList());
		 return new GeneralConfigData(apprId, responseConfigList);
	}
	
	@Override
	public CompensationLetter getCompensationLetter(Integer empId, Integer apprId, String salaryType, boolean isPdf) {

		apprId = getApprId(apprId);
		String currentApprYear = apprService.getAppraisalPeriodbyId(apprId).getPeriodDesc();
		String nextApprYear = String.valueOf(NumberUtils.toInt(currentApprYear) + 1);
		List<Map<String, String>> result = compDao.executeDynamicProcedure(YumPmsConstants.SP_COMP_LETTER, empId, currentApprYear, nextApprYear, salaryType);
		CompensationLetter letter = new CompensationLetter();
		letter.setEmpId(empId);
		letter.setApprId(apprId);
		letter.setApprYearDesc(currentApprYear);
		if(CollectionUtils.isEmpty(result)) {
			return letter;
		}
		Map<String, String> resultMap = result.get(0);
		LOGGER.info("Sp Result = {}", resultMap);
		StringBuilder letterBody = new StringBuilder();
		String functionLtSignature = null;
		for(Entry<String, String> entry : resultMap.entrySet()) {
			
			String columnName = entry.getKey();
			if(StringUtils.containsIgnoreCase(columnName, YumPmsConstants.COLUMN_EMP_NAME)) {
				letter.setEmpNme(entry.getValue());
			} else if(StringUtils.containsIgnoreCase(columnName, YumPmsConstants.COLUMN_LETTER_BODY)) {
				letterBody.append(entry.getValue());
			} else if(StringUtils.containsIgnoreCase(columnName, "function_lt_signature_image")) {
				functionLtSignature = entry.getValue();
			}
		}
		letter.setHtmlReportContent(replaceSignatureImage(letterBody.toString(), isPdf, functionLtSignature));
		return letter;
	}
	
	private String replaceSignatureImage(String letterBody, boolean isPdf, String functionLtSignature) {

		if(StringUtils.isNotBlank(functionLtSignature)) {
			String imgStart = "<img width=\"150px\" height=\"auto\" src=\"";
			String imgEnd = "\"/>";
			String signatureImage = isPdf ? 
					imgStart + ResourceUtils.getYumRootPath() + functionLtSignature + imgEnd : imgStart + functionLtSignature + imgEnd;
			if(letterBody.contains(YumPmsConstants.FUNCTION_LT_SIGNATURE)) {
				return letterBody.replace(YumPmsConstants.FUNCTION_LT_SIGNATURE, signatureImage);
			}
		} else {
			return letterBody.replace(YumPmsConstants.FUNCTION_LT_SIGNATURE, StringUtils.EMPTY);
		}
		return letterBody;
	}
	
	@Override
	public List<SalaryHeadData> getSalaryDetails(int empId, int appraisalId) throws YumPMSDataSaveException {
		List<SalaryHeadData> listOfSalaryHeadDataWithValue = new ArrayList<>();
		List<CompSalaryFileds> salaryFieldList = compDao.findAllByApprId(CompSalaryFileds.class, appraisalId);
		List<CompYearWiseSalary> empYearWiseSalList = compDao.getSalaryDetails(empId, appraisalId);
		if(CollectionUtils.isNotEmpty(empYearWiseSalList)) {
			empYearWiseSalList = empYearWiseSalList.stream().filter(yearWiseSal -> yearWiseSal.getEmployee() != null && yearWiseSal.getEmployee().getEmpId() == empId).collect(Collectors.toList());	
		}else {
			empYearWiseSalList = Collections.emptyList();
		}
		for(CompSalaryFileds field : salaryFieldList) {
			SalaryHeadData head = new SalaryHeadData();
			head.setHeadId(field.getSalaryFieldId());
			head.setHeadName(field.getHeadName());
			head.setHeadDisplayName(field.getNewEmployeeSalarySheetDisplayName());
			head.setDisplayOrder(field.getDisplayOrder());
			head.setAppraisalPeriodId(appraisalId);
			head.setEmpId(empId); 
			head.setIsDisplay(field.getIsDisplayed());
			head.setIsMadatory(field.getIsMandatory());
			
			Optional<CompYearWiseSalary> optionlSal = empYearWiseSalList.stream().filter(sal -> sal.getSalaryFileds().getSalaryFieldId().equals(field.getSalaryFieldId())).findFirst();
			if(optionlSal.isPresent()) {
				CompYearWiseSalary salary = optionlSal.get();
				head.setHeadAmount(null != salary.getAmount()? salary.getAmount() : 0.0); 
				head.setEmployeeYearWiseId(salary.getYearWiseSalaryId()); 
			}
			listOfSalaryHeadDataWithValue.add(head); 
		}
		return listOfSalaryHeadDataWithValue;
	}

	@Override
	public List<SalaryHeadData> insertOrUpdateSalray(List<SalaryHeadData> uiSalaryObjectList) throws YumPMSDataSaveException {
		List<CompYearWiseSalary> empYearWiseSalList = new ArrayList<>();
		for(SalaryHeadData uiSalObject : uiSalaryObjectList) {
			CompYearWiseSalary dbSalObject = new CompYearWiseSalary();
			dbSalObject.setAppraisalPeriod(new AppraisalPeriod(uiSalObject.getAppraisalPeriodId())); 
			dbSalObject.setEmployee(new EmployeeNew(uiSalObject.getEmpId()));
			dbSalObject.setSalaryFileds(new CompSalaryFileds(uiSalObject.getHeadId()));
			dbSalObject.setAmount(uiSalObject.getHeadAmount());
			if(StringUtils.isBlank(dbSalObject.getStatus())) {
				dbSalObject.setStatus("RevisedSalary");
			}
			if(null != uiSalObject.getEmployeeYearWiseId() && uiSalObject.getEmployeeYearWiseId()>0) {
				dbSalObject.setYearWiseSalaryId(uiSalObject.getEmployeeYearWiseId());
				dbSalObject.setUpdatedBy(uiSalObject.getLoginId().toString());
			}else {
				dbSalObject.setCreatedBy(uiSalObject.getLoginId().toString());
			}
			empYearWiseSalList.add(dbSalObject);
		}
		compDao.saveOrUpdateAll(empYearWiseSalList);
		return getSalaryDetails(uiSalaryObjectList.get(0).getEmpId(), uiSalaryObjectList.get(0).getAppraisalPeriodId());
	}
	
	/* This is used to get all employee details against an appraisal period id
	 */
	@Override
	public CompLetterUiData getAllEmpsDetailsForCompLetter(Integer apprId, String salaryType) {
		
		apprId = getApprId(apprId);
		List<Map<String, String>> result = compDao.executeDynamicProcedure(YumPmsConstants.SP_COMP_DISPLAY_LETTER, apprId, salaryType);
		if(CollectionUtils.isEmpty(result)) {
			return new CompLetterUiData(apprId, Collections.emptyList());
		}
		List<List<Properties>> empList = 
				result.stream()
				.map(map -> map.entrySet().stream().map(this :: createProperties).collect(Collectors.toList()))
				.collect(Collectors.toList());
		return new CompLetterUiData(apprId, empList);
	}
	

	/**
	 * This method will return all Compensation letter template
	 */
	@Override
	public LetterTemplateConfig getCompLetterTemplate(Integer apprId) {
		
		apprId = getApprId(apprId);
		return new LetterTemplateConfig(apprId, compDao.getLetterPlaceholders(), getCompLetterTemplateByLetterIds(apprId, null));
	}
	
	@Override
	public LetterTemplateConfig saveCompLetterTemplate(LetterTemplateConfig letterConfig) throws YumPMSDataSaveException {
		
		List<CompLetterTemplate> compLetterTemplate = letterConfig.getLetterTemplateList();
		Integer apprId = letterConfig.getApprId();
		if(CollectionUtils.isEmpty(compLetterTemplate) || YumPmsUtils.isLessThanZero(apprId)) {
			throw new CommonException(YumPmsConstants.INVALID_REQUEST_DATA);
		}
		Map<Integer, CompLetterTemplate> letterTemplateMap = 
				getCompLetterTemplateByLetterIds(apprId, null)
				.stream()
				.collect(Collectors.toMap(CompLetterTemplate :: getLetterId, Function.identity()));
		
		List<CompLetterTemplate> saveLetterTemplateList = new ArrayList<>(compLetterTemplate.size());
		Set<Integer> letterIds = new HashSet<>(compLetterTemplate.size());
		for(CompLetterTemplate letterTemplate : compLetterTemplate) {
			letterIds.add(letterTemplate.getLetterId());
			CompLetterTemplate saveLetterTemplate =  letterTemplateMap.get(letterTemplate.getLetterId());
			saveLetterTemplate.setAppraisalPeriod(new AppraisalPeriod(apprId));
			saveLetterTemplate.setLetterName(letterTemplate.getLetterName());
			saveLetterTemplate.setLetterDescription(letterTemplate.getLetterDescription());
			saveLetterTemplate.setDefaultLetter(letterTemplate.getDefaultLetter());
			saveLetterTemplate.setCustomLetterBody(letterTemplate.getCustomLetterBody());
			saveLetterTemplate.setUpdatedBy(letterConfig.getLogonUser());
			saveLetterTemplate.setUpdatedDate(DateTimeUtils.now());
			saveLetterTemplateList.add(saveLetterTemplate);
		}
		compDao.saveOrUpdateAll(saveLetterTemplateList);
		
		return new LetterTemplateConfig(apprId, compDao.getLetterPlaceholders(), getCompLetterTemplateByLetterIds(apprId, letterIds));
	}
	
	@Override
	public Map<String, byte[]> downloadOrAcceptCompLetter(Integer empId, Integer apprId, String salaryType, boolean isAccepted) {
		
		CompensationLetter letter = getCompensationLetter(empId, apprId, salaryType, true);
		byte[] content = YumPmsUtils.getCompensationLetterContent(letter);
		String fileName = YumPmsUtils.getCompLetterFileName(letter.getEmpNme(), letter.getApprYearDesc(), salaryType);
		Map<String, byte[]> map = new HashMap<>(1);
		map.put(fileName, content);
		if(BooleanUtils.toBoolean(isAccepted)) {
			compDao.executeProcedure(YumPmsConstants.SP_COMP_LETTER_STATUS, empId, apprId, salaryType, null, 1);
			//Send email with attachment
			String attachmentPath = null;
			try {
				attachmentPath = ResourceUtils.createTempFile(fileName, content);
				List<Object[]> result = compDao.executeProcedure(YumPmsConstants.COMP_LETTER_ACCEPTANCE_EMAIL, empId, apprId, salaryType);
				if(CollectionUtils.isNotEmpty(result) && StringUtils.isNotBlank(attachmentPath)) {
					Object[] tuples = result.get(0);
					mailSender.sendMail(YumPmsUtils.toStringorNull(tuples[0]), 
							YumPmsUtils.toStringorNull(tuples[1]), 
							YumPmsUtils.toStringorNull(tuples[2]), 
							YumPmsUtils.toStringorNull(tuples[3]), 
							attachmentPath);
				}
			} catch(Exception e) {
				LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			} finally {
				if(StringUtils.isNotBlank(attachmentPath)) {
					ResourceUtils.delete(attachmentPath);
				}
			}
		}
		return map;
	}
	
	@Override
	public List<CompLetterStatus> getCompLettersForDownload(Integer empId, Integer apprId) {
		return compDao.getCompLettersForDownload(empId, apprId);
	}
	
	@Override
	public CompBonusConfiguration getBonusConfig(Integer apprId) {
		
		return compDao.findOneByApprId(CompBonusConfiguration.class, apprId).orElse(new CompBonusConfiguration());
	}
	
	@Override
	public CompBonusConfiguration saveBonusConfig(CompBonusConfiguration request) throws YumPMSDataSaveException {
		
		if(request == null || YumPmsUtils.isLessThanZero(request.getApprId())) {
			throw new CommonException("Invalid apprId request");
		}
		CompBonusConfiguration config = 
				compDao.findOneByApprId(CompBonusConfiguration.class, request.getApprId()).orElse(new CompBonusConfiguration());
		if(YumPmsUtils.isGreaterThanZero(config.getBonusConfigId())) {
			config.setBonusConfigId(config.getBonusConfigId());
		} else {
			config.setCreatedBy(request.getLogonUser());
			config.setCreatedDate(DateTimeUtils.now());
		}
		config.setAppraisalPeriod(new AppraisalPeriod(request.getApprId()));
		config.setBonusStartDate(request.getBonusStartDate());
		config.setBonusEndDate(request.getBonusEndDate());
		config.setUpdatedBy(request.getLogonUser());
		config.setUpdatedDate(DateTimeUtils.now());
		return compDao.saveOrUpdate(config).orElse(new CompBonusConfiguration());
	}
	
	/*#################### All helper methods ###########################*/
	
	private List<CompLetterTemplate> getCompLetterTemplateByLetterIds(Integer apprId, Set<Integer> letterIds) {
		
		List<CompLetterTemplate> result = compDao.findAllByApprId(CompLetterTemplate.class, apprId);
		if(CollectionUtils.isEmpty(letterIds)) {
			return result;
		}
		return result.stream().filter(letterTemplate -> letterIds.contains(letterTemplate.getLetterId())).collect(Collectors.toList());
	}
	
	private Properties createProperties(Map.Entry<String, String> entry) {
		
		Properties prop = new Properties();
		prop.setProperty(YumPmsConstants.COLUMN_NAME, entry.getKey());
		prop.setProperty(YumPmsConstants.COLUMN_VALUE, entry.getValue());
		return prop;
	}
	
	private Integer getApprId(Integer apprId) {
		
		LOGGER.info("apprId = {}", apprId);
		if(YumPmsUtils.isNullOrZero(apprId)) {
			apprId = apprService.getDefaultAppraisalService().getAppraisalPeriodId();
			LOGGER.info("Default apprId = {}", apprId);
		}
		return apprId;
	}

	private Map<String, Object> prepareResultMap(CompRevisedSalaryEffectiveDuration effectiveDuration) {

		Map<String, Object> resultMap = new HashMap<>(5);
		resultMap.put(YumPmsConstants.ID, effectiveDuration.getId());
		resultMap.put(YumPmsConstants.SALARY_DURATION, effectiveDuration.getSalaryDuration());
		resultMap.put(YumPmsConstants.LOGIN_ID, StringUtils.EMPTY);
		Optional<Integer> optional = YumPmsUtils.resolve(() -> effectiveDuration.getAppraisalPeriod().getAppraisalPeriodId());
		resultMap.put(YumPmsConstants.APPR_ID, optional.isPresent() ? optional.get() : 0);
		return resultMap;
	}
	
	private YearWisePayRangeData getPayRangeDataForGrades(Integer apprId, Map<Integer, String> gradeIdValueMap) {
		
		Map<Integer, Map<Integer, CompYearWisePayRange>> gradeWiseMap = getPayRangeMapByGradeAndPayRangeCategory(apprId);
		List<GradeWisePayRange> gradeWisePayRangeList = new ArrayList<>(gradeIdValueMap.size());
		List<CompPayRangeCategoryMaster>  payRangeCategoryList = compDao.getPayRangeCategorysByApprId(apprId);
		for(Map.Entry<Integer, String> entry : gradeIdValueMap.entrySet()) {
			List<PayRangeData> payRangeDataList = new ArrayList<>(payRangeCategoryList.size());
			Integer gradeId = entry.getKey();
			Map<Integer, CompYearWisePayRange> payRangeMapByRangeCategory = gradeWiseMap.get(gradeId);
			for(CompPayRangeCategoryMaster category : payRangeCategoryList) {
				Optional<CompYearWisePayRange> optional = YumPmsUtils.resolve(() -> payRangeMapByRangeCategory.get(category.getPayRangeCategoryId()));
				PayRangeData payRange = optional.isPresent() ? new PayRangeData(optional.get()) : new PayRangeData();
				payRange.setPayRangeMasterData(category);
				payRangeDataList.add(payRange);
			}
			gradeWisePayRangeList.add(new GradeWisePayRange(gradeId, entry.getValue(), payRangeDataList));
		}
		return new YearWisePayRangeData(apprId, gradeWisePayRangeList);
	}
	
	private LookupUiConfig getLookupUiConfigForGrades(Integer apprId, Map<Integer, String> gradeIdValueMap) {
		
		Map<Integer, Map<Integer, CompLevelYearWiseLookup>> lookupGradeIdSalaryHeadMap = getSalaryHeadIdAndValueGroupByGradeId(apprId);
		List<CompLookupSalaryHeads> salaryHeadList = compDao.getSalaryHeadsByApprId(apprId);
		List<GradeLookupData> lookupDataList = new ArrayList<>(gradeIdValueMap.size());
		for (Map.Entry<Integer, String> entry : gradeIdValueMap.entrySet()) {
			List<SalaryHeadData> salaryHeads = new ArrayList<>(salaryHeadList.size());
			Map<Integer, CompLevelYearWiseLookup> salaryHeadIdValueMap = lookupGradeIdSalaryHeadMap.get(entry.getKey());
			for(CompLookupSalaryHeads salaryHead : salaryHeadList) {
				SalaryHeadData data = new SalaryHeadData(salaryHead);
				Optional<CompLevelYearWiseLookup> optional = YumPmsUtils.resolve(() -> salaryHeadIdValueMap.get(salaryHead.getHeadId()));
				if(optional.isPresent()) {
					data.setHeadValue(optional.get().getHeadValue());
					data.setLookupId(optional.get().getLookupId());
				}
				salaryHeads.add(data);
			}
			lookupDataList.add(new GradeLookupData(entry.getKey(), entry.getValue(), salaryHeads));
		}
		return new LookupUiConfig(apprId, lookupDataList);
	}
	
	private Map<Integer, String> getGradeIdValueMap() {

		Map<Integer, String> map = 
				compDao.findAll(EmployeeGradeMaster.class)
				.stream().collect(Collectors.toMap(EmployeeGradeMaster :: getGradeId, EmployeeGradeMaster :: getGradeDesc));
		return YumPmsUtils.getEmptyMapIfNull(map);	
	}
	
	private Map<Integer, Map<Integer, CompLevelYearWiseLookup>> getSalaryHeadIdAndValueGroupByGradeId(Integer apprId) {
		
		Map<Integer, List<CompLevelYearWiseLookup>> lookupMapByGradeId = getGradeWiseLookupMap(apprId);
		Map<Integer, Map<Integer, CompLevelYearWiseLookup>> lookupGradeIdSalaryHeadMap = new HashMap<>(lookupMapByGradeId.size());
		for(Map.Entry<Integer, List<CompLevelYearWiseLookup>> entry : lookupMapByGradeId.entrySet()) {
			Map<Integer, CompLevelYearWiseLookup> salaryHeadIdValueMap = Collections.emptyMap();
			if(CollectionUtils.isNotEmpty(entry.getValue())) {
				salaryHeadIdValueMap = entry.getValue().stream()
						.collect(Collectors.toMap(lookup -> lookup.getSalaryHeads().getHeadId(), Function.identity()));
			}
			lookupGradeIdSalaryHeadMap.put(entry.getKey(), salaryHeadIdValueMap);
		}
		return lookupGradeIdSalaryHeadMap;
	}
	
	private Map<Integer, List<CompLevelYearWiseLookup>> getGradeWiseLookupMap(Integer apprId) {
		
		List<CompLevelYearWiseLookup> allYearWiseLookupData = compDao.findAllByApprId(CompLevelYearWiseLookup.class, apprId);
		Map<Integer, List<CompLevelYearWiseLookup>> lookupMapByGradeId = 
				allYearWiseLookupData.stream().collect(
						Collectors.groupingBy(lookup -> lookup.getGradeMaster().getGradeId(), 
								YumPmsUtils.toSortedList(Comparator.comparing(lookup -> lookup.getSalaryHeads().getDisplayOrder()))));
		return YumPmsUtils.getEmptyMapIfNull(lookupMapByGradeId);
	}
	
	/**
	 * Get the Map of gradeId and another map which again contains pay range category id and pay range object
	 * outer map key : grade Id
	 * inner map key : pay range category id
	 * @param apprId
	 * @return Map<gradeId, Map<payRangeCategoryId, CompYearWisePayRange>>
	 */
	private Map<Integer, Map<Integer, CompYearWisePayRange>> getPayRangeMapByGradeAndPayRangeCategory(Integer apprId) {
		
		Map<Integer, List<CompYearWisePayRange>> gradeWiseMap = 
				compDao.getYearWisePayRangeListByApprId(apprId).stream()
				.collect(Collectors.groupingBy(data -> data.getGradeMaster().getGradeId(),
						YumPmsUtils.toSortedList(Comparator.comparing(data -> data.getPayRangeCategory().getDisplayOrder()))));
		
		if(MapUtils.isEmpty(gradeWiseMap)) {
			return Collections.emptyMap();
		}
		Map<Integer, Map<Integer, CompYearWisePayRange>> payRangeMapByGradeAndRangeCategoryId = new LinkedHashMap<>();
		for(Map.Entry<Integer, List<CompYearWisePayRange>> entry : gradeWiseMap.entrySet()) {
			Map<Integer, CompYearWisePayRange> payRangeMapByCategory = Collections.emptyMap();
			if(CollectionUtils.isNotEmpty(entry.getValue())) {
				payRangeMapByCategory = entry.getValue().stream()
						.collect(Collectors.toMap(thisObj -> thisObj.getPayRangeCategory().getPayRangeCategoryId(), Function.identity()));
			}
			payRangeMapByGradeAndRangeCategoryId.put(entry.getKey(), payRangeMapByCategory);
		}
		return payRangeMapByGradeAndRangeCategoryId;
	}
	
	// We will remove this method later
	@SuppressWarnings("unused")
	private Map<Integer, CompYearWisePayRange> getYearWisePayRangeMapByPayRangeId(Integer apprId) {
		
		List<CompYearWisePayRange> yearWisePayRangeList = compDao.getYearWisePayRangeListByApprId(apprId);
		Map<Integer, CompYearWisePayRange> map = 
				yearWisePayRangeList.stream()
				.collect(Collectors.toMap(CompYearWisePayRange :: getYearWisePayRangeId, Function.identity()));
		
		if(map == null || map.isEmpty()) {
			return Collections.emptyMap();
		}
		return map;
	}
	
	private Map<Integer, CompYearWiseMultiplier> getYearWiseMultiplierMapById(Integer apprId) {
		
		Map<Integer, CompYearWiseMultiplier> map = 
				compDao.getYearWiseMultiplierList(apprId)
				.stream()
				.collect(Collectors.toMap(CompYearWiseMultiplier :: getYearWiseMultiplierId, Function.identity()));
		return MapUtils.isEmpty(map) ? Collections.emptyMap() : map;
	}

	/* (non-Javadoc)
	 * @see com.yum.comp.service.CompensationService#getSpecialSalary(java.lang.Integer, java.lang.String, java.lang.String)
	 */
	@Override
	public List<CompensationUiConfig> getSpecialSalary(Integer appraisalPeriord, Integer nextAppraisalPeriord, Integer empId, String purpose, String dataList, String action)throws SQLException {
		List<CompensationUiConfig> reviseSalaryList = new ArrayList<>();
		CompensationUiConfig reviseSal = new CompensationUiConfig();
		EmployeeNew employee = employeeService.getEmployeeById(empId);
		reviseSal.setEmpId(empId);
		reviseSal.setEmpName(employee.getEmpName());
		reviseSal.setCurYear(appraisalPeriord.toString());
		reviseSal.setNextYear(nextAppraisalPeriord.toString()); 
		List<Map<String, String>> reviseSalData = compDao.executeDynamicProcedure(
				String.format(YumPmsConstants.SP_SPECIAL_SALARY_HANDEL, appraisalPeriord, nextAppraisalPeriord), 
				empId,appraisalPeriord,nextAppraisalPeriord, dataList, action, purpose);
		for(Map<String, String> reviseSalary : reviseSalData) {
			List<DisplayDomain> columnList = new ArrayList<>(reviseSalary.size());
			
			for(Map.Entry<String, String> entry : reviseSalary.entrySet()) {
				String columnName = entry.getKey();
				String columnValue = entry.getValue();
				DisplayDomain columnDetails = new DisplayDomain();

				columnDetails.setColumnName(columnName.substring(0, columnName.indexOf(YumPmsConstants.HASH)));  
				columnDetails.setColumnPosition(NumberUtils.toInt(columnName.substring(columnName.indexOf(YumPmsConstants.HASH)+1)));
				columnDetails.setIsEditable(columnValue.substring(columnValue.lastIndexOf(YumPmsConstants.EDITABLE)+10, columnValue.lastIndexOf(YumPmsConstants.DISPLAY)).equalsIgnoreCase(YumPmsConstants.YES)? true : false);
				columnDetails.setIsDisplay(columnValue.substring(columnValue.lastIndexOf(YumPmsConstants.DISPLAY)+9).equalsIgnoreCase(YumPmsConstants.YES)? true : false);
				columnDetails.setColumnValue(columnValue.substring(0, columnValue.indexOf(YumPmsConstants.DOLLER))); 
				columnList.add(columnDetails);
			}
		reviseSal.getDisplayList().add(columnList);  
		}
		reviseSalaryList.add(reviseSal);
		return reviseSalaryList;
	}

	/* (non-Javadoc)
	 * @see configureNewAppraisalPeriod(Integer currentApprId, Integer nextApprId)
	 */
	@Override
	public String configureNewAppraisalPeriod(Integer currentApprId, Integer nextApprId) {
		
		Object result = compDao.returningSingleValueProcedure(String.format(YumPmsConstants.SP_NEW_YEAR_CONFIGURATION, currentApprId, nextApprId));
		return result != null ? result.toString() : StringUtils.EMPTY; 
	}
//------------------------------------------------------bulk submit--------------------------------------------------------------------
	/* (non-Javadoc) 
	 * @see com.yum.comp.service.CompensationService#saveEditReviesSalary_new(com.yum.comp.domain.CompensationUiConfig)
	 */
	@Override
	public CompUiRevisedSalary saveEditReviesSalaryNew(CompUiRevisedSalary revisedSalry) throws SQLException {
		CompUiRevisedSalary reviseSal = new CompUiRevisedSalary();
		
		Map<String, String> params = new HashMap<>();
		params.put(YumPmsConstants.SP_REVISED_SALARY_PARAM_EMP_ID, revisedSalry.getAction().equalsIgnoreCase(YumPmsConstants.REVISED_SAL)|| 
		  revisedSalry.getAction().equalsIgnoreCase(YumPmsConstants.SAVE)? StringUtils.EMPTY : YumPmsUtils.toStringorNull(revisedSalry.getRevisedSalrayList().get(0).getEmpId())); 
		params.put(YumPmsConstants.SP_PARAM_CUR_YEAR, revisedSalry.getCurYear());
		params.put(YumPmsConstants.SP_PARAM_NEXT_YEAR, revisedSalry.getNextYear());
		params.put(YumPmsConstants.ACTION, revisedSalry.getAction());
		
		if(revisedSalry.getAction().equalsIgnoreCase(YumPmsConstants.REVISED_SAL) || revisedSalry.getAction().equalsIgnoreCase(YumPmsConstants.SAVE)) {
			compDao.deleteParamiterOfRevisedSalary();
			compDao.saveParamiterOfRevisedSalary(revisedSalry.getRevisedSalrayList()); 
		}
		List<Map<String, String>> reviseSalData = compDao.revisedSalary(params);
		
		for(Map<String, String> reviseSalary : reviseSalData) {
			CompensationUiConfig responseRevisedSalry =  new CompensationUiConfig();
			reviseSal.setCurYear(revisedSalry.getCurYear());
			reviseSal.setNextYear(revisedSalry.getNextYear()); 
			List<DisplayDomain> columnList = new ArrayList<>(reviseSalary.size());
			Map<String, String> topProperty = new HashMap<>();
			int count = 1 ;
			for(Map.Entry<String, String> entry : reviseSalary.entrySet()) {
				String columnName = entry.getKey();
				String columnValue =  entry.getValue();
				DisplayDomain columnDetails = new DisplayDomain();
				columnDetails.setColumnName(columnName.substring(0, columnName.indexOf(YumPmsConstants.HASH)));  
				columnDetails.setColumnPosition(NumberUtils.toInt(columnName.substring(columnName.indexOf(YumPmsConstants.HASH)+1, columnName.indexOf(YumPmsConstants.DOLLER))));
				columnDetails.setIsEditable(columnName.substring(columnName.lastIndexOf(YumPmsConstants.EDITABLE)+10, columnName.lastIndexOf(YumPmsConstants.DISPLAY)).equalsIgnoreCase(YumPmsConstants.YES)? true : false);
				columnDetails.setIsDisplay(columnName.substring(columnName.lastIndexOf(YumPmsConstants.DISPLAY)+9, columnName.lastIndexOf(YumPmsConstants.TOP_PROPERTY)).equalsIgnoreCase(YumPmsConstants.YES)? true : false);
				columnDetails.setColumnValue(columnValue); 
				checkCount(responseRevisedSalry, count, columnValue);
				count++;
				checkRevisedSalaryCondition(revisedSalry, columnList,
						topProperty, columnName, columnValue, columnDetails);
			}
			responseRevisedSalry.setTopProperty(topProperty);
			responseRevisedSalry.setColumnList(columnList); 
			reviseSal.getRevisedSalrayList().add(responseRevisedSalry);
		}	
		return reviseSal;
	}

	private void checkCount(CompensationUiConfig responseRevisedSalry,
			int count, String columnValue) {
		if(count == 2) {
			responseRevisedSalry.setEmpId(NumberUtils.toInt(columnValue));
		}
	}

	private void checkRevisedSalaryCondition(CompUiRevisedSalary revisedSalry,
			List<DisplayDomain> columnList, Map<String, String> topProperty,
			String columnName, String columnValue, DisplayDomain columnDetails) {
		if(null != revisedSalry && null!=revisedSalry.getAction() && revisedSalry.getAction().equalsIgnoreCase(YumPmsConstants.VIEW)) {
			if(CollectionUtils.isNotEmpty(columnList) && (columnDetails.getColumnPosition() == columnList.get(columnList.size()-1).getColumnPosition())) {
				columnList.get(columnList.size()-1).setNextColumnValue(columnDetails.getColumnValue());
			}else if((columnName.substring(columnName.lastIndexOf(YumPmsConstants.TOP_PROPERTY)+11).equalsIgnoreCase(YumPmsConstants.YES))){
				topProperty.put(columnDetails.getColumnName(), columnValue);
			}else {
				columnList.add(columnDetails);
			}
		}else {
			columnList.add(columnDetails);
		}
	}
	
// -----------------------------------------------------------bulk email----------------------------------------------------------	

	/* (non-Javadoc)
	 * @see com.yum.comp.service.CompensationService#getCompensationLetterNew(java.lang.String, java.lang.Integer)
	 */
	@Override
	public List<CompensationLetter> getCompensationLetterNew(String empIds, Integer apprId, String salaryType) {
		List<CompensationLetter> letterList = new ArrayList<>();
		apprId = getApprId(apprId);
		String currentApprYear = apprService.getAppraisalPeriodbyId(apprId).getPeriodDesc();
		String nextApprYear = String.valueOf(NumberUtils.toInt(currentApprYear) + 1);	
		List<Map<String, String>> result = compDao.executeDynamicProcedure(YumPmsConstants.SP_COMP_LETTER, empIds, currentApprYear, nextApprYear, salaryType);				
		if(CollectionUtils.isEmpty(result)) {
			return Collections.emptyList();
		}
		for(Map<String, String> resultMap : result) {
			CompensationLetter letter = new CompensationLetter();
			letter.setApprId(apprId);
			letter.setApprYearDesc(currentApprYear);
			
			LOGGER.info("Sp Result = {}", resultMap);
			StringBuilder letterBody = new StringBuilder();
			String functionLtSignature = null;
			for(Entry<String, String> entry : resultMap.entrySet()) {			
				String columnName = entry.getKey();
				if(StringUtils.containsIgnoreCase(columnName, YumPmsConstants.COLUMN_EMP_NAME)) {
					letter.setEmpNme(entry.getValue());
				}else if(StringUtils.containsIgnoreCase(columnName, YumPmsConstants.EMPLOYEE_ID)){
					letter.setEmpId(Integer.parseInt(entry.getValue()));  
				}else if(StringUtils.containsIgnoreCase(columnName, YumPmsConstants.SUPERVISOR_ID)){
					letter.setSupervisorId(Integer.parseInt(entry.getValue()));  
				} else if(StringUtils.containsIgnoreCase(columnName, YumPmsConstants.COLUMN_LETTER_BODY)) {
					letterBody.append(entry.getValue());
				} else if(StringUtils.containsIgnoreCase(columnName, "function_lt_signature_image")) {
					functionLtSignature = entry.getValue();
				}
			}
			letter.setHtmlReportContent(replaceSignatureImage(letterBody.toString(), true, functionLtSignature));
			letterList.add(letter);
		}
		return letterList;
	}
	/* (non-Javadoc)
	 * @see com.yum.comp.service.CompensationService#sendEmailWithCompensationLetter(java.lang.String, java.lang.Integer, java.lang.String)
	 */
	@Override
	public void sendEmailWithCompensationLetter(String empIds, String supervisorIds, Integer apprId, Map<Integer, String> attachmentPath) {
		
		LOGGER.info("Comp Letter Email Service; empId = {}, apprId = {} and attachmentPath = {}", supervisorIds, apprId, attachmentPath); 
		apprId = getApprId(apprId);
		List<Object[]> result = compDao.executeProcedure(YumPmsConstants.SP_COMP_EMAIL_LETTER, apprId, supervisorIds, empIds);
		if(CollectionUtils.isNotEmpty(result) && ArrayUtils.getLength(result.get(0)) > 0) {
			for(Object[] tuples : result) {
				mailSender.sendMail(YumPmsUtils.toStringorNull(tuples[0]), 
						YumPmsUtils.toStringorNull(tuples[1]), 
						YumPmsUtils.toStringorNull(tuples[2]), 
						YumPmsUtils.toStringorNull(tuples[3]), 
						attachmentPath.get(YumPmsUtils.castToInt(tuples[4])));
				if(StringUtils.isNotBlank(attachmentPath.get(YumPmsUtils.castToInt(tuples[4])))) {
					ResourceUtils.delete(attachmentPath.get(YumPmsUtils.castToInt(tuples[4])));
				}
			}
		}
	}
	
	@Override
	public void updateCompLetterEmailStatus(String empIds, Integer apprId, String purpose) {
		
		paDao.updateCompLetterEmailStatus(empIds, apprId, purpose);
	}

	/* (non-Javadoc)
	 * @see com.yum.comp.service.CompensationService#updateCompLetterDownloadPermission(java.lang.String, java.lang.String)
	 */
	@Override
	public void updateCompLetterDownloadPermission(String empIds, Map<Integer, Boolean> compLetterDownloadPermissionMap, Integer apprId, String purpose) 
			throws YumPMSDataSaveException { 
		
		if(StringUtils.isEmpty(purpose)) {
			throw new CommonException("Letter Type is mandatory");
		}
		String permittedEmpId = compLetterDownloadPermissionMap.entrySet().stream()
			.filter(entry -> BooleanUtils.toBoolean(entry.getValue()))
			.map(Map.Entry :: getKey)
			.filter(YumPmsUtils :: isGreaterThanZero)
			.map(String:: valueOf)
			.collect(Collectors.joining(YumPmsConstants.COMMA));
		if(StringUtils.isNotEmpty(permittedEmpId)) {
			paDao.updateCompLetterPermission(permittedEmpId, apprId, purpose, 1);
		}
		String permitionRemovedEmpId = compLetterDownloadPermissionMap.entrySet().stream()
				.filter(entry -> !BooleanUtils.toBoolean(entry.getValue()))
				.map(Map.Entry :: getKey)
				.filter(YumPmsUtils :: isGreaterThanZero)
				.map(String:: valueOf)
				.collect(Collectors.joining(YumPmsConstants.COMMA));
		if(StringUtils.isNotEmpty(permitionRemovedEmpId)) {
			paDao.updateCompLetterPermission(permitionRemovedEmpId, apprId, purpose, 0);
		}
	}
	
	@Override
	public byte[] bulkDownloadCompensationLetter(String empIds, Integer apprId, String salaryType) {
		
		if(StringUtils.isEmpty(empIds) || YumPmsUtils.isLessThanZero(apprId) || StringUtils.isEmpty(salaryType)) {
			throw new CommonException("Invalid Request Data");
		}
		List<CompensationLetter> letterList = getCompensationLetterNew(empIds, apprId, salaryType);
		Map<String, byte[]> zipFilesMap = new HashMap<>();
		for(CompensationLetter letter : letterList) {
			byte[] content = YumPmsUtils.getCompensationLetterContent(letter);
			String fileName = YumPmsUtils.getCompLetterFileName(letter.getEmpNme(), letter.getApprYearDesc(), salaryType);
			zipFilesMap.put(fileName, content);
		}
		String zipFilesName = YumPmsUtils.getPdfFileName("Admin_Download", letterList.get(0).getApprYearDesc(), YumPmsConstants.PA_ZIP_FILE);
		return ResourceUtils.getZipAsBytes(zipFilesName, zipFilesMap);
	}
	
}
