package com.yum.comp.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.transform.AliasedTupleSubsetResultTransformer;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.yum.pms.dao.AppraisalPeriod;
import com.yum.pms.exception.CommonException;
import com.yum.pms.exception.YumPMSDataSaveException;
import com.yum.pms.service.YumHibernateUtilServices;
import com.yum.pms.utils.YumPmsConstants;
import com.yum.pms.utils.YumPmsUtils;

@Repository
@SuppressWarnings("unchecked")
public abstract class AbstractDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractDao.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	/**
	 * Get the meta data object of the provided model/entity class
	 * @param clazz
	 * @return
	 */
	public <T> ClassMetadata getClassMetadata(Class<T> clazz) {
		
		if(!clazz.isAnnotationPresent(javax.persistence.Entity.class)) {
			throw new CommonException("Invalid Entity class");
		}
		return sessionFactory.getClassMetadata(clazz);
	}
	
	/**
	 * Return true if Identifier property is available else false
	 * @param clazz
	 * @return
	 */
	public <T> boolean hasIdentifierProperty(Class<T> clazz) {
		
		return getClassMetadata(clazz).hasIdentifierProperty();
	}
	
	/**
	 * Return true if Identifier property is available else false
	 * @param classMetadata
	 * @return
	 */
	public boolean hasIdentifierProperty(ClassMetadata classMetadata) {
		
		return classMetadata.hasIdentifierProperty();
	}
	
	/**
	 * Get the ID property name
	 * @param clazz
	 * @return
	 */
	public <T> String getIdentifierPropertyName(Class<T> clazz) {
		
		ClassMetadata classMetadata = getClassMetadata(clazz);
		if(hasIdentifierProperty(classMetadata)) {
			return classMetadata.getIdentifierPropertyName();
		}
		return StringUtils.EMPTY;
	}
	
	/**
	 * Is the field is exist in the table or not
	 * @param clazz
	 * @param fieldName
	 * @return
	 */
	public <T> boolean hasFieldExist(Class<T> clazz, String fieldName) {
		
		if(StringUtils.isEmpty(fieldName)) {
			return false;
		}
		return getAllPropertyNames(clazz).contains(StringUtils.lowerCase(fieldName));
	}
	
	/**
	 * Get all the property names of the table
	 * @param clazz
	 * @return
	 */
	public <T> Set<String> getAllPropertyNames(Class<T> clazz) {
		
		List<String> propertyNames = Arrays.asList(getClassMetadata(clazz).getPropertyNames());
		if(CollectionUtils.isEmpty(propertyNames)) {
			return Collections.emptySet();
		}
		return propertyNames.stream().map(StringUtils :: lowerCase).collect(Collectors.toSet());
	}
	
	/**
	 * Get all the records 
	 * @param clazz
	 * @return
	 */
	public <T> List<T> findAll(Class<T> clazz) {
		
		return YumPmsUtils.getEmptyListIfNull(buildCriteriaForFindAll(clazz).list());
	}

	/**
	 * create a criteria object for the model class
	 * @param clazz
	 * @return
	 */
	private <T> Criteria buildCriteriaForFindAll(Class<T> clazz) {
		
		Criteria criteria = YumHibernateUtilServices.getSession(sessionFactory).createCriteria(clazz);
		if(hasFieldExist(clazz, YumPmsConstants.ACTIVE)) {
			criteria.add(Restrictions.eq(YumPmsConstants.ACTIVE, true));
		}
		return criteria;
	}
	
	/**
	 * Get all the records with order by fields
	 * Example: column1 ASC, column2 DESC, column3 ASC, etc
	 * @param clazz
	 * @param orderByColumns
	 * @return
	 */
	public <T> Optional<List<T>> findAll(Class<T> clazz, String ...orderByColumns) {
		
		Criteria criteria = buildCriteriaForFindAll(clazz);
		if(ArrayUtils.isNotEmpty(orderByColumns)) {
			for(String column : orderByColumns) {
				String[] split = StringUtils.split(column);
				if(ArrayUtils.getLength(split) == 2) {
					if(StringUtils.equalsIgnoreCase(split[1], "ASC")) {
						criteria.addOrder(Order.asc(split[0]));
					} else if(StringUtils.equalsIgnoreCase(split[1], "DESC")) {
						criteria.addOrder(Order.desc(split[0]));
					}
				} else if(ArrayUtils.getLength(split) == 1) {
					criteria.addOrder(Order.asc(split[0]));
				}
			}
		}
		return Optional.ofNullable(criteria.list());
	}
	
	/**
	 * Get all the records against the provided appraisal period.
	 * Note: Make sure that the field name of the AppraisalPeriod in the given class is : "appraisalPeriod"
	 * @param clazz
	 * @param apprId
	 * @return
	 */
	public <T> List<T> findAllByApprId(Class<T> clazz, Integer apprId) {
		
		Criteria criteria = YumHibernateUtilServices.getSession(sessionFactory).createCriteria(clazz);
		if(hasFieldExist(clazz, YumPmsConstants.ACTIVE)) {
			criteria.add(Restrictions.eq(YumPmsConstants.ACTIVE, true));
		}
		if(hasFieldExist(clazz, YumPmsConstants.APPRAISAL_PERIOD)) {
			criteria.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, new AppraisalPeriod(apprId)));
		}
		return YumPmsUtils.getEmptyListIfNull(criteria.list());
	}
	
	/**
	 * Get the first row of the result set
	 * @param clazz
	 * @param apprId
	 * @return
	 */
	public <T> Optional<T> findOneByApprId(Class<T> clazz, Integer apprId) {
		
		LOGGER.info("apprId = {}", apprId);
		List<T> list = findAllByApprId(clazz, apprId);
		if(CollectionUtils.isEmpty(list)) {
			return Optional.empty();
		}
		return YumPmsUtils.resolve(() -> list.get(0));
	}
	
	/**
	 * Get the persistent object by the id
	 * @param clazz
	 * @param id
	 * @return persistent object
	 */
	public <T> Optional<T> findById(Class<T> clazz, Number id) {
		
		if(clazz == null || StringUtils.isEmpty(clazz.getSimpleName())) {
			return Optional.empty();
		}
		String idFieldName = getIdentifierPropertyName(clazz);
		if(StringUtils.isEmpty(idFieldName)) {
			return Optional.empty();
		}
		T obj = YumHibernateUtilServices.getSession(sessionFactory).get(clazz, id);
		return Optional.ofNullable(obj);
	}
	
	/**
	 * saveOrUpdate the provided object
	 * @param t
	 * @return saved object
	 * @throws YumPMSDataSaveException
	 */
	public <T> Optional<T> saveOrUpdate(final T t) throws YumPMSDataSaveException {
		
		if(t == null) {
			throw new IllegalArgumentException("Invalid Persistance Object(null)");
		}
		if(!hasIdentifierProperty(t.getClass())) {
			throw new CommonException("Invalid Operation: This object does not contains the IdentifierProperty");
		}
		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
		T obj = null;
		try {
			obj = (T)session.merge(t);
			YumHibernateUtilServices.commitTransaction(session);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException("Failed to save or update in " + YumPmsUtils.resolve(() -> t.getClass().getSimpleName()), e);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return Optional.ofNullable(obj);
	}
	
	/**
	 * saveOrUpdate multiple records
	 * @param list
	 * @return all the saved records
	 * @throws YumPMSDataSaveException
	 */
	public <T> List<T> saveOrUpdateAll(final List<T> list) throws YumPMSDataSaveException {
		
		if(CollectionUtils.isEmpty(list)) {
			return Collections.emptyList();
		}
		Session session = null;
		List<T> resultList = new ArrayList<>(list.size());
		try {
			session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
			for(T payRange : list) {
				payRange = (T) session.merge(payRange);
				resultList.add(payRange);
			}
			YumHibernateUtilServices.commitTransaction(session);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException(YumPmsConstants.ERROR_FAIL_TO_SAVE , e);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return YumPmsUtils.getEmptyListIfNull(resultList);
	}
	
	/**
	 * Delete any persistence object by it's id
	 * @param clazz
	 * @param id
	 * @return true/false
	 */
	public <T> boolean deleteById(Class<T> clazz, Number id) {

		try {
			findById(clazz, id).ifPresent(persistentInstance -> {
				Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
				session.delete(persistentInstance);
				YumHibernateUtilServices.commitTransaction(session);
			});
			return true;
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			return false;
		}
	}
	
	/**
	 * Get the map(id, description) of master table data 
	 * @param clazz
	 * @param descFieldName
	 * @return
	 */
	public <T> Map<Integer, String> getMasterTableMap(Class<T> clazz, String descFieldName) {
		
		// Validation logic
		if(clazz == null || StringUtils.isEmpty(clazz.getSimpleName()) || StringUtils.isEmpty(descFieldName)) {
			return Collections.emptyMap();
		}
		String idFieldName = getIdentifierPropertyName(clazz);
		if(StringUtils.isEmpty(idFieldName) || !hasFieldExist(clazz, descFieldName)) {
			return Collections.emptyMap();
		}
		// Create dynamic HQL
		StringBuilder hql = new StringBuilder("SELECT T.")
				.append(idFieldName).append(", T.").append(descFieldName)
				.append(" FROM ").append(clazz.getSimpleName()).append(" T");
		
		if(hasFieldExist(clazz, YumPmsConstants.ACTIVE)) {
			hql.append(" WHERE ").append(YumPmsConstants.ACTIVE).append(" = 1");
		}
		LOGGER.info("Dynamic HQL Generated... {}", hql);
		List<Object[]> list = YumHibernateUtilServices.getSession(sessionFactory).createQuery(hql.toString()).list();
		if(CollectionUtils.isEmpty(list)) {
			return Collections.emptyMap();
		}
		Map<Integer, String> resultMap = new HashMap<>(list.size());
		for (Object[] obj : list) {
			resultMap.put(YumPmsUtils.castToInt(obj[0]), YumPmsUtils.toString(obj[1]));
		}
		return resultMap;
	}
	
	/**
	 * Returns List<Object[]> where all parameters are mandatory (Not a single parameter can be null).
	 * @param procedureName
	 * @param params
	 * @return
	 */
	public List<Object[]> executeProcedure(String procedureName, Object ...params) {
		
		return YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(prepareSqlQuery(procedureName, params))
				.list();
	}

	/**
	 * Returns List<EntityType> where all parameters are mandatory (Not a single parameter can be null).
	 * If the 1st parameter is not entity class, then the result column name should match with this class's properties or fields.
	 * and will inject aliased values into instances of Class via property methods or fields. 
	 * 2nd parameter is the procedure name
	 * 3rd parameters are the procedure parameters
	 * @param entity
	 * @param procedureName
	 * @param params
	 * @return
	 */
	public <T> List<T> executeProcedure(Class<T> entity, String procedureName, Object ...params) {

		SQLQuery query = YumHibernateUtilServices.getSession(sessionFactory).createSQLQuery(prepareSqlQuery(procedureName, params));
		if(entity.isAnnotationPresent(javax.persistence.Entity.class)) {
			query.addEntity(entity);
		} else {
			query.setResultTransformer(Transformers.aliasToBean(entity));
		}
		return query.list();
	}
	
	public Object returningSingleValueProcedure(String procedureName, Object ...params) {

		SQLQuery query = YumHibernateUtilServices.getSession(sessionFactory).createSQLQuery(prepareSqlQuery(procedureName, params));
		List<Object> list = query.list();
		return CollectionUtils.isEmpty(list) ? new Object() : list.get(0);
	}
	
	public Object invokeScalarFunction(String functionName, Object ...params) {
		
		StringBuilder sql = new StringBuilder("SELECT dbo." + functionName + "(");
		if(ArrayUtils.isNotEmpty(params)) {
			String namedParams = IntStream.range(0, params.length).mapToObj(index -> ":param" + index).collect(Collectors.joining(", "));
			sql.append(namedParams);
		}
		sql.append(")");
		SQLQuery sqlQuery = YumHibernateUtilServices.getSession(sessionFactory).createSQLQuery(sql.toString().trim());
		if(ArrayUtils.isNotEmpty(params)) {
			for(int i=0; i<params.length; i++) {
				sqlQuery.setParameter("param"+i, params[i]);
			}
		}
		return sqlQuery.uniqueResult();
	}
	
	/**
	 * This method will return List<Map<String, String>> where map key is column name and map value is column value
	 * @param procedureName
	 * @param params
	 * @return
	 */
	public List<Map<String, String>> executeDynamicProcedure(String procedureName, Object ...params) {

		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery(prepareSqlQuery(procedureName, params))
				.setResultTransformer(CustomResultTransformer.INSTANCE)
				.list();

	}
	
	private String prepareSqlQuery(String procedureName, Object... params) {
		
		if(StringUtils.isBlank(procedureName)) {
			throw new CommonException(YumPmsConstants.INVALID_SP_NAME);
		}
		StringBuilder sql = new StringBuilder("exec ").append(procedureName);
		if(params != null && params.length > 0) {
			String strParams = 
					Arrays.stream(params)
					.map(this :: prepareParam)
					.collect(Collectors.joining(", "));
			sql.append(YumPmsConstants.SPACE).append(strParams);
		}
		LOGGER.info("SP : {}", sql);
		return sql.toString().trim();
	}
	
	private String prepareParam(Object param) {
		
		if(YumPmsUtils.toString(param).isEmpty()) {
			return "''";
		} else {
			if(param instanceof String) {
				return YumPmsConstants.SINGLE_CODE + String.valueOf(param) + YumPmsConstants.SINGLE_CODE;
			}
			return String.valueOf(param);
		}
	}
	
	// ResultTransformer customized in order to get the result in a ordered map(LinkedHashMap) with string key and value
	private static class CustomResultTransformer extends AliasedTupleSubsetResultTransformer {
		
		private static final long serialVersionUID = 3136254208513883565L;
		public static final CustomResultTransformer INSTANCE = new CustomResultTransformer();

		private CustomResultTransformer() {
		}
		
		@Override
		public Object transformTuple(Object[] tuple, String[] aliases) {
			
			if(ArrayUtils.isEmpty(tuple)) {
				return Collections.emptyMap();
			}
			Map<String, String> result = new LinkedHashMap<>(tuple.length);
			for (int i=0; i<tuple.length; i++) {
				String alias = aliases[i];
				if (alias != null) {
					result.put(alias, Objects.toString(tuple[i], StringUtils.EMPTY));
				}
			}
			return result;
		}
		
		@Override
		public boolean isTransformedValueATupleElement(String[] aliases, int tupleLength) {
			return false;
		}
	
	}
}
