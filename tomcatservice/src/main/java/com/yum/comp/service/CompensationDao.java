package com.yum.comp.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.yum.comp.domain.CompensationUiConfig;
import com.yum.pms.dao.AppraisalPeriod;
import com.yum.pms.dao.CompLetterStatus;
import com.yum.pms.dao.CompLookupSalaryHeads;
import com.yum.pms.dao.CompPayRangeCategoryMaster;
import com.yum.pms.dao.CompRevisedSalaryEffectiveDuration;
import com.yum.pms.dao.CompYearWiseMultiplier;
import com.yum.pms.dao.CompYearWisePayRange;
import com.yum.pms.dao.CompYearWiseSalary;
import com.yum.pms.dao.EmployeeNew;
import com.yum.pms.exception.YumPMSDataSaveException;
import com.yum.pms.service.YumHibernateUtilServices;
import com.yum.pms.utils.YumPmsConstants;
import com.yum.pms.utils.YumPmsUtils;

/**
 * @author dipak.swain
 * All Compensation related database operation methods are defined here
 */
@Repository
@SuppressWarnings("unchecked")
public class CompensationDao extends AbstractDao {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CompensationDao.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private DataSource datasource;
	
	/**
	 * Get the configured pay range in a particular appraisal year
	 * @param apprId
	 * @return List<CompYearWisePayRange> 
	 */
	public List<CompYearWisePayRange> getYearWisePayRangeListByApprId(Integer apprId) {
		
		if(apprId == null) {
			return Collections.emptyList();
		}
		List<CompYearWisePayRange> yearWisePayRangeList =  
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(CompYearWisePayRange.class)
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, new AppraisalPeriod(apprId)))
				.addOrder(Order.asc(YumPmsConstants.GRADE_MASTER))
				//.addOrder(Order.asc(YumPmsConstants.DISPLAY_ORDER))
				.list();
		return YumPmsUtils.getEmptyListIfNull(yearWisePayRangeList);
	}
	
	/**
	 * Save the pay range which has provided by admin user
	 * @param yearWisePayRangeList
	 * @return List<CompYearWisePayRange>
	 * @throws YumPMSDataSaveException
	 */
	public List<CompYearWisePayRange> saveYearWisePayRangeList(List<CompYearWisePayRange> yearWisePayRangeList) throws YumPMSDataSaveException {
		
		if(CollectionUtils.isEmpty(yearWisePayRangeList)) {
			return Collections.emptyList();
		}
		Session session = null;
		List<CompYearWisePayRange> savedPayRangeList = new ArrayList<>(yearWisePayRangeList.size());
		try {
			session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);
			for(CompYearWisePayRange payRange : yearWisePayRangeList) {
				payRange = (CompYearWisePayRange) session.merge(payRange);
				savedPayRangeList.add(payRange);
			}
			YumHibernateUtilServices.commitTransaction(session);
		} catch(Exception e) {
			LOGGER.error(YumPmsConstants.ERROR_STACK_TRACE, e);
			YumHibernateUtilServices.rollbackTransaction(session);
			throw new YumPMSDataSaveException(YumPmsConstants.ERROR_FAIL_TO_SAVE , e);
		} finally {
			YumHibernateUtilServices.clearSession(session);
		}
		return savedPayRangeList;
	}
	
	/**
	 * Execute "sp_comp_RevisedSalaryCalculation" procedure by passing desired parameter and return the result
	 * @param params
	 * @return all the rows with column name and value
	 * @throws SQLException
	 */
	public List<Map<String, String>> revisedSalary(Map<String, String> params) throws SQLException {

		String sql = new StringBuilder(YumPmsConstants.SP_REVISE_SALARY)
				.append(params.get(YumPmsConstants.SP_PARAM_CUR_YEAR)).append(YumPmsConstants.UNDER_SCORE)
				.append(params.get(YumPmsConstants.SP_PARAM_NEXT_YEAR)).append(YumPmsConstants.SP_REVISE_SALARY_PARAM).toString();
		try (
				Connection connection = datasource.getConnection();
				PreparedStatement ps = connection.prepareStatement(sql);
				) {
			ps.setString(1, StringUtils.trimToEmpty(params.get(YumPmsConstants.SP_REVISED_SALARY_PARAM_EMP_ID)));
			ps.setString(2, StringUtils.trimToEmpty(params.get(YumPmsConstants.SP_PARAM_CUR_YEAR)));
			ps.setString(3, StringUtils.trimToEmpty(params.get(YumPmsConstants.SP_PARAM_NEXT_YEAR)));
			ps.setString(4, StringUtils.trimToEmpty(params.get(YumPmsConstants.ACTION)));
			return buildResultData(ps, params);
		}


	}
	
	/**
	 * Execute sp_comp_RevisedBonusCalculation procedure by passing desired value and return the result
	 * @param empId
	 * @param curYear
	 * @param nextYear
	 * @return all the rows with column name and value 
	 * @throws SQLException
	 */
	public List<Map<String, String>> bonusCalculation(String empId, String curYear, String nextYear) throws SQLException {

		String sql = new StringBuilder(YumPmsConstants.SP_BONUS_CALCULATION)
				.append(curYear).append(YumPmsConstants.UNDER_SCORE).append(nextYear).append(" ?, ?, ?").toString();
		
		try (
				Connection connection = datasource.getConnection();
				PreparedStatement ps = connection.prepareStatement(sql);
				) {
			ps.setString(1, empId != null ? empId : StringUtils.EMPTY);
			ps.setString(2, curYear != null ? curYear : StringUtils.EMPTY);
			ps.setString(3, nextYear != null ? nextYear : StringUtils.EMPTY);
			LOGGER.info("SP call ===> SP Defination: {}, empId: {} ,curYear: {} ,nextYear: {}", sql, empId, curYear, nextYear);
			return buildResultData(ps, null);
		}
	}

	/**
	 * It is a helper method where we can pass the prepared statement with necessary parameter and it will return the result by executing in database
	 * @param ps
	 * @param params
	 * @return all the rows with column name and value
	 * @throws SQLException
	 */
	private List<Map<String, String>> buildResultData(PreparedStatement ps, Map<String, String> params) throws SQLException {

		if(null == params || (null != params.get(YumPmsConstants.ACTION) 
				&& !params.get(YumPmsConstants.ACTION).equalsIgnoreCase(YumPmsConstants.SAVE))) { 
			try(ResultSet rs = ps.executeQuery()) {
				if(!rs.isBeforeFirst()) {
					return Collections.emptyList();
				}
				List<Map<String, String>> resultList = new ArrayList<>();
				ResultSetMetaData rsmd = rs.getMetaData();
				while(rs.next()) {
					Map<String, String> dataMap = new LinkedHashMap<>(rsmd.getColumnCount());
					for(int i = 1; i <= rsmd.getColumnCount(); i++) {	
						dataMap.put(rsmd.getColumnName(i), rs.getString(i));
					}
					resultList.add(dataMap);
				}
				return resultList;
			}
		} else {
			ps.executeUpdate();
			params.put(YumPmsConstants.ACTION, YumPmsConstants.REVISED_SAL);
			return revisedSalary(params);
		}
	}

	/**
	 * Get the multiplier value(like 1.0, 1.5, 1.9) corresponding to the appraisal rating in a particular year
	 * @param apprId
	 * @return List<CompYearWiseMultiplier>
	 */
	public List<CompYearWiseMultiplier> getYearWiseMultiplierList(Integer apprId) {
		
		if(apprId == null) {
			return Collections.emptyList();
		}
		List<CompYearWiseMultiplier> yearWisePayRangeList =  
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(CompYearWiseMultiplier.class)
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, new AppraisalPeriod(apprId)))
				.list();
		return YumPmsUtils.getEmptyListIfNull(yearWisePayRangeList);
	}
	
	/**
	 * This will return all letter template placeholder name list
	 */
	public List<String> getLetterPlaceholders() {

		return YumHibernateUtilServices.getSession(sessionFactory)
				.createSQLQuery("SELECT placeholder_name FROM comp_letter_placeholder WHERE IsActive = 1")
				.list();

	}
	
	/**
	 * All the configured salary heads in a predefined order in a particular appraisal year
	 * @param apprId
	 * @return List<CompLookupSalaryHeads>
	 */
	public List<CompLookupSalaryHeads> getSalaryHeadsByApprId(Integer apprId) {
		
		List<CompLookupSalaryHeads> salaryHeadList = findAllByApprId(CompLookupSalaryHeads.class, apprId)
				.stream().sorted(Comparator.comparing(CompLookupSalaryHeads :: getDisplayOrder)).collect(Collectors.toList());
		
		return YumPmsUtils.getEmptyListIfNull(salaryHeadList);
	}
	
	public List<CompPayRangeCategoryMaster> getPayRangeCategorysByApprId(Integer apprId) {
		
		List<CompPayRangeCategoryMaster>  payRangeCategoryList = findAllByApprId(CompPayRangeCategoryMaster.class, apprId);
		if(CollectionUtils.isNotEmpty(payRangeCategoryList)) {
			payRangeCategoryList.sort(Comparator.comparing(CompPayRangeCategoryMaster :: getDisplayOrder));
		}
		return YumPmsUtils.getEmptyListIfNull(payRangeCategoryList);
	}
	
	/**
	 * Get all the salary fields of an employee in a particular appraisal year
	 * @param empId
	 * @param appraisalId
	 * @return List<CompYearWiseSalary> 
	 */
	public List<CompYearWiseSalary> getSalaryDetails(Integer empId, Integer appraisalId) {
		
		List<CompYearWiseSalary> salaryDetailsList =  
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(CompYearWiseSalary.class)
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, new AppraisalPeriod(appraisalId)))
				.add(Restrictions.eq(YumPmsConstants.EMPLOYEE_MODEL, new EmployeeNew(empId)))
				.list();
		return YumPmsUtils.getEmptyListIfNull(salaryDetailsList);
	}
	

	/**
	 * Get the revised salary effective date of a particular appraisal year
	 * @param appraisalPeriord
	 * @return CompRevisedSalaryEffectiveDuration
	 */
	public CompRevisedSalaryEffectiveDuration getEffectiveDurationDate(AppraisalPeriod appraisalPeriord) { 
		CompRevisedSalaryEffectiveDuration gradeMstr = 
				(CompRevisedSalaryEffectiveDuration)YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(CompRevisedSalaryEffectiveDuration.class)
				.add(Restrictions.eq(YumPmsConstants.APPRAISAL_PERIOD, appraisalPeriord))
				.add(Restrictions.eq(YumPmsConstants.ACTIVE, true))
				.uniqueResult();  
		return gradeMstr == null ? new CompRevisedSalaryEffectiveDuration() : gradeMstr;
	}
	
	/**
	 * Get all the compensation letter status (download permission, email sent status or letter acceptance status) 
	 * of an employee in a particular appraisal year
	 * @param empId
	 * @param apprId
	 * @return List<CompLetterStatus>
	 */
	public List<CompLetterStatus> getCompLettersForDownload(Integer empId, Integer apprId) {
		
		return 
				YumHibernateUtilServices.getSession(sessionFactory)
				.createCriteria(CompLetterStatus.class)
				.add(Restrictions.eq(YumPmsConstants.APPR_ID, apprId))
				.add(Restrictions.eq(YumPmsConstants.EMP_ID, empId))
				.add(Restrictions.eq(YumPmsConstants.LETTER_DOWNLOAD_PERMISSION, true))
				.list();
	}
	
	
	// -----------------------------------bulk submit---------------------------------------------------
	
	/**
	 * Bulk insertion of empId, additional correction percentage and promoted designation into comp_revised_salary_changed_value table
	 * @param revisedSalrayList
	 */
	public void saveParamiterOfRevisedSalary(List<CompensationUiConfig> revisedSalrayList) {
		
		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory); 
		for(CompensationUiConfig tupules : revisedSalrayList) {
			
			SQLQuery sqlQuery1 = session.createSQLQuery(String.format(YumPmsConstants.INSERT_QUERY_FOR_PARAMITER_OF_REVISED_SALARY, 
					tupules.getEmpId(), tupules.getAdditionalCorrectionPercentage(), tupules.getNewDesignationId()));
			sqlQuery1.executeUpdate();
			
			SQLQuery sqlQuery2 = session.createSQLQuery(String.format(YumPmsConstants.INSERT_QUERY_FOR_MOBILITY_ALLOWANCE, 
					tupules.getEmpId(), tupules.getMobilityAllowance()));
			sqlQuery2.executeUpdate();
		}		
		YumHibernateUtilServices.commitTransaction(session); 
	    YumHibernateUtilServices.clearSession(session); 
	}
	
	/**
	 * Clean the "comp_revised_salary_changed_value" table for inserting again for another employee
	 */
	public void deleteParamiterOfRevisedSalary() {
		
		Session session = YumHibernateUtilServices.getSessionAndBeginTransaction(sessionFactory);    

		SQLQuery sqlQuery = session.createSQLQuery(YumPmsConstants.DELETE_QUERY_FOR_PARAMITER_OF_REVISED_SALARY);
		sqlQuery.executeUpdate();	
		
		sqlQuery = session.createSQLQuery(YumPmsConstants.DELETE_QUERY_FOR_MOBILITY_ALLOWANCE);
		sqlQuery.executeUpdate();	
		
		YumHibernateUtilServices.commitTransaction(session); 
	    YumHibernateUtilServices.clearSession(session); 
	}

}
