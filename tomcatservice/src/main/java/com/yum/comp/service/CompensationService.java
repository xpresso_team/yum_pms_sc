package com.yum.comp.service;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.yum.comp.domain.CompLetterUiData;
import com.yum.comp.domain.CompUiRevisedSalary;
import com.yum.comp.domain.CompensationLetter;
import com.yum.comp.domain.CompensationUiConfig;
import com.yum.comp.domain.DesgWiseVpay;
import com.yum.comp.domain.GeneralConfigData;
import com.yum.comp.domain.LetterTemplateConfig;
import com.yum.comp.domain.LookupUiConfig;
import com.yum.comp.domain.SalaryHeadData;
import com.yum.comp.domain.YearWisePayRangeData;
import com.yum.pms.dao.CompBonusConfiguration;
import com.yum.pms.dao.CompLetterStatus;
import com.yum.pms.dao.CompYearWiseMultiplier;
import com.yum.pms.exception.YumPMSDataSaveException;

public interface CompensationService {
	
	List<CompensationUiConfig> getReviesSalary(Integer empId, String curYear, String nextYear, String action) throws SQLException;
	
	List<CompensationUiConfig> getBonusSheetList(Integer empId, String curYear, String nextYear) throws SQLException;
	
	YearWisePayRangeData getYearWisePayRangeData(Integer apprId);
	
	YearWisePayRangeData saveYearWisePayRangeData(YearWisePayRangeData payRangeData) throws YumPMSDataSaveException;

	List<CompYearWiseMultiplier> getYearWiseMultiplierData(Integer apprId);

	List<CompYearWiseMultiplier> saveYearWiseMultiplierData(List<CompYearWiseMultiplier> multiplierData) throws YumPMSDataSaveException;
	
	LookupUiConfig getGradeAndYearWiseLookupData(Integer apprId);
	
	CompUiRevisedSalary saveEditReviesSalaryNew(CompUiRevisedSalary revisedSalry) throws SQLException;

	LookupUiConfig saveGradeAndYearWiseLookupData(LookupUiConfig lookupUiConfig) throws YumPMSDataSaveException;

	Map<String, Object> getRevisedSalaryEffectiveDuration(Integer apprId);

	Map<String, Object> saveRevisedSalaryEffectiveDuration(Map<String, Object> requestMap) throws YumPMSDataSaveException;

	DesgWiseVpay getDesgWiseVpayPercentage(Integer apprId);

	DesgWiseVpay saveDesgWiseVpayPercentage(DesgWiseVpay desgWiseVpay) throws YumPMSDataSaveException;

	GeneralConfigData getGeneralConfiguration(Integer apprId);
	
	GeneralConfigData saveGeneralConfiguration(GeneralConfigData generalConfigData) throws YumPMSDataSaveException;
	
	CompensationLetter getCompensationLetter(Integer empId, Integer apprId, String salaryType, boolean isPdf);
	
	List<CompLetterStatus> getCompLettersForDownload(Integer empId, Integer apprId);
	
	Map<String, byte[]> downloadOrAcceptCompLetter(Integer empId, Integer apprId, String salaryType, boolean isAccepted);
	
	List<CompensationLetter> getCompensationLetterNew(String empIds, Integer apprId, String salaryType);
	
	byte[] bulkDownloadCompensationLetter(String empIds, Integer apprId, String salaryType);
	
	//void sendEmailWithCompensationLetter(Integer empId, Integer apprId, String attachmentPath);	

	void sendEmailWithCompensationLetter(String empIds, String supEmpIds, Integer apprId, Map<Integer, String> attachmentPath);
	
	void updateCompLetterEmailStatus(String empIds, Integer apprId, String purpose);
	
	LetterTemplateConfig getCompLetterTemplate(Integer apprId);
	
	CompLetterUiData getAllEmpsDetailsForCompLetter(Integer apprId, String salaryType);

	LetterTemplateConfig saveCompLetterTemplate(LetterTemplateConfig compLetterTemplate) throws YumPMSDataSaveException; 
	
	List<SalaryHeadData> getSalaryDetails(int empId, int appraisalId) throws YumPMSDataSaveException;
	
	List<SalaryHeadData> insertOrUpdateSalray(List<SalaryHeadData> uiSalaryObjectList) throws YumPMSDataSaveException;
	
	List<CompensationUiConfig> getSpecialSalary(Integer appraisalPeriord, Integer nextAppraisalPeriord, Integer empId, String purpose, String dataList, String action) throws SQLException;
	
	String configureNewAppraisalPeriod(Integer currentApprId, Integer nextApprId);
	
	void updateCompLetterDownloadPermission(String empIds, Map<Integer, Boolean> compLetterDownloadPermissionMap, Integer apprId, String purpose) throws YumPMSDataSaveException;

	CompBonusConfiguration getBonusConfig(Integer apprId);

	CompBonusConfiguration saveBonusConfig(CompBonusConfiguration request) throws YumPMSDataSaveException;	 

}
