/**
 * 
 */
package com.yum.comp.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jayanta.biswas
 *
 */
public class CompensationUiConfig implements Serializable {
	
	private static final long serialVersionUID = -351719573606103162L;
	
	private Integer empId;
	
	private String empName;
	
	private List<List<DisplayDomain>> displayList = new ArrayList<>();
	
	private List<DisplayDomain> columnList = new ArrayList<>();
	
	private List<DisplayDomain> columnListLastYear = new ArrayList<>();
	
	private List<DisplayDomain> columnListSecondLastYear = new ArrayList<>();
	
	private String curYear;
	
	private String nextYear;
	
	private String action;
	
	private String additionalCorrectionPercentage;
	
	private String mobilityAllowance;
	
	private String newDesignationId;
	
	private Double  bonusField;
	
	private Date timeLine;
	
	private Map<String, String> topProperty = new HashMap<>();
	
	private Boolean additionalCorrectionEditableFlag = false ;

	/**
	 * @return the empId
	 */
	public Integer getEmpId() {
		return empId;
	}

	/**
	 * @param empId the empId to set
	 */
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	/**
	 * @return the displayList
	 */
	public List<List<DisplayDomain>> getDisplayList() {
		return displayList;
	}

	/**
	 * @param displayList the displayList to set
	 */
	public void setDisplayList(List<List<DisplayDomain>> displayList) {
		this.displayList = displayList;
	}

	/**
	 * @return the columnList
	 */
	public List<DisplayDomain> getColumnList() {
		return columnList;
	}

	/**
	 * @param columnList the columnList to set
	 */
	public void setColumnList(List<DisplayDomain> columnList) {
		this.columnList = columnList;
	}

	/**
	 * @return the curYear
	 */
	public String getCurYear() {
		return curYear;
	}

	/**
	 * @param curYear the curYear to set
	 */
	public void setCurYear(String curYear) {
		this.curYear = curYear;
	}

	/**
	 * @return the nextYear
	 */
	public String getNextYear() {
		return nextYear;
	}

	/**
	 * @param nextYear the nextYear to set
	 */
	public void setNextYear(String nextYear) {
		this.nextYear = nextYear;
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return the additionalCorrectionPercentage
	 */
	public String getAdditionalCorrectionPercentage() {
		return additionalCorrectionPercentage;
	}

	/**
	 * @param additionalCorrectionPercentage the additionalCorrectionPercentage to set
	 */
	public void setAdditionalCorrectionPercentage(String additionalCorrectionPercentage) {
		this.additionalCorrectionPercentage = additionalCorrectionPercentage;
	}

	/**
	 * @return the newDesignationId
	 */
	public String getNewDesignationId() {
		return newDesignationId;
	}

	/**
	 * @param newDesignationId the newDesignationId to set
	 */
	public void setNewDesignationId(String newDesignationId) {
		this.newDesignationId = newDesignationId;
	}

	/**
	 * @return the topProperty
	 */
	public Map<String, String> getTopProperty() {
		return topProperty;
	}

	/**
	 * @param topProperty the topProperty to set
	 */
	public void setTopProperty(Map<String, String> topProperty) {
		this.topProperty = topProperty;
	}

	public Double getBonusField() {
		return bonusField;
	}

	public void setBonusField(Double bonusField) {
		this.bonusField = bonusField;
	}

	public Date getTimeLine() {
		return timeLine;
	}

	public void setTimeLine(Date timeLine) {
		this.timeLine = timeLine;
	}

	/**
	 * @return the columnListLastYear
	 */
	public List<DisplayDomain> getColumnListLastYear() {
		return columnListLastYear;
	}

	/**
	 * @param columnListLastYear the columnListLastYear to set
	 */
	public void setColumnListLastYear(List<DisplayDomain> columnListLastYear) {
		this.columnListLastYear = columnListLastYear;
	}

	/**
	 * @return the columnListSecondLastYear
	 */
	public List<DisplayDomain> getColumnListSecondLastYear() {
		return columnListSecondLastYear;
	}

	/**
	 * @param columnListSecondLastYear the columnListSecondLastYear to set
	 */
	public void setColumnListSecondLastYear(List<DisplayDomain> columnListSecondLastYear) {
		this.columnListSecondLastYear = columnListSecondLastYear;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {
		return empName;
	}

	/**
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	/**
	 * @return the additionalCorrectionEditableFlag
	 */
	public Boolean getAdditionalCorrectionEditableFlag() {
		return additionalCorrectionEditableFlag;
	}

	/**
	 * @param additionalCorrectionEditableFlag the additionalCorrectionEditableFlag to set
	 */
	public void setAdditionalCorrectionEditableFlag(Boolean additionalCorrectionEditableFlag) {
		this.additionalCorrectionEditableFlag = additionalCorrectionEditableFlag;
	}

	public String getMobilityAllowance() {
		return mobilityAllowance;
	}

	public void setMobilityAllowance(String mobilityAllowance) {
		this.mobilityAllowance = mobilityAllowance;
	}

}
