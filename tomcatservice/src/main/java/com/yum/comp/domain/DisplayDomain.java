/**
 * 
 */
package com.yum.comp.domain;

import java.io.Serializable;
import java.util.Map.Entry;

/**
 * @author jayanta.biswas
 *
 */
public class DisplayDomain implements Serializable {
	
	private static final long serialVersionUID = -6246577232884532160L;
	
	private String columnName;
	
	private String columnValue;
	
	private String nextColumnValue;
	
	private int columnPosition;
	
	private Boolean isEditable;
	
	private Boolean isDisplay;

	public DisplayDomain() {
	}

	public DisplayDomain(Entry<String, String> entry) {
		this.columnName = entry.getKey();
		this.columnValue = entry.getValue();
	}

	/**
	 * @return the columnName
	 */
	public String getColumnName() {
		return columnName;
	}

	/**
	 * @param columnName the columnName to set
	 */
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	/**
	 * @return the columnPosition
	 */
	public int getColumnPosition() {
		return columnPosition;
	}

	/**
	 * @param columnPosition the columnPosition to set
	 */
	public void setColumnPosition(int columnPosition) {
		this.columnPosition = columnPosition;
	}

	/**
	 * @return the columnValue
	 */
	public String getColumnValue() {
		return columnValue;
	}

	/**
	 * @param columnValue the columnValue to set
	 */
	public void setColumnValue(String columnValue) {
		this.columnValue = columnValue;
	}

	/**
	 * @return the nextColumnValue
	 */
	public String getNextColumnValue() {
		return nextColumnValue;
	}

	/**
	 * @param nextColumnValue the nextColumnValue to set
	 */
	public void setNextColumnValue(String nextColumnValue) {
		this.nextColumnValue = nextColumnValue;
	}

	/**
	 * @return the isEditable
	 */
	public Boolean getIsEditable() {
		return isEditable;
	}

	/**
	 * @param isEditable the isEditable to set
	 */
	public void setIsEditable(Boolean isEditable) {
		this.isEditable = isEditable;
	}

	/**
	 * @return the isDisplay
	 */
	public Boolean getIsDisplay() {
		return isDisplay;
	}

	/**
	 * @param isDisplay the isDisplay to set
	 */
	public void setIsDisplay(Boolean isDisplay) {
		this.isDisplay = isDisplay;
	}


}
