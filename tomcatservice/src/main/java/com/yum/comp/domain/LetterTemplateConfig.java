package com.yum.comp.domain;

import java.io.Serializable;
import java.util.List;

import com.yum.pms.dao.CompLetterTemplate;

public class LetterTemplateConfig implements Serializable {

	private static final long serialVersionUID = 746255490562345201L;

	private String logonUser;
	
	private String statusMessage;
	
	private Integer apprId;
	
	private List<CompLetterTemplate> letterTemplateList;
	
	private List<String> possiblePlaceholder; 
	
	public LetterTemplateConfig() {
	}
	
	public LetterTemplateConfig(Integer apprId, List<CompLetterTemplate> letterTemplateList) {
		this.apprId = apprId;
		this.letterTemplateList = letterTemplateList;
	}
	
	public LetterTemplateConfig(Integer apprId, List<String> possiblePlaceholder, List<CompLetterTemplate> letterTemplateList) {
		this.apprId = apprId;
		this.possiblePlaceholder = possiblePlaceholder;
		this.letterTemplateList = letterTemplateList;
	}
	
	public LetterTemplateConfig(List<CompLetterTemplate> letterTemplateList) {
		this.letterTemplateList = letterTemplateList;
	}

	public String getLogonUser() {
		return logonUser;
	}

	public void setLogonUser(String logonUser) {
		this.logonUser = logonUser;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public Integer getApprId() {
		return apprId;
	}

	public void setApprId(Integer apprId) {
		this.apprId = apprId;
	}

	public List<CompLetterTemplate> getLetterTemplateList() {
		return letterTemplateList;
	}

	public void setLetterTemplateList(List<CompLetterTemplate> letterTemplateList) {
		this.letterTemplateList = letterTemplateList;
	}

	public List<String> getPossiblePlaceholder() {
		return possiblePlaceholder;
	}

	public void setPossiblePlaceholder(List<String> possiblePlaceholder) {
		this.possiblePlaceholder = possiblePlaceholder;
	}
	
}
