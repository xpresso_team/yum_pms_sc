package com.yum.comp.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GradeWisePayRange implements Serializable {
	
	private static final long serialVersionUID = 8836993664200194520L;

	private Integer gradeId;
	
	private String grade;
	
	private List<PayRangeData> payRangeList = new ArrayList<>(5);
	
	public GradeWisePayRange() {
	}

	public GradeWisePayRange(Integer gradeId, String grade) {
		this.gradeId = gradeId;
		this.grade = grade;
	}

	public GradeWisePayRange(Integer gradeId, String grade, List<PayRangeData> payRangeList) {
		this.gradeId = gradeId;
		this.grade = grade;
		this.payRangeList = payRangeList;
	}

	public Integer getGradeId() {
		return gradeId;
	}

	public void setGradeId(Integer gradeId) {
		this.gradeId = gradeId;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public List<PayRangeData> getPayRangeList() {
		return payRangeList;
	}

	public void setPayRangeList(List<PayRangeData> payRangeList) {
		this.payRangeList = payRangeList;
	}

	@Override
	public String toString() {
		return "GradeWisePayRange [gradeId=" + gradeId + ", grade=" + grade + ", payRangeList Size=" 
				+ (payRangeList != null ? payRangeList.size() : 0) + "]";
	}
	
}
