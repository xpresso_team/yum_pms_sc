package com.yum.comp.domain;

import java.io.Serializable;

import com.yum.pms.dao.CompDesignationWiseVpayPercentage;

public class VpayData implements Serializable {
	
	private static final long serialVersionUID = 8159247914124643838L;

	private Integer vpayPercentId;
	
	private Integer designationId;
	
	private Double vpayPercentValue;
	
	public VpayData() {
	}

	public VpayData(Integer vpayPercentId) {
		this.vpayPercentId = vpayPercentId;
	}

	public VpayData(Integer vpayPercentId, Integer designationId, Double vpayPercentValue) {
		this.vpayPercentId = vpayPercentId;
		this.designationId = designationId;
		this.vpayPercentValue = vpayPercentValue;
	}
	
	public VpayData(CompDesignationWiseVpayPercentage vpayPercentage) {
		this(vpayPercentage.getId(), vpayPercentage.getDesgMaster().getDesignationId(), vpayPercentage.getvPayValue());
	}

	public Integer getVpayPercentId() {
		return vpayPercentId;
	}

	public void setVpayPercentId(Integer vpayPercentId) {
		this.vpayPercentId = vpayPercentId;
	}

	public Integer getDesignationId() {
		return designationId;
	}

	public void setDesignationId(Integer designationId) {
		this.designationId = designationId;
	}

	public Double getVpayPercentValue() {
		return vpayPercentValue;
	}

	public void setVpayPercentValue(Double vpayPercentValue) {
		this.vpayPercentValue = vpayPercentValue;
	}

	@Override
	public String toString() {
		return "VpayData [vpayPercentId=" + vpayPercentId + ", designationId=" + designationId + ", vpayPercentValue="
				+ vpayPercentValue + "]";
	}
}
