package com.yum.comp.domain;

import java.io.Serializable;
import java.util.List;

public class GradeLookupData implements Serializable {
	
	private static final long serialVersionUID = -4476223500735119682L;

	private Integer gradeId;
	
	private String gradeDesc;
	
	private List<SalaryHeadData> salaryHead;
	
	public GradeLookupData() {
	}

	public GradeLookupData(Integer gradeId, String gradeDesc) {
		this.gradeId = gradeId;
		this.gradeDesc = gradeDesc;
	}

	public GradeLookupData(Integer gradeId, String gradeDesc, List<SalaryHeadData> salaryHead) {
		this.gradeId = gradeId;
		this.gradeDesc = gradeDesc;
		this.salaryHead = salaryHead;
	}

	public Integer getGradeId() {
		return gradeId;
	}

	public void setGradeId(Integer gradeId) {
		this.gradeId = gradeId;
	}

	public String getGradeDesc() {
		return gradeDesc;
	}

	public void setGradeDesc(String gradeDesc) {
		this.gradeDesc = gradeDesc;
	}

	public List<SalaryHeadData> getSalaryHead() {
		return salaryHead;
	}

	public void setSalaryHead(List<SalaryHeadData> salaryHead) {
		this.salaryHead = salaryHead;
	}
}
