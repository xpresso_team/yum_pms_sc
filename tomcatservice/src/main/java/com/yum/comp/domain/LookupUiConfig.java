package com.yum.comp.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LookupUiConfig implements Serializable {
	
	private static final long serialVersionUID = 2020867587880592189L;

	private Integer apprId;
	
	private String logonUser;
	
	private String statusMsg;
	
	private List<GradeLookupData> lookupDataList = new ArrayList<>(1);
	
	public LookupUiConfig() {
	}

	public LookupUiConfig(Integer apprId) {
		this.apprId = apprId;
	}

	public LookupUiConfig(Integer apprId, List<GradeLookupData> lookupDataList) {
		this.apprId = apprId;
		this.lookupDataList = lookupDataList;
	}

	public LookupUiConfig(Integer apprId, String logonUser, String statusMsg, List<GradeLookupData> lookupDataList) {
		this.apprId = apprId;
		this.logonUser = logonUser;
		this.statusMsg = statusMsg;
		this.lookupDataList = lookupDataList;
	}

	public Integer getApprId() {
		return apprId;
	}

	public void setApprId(Integer apprId) {
		this.apprId = apprId;
	}

	public String getLogonUser() {
		return logonUser;
	}

	public void setLogonUser(String logonUser) {
		this.logonUser = logonUser;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	public List<GradeLookupData> getLookupDataList() {
		return lookupDataList;
	}

	public void setLookupDataList(List<GradeLookupData> lookupDataList) {
		this.lookupDataList = lookupDataList;
	}
}
