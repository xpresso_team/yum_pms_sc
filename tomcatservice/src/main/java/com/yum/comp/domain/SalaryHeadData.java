package com.yum.comp.domain;

import java.io.Serializable;

import com.yum.pms.dao.CompLookupSalaryHeads;

public class SalaryHeadData implements Serializable {
	
	private static final long serialVersionUID = 267440976927193348L;
	
	private Integer lookupId;
	
	private Integer headId;
	
	private String headName;
	
	private String headDisplayName;
	
	private Double headValue;
	
	private Integer displayOrder;

	private Boolean isDisplay;  //from there, added by Jayanta for insert or update of new employee	
	
	private Double headAmount;
	
	private Integer appraisalPeriodId;
	
	private Integer employeeYearWiseId;
	
	private Integer empId;
	
	private Integer loginId;
	
	private Boolean isMadatory;
	
	public SalaryHeadData() {
	}

	public SalaryHeadData(CompLookupSalaryHeads salaryHead) {
		this.headId = salaryHead.getHeadId();
		this.headName = salaryHead.getHeadName();
		this.headDisplayName = salaryHead.getDisplayName();
		this.displayOrder = salaryHead.getDisplayOrder();
	}

	public Integer getLookupId() {
		return lookupId;
	}

	public void setLookupId(Integer lookupId) {
		this.lookupId = lookupId;
	}

	public Integer getHeadId() {
		return headId;
	}

	public void setHeadId(Integer headId) {
		this.headId = headId;
	}

	public String getHeadName() {
		return headName;
	}

	public void setHeadName(String headName) {
		this.headName = headName;
	}

	public String getHeadDisplayName() {
		return headDisplayName;
	}

	public void setHeadDisplayName(String headDisplayName) {
		this.headDisplayName = headDisplayName;
	}

	public Double getHeadValue() {
		return headValue;
	}

	public void setHeadValue(Double headValue) {
		this.headValue = headValue;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public Boolean getIsDisplay() {
		return isDisplay;
	}

	public void setIsDisplay(Boolean isDisplay) {
		this.isDisplay = isDisplay;
	}

	public Double getHeadAmount() {
		return headAmount;
	}

	public void setHeadAmount(Double headAmount) {
		this.headAmount = headAmount;
	}

	public Integer getAppraisalPeriodId() {
		return appraisalPeriodId;
	}

	public void setAppraisalPeriodId(Integer appraisalPeriodId) {
		this.appraisalPeriodId = appraisalPeriodId;
	}

	public Integer getEmployeeYearWiseId() {
		return employeeYearWiseId;
	}

	public void setEmployeeYearWiseId(Integer employeeYearWiseId) {
		this.employeeYearWiseId = employeeYearWiseId;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public Integer getLoginId() {
		return loginId;
	}

	public void setLoginId(Integer loginId) {
		this.loginId = loginId;
	}

	public Boolean getIsMadatory() {
		return isMadatory;
	}

	public void setIsMadatory(Boolean isMadatory) {
		this.isMadatory = isMadatory;
	}

	
}
