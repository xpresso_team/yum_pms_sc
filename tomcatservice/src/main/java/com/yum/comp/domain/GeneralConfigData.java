package com.yum.comp.domain;

import java.io.Serializable;
import java.util.List;

public class GeneralConfigData implements Serializable {

	private static final long serialVersionUID = 6004599590656336240L;

	private Integer apprId;
	
	private String logonUser;
	
	private String statusMsg;
	
	private List<ConfigData> configList;
	
	public GeneralConfigData() {
	}

	public GeneralConfigData(Integer apprId, List<ConfigData> configList) {
		this.apprId = apprId;
		this.configList = configList;
	}

	public Integer getApprId() {
		return apprId;
	}

	public void setApprId(Integer apprId) {
		this.apprId = apprId;
	}

	public String getLogonUser() {
		return logonUser;
	}

	public void setLogonUser(String logonUser) {
		this.logonUser = logonUser;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	public List<ConfigData> getConfigList() {
		return configList;
	}

	public void setConfigList(List<ConfigData> configList) {
		this.configList = configList;
	} 
}
