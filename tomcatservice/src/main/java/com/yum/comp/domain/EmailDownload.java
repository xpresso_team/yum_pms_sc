/**
 * 
 */
package com.yum.comp.domain;

import java.io.Serializable;
import java.util.Map;

/**
 * @author jayanta.biswas
 *
 */
public class EmailDownload implements Serializable {

	private static final long serialVersionUID = 6409626986521720014L;

	private String empIds;
	
	private Map<Integer, Boolean> permissions;
	
	private Integer apprId;
	
	private Boolean isEmail;
	
	private String purpose; 

	/**
	 * @return the empIds
	 */
	public String getEmpIds() {
		return empIds;
	}

	/**
	 * @param empIds the empIds to set
	 */
	public void setEmpIds(String empIds) {
		this.empIds = empIds;
	}

	/**
	 * @return the permissions
	 */
	public Map<Integer, Boolean> getPermissions() {
		return permissions;
	}

	/**
	 * @param permissions the permissions to set
	 */
	public void setPermissions(Map<Integer, Boolean> permissions) {
		this.permissions = permissions;
	}

	/**
	 * @return the apprId
	 */
	public Integer getApprId() {
		return apprId;
	}

	/**
	 * @param apprId the apprId to set
	 */
	public void setApprId(Integer apprId) {
		this.apprId = apprId;
	}

	/**
	 * @return the isEmail
	 */
	public Boolean getIsEmail() {
		return isEmail;
	}

	/**
	 * @param isEmail the isEmail to set
	 */
	public void setIsEmail(Boolean isEmail) {
		this.isEmail = isEmail;
	}

	/**
	 * @return the purpose
	 */
	public String getPurpose() {
		return purpose;
	}

	/**
	 * @param purpose the purpose to set
	 */
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	
	
}
