package com.yum.comp.domain;

import java.io.Serializable;

import com.yum.pms.dao.CompPayRangeCategoryMaster;
import com.yum.pms.dao.CompYearWisePayRange;

public class PayRangeData implements Serializable {
	
	private static final long serialVersionUID = -5519500232471995046L;
	
	private Integer yearWisePayRangeId;

	private Double minRange;
	
	private Double maxRange;
	
	private Double incrPercentage;
	
	private Integer payRangeCategoryId;
	
	private String payRangeCategoryName;
	
	private Integer displayOrder;
	
	private String displayName;
	
	public PayRangeData() {
	}

	public PayRangeData(Double minRange, Double maxRange, Double incrPercentage) {
		this.minRange = minRange;
		this.maxRange = maxRange;
		this.incrPercentage = incrPercentage;
	}
	
	public PayRangeData(CompYearWisePayRange payRange) {
		this.yearWisePayRangeId = payRange.getYearWisePayRangeId();
		this.minRange = payRange.getMinRange();
		this.maxRange = payRange.getMaxRange();
		this.incrPercentage = payRange.getIncreasePercentage();
	}
	
	public void setPayRangeMasterData(CompPayRangeCategoryMaster category) {
		this.payRangeCategoryId = category.getPayRangeCategoryId();
		this.payRangeCategoryName = category.getPayRangeCategoryName();
		this.displayOrder = category.getDisplayOrder();
		this.displayName = category.getDisplayName();
	}

	public Integer getYearWisePayRangeId() {
		return yearWisePayRangeId;
	}

	public void setYearWisePayRangeId(Integer yearWisePayRangeId) {
		this.yearWisePayRangeId = yearWisePayRangeId;
	}

	public Double getMinRange() {
		return minRange;
	}

	public void setMinRange(Double minRange) {
		this.minRange = minRange;
	}

	public Double getMaxRange() {
		return maxRange;
	}

	public void setMaxRange(Double maxRange) {
		this.maxRange = maxRange;
	}

	public Double getIncrPercentage() {
		return incrPercentage;
	}

	public void setIncrPercentage(Double incrPercentage) {
		this.incrPercentage = incrPercentage;
	}

	public Integer getPayRangeCategoryId() {
		return payRangeCategoryId;
	}

	public void setPayRangeCategoryId(Integer payRangeCategoryId) {
		this.payRangeCategoryId = payRangeCategoryId;
	}

	public String getPayRangeCategoryName() {
		return payRangeCategoryName;
	}

	public void setPayRangeCategoryName(String payRangeCategoryName) {
		this.payRangeCategoryName = payRangeCategoryName;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	@Override
	public String toString() {
		return "PayRangeData [Key=" + yearWisePayRangeId + ", minRange=" + minRange + ", maxRange="
				+ maxRange + ", incrPercentage=" + incrPercentage + "]";
	}
	
}
