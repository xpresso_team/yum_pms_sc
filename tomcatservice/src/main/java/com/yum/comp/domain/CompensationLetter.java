package com.yum.comp.domain;

import java.io.Serializable;

public class CompensationLetter implements Serializable {
	
	private static final long serialVersionUID = 6108977926986902782L;

	private Integer empId;
	
	private String empNme;
	
	private Integer apprId;
	
	private String statusMsg;
	
	private String apprYearDesc;
	
	private String reportName;
	
	private String htmlReportContent;
	
	private Integer supervisorId;
	
	public CompensationLetter() {
	}

	public CompensationLetter(Integer empId, Integer apprId) {
		this.empId = empId;
		this.apprId = apprId;
	}
	
	public CompensationLetter(Integer empId, Integer apprId, String apprYearDesc) {
		this.empId = empId;
		this.apprId = apprId;
		this.apprYearDesc = apprYearDesc;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getEmpNme() {
		return empNme;
	}

	public void setEmpNme(String empNme) {
		this.empNme = empNme;
	}

	public Integer getApprId() {
		return apprId;
	}

	public void setApprId(Integer apprId) {
		this.apprId = apprId;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	public String getApprYearDesc() {
		return apprYearDesc;
	}

	public void setApprYearDesc(String apprYearDesc) {
		this.apprYearDesc = apprYearDesc;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getHtmlReportContent() {
		return htmlReportContent;
	}

	public void setHtmlReportContent(String htmlReportContent) {
		this.htmlReportContent = htmlReportContent;
	}

	/**
	 * @return the supervisorId
	 */
	public Integer getSupervisorId() {
		return supervisorId;
	}

	/**
	 * @param supervisorId the supervisorId to set
	 */
	public void setSupervisorId(Integer supervisorId) {
		this.supervisorId = supervisorId;
	}
}
