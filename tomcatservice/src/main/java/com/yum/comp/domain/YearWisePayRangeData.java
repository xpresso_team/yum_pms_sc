package com.yum.comp.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class YearWisePayRangeData implements Serializable {

	private static final long serialVersionUID = 3445432342409362508L;

	private Integer apprId;
	
	private String logonUser;
	
	private String statusMsg;
	
	private List<GradeWisePayRange> gradeWisePayRangeList = new ArrayList<>(1);
	
	public YearWisePayRangeData() {
	}

	public YearWisePayRangeData(Integer apprId) {
		this.apprId = apprId;
	}

	public YearWisePayRangeData(Integer apprId, List<GradeWisePayRange> gradeWisePayRangeList) {
		this.apprId = apprId;
		this.gradeWisePayRangeList = gradeWisePayRangeList;
	}

	public Integer getApprId() {
		return apprId;
	}

	public void setApprId(Integer apprId) {
		this.apprId = apprId;
	}

	public String getLogonUser() {
		return logonUser;
	}

	public void setLogonUser(String logonUser) {
		this.logonUser = logonUser;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	public List<GradeWisePayRange> getGradeWisePayRangeList() {
		return gradeWisePayRangeList;
	}

	public void setGradeWisePayRangeList(List<GradeWisePayRange> gradeWisePayRangeList) {
		this.gradeWisePayRangeList = gradeWisePayRangeList;
	}

	@Override
	public String toString() {
		return "YearWisePayRangeData [apprId=" + apprId + ", logonUser=" + logonUser + ", statusMsg=" + statusMsg
				+ ", gradeWisePayRangeList Size=" + (gradeWisePayRangeList != null ? gradeWisePayRangeList.size() : 0) + "]";
	}
	
}
