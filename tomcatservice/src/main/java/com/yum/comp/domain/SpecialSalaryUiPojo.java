/**
 * 
 */
package com.yum.comp.domain;

import java.io.Serializable;

/**
 * @author jayanta.biswas
 *
 */
public class SpecialSalaryUiPojo implements Serializable {
	
	private static final long serialVersionUID = 2947388349946915047L;

	private Integer appraisalPeriord;
	
	private Integer nextAppraisalPeriord;
	
	private Integer empId;
	
	private String dataList;
	
	private String action;
	
	private String purpose;

	/**
	 * @return the appraisalPeriord
	 */
	public Integer getAppraisalPeriord() {
		return appraisalPeriord;
	}

	/**
	 * @param appraisalPeriord the appraisalPeriord to set
	 */
	public void setAppraisalPeriord(Integer appraisalPeriord) {
		this.appraisalPeriord = appraisalPeriord;
	}

	/**
	 * @return the nextAppraisalPeriord
	 */
	public Integer getNextAppraisalPeriord() {
		return nextAppraisalPeriord;
	}

	/**
	 * @param nextAppraisalPeriord the nextAppraisalPeriord to set
	 */
	public void setNextAppraisalPeriord(Integer nextAppraisalPeriord) {
		this.nextAppraisalPeriord = nextAppraisalPeriord;
	}

	/**
	 * @return the empId
	 */
	public Integer getEmpId() {
		return empId;
	}

	/**
	 * @param empId the empId to set
	 */
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	/**
	 * @return the dataList
	 */
	public String getDataList() {
		return dataList;
	}

	/**
	 * @param dataList the dataList to set
	 */
	public void setDataList(String dataList) {
		this.dataList = dataList;
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return the purpose
	 */
	public String getPurpose() {
		return purpose;
	}

	/**
	 * @param purpose the purpose to set
	 */
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	
	

}
