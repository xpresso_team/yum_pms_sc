/**
 * 
 */
package com.yum.comp.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jayanta.biswas
 *
 */
public class CompUiRevisedSalary implements Serializable {
	
	private static final long serialVersionUID = 3143441619328863390L;

	private List<CompensationUiConfig> revisedSalrayList = new ArrayList<>();
	
	private String curYear;
	
	private String nextYear;
	
	private String action;

	/**
	 * @return the revisedSalrayList
	 */
	public List<CompensationUiConfig> getRevisedSalrayList() {
		return revisedSalrayList;
	}

	/**
	 * @param revisedSalrayList the revisedSalrayList to set
	 */
	public void setRevisedSalrayList(List<CompensationUiConfig> revisedSalrayList) {
		this.revisedSalrayList = revisedSalrayList;
	}

	/**
	 * @return the curYear
	 */
	public String getCurYear() {
		return curYear;
	}

	/**
	 * @param curYear the curYear to set
	 */
	public void setCurYear(String curYear) {
		this.curYear = curYear;
	}

	/**
	 * @return the nextYear
	 */
	public String getNextYear() {
		return nextYear;
	}

	/**
	 * @param nextYear the nextYear to set
	 */
	public void setNextYear(String nextYear) {
		this.nextYear = nextYear;
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}


}
