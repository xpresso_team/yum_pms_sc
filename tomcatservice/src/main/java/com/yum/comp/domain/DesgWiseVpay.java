package com.yum.comp.domain;

import java.util.List;
import java.util.Map;

public class DesgWiseVpay {
	
	private Integer apprId;
	
	private String logonUser;
	
	private String statusMsg;
	
	private Map<Integer, String> designationList;
	
	private List<VpayData> vpayDataList;
	
	public DesgWiseVpay() {
	}

	public DesgWiseVpay(Integer apprId) {
		this.apprId = apprId;
	}

	public DesgWiseVpay(Integer apprId, List<VpayData> vpayDataList) {
		this.apprId = apprId;
		this.vpayDataList = vpayDataList;
	}

	public DesgWiseVpay(Integer apprId, List<VpayData> vpayDataList, Map<Integer, String> designationList) {
		this.apprId = apprId;
		this.vpayDataList = vpayDataList;
		this.designationList = designationList;
	}

	public Integer getApprId() {
		return apprId;
	}

	public void setApprId(Integer apprId) {
		this.apprId = apprId;
	}

	public String getLogonUser() {
		return logonUser;
	}

	public void setLogonUser(String logonUser) {
		this.logonUser = logonUser;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	public Map<Integer, String> getDesignationList() {
		return designationList;
	}

	public void setDesignationList(Map<Integer, String> designationList) {
		this.designationList = designationList;
	}

	public List<VpayData> getVpayDataList() {
		return vpayDataList;
	}

	public void setVpayDataList(List<VpayData> vpayDataList) {
		this.vpayDataList = vpayDataList;
	}

	@Override
	public String toString() {
		return "DesgWiseVpay [apprId=" + apprId + ", logonUser=" + logonUser + ", statusMsg=" + statusMsg
				+ ", designationList=" + designationList + ", vpayDataList=" + vpayDataList + "]";
	}
}
