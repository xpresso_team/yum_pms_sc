package com.yum.comp.domain;

import java.io.Serializable;

import com.yum.pms.dao.CompGeneralConfiguration;

public class ConfigData implements Serializable {

	private static final long serialVersionUID = -8255804740124469409L;

	private Integer id;
	
	private String headName;
	
	private Double headValue;
	
	private String displayHeadName;
	
	private Integer displayOrder;
	
	public ConfigData() {
	}

	public ConfigData(Integer id, String headName, Double headValue, String displayHeadName, Integer displayOrder) {
		this.id = id;
		this.headName = headName;
		this.headValue = headValue;
		this.displayHeadName = displayHeadName;
		this.displayOrder = displayOrder;
	}
	
	public ConfigData(CompGeneralConfiguration config) {
		this(config.getId(), config.getHeadName(), config.getHeadValue(), config.getDisplayHeadName(), config.getDisplayOrder());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getHeadName() {
		return headName;
	}

	public void setHeadName(String headName) {
		this.headName = headName;
	}

	public String getDisplayHeadName() {
		return displayHeadName;
	}

	public void setDisplayHeadName(String displayHeadName) {
		this.displayHeadName = displayHeadName;
	}

	public Double getHeadValue() {
		return headValue;
	}

	public void setHeadValue(Double headValue) {
		this.headValue = headValue;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}
	
}
