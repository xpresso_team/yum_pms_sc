package com.yum.comp.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Properties;

public class CompLetterUiData implements Serializable {
	
	private static final long serialVersionUID = 6536462244931896168L;

	private Integer apprId;
	
	private String statusMsg;
	
	private List<List<Properties>> empList;
	
	public CompLetterUiData() {
	}

	public CompLetterUiData(Integer apprId, List<List<Properties>> empList) {
		this.apprId = apprId;
		this.empList = empList;
	}

	public Integer getApprId() {
		return apprId;
	}

	public void setApprId(Integer apprId) {
		this.apprId = apprId;
	}

	public String getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(String statusMsg) {
		this.statusMsg = statusMsg;
	}

	public List<List<Properties>> getEmpList() {
		return empList;
	}

	public void setEmpList(List<List<Properties>> empList) {
		this.empList = empList;
	}
}
