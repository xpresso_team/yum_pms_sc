/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	config.toolbarGroups = [
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
		{ name: 'links', groups: [ 'links' ] },
		{ name: 'insert', groups: [ 'insert' ] },
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] }
	];

	//config.title = false;
	config.removeButtons = 'About,Maximize,ShowBlocks,Font,FontSize,Format,Styles,Image,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,JustifyRight,JustifyCenter,JustifyLeft,BidiLtr,BidiRtl,JustifyBlock,Language,Anchor,Unlink,Link,Blockquote,CreateDiv,Indent,Outdent,NumberedList,Superscript,Subscript,RemoveFormat,HiddenField,ImageButton,Button,Select,Textarea,TextField,Radio,Checkbox,Form,Scayt,SelectAll,Find,Replace,Undo,Redo,PasteFromWord,PasteText,Paste,Copy,Cut,Save,NewPage,Preview,Print,Templates,Source';
};
