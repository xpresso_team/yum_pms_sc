<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Login page</title>
		<!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" /> -->
		<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
		<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
	</head>

	<body>
		<div id="mainWrapper">
			<div class="logo-wrapper">
				<img src="<c:url value='/static/assets/images/logo.png' />" alt="Yum Performance Management System" class="" />
				<h1>Performance Management System</h1>
			</div>
			<c:url var="loginUrl" value="/login" />
			<form action="${loginUrl}" method="post" class="form-horizontal">
				<div class="input-fields">
					<c:if test="${param.error != null}">
						<div class="alert alert-danger">Invalid username and password.</div>
					</c:if>
					<c:if test="${param.logout != null}">
						<div class="alert alert-success">You have been logged out successfully.</div>
					</c:if>
					<div class="input-group">
						<input type="text" class="form-control" id="username" name="username" placeholder="Enter Username" required />
						<i class="fa fa-user"></i>
					</div>
					<div class="input-group">
						<input type="password" class="form-control" id="password" name="password" placeholder="Enter Password" required />
						<i class="fa fa-lock"></i>
					</div>
	
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				</div>	
				<!-- <div class="form-actions">
					<a href="#" class="forgot-password">Forgot Password?</a>
					<input type="submit" class="login-btn" value="Login">
				</div> -->
			</form>
		</div>

	</body>
</html>