<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<base href="<c:url value='/static/js/' />" >
<!-- Polyfills -->
<script src="https://unpkg.com/core-js/client/shim.min.js"></script>

<!-- Update these package versions as needed -->
<script src="https://unpkg.com/zone.js@0.8.4?main=browser"></script>
<script src="https://unpkg.com/systemjs@0.19.39/dist/system.src.js"></script>

<!-- This SystemJS configuration loads umd packages from the web -->
<script src="systemjs.config.server.js"></script>
<script>
  System.import('main.js').catch(function(err){ console.error(err); });
</script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"  rel="stylesheet"></link>
<link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
<title>Welcome to YUM PMS1</title>



</head>
<body>
	<div class="container">
		
		<div class="generic-container">
		<h1>Welcome to Yum PMS</h1>
		<%-- <%@include file="authheader.jsp" %> --%>
		<%-- 
		<p>
			Hello <b><c:out value="${pageContext.request.remoteUser}" /></b>
		</p> --%>
		
		<yumauth username="<c:out value="${pageContext.request.remoteUser}" />"></yumauth>
		
		<c:url var="logoutUrl" value="/logout" />
		<form class="form-inline" action="${logoutUrl}" method="post">
			<input type="submit" value="Log out" /> <input type="hidden"
				name="${_csrf.parameterName}" value="${_csrf.token}" />
		</form>
		
		<c:if test="${pageContext.request.userPrincipal.name != null}">
	<a href="javascript:document.getElementById('logout').submit()">Logout</a>
</c:if>

		</div>
		
	</div>
	
</body>
</html>
