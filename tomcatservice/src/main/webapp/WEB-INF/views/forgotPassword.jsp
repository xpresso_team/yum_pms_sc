<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <title>Forgot Password to YUM PMS</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="<c:url value='/static/favicon.ico' />" type="image/ico">
        <link href="<c:url value='/static/css/app.css' />" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet"> <!-- Path for Roboto -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"  rel="stylesheet" />
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type = "text/javascript">
            function validateEmail(email) { 
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            } 
            function continueOrNot() {
                if(validateEmail(document.getElementById('emailfield').value)){
                    return true;
                }else{ alert("Kindly enter a valide email address"); return false;}
            }
        </script>
    </head>
    <body class="yum-body-wapper">
        <div id="mainWrapper">
        	<div class="logo-wrapper">
                <img src="<c:url value='/static/assets/images/logo.png' />" alt="Yum Performance Management System" class="" />
                <h1>Performance Management System</h1>
            </div>
        	
        	
            <div id="form-olvidado">
                <c:url var="forgotPassUrl" value="/setForgotPass" />
                <form accept-charset="UTF-8" role="form" id="login-recordar" action="${forgotPassUrl}" method="post">
                    <div class="input-wapper">
                    	<h4 class="forgot-txt"> Forgot your password? </h4>
                        <span class="help-block">Enter the Email address you use to log in to your account </br>
                        We'll send you an email with a temporary password. </span>
                        <div class="input-content">
                            <input placeholder="Enter your registered Email" name="email" id="email" type="email" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" required>
                            <span class="fa">@</span>
                        </div>
                        <c:choose>
                        <c:when test="${failure != null}"> 
                           <div id="failure" title="Attention!" class="alert alert-danger alert-dismissable mt-3">Password couldn't be changed, please try again. <a href="#" class="close" data-dismiss="alert" aria-label="close">�</a></div>
                           <!--  <script type="text/javascript">
                                // alert("Password couldn't be changed , please try again ")
                                $( "#failure" ).dialog();
                                $( function() {
                                    $( "#failure" ).dialog({
                                        buttons: {
                                            OK: function() {$(this).dialog("close");}
                                        },
                                        title: "failure",
                                        position: {
                                            my: "center",
                                            at: "center"
                                        }
                                    });
                                } );
                            </script>  -->
                        </c:when>
                        <c:when test="${failureNoEmail != null}">
                         <div id="failureNoEmail" title="Attention!" class="alert alert-danger alert-dismissable mt-3">No user exists with such an email . Please enter a valid email and try again. <a href="#" class="close" data-dismiss="alert" aria-label="close">�</a></div> 
                            <!-- <script type="text/javascript">
                                //alert("You have a temporary password set ,Please change your password to continue ...");
                                $( function() {
                                    $( "#failureNoEmail" ).dialog({
                                        buttons: {
                                            OK: function() {$(this).dialog("close");}
                                        },
                                        title: "Attention",
                                        position: {
                                            my: "center",
                                            at: "center"
                                        }
                                    });
                                } );
                                console.log("Came here  success");
                            </script>  -->
                        </c:when>
                        </c:choose>
                    </div>
                    <div class="form-actions">
                        <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
                        <div class="row">
                        	<div class="col-md-6 col-sm-12">
                        		<button type="submit" class="login-btn blue-btn" id="btn-olvidado">Submit</button>
                        	</div>
                        	<div class="col-md-6 col-sm-12">
                        		<c:url var="loginUrl" value="/login" />
                            	<a href="${loginUrl}" data-toggle="modal" class="login-btn">Back</a>
                        	</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
	</body>
</html>
