<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
        <head>
        <title>Change Password to YUM PMS</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="<c:url value='/static/favicon.ico' />" type="image/ico">
        <link href="<c:url value='/static/css/app.css' />" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet"><!-- Path for Roboto -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> <!--CSS file path for Material Icons -->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript">
        	window.onload = function(){
        		var user = localStorage.getItem('user');
        		document.getElementById("welcome_user").innerHTML = 'Welcome '+ user;
        	}
			function pswderr() {
				var pswd1 = document.getElementById("password").value;
				var pswd2 = document.getElementById("confirm_password").value;
				if (pswd1 !== pswd2) {
					alert("Password and password verification do not match. Retry");
					//document.getElementById("alarm").innerHTML = "Password and password verification do not match. Retry";
					return false;
				} else {
					//document.getElementById("alarm").innerHTML = "";
					return true;
				}
			}
        </script>
        </head>
        <body class="yum-body-wapper">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12">
              <header class="row">
                <h1 class="logo-wapper"> <img src="<c:url value='/static/assets/images/yum-logo.png' />" alt="" class="img-fluid company-logo" /> <span>Performance Management System</span> </h1>
                <nav class="navbar navbar-fixed-top topnav col-sm text-right">
                  <div class="collapse navbar-toggleable-xs clearfix text-center">Header Panel</div>
                  <form action="./logout" method="POST" name="logout-form" ngNoForm class="mb-0">
                    <ul class="tp-nav">
                      <li> <span class="welcome-user" id="welcome_user"></span> <span class="user-icon"><i class="material-icons">account_circle</i></span> </li>
                      <li title="Home">
                        <div class="setting-icon"><a href="./" ><i class="fa fa-home" aria-hidden="true"></i></a></div>
                      </li>
                      <li>
                        <button title="Logout" type="submit" class="logout"><i class="fa fa-sign-out" aria-hidden="true"></i></button>
                      </li>
                    </ul>
                    <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
                  </form>
                </nav>
              </header>
              <div id="changepasswordWrapper">
                <div id="login">
                  <c:url var="changePassUrl" value="/setChangedPass" />
                  <form action="${changePassUrl}" method="post" class="form-horizontal"  onsubmit="return pswderr(this)" >
                    <%-- <c:if test="${success != null}">
                    <script>alert("Password has been changed . Please Login again using your new Password");
                    </script>
                    </c:if>
                    --%>
                    <c:choose>
                      <c:when test="${failure != null}">
                        <div id="failure" title="Warning!" class="alert alert-danger alert-dismissable mt-3">Password couldn't be changed, please try again. <a href="#" class="close" data-dismiss="alert" aria-label="close">�</a></div>
                        <!--<script type="text/javascript">
                            $( "#failure" ).dialog();
                            $( function() {
                            $( "#failure" ).dialog({
                            buttons: {
                            OK: function() {$(this).dialog("close");}
                            },
                            title: "Error",
                            position: {
                            my: "center",
                            at: "center"
                            }	
                            });
                            } );
                        </script>--> 
                      </c:when>
                      <c:when test="${passwordChanged != null}">
                        <div id="passwordChanged" title="Attention!" class="alert alert-success alert-dismissable mt-3">Password Successfully changed. <a href="#" class="close" data-dismiss="alert" aria-label="close">�</a></div>
                        <!--<script type="text/javascript">
                            $( function() {
                            $( "#passwordChanged" ).dialog({
                            buttons: {
                            OK: function() {window.location.href = "home";}
                            },
                            title: "Success",
                            position: {
                            my: "center",
                            at: "center"
                            },
                            close: function(event, ui) { window.location.href = "home"; }
                            });
                            } );
                            console.log("Came here passwordChanged");
                        </script> --> 
                      </c:when>
                      <c:when test="${failurePassMisMatch != null}">
                        <div id="failurePassMisMatch" title="Warning!" class="alert alert-danger alert-dismissable mt-3">Please enter a correct current password. <a href="#" class="close" data-dismiss="alert" aria-label="close">�</a></div>
                        <!--<script type="text/javascript">
                            $( function() {
                            $( "#failurePassMisMatch" ).dialog({
                            buttons: {
                            OK: function() {$(this).dialog("close");}
                            },
                            title: "Error",
                            position: {
                            my: "center",
                            at: "center"
                            }
                            });
                            } );
                            console.log("Came here  success");
                        </script> --> 
                      </c:when>
                    </c:choose>
                    <div class="input-wapper">
                      <%-- <h2 class="text-center" style="color: #5cb85c;"> <strong> Hello <b><c:out value="${pageContext.request.remoteUser}" /></b> Please enter your new password. </strong></h2><hr /> --%>
                      <div class="input-content mb-3">
                        <input type="password" placeholder="Current Password" id="currentPassword" name="currentPassword" onkeyup='check();' required>
                        <i class="fa fa-lock"></i> </div>
                      <div class="input-content mb-3">
                        <input type="password" placeholder="New Password" id="password" name="password" onkeyup='check();' required>
                        <i class="fa fa-lock"></i> </div>
                      <div class="input-content mb-3">
                        <input type="password" placeholder="Confirm New Password" id="confirm_password" name="confirm_password" onkeyup='check();' required>
                        <i class="fa fa-lock"></i> </div>
                    </div>
                    <div class="form-actions">
                      <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
                      <button type="submit" class="login-btn blue-btn" onClick="return pswderr();"> Submit </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
</body>
</html>