<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.io.FileNotFoundException,java.util.Properties,java.io.InputStream,java.io.IOException" %>
<!DOCTYPE html>
<html>

<head>
    <title>Welcome to YUM PMS</title>
    <base href="./"> <!-- After replacing this folder with index.html from angular dist folder set "href" to "./static/" -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="icon" href="./static/favicon.ico" type="image/izco">
    <!-- CSS file path for Font Awesome -->
    <link href="./static/assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- Path for Roboto -->
    <link href="./static/assets/css/css.css" rel="stylesheet">
    <!--CSS file path for Material Icons -->
    <link href="./static/assets/css/icon.css" rel="stylesheet">
    <link rel="stylesheet" href="./static/assets/css/styles.css">
	<link rel="stylesheet" href="./static/assets/css/jquery-ui.min.css">
    <!-- Polyfill(s) for older browsers -->
    <!-- <script src="node_modules/core-js/client/shim.min.js"></script>

    <script src="node_modules/zone.js/dist/zone.js"></script>
    <script src="node_modules/systemjs/dist/system.src.js"></script> -->
    <!-- Polyfills -->
    <script src="./static/assets/js/shim.min.js"></script>
    <!-- Update these package versions as needed -->
    <!-- <script src="https://unpkg.com/zone.js@0.8.4?main=browser"></script> -->
    <script src="./static/assets/js/system.src.js"></script>
    <script type="text/javascript" src="./static/assets/js/systemjs.config.js"></script>
    <!-- This SystemJS configuration loads umd packages from the web -->
    <!-- <script src="systemjs.config.server.js"></script> -->
    <!-- <script type="text/javascript">
      System.import('./assets/js/main.js').catch(function(err){ console.error(err); });
    </script> -->
    <script src="./static/assets/js/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="./static/assets/js/config.js"></script>	
    <script src="./static/assets/js/jquery.min.js"></script>
    <!--<script type="text/javascript" src="./static/assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="./static/assets/js/jquery-ui.min.js"></script>-->
	<style type="text/css">
        .app-loading-txt {
            font-size: 1.25rem;
            font-weight: 700;
            line-height: 3.125rem;
            position: absolute;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            width: 150px;
            height: 50px;
            margin: auto;
            color: #555;
        }

        .app-loading-txt>div {
            position: absolute;
            right: 0px;
            top: 5px;
        }

        }

        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }
            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
	<!-- <link href="./static/styles.bbb99f6e284c89450231.bundle.css" rel="stylesheet" /> -->
</head>

<body class="yum-body-wapper">
<%
Properties prop = new Properties();
InputStream input = null;
try {
	input =  this.getClass().getResourceAsStream("/application.properties");
	prop.load(input);
} catch (FileNotFoundException e1) {
	e1.printStackTrace();
} catch (IOException e) {
	e.printStackTrace();
}
%>
<c:set var="myVar" value='${sessionScope.userEmail}' />
	<!-- Keep this component as it is after replaceing this file with the updated index.html from angular dist folder -->
    <%-- <yumauth username="<c:out value="${pageContext.request.remoteUser}"/>" paramName ="<c:out value="${_csrf.parameterName}"/>" paramValue ="<c:out value="${_csrf.token}"/>" > --%>
    <%if(prop.getProperty("ldap.enabled").equals("true")){ %>
    <yumauth username="<c:out value="${sessionScope.userEmail}"/>" paramName ="<c:out value="${_csrf.parameterName}"/>" paramValue ="<c:out value="${_csrf.token}"/>" >
         <% System.out.println("LDAP enabled"); %>
    <%}else{ %>
         <% 
         	System.out.println("LDAP disabled"); 
         	if(session.getAttribute("userEmail")==null)
         		session.setAttribute("userEmail",request.getParameter("email"));
      
         %>
    <%-- <yumauth username="<c:out value="<%=request.getParameter(\"email\") %>"/>" paramName ="<c:out value="${_csrf.parameterName}"/>" paramValue ="<c:out value="${_csrf.token}"/>" > --%>
		<yumauth username="<c:out value="<%=session.getAttribute(\"userEmail\") %>"/>" paramName ="<c:out value="${_csrf.parameterName}"/>" paramValue ="<c:out value="${_csrf.token}"/>" >    
    <%} %>
     <% System.out.println("Inside jsp"); %>
     <% System.out.println(pageContext.findAttribute("myVar") ); %>
		<div style="background-color: #fff; position: fixed; width: 100%; height: 100%">
            <div class="app-loading-txt" style="color: #555">Loading
                <div style="border: 4px solid #d1d1d1;
                            border-radius: 50%;
                            border-top: 4px solid #555;
                            width: 40px;
                            height: 40px;
                            -webkit-animation: spin 2s linear infinite;
                            animation: spin 2s linear infinite"></div>
            </div>
        </div>
	</yumauth>
     <!-- <yumauth username="prasanna.kumar@yum.com" paramName ="_csrf" paramValue ="36138678-0847-42a5-8a43-dfa9ec6a0e81" >Loading AppComponent content here ...</yumauth> --> 
    <!-- ======================= -->
    <!--  Making a logout form which can be used by angular.. -->
    
    <script type="text/javascript" src="./static/inline.bundle.js"></script>
    <script type="text/javascript" src="./static/polyfills.bundle.js"></script>
    <script type="text/javascript" src="./static/scripts.bundle.js"></script>
    <script type="text/javascript" src="./static/styles.bundle.js"></script>
    <script type="text/javascript" src="./static/vendor.bundle.js"></script>
    <script type="text/javascript" src="./static/main.bundle.js"></script>
</body>
</html>