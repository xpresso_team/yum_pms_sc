<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <title>Change Password to YUM PMS</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="<c:url value='/static/favicon.ico' />" type="image/ico">
        <link href="<c:url value='/static/css/app.css' />" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet"><!-- Path for Roboto -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> <!--CSS file path for Material Icons -->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript">
			function pswderr() {
				var pswd1 = document.getElementById("password").value;
				var pswd2 = document.getElementById("confirm_password").value;
				if (pswd1 !== pswd2) {
					alert("Password and password verification do not match. Retry");
					//document.getElementById("alarm").innerHTML = "Password and password verification do not match. Retry";
					return false;
				} else {
					//document.getElementById("alarm").innerHTML = "";
					return true;
				}
			}
        </script>
    </head>
    <body class="yum-body-wapper">
    	<div id="mainWrapper">
        	<div class="logo-wrapper">
                <img src="<c:url value='/static/assets/images/logo.png' />" alt="Yum Performance Management System" class="" />
                <h1>Performance Management System</h1>
            </div>
           
        <div id="login">
        <c:url var="forceUpdatePassUrl" value="/setForceUpdatedPass" />
        <form action="${forceUpdatePassUrl}" method="post" onsubmit="return pswderr(this)" >
        <%-- <c:if test="${success != null}">
        <script>alert("Password has been changed . Please Login again using your new Password");
        </script>
        </c:if>
        --%>
        <c:choose>
            <c:when test="${failure != null}">
            	<div id="failure" title="Attention!" class="alert alert-danger alert-dismissable mt-3"> Password couldn't be changed , please try again </div> 
				<!--<script type="text/javascript">
					// alert("Password couldn't be changed , please try again ")
					
					$( "#failure" ).dialog();
					
					$( function() {
					$( "#failure" ).dialog({
					buttons: {
					OK: function() {$(this).dialog("close");}
					},
					title: "Attention",
					position: {
					my: "center",
					at: "center"
					}	
					});
					} );
                </script>--> 
            </c:when>
            <c:when test="${success == null}">
            	<div id="success" title="Attention!" class="alert alert-success alert-dismissable mt-3"> You have a temporary password set ,Please change your password to continue ... </div> 
				<!--<script type="text/javascript">
					//alert("You have a temporary password set ,Please change your password to continue ...");
					$( function() {
					$( "#success" ).dialog({
					buttons: {
					OK: function() {$(this).dialog("close");}
					},
					title: "Success",
					position: {
					my: "center",
					at: "center"
					}
					});
					} );
					console.log("Came here  success");
                </script>--> 
            </c:when>
        </c:choose>
        	<div class="input-wapper">
                <%-- <h2>Hello <c:out value="${pageContext.request.remoteUser}" /></h2> --%>
                <h4>Please enter your new password.</h4>
                <div class="input-content mb-3">
                    <input type="password" placeholder="Password" id="password" name="password" onkeyup='check();' required>
                    <i class="fa fa-lock"></i>
                </div>
                <div class="input-content mb-3">
                	<input type="password" placeholder="Confirm Password" id="confirm_password" name="confirm_password" onkeyup='check();' required>
                	<i class="fa fa-lock"></i>
                </div>
                <input type="hidden" name="${_csrf.parameterName}"  value="${_csrf.token}" />
            </div>
            <div class="form-actions">
                <button type="submit" class="login-btn" onClick="return pswderr();"> Submit </button>
            </div>
        </form>
        </div>
        </div>
    </body>
</html>