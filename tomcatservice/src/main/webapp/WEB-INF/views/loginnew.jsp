<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
    	<title>Login YUM PMS</title>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link rel="icon" href="<c:url value='/static/favicon.ico' />" type="image/ico">
        <link href="<c:url value='/static/css/app.css' />" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet"> <!-- Path for Roboto -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"  rel="stylesheet" />
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
    </head>
    <body class="yum-body-wapper">
    	<div id="mainWrapper">
            <div class="logo-wrapper">
                <img src="<c:url value='/static/assets/images/logo.png' />" alt="Yum Performance Management System" class="" />
                <h1>Performance Management System</h1>
            </div>
            <div id="login">
            	<c:url var="loginUrl" value="/login" />
                <form action="${loginUrl}" method="post" class="form-horizontal">
                	<div class="input-wapper">
                        <c:if test="${param.error != null}">
                            <div class="alert alert-danger alert-dismissable mb-3">Invalid username and password.  <a href="#" class="close" data-dismiss="alert" aria-label="close">�</a></div>
                        </c:if>
                        <c:if test="${param.logout != null}">
                            <div class="alert alert-success alert-dismissable mb-3">You have been logged out successfully. <a href="#" class="close" data-dismiss="alert" aria-label="close">�</a></div>
                        </c:if>
                        <c:if test="${passwordChanged != null}">
                        	 <div id="passwordChanged" title="Attention!" class="alert alert-danger alert-dismissable">Password has been changed. Please Login again using your new Password. <a href="#" class="close" data-dismiss="alert" aria-label="close">�</a></div>
                            <!--<script type="text/javascript">
                                //alert("Password has been changed . Please Login again using your new Password")
                                /* $( function() {
                                $( "#passwordChanged" ).dialog();
                                    $( "#passwordChanged" ).dialog({
                                        buttons: {
                                            OK: function() {$(this).dialog("close");}
                                        },
                                        title: "Success",
                                        position: {
                                            my: "center",
                                            at: "center"
                                        }
                                    });
                                
                                } );
                                console.log("Came here  2"); */
                            </script>-->			
                        </c:if>
                        <c:if test="${passSuccess != null}">
                        	<div id="passSuccess" title="Attention!" class="alert alert-success alert-dismissable">An email has been sent to your registered email address with a temporary password. Please use it to login again. <a href="#" class="close" data-dismiss="alert" aria-label="close">�</a></div>
							<!--<script type="text/javascript">
                                //alert("An email has been sent to your registered email address with a temporary password. Please use it to login again.");
                                $( function() {
                                    $( "#passSuccess" ).dialog({
                                        buttons: {
                                            OK: function() {$(this).dialog("close");}
                                        },
                                        title: "Success",
                                        position: {
                                            my: "center",
                                            at: "center"
                                        }
                                    });
                                } );
                                console.log("Came here");
                            </script>
-->                        </c:if>
                        <div class="input-content">
                            <input type="text" id="username" placeholder="User Name" name="username" />
                            <i class="fa fa-user"></i>
                        </div>
                        <div class="input-content">
                        	<input type="password" placeholder="Password" id="password" name="password" />
                            <i class="fa fa-lock"></i>
                        </div>
                    </div>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <div class="form-actions">
                        <%-- <div class="row">
                        	<!-- <div class="col-sm-6"><input type="checkbox" name="check" checked> Remember Me</div> -->
                        	<div class="col-sm-12">
                            	<c:url var="forgetPassUrl" value="/forgotPassword" />
                            	<a href="${forgetPassUrl}" data-toggle="modal" class="forgot-password"> Forgot Password? </a>
                            </div>
                        </div> --%>
                        <button type="submit" class="login-btn">Login</button>
                	</div>
                </form>
           </div> 
        </div>
    </body>
</html>