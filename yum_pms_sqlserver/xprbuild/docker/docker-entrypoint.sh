#! /bin/bash

runWithDelay () {
    sleep $1;
    shift;
    "${@}";
}

runWithDelay 20 /opt/mssql-tools/bin/sqlcmd -H localhost -U sa -P "$SA_PASSWORD" -i /opt/data.sql &

/opt/mssql/bin/sqlservr

